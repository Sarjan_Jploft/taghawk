//
//  TutorialVCViewController.swift
//  TagHawk
//
//  Created by Appinventiv on 18/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class TutorialVC : BaseVC {

    //MARK:- IBOutlets
    @IBOutlet weak var tutorialCollectionView: UICollectionView!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    //MARK:- Varialbel:
    let titleArray : [String] = ["Sell & Buy Products!","Sell & Buy Products!","Sell & Buy Products!"]
    let descArray : [String] = ["Never ever think of giving up.\nWinners never quit and quiters\n never win", "Never ever think of giving up.\nWinners never quit and quiters\n never win", "Never ever think of giving up.\nWinners never quit and quiters\n never win"]

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    @IBAction func skipButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = LoginVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if self.tutorialCollectionView.contentOffset.x == 0{
        
            self.tutorialCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
      
        }else if self.tutorialCollectionView.contentOffset.x == screenWidth{
        
        self.tutorialCollectionView.scrollToItem(at: IndexPath(item: 2, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
  
    }else{
        let vc = LoginVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- Private functions
private extension TutorialVC{
    
    func setUpSubView(){
        pageControl.numberOfPages = 3
        pageControl.isUserInteractionEnabled = false
        self.configureCollectionView()
        self.nextButton.round(radius: 10)
    }
    
    func setColorAndFontsOfUiElements(){
        
    }
    
    func configureCollectionView(){
        self.tutorialCollectionView.registerNib(nibName: TutorialCollectionViewCell.defaultReuseIdentifier)
        self.tutorialCollectionView.showsHorizontalScrollIndicator = false
        self.tutorialCollectionView.isPagingEnabled = true
        self.tutorialCollectionView.delegate = self
        self.tutorialCollectionView.dataSource = self
    }
    
    func setNextButtonTitle(){
        let title = self.tutorialCollectionView.contentOffset.x >= screenWidth * 2 ? LocalizedString.Let_Hawk.localized : LocalizedString.Next.localized
        self.nextButton.setTitle(title)
        
      self.skipButton.isHidden =  self.tutorialCollectionView.contentOffset.x >= screenWidth * 2
//
//        if self.tutorialCollectionView.contentOffset.x <= screenWidth {
//            pageControl.currentPage = 1
//        }else if self.tutorialCollectionView.contentOffset.x > screenWidth && self.tutorialCollectionView.contentOffset.x <= 2*screenWidth {
//            pageControl.currentPage = 2
//        } else {
//            pageControl.currentPage = 3
//        }
    }
    
}

//MARK:- Configure Navigation
private extension TutorialVC{
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}


extension TutorialVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.titleArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getTutorialCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getTutorialCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TutorialCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? TutorialCollectionViewCell else {
            fatalError("Could not dequeue TutorialCell at index \(indexPath) in LoginVC")
        }
        
        cell.populateData(title: self.titleArray[indexPath.item], description: self.descArray[indexPath.item])
        return cell
    }
}

//MARK: scrolview delegate
extension TutorialVC  {
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
      self.setNextButtonTitle()
    }
 
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       self.setNextButtonTitle()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.setNextButtonTitle()
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}

