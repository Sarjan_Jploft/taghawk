//
//  SignUpVC.swift
//  TagHawk
//
//  Created by Appinventiv on 22/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

struct UserInput {
    
    var fullName : String
    var email : String
    var password : String
    var confirmPassword : String
    var invitationCode : String
    var profileImage : String
    var fbId : String
    var socialLoginType : String
    var firstName: String
    var lastName: String
    
    init(){
        fullName = ""
        email = ""
        password = ""
        confirmPassword = ""
        invitationCode = ""
        socialLoginType = ""
        fbId = ""
        profileImage = ""
        firstName = ""
        lastName = ""
    }
}

class SignUpVC : BaseVC {

    //MARK:- IBOutlets
    @IBOutlet weak var signUpTableView: UITableView!
    @IBOutlet weak var tableViewBackView: UIView!
    @IBOutlet weak var signUpButton: UIButton!
//    @IBOutlet weak var facebookButton : UIButton!
    @IBOutlet weak var tableFooterView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var alreadyMemberButton: UIButton!
    @IBOutlet weak var loginForMoreFunctionsLabel: UILabel!
    @IBOutlet weak var tableBackViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var exploreAsGuestButton: UIButton!
    
    
    
    @IBOutlet weak var facebookButton: UIButton!
    
    
    
    //MARK:- Variables
    private let fieldsHeadingArray = ["\(LocalizedString.firstName.localized)*", "\(LocalizedString.lastName.localized)*" ,"\(LocalizedString.Email_Id.localized)*", "\(LocalizedString.password.localized)*", LocalizedString.Invitation_Code_Optional.localized]

    private var validationStateArray : [(type : TagHawkTextFieldTableViewCell.TextFieldType,state : TextFieldState)] = [
        (TagHawkTextFieldTableViewCell.TextFieldType.firstName,TextFieldState.none),
        (TagHawkTextFieldTableViewCell.TextFieldType.lastName,TextFieldState.none),
        (TagHawkTextFieldTableViewCell.TextFieldType.email,TextFieldState.none),
        (TagHawkTextFieldTableViewCell.TextFieldType.password,TextFieldState.none),
        (TagHawkTextFieldTableViewCell.TextFieldType.invitationCode,TextFieldState.none),
    ]
    
    
    private var userInput = UserInput()
    private let controler = SignUpControler()
    private let socialControler = SocialLoginController()
    private let controlerLg = LoginControler()
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }

    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
        self.controlerLg.delegate = self
        self.socialControler.socialServiceDelegate = self
        self.socialControler.checkFbLoginDelegate = self
        self.socialControler.delegate = self
    }
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.signUp(userInput: self.userInput)
        
       // self.socialControler.fbLogin()
    }
    
    @IBAction func alreadyAMember(_ sender: UIButton) {
        view.endEditing(true)
       // self.navigationController?.popViewController(animated: true)
        
        let vc = LoginVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.navigationController?.pushViewController(vc, animated: true)
    }
  
    @IBAction func guestButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
    }
    
    @IBAction func btnFacebookClicked(_ sender: UIButton) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.socialControler.fbLogin()
        
        
    }
    
    
    
    @IBAction func exploreAsGuestButtonTapped(_ sender: UIButton) {
           view.endEditing(true)
           if !AppNetworking.isConnectedToInternet {
               CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
               return
           }
           self.controlerLg.loginAsAGuest()
       }
    
    
    
    
}

extension SignUpVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.tableViewBackView.round(radius: 10)
        self.signUpButton.round(radius: 10)
     
        self.facebookButton.round(radius: 10)
        
        self.facebookButton.setAttributes(title: LocalizedString.Connect_with_Facebook.localized, font: AppFonts.Galano_Medium.withSize(13), titleColor: AppColors.blueTextColor, backgroundColor: AppColors.faceBookBackColor)
        
        self.loginForMoreFunctionsLabel.setAttributes(text: LocalizedString.Let_Start_With_Register.localized, font: AppFonts.Galano_Medium.withSize(18), textColor: UIColor.white)
        
        self.signUpButton.setAttributes(title: LocalizedString.Sign_Up.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
        
        let attrTitle = LocalizedString.Already_A_Member_Login.localized.attributeBoldStringWithUnderline(stringsToUnderLine: [LocalizedString.login.localized], size: 15, strColor: UIColor.black, font: AppFonts.Galano_Bold.withSize(15))
        self.alreadyMemberButton.setAttributedTitle(attrTitle, for: UIControl.State.normal)
        
         self.exploreAsGuestButton.setAttributes(title: LocalizedString.Explore_As_Guest.localized, font: AppFonts.Galano_Semi_Bold.withSize(20), titleColor: UIColor.white)
        
        
        self.configureTableView()
        adjuestLayout()
    }
    
    //MARK:- Adjuest layout
    func adjuestLayout(){
//       let tableFooterHeight : CGFloat = Display.typeIsLike == .iphone5
//        ? 65 : 100
         self.tableFooterView.frame.size.height = 150//tableFooterHeight
        
        if Display.typeIsLike == .iphone5{
            
            self.tableBackViewHeight.constant = 375
            
        }else if Display.typeIsLike == .iphone6{
            
            self.tableBackViewHeight.constant = 445
            
        }else if Display.typeIsLike == .iphone6plus{
            
            self.tableBackViewHeight.constant = 510
            
        }else if Display.typeIsLike == .iphoneX{
            
            self.tableBackViewHeight.constant = 520
            
        }else if Display.typeIsLike == .iPhoneXsOrXROrXi{
            
            self.tableBackViewHeight.constant = 540
            
        }else{
            
            self.tableBackViewHeight.constant = 400
            
        }
        
//        self.tableBackViewHeight.constant = Display.typeIsLike == .iphone5 ? 375 : 400
//        self.tableBackViewHeight.constant = Display.typeIsLike == .iphone6 ? 450 : 400
//        self.tableBackViewHeight.constant = Display.typeIsLike == .iphone6plus ? 440 : 400
//        self.tableBackViewHeight.constant = Display.typeIsLike == .iphoneX ? 520 : 400
//        self.tableBackViewHeight.constant = Display.typeIsLike == .iPhoneXsOrXROrXi ? 520 : 400
        print("final height",self.tableBackViewHeight.constant)
        print("screen size",UIScreen.main.bounds)
    }
    
    //MARK:- Configure tableview
    func configureTableView(){
        self.signUpTableView.delegate = self
        self.signUpTableView.dataSource = self
        let inset : CGFloat = Display.typeIsLike == .iphone5 ? 10 : 20
        self.signUpTableView.isScrollEnabled = true
        self.signUpTableView.contentInset = UIEdgeInsets(top: inset, left: 0, bottom: inset, right: 0)
        self.registerNib()
    }
    
    func registerNib(){
        self.signUpTableView.registerNib(nibName: TagHawkTextFieldTableViewCell.defaultReuseIdentifier)
//        self.signUpTableView.registerCell(with: FirstLastNameCell.self)
    }
    
    //MARK:- Open email popup
    func openEmailPopUp(userInput : UserInput) {
        let vc = EnterEmailPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.userinput = userInput
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
}



//MARK:- Configure navigation
private extension SignUpVC {
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

//MARK:- Get email back
extension SignUpVC : GetEmailBack {
    
    func getEmailBack(userInput : UserInput) {
        self.socialControler.sociialLogin(userInput: userInput)
    }
    
}


//MARK:_ Table view datasource and delegates
extension SignUpVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldsHeadingArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TagHawkTextFieldTableViewCell.defaultReuseIdentifier) as? TagHawkTextFieldTableViewCell else {
            fatalError("SettingsVC....\(TagHawkTextFieldTableViewCell.defaultReuseIdentifier) cell not found") }
        cell.delegate = self
        cell.textField.placeholder = fieldsHeadingArray[indexPath.row]
        cell.textFieldType = self.validationStateArray[indexPath.row].type
        cell.didShowError(state: self.validationStateArray[indexPath.row].state)
        cell.populateText(type: self.validationStateArray[indexPath.row].type, userInput: self.userInput)
        return cell
    }
    
    
}


//MARK:- TextField Delegats
extension SignUpVC : CustomFieldDelagate, UITextFieldDelegate {

    func textFieldShouldReturn(type: TagHawkTextFieldTableViewCell.TextFieldType) {
        
        switch type {
            
        case .invitationCode, .password:
            self.view.endEditing(true)
            if !AppNetworking.isConnectedToInternet {
                CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                return
            }
            self.controler.signUp(userInput: self.userInput)
            
        default:
            self.view.endEditing(true)
        }
    }
    
 
    func texfieldDidChange(type: TagHawkTextFieldTableViewCell.TextFieldType, text: String?) {
        
        guard let txt = text else { return }

        switch type {
        case .fullName:
             self.userInput.fullName = txt
        case .firstName:
            self.userInput.firstName = txt
        case .lastName:
            self.userInput.lastName = txt
        case .email:
            self.userInput.email = txt
        case .password:
            self.userInput.password = txt
        case .invitationCode:
            self.userInput.invitationCode = txt
        default : break
        }
    }
}

//MARK:- Popup button tapped
extension SignUpVC  {
    override func okButtonTapped() {
//        AppNavigator.shared.goToHomeVC()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInMapVC") as! LogInMapVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Signup controler
extension SignUpVC : SignUpControllerDelegate {
    
    func willSignUp(){
         self.view.showIndicator()
    }
    
    func signUpSuccess(user: UserProfile){
         self.view.hideIndicator()
        //AppNavigator.shared.goToHomeVC()
        
        self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.signUpSuccess)
    }
    
    func signUpFailed(message: String){
         self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
    
    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState){
        
        if let ind = self.validationStateArray.firstIndex(where: { (value) -> Bool in
            value.type == textFieldType
        }){
            self.validationStateArray[ind].state = textFieldState
            self.signUpTableView.reloadRows(at: [IndexPath(row: ind, section: 0)], with: UITableView.RowAnimation.none)
            self.signUpTableView.scrollToRow(at: IndexPath(row: ind, section: 0), at: .top, animated: true)
        }
    }
}


//MARK:- Login controler
extension SignUpVC : LoginControllerDelegate{
    
    func willLogin(){
        self.view.showIndicator()
    }
    
    func loginSuccess(user: UserProfile){
        self.view.hideIndicator()
//        AppNavigator.shared.goToHomeVC()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInMapVC") as! LogInMapVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loginFailed(message: String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
    
//    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState){
//
//        if let ind = self.validationStateArray.firstIndex(where: { (value) -> Bool in
//            value.type == textFieldType
//        }){
//            self.validationStateArray[ind].state = textFieldState
//            self.loginTableView.reloadRows(at: [IndexPath(row: ind, section: 0)], with: UITableView.RowAnimation.none)
//        }
//    }
}

//MARK:- Social login delegate
extension SignUpVC : SocialLoginServiceDelegate{
    
    func willLoginSocialLoginService() {
        self.view.showIndicator()
    }
    
    func loginSuccessForSocialServicerModel(user: UserProfile) {
        self.view.hideIndicator()
//        AppNavigator.shared.goToHomeVC()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInMapVC") as! LogInMapVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func socialLoginServiceFailed(message: String){
        self.view.hideIndicator()
    }
    
    func invalidInputFromSocialAccount(message : String){
        CommonFunctions.showToastMessage(message)
    }
    
    func getEmail() {
        self.view.hideIndicator()
        self.openEmailPopUp(userInput: self.userInput)
    }
    
}


//MARK:- Social login callback
extension SignUpVC : SocialLoginControllerDelegate {
    
    func willLoginSocialAccount(){
        
    }
    
    func socialLoginSuccess(email: String, socialId: String,profilePicture : String, name : String ,socialType: SocialLoginController.LoginType, firstName: String, lastName: String){
        var userInput = UserInput()
        userInput.email = email
        userInput.fullName = name
        userInput.profileImage = profilePicture
        userInput.fbId = socialId
        userInput.socialLoginType = socialType.rawValue
        userInput.firstName = firstName
        userInput.lastName = lastName
        self.userInput = userInput
        self.socialControler.checkSocialLogin(email: email, fbId: socialId)
    }
    
    func socialLoginFailed(message: String){
        CommonFunctions.showToastMessage(message)
    }
    
    func userNotRegistered(){
        
    }
}


//MARK:- Check social login delegate
extension SignUpVC : CheckSocialLoginDelegate {
    
    func willCheckSocialLogin() {
        self.view.showIndicator()
    }
    
    func checkFacebookLoginSuccessFull(isExist: Bool) {
        self.view.hideIndicator()
        if isExist {
            self.socialControler.sociialLogin(userInput: userInput)
        }else{
            let emailVC = EnterCodePopUpVC.instantiate(fromAppStoryboard: .PreLogin)
            emailVC.delegate = self
            emailVC.modalPresentationStyle = .overCurrentContext
            self.present(emailVC, animated: true, completion: nil)
        }
    }
    
    func failedToCheckFacebookLogin(message: String) {
        self.view.hideIndicator()
    }
}

//MARK:- Enter code callback
extension SignUpVC : ENterCodeDelegate {
    
    func getCodeBack(code: String) {
        self.userInput.invitationCode = code
        self.socialControler.sociialLogin(userInput: userInput)
    }
    
    func skipAndContinue() {
        self.socialControler.sociialLogin(userInput: userInput)
    }
}
