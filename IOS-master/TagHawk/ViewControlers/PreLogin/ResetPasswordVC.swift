//
//  ResetPasswordVC.swift
//  TagHawk
//
//  Created by Appinventiv on 25/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ResetPasswordVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var newPasswordTextField: CustomTextField!
    @IBOutlet weak var confirmPasswordTextField: CustomTextField!
    @IBOutlet weak var submitButton: UIButton!
    
    //MARK:- Variables
    private let controler = ResetPasswordControler()
    private var userInput = UserInput()
    var resetPasswordToken : String = ""
    private var validationStateArray : [(type : TagHawkTextFieldTableViewCell.TextFieldType,state : TextFieldState)] = [
        (TagHawkTextFieldTableViewCell.TextFieldType.password,TextFieldState.none),
        (TagHawkTextFieldTableViewCell.TextFieldType.confirmPassword,TextFieldState.none),
        ]
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
    }
    
    //MARK:- IBAction
    
    //MARK:- Back button Tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- SUbmit button tapped
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.resetPassword(userInput: self.userInput, token: self.resetPasswordToken)
    }
}

//MARK:- Privte functions
private extension ResetPasswordVC{
    
    //MARK:- Set up View
    func setUpSubView(){
        
        self.newPasswordTextField.placeholder = LocalizedString.newPassword.localized
        self.confirmPasswordTextField.placeholder = LocalizedString.confirmPassword.localized
        
        self.descriptionLabel.setAttributes(text: LocalizedString.Please_Enter_New_Password.localized, font: AppFonts.Galano_Regular.withSize(12), textColor: AppColors.lightGreyTextColor)
        self.descriptionLabel.attributedText = LocalizedString.Please_Enter_New_Password.localized.addLine(spacing: 6)
        
        self.descriptionLabel.textAlignment = .center
        
        self.submitButton.setAttributes(title: LocalizedString.Submit.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
        
        self.submitButton.round(radius: 10)
        self.backView.round(radius: 10)
        
        self.newPasswordTextField.delegate = self
        self.confirmPasswordTextField.delegate = self
        
        self.newPasswordTextField.isSecureTextEntry = true
        self.confirmPasswordTextField.isSecureTextEntry = true
        
        self.newPasswordTextField.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)
        self.confirmPasswordTextField.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)
    }
    
    //MARK:- Change textfield state
    func didShowError(type : TagHawkTextFieldTableViewCell.TextFieldType ,state : TextFieldState){
        
        type == .password ? self.showError(textField: self.newPasswordTextField, state: state) : self.showError(textField: self.confirmPasswordTextField, state: state)
        
    }
    
    func showError(textField : CustomTextField ,state : TextFieldState){
        switch state {
        case .invalid:
            textField.error = state.associatedValue
        default:
            textField.error = nil
        }
    }
}

//MARK:- Configure Navigation
private extension ResetPasswordVC{
    
    func configureNavigation(){
        
    }
}

//MARK: Textfield delegates
extension ResetPasswordVC : UITextFieldDelegate {
    
    @objc private func texfieldDidChange(_ textfield: UITextField) {
        
        if textfield == self.newPasswordTextField{
            self.userInput.password = textfield.text ?? ""
        }else{
            self.userInput.confirmPassword = textfield.text ?? ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let userEnteredString = textField.text else { return false }
        
        textField == self.newPasswordTextField ? self.changeInvalidState(textField: self.newPasswordTextField) : self.changeInvalidState(textField: self.confirmPasswordTextField)
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        
        return self.isCharacterAcceptableForPassword(password: userEnteredString, char: string)
        
    }
    
    func changeInvalidState(textField : CustomTextField){
        
        switch textField.textFieldState {
        case .invalid:
            self.newPasswordTextField.error = nil
        default:
            break
        }
    }
    
    func isCharacterAcceptableForPassword(password : String, char : String) -> Bool {
        return char.isBackSpace || password.count <= 15
    }
}

//MARK:- Forgot Password delegate
extension ResetPasswordVC : ResetPasswordDelegate {
    
    func willRequestResetPassword(){
        self.view.showIndicator()
    }
    
    func resetPasswordSuccessfully(){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage("Reset Password Successfully.")
        self.navigationController?.popViewController(animated: true)
    }
    
    func failedToResetPassword(message : String){
        self.view.hideIndicator()
    }
    
    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState){
        
        if textFieldType == .password{
            self.validationStateArray[0].state = textFieldState
            self.newPasswordTextField.textFieldState = textFieldState
        }else{
            self.validationStateArray[1].state = textFieldState
            self.confirmPasswordTextField.textFieldState = textFieldState
        }
        self.didShowError(type: textFieldType, state: textFieldState)
    }
}
