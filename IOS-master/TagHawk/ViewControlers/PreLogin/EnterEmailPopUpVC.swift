//
//  EnterEmailVC.swift
//  TagHawk
//
//  Created by Appinventiv on 04/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol GetEmailBack : class {
    func getEmailBack(userInput : UserInput)
}

class EnterEmailPopUpVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var emailRequiredLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var emailField: CustomTextField!
    @IBOutlet weak var popUpView: UIView!
    
    //MARK:- Variables
    let controler = EnterEmailControler()
    weak var delegate : GetEmailBack?
    var userinput = UserInput()
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- OKbutton tapped
    @IBAction func okButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !self.controler.validateEmail(email: self.emailField.text ?? "") { return }
        self.userinput.email = self.emailField.text ?? ""
        self.delegate?.getEmailBack(userInput: self.userinput)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- cancel button tapped
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}


private extension EnterEmailPopUpVC {
    
    //MARK:- Set up view
    func setUpSubView() {
        self.okButton.round(radius: 10)
        self.cancelButton.round(radius: 10)
        self.popUpView.round(radius: 10)
        self.cancelButton.setBorder(width: 1.0, color: AppColors.appBlueColor)
        self.view.backgroundColor = UIColor.clear
        self.backView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.emailField.placeholder = LocalizedString.Email_Id.localized
        self.emailField.delegate = self
        self.emailField.keyboardType = .emailAddress
        self.emailField.returnKeyType = .done
        self.cancelButton.setAttributes(title: LocalizedString.cancel.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: AppColors.appBlueColor)
        self.okButton.setAttributes(title: LocalizedString.ok.localized, font: AppFonts.Galano_Semi_Bold.withSize(16) , titleColor: UIColor.white)
        self.emailRequiredLabel.setAttributes(text: LocalizedString.Email_Required.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), textColor: UIColor.black)
    }
}

//MARK:- Edit email popup delegates
extension EnterEmailPopUpVC : EnterEmailDelegate {

    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState) {
        self.emailField.textFieldState = textFieldState
        self.didShowError(state: textFieldState)
    }
    
    func didShowError(state : TextFieldState){
        switch state {
        case .invalid:
            self.emailField.error = state.associatedValue
        default:
            self.emailField.error = nil
        }
    }
}

//MARK:- Textfield delegate
extension EnterEmailPopUpVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let userEnteredString = textField.text else { return false }
        
        switch self.emailField.textFieldState {
            case .invalid:
                self.emailField.error = nil
            default:
                break
        }
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        return self.isCharacterAcceptableForEmail(email: userEnteredString, char: string)
    }
    
    func isCharacterAcceptableForEmail(email : String, char : String) -> Bool {
        return char.isBackSpace || email.count <= 50
    }
    
}
