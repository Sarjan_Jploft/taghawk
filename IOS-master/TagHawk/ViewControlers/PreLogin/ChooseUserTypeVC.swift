//
//  ChooseUserTypeVc.swift
//  TagHawk
//
//  Created by Appinventiv on 21/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ChooseUserTypeVC: BaseVC {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var customerBackView: UIView!
    @IBOutlet weak var customerButton: UIButton!
    @IBOutlet weak var customerImageView: UIImageView!
    @IBOutlet weak var customerStartLabel: UILabel!
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var hawkDriverBackView: UIView!
    @IBOutlet weak var hawkDriverButton: UIButton!
    @IBOutlet weak var hawkDriverImageView: UIImageView!
    @IBOutlet weak var hawkDriverLabel: UILabel!
    @IBOutlet weak var hawkDriverStartLabel: UILabel!
    @IBOutlet weak var movingCompanyBackView: UIView!
    @IBOutlet weak var movingCompanyImageView: UIImageView!
    @IBOutlet weak var movingCompanyButton: UIButton!
    @IBOutlet weak var movingCompanyStartLabel: UILabel!
    @IBOutlet weak var movingCompanyLabel: UILabel!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var backGroundImageView: UIImageView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
        
    }
    
    
    @IBAction func customerButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        let vc = LoginVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func hawkDriverButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = LoginVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    @IBAction func movingCompanyButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = LoginVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}

private extension ChooseUserTypeVC{
 
    func setUpSubView(){
        
        self.headingLabel.setAttributes(text: LocalizedString.Choose_Role_And_Start_With_Taghawk.localized, font: AppFonts.Galano_Medium.withSize(22), textColor: UIColor.white)
        
        self.headingLabel.attributedText = LocalizedString.Choose_Role_And_Start_With_Taghawk.localized.addLine(spacing: 6)
        
        self.customerLabel.setAttributes(text: LocalizedString.Customer.localized, font: AppFonts.Galano_Medium.withSize(19), textColor: UIColor.black)
     
        self.hawkDriverLabel.setAttributes(text: LocalizedString.Hawk_Driver.localized, font: AppFonts.Galano_Medium.withSize(19), textColor: UIColor.black)
    
        self.movingCompanyLabel.setAttributes(text: LocalizedString.Moving_Company.localized, font: AppFonts.Galano_Medium.withSize(19), textColor: UIColor.black)
        
        self.customerStartLabel.setAttributes(text: LocalizedString.Lets_Start.localized, font: AppFonts.Galano_Light.withSize(12), textColor: AppColors.lightGreyTextColor)

        self.hawkDriverStartLabel.setAttributes(text: LocalizedString.Lets_Start.localized, font: AppFonts.Galano_Light.withSize(12), textColor: AppColors.lightGreyTextColor)

        self.movingCompanyStartLabel.setAttributes(text: LocalizedString.Lets_Start.localized, font: AppFonts.Galano_Light.withSize(12), textColor: AppColors.lightGreyTextColor)

        self.customerBackView.round(radius: 10.0)
        self.hawkDriverBackView.round(radius: 10.0)
        self.movingCompanyBackView.round(radius: 10.0)

    }
    
}

//MARK:- Configure Navigation
private extension ChooseUserTypeVC{
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}
