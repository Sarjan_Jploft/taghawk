//
//  CustomPopUpVC.swift
//  TagHawk
//
//  Created by Appinventiv on 25/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol CustomPopUpDelegate : class {
    func okButtonTapped()
}

protocol CustomPopUpDelegateWithType : class {
    func okTapped(type : CustomPopUpVC.CustomPopUpFor, product : CartProduct?)
    func noButtonTapped(type : CustomPopUpVC.CustomPopUpFor, product : CartProduct?)
    func yesButtonTapped(type : CustomPopUpVC.CustomPopUpFor, product : CartProduct?)
    func yesButtonTapped(type : CustomPopUpVC.CustomPopUpFor, orderObj : PaymentHistory)
}

extension CustomPopUpDelegateWithType {
    func yesButtonTapped(type : CustomPopUpVC.CustomPopUpFor, orderObj : PaymentHistory){ }
}


class CustomPopUpVC : BaseVC {

    enum CustomPopUpFor {
        case forgotSuccess
        case signUpSuccess
        case itemAlreadyAddedToCart
        case removePreviousItemFromCartAndAddNew
        case sureToDeleteProductFromCart
        case productAddedSuccessFullyToCart
        case nonShippingProductAddedToCart
        case deleteProduct
        case notFound
        case productPurchasedSuccessfully
        case rateLater
        case none
        case emailVerified
        case enterBankandProfileDetails
        case enterBankDetails
        case paymentSuccessFull
        case changePasswordSuccess
        case sureToPayFromParticularCard
        case verifyEmailAddress
        case includeTransactionInfo
        case loginSignup
        case documentVerify
        case reportTag
        case cashout
        case logout
        case noLongerMember
        case paymentHistory
        case releasePayment
        case refundRequest
        case acceptRefundRequest
        case declineRefundRequest
        case releaseRefund
        case cancelRefund
        case despute
        case cashoutNotActivated
        case cancelPendingRequest
        case forceUpdate
        case normalUpdate
        case cashoutNotActivatedDueToInvalidAddress
        case cashoutNotActivatedDueToInvalidInfo
    }
    
    //IBOutlets:-
    @IBOutlet weak var dismisView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var popupHeight: NSLayoutConstraint!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var multipleButtonBackView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    
    //MARK:- Variables:-
    var popupFor = CustomPopUpFor.none
    weak var delegate : CustomPopUpDelegate?
    weak var delegateWithType : CustomPopUpDelegateWithType?
    var msg : String = ""
    var prod : CartProduct? = nil
    var paymentHistory = PaymentHistory()
    var merchant = MerchantData()

    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- IBActions:-
    @IBAction func okButtontapped(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true) {
            self.delegate?.okButtonTapped()
            self.delegateWithType?.okTapped(type: self.popupFor, product: self.prod)
        }
    }
    
    @IBAction func noButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
         self.dismiss(animated: true) {
            self.delegateWithType?.noButtonTapped(type: self.popupFor, product: self.prod)
        }
    }
    
    @IBAction func yesButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
         self.dismiss(animated: true) {
            self.delegateWithType?.yesButtonTapped(type: self.popupFor, product: self.prod)
            self.delegateWithType?.yesButtonTapped(type: self.popupFor, orderObj: self.paymentHistory)
        }
    }
    
    @IBAction func closeBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension CustomPopUpVC {
    
    func setUpSubView(){
        self.headingLabel.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(17.5), textColor: UIColor.black)
        self.descriptionLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
        self.noButton.setAttributes(title: LocalizedString.cancel.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.black, backgroundColor: UIColor.white)
        
        self.yesButton.setAttributes(title: LocalizedString.Delete.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
        self.descriptionLabel.textAlignment = .center
        self.okButton.setAttributes(title: LocalizedString.ok.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)

        self.populateData()
        self.dismisView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.backgroundColor = UIColor.clear
        self.okButton.round(radius: 10)
        self.noButton.round(radius: 10)
        self.yesButton.round(radius: 10)
        self.noButton.backgroundColor = UIColor.white
        self.noButton.setBorder(width: 0.5, color: AppColors.blackBorder)
    }
    
    func dismisViewTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func populateData(){
        
        switch self.popupFor {
  
        case .forgotSuccess:
            self.headingLabel.text = LocalizedString.Successfully.localized
            self.descriptionLabel.text = LocalizedString.Link_Sent_To_Registered_Email.localized
            self.multipleButtonBackView.isHidden = true
            
        case .notFound:
            self.headingLabel.text = LocalizedString.Oops.localized
            self.descriptionLabel.text = msg
            self.multipleButtonBackView.isHidden = true
            
        case .signUpSuccess:
            self.headingLabel.text = LocalizedString.success.localized
            self.descriptionLabel.text = LocalizedString.Verification_Link_Has_Been_Sent_To_Your_Email.localized
            self.multipleButtonBackView.isHidden = true
       
        case .itemAlreadyAddedToCart:
            self.headingLabel.text = LocalizedString.Already_Exist.localized
            self.descriptionLabel.text = self.msg
            self.multipleButtonBackView.isHidden = false
            self.noButton.setTitle(LocalizedString.cancel.localized)
            self.yesButton.setTitle(LocalizedString.ok.localized)
            
        case .removePreviousItemFromCartAndAddNew:
            self.headingLabel.text = LocalizedString.Remove_Product.localized
            self.descriptionLabel.text = self.msg
            self.multipleButtonBackView.isHidden = false
            self.noButton.setTitle(LocalizedString.cancel.localized)
            self.yesButton.setTitle(LocalizedString.Remove.localized)
            
        case .sureToDeleteProductFromCart:
            self.headingLabel.text = LocalizedString.Delete_Product.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Delete_Product_From_Cart.localized
            self.multipleButtonBackView.isHidden = false
        
        case .productAddedSuccessFullyToCart:
            self.headingLabel.text = LocalizedString.Product_Added_Successfully.localized
            self.descriptionLabel.text = ""
            self.multipleButtonBackView.isHidden = true
            
        case .nonShippingProductAddedToCart:
            self.headingLabel.text = LocalizedString.Product_Added_Successfully.localized
            self.descriptionLabel.text = ""
            self.multipleButtonBackView.isHidden = false
            self.noButton.setTitle(LocalizedString.Shop_More.localized)
            self.yesButton.setTitle(LocalizedString.CheckOut.localized)

        case .deleteProduct:
            self.headingLabel.text = LocalizedString.Delete_Product.localized
            self.descriptionLabel.text = LocalizedString.Delete_Prod_Desc.localized
            self.multipleButtonBackView.isHidden = false
            
        case .productPurchasedSuccessfully:
            self.headingLabel.text = LocalizedString.Congradulations.localized
            self.descriptionLabel.text = LocalizedString.Payment_Successfull.localized
            self.multipleButtonBackView.isHidden = true
            
        case .rateLater:
            self.headingLabel.text = LocalizedString.RateSeller.localized
            self.descriptionLabel.text = LocalizedString.wouldYouLikeToRateLater.localized
            self.noButton.setTitle(LocalizedString.cancel.localized)
            self.yesButton.setTitle(LocalizedString.Later.localized)
            
        case .emailVerified:
            self.headingLabel.text = LocalizedString.verifyEmail.localized
            self.descriptionLabel.text = msg
            self.multipleButtonBackView.isHidden = true
            
        case .enterBankandProfileDetails:
            self.headingLabel.text = LocalizedString.Enter_Details.localized
            self.descriptionLabel.text = LocalizedString.Enter_Dob_And_ssn.localized
            self.multipleButtonBackView.isHidden = true
            
        case .enterBankDetails:
            self.headingLabel.text = LocalizedString.Enter_Details.localized
            self.descriptionLabel.text = LocalizedString.Please_Enter_Bank_Details.localized
            self.multipleButtonBackView.isHidden = true
            
        case .changePasswordSuccess:
            
            self.headingLabel.text = LocalizedString.Congradulations.localized
            self.descriptionLabel.text = LocalizedString.Change_Passwod_SuccessFully.localized
            self.okButton.setTitle(LocalizedString.Go_To_Login.localized)
            self.multipleButtonBackView.isHidden = true
        
        case .sureToPayFromParticularCard:
            self.headingLabel.text = LocalizedString.Payment.localized
            self.descriptionLabel.text = self.msg
            self.multipleButtonBackView.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
 
        case .verifyEmailAddress:
            self.headingLabel.text = LocalizedString.verifyEmail.localized
            self.descriptionLabel.text = LocalizedString.PleaseVerifyEmailAddress.localized
            self.multipleButtonBackView.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .includeTransactionInfo:
            self.headingLabel.text = LocalizedString.TransactionFeeInfo.localized
            self.descriptionLabel.text = LocalizedString.TransactionFeeInfoDesc.localized
            self.multipleButtonBackView.isHidden = true
            self.descriptionLabel.textColor = UIColor.black
            
        case .loginSignup:
            self.headingLabel.text = LocalizedString.accessDenied.localized
            self.descriptionLabel.text = LocalizedString.guestLoginSignupDesc.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.Sign_Up.localized)
            self.yesButton.setTitle(LocalizedString.login.localized)
            
        case .documentVerify:
            self.headingLabel.text = LocalizedString.Become_A_Verified_Seller.localized
            self.descriptionLabel.text = LocalizedString.Upload_Document_To_Apply_For_Tag.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.cancel.localized)
            self.yesButton.setTitle(LocalizedString.Scan.localized)
            
        case .cashout:
            self.headingLabel.text = LocalizedString.Cash_Out.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Cashout.localized
            self.multipleButtonBackView.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .reportTag:
            self.headingLabel.text = LocalizedString.Report_Tag.localized
            self.descriptionLabel.text = LocalizedString.Are_You_Sure_You_Want_To_Report_Tag.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.cancel.localized)
            self.yesButton.setTitle(LocalizedString.Report.localized)
            
        case .logout:
            self.headingLabel.text = LocalizedString.logout.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Logout.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.cancel.localized)
            self.yesButton.setTitle(LocalizedString.logout.localized)
            
        case .releasePayment:
            self.headingLabel.text = LocalizedString.Release_Payment.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Release_Payment.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .refundRequest:
            self.headingLabel.text = LocalizedString.Refund_Requested.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Request_Refund.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .acceptRefundRequest:
            self.headingLabel.text = LocalizedString.Accept_Refund.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Accept_Refund.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .releaseRefund:
            self.headingLabel.text = LocalizedString.Release_Refund.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Release_Refund.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .declineRefundRequest:
            self.headingLabel.text = LocalizedString.Decline_Refund.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Decline_Refund_Request.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .cancelRefund:
            self.headingLabel.text = LocalizedString.Cancel_Refund.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Cancel_Refund.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .despute:
            self.headingLabel.text = LocalizedString.Despute.localized
            self.descriptionLabel.text = LocalizedString.Sure_To_Cancel_Refund.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .cashoutNotActivated:
            self.headingLabel.text = LocalizedString.Account_Pending.localized
            self.descriptionLabel.text = LocalizedString.Successfully_Submitted_Your_Info.localized
            self.descriptionLabel.attributedText = LocalizedString.Successfully_Submitted_Your_Info.localized.attributeStringWithColors(stringToColor: LocalizedString.stripe.localized, strClr: UIColor.black, substrClr: AppColors.stripeColor, strFont: AppFonts.Galano_Regular.withSize(13), strClrFont: AppFonts.Galano_Bold.withSize(19))
            self.multipleButtonBackView.isHidden = true
            
        case .cancelPendingRequest:
            self.headingLabel.text = LocalizedString.Cancel_Request.localized
            self.descriptionLabel.text = LocalizedString.Do_You_Want_To_Cancel_Your_Request.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.No.localized)
            self.yesButton.setTitle(LocalizedString.Yes.localized)
            
        case .forceUpdate:
            self.headingLabel.text = LocalizedString.Force_Update_Heading.localized
            self.descriptionLabel.text = LocalizedString.Force_Update_Message.localized
            self.multipleButtonBackView.isHidden = true
//            self.closeBtn.isHidden = false
//            self.noButton.setTitle(LocalizedString.No.localized)
//            self.yesButton.setTitle(LocalizedString.Update.localized)
            self.okButton.setTitle(LocalizedString.Update.localized)
            
        case .normalUpdate:
            self.headingLabel.text = LocalizedString.Normal_Update_Heading.localized
            self.descriptionLabel.text = LocalizedString.Normal_Update_Message.localized
            self.multipleButtonBackView.isHidden = false
            self.closeBtn.isHidden = false
            self.noButton.setTitle(LocalizedString.cancel.localized)
            self.yesButton.setTitle(LocalizedString.Update.localized)
            
        case .cashoutNotActivatedDueToInvalidAddress:
            self.headingLabel.text = LocalizedString.verification_Status.localized
            self.descriptionLabel.text = LocalizedString.Invalid_Address_Entered.localized
            self.multipleButtonBackView.isHidden = true
            self.okButton.setTitle(LocalizedString.ok.localized)
     
        case .cashoutNotActivatedDueToInvalidInfo:
            self.headingLabel.text = LocalizedString.verification_Status.localized
            self.descriptionLabel.text = self.getInvalidInfoMessage()
            self.multipleButtonBackView.isHidden = true
            self.okButton.setTitle(LocalizedString.ok.localized)

            
        default:
            break
        }
    }
    
    
    func getInvalidInfoMessage() -> String {
    
        var invalidItemsString = ""
        
        
       if UserProfile.main.passport{
            if !self.merchant.isPassportVerified || !(self.merchant.ssnAndPassPortStatus == .verified){
                let commaToBeAppended = !invalidItemsString.isEmpty ? ", " : ""
                invalidItemsString = invalidItemsString + "\(commaToBeAppended)" + LocalizedString.Passport.localized
            }
       }else{
            if !self.merchant.isSsnVerified || !(self.merchant.ssnAndPassPortStatus == .verified) {
                let commaToBeAppended = !invalidItemsString.isEmpty ? ", " : ""
                invalidItemsString = invalidItemsString + "\(commaToBeAppended)" + LocalizedString.SSN.localized
            }
        }


        
       let completeStr = LocalizedString.Account_Pending_Due_To_Invalid_Info.localized.replacingOccurrences(of: "XXX", with: invalidItemsString)
        
        if completeStr.contains(s: ",") {
            return completeStr.replacingOccurrences(of: "is", with: "are")
        }
        
        return completeStr
        
    }
}
