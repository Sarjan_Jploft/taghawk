//
//  LoginVC.swift
//  TagHawk
//
//  Created by Appinventiv on 21/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class LoginVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var backGroundImageView: UIImageView!
    @IBOutlet weak var bottomBackView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var loginTableView: UITableView!
    @IBOutlet weak var newUserButton: UIButton!
    @IBOutlet weak var exploreAsGuestButton: UIButton!
    @IBOutlet weak var tableViewBackView: UIView!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableBackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomFooterView: UIView!
    @IBOutlet weak var loginButtonYPosition: NSLayoutConstraint!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    
    //MARK:- Variables
    private let fieldsHeadingArray = ["\(LocalizedString.Email_Id.localized)*","\(LocalizedString.password.localized)*"]
    private var validationStateArray : [(type : TagHawkTextFieldTableViewCell.TextFieldType,state : TextFieldState)] = [
        (TagHawkTextFieldTableViewCell.TextFieldType.email,TextFieldState.none),
        (TagHawkTextFieldTableViewCell.TextFieldType.password,TextFieldState.none)]
    private let socialLoginController = SocialLoginController()
    private let controler = LoginControler()
    private var userInput = UserInput()
    var userType = UserProfile.UserType.notmal
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func bindControler() {
        super.bindControler()
        controler.delegate = self
        socialLoginController.delegate = self
        socialLoginController.socialServiceDelegate = self
        socialLoginController.checkFbLoginDelegate = self
    }
    
    //MARK:- Login button tapped
    @IBAction func loginButtonTapped(_ sender: Any) {
        self.view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.login(userInput: self.userInput)
    }
    
    //MARK:- Facebook button tapped
    @IBAction func facebookButtonTapped(_ sender: Any) {
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.socialLoginController.fbLogin()
    }
    
    //MARK:- New user button tapped
    @IBAction func newUserButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
//        let vc = SignUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
//        self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Explore as guest tapped
    @IBAction func exploreAsGuestButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.loginAsAGuest()
    }
    
    //MARK:- Forgot password button tapped
    @IBAction func forgotButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = ForgotPasswordVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Private functions
private extension LoginVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        
        self.configureTableView()
        
        makeUiAdjuestments()
        
        self.tableViewBackView.round(radius: 10)
        
        self.loginButton.round(radius: 10)
        self.facebookButton.round(radius: 10)
        
        self.facebookButton.setAttributes(title: LocalizedString.Connect_with_Facebook.localized, font: AppFonts.Galano_Medium.withSize(13), titleColor: AppColors.blueTextColor, backgroundColor: AppColors.faceBookBackColor)
        
        self.loginButton.setAttributes(title: LocalizedString.login.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
        
        self.forgotPasswordButton.setAttributes(title: LocalizedString.Forgot_Password.localized, font: AppFonts.Galano_Regular.withSize(11.5), titleColor: UIColor.black)
        
        let attrTitle = LocalizedString.Am_A_New_User_SIgn_Up.localized.attributeBoldStringWithUnderline(stringsToUnderLine: [LocalizedString.Sign_Up.localized], size: 20, strColor: UIColor.black, font: AppFonts.Galano_Bold.withSize(20))
        
        self.newUserButton.setAttributedTitle(attrTitle, for: UIControl.State.normal)
        
        self.exploreAsGuestButton.setAttributes(title: LocalizedString.Explore_As_Guest.localized, font: AppFonts.Galano_Semi_Bold.withSize(20), titleColor: UIColor.white)
        
    }
    
    //MARK:- Configure tableview
    func configureTableView(){
        self.loginTableView.delegate = self
        self.loginTableView.dataSource = self
        let inset : CGFloat = Display.typeIsLike == .iphone5 ? 10 : 20
        self.loginTableView.contentInset = UIEdgeInsets(top: inset, left: 0, bottom: inset, right: 0)
        self.registerNib()
    }
    
    //MARK:- Make ui adjuestments
    
    func makeUiAdjuestments(){
        self.bottomViewHeight.constant = screenHeight * 142 / 667
        self.bottomFooterView.frame.size.height = Display.typeIsLike == .iphone5 ? 140 : 180
        self.loginButtonYPosition.constant = Display.typeIsLike == .iphone5 ? -8 : 0
        DispatchQueue.delay(0.01) {
            let extraHeight : CGFloat = Display.typeIsLike == .iphone5 ? 20 : 20
            self.tableBackViewHeight.constant = self.loginTableView.contentSize.height + extraHeight
        }
    }
    
    //MARK:- Register nib
    func registerNib(){
        self.loginTableView.registerNib(nibName: TagHawkTextFieldTableViewCell.defaultReuseIdentifier)
    }
    
    //MARK:- Open email popup
    func openEmailPopUp(userInput : UserInput) {
        let vc = EnterEmailPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.userinput = userInput
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
}


//MARK:- Configure navigation
private extension LoginVC {
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}


//MARK:_ Table view datasource and delegates
extension LoginVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldsHeadingArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TagHawkTextFieldTableViewCell.defaultReuseIdentifier) as? TagHawkTextFieldTableViewCell else {
            fatalError("SettingsVC....\(TagHawkTextFieldTableViewCell.defaultReuseIdentifier) cell not found") }
        cell.delegate = self
        cell.textField.placeholder = fieldsHeadingArray[indexPath.row]
        cell.textFieldType = self.validationStateArray[indexPath.row].type
        cell.didShowError(state: self.validationStateArray[indexPath.row].state)
        cell.populateText(type: self.validationStateArray[indexPath.row].type, userInput: self.userInput)
        
        return cell
    }
}

//MARK:- Cusom emil delegate
extension LoginVC : CustomFieldDelagate {
    
    func textFieldShouldReturn(type: TagHawkTextFieldTableViewCell.TextFieldType) {
        switch type {
            
        case .password:
            self.view.endEditing(true)
            if !AppNetworking.isConnectedToInternet {
                CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                return
            }
            self.controler.login(userInput: self.userInput)
            
        default:
            self.view.endEditing(true)
        }
    }
    
    
    func texfieldDidChange(type: TagHawkTextFieldTableViewCell.TextFieldType, text: String?) {
        
        guard let txt = text else { return }
        
        switch type {
            
        case .email:
            self.userInput.email = txt
            
        case .password:
            self.userInput.password = txt
            
        default :
            break
            
        }
    }
}

//MARK:- Webservices

//MARK:- Social login callback
extension LoginVC : SocialLoginControllerDelegate {
    
    func willLoginSocialAccount(){
        
    }
    
    func socialLoginSuccess(email: String, socialId: String,profilePicture : String, name : String ,socialType: SocialLoginController.LoginType, firstName: String, lastName: String){
        var userInput = UserInput()
        userInput.email = email
        userInput.fullName = name
        userInput.profileImage = profilePicture
        userInput.fbId = socialId
        userInput.socialLoginType = socialType.rawValue
        userInput.firstName = firstName
        userInput.lastName = lastName
        self.userInput = userInput
        self.socialLoginController.checkSocialLogin(email: email, fbId: socialId)
    }
    
    func socialLoginFailed(message: String){
        CommonFunctions.showToastMessage(message)
    }
    
    func userNotRegistered(){
        
    }
}

//MARK:- Enter code callback
extension LoginVC : ENterCodeDelegate {
    
    func getCodeBack(code: String) {
        self.userInput.invitationCode = code
        self.socialLoginController.sociialLogin(userInput: userInput)
    }
    
    func skipAndContinue() {
        self.socialLoginController.sociialLogin(userInput: userInput)
    }
}

//MARK:- Get email back
extension LoginVC : GetEmailBack {
    
    func getEmailBack(userInput : UserInput) {
        self.socialLoginController.sociialLogin(userInput: userInput)
    }
    
}

//MARK:- Social login delegate
extension LoginVC : SocialLoginServiceDelegate{
    
    func willLoginSocialLoginService() {
        self.view.showIndicator()
    }
    
    func loginSuccessForSocialServicerModel(user: UserProfile) {
        self.view.hideIndicator()
        AppNavigator.shared.goToHomeVC()
    }
    
    func socialLoginServiceFailed(message: String){
        self.view.hideIndicator()
    }
    
    func invalidInputFromSocialAccount(message : String){
        CommonFunctions.showToastMessage(message)
    }
    
    func getEmail() {
        self.view.hideIndicator()
        self.openEmailPopUp(userInput: self.userInput)
    }
    
}

//MARK:- Login controler
extension LoginVC : LoginControllerDelegate{
    
    func willLogin(){
        self.view.showIndicator()
    }
    
    func loginSuccess(user: UserProfile){
        self.view.hideIndicator()
        AppNavigator.shared.goToHomeVC()
       // let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInMapVC") as! LogInMapVC
       // self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loginFailed(message: String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
    
    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState){
        
        if let ind = self.validationStateArray.firstIndex(where: { (value) -> Bool in
            value.type == textFieldType
        }){
            self.validationStateArray[ind].state = textFieldState
            self.loginTableView.reloadRows(at: [IndexPath(row: ind, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
}

//MARK:- Check social login delegate
extension LoginVC : CheckSocialLoginDelegate {
    
    func willCheckSocialLogin() {
        self.view.showIndicator()
    }
    
    func checkFacebookLoginSuccessFull(isExist: Bool) {
        self.view.hideIndicator()
        if isExist {
            self.socialLoginController.sociialLogin(userInput: userInput)
        }else{
            let emailVC = EnterCodePopUpVC.instantiate(fromAppStoryboard: .PreLogin)
            emailVC.delegate = self
            emailVC.modalPresentationStyle = .overCurrentContext
            self.present(emailVC, animated: true, completion: nil)
        }
    }
    
    func failedToCheckFacebookLogin(message: String) {
        self.view.hideIndicator()
    }
//    func failedToCheckLogin(message: String) {
//        self.view.hideIndicator()
//    }
}
