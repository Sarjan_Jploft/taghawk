//
//  ForgotPasswordVC.swift
//  TagHawk
//
//  Created by Appinventiv on 24/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ForgotPasswordVC: BaseVC {

    //MARK:-  IBOutlets
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forgotPasswordTextField: CustomTextField!
    @IBOutlet weak var resetLinkButton: UIButton!
    @IBOutlet weak var backView: UIView!
    
    //MARK:- Variables
    var userType = UserProfile.UserType.notmal
    let controler = ForgotPasswordControler()
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func bindControler() {
        super.bindControler()
        controler.delegate = self
    }
    
    //MARK:- IBAction
    
    //MARK:- Reset link button tapped
    @IBAction func resetLinkButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.forgotPassword(emil: self.forgotPasswordTextField.text ?? "", userType: self.userType)
    }
    
    //MARK:- Back button Tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK:- Privte functions
private extension ForgotPasswordVC{
    
    //MARK:- Set up View
    func setUpSubView(){
        self.forgotPasswordTextField.placeholder = LocalizedString.Email_Id.localized
       
        self.headingLabel.setAttributes(text: LocalizedString.Trouble_Logging_In.localized, font: AppFonts.Galano_Medium.withSize(20), textColor: UIColor.white)
        
        self.descriptionLabel.setAttributes(text: LocalizedString.Please_Enter_Your_Registered_Email.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: AppColors.lightGreyTextColor)
        
        self.descriptionLabel.attributedText = LocalizedString.Please_Enter_Your_Registered_Email.localized.addLine(spacing: 6)
        
        self.descriptionLabel.textAlignment = .center
       
        self.resetLinkButton.setAttributes(title: LocalizedString.Send_Reset_Link.localized, font: AppFonts.Galano_Semi_Bold.withSize(17), titleColor: UIColor.white, backgroundColor: UIColor.clear)
       
        self.resetLinkButton.round(radius: 10)
        self.backView.round(radius: 10)
        self.forgotPasswordTextField.delegate = self
        
        self.forgotPasswordTextField.keyboardType = .emailAddress
        self.forgotPasswordTextField.returnKeyType = .done
        
    }
    
    //MARK:- Change textfield state
    func didShowError(state : TextFieldState){
        switch state {
        case .invalid:
            self.forgotPasswordTextField.error = state.associatedValue
        default:
            self.forgotPasswordTextField.error = nil
        }
    }
}

//MARK:- Configure Navigation
private extension ForgotPasswordVC{
    
    func configureNavigation(){
        
    }
    
}


//MARK:- Textfield delegates
extension ForgotPasswordVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.controler.forgotPassword(emil: self.forgotPasswordTextField.text ?? "", userType: self.userType)
        return true
    }
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let userEnteredString = textField.text else { return false }
        
        switch self.forgotPasswordTextField.textFieldState {
            case .invalid:
                self.forgotPasswordTextField.error = nil
            default:
                break
            }
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        
        return self.isCharacterAcceptableForEmail(email: userEnteredString, char: string)
        
    }
    
    func isCharacterAcceptableForEmail(email : String, char : String) -> Bool {
        return char.isBackSpace || email.count <= 50
    }
    
    
}

extension ForgotPasswordVC  {

 
    @objc override func okButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- Forgot Password delegate
extension ForgotPasswordVC : ForgotPasswordDelegate {
   
    func willRequestForgotPassword(){
        self.view.showIndicator()
    }
    
    func forgotPasswordSuccessfully(){
        self.view.hideIndicator()
       self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.forgotSuccess)
    }
    
    func invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType, textFieldState: TextFieldState) {
        self.forgotPasswordTextField.textFieldState = textFieldState
        self.didShowError(state: textFieldState)
    }
    
    func failedToForgotPassword(message: String) {
         self.view.hideIndicator()
    }
}

