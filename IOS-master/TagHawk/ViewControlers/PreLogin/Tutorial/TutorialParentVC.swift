//
//  TutorialParentVC.swift
//  TagHawk
//
//  Created by Admin on 6/25/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit


class TutorialParentVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tutorialScrollView: UIScrollView!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var letsHawkButton: UIButton!
    
    @IBOutlet weak var viewSuperBackground: UIView!
    @IBOutlet weak var imgSuperBackgrnd: UIImageView!
    
    //MARK:- Variables
    private var tut1 : Tutorial1VC!
    private var tut2 : Tutorial2VC!
    private var tut3 : Tutorial3VC!
    private var tut4 : Tutorial4VC!
    
   
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
       
    }
    
    //MARK:- IBAction
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        
        switch self.tutorialScrollView.contentOffset.x {
        case 0.0:
            self.tutorialScrollView.setContentOffset(CGPoint(x: screenWidth, y: 0), animated: true)
            CommonFunctions.delay(delay: 0.2) {
                self.setButtonsLayout(horizontalOffset: screenWidth)
            }
        case screenWidth:
            self.tutorialScrollView.setContentOffset(CGPoint(x: screenWidth * 2, y: 0), animated: true)
            CommonFunctions.delay(delay: 0.2) {
                self.setButtonsLayout(horizontalOffset: screenWidth * 2)
            }
        case screenWidth * 2:
            self.tutorialScrollView.setContentOffset(CGPoint(x: screenWidth * 3, y: 0), animated: true)
            CommonFunctions.delay(delay: 0.2) {
                self.setButtonsLayout(horizontalOffset: screenWidth * 3)
            }
            
        default:
            
            break
        }
        
    }
    
    //MARK:-
    @IBAction func skipButtonTapped(_ sender: UIButton) {
        let vc = SignUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- lets hawk button tapped
    @IBAction func letsHawkButtonTapped(_ sender: UIButton) {
        let vc = SignUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension TutorialParentVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        
       
        self.tutorialScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tutorialScrollView.contentSize = CGSize(width: screenWidth * 4, height: self.tutorialScrollView.frame.height)
        self.tutorialScrollView.isPagingEnabled = true
        self.tutorialScrollView.bounces = false
        self.nextButton.setAttributes(title: LocalizedString.Start.localized, font: AppFonts.Galano_Medium.withSize(16), titleColor: AppColors.appBlueColor, backgroundColor: UIColor.white)
        self.skipButton.setAttributes(title: LocalizedString.Skip.localized, font: AppFonts.Galano_Medium.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
        self.letsHawkButton.setAttributes(title: LocalizedString.Lets_Hawk.localized, font: AppFonts.Galano_Medium.withSize(16), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
        self.nextButton.round(radius: 8)
        self.letsHawkButton.round(radius: 8)
        self.tutorialScrollView.delegate = self
        self.addTutorial1()
        self.addTutorial2()
        self.addTutorial3()
        self.addTutorial4()
        self.setButtonsLayout(horizontalOffset: 0.0)
        
    }
    
    //MARK:- Add tutorial1 vc
    func addTutorial1(){
        self.tut1 = Tutorial1VC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.tutorialScrollView.frame = self.tut1.view.frame
        self.tutorialScrollView.addSubview(self.tut1.view)
        self.tut1.willMove(toParent: self)
        self.addChild(self.tut1)
        self.tut1.view.frame.size.height = self.tutorialScrollView.frame.height
        self.tut1.view.frame.origin = CGPoint(x: 0, y: 0)//CGPoint.zero
    }
    
    //MARK:- Add tutorial2 vc
    func addTutorial2(){
        self.tut2 =  Tutorial2VC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.tutorialScrollView.frame = self.tut2.view.frame
        self.tutorialScrollView.addSubview(self.tut2.view)
        self.tut2.willMove(toParent: self)
        self.addChild(self.tut2)
        self.tut2.view.frame.size.height = self.tutorialScrollView.frame.height
        self.tut2.view.frame.origin = CGPoint(x: screenWidth * 2, y: 0)
    }
    
    //MARK:- Add tutorial3 vc
    func addTutorial3(){
        self.tut3 =  Tutorial3VC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.tutorialScrollView.frame = self.tut3.view.frame
        self.tutorialScrollView.addSubview(self.tut3.view)
        self.tut3.willMove(toParent: self)
        self.addChild(self.tut3)
        self.tut3.view.frame.size.height = self.tutorialScrollView.frame.height
        self.tut3.view.frame.origin = CGPoint(x: screenWidth, y: 0)
    }
    
    //MARK:- Add tutorial4 vc
    func addTutorial4(){
        self.tut4 =  Tutorial4VC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        self.tutorialScrollView.frame = self.tut4.view.frame
        self.tutorialScrollView.addSubview(self.tut4.view)
        self.tut4.willMove(toParent: self)
        self.addChild(self.tut4)
        self.tut4.view.frame.size.height = self.tutorialScrollView.frame.height
        self.tut4.view.frame.origin = CGPoint(x: screenWidth * 3, y: 0)
    }
    
    //MARK:- Set button layout
    func setButtonsLayout(horizontalOffset: CGFloat){
        if horizontalOffset == 0.0{
            self.skipButton.setTitleColor(UIColor.white)
            self.nextButton.setTitleColor(AppColors.appBlueColor)
            self.nextButton.backgroundColor = UIColor.white
            self.nextButton.setTitle("Start")
            self.letsHawkButton.isHidden = true
            self.skipButton.isHidden = false
            self.nextButton.isHidden = false
            self.imgSuperBackgrnd.isHidden = false
        }else if horizontalOffset >= screenWidth * 3{
            self.letsHawkButton.isHidden = false
            self.skipButton.isHidden = true
            self.nextButton.isHidden = true
            self.imgSuperBackgrnd.isHidden = true
            
        }else{
            self.skipButton.setTitleColor(AppColors.appBlueColor)
            self.nextButton.setTitleColor(UIColor.white)
            self.nextButton.backgroundColor = AppColors.appBlueColor
            self.letsHawkButton.isHidden = true
            self.skipButton.isHidden = false
            self.nextButton.isHidden = false
            self.imgSuperBackgrnd.isHidden = true
            self.nextButton.setTitle("Next")
            
        }
    }
}

//MARK:- Scrollview delegates
extension TutorialParentVC  {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        tutorialScrollView.contentOffset = CGPoint(x: tutorialScrollView.contentOffset.x, y: 0)
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.setButtonsLayout(horizontalOffset: scrollView.contentOffset.x)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.setButtonsLayout(horizontalOffset: scrollView.contentOffset.x)
    }
    
}
