//
//  Tutorial1VC.swift
//  TagHawk
//
//  Created by Admin on 6/25/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import YoutubeKit
//import AVKit

class Tutorial1VC: BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var playerView: UIView!
    private var player: YTSwiftyPlayer!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var point1Label: UILabel!
    @IBOutlet weak var point2Label: UILabel!
    @IBOutlet weak var point3Label: UILabel!
    @IBOutlet weak var point4Label: UILabel!
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headingLabel.setAttributes(text: LocalizedString.Easily_List_And_Buy.localized, font: AppFonts.Galano_Bold.withSize(19), textColor: UIColor.black, backgroundColor: UIColor.clear)
        
        self.subHeadingLabel.setAttributes(text: LocalizedString.Find_Your_Tag_Now.localized, font: AppFonts.Galano_Medium.withSize(17), textColor: UIColor.black, backgroundColor: UIColor.clear)
        
        self.point1Label.setAttributes(text: LocalizedString.Community_Based_Marketplace.localized, font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black, backgroundColor: UIColor.clear)
        
        self.point2Label.setAttributes(text: LocalizedString.Risk_Free_Cashless_Payment.localized, font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black, backgroundColor: UIColor.clear)

        self.point3Label.setAttributes(text: LocalizedString.Labor_Transportation_Service.localized, font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black, backgroundColor: UIColor.clear)

        self.point4Label.setAttributes(text: LocalizedString.In_App_Live_Chat.localized, font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black, backgroundColor: UIColor.clear)
        
        //self.configurePlayer()
    }
    
    //MARK:- Configure player
    func configurePlayer(){
        
        // Create a new player
        player = YTSwiftyPlayer(
            frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenWidth * 9 / 16),
            playerVars: [.playsInline(true), .videoID("m1pnwFSdOLU"), .loopVideo(false), .showRelatedVideo(false)])
        
        // Enable auto playback when video is loaded
        
        player.autoplay = false
        
        // Set player view
        
        //        playerView = player
        playerView.addSubview(player)
        // Set delegate for detect callback information from the player
        player.delegate = self
        
        // Load video player
        player.loadPlayer()
    }
    
    
    
    @IBAction func btnPlayClicked(_ sender: UIButton) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
        self.present(vc, animated: true, completion: nil)
        
        
//        let videoURL = URL(string: "https://www.youtube.com/watch?time_continue=2&v=m1pnwFSdOLU&feature=emb_logo")!
//        let player = AVPlayer(url: videoURL)
//        let vc = AVPlayerViewController()
//        vc.player = player
//
//        present(vc, animated: true) {
//            vc.player?.play()
//        }
        
        
        
        
        
    }
    
    
    
}

//MARK:- Youtube delegate
extension Tutorial1VC: YTSwiftyPlayerDelegate {
    
    func playerReady(_ player: YTSwiftyPlayer) {
        printDebug(#function)
    }
    
    func player(_ player: YTSwiftyPlayer, didUpdateCurrentTime currentTime: Double) {
        printDebug("\(#function):\(currentTime)")
    }
    
    func player(_ player: YTSwiftyPlayer, didChangeState state: YTSwiftyPlayerState) {
        printDebug("\(#function):\(state)")
    }
    
    func player(_ player: YTSwiftyPlayer, didChangePlaybackRate playbackRate: Double) {
        printDebug("\(#function):\(playbackRate)")
    }
    
    func player(_ player: YTSwiftyPlayer, didReceiveError error: YTSwiftyPlayerError) {
        printDebug("\(#function):\(error)")
    }
    
    func player(_ player: YTSwiftyPlayer, didChangeQuality quality: YTSwiftyVideoQuality) {
        printDebug("\(#function):\(quality)")
    }
    
    func apiDidChange(_ player: YTSwiftyPlayer) {
        printDebug(#function)
    }
    
    func youtubeIframeAPIReady(_ player: YTSwiftyPlayer) {
        printDebug(#function)
    }
    
    func youtubeIframeAPIFailedToLoad(_ player: YTSwiftyPlayer) {
        printDebug(#function)
    }
}
