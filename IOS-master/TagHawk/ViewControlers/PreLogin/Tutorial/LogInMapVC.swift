//
//  LogInMapVC.swift
//  TagHawk
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import GoogleMaps
import Popover



class LogInMapVC: BaseVC {

    @IBOutlet weak var myMapView: GMSMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnCross: UIButton!
    
    
    
    var tags : [Tag] = []
    var tappedClusterData : [TagItem] = []
    var clusterManager: GMUClusterManager!
    
    weak var tagDelegate : GetTagsDelegate?
    
    
    
    private var lastSearchText = ""
    var marker=GMSMarker()
    
//    var locManager = CLLocationManager()
//    var currentLocation1: CLLocation!
    
    
    var currentLocation : CLLocation = CLLocation() {
        
        didSet {
            if !serviceCalledFirstTime{
                SelectedFilters.shared.appliedFiltersOnTag.lat = currentLocation.coordinate.latitude
                SelectedFilters.shared.appliedFiltersOnTag.long = currentLocation.coordinate.longitude
                self.setCameraPosition(lat: currentLocation.coordinate.latitude, long: currentLocation.coordinate.longitude)
                self.getTagsData()
            }
        }
    }
    
    var serviceCalledFirstTime = false
    var isNavigatedToTagDetail = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.setUpSubView()
        
       
        
       // locManager.requestWhenInUseAuthorization()
        

//        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
//                CLLocationManager.authorizationStatus() ==  .authorizedAlways){
//
//              currentLocation1 = locManager.location
//
//        }
        
//        print("currnt loc lat",currentLocation1.coordinate.longitude)
//        print("currnt loc lng",currentLocation1.coordinate.latitude)
        
         //
//                if !serviceCalledFirstTime{
//                    SelectedFilters.shared.appliedFiltersOnTag.lat = currentLocation1.coordinate.latitude
//                    SelectedFilters.shared.appliedFiltersOnTag.long = currentLocation1.coordinate.longitude
//        //            self.setCameraPosition(lat: currentLocation.coordinate.latitude, long: currentLocation.coordinate.longitude)
//                    self.getTagsData()
//                }
        
        getLocationTagData()
        //show_CurrentLocation()
        
        // Do any additional setup after loading the view.
    }
    
    
    //MARK:- Get Location
    func getLocationTagData(){
        LocationManager.shared.startUpdatingLocation {[weak self] (loc, error) in
            guard let location = loc, let weakSelf = self else { return }
                           weakSelf.currentLocation = location
                           weakSelf.currentLocation = location
//            SelectedFilters.shared.appliedFiltersOnTag.lat = location.coordinate.latitude
//            SelectedFilters.shared.appliedFiltersOnTag.long = location.coordinate.longitude
//            self?.setCameraPosition(lat: location.coordinate.latitude, long: location.coordinate.longitude)
           // self?.getTagsData()
            
            LocationManager.shared.stopLocationManger()
        }
    }
    
    
    override func bindControler() {
        super.bindControler()
        self.tagDelegate = self
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    @IBAction func btnAddClicked(_ sender: UIButton) {
        
        let appVC = AddTagViewController.instantiate(fromAppStoryboard: .AddTag)
        
        appVC.tagCreateSuccess = {[weak self] tag in
            self?.tags.append(tag)
            self?.myMapView.clear()
            self?.clusterManager.clearItems()
            self?.generateClusterItems()
        }
        AppNavigator.shared.parentNavigationControler.pushViewController(appVC, animated: true)
        
    }
    
    
    
    @IBAction func btnDoneClicked(_ sender: UIButton) {
        
        AppNavigator.shared.goToHomeVC()
        
    }
    
    
    
    
    
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        
        view.endEditing(true)
        let vc = SearchVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.searchFrom = .homeTags
        vc.searchTagsDelegate = self
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: false)
        
    }
    
    
    @IBAction func btnCrossClicked(_ sender: UIButton) {
        
//        print("crrnt l",currentLocation1.coordinate.latitude,currentLocation1.coordinate.longitude)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        
        self.btnCross.setImage(nil, for: .normal)
        
        
        self.btnCross.isHidden = true
        
        self.searchBar.text = ""
        self.myMapView.clear()
        self.tags.removeAll()
        self.lastSearchText = ""
        self.getTagsData()
        
        
    }
    
    
    
    
    @IBAction func btnCurrentLocationClicked(_ sender: UIButton) {
        
        show_CurrentLocation()
        
    }
    
    
    func show_CurrentLocation(){
        
        view.endEditing(true)
        
        DispatchQueue.main.async {
            LocationManager.shared.startUpdatingLocation { (loc, erro) in
                guard let location = loc else { return }
                self.setCameraPosition(lat: location.coordinate.latitude, long: location.coordinate.longitude)
                
                let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                
                self.marker.position = center
                
                //self.marker = GMSMarker(position: center)
                self.marker.map = self.myMapView
                
                LocationManager.shared.stopLocationManger()
            }
        }
        
    }
    
    
    //MARK:- generate cluster item
       func generateClusterItems() {
           
           let context = 0.0001
           for (_, item) in self.tags.enumerated() {
               let position = CLLocationCoordinate2D(latitude: item.tagLatitude + context * randomScale() , longitude: item.tagLongitude + context * randomScale())
               let item =
                   TagItem(position: position, tag: item)
               clusterManager.add(item)
           }
           clusterManager.cluster()
       }
    
    
    
    
     //MARK:- get tags
        func getTagsData() {
            
            self.tagDelegate?.willRequestCommunities()
            
            var params : JSONDictionary = [:]
            
            if SelectedFilters.shared.appliedFiltersOnTag.lat != 0{
                params[ApiKey.lat1] = String(SelectedFilters.shared.appliedFiltersOnTag.lat)
            }
            
            if SelectedFilters.shared.appliedFiltersOnTag.long != 0{
                params[ApiKey.long1] = String(SelectedFilters.shared.appliedFiltersOnTag.long)
            }
            
            if SelectedFilters.shared.appliedFiltersOnTag.distance != 5{
                params[ApiKey.distance] = SelectedFilters.shared.getMilesForIndex(index: SelectedFilters.shared.appliedFiltersOnTag.distance)
            }
            
            if SelectedFilters.shared.appliedFiltersOnTag.tagType != .all {
                params[ApiKey.tagType] = SelectedFilters.shared.appliedFiltersOnTag.tagType.rawValue
            }
            
            if SelectedFilters.shared.appliedFiltersOnTag.members != .allMembers {
                params[ApiKey.memberSize] = SelectedFilters.shared.appliedFiltersOnTag.members.rawValue
            }
            print("Param lat lng",params)
    //        ApiKey.lat : lat, ApiKey.long : long
            WebServices.getTagData(parameters: params, success: {[weak self] (json) in
                
                guard let weakSelf = self else { return }
            weakSelf.tagDelegate?.communitiesReceivedSuccessfully(comunities: json[ApiKey.data].arrayValue.map { Tag(json: $0) })
                
            }) {[weak self] (error) -> (Void) in
                guard let weakSelf = self else { return }
                weakSelf.tagDelegate?.failedToReceiveCommunities()
                
            }
        }
    
   
    
    
    
    
    // Marker Action
//    let vc = TagDetailVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
//    vc.tagId = mark.tag.id
//    AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension LogInMapVC:GetSearchedTags{
    
    
    //MARK:- get searched title
       func getSearchedTitle(title: String) {
           self.lastSearchText = title
       // self.searchBar.searchTextField.text = title
        self.searchBar.text = title
        
        if title != ""{
            self.btnCross.isHidden = false
        }else{
            self.btnCross.isHidden = true
        }
        
        
           //self.searchLabel.text = title
//           self.searchBarRightImageView.image = AppImages.cross.image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
       }
       
       func getSearchedTags(tags: [Tag]) {
           self.myMapView.clear()
           self.tags.removeAll()
           self.tags = tags
        
//        if self.tags.count>0{
            self.btnCross.setImage(UIImage(named: "icCloseBlack"), for: .normal)
           // multiply
            //icCloseBlack
//        }
        //else{
//            self.btnCross.setImage(nil, for: .normal)
        //}
        
           if let firstTag = tags.first {
               self.myMapView.animate(toLocation: CLLocationCoordinate2D(latitude: firstTag.tagLatitude, longitude: firstTag.tagLongitude))
           }
           self.myMapView.clear()
           self.clusterManager.clearItems()
           self.generateClusterItems()
       }
    
}


//MARK:- Private functions
private extension LogInMapVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.myMapView.settings.rotateGestures = false
        makeClusterMap()
       // configureCollectionView()
    }
    
    //MARK:- Configure collection view
//    func configureCollectionView(){
//        self.clusterListCollectionView.registerNib(nibName: ClusterListCellCollectionViewCell.defaultReuseIdentifier)
//        self.clusterListCollectionView.showsHorizontalScrollIndicator = false
//        self.clusterListCollectionView.isPagingEnabled = true
//        self.collectionViewButtom.constant = -150
//        self.clusterListCollectionView.delegate = self
//        self.clusterListCollectionView.dataSource = self
//    }
    
    //MARK:- Make cluster in map
    func makeClusterMap(){
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: myMapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        self.clusterManager = GMUClusterManager(map: myMapView, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        
    }
    
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
}


//MARK:- map delegates
extension LogInMapVC : GMUClusterManagerDelegate {
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        self.setCameraPosition(lat:  cluster.position.latitude, long:  cluster.position.longitude)
        self.myMapView.animate(toZoom: self.myMapView.camera.zoom + 1.0)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        DispatchQueue.main.async {
            if let mark = marker.userData as? TagItem{
                
                self.setCameraPosition(lat: mark.tag.tagLatitude, long: mark.tag.tagLongitude, withAnimation: false, withZoomLevel: self.myMapView.camera.zoom)
                
                self.isNavigatedToTagDetail = true
                let vc = TagDetailVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
                vc.tagId = mark.tag.id
                AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
                
            }
        }
        
        return nil
        
    }
    
}



extension LogInMapVC : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        UIView.animate(withDuration: 0.3) {
            //self.collectionViewButtom.constant = -150
            self.view.layoutIfNeeded()
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if let mark = marker.userData as? TagItem{
            let vc = TagDetailVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
            vc.tagId = mark.tag.id
            AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
        }
    }
}

extension LogInMapVC : GMUClusterRendererDelegate{
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        
        if let mark = marker.userData as? TagItem{
            
            let markerView : CustomMarkerView = .instantiateFromNib()
            markerView.populateData(tag: mark.tag)
            markerView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            marker.iconView = markerView
        }
    }
    
}



//MARK:- Map related methods
extension LogInMapVC {
    
    //MARK:- Add marker to cordinates
    func addMarkerToCordinates(lat : Double, long : Double){
        let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let marker = GMSMarker(position: position)
        marker.icon = AppImages.blueMarker.image
        marker.map = myMapView
    }
    
    func setCameraPosition(lat : Double, long : Double, withAnimation : Bool = true, withZoomLevel : Float = 14){
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: withZoomLevel)
        withAnimation ? (self.myMapView.animate(to: camera)) : (self.myMapView.camera = camera)
        //        self.mapView.animate(to: camera)
    }
}




extension LogInMapVC : GetTagsDelegate {
    
    func willRequestCommunities() {
        self.view.showIndicator()
        
    }
    
    func communitiesReceivedSuccessfully(comunities: [Tag]) {
        if comunities.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.NoTagsFound.localized)
        }
        self.view.hideIndicator()
        self.tags = comunities
        printDebug(self.tags.count)
        self.myMapView.clear()
        clusterManager.clearItems()
        generateClusterItems()
        self.setCameraPosition(lat: SelectedFilters.shared.appliedFiltersOnTag.lat, long: SelectedFilters.shared.appliedFiltersOnTag.long)
    }
    
    func failedToReceiveCommunities() {
        self.view.hideIndicator()
    }
}

