//
//  VideoPlayerVC.swift
//  TagHawk
//
//  Created by Apple on 13/11/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import YoutubeKit

class VideoPlayerVC: UIViewController {

    
    @IBOutlet weak var playerView: UIView!
    
      private var player: YTSwiftyPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.configurePlayer()
        
        // Do any additional setup after loading the view.
    }
    

    
    
    func configurePlayer(){
        
        // Create a new player
        player = YTSwiftyPlayer(
            frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenWidth * 9 / 16),
            playerVars: [.playsInline(true), .videoID("m1pnwFSdOLU"), .loopVideo(false), .showRelatedVideo(false)])
        
        // Enable auto playback when video is loaded
        
        player.autoplay = true
        
        // Set player view
        
        //        playerView = player
        playerView.addSubview(player)
        // Set delegate for detect callback information from the player
        player.delegate = self
        
        // Load video player
        player.loadPlayer()
    }
    
    
    
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:- Youtube delegate
extension VideoPlayerVC: YTSwiftyPlayerDelegate {
    
    func playerReady(_ player: YTSwiftyPlayer) {
        printDebug(#function)
    }
    
    func player(_ player: YTSwiftyPlayer, didUpdateCurrentTime currentTime: Double) {
        printDebug("\(#function):\(currentTime)")
    }
    
    func player(_ player: YTSwiftyPlayer, didChangeState state: YTSwiftyPlayerState) {
        printDebug("\(#function):\(state)")
    }
    
    func player(_ player: YTSwiftyPlayer, didChangePlaybackRate playbackRate: Double) {
        printDebug("\(#function):\(playbackRate)")
    }
    
    func player(_ player: YTSwiftyPlayer, didReceiveError error: YTSwiftyPlayerError) {
        printDebug("\(#function):\(error)")
    }
    
    func player(_ player: YTSwiftyPlayer, didChangeQuality quality: YTSwiftyVideoQuality) {
        printDebug("\(#function):\(quality)")
    }
    
    func apiDidChange(_ player: YTSwiftyPlayer) {
        printDebug(#function)
    }
    
    func youtubeIframeAPIReady(_ player: YTSwiftyPlayer) {
        printDebug(#function)
    }
    
    func youtubeIframeAPIFailedToLoad(_ player: YTSwiftyPlayer) {
        printDebug(#function)
    }
}
