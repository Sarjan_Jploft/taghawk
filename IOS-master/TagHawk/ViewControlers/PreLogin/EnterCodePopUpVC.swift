//
//  EnterCodePopUpVC.swift
//  TagHawk
//
//  Created by Admin on 8/22/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ENterCodeDelegate : class {
    func getCodeBack(code : String)
    func skipAndContinue()
}

class EnterCodePopUpVC : BaseVC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var popupView: UIView!
    
    weak var delegate : ENterCodeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popupView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @IBAction func clickCancelButton(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
        self.delegate?.skipAndContinue()
    }
    
    @IBAction func clickApplyButton(_ sender: UIButton) {
        view.endEditing(true)
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        guard let code = self.textField.text else { return }
        if code.isEmpty { CommonFunctions.showToastMessage(LocalizedString.codeEmpty.localized)
            return
        }
        
        self.delegate?.getCodeBack(code: code)
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension EnterCodePopUpVC {
    
    private func setUpSubView() {
        textField.returnKeyType = .done
        self.textField.placeholder = LocalizedString.Invitation_Code_Optional.localized
        self.view.backgroundColor = UIColor.clear
    }
    
    func willJoinTag() {
        self.view.showIndicator()
    }
    
}

extension EnterCodePopUpVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
