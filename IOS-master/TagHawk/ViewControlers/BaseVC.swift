//
//  BaseVC.swift
//  TagHawk
//
//  Created by Appinventiv on 18/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import  AMScrollingNavbar

struct EmptyDataSetDetails {
    
    let heading : String
    let descriptiin : String
    let image : UIImage?
    
    init(){
        self.heading = ""
        self.descriptiin = ""
        self.image = nil
    }
    
    init(heading: String,descriptiin : String,image : UIImage?){
        self.heading = heading
        self.descriptiin = descriptiin
        self.image = image
        
    }
}

struct EmptyDataSetLayoutAttributes {
    let titleFont : UIFont
    let descriptionFont : UIFont
    let spacingInBetween : Int
    
    init(titleFont : UIFont, descriptionFont : UIFont, spacingInBetween : Int){
        self.titleFont = titleFont
        self.descriptionFont = descriptionFont
        self.spacingInBetween = spacingInBetween
    }
    
}


class BaseVC : ScrollingNavigationViewController {

    var page = 1
    var nextPage = 0
    var shouldDisplayEmptyDataView : Bool = false
    var emptyDataSetData = EmptyDataSetDetails()
    var emptyDataSetAttributes = EmptyDataSetLayoutAttributes(titleFont: AppFonts.Galano_Semi_Bold.withSize(17), descriptionFont: AppFonts.Galano_Regular.withSize(15), spacingInBetween: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bindControler()
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        // Do any additional setup after loading the view.
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setBarShadow(color: UIColor.clear)
        self.leftBarItemImage(change: AppImages.backWhite.image, ofVC: self)
    }

    func bindControler(){
    
    }
    
    func resetPage(){
        self.page = 1
        self.nextPage = 0
    }
}

extension BaseVC : UIGestureRecognizerDelegate {
    

    func shareUrl(url: String, subject: String = "") {
        
       
            let video = URL(string: url)
            let  textToShare = [ video ]
            let activityViewController =
                UIActivityViewController(activityItems: textToShare as [Any],
                                         applicationActivities: nil)
        
        activityViewController.setValue(subject, forKey: "Subject")
        
            present(activityViewController, animated: true)
    }
}

//MARK:- Configure navigationBar
extension BaseVC{
    
    
    // MARK:- Function to change the image of leftBarButtonItem and add the pop functionality
    public func leftBarItemImage(change withImage: UIImage, ofVC target: BaseVC){
        
        var image: UIImage {
            if withImage.hasContent {
                return withImage.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            } else {
                return withImage
            }
        }
        
        let barButtonItem =  UIBarButtonItem(image: image, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.leftBarButtonTapped))
        /// shifting the image to the left by 8 points.
        barButtonItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        target.navigationItem.leftBarButtonItem = barButtonItem
        
    }
    
    @objc func leftBarButtonTapped(){
        
    }
    
    public func rightBarItemImage(change withImage: UIImage, ofVC target: BaseVC){
        
        var image: UIImage {
            if withImage.hasContent {
                return withImage.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            } else {
                return withImage
            }
        }
        
        target.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.rightBarButtonTapped))
    }
    
    public func rightBarTitle(title: String, titleColor : UIColor = AppColors.appBlueColor ,ofVC target: BaseVC){
        
        let barButton = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.rightBarButtonTapped))
        barButton.tintColor = AppColors.appBlueColor
        target.navigationItem.rightBarButtonItem = barButton
        
    }
    
    
    @objc func rightBarButtonTapped(_ sender : UIBarButtonItem) {
        
    }
    
    func configureNavigationScrolling(scrollView : UIScrollView){
        
        if let navController = self.navigationController as? ScrollingNavigationController, let tabBar = AppNavigator.shared.tabBar?.tabBar {
            
            navController.followScrollView(scrollView  , delay: 0, scrollSpeedFactor: 2, followers: [NavigationBarFollower(view: tabBar, direction: .scrollDown)])
            
            navController.scrollingNavbarDelegate = self
            navController.expandOnActive = false
        }
    }
    
}

extension BaseVC : ScrollingNavigationControllerDelegate{
    
    func scrollingNavigationController(_ controller: ScrollingNavigationController, didChangeState state: NavigationBarState) { }
    
    @objc func scrollViewDidScroll(_ scrollView: UIScrollView) {
        printDebug("scrollViewDidScroll")
    }
}

extension BaseVC : CustomPopUpDelegate {
    
    func okButtonTapped(for: CustomPopUpVC.CustomPopUpFor) {
        
    }
    
   
    func showPopUp(type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none, msg : String = ""){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegate = self
        vc.msg = msg
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
     @objc func okButtonTapped() {
        
    }
    
 func openActionSheetWith(arrOptions: [String], openIn viewController: UIViewController?, optionSelected: @escaping(_ index: Int) -> ()) {
        
        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // Add Items in action sheet
        for title in arrOptions {
            let alertAction: UIAlertAction = UIAlertAction(title: title, style: .default, handler: { (action) in
                if let alertIndex = actionSheet.actions.index(of: action) {
                    optionSelected(alertIndex)
                }
            })
            actionSheet.addAction(alertAction)
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: AppConstants.cancel.rawValue , style: .cancel, handler: { (alert) in
            
        })
    
        actionSheet.addAction(cancelAction)
        
        //Open
        var controller: UIViewController? = viewController
        if viewController == nil {
            controller = UIApplication.shared.keyWindow?.rootViewController
        }
        controller?.present(actionSheet, animated: true)
    }
    

    
}

extension BaseVC: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func setDataSourceAndDelegate(forScrollView : UIScrollView){
        forScrollView.emptyDataSetSource = self
        forScrollView.emptyDataSetDelegate = self
    }
    
    func setDataSourceAndDelegateForTable(forTableView : UITableView){
        forTableView.emptyDataSetSource = self
        forTableView.emptyDataSetDelegate = self
    }
    
    func setEmptyDataView(heading : String, description : String = "", image : UIImage? = nil) {
        self.emptyDataSetData = EmptyDataSetDetails(heading: heading, descriptiin: description, image: image)
    }
    
    func setEmptyDataViewAttributes(titleFont : UIFont, descriptionFont : UIFont, spacingInBetween : Int) {
        self.emptyDataSetAttributes = EmptyDataSetLayoutAttributes(titleFont: titleFont, descriptionFont: descriptionFont, spacingInBetween: spacingInBetween)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return self.shouldDisplayEmptyDataView
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: self.emptyDataSetData.heading , attributes: [.font: self.emptyDataSetAttributes.titleFont, .foregroundColor: UIColor.black])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: self.emptyDataSetData.descriptiin , attributes: [.font: self.emptyDataSetAttributes.descriptionFont, .foregroundColor: AppColors.lightGreyTextColor])
        
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return self.emptyDataSetData.image
    }

    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -30
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return CGFloat(self.emptyDataSetAttributes.spacingInBetween)
        
    }
    
    
    //MARK:- =============SET NAVIGATION BAR================
    func setNavigationBar(withTitle title: String = "",
                          firstLeftButtonImage: UIImage? = nil,
                          secLeftButtonImage: UIImage? = nil,
                          firstRightButtonImage: UIImage? = nil,
                          secRightButtonImage: UIImage? = nil,
                          navBarColor: UIColor = AppColors.whiteColor) {
        
        var firstLeftBtn = UIBarButtonItem()
        var secLeftBtn = UIBarButtonItem()
        var firstRightBtn = UIBarButtonItem()
        var secRightBtn = UIBarButtonItem()
        
        
        if let firLeftImg = firstLeftButtonImage {
            firstLeftBtn = UIBarButtonItem(image: firLeftImg,
                                           style: .plain,
                                           target: self,
                                           action: #selector(onClickFirstLeftNavigationBarButton))
        } else {
            firstLeftBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "icSearchClose"),
                                           style: .plain,
                                           target: self,
                                           action: #selector(onClickFirstLeftNavigationBackBarButton))
        }
        
        if let secLeftImg = secLeftButtonImage {
            secLeftBtn = UIBarButtonItem(image: secLeftImg,
                                         style: .plain,
                                         target: self,
                                         action: #selector(onClickSecLeftNavigationBarButton))
        }
        
        if let firRightImg = firstRightButtonImage {
            firstRightBtn = UIBarButtonItem(image: firRightImg,
                                            style: .plain,
                                            target: self,
                                            action: #selector(onClickFirstRightNavigationBarButton))
        }
        
        if let secRightImg = secRightButtonImage {
            secRightBtn = UIBarButtonItem(image: secRightImg,
                                          style: .plain,
                                          target: self,
                                          action: #selector(onClickSecRightNavigationBarButton))
        }
        
        self.updateNavigationBar(withTitle: title.capitalized,
                                 leftButtons: [firstLeftBtn,secLeftBtn],
                                 rightButtons: [firstRightBtn,secRightBtn],
                                 navBarColor:navBarColor)
    }
    
    
    //MARK:- =============UPDATING NAVIGATION BAR================
    func updateNavigationBar(withTitle title:String,
                             leftButtons:[UIBarButtonItem],
                             rightButtons:[UIBarButtonItem],
                             navBarColor: UIColor) {
        
        self.navigationController?.view.backgroundColor = navBarColor
        self.navigationController?.navigationBar.barTintColor = navBarColor
        self.navigationController?.navigationBar.tintColor = AppColors.themeColor
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationItem.leftBarButtonItems = leftButtons
        self.navigationItem.rightBarButtonItems = rightButtons
        self.title = title
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    @objc func onClickFirstLeftNavigationBackBarButton(_ sender: UIButton) {
        view.endEditing(true)
        self.pop()
    }
    
    @objc func onClickFirstLeftNavigationBarButton(_ sender: UIButton) {
        view.endEditing(true)
    }
    
    @objc func onClickSecLeftNavigationBarButton(_ sender: UIButton) {
        view.endEditing(true)
    }
    @objc func onClickFirstRightNavigationBarButton(_ sender: UIButton) {
        view.endEditing(true)
    }
    @objc func onClickSecRightNavigationBarButton(_ sender: UIButton) {
        view.endEditing(true)
    }

    /// Check Camera Access
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            self.presentCameraSettings()
        case .restricted:
            self.presentCameraSettings()
        case .authorized:
            break
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    
                } else {
                    self.presentCameraSettings()
                }
            }
        }
    }
    
    private func presentCameraSettings() {
        let alertController = UIAlertController(title: LocalizedString.error.localized,
                                                message: LocalizedString.CameraAccessDenied.localized,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: LocalizedString.cancel.localized, style: .default) { _ in
            self.dismiss(animated: true, completion: nil)
        })
        
        alertController.addAction(UIAlertAction(title: LocalizedString.settings.localized, style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        self.present(alertController, animated: true)
    }
}


//MARK:- Refresh vc's
extension BaseVC {
    
    func refreshHome(){
        guard let navVcs = AppNavigator.shared.tabBar?.viewControllers else { return }
        guard let homeNavigation = navVcs[0] as? UINavigationController else { return }
        let allNavigationVcs = homeNavigation.viewControllers
        
        for item in allNavigationVcs {
            if item.isKind(of: HomeVC.self){
                guard let homeObj = item as? HomeVC else { return }
                //homeObj.allButtonTapped()
                homeObj.homeItemsVc.refresh()
                homeObj.setSortingLabelTextForProducts()
            }
        }
    }
    
    func refreshTagShelf(){
        guard let navVcs = AppNavigator.shared.tabBar?.viewControllers else { return }
        guard let homeNavigation = navVcs[0] as? UINavigationController else { return }
        let allNavigationVcs = homeNavigation.viewControllers
        
        for item in allNavigationVcs {
            if item.isKind(of: TagShelfVC.self){
                guard let homeObj = item as? TagShelfVC else { return }
                homeObj.refresh()
                homeObj.setSortingText()
            }
        }
        
        guard let messageNavigation = navVcs[2] as? UINavigationController else { return }
        let allMessageNavigationVcs = messageNavigation.viewControllers

        for item in allMessageNavigationVcs {
            if item.isKind(of: TagShelfVC.self){
                guard let homeObj = item as? TagShelfVC else { return }
                homeObj.refresh()
                homeObj.setSortingText()
            }
        }
    }
    
    func refreshSellfingTabForUserProfile(){
        guard let navVcs = AppNavigator.shared.tabBar?.viewControllers else { return }
        guard let homeNavigation = navVcs[3] as? UINavigationController else { return }
        let allNavigationVcs = homeNavigation.viewControllers
        
        for item in allNavigationVcs {
            if item.isKind(of: UserProfileVC.self){
                guard let profileObj = item as? UserProfileVC, let savingvc = profileObj.savingVC else { return }
                savingvc.refresh()
            }
        }
    }
}

