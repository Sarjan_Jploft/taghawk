//
//  TagHawkTabBarControler.swift
//  TagHawk
//
//  Created by Appinventiv on 24/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class TagHawkTabBarControler : UITabBarController {
    
    
    @IBOutlet weak var viewButtonContain: UIView!
    @IBOutlet weak var viewBackRoundCam: UIView!
    @IBOutlet weak var viewFrontRoundCam: UIView!
    
    @IBOutlet var buttonsContainerView: UIView!
    @IBOutlet weak var homeBackView: UIView!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var hawkDriverView: UIView!
    @IBOutlet weak var hawkDriverButton: UIButton!
    @IBOutlet weak var messageBackView: UIView!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var profileBackView: UIView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var cameraBUtton: UIButton!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageCount: UILabel!
    
    var backView = UIView()
    let homeControler = HomeControler()
    let chatController = FireBaseChat()
    private let settingsController = SettingsControler()
    var previousIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.barStyle = .black
        self.tabBar.backgroundColor = UIColor.clear
        self.homeControler.createMerchangtDelegate = self
        chatController.delegate = self
        chatController.getTotalUnreadMessage()
        messageView.round()
        messageView.isHidden = true
        messageView.layer.borderWidth = 1.5
        messageView.layer.borderColor = AppColors.whiteColor.cgColor
        messageView.backgroundColor = AppColors.deleteBackgroundColor
        messageCount.font = AppFonts.Galano_Medium.withSize(9)
        messageCount.textColor = AppColors.whiteColor
        let tap = UITapGestureRecognizer(target: self, action: #selector(messageViewTapped))
        self.messageView.addGestureRecognizer(tap)
        self.messageView.isUserInteractionEnabled = true
        self.delegate = self
        
        self.tabBar.frame.origin.y = screenHeight - (Display.typeIsLike == .iphoneX || Display.typeIsLike == .unknown ? 83 : 49)
        self.view.layoutIfNeeded()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override  init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    //MARK:- init Tabbar
    func initTabbar(){
        self.setBackgrounfViews()
        self.setUpButtonsView()
        self.initViews()
    }
    
    //MARK:- Set tabbar background views
    func setBackgrounfViews(){
        
        //change
        self.viewBackRoundCam.layer.cornerRadius = self.viewBackRoundCam.frame.height/2
        self.viewBackRoundCam.clipsToBounds = true
        self.viewButtonContain.addShadow(ofColor: UIColor.black, radius: 5, offset: CGSize(width: 0, height: -6), opacity: 0.2, cornerRadius: nil)
        self.viewBackRoundCam.addShadow(ofColor: UIColor.black, radius: 5, offset: CGSize(width: 0, height: 0), opacity: 0.2, cornerRadius: nil)
        
//        self.viewFrontRoundCam.layer.cornerRadius = self.viewBackRoundCam.frame.height/2
//        self.viewFrontRoundCam.clipsToBounds = true
        
        
        let backViewHeight = Display.typeIsLike == .iphoneX ? 83 : 49
        let backViewWidth : CGFloat = self.view.frame.width
        self.backView = UIView(frame: CGRect(x: 0, y: 0, width: Int(backViewWidth), height: backViewHeight))
        self.backView.backgroundColor = UIColor.white
//        self.tabBar.isUserInteractionEnabled = false
        self.tabBar.addSubview(self.backView)
    }
    
    func setUpButtonsView(){
        
        self.buttonsContainerView.frame = CGRect(x: 0, y: 0, width: self.backView.frame.width, height: 49)
        //change
//        self.buttonsContainerView.frame = CGRect(x: 0, y: 0, width: self.backView.frame.width, height: 73)
        self.backView.addSubview(self.buttonsContainerView)
        
        self.homeButton.setImage(AppImages.homeDeselected.image, for: UIControl.State.normal)
        self.homeButton.setImage(AppImages.homeSelected.image, for: UIControl.State.selected)
        self.hawkDriverButton.setImage(AppImages.wallet.image, for: UIControl.State.normal)
        self.hawkDriverButton.setImage(AppImages.walletSelected.image, for: UIControl.State.selected)
        
        self.messageButton.setImage(AppImages.messageDeselected.image, for: UIControl.State.normal)
        self.messageButton.setImage(AppImages.messageSelected.image, for: UIControl.State.selected)
        
        self.profileButton.setImage(AppImages.profileDeselected.image, for: UIControl.State.normal)
        self.profileButton.setImage(AppImages.profileSelected.image, for: UIControl.State.selected)
    }
    
    func initViews(){
        //        let unitWidth = buttonsBackView.frame.width / CGFloat(5)
        
        for tabBarItem in self.tabBar.items!{
            tabBarItem.title = ""
            tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                tabBarItem.isEnabled = false
        }
    }
    
    func showTabBar(animated:Bool) {
      
        if animated{
            UIView.animate(withDuration: 0.38, animations: { () -> Void in
                
                var tempHeght = 0.0
                if Display.typeIsLike == .iphoneX || Display.typeIsLike == .iPhoneXsOrXROrXi
                {
                    tempHeght = 83
                }
                else
                {
                    tempHeght = 49
                }
                self.tabBar.frame.origin.y = screenHeight - CGFloat(tempHeght)
//                self.tabBar.frame.origin.y = screenHeight - (Display.typeIsLike == .iphoneX   || Display.typeIsLike == .unknown ? 83 : 49)
                self.view.layoutIfNeeded()
            }, completion: {(success) in
            })
        }else{
            
            var tempHeght = 0.0
            if Display.typeIsLike == .iphoneX || Display.typeIsLike == .iPhoneXsOrXROrXi
            {
                tempHeght = 83
            }
            else
            {
                tempHeght = 49
            }
            self.tabBar.frame.origin.y = screenHeight - CGFloat(tempHeght)
//            self.tabBar.frame.origin.y = screenHeight - (Display.typeIsLike == .iphoneX || Display.typeIsLike == .unknown ? 83 : 49)
            self.view.layoutIfNeeded()
        }
    }
    
    //hide tabBar with animation
    func hideTabBar(animated:Bool) {
        if animated{
            UIView.animate(withDuration: 0.38, animations: { () -> Void in
                self.tabBar.frame.origin.y = screenHeight + (Display.typeIsLike == .iphoneX || Display.typeIsLike == .unknown ? 83 : 49)
                self.view.layoutIfNeeded()
            }, completion: {(success) in
            })
        }else{
            self.tabBar.frame.origin.y = screenHeight + (Display.typeIsLike == .iphoneX || Display.typeIsLike == .unknown ? 83 : 49)
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func tabBarButtonsTapped(_ sender: UIButton) {
        view.endEditing(true)
    
        switch sender {
        case self.homeButton:
            self.selectTab(index: 0)
            
        case self.hawkDriverButton:
            self.selectTab(index: 1)
            
        case self.cameraBUtton:
            self.selectTab(index: 2)

        case self.messageButton:
            self.selectTab(index: 3)
            
        case self.profileButton:
            self.selectTab(index: 4)
            
        default:
            break
        }
    }
    
    @objc func messageViewTapped(){
        self.selectTab(index: 3)
    }
}


extension TagHawkTabBarControler : UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
      return false
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem){
       // let index = tabBar.items?.index(of: item)
       // self.selectTab(index: index ?? 0)
    }
    
    func selectTab(index : Int){
        
        switch index {
        case 0:
            self.selectedIndex = index
            previousIndex = index
            self.selectHoneButton()

        case 1:
            if UserProfile.main.loginType == .none {
                showLoginSignupPopup()
                return
            }
            self.selectHawkDriverTab()
            self.selectedIndex = index
            previousIndex = index
        
        case 2:
            if UserProfile.main.loginType == .none {
                showLoginSignupPopup()
                return
            }
            self.selectedIndex = index
            self.selectCameraTab()
      
        case 3:
            if UserProfile.main.loginType == .none {
                showLoginSignupPopup()
                return
            }
            self.selectedIndex = index
            previousIndex = index
            self.selectMessageTab()

        case 4:
            if UserProfile.main.loginType == .none {
                showLoginSignupPopup()
                return
            }
            self.selectedIndex = index
            previousIndex = index
            self.selectProfileTab()
            
        default: break
        }
        
        if index != 3{
            removeObservers()
        }
    }
    
    func removeObservers(){
        if let tabBar = AppNavigator.shared.tabBar,
            let vc = tabBar.viewControllers?[3] as? MessageVC {
            vc.messagesVC.controller.removeObservers()
        }
    }
    
    func selectHoneButton(){
        self.homeButton.isSelected = true
        self.hawkDriverButton.isSelected = false
        self.messageButton.isSelected = false
        self.profileButton.isSelected = false
    }
    
    func selectHawkDriverTab(){
        self.homeButton.isSelected = false
        self.hawkDriverButton.isSelected = true
        self.messageButton.isSelected = false
        self.profileButton.isSelected = false
    }
    
    func selectCameraTab(){
        self.openCamera()
       
      //  CommonFunctions.delay(delay: 0.1) {
            self.selectTab(index: self.previousIndex)
      //  }
    }
    
    func openCamera(){
        let vc = CameraVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: true)
        vc.addProductSuccessFullDelegate = self
        vc.modalPresentationStyle = .fullScreen
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
    
    func selectMessageTab(){
        
        if UserProfile.main.loginType == .none {
            showLoginSignupPopup()
            return
        }
        
        self.homeButton.isSelected = false
        self.hawkDriverButton.isSelected = false
        self.messageButton.isSelected = true
        self.profileButton.isSelected = false
        self.selectedIndex = 2
    }
    
    func selectProfileTab(){
        self.homeButton.isSelected = false
        self.hawkDriverButton.isSelected = false
        self.messageButton.isSelected = false
        self.profileButton.isSelected = true
        self.selectedIndex = 3
    }
    
    func showLoginSignupPopup() {
        
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        settingsController.logoutDelegate = self
        vc.popupFor = .loginSignup
        vc.delegateWithType = self
        vc.modalPresentationStyle = .overCurrentContext
        AppNavigator.shared.parentNavigationControler.present(vc, animated: true, completion: nil)
    }
}


extension TagHawkTabBarControler : ProductCreatedSuccessFully{
    
    func addProductSuccessFully(prod: AddProductData) {
        let vc = FeatureProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.addProductData = prod
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true) {
        }
    }
    
}

extension TagHawkTabBarControler : PostnotherDelegate{
  
    func productPramotedSuccessfull() {
        
    }
    
    func postAnother() {
        self.selectCameraTab()
    }
}

extension TagHawkTabBarControler : CustomPopUpDelegateWithType {
   
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        if type == .enterBankandProfileDetails{
           let vc = EditProfileVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.userData = UserProfile.main
            self.present(vc, animated: true, completion: nil)
        }else if type == .enterBankDetails{
            let vc = AddDepositAccountVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
//            vc.addDetailsFrom = .tabbar
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        if type == .loginSignup {
            settingsController.logout()

        }
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        if type == .loginSignup {
            settingsController.logout()
        }
        
    }
    
    func openPaymentVc(){
        let vc = PaymentVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension TagHawkTabBarControler : CreateMerchantDelegate {
    
    func willrequestCreteMerchant(){
        self.view.showIndicator()
        
    }
    
    func merchantCreatedSuccessFully(customerId: String, from: AddDepositAccountVC.AddBankDetailsWhile) {
        self.view.hideIndicator()
        self.openCamera()
    }
    
    func failedToCreateMerchant(message : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
}

extension TagHawkTabBarControler: FireBaseChatDelegate {
    
    func getTotalCount(){
        if let data = chatController.userData{
            messageView.isHidden = !(data.totalUnreadCount > 0)
            messageCount.text = data.totalUnreadCount > 99 ? "99+" : "\(data.totalUnreadCount)"
        }else{
            messageView.isHidden = true
        }
    }
    
}


extension TagHawkTabBarControler: LogoutDelegate {
    
    func willLogout() {
        view.showIndicator()
    }
    
    func logoutSuccessFully() {
        view.hideIndicator()
        AppNavigator.shared.actionsOnLogout(comingFromGuest: true)
    }
    
    func failedToLogout(message: String) {
        view.hideIndicator()
    }
}


