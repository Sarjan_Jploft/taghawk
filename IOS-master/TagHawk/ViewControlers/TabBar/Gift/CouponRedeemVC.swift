//
//  CouponRedeemVC.swift
//  TagHawk
//
//  Created by Admin on 5/21/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import AMScrollingNavbar

protocol ProductPromotedSuccessfullyWithPoints : class {
    func promotedSuccessfully(withPoints : Int)
}

class CouponRedeemVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var productsCollectionView: UICollectionView!
    @IBOutlet weak var redeemButton: UIButton!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataHeadingLabel: UILabel!
    @IBOutlet weak var noDataDescriptionView: UILabel!
    @IBOutlet weak var noDataViewHeight: NSLayoutConstraint!
    @IBOutlet weak var safeAreaImageView: UIImageView!
    
    //MARK:- Variables
    private var products : [Product] = []
    private var selectedProducts : [Product] = []
    var selectedPlan = GiftPlan()
    var totalPoints : Int = 0
    private let controler = GiftControler()
    weak var delegate : ProductPromotedSuccessfullyWithPoints?
    
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func bindControler() {
        self.controler.productDelegate = self
        self.controler.promotionDelegate = self
    }
    
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBActions
    @IBAction func redeemButtonTapped(_ sender: UIButton) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.promoteSelectedProducts(productsToPromote: self.selectedProducts, selectedGiftPlan: self.selectedPlan, totalPoints: self.totalPoints)
        
    }
    
}

//MARK:- Private functions
private extension CouponRedeemVC {
    
    func setUpSubView(){
        self.redeemButton.setAttributes(title: LocalizedString.Redeem.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)
        configureCollectionView()
        self.controler.getUserProduct(userId: UserProfile.main.userId, productStatus: ProfileProductVC.ProductsType.Selling.rawValue , page: self.page)
        self.noDataViewHeight.constant = screenHeight - (screenHeight * 563 / 1334) - 64
    }
    
    func configureCollectionView(){
        self.productsCollectionView.registerNib(nibName: ProfileCollectionViewCell.defaultReuseIdentifier)
        self.productsCollectionView.register(UINib(nibName: "CoupanRedeemHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: CoupanRedeemHeaderView.reuseIdentifier)
        self.productsCollectionView.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)
        self.productsCollectionView.delegate = self
        self.productsCollectionView.dataSource = self
        self.noDataHeadingLabel.setAttributes(text: LocalizedString.Oops_It_Is_Empty.localized, font: AppFonts.Galano_Semi_Bold.withSize(17), textColor: UIColor.black)
        self.noDataDescriptionView.setAttributes(text: LocalizedString.No_Products_To_Promote.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: AppColors.lightGreyTextColor)
    }
    
}

//MARK:- Navigation configuration
private extension CouponRedeemVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Coupon_Redeem.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
        if let navController = self.navigationController as? ScrollingNavigationController, let tabBar = AppNavigator.shared.tabBar?.tabBar {
            navController.followScrollView(self.productsCollectionView  , delay: 0, scrollSpeedFactor: 2, followers: [NavigationBarFollower(view: tabBar, direction: .scrollDown)])
            navController.scrollingNavbarDelegate = self
            navController.expandOnActive = false
        }
    }
}

//MARK:- CollectionView Datasource and Delegats
extension CouponRedeemVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CoupanRedeemHeaderView", for: indexPath) as? CoupanRedeemHeaderView else {
            fatalError("CoupanRedeemHeaderView not found")
        }
        header.populateData(plan: self.selectedPlan, totalPoints: self.totalPoints)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: screenHeight * 563 / 1334)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 2
        return CGSize(width: finalContentWidth / 3, height: finalContentWidth / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getProductCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueCell(with: ProfileCollectionViewCell.self, indexPath: indexPath)
        if !products[indexPath.item].images.isEmpty{
            cell.productImage.setImage_kf(imageString: products[indexPath.item].images[0].image, placeHolderImage: UIImage())
        }else{
            cell.productImage.image = UIImage()
        }
        cell.moreBtn.isHidden = true
        cell.selectionImageView.isHidden = !self.selectedProducts.contains(where: { (product) -> Bool in
            return product.productId == self.products[indexPath.item].productId
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let index = self.selectedProducts.lastIndex(where: { $0.productId == self.products[indexPath.item].productId}){
            self.selectedProducts.remove(at: index)
        } else {
            self.selectedProducts.append(self.products[indexPath.item])
        }
        self.productsCollectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if self.products.count >= 17 {
            if indexPath.item == self.products.count - 1 && self.nextPage != 0{
                if !AppNetworking.isConnectedToInternet {
                    CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                    return
                }
                self.controler.getUserProduct(userId: UserProfile.main.userId, productStatus: ProfileProductVC.ProductsType.Selling.rawValue , page: self.page)
            }
        }
    }
}

//MARK:- Webservice response
extension CouponRedeemVC  : UserProductDelegate {
    
    func willGetProducts() {
        self.view.showIndicator()
        self.noDataView.isHidden = true
        self.redeemButton.isHidden = true
        self.safeAreaImageView.isHidden = true
    }
    
    func userProductServiceReturn(userProduct: [Product], nextPage: Int) {
        self.view.hideIndicator()
        self.noDataView.isHidden = !(self.page == 1 && userProduct.isEmpty)
        self.redeemButton.isHidden = false
        self.safeAreaImageView.isHidden = false
        self.nextPage = self.page
        self.products.append(contentsOf: userProduct)
        self.productsCollectionView.reloadData()
    }
    
    func userProductsFailed(errorType: ApiState) {
        self.view.hideIndicator()
        self.noDataView.isHidden = false
        self.redeemButton.isHidden = false
        self.safeAreaImageView.isHidden = false
    }
    
    func productDeleteSuccess() {
        
    }
    
    func productDeleteFailed() {
        
    }
}

extension CouponRedeemVC : PromoteSelectedProductsDelegate {
    
    func vaildatePromotion(msg: String) {
        CommonFunctions.showToastMessage(msg)
    }
    
    func willPromoteProduct() {
        self.view.showIndicator()
        
    }
    
    func productsPromotedSuccessfully(msg : String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
        self.delegate?.promotedSuccessfully(withPoints: selectedPlan.rewardPoint * self.selectedProducts.count)
        self.updatePromotedProducts()
        self.navigationController?.popViewController(animated: true)
    }
    
    func updatePromotedProducts(){
        
        guard let tabBar = AppNavigator.shared.tabBar else {  return }
        guard let navigations = tabBar.viewControllers else { return }
        
        guard let lastNav = navigations[3] as? UINavigationController else { return }
        let allvcs = lastNav.viewControllers
        for item in allvcs {
            if item.isKind(of: UserProfileVC.self){
                guard let profileVC = item as? UserProfileVC else { return }
                profileVC.updatePromotedProductsStatus(promotedProducts: self.selectedProducts)
            }
        }
    }
    
    func failedToPromoteProduct(msg: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
        
    }
    
}
