//
//  KnowMoreForGiftVC.swift
//  TagHawk
//
//  Created by Admin on 6/10/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class KnowMoreForGiftVC : BaseVC {

    //MARK:- IBOutlets
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var cancellButton: UIButton!
    @IBOutlet weak var redeemedHeadingLabel: UILabel!
    @IBOutlet weak var redeemedSecondPointLabel: UILabel!
    @IBOutlet weak var redeemFirstPointLabel: UILabel!
    @IBOutlet weak var gainedHeadingLabel: UILabel!
    @IBOutlet weak var gainedFirstPointLabel: UILabel!
    @IBOutlet weak var gainedSecondPointLabel: UILabel!
    @IBOutlet weak var popUpView: UIView!
   
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSubView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:- View life cycle
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}


extension KnowMoreForGiftVC {
    
    func setUpSubView(){
       
        redeemedHeadingLabel.setAttributes(text: LocalizedString.Reward_Points_Redeemed_For.localized, font: AppFonts.Galano_Bold.withSize(16), textColor: UIColor.black)
      
        redeemFirstPointLabel.setAttributes(text: LocalizedString.Promoting_Your_Selling_Items.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: UIColor.black)
       
        redeemedSecondPointLabel.setAttributes(text: LocalizedString.Creating_Tags_On_Map.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: UIColor.black)

        gainedHeadingLabel.setAttributes(text: LocalizedString.Reward_Points_Can_Be_Gained_From.localized, font: AppFonts.Galano_Bold.withSize(16), textColor: UIColor.black)

        gainedFirstPointLabel.setAttributes(text: LocalizedString.Each_New_Signup_With_Reward_Both_Of_You_50_Points.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: UIColor.black)
    
        gainedSecondPointLabel.setAttributes(text: LocalizedString.Each_Dollar_Transaction_Will_Reward_You_1_Point.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: UIColor.black)

        let tap = UITapGestureRecognizer(target: self, action: #selector(dismisViewtapped))
        self.dismissView.addGestureRecognizer(tap)
        self.dismissView.isUserInteractionEnabled = true
        
    }
    
    
    @objc func dismisViewtapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

