//
//  MyRewardsVCViewController.swift
//  TagHawk
//
//  Created by Admin on 5/20/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class MyRewardsVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var myRewardsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rewardPointsLabel: UILabel!
    @IBOutlet weak var rewardPointsEarned: UILabel!
    @IBOutlet weak var youCanRedeemLabel: UILabel!
    @IBOutlet weak var knowMoreButton: UIButton!
    @IBOutlet weak var rewardPointsBackView: UIView!
    @IBOutlet weak var packsHeadingLabel: UILabel!
    @IBOutlet weak var packsCollectionView: UICollectionView!
    
    
    //Variable
    let controler = GiftControler()
    var plans : [GiftPlan] = []
    private var totalPoints : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.packageListDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    //MARK:- IBActions
    
    //MARK:- Back button Tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Know more button tapped
    @IBAction func knowMoreButtonTapped(_ sender: UIButton) {
        let vc = KnowMoreForGiftVC.instantiate(fromAppStoryboard: AppStoryboard.Gift)
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
}

//MARK:- Navigation configuration
extension MyRewardsVC {
    
    func configureNavigation() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}

//MARK:- Private functions
private extension MyRewardsVC {
    
    func setUpSubView(){
        self.rewardPointsLabel.setAttributes(text: "670", font: AppFonts.Galano_Bold.withSize(40), textColor: AppColors.appBlueColor)
        self.rewardPointsEarned.setAttributes(text: LocalizedString.Reward_Points_Earned.localized, font: AppFonts.Galano_Medium.withSize(14), textColor: AppColors.blackColor)
        self.youCanRedeemLabel.setAttributes(text: LocalizedString.You_Can_Redeem_Reward_Points.localized, font: AppFonts.Galano_Regular.withSize(10), textColor: AppColors.lightGreyTextColor)
        self.knowMoreButton.setAttributes(title: LocalizedString.Know_More.localized, font: AppFonts.Galano_Regular.withSize(12), titleColor: AppColors.appBlueColor)
        self.titleLabel.setAttributes(text: LocalizedString.My_Rewards.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.white)
        self.packsHeadingLabel.setAttributes(text: LocalizedString.Use_Reward_Points_To_Redeem.localized, font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        self.rewardPointsBackView.round(radius: 5)
        self.rewardPointsBackView.setBorder(width: 1.0, color: AppColors.textfield_Border)
        self.rewardPointsBackView.drawShadow()
        self.configureCollectionView()
        self.controler.getPromotionPackages()
    }
    
    func configureCollectionView(){
        self.packsCollectionView.registerNib(nibName: RewardCollViewCell.defaultReuseIdentifier)
        self.packsCollectionView.delegate = self
        self.packsCollectionView.dataSource = self
        self.packsCollectionView.contentInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
}

//MARK:- Collectionview datasource and delegates
extension MyRewardsVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.plans.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RewardCollViewCell.defaultReuseIdentifier, for: indexPath) as? RewardCollViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        
        cell.populateData(plan: self.plans[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let finalWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right) - 17
        
        return CGSize(width : finalWidth/2, height: finalWidth/2)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 17
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 17
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = CouponRedeemVC.instantiate(fromAppStoryboard: AppStoryboard.Gift)
        vc.selectedPlan = self.plans[indexPath.item]
        vc.totalPoints = self.totalPoints
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Webservice resolnse
extension MyRewardsVC : GetPromotionPackagesDelegate {
    
    func willRequestPromotionPackages(){
        self.view.showIndicator()
        self.rewardPointsBackView.isHidden = true
        self.packsHeadingLabel.isHidden = true
    }
    
    func promotionPackagesReceivedSuccessFully(plans: [GiftPlan], totalPoints: Int) {
        self.view.hideIndicator()
        self.plans = plans
        self.totalPoints = totalPoints
        self.rewardPointsLabel.text = "\(totalPoints)"
        self.packsCollectionView.reloadData()
        self.rewardPointsBackView.isHidden = false
        self.packsHeadingLabel.isHidden = false
    }
    
    func failedToReceivePromotionPackages(msg : String){
        self.view.hideIndicator()
        self.rewardPointsBackView.isHidden = false
        self.packsHeadingLabel.isHidden = false
    }
}


extension MyRewardsVC : ProductPromotedSuccessfullyWithPoints {
    
    func promotedSuccessfully(withPoints : Int) {
        self.totalPoints = self.totalPoints - withPoints
        self.rewardPointsLabel.text = "\(self.totalPoints)"
    }
    
}
