//
//  UnblockUserVC.swift
//  TagHawk
//
//  Created by Appinventiv on 12/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class BlockedUsersVC : BaseVC {

    @IBOutlet weak var unblockTableView: UITableView!
    
    var users : [FollowingFollowersModel] = []
    let controler = BlockedUserControler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        controler.delegate = self
        controler.unblockUserDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension BlockedUsersVC {
    
    func setUpSubView(){
        self.configureTableView()
        self.controler.getBlockedUserList(page: self.page)
    }
    
    func configureTableView(){
        self.unblockTableView.registerNib(nibName: BlockedUsersCell.defaultReuseIdentifier)
        self.unblockTableView.separatorStyle = .none
        self.unblockTableView.showsVerticalScrollIndicator = false
        self.configureEmptyDataSet()
        self.unblockTableView.delegate = self
        self.unblockTableView.dataSource = self
    }
    
    func configureEmptyDataSet(){
        self.setEmptyDataView(heading: LocalizedString.Oops_It_Is_Empty.localized, description: "", image: AppImages.noData.image)
        self.setEmptyDataViewAttributes(titleFont: AppFonts.Galano_Semi_Bold.withSize(15), descriptionFont: AppFonts.Galano_Regular.withSize(12), spacingInBetween: 10)
        self.setDataSourceAndDelegate(forScrollView: self.unblockTableView)
  }
    
    @objc func unblockButtonTapped(sender : UIButton){
 
        guard let indexPath = sender.tableViewIndexPath(self.unblockTableView) else { return }
        self.showPopUp(user: self.users[indexPath.row], type: RemoveFriendPopUp.RemoveFriendFor.unblockUser)
//        self.controler.unBlockUser(user: self.users[indexPath.row]._id)
        
    }
    
    func showPopUp(user : FollowingFollowersModel, type : RemoveFriendPopUp.RemoveFriendFor){
        let vc = RemoveFriendPopUp.instantiate(fromAppStoryboard: AppStoryboard.Profile)
        vc.commingFor = type
        vc.user = user
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
}

extension BlockedUsersVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Blocked_Users.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
}


//MARK:- TableView Datasource and Delegate
extension BlockedUsersVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BlockedUsersCell.defaultReuseIdentifier) as? BlockedUsersCell else {
            fatalError("BlockedUsersCell....\(BlockedUsersCell.defaultReuseIdentifier) cell not found")}
        cell.populateData(user: self.users[indexPath.row])
        cell.unblockButton.addTarget(self, action: #selector(unblockButtonTapped), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let userProfileScene = OtherUserProfileVC.instantiate(fromAppStoryboard: .Profile)
//        userProfileScene.userId = self.users[indexPath.row].userId
//        navigationController?.pushViewController(userProfileScene, animated: true)
    }
}

extension BlockedUsersVC : GetBlockUsersList {

    func willGetBlockUsers(){
        self.view.showIndicator()
        
    }
    
    func blockedUsersReceivedSuccessfully(users : [FollowingFollowersModel], nextPage : Int){
        self.view.hideIndicator()
        self.users = users
        self.nextPage = nextPage
        self.unblockTableView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.unblockTableView.reloadEmptyDataSet()
    }
    
      func failedToGetBlockedUsers(message: String) {
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = true
        self.unblockTableView.reloadEmptyDataSet()
    }
    
}


extension BlockedUsersVC : UnBlockUserDelegate{
    
    func willRequestUnBlockUser() {
        self.view.showIndicator()
        
    }
    
    func unBlockUserSuccessfull(user: String) {
        self.view.hideIndicator()
        
        if let ind = self.users.index(where: { $0._id == user }) {
            self.users.remove(at: ind)
            self.unblockTableView.reloadData()
            
            if self.users.isEmpty{
                self.shouldDisplayEmptyDataView = true
                self.unblockTableView.reloadEmptyDataSet()
            }
        }
    }
    
    func failedToUnBlockUser(message: String) {
        self.view.hideIndicator()
        
    }
}


extension BlockedUsersVC : RemoveFriendPopUpDelegate{
    
    func noButtonTapped(type: RemoveFriendPopUp.RemoveFriendFor) {
        
    }
    
    func yesButtonTapped(type: RemoveFriendPopUp.RemoveFriendFor, user: FollowingFollowersModel) {
        
        if !AppNetworking.isConnectedToInternet {CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        self.controler.unBlockUser(user: user._id)
     
    }
}
