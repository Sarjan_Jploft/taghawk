//
//  DocumentPopUpVC.swift
//  TagHawk
//
//  Created by Admin on 6/12/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Photos
import IQKeyboardManagerSwift

protocol GetDisputeReasons : class {
    func getDisputeReasonsBack(mssg : String, images: [String], paymentHistory : PaymentHistory)
}

class DisputePopUpVC : UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var documentCollectionView: UICollectionView!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var proofHeadingLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var selectDocButton: UIButton!
    @IBOutlet weak var messageTextView: IQTextView!
    
    //MARK:- Variables
    var selectedMediaArray = [Media]()
    var images = [UIImage]()
    var imageUrl = [String]()
    var serviceReturnSuccess: (() -> ())?
    weak var delegate: GetDisputeReasons?
    var paymentHistory = PaymentHistory()

    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       IQKeyboardManager.shared.enable = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    //MARK:- IBActions
    
      @IBAction func clickApplyButton(_ sender: UIButton) {
        if self.messageTextView.text.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.Please_Write_A_Reason_For_Dispute.localized)
            return
        }
        
        if self.selectedMediaArray.isEmpty {
            self.delegate?.getDisputeReasonsBack(mssg: self.messageTextView.text ?? "", images: [], paymentHistory: self.paymentHistory)
            self.dismiss(animated: true, completion: nil)
        }else{
            uploadDocsToS3()
        }
    }
    
    //MARK:- Cancel button tapped
    @IBAction func clickCancelButton(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- click upload document button
    @IBAction func clickUploadDocumentsButton(_ sender: UIButton) {
        view.endEditing(true)
        let galleryVC = GalleryVC.instantiate(fromAppStoryboard: .AddTag)
        galleryVC.mediaDelegate = self
        self.present(galleryVC, animated: true, completion: {
            CommonFunctions.showToastMessage(LocalizedString.MaxFiveImages.localized)
        })
    }
    
    //MARK:- Remove button tapped
    @objc func removeButtobTapped(sender : UIButton){
        guard let indexPath = sender .collectionViewIndexPath(documentCollectionView) else { return }
        self.selectedMediaArray.remove(at: indexPath.item)
        self.documentCollectionView.reloadData()
    }
    
}

extension DisputePopUpVC{
    
    //MARK:- set up view
    func setUpSubView() {
        self.messageTextView.delegate = self
        self.messageTextView.round(radius: 10)
        self.messageTextView.setBorder(width: 1, color: AppColors.textfield_Border)
        self.configureCollectionView()
    }
    
    func configureCollectionView(){
        self.documentCollectionView.delegate = self
        self.documentCollectionView.dataSource = self
    }
    
}

//MARK: - Extension CollectionView's delegates
extension DisputePopUpVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedMediaArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let documentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DisputeCollectionViewCell", for: indexPath) as? DisputeCollectionViewCell
        
        let asset = self.selectedMediaArray[indexPath.row].asset
        let manager = PHImageManager.default()
        
        manager.requestImage(for: asset,
                             targetSize: CGSize(width: collectionView.frame.width/3,
                                                height: collectionView.frame.width/3),
                             contentMode: .aspectFit,
                             options: nil) {(result, _) in
                                
                                documentCell?.documentImage.image = result
                                documentCell?.outerView.layer.cornerRadius = 20
                                documentCell?.outerView.layer.borderColor = AppColors.textViewPlaceholderColor.cgColor
                                documentCell?.outerView.layer.borderWidth = 0.5
        }
        
        documentCell?.crossBtn.addTarget(self, action: #selector(removeButtobTapped), for: UIControl.Event.touchUpInside)
        
        return documentCell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func uploadDocsToS3() {
        self.images.removeAll()
        self.imageUrl.removeAll()
        
        for i in 0..<self.selectedMediaArray.count {
            let asset = self.selectedMediaArray[i].asset
            let manager = PHImageManager.default()
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.isSynchronous = true
            
            manager.requestImage(for: asset,
                                 targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight),
                                 contentMode: .aspectFit,
                                 options: options) { (result, _) in
                                    if let image = result {
                                        self.images.append(image)
                }
            }
        }
        
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        DispatchQueue.main.async {
            self.view.showIndicator()
        }
        
        for (index,item) in self.images.enumerated() {
            
            item.uploadImageToS3WithUtility(imageIndex: index, compressionRatio: 0.4,success: { (imageIndex, success, imageUrl) in
                self.imageUrl.append(imageUrl)
                if self.imageUrl.count == self.selectedMediaArray.count {
                    self.uploadDocsToServer()
                }
                self.view.hideIndicator()
            }, progress: { (imageIndex, progress) in
                
            }) { (imageIndex, errer) in
                self.view.hideIndicator()
            }
        }
    }
    
    //MARK:- Upload to server
    func uploadDocsToServer() {
        DispatchQueue.main.async {
            self.view.showIndicator()
        }
        self.delegate?.getDisputeReasonsBack(mssg: self.messageTextView.text ?? "", images: self.imageUrl, paymentHistory: self.paymentHistory)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- textview delegates
extension DisputePopUpVC : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (range.location == 0 && text == " ") || text.containsEmoji {return false}
        return true
    }

}

//MARK:- Delegates
extension DisputePopUpVC : GalleryDelegate {
    
    func passMediaObject(media: [Media]) {
        self.selectedMediaArray = media
        self.documentCollectionView.reloadData()
    }
    
}


class DisputeCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var documentImage: UIImageView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var crossBtn: UIButton!
    
    //MARK: - View's lifecycle
    override func awakeFromNib() {
        self.outerView.layer.cornerRadius = 20
        self.outerView.layer.borderColor = UIColor.lightText.cgColor
        self.outerView.layer.borderWidth = 0.5
    }
    
    @IBAction func crossBtnAction(_ sender: UIButton) {
        
    }
    
}

