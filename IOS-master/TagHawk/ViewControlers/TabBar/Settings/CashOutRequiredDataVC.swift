//
//  CashOutRequiredDataVC.swift
//  TagHawk
//
//  Created by Admin on 6/13/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Photos

struct CashOutInputDtata {
    
    var dob : String = ""
    var ssn : String = ""
    var phoneNumber : String = ""
    var countryCode = ""
    var isPhoneVerified = false
    var passport = false
    
    init(){
        
    }
}

class CashOutRequiredDataVC : BaseVC {
    
    enum CashoutRequiredCells : Int {
        case heading = 0
        case dob = 1
        case ssn = 2
        case phone = 3
        case address = 4
        case passport = 5
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var cashOutRequiredTableView: UITableView!
    @IBOutlet weak var cashoutButton: UIButton!
    
    //mARK:- Variables
    private var cashoutInputData = CashOutInputDtata()
    private var controler = CashOutRequiredDataControler()
    private let bankDetailsControler = BankDetailsControler()
    var cashoutBalance : Double = 0.0
    var merchant = MerchantData()
    private var selectedMediaArray : [Media] = []
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigation()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
        self.controler.uploadIdentityDelegate = self
        self.bankDetailsControler.cashOutDelegate = self
        self.bankDetailsControler.updateMerchantDelegate = self
    }
    
    override func leftBarButtonTapped() {
        self.popToAccountAndHistory()
    }
    
    //MARK:- IBOutlets
    @IBAction func cashoutButtonTapped(_ sender: UIButton) {
        if self.cashoutInputData.dob.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.Please_Select_Dob.localized)
        }else if self.cashoutInputData.ssn.isEmpty && !self.cashoutInputData.passport{
            CommonFunctions.showToastMessage(LocalizedString.Please_Add_Ssn_Or_Upload_Your_Passport.localized)
        }else if cashoutInputData.phoneNumber.isEmpty{
            CommonFunctions.showToastMessage(LocalizedString.Please_Add_Phone_Number.localized.localized)
        }else if UserProfile.main.city.isEmpty{
            CommonFunctions.showToastMessage(LocalizedString.Please_Enter_Address.localized.localized)
        }else{
            self.bankDetailsControler.getMerchantStatus()
        }
    }
}


extension CashOutRequiredDataVC {
    
    //mark:- Set up view
    private func setUpSubView(){
        self.cashoutInputData.ssn = UserProfile.main.ssnNumber
        self.cashoutInputData.dob = UserProfile.main.dob
        self.cashoutInputData.phoneNumber = UserProfile.main.phoneNumber
        self.cashoutInputData.countryCode = UserProfile.main.countryCode
        self.cashoutInputData.isPhoneVerified = UserProfile.main.isPhoneVerified
        configureTableView()
        self.cashoutButton.setAttributes(title: LocalizedString.Cash_Out.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
    }
    
    //MARK:- Configure tableview
    private func configureTableView(){
        self.cashOutRequiredTableView.registerNib(nibName: CashOutRequiredDataCell.defaultReuseIdentifier)
        self.cashOutRequiredTableView.registerNib(nibName: CellWithLabel.defaultReuseIdentifier)
        self.cashOutRequiredTableView.registerNib(nibName: TakePasportCell.defaultReuseIdentifier)
        self.cashOutRequiredTableView.separatorStyle = .none
        self.cashOutRequiredTableView.showsVerticalScrollIndicator = false
        self.cashOutRequiredTableView.delegate = self
        self.cashOutRequiredTableView.dataSource = self
        self.cashOutRequiredTableView.rowHeight = UITableView.automaticDimension
        self.cashOutRequiredTableView.estimatedRowHeight = 100
    }
    
    //MARK:- add or remove button
    @objc func addOrRemoveButtonTapped(sender : UIButton) {
        guard let indexPath = sender.tableViewIndexPath(cashOutRequiredTableView) else { return }
        
        switch indexPath.row {
            
        case CashoutRequiredCells.phone.rawValue:
            openPhoneAndSsnPopUp(opeFor: CustomPhoneVerifyPopUpVCInstantiatedFor.phoneNumber)
            
        case CashoutRequiredCells.ssn.rawValue:
            openPhoneAndSsnPopUp(opeFor: CustomPhoneVerifyPopUpVCInstantiatedFor.ssn)
            
        case CashoutRequiredCells.address.rawValue:
            let vc = AddAddressVC.instantiate(fromAppStoryboard: AppStoryboard.PaymentAndShipping)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        default:
            break
        }
    }
    
    func openPhoneAndSsnPopUp(opeFor : CustomPhoneVerifyPopUpVCInstantiatedFor){
        if opeFor == .phoneNumber && self.cashoutInputData.isPhoneVerified { return }
        let phoneVerifyVC = CustomPhoneVerifyPopUpVC.instantiate(fromAppStoryboard: .Home)
        var data = UserProfile()
        data.ssnNumber = self.cashoutInputData.ssn
        phoneVerifyVC.userData = data
        phoneVerifyVC.instantiated = opeFor
        phoneVerifyVC.delegate = self
        phoneVerifyVC.modalTransitionStyle = .coverVertical
        phoneVerifyVC.modalPresentationStyle = .overCurrentContext
        self.present(phoneVerifyVC, animated: true, completion: nil)
    }
    
    private func showOtpVerificationVC() {
        let otpScene = VerifyOtpPopupVC.instantiate(fromAppStoryboard: .Profile)
        otpScene.phoneNumberVerify = { [weak self] in
            guard let `self` = self else { return }
            UserProfile.main.countryCode = self.cashoutInputData.countryCode
            UserProfile.main.phoneNumber = self.cashoutInputData.phoneNumber
            UserProfile.main.isPhoneVerified = true
            self.cashoutInputData.isPhoneVerified = true
            UserProfile.main.saveToUserDefaults()
            `self`.cashOutRequiredTableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: UITableView.RowAnimation.none) }
        otpScene.modalPresentationStyle = .overCurrentContext
        self.present(otpScene, animated: true, completion: nil)
    }
    
    //MARK:- Shouw popup
    func showPopup(type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func uploadDocument(sender : UIButton){
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        if self.selectedMediaArray.count < 2{
            CommonFunctions.showToastMessage(LocalizedString.Please_Select_Both_First_And_Last.localized)
            return
        }
        
        uploadIdentity()
    }
    
    //MARK:- Upload identity
    func uploadIdentity(){
        
        var images : [UIImage] = []
        
        for i in 0..<self.selectedMediaArray.count {
            let asset = self.selectedMediaArray[i].asset
            let manager = PHImageManager.default()
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.isSynchronous = true
            
            manager.requestImage(for: asset,
                                 targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight),
                                 contentMode: .aspectFit,
                                 options: options) { (result, _) in
                                    if let image = result {
                                        images.append(image)
                                    }
            }
        }
        
        self.controler.uploadData(imgData: images)
    }
    
    //MARK:- Pop to account history
    func popToAccountAndHistory(shouldAddLoader : Bool = false){
        guard let vcs = self.navigationController?.viewControllers else { return }
        for itm in vcs {
            if itm.isKind(of: AccountInfoAndPaymentHistoryVC.self){
                if shouldAddLoader{
                    guard let accountVc = itm as? AccountInfoAndPaymentHistoryVC else { return }
                    accountVc.addLoaderFor20Seconds()
                }
                self.navigationController?.popToViewController(itm, animated: true)
            }
        }
    }
}

//MARK:- configure navigation
extension CashOutRequiredDataVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Cashout_Details.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
    
}


//MARK:- TableView Datasource and Delegate
extension CashOutRequiredDataVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case CashoutRequiredCells.heading.rawValue:
            return UITableView.automaticDimension
            
        case  CashoutRequiredCells.dob.rawValue,  CashoutRequiredCells.ssn.rawValue, CashoutRequiredCells.phone.rawValue, CashoutRequiredCells.address.rawValue:
            return 70
            
        default:
            return UITableView.automaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case CashoutRequiredCells.heading.rawValue :
            return self.getCellWithLabel(tableView, indexPath: indexPath)
            
        case CashoutRequiredCells.dob.rawValue, CashoutRequiredCells.ssn.rawValue, CashoutRequiredCells.phone.rawValue, CashoutRequiredCells.address.rawValue:
            return self.getCashOutCell(tableView, indexPath: indexPath)
            
        default:
            return self.getPassportCell(tableView, indexPath: indexPath)
        }
    }
    
    
    func getCashOutCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CashOutRequiredDataCell.defaultReuseIdentifier) as? CashOutRequiredDataCell else {
            fatalError("PaymentHistoryCell....\(CashOutRequiredDataCell.defaultReuseIdentifier) cell not found") }
        cell.dobTextField.delegate = self
        
        switch indexPath.row {
            
        case CashoutRequiredCells.dob.rawValue:
            cell.setUpFor = CashOutRequiredDataCell.CashoutRequiredCellFor.dob
            
        case CashoutRequiredCells.ssn.rawValue:
            cell.setUpFor = CashOutRequiredDataCell.CashoutRequiredCellFor.ssn
            
        case CashoutRequiredCells.phone.rawValue:
            cell.setUpFor = CashOutRequiredDataCell.CashoutRequiredCellFor.phoneNumber
            
        case CashoutRequiredCells.address.rawValue:
            cell.setUpFor = CashOutRequiredDataCell.CashoutRequiredCellFor.address
            
        default: break
            
        }
        
        cell.populateData(data: self.cashoutInputData)
        cell.addOrEditButton.addTarget(self, action: #selector(addOrRemoveButtonTapped), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    func getCellWithLabel(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellWithLabel.defaultReuseIdentifier) as? CellWithLabel else {
            fatalError("PaymentHistoryCell....\(CellWithLabel.defaultReuseIdentifier) cell not found") }
        
        cell.cellLabel.textAlignment = .center
        let firstString = LocalizedString.Funds_Safely_Transfered_To_Account.localized.attributeStringWithColors(stringToColor: LocalizedString.stripe.localized, strClr: UIColor.black, substrClr: AppColors.stripeColor, strFont: AppFonts.Galano_Regular.withSize(15), strClrFont: AppFonts.Galano_Bold.withSize(19))
        let secondString = LocalizedString.We_Wont_Store_Your_Personal_Information.localized.attributeStringWithColors(stringToColor: LocalizedString.stripe.localized, strClr: UIColor.black, substrClr: AppColors.stripeColor, strFont: AppFonts.Galano_Regular.withSize(15), strClrFont: AppFonts.Galano_Bold.withSize(19))
        cell.cellLabel.attributedText = firstString.concatinate(attributedString: secondString)
        return cell
    }
    
    func getPassportCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TakePasportCell.defaultReuseIdentifier) as? TakePasportCell else {
            fatalError("PaymentHistoryCell....\(TakePasportCell.defaultReuseIdentifier) cell not found") }
        cell.uploadDocumentButton.addTarget(self, action: #selector(uploadDocument), for: .touchUpInside)
        cell.uploadBtn.addTarget(self, action: #selector(uploadBtnTapped(_:)), for: .touchUpInside)
        cell.selectedMediaArray = self.selectedMediaArray
        cell.removeDocDelegate = self
        cell.uploadDocumentButton.isHidden = self.cashoutInputData.passport
        cell.takePassPortCollectionView.reloadData()
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func uploadBtnTapped(_ sender: UIButton){
        let galleryVC = GalleryVC.instantiate(fromAppStoryboard: .AddTag)
        galleryVC.uploadFor = .uploadPassport
        galleryVC.mediaDelegate = self
        galleryVC.selectedMediaArray = self.selectedMediaArray
        self.present(galleryVC, animated: true, completion: {
            CommonFunctions.showToastMessage(LocalizedString.MaxTwoImages.localized)
        })
    }
}

//MARK:- textfield delegates
extension CashOutRequiredDataVC : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let startDate = Date().addingTimeInterval(TimeInterval.fromNowTo(yearsAgo: 90))
        let endDate = Date().addingTimeInterval(TimeInterval.fromNowTo(yearsAgo: 13))
        
        let selectedDate = self.cashoutInputData.dob.toDate(dateFormat: Date.DateFormat.ddMMMyyyy.rawValue)
        
        DatePicker.openPicker(in: textField, currentDate: selectedDate ?? Date(), minimumDate: startDate, maximumDate: endDate, pickerMode: UIDatePicker.Mode.date) {[weak self] (date) in
            
            guard let `self` = self else { return }
            let dateString = date.toString(dateFormat: Date.DateFormat.MMddyyyy.rawValue)
            self.cashoutInputData.dob = dateString
            self.controler.updateCashoutDetails(updateFor: UpdatedProfile.dob, valueToUpdate: dateString)
        }
        
        return true
    }
}

//MARK:- Delegates
extension CashOutRequiredDataVC : CustomPhoneVerifyPopUpDelegate {
    
    func doneBtnTapped(instantaited: CustomPhoneVerifyPopUpVCInstantiatedFor,
                       updatedValue : String,
                       countryCode: String) {
        if instantaited == .ssn{
            self.controler.updateCashoutDetails(updateFor: UpdatedProfile.ssn, valueToUpdate: updatedValue)
        }else{
            self.controler.updateCashoutDetails(updateFor: UpdatedProfile.phone, valueToUpdate: updatedValue, countryCode: countryCode)
        }
    }
}

extension CashOutRequiredDataVC : UpdateCashoutDataDelegate {
    
    func willUpdateCashOutData(){
        self.view.showIndicator()
        
    }
    
    func cashOutDataUpdatedSuccessFully(updateFor : UpdatedProfile, valueToUpdate : String, countryCode : String){
        self.view.hideIndicator()
        
        switch updateFor {
            
        case .dob:
            self.cashoutInputData.dob = valueToUpdate
            self.cashOutRequiredTableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: UITableView.RowAnimation.none)
            UserProfile.main.dob = valueToUpdate
            UserProfile.main.saveToUserDefaults()
            
        case .ssn:
            self.cashoutInputData.ssn = valueToUpdate
            self.cashOutRequiredTableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: UITableView.RowAnimation.none)
            UserProfile.main.ssnNumber = valueToUpdate
            UserProfile.main.saveToUserDefaults()
            
        case .phone:
            self.cashoutInputData.phoneNumber = valueToUpdate
            self.cashoutInputData.countryCode = countryCode
            self.showOtpVerificationVC()
            
        default:
            break
        }
        
    }
    
    func failedToUpdateCashOutData(msg : String) {
        self.view.hideIndicator()
        
    }
}

extension CashOutRequiredDataVC : AddressDetailAdded {
    
    func addressDetailsAdded(){
        self.cashOutRequiredTableView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: UITableView.RowAnimation.none)
    }
    
}


extension CashOutRequiredDataVC : CustomPopUpDelegateWithType {
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        if type == .cashoutNotActivated{
            self.popToAccountAndHistory(shouldAddLoader: true)
        }
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        self.bankDetailsControler.payOut(amt: self.cashoutBalance)
    }
    
    func openPaymentVc(){
        let vc = PaymentVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CashOutRequiredDataVC : RemoveDocumentDelegate{
    
    func removeDocumentAt(index: Int) {
        self.selectedMediaArray.remove(at: index)
        self.cashOutRequiredTableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: UITableView.RowAnimation.none)
    }
}

extension CashOutRequiredDataVC : GalleryDelegate{
    
    func passMediaObject(media: [Media]){
        self.selectedMediaArray = media
        self.cashOutRequiredTableView.reloadData()
    }
    
}

extension CashOutRequiredDataVC : CashoutDelegate{
    
    func willCashOut() {
        self.view.showIndicator()
        
    }
    
    func cashoutSuccessFull(msg: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func failedToCashOut(msg: String) {
        self.view.hideIndicator()
        
    }
    
}

extension CashOutRequiredDataVC : UpdateMerchantDelegate{
    
    
    func willUpdateMerchant(){
        self.view.showIndicator()
    }
    
    func updateMerchantSuccessFully(merchant: MerchantData, shouldHandleCashoutFlow: Bool) {
        self.view.hideIndicator()
        self.merchant = merchant
        self.merchant.isPaymentEnabled ? self.showPopup(type: CustomPopUpVC.CustomPopUpFor.cashout) : self.showPopup(type: CustomPopUpVC.CustomPopUpFor.cashoutNotActivated)
    }
    
    func failedToUpdateMerchant(msg : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
    
}

extension CashOutRequiredDataVC: GetImagesBackDelegate {
    
    func getImagesBack(images : [UIImage]){
        self.cashOutRequiredTableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: UITableView.RowAnimation.none)
    }
    
}

//Webservices
extension CashOutRequiredDataVC : UploadIdentitiDelegate {
    
    func willUploadIdentiry() {
        self.view.showIndicator()
    }
    
    func uploadIdedtitySuccessFull() {
        self.view.hideIndicator()
        self.cashoutInputData.passport = true
        self.cashOutRequiredTableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func failedToUploadIdentity(msg: String) {
        self.view.hideIndicator()
        
    }
}
