//
//  loadUrlVCViewController.swift
//  TagHawk
//
//  Created by Appinventiv on 01/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import WebKit

class LoadUrlVC : BaseVC {

    enum LodeUrlType : String{
        case privacy = "1"
        case terms = "2"
        case faq = "3"
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var webView: WKWebView!
  
    var loadUrlFor = LodeUrlType.terms
    let controler = SettingsControler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.gethtmlDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }

    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- set up view
extension LoadUrlVC{
    
    //MARK:- Set up view
    func setUpSubView(){
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.viewHtmlContent(type: self.loadUrlFor)
        
    }
    
    func loadUrl(){
        
        var urlToBeLoaded : String = ""
        
        switch self.loadUrlFor {
            case .privacy:
                urlToBeLoaded = WebServices.EndPoint.privacy.rawValue
            default:
                urlToBeLoaded = WebServices.EndPoint.terms.rawValue
        }
        
        guard let url = URL(string: urlToBeLoaded) else { return }
        let urlRequest = URLRequest(url: url)
        webView.load(urlRequest)
    }
    
}


extension LoadUrlVC {
    
    func configureNavigation(){
        
        switch self.loadUrlFor {
        case .privacy:
            self.navigationItem.title = LocalizedString.Privacy_Policy.localized
            
        case .terms:
             self.navigationItem.title = LocalizedString.Terms_Of_Use.localized
    
        default:
            self.navigationItem.title = LocalizedString.FAQ.localized
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
}

//MARK:- Webservices
extension LoadUrlVC : GetHtmlContentDelegate {
   
    func willGetHtmlContent() {
        self.view.showIndicator()
    }
    
    func htmlContentReceivedSuccessFully(htmlString: String) {
        self.view.hideIndicator()
        self.webView.loadHTMLString(htmlString, baseURL: nil)
    }
    
    func failedToreceiveHtmlContent(message: String) {
      self.view.hideIndicator()
    }
    
}
