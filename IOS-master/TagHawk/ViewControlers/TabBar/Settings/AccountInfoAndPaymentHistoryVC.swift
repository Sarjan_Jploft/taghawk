//
//  AccountInfoAndPaymentHistoryVC.swift
//  TagHawk
//
//  Created by Admin on 6/4/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import MessageUI

class AccountInfoAndPaymentHistoryVC : BaseVC {

    enum CurrentlySelectedHomeTab {
        case accountInfo
        case history
    }
    
    enum AccountInfoCommingFrom  {
        case tabbar
        case feomProfile
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var accountInfoButton: UIButton!
    @IBOutlet weak var paymentHistoryButton: UIButton!
    @IBOutlet weak var segmentOuterView: UIView!
    @IBOutlet weak var segmentInnerView: UIView!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var currentTabSelectedView: UIView!
    @IBOutlet weak var currentTabSelectedViewLeading: NSLayoutConstraint!
    @IBOutlet weak var paymentScrollView: UIScrollView! {
        didSet {
            self.paymentScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    @IBOutlet weak var indicatorBackView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var dataProcessingLabel: UILabel!
    
    
    //MARK:- Variables
    var accountInfoVc : PaymentVC!
    var paymentHistoryVc : PaymentHistoryVC!
    var selectedTab = CurrentlySelectedHomeTab.accountInfo
    var commingFrom = AccountInfoCommingFrom.tabbar
    var timer : Timer?
    var timerScconds = 0
    
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
        if self.commingFrom == .tabbar {
            self.accountInfoVc.isRefreshing = true
            self.accountInfoVc.controler.getBalance()
            self.paymentHistoryVc.refresh()
        }
    }
    
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBAction
    @IBAction func accountInfoButtonTapped(_ sender: UIButton) {
        self.paymentScrollView.setContentOffset(CGPoint.zero, animated: true)
        self.selectAccountInfoTab()
    }
    
    @IBAction func paymentHistoryButtonTapped(_ sender: UIButton) {
        self.paymentScrollView.setContentOffset(CGPoint(x: screenWidth, y: 0), animated: true)
        self.selectPaymentHistoryTab()
    }
    
    func addLoaderFor20Seconds(){
        self.showIndicator()
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
    }
    
    @objc func fireTimer(){
        self.timerScconds += 1
        if self.timerScconds >= 20{
            printDebug("timerScconds...\(timerScconds)")
            self.timer?.invalidate()
            self.timerScconds = 0
            self.hideIndicator()
        }
    }
    
    func showIndicator(){
        self.indicator.startAnimating()
        self.indicatorBackView.isHidden = false
    }
    
    func hideIndicator(){
        self.indicator.stopAnimating()
        self.indicatorBackView.isHidden = true
    }
}


extension AccountInfoAndPaymentHistoryVC {

    //MARK:- set up view
    func setUpSubView(){
        self.segmentOuterView.round()
        self.segmentOuterView.backgroundColor = AppColors.selectedSegmentColor
        self.shadowView.addShadow()
        self.shadowView.round()
        self.shadowView.clipsToBounds = false
        self.accountInfoButton.round()
        self.paymentHistoryButton.round()
        self.accountInfoButton.setAttributes(title: LocalizedString.Account_Info.localized, font: AppFonts.Galano_Medium.withSize(14), titleColor: UIColor.white)
        self.paymentHistoryButton.setAttributes(title: LocalizedString.Payment_History.localized, font: AppFonts.Galano_Medium.withSize(14), titleColor: UIColor.white)
//        self.currentTabSelectedView.backgroundColor = AppColors.selectedSegmentColor
        self.currentTabSelectedView.backgroundColor = AppColors.whiteColor
        self.currentTabSelectedView.round()
        self.paymentScrollView.contentSize = CGSize(width: screenWidth * 2, height: self.paymentScrollView.frame.height - 64)
        self.paymentHistoryButton.backgroundColor = UIColor.clear
        self.paymentScrollView.bounces = false
        self.paymentScrollView.delegate = self
        self.paymentScrollView.isPagingEnabled = true
        self.paymentScrollView.showsHorizontalScrollIndicator = false
        self.addAccountInfoVc()
        self.addPaymentHistoryVc()
        self.setinitialtabSelection()
        self.dataProcessingLabel.setAttributes(text: LocalizedString.Data_Processing.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: AppColors.appBlueColor, backgroundColor: UIColor.clear)
        self.hideIndicator()
        self.indicator.hidesWhenStopped = true
    }

    func setinitialtabSelection(){
        if self.selectedTab == .history {
            self.paymentScrollView.setContentOffset(CGPoint(x: screenWidth, y: 0), animated: true)
            self.selectPaymentHistoryTab()
        }else{
            self.selectAccountInfoTab()
        }
    }
    
    func selectAccountInfoTab(){
        self.accountInfoButton.setTitleColor(UIColor.black)
        self.paymentHistoryButton.setTitleColor(AppColors.whiteColor)
        self.selectedTab = .accountInfo
//        self.currentTabSelectedView.backgroundColor = AppColors.selectedSegmentColor
        self.currentTabSelectedView.backgroundColor = AppColors.whiteColor
    }
    
    func selectPaymentHistoryTab() {
        self.paymentHistoryButton.setTitleColor(UIColor.black)
        self.accountInfoButton.setTitleColor(AppColors.whiteColor)
        self.selectedTab = .history
//        self.currentTabSelectedView.backgroundColor = AppColors.segmentTextColor
        self.currentTabSelectedView.backgroundColor = AppColors.whiteColor
        
    }
    
    //add followers vc
    //====================
    func addAccountInfoVc(){
        self.accountInfoVc = PaymentVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
//        self.accountInfoVc.delegate = self
        self.paymentScrollView.frame = self.accountInfoVc.view.frame
        self.paymentScrollView.addSubview(self.accountInfoVc.view)
        self.accountInfoVc.willMove(toParent: self)
        self.addChild(self.accountInfoVc)
        self.accountInfoVc.view.frame.size.height = self.paymentScrollView.frame.height
        self.accountInfoVc.view.frame.origin = CGPoint.zero
    }
    
    //MARK:- add following vc
    //=====================
    func addPaymentHistoryVc(){
        self.paymentHistoryVc =  PaymentHistoryVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        self.paymentScrollView.frame = self.paymentHistoryVc.view.frame
        self.paymentScrollView.addSubview(self.paymentHistoryVc.view)
        self.paymentHistoryVc.willMove(toParent: self)
        self.addChild(self.paymentHistoryVc)
        self.paymentHistoryVc.view.frame.size.height = self.paymentScrollView.frame.height
        self.paymentHistoryVc.view.frame.origin = CGPoint(x: screenWidth, y: 0)
    }
    
}

extension AccountInfoAndPaymentHistoryVC {

    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Payment_Details.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        AppNavigator.shared.parentNavigationControler.setNavigationBarHidden(self.commingFrom == .tabbar, animated: false)
        if commingFrom != .tabbar{ self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self) }
    }
    
}

extension AccountInfoAndPaymentHistoryVC {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.currentTabSelectedViewLeading.constant = (scrollView.contentOffset.x / 2) * self.segmentInnerView.width / screenWidth
        scrollView.contentOffset.x == 0.0 ? self.selectAccountInfoTab() : self.selectPaymentHistoryTab()
    }
        
}


