//
//  PaymentHistoryVC.swift
//  TagHawk
//
//  Created by Admin on 6/4/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class PaymentHistoryVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var paymentHistoryTableView: UITableView!
    
    //MARK:- Variables
    private let controler = PaymentHistoryControler()
    private var paymentHistory : [PaymentHistory] = []
    private let refreshControl = UIRefreshControl()
    private var isRefreshed = false
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
        self.controler.getPaymentHistory()
    }
    
    override func bindControler() {
        self.controler.historyDelegate = self
        self.controler.releasePaymentDelegate = self
        self.controler.refundDelegate = self
        self.controler.acceptRefundDelegate = self
        self.controler.declineRefundDelegate = self
        self.controler.releaseRefundDelegate = self
        self.controler.desputeDelegate = self
        self.controler.getHistoryStatus = self
    }
}


//MARK:- Private functions
extension PaymentHistoryVC {
    
    //MARK:- Set up view
    private func setUpSubView(){
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.paymentHistoryTableView.addSubview(refreshControl)
        self.paymentHistoryTableView.alwaysBounceVertical = true
        configureTableView()
    }
    
    //MARK:- Configure table view
    private func configureTableView(){
        self.paymentHistoryTableView.registerNib(nibName: PaymentHistoryCell.defaultReuseIdentifier)
        self.paymentHistoryTableView.separatorStyle = .none
        self.paymentHistoryTableView.showsVerticalScrollIndicator = false
        self.paymentHistoryTableView.delegate = self
        self.paymentHistoryTableView.dataSource = self
        self.setEmptyDataView(heading: LocalizedString.No_Payment_History_Yet.localized)
        self.setDataSourceAndDelegate(forScrollView: self.paymentHistoryTableView)
    }
    
    @objc func refresh(){
        self.isRefreshed = true
        self.controler.getPaymentHistory()
    }
    
    //MARK:- show popup
    func showPopUp(type: CustomPopUpVC.CustomPopUpFor, history : PaymentHistory) {
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        vc.paymentHistory = history
        vc.modalPresentationStyle = .overCurrentContext
        AppNavigator.shared.parentNavigationControler.present(vc, animated: true, completion: nil)
    }
    
    func showDeclinePopUp(historyObj : PaymentHistory){
        let vc = GetReasonVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        vc.delegate = self
        vc.historyObj = historyObj
        vc.modalPresentationStyle = .overCurrentContext
        AppNavigator.shared.parentNavigationControler.present(vc, animated: true, completion: nil)
    }
    
    @objc func leftBtnTapped(sender : UIButton){
        guard let indexPath = sender.tableViewIndexPath(self.paymentHistoryTableView) else { return }
        let orderObj = self.paymentHistory[indexPath.row]
        
        switch orderObj.deliveryStatus {
        case .pending:
            self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.refundRequest, history: orderObj)
            
        case .requestForRefund, .refundAccepted:
            //            self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.declineRefundRequest, history: orderObj)
            self.showDeclinePopUp(historyObj: orderObj)
        case .declined:
            if orderObj.sellerId != UserProfile.main.userId {
                self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.cancelRefund, history: orderObj)
            }
            
        case .disputeStarted:
            if orderObj.sellerId == UserProfile.main.userId {
                //                submit proof
                //                self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.despute, history: orderObj)
                let documentVC = DisputePopUpVC.instantiate(fromAppStoryboard: .Settings)
                documentVC.serviceReturnSuccess = { }
                documentVC.delegate = self
                documentVC.paymentHistory = orderObj
                documentVC.modalPresentationStyle = .overCurrentContext
                AppNavigator.shared.parentNavigationControler.present(documentVC, animated: true, completion: nil)
            }
            
        default:
            break
        }
        
    }
    
    @objc func rightBtnTapped(sender : UIButton){
        guard let indexPath = sender.tableViewIndexPath(self.paymentHistoryTableView) else { return }
        let orderObj = self.paymentHistory[indexPath.row]
        
        switch orderObj.deliveryStatus {
        case .pending:
            self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.releasePayment, history: orderObj)
            
        case .requestForRefund:
            self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.acceptRefundRequest, history: orderObj)
            
        case .refundAccepted:
            self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.releaseRefund, history: orderObj)
            
        case .declined:
            if orderObj.sellerId != UserProfile.main.userId {
                //despute
                
                let documentVC = DisputePopUpVC.instantiate(fromAppStoryboard: .Settings)
                documentVC.serviceReturnSuccess = { }
                documentVC.delegate = self
                documentVC.paymentHistory = orderObj
                documentVC.modalPresentationStyle = .overCurrentContext
                AppNavigator.shared.parentNavigationControler.present(documentVC, animated: true, completion: nil)
                
                //                self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.despute, history: orderObj)
            }
            
        case .disputeStarted:
            if orderObj.sellerId == UserProfile.main.userId {
                self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.releaseRefund, history: orderObj)
            }
            
        default:
            break
        }
    }
    
    @objc func cancelRequestButtonTapped(sender : UIButton) {
        guard let indexPath = sender.tableViewIndexPath(self.paymentHistoryTableView) else { return }
        let orderObj = self.paymentHistory[indexPath.row]
        
        switch orderObj.deliveryStatus {
            
        case .requestForRefund:
            if orderObj.sellerId != UserProfile.main.userId {
                self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.cancelRefund, history: orderObj)
            }
            
        case .sellarStatementDone:
            
            orderObj.sellerId == UserProfile.main.userId ? self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.releaseRefund, history: orderObj) : self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.cancelRefund, history: orderObj)
        
        default:
            break
        }
    }
    
    //MARK:- chat button tapped
    @objc func chatButtonTapped(sender : UIButton){
        
        guard let indexPath = sender.tableViewIndexPath(self.paymentHistoryTableView) else { return }
        let orderObj = self.paymentHistory[indexPath.row]
        let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
        var product = Product()
        product.images = orderObj.images
        product.userId = orderObj.sellerId == UserProfile.main.userId ? orderObj.userId : orderObj.sellerId
        product.productId = orderObj.productId
        product.title = orderObj.title
        product.firmPrice = orderObj.price
        product.productStatus = ProductStatus.getStatus(deliveryStatus: orderObj.deliveryStatus)
        
        scene.productDetail = product
        scene.selectedProductID = product.productId
        scene.vcInstantiated = .productDetail
        AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
    }
    
    func refreshProductStatus(entityId: String){
        self.controler.getProductPaymentHistory(id: entityId)
    }
}


//MARK:- TableView Datasource and Delegate
extension PaymentHistoryVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.paymentHistory.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.getHeightForRow(tableView, heightForRowAt: indexPath)
    }
    
    func getHeightForRow(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let orderObj = self.paymentHistory[indexPath.row]
        
        switch orderObj.deliveryStatus {
        case .pending, .declined:
            // Temp code
//            return orderObj.sellerId == UserProfile.main.userId ? 110 : 150
            return orderObj.sellerId == UserProfile.main.userId ? 110 : 110
            
        case .completed,.requestSuccess, .disputeCanStart:
            return 110
            
        case .requestForRefund, .sellarStatementDone:
            return 110//150
            
        case .refundAccepted, .disputeStarted:
//            return orderObj.sellerId == UserProfile.main.userId ? 150 : 110
            return orderObj.sellerId == UserProfile.main.userId ? 110 : 110
            
        default:
            return 110//150
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentHistoryCell.defaultReuseIdentifier) as? PaymentHistoryCell else {
            fatalError("PaymentHistoryCell....\(PaymentHistoryCell.defaultReuseIdentifier) cell not found") }
        cell.populateData(history: self.paymentHistory[indexPath.row])
        cell.leftButton.addTarget(self, action: #selector(leftBtnTapped), for: UIControl.Event.touchUpInside)
        cell.rightButton.addTarget(self, action: #selector(rightBtnTapped), for: UIControl.Event.touchUpInside)
        cell.cancelRefundButton.addTarget(self, action: #selector(cancelRequestButtonTapped), for: UIControl.Event.touchUpInside)
        cell.chatButton.addTarget(self, action: #selector(chatButtonTapped), for: UIControl.Event.touchUpInside)
        return cell
    }
}

//MARK:- Delegates and callbacks
extension PaymentHistoryVC : CustomPopUpDelegateWithType{
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type : CustomPopUpVC.CustomPopUpFor, orderObj : PaymentHistory){
        
        switch type {
            
        case .releasePayment, .cancelRefund:
            if orderObj.sellerId != UserProfile.main.userId{
                self.controler.releasePayment(orderId: orderObj._id)
            }
            
        case .refundRequest:
            if orderObj.sellerId != UserProfile.main.userId {
                self.controler.requestRefund(orderId: orderObj._id)
            }
            
        case .declineRefundRequest:
            if orderObj.sellerId == UserProfile.main.userId {
                // self.controler.declineRefund(orderId: orderObj._id, msg: <#String#>)
            }
            
        case .acceptRefundRequest:
            if orderObj.sellerId == UserProfile.main.userId {
                self.controler.acceptRefund(orderId: orderObj._id)
            }
            
        case .releaseRefund:
            if orderObj.sellerId == UserProfile.main.userId {
                self.controler.releaseRefund(orderId: orderObj._id)
            }
            
            //        case .despute:
            // if orderObj.sellerId != UserProfile.main.userId {
            //                self.controler.makeDispute(orderId: orderObj._id)
            //  }
            
        default:
            break
        }
    }
}

//MARK:- Webservices
extension PaymentHistoryVC : PaymentHistoryDelegate {
    
    func willRequestPaymentHistory() {
        if !isRefreshed {
            self.view.showIndicator()
        }
    }
    
    func paymentHistoryReceivedSuccessFully(history: [PaymentHistory]) {
        self.view.hideIndicator()
        self.paymentHistory = history
        self.paymentHistoryTableView.reloadData()
        self.refreshControl.endRefreshing()
        self.isRefreshed = false
        self.shouldDisplayEmptyDataView = true
        self.paymentHistoryTableView.reloadEmptyDataSet()
    }
    
    func failedToReceivePaymentHistory(msg: String) {
        self.view.hideIndicator()
        self.refreshControl.endRefreshing()
        self.isRefreshed = false
    }
}

extension PaymentHistoryVC : ReleasepaymentDelegate {
    
    func willReleasePayment() {
        self.view.showIndicator()
        
    }
    
    func paymentReleasedSuccessFully(historyObj: PaymentHistory) {
        self.view.hideIndicator()
        if let index =  self.paymentHistory.firstIndex(where: { (obj) -> Bool in
            return obj._id == historyObj._id
        }){
            self.paymentHistory[index] = historyObj
            self.paymentHistoryTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
    func failedToReleasePayment(msg: String) {
        self.view.hideIndicator()
    }
    
}


extension PaymentHistoryVC : RequestRefundDelegate {
    
    func willRequestRefund() {
        self.view.showIndicator()
        
    }
    
    func refundreceivedSuccessfully(historyObj: PaymentHistory) {
        self.view.hideIndicator()
        if let index =  self.paymentHistory.firstIndex(where: { (obj) -> Bool in
            return obj._id == historyObj._id
        }){
            self.paymentHistory[index] = historyObj
            self.paymentHistoryTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
    func failedToReceiveRefund(msg: String) {
        self.view.hideIndicator()
        
    }
    
}

extension PaymentHistoryVC  : AcceptRefundDelegate{
    
    func willAcceptRefundDelegate() {
        self.view.showIndicator()
        
    }
    
    func refundAcceptedSuccessfully(historyObj: PaymentHistory) {
        self.view.hideIndicator()
        if let index =  self.paymentHistory.firstIndex(where: { (obj) -> Bool in
            return obj._id == historyObj._id
        }){
            self.paymentHistory[index] = historyObj
            self.paymentHistoryTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
    func failedToAcceptRefund(msg: String) {
        self.view.hideIndicator()
        
    }
    
}

extension PaymentHistoryVC : DeclineRefundDelegate {
    func willDeclineRefund() {
        self.view.showIndicator()
        
    }
    
    func declineSuccessFully(historyObj: PaymentHistory) {
        self.view.hideIndicator()
        if let index =  self.paymentHistory.firstIndex(where: { (obj) -> Bool in
            return obj._id == historyObj._id
        }){
            self.paymentHistory[index] = historyObj
            self.paymentHistoryTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
    func failedToDecline(msg: String) {
        self.view.hideIndicator()
        
    }
    
}


extension PaymentHistoryVC : ReleaseRefundDelegate{
    
    func willReleaseRefund() {
        self.view.showIndicator()
        
    }
    
    func refundReleasedSuccessfully(historyObj: PaymentHistory) {
        self.view.hideIndicator()
        if let index =  self.paymentHistory.firstIndex(where: { (obj) -> Bool in
            return obj._id == historyObj._id
        }){
            self.paymentHistory[index] = historyObj
            self.paymentHistoryTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
    func failedToReleaseRefund(msg: String) {
        self.view.hideIndicator()
        
    }
}

extension PaymentHistoryVC : MakeDesputeProtocol{
    
    func willMmakeDespute(){
        self.view.showIndicator()
    }
    
    func desputeMadeSuccessfully(historyObj : PaymentHistory){
        self.view.hideIndicator()
        if let index =  self.paymentHistory.firstIndex(where: { (obj) -> Bool in
            return obj._id == historyObj._id
        }){
            self.paymentHistory[index] = historyObj
            self.paymentHistoryTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
    func failedToMakeDespute(msg : String){
        self.view.hideIndicator()
        
    }
}


extension PaymentHistoryVC : GetReasonToDeclineBackDelegate{
    
    func getReasonBack(msg : String, history : PaymentHistory){
        if history.sellerId == UserProfile.main.userId {
            self.controler.declineRefund(orderId: history._id, msg: msg)
        }
    }
}

extension PaymentHistoryVC : GetDisputeReasons {
    
    func getDisputeReasonsBack(mssg : String, images: [String], paymentHistory : PaymentHistory){
        printDebug(images)
        self.controler.makeDispute(orderId: paymentHistory._id, msg: mssg, images: images)
    }
    
}


extension PaymentHistoryVC : GetPaymentHistoryStatusDelegate {
    
    func willGetPaymentHistoryStatus(){
        
    }
    
    func paymentHistoryStatusSuccessfully(history : PaymentHistory){
        
        if let ind = self.paymentHistory.firstIndex(where: { (obj) -> Bool in
            return obj._id == history._id
        }){
            self.paymentHistory[ind] = history
            self.paymentHistoryTableView.reloadRows(at: [IndexPath(row: ind, section: 0)], with: UITableView.RowAnimation.none)
        }
        
    }
    
    func failedToReceivePaymentHistoryStatus(msg : String){
        CommonFunctions.showToastMessage(msg)
    }
    
}
