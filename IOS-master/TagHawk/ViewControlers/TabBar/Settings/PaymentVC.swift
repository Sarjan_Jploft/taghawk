//
//  PaymentVC.swift
//  TagHawk
//
//  Created by Appinventiv on 02/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Crashlytics
import MessageUI

class PaymentVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var depositAccountLabel: UILabel!
    @IBOutlet weak var paymentMethodsLabel: UILabel!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var depositAccountSepratorView: UIView!
    @IBOutlet weak var paymentMethodsSepratorView: UIView!
    @IBOutlet weak var paymentTableView: UITableView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var avilableBalanceLabel: UILabel!
    @IBOutlet weak var cashOutButton: UIButton!
    @IBOutlet weak var pendingAmmountLabel: UILabel!
    @IBOutlet weak var avilableNow: UILabel!
    @IBOutlet weak var avilableSoon: UILabel!
    @IBOutlet weak var pendingAmmount: UILabel!
    @IBOutlet weak var pendingAmmountDescription: UILabel!
    @IBOutlet weak var outerHeaderView: UIView!
    @IBOutlet weak var innerHeaderView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var avilableNowDescriptionLabel: UILabel!
    
    //MARK:- Variables
    let controler = BankDetailsControler()
    var cards : [Card] = []
    var addDetailsFrom = AddDepositAccountVC.AddBankDetailsWhile.adding
    let homeControler = HomeControler()
    private let settingsController = SettingsControler()
    weak var delegate : BankDetailsAdded?
    private let refreshControl = UIRefreshControl()
    var isRefreshing = false
    var merchant = MerchantData()
    var cashoutBalance : Double = 0.0
    
    
    //MARK:- View lifr cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.cardDelegate = self
        self.controler.deleteCardDelegate = self
        self.homeControler.createMerchangtDelegate = self
        self.settingsController.logoutDelegate = self
        self.controler.cashOutDelegate = self
        self.controler.getDetailsDelegate = self
        self.controler.getBalanceDelegate = self
        self.controler.updateMerchantDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBAction
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        if self.cashoutBalance <= 0.0{
            CommonFunctions.showToastMessage(LocalizedString.You_Have_Insufficient.localized)
            return
        }
        
        if UserProfile.main.stripeMerchantId.isEmpty{
            self.homeControler.createMerchant(from: AddDepositAccountVC.AddBankDetailsWhile.adding)
        }else{
            self.navigateToDepositAccount()
        }
    }
    
    //MARK:- Cashout button tapped
    @IBAction func cashOutButton(_ sender: UIButton) {
        if self.cashoutBalance <= 0.0{
            CommonFunctions.showToastMessage(LocalizedString.You_Have_Insufficient.localized)
            return }
        UserProfile.main.stripeMerchantId.isEmpty ? self.homeControler.createMerchant(from: AddDepositAccountVC.AddBankDetailsWhile.cashout) : self.controler.getMerchantStatus(shouldHandleCashoutFlow: true)
    }
    
    func handleCashoutFlow(){
        if self.merchant.last4.isEmpty{
            self.navigateToDepositAccount(depositFor: AddDepositAccountVC.AddBankDetailsWhile.cashout)
        }else if !UserProfile.main.dob.isEmpty && (!UserProfile.main.ssnNumber.isEmpty || UserProfile.main.passport) && !UserProfile.main.phoneNumber.isEmpty{
            self.merchant.isPaymentEnabled ? self.showPopup(type: CustomPopUpVC.CustomPopUpFor.cashout) : handleAccountVerifiedFlow()
        } else{
            let vc = CashOutRequiredDataVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
            vc.cashoutBalance = self.cashoutBalance
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func handleAccountVerifiedFlow(){
        if !self.merchant.isAddressVerified{
            self.showPopup(type: CustomPopUpVC.CustomPopUpFor.cashoutNotActivatedDueToInvalidAddress)
        } else if self.merchant.ssnAndPassPortStatus == .pending{
            self.showPopup(type: CustomPopUpVC.CustomPopUpFor.cashoutNotActivated)
        }else if  !self.merchant.isSsnVerified || !self.merchant.isPassportVerified || !(self.merchant.ssnAndPassPortStatus == .verified) {
            self.showPopup(type: CustomPopUpVC.CustomPopUpFor.cashoutNotActivatedDueToInvalidInfo)
        }else{
            self.showPopup(type: CustomPopUpVC.CustomPopUpFor.cashoutNotActivated)
        }
    }
    
    func showPopup(type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.merchant = self.merchant
        vc.delegateWithType = self
        vc.modalPresentationStyle = .overCurrentContext
        AppNavigator.shared.parentNavigationControler.present(vc, animated: true, completion: nil)
    }
    
    @objc func refresh(){
        self.isRefreshing = true
        self.controler.getBalance()
        self.controler.getCatds()
    }
}

//MARK:- Private fucctions
private extension PaymentVC {
    //MARK:- Set up view
    func setUpSubView(){
        self.depositAccountLabel.setAttributes(text: LocalizedString.DEPOSIT_ACCOUNT.localized, font: AppFonts.Galano_Regular.withSize(15) , textColor: AppColors.lightGreyTextColor)
        self.paymentMethodsLabel.setAttributes(text: LocalizedString.PAYMENT_METHODS.localized, font: AppFonts.Galano_Regular.withSize(15) , textColor: AppColors.lightGreyTextColor)
        self.accountNumberLabel.setAttributes(text: LocalizedString.Account_Number.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: UIColor.black)
        self.paymentMethodsSepratorView.backgroundColor = AppColors.textfield_Border
        self.avilableBalanceLabel.setAttributes(text: LocalizedString.Account_Balance.localized, font: AppFonts.Galano_Regular.withSize(15) , textColor: AppColors.lightGreyTextColor)
        self.cashOutButton.setAttributes(title: LocalizedString.Cash_Out.localized, font: AppFonts.Galano_Medium.withSize(15), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
        self.avilableNow.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(16), textColor: AppColors.appBlueColor)
        self.avilableSoon.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(16), textColor: UIColor.black)
        self.pendingAmmountLabel.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(15), textColor: AppColors.lightGreyTextColor)
        self.descriptionLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(12), textColor: AppColors.lightGreyTextColor)
        self.descriptionLabel.text = LocalizedString.You_Can_Track_Orde.localized
        self.avilableNowDescriptionLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(14), textColor: AppColors.appBlueColor)
        self.avilableNowDescriptionLabel.text = LocalizedString.will_Receive_The_Money_Within_2_7_Days.localized
        self.cashOutButton.round(radius: 5)
        self.cardNumberLabel.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(cardNumberTapped))
        self.cardNumberLabel.addGestureRecognizer(tap)
        self.configureTableView()
        self.populateCardNumber()
        self.paymentTableView.isHidden = true
        self.paymentMethodsLabel.isHidden = true
        self.paymentMethodsSepratorView.isHidden = true
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.paymentTableView.addSubview(refreshControl)
        self.pendingAmmountLabel.text = LocalizedString.Awating_Buyyers_Confirmation.localized
        
        self.controler.getBalance()
        self.controler.getCatds()
        self.outerHeaderView.frame.size.height = self.innerHeaderView.frame.height
    }
    
    func populateCardNumber(){
        //        self.cardNumberLabel.text = "xxxxxx\(UserProfile.main.bankDetails.accountNumber.suffix(4))"
        //        self.cardNumberLabel.isHidden = UserProfile.main.bankDetails.accountNumber.isEmpty
        //        self.addButton.isHidden = !UserProfile.main.bankDetails.accountNumber.isEmpty
        //        self.amountLabel.text = "$\(UserProfile.main.balance)"
    }
    
    func configureTableView(){
        self.paymentTableView.separatorStyle = .none
        self.paymentTableView.registerNib(nibName: CardCell.defaultReuseIdentifier)
        self.paymentTableView.delegate = self
        self.paymentTableView.dataSource = self
    }
    
    @objc func cardNumberTapped(sender : UITapGestureRecognizer){
        if UserProfile.main.stripeMerchantId.isEmpty{
            self.homeControler.createMerchant(from: AddDepositAccountVC.AddBankDetailsWhile.adding)
        }else{
            self.navigateToDepositAccount()
        }
    }
    
    func navigateToDepositAccount(depositFor : AddDepositAccountVC.AddBankDetailsWhile = .adding){
        let vc = WhereToDepositVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        vc.delegate = self
        vc.addDetailsWhile = depositFor
        vc.cashOutBalance = self.cashoutBalance
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension PaymentVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Payment.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
    
}

//MARK:- Table View datasource and delegates
extension PaymentVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CardCell.defaultReuseIdentifier) as? CardCell else {
            fatalError("FilterVC....\(CardCell.defaultReuseIdentifier) cell not found")
        }
        cell.populateData(card: self.cards[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            if !AppNetworking.isConnectedToInternet {
                CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                return
            }
            self.controler.deleteCard(card: self.cards[indexPath.row])
        }else{
            
        }
    }
}

//MARK:- Get cards delegate
extension PaymentVC : GetCardsDelegate{
    
    func willgetCards(){
        if !self.isRefreshing{
            self.view.showIndicator()
        }
    }
    
    func cardsReceivedSuccessFully(cards : [Card]){
        self.view.hideIndicator()
        self.cards = cards
        self.paymentMethodsLabel.isHidden = self.cards.isEmpty
        self.paymentMethodsSepratorView.isHidden = self.cards.isEmpty
        self.paymentTableView.reloadData()
    }
    
    func failedToReceiveCards(message : String){
        self.view.hideIndicator()
        self.refreshControl.endRefreshing()
    }
}

//MARK:- Delagets
extension PaymentVC : DeleteCardDelegate {
    
    func willDeleteCard(){
        self.view.showIndicator()
        
    }
    
    func deleteCardSuccessFully(card : Card){
        self.view.hideIndicator()
        self.cards = self.cards.filter { $0.id != card.id }
        self.paymentMethodsLabel.isHidden = self.cards.isEmpty
        self.paymentMethodsSepratorView.isHidden = self.cards.isEmpty
        self.paymentTableView.reloadData()
    }
    
    func failedToDeleteCard(message : String){
        self.view.hideIndicator()
    }
}

extension PaymentVC : BankDetailsAdded {
    
    func bankDetailsAdded() {
        self.populateCardNumber()
    }
    
}

extension PaymentVC : CustomPopUpDelegateWithType {
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        switch type {
        case .enterBankandProfileDetails:
            let vc = EditProfileVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.userData = UserProfile.main
            self.present(vc, animated: true, completion: nil)
            
        case .enterBankDetails:
            let vc = AddDepositAccountVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
            //            vc.addDetailsFrom = .tabbar
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .cashoutNotActivatedDueToInvalidAddress:
            guard let tabBar = AppNavigator.shared.tabBar else { return }
            tabBar.selectTab(index: 4)
            
        case .cashoutNotActivatedDueToInvalidInfo:
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                AppNavigator.shared.parentNavigationControler.present(mailComposeViewController, animated: true, completion: nil)
            }
            
        default:
            break
        }
        
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        if type == .loginSignup {
            settingsController.logout()
            
        }
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        if type == .loginSignup {
            settingsController.logout()
        } else if type == .cashout{
            self.controler.payOut(amt: self.cashoutBalance)
        }
        
    }
    
    func openPaymentVc(){
        let vc = PaymentVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Webservices
extension PaymentVC : CreateMerchantDelegate {
    
    func willrequestCreteMerchant(){
        self.view.showIndicator()
    }
    
    func merchantCreatedSuccessFully(customerId: String, from: AddDepositAccountVC.AddBankDetailsWhile) {
        self.view.hideIndicator()
        //        self.controler.cashOut()
        from == .cashout ? self.handleCashoutFlow() : self.navigateToDepositAccount()
    }
    
    func failedToCreateMerchant(message : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
}

extension PaymentVC: LogoutDelegate {
    
    func willLogout() {
        view.showIndicator()
    }
    
    func logoutSuccessFully() {
        view.hideIndicator()
        AppNavigator.shared.actionsOnLogout(comingFromGuest: true)
    }
    
    func failedToLogout(message: String) {
        view.hideIndicator()
    }
}


extension PaymentVC : CashoutDelegate {
    
    func willCashOut() {
        self.view.showIndicator()
    }
    
    func cashoutSuccessFull(msg : String) {
        self.view.hideIndicator()
        self.controler.getBalance()
        CommonFunctions.showToastMessage(msg)
    }
    
    func failedToCashOut(msg : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
}

extension PaymentVC : GetBankDetailsProtocol{
    
    func willGetBankDetail() {
        self.view.showIndicator()
    }
    
    func getBankDetailsSuccessFully(details: BankDetail) {
        self.view.hideIndicator()
        self.controler.getBalance()
        //        self.amountLabel.text = "$\(details.balance)"
    }
    
    func failedTOGetBankDetails(message: String) {
        self.view.hideIndicator()
    }
    
}

extension PaymentVC : GetBalanceDelegate {
    
    func willGetBalance() {
        if !self.isRefreshing{
            self.view.showIndicator()
        }
    }
    
    func getBalanceSuccessFully(cashoutBalance : Double,avilableSoonBalance : Double ,pendingBalance : Double) {
        if !self.isRefreshing{
            self.view.hideIndicator()
        }
        self.cashoutBalance = cashoutBalance
        self.avilableNow.text = "\(LocalizedString.Avilable_Now.localized): $\(cashoutBalance.rounded(toPlaces: 2).removeTrailingZero())"
        self.avilableSoon.text = "\(LocalizedString.Avilable_Soon.localized): $\(avilableSoonBalance.rounded(toPlaces: 2).removeTrailingZero())"
        self.pendingAmmountLabel.text = "\(LocalizedString.Awating_Buyyers_Confirmation.localized): $\(pendingBalance.rounded(toPlaces: 2).removeTrailingZero())"
        //  if !self.isRefreshing{
        self.controler.getMerchantStatus()
        //   }
        self.refreshControl.endRefreshing()
        self.paymentTableView.isHidden = false
        //    self.timer?.invalidate()
        //        self.timerScconds = 0
    }
    
    func failedToGetBalance(msg: String) {
        self.view.hideIndicator()
        self.refreshControl.endRefreshing()
    }
}


extension PaymentVC : UpdateMerchantDelegate{
    
    func willUpdateMerchant(){
        if !self.isRefreshing{
            self.view.showIndicator()
        }
    }
    
    func updateMerchantSuccessFully(merchant: MerchantData, shouldHandleCashoutFlow: Bool) {
        if !self.isRefreshing{ self.view.hideIndicator() }
        self.merchant = merchant
        self.cardNumberLabel.text = "xxxxxx\(merchant.last4)"
        self.cardNumberLabel.isHidden = merchant.last4.isEmpty
        self.addButton.isHidden = !merchant.last4.isEmpty
        self.paymentTableView.isHidden = false
        isRefreshing = false
        if shouldHandleCashoutFlow { self.handleCashoutFlow() }
    }
    
    func failedToUpdateMerchant(msg : String){
        self.view.hideIndicator()
        //        CommonFunctions.showToastMessage(msg)
    }
    
}

extension PaymentVC : MFMailComposeViewControllerDelegate {
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients(["support@taghawk.app"])
        return mailComposerVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
