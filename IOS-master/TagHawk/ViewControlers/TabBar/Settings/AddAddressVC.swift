//
//  AddAddressVC.swift
//  TagHawk
//
//  Created by Admin on 8/9/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol  AddressDetailAdded : class {
    func addressDetailsAdded()
}

class AddAddressVC : BaseVC {

    struct AddressInput {
        var addressLine1 : String = ""
        var addressLine2 : String = ""
        var zipCode : String = ""
        var city : String = ""
        var state : String = ""
        var stateCode : String = ""
        var country = ""
        
        init(){
            
        }
    }
    
    private enum TableCells: Int {
        case addressLine1 = 0
        case addressLine2
        case zipcode
        case city
        case state
        
        func stringValue() -> String {
            switch self {
            case .addressLine1:
                return LocalizedString.Address_Line_1.localized + "*"
            case .addressLine2:
                return LocalizedString.Address_Line_2.localized
            case .city:
                return LocalizedString.city.localized + "*"
            case .state:
                return LocalizedString.State.localized + "*"
            case .zipcode:
                return LocalizedString.ZipCode.localized + "*"
            }
        }
    }
    
    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    private var tableCells: [TableCells] = [.addressLine1, .addressLine2, .zipcode, .city, .state]
    let controller = EditProfileController()
    let addAddressControler = AddAddressControler()
    var addressInput = AddressInput()
    weak var delegate : AddressDetailAdded?
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSubView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controller.delegate = self
        self.addAddressControler.delegate = self
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if self.addAddressControler.validateAddressInput(addressInput: self.addressInput){
            self.controller.updateProfile(userData: UserProfile.main, status: UpdatedProfile.address, addressInput: self.addressInput)
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddAddressVC{
    
    func setUpSubView() {
        self.addressInput.addressLine1 = UserProfile.main.line1
        self.addressInput.addressLine2 = UserProfile.main.line2
        self.addressInput.zipCode = UserProfile.main.zipCode
        self.addressInput.city = UserProfile.main.city
        self.addressInput.state = UserProfile.main.state
        self.configureTableView()
    }
    
    func configureTableView(){
        addressTableView.delegate = self
        addressTableView.dataSource = self
        addressTableView.registerCell(with: AddProductTextFieldCell.self)
    }
}

extension AddAddressVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Address_Details.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
        self.titleLabel.text = UserProfile.main.city.isEmpty ? LocalizedString.Add_Address.localized : LocalizedString.Edit_Address.localized
    }
}

extension AddAddressVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableCells.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueCell(with: AddProductTextFieldCell.self)
        cell.titleLabel.attributedText = tableCells[indexPath.row].stringValue().attributeStringWithAstric()
        cell.textField.delegate = self
        cell.titleLblTop.constant = 5
        cell.textField.returnKeyType = .next
        cell.textField.keyboardType = .default
        cell.textField.addTarget(self, action: #selector(textDidChange), for: UIControl.Event.editingChanged)
        
        switch tableCells[indexPath.row] {
       
        case .addressLine1:
            cell.textField.isEnabled = true
            cell.textField.text = self.addressInput.addressLine1

        case .addressLine2:
            cell.textField.isEnabled = true
            cell.textField.text = self.addressInput.addressLine2

        case .city:
            cell.textField.isEnabled = false
            cell.textField.text = self.addressInput.city
       
        case .state:
            cell.textField.isEnabled = false
            cell.textField.text = self.addressInput.state

        case .zipcode:
            cell.textField.isEnabled = true
            cell.textField.text = self.addressInput.zipCode
            cell.textField.keyboardType = .numberPad
        default: break
        }
        return cell
    }
}


extension AddAddressVC : EditProfileControllerDelegate {
    
    func willRequestUpdateProfile(){
        self.view.showIndicator()
    }
    
    func updateProfileSuccessfully(updatedFor: UpdatedProfile, msg: String, userData : UserProfile){
        self.view.hideIndicator()
        UserProfile.main.line1 = userData.line1
        UserProfile.main.line2 = userData.line2
        UserProfile.main.city = userData.city
        UserProfile.main.state = userData.state
        UserProfile.main.zipCode = userData.zipCode
        UserProfile.main.saveToUserDefaults()
        self.delegate?.addressDetailsAdded()
        self.dismiss(animated: true, completion: nil)
    }
    
    func failedToUpdateProfile(message : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
    
}

extension AddAddressVC: UITextFieldDelegate {
    
    @objc func textDidChange(textField: UITextField){
        guard let cell = addressTableView.cell(forItem: textField) as? AddProductTextFieldCell else { return }
        guard let indexPath = addressTableView.indexPath(for: cell) else { return }
        let curCell = tableCells[indexPath.row]

         switch curCell {
            case .addressLine1:
                addressInput.addressLine1 = textField.text ?? ""
            case .addressLine2:
                addressInput.addressLine2 = textField.text ?? ""
            
            default:
                break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let cell = addressTableView.cell(forItem: textField) as? AddProductTextFieldCell else { return }
        guard let indexPath = addressTableView.indexPath(for: cell) else { return }
        let curCell = tableCells[indexPath.row]

        switch curCell {
        case .zipcode:
            self.addressInput.city = ""
            self.addressInput.state = ""
            self.addressTableView.reloadRows(at: [IndexPath(row: TableCells.city.rawValue, section: 0), IndexPath(row: TableCells.state.rawValue, section: 0)], with: .none)
            
        default: break
            
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
            guard let cell = addressTableView.cell(forItem: textField) as? AddProductTextFieldCell else { return }
        guard let indexPath = addressTableView.indexPath(for: cell) else { return }
        
        let curCell = tableCells[indexPath.row]
        
        switch curCell {
            case .zipcode:
             
                self.addressInput.zipCode = textField.text ?? ""
              
                LocationManager.shared.geocodeAddressStringFromGoogle(address: addressInput.zipCode) { (addressDict) in
                    
                    if addressDict.isEmpty {
                CommonFunctions.showToastMessage(LocalizedString.invalidZipCode.localized)
                        self.addressInput.city = ""
                        self.addressInput.state = ""
                        self.addressTableView.reloadRows(at: [IndexPath(row: TableCells.city.rawValue, section: 0), IndexPath(row: TableCells.state.rawValue, section: 0)], with: .none)
                        
                    } else {
                        self.addressInput.city = "\(addressDict[ApiKey.city] ?? "")"
                        self.addressInput.state = "\(addressDict[ApiKey.state] ?? "")"
                        self.addressInput.stateCode = "\(addressDict[ApiKey.stateCode] ?? "")"
                        self.addressInput.country = "\(addressDict[ApiKey.country] ?? "")"
                        self.addressTableView.reloadRows(at: [IndexPath(row: TableCells.city.rawValue, section: 0), IndexPath(row: TableCells.state.rawValue, section: 0)], with: .none)
                    }
                }
            
//            case .addressLine1:
//                addressInput.addressLine1 = textField.text ?? ""
//            case .addressLine2:
//                addressInput.addressLine2 = textField.text ?? ""
          
            default:
                break
            }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let cell = addressTableView.cell(forItem: textField) as? AddProductTextFieldCell, let indexPath = addressTableView.indexPath(for: cell) {
            let curCell = tableCells[indexPath.row]
            
            if (range.location == 0 && string == " ") || string.containsEmoji {return false}
            
            if string.isBackSpace { return true }
            
            switch curCell {
                
            case .addressLine1, .addressLine2:
                if (textField.text ?? "").count > 100 { return false }
                
            case .zipcode:
                if (textField.text ?? "").count > 9 { return false }
            default: break
            }
        }
        return true
    }
    
    func isCharacterAcceptableForFirstName(name : String, char : String) -> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 30{
            return false
        }else{
            return char.isAllowed(validationStr: AllowedCharactersValidation.name)
        }
    }
}

extension AddAddressVC : ValidateAddress {
    
    func validateAddress(message: String) {
        CommonFunctions.showToastMessage(message)
    }
}
