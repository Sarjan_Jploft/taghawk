//
//  WhereToDepositVC.swift
//  TagHawk
//
//  Created by Admin on 5/22/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class WhereToDepositVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var debitCardButton: UIButton!
    @IBOutlet weak var depositAccountButton: UIButton!
    
    //MARK:- Variables
    weak var delegate : BankDetailsAdded?
    var addDetailsWhile = AddDepositAccountVC.AddBankDetailsWhile.adding
    var cashOutBalance : Double = 0.0
    var merchant = MerchantData()
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBActions
    @IBAction func depositAccountButtonTapped(_ sender: UIButton) {
        let vc = AddDepositAccountVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        vc.delegate = self
        vc.addDetailsFrom = self.addDetailsWhile
        vc.commingFor = .whereTDepositVc
        vc.cashOutBalance = self.cashOutBalance
        vc.merchant = self.merchant
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func debitCardButtonTapped(_ sender: UIButton) {
        let vc = TransactionDetaildVC.instantiate(fromAppStoryboard: AppStoryboard.PaymentAndShipping)
        vc.commingFor = .whereTDepositVc
        vc.addDetailsWhile = self.addDetailsWhile
        vc.cashOutBalance = self.cashOutBalance
        vc.merchant = self.merchant
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


private extension WhereToDepositVC {
    
    func setUpSubView(){
        self.descriptionLabel.setAttributes(text: LocalizedString.Where_Would_You_Like_To_Deposit.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.debitCardButton.setAttributes(title: LocalizedString.Debit_Card.localized, font: AppFonts.Galano_Medium.withSize(16), titleColor: UIColor.white)
        self.depositAccountButton.setAttributes(title: LocalizedString.Deposit_Account.localized, font: AppFonts.Galano_Medium.withSize(16), titleColor: UIColor.white)
    }
}

//MARK:- configure navigation
extension WhereToDepositVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Cash_Out.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
    
}

//MARK:p- Delegates
extension WhereToDepositVC  : BankDetailsAdded{
    func bankDetailsAdded() {
        self.delegate?.bankDetailsAdded()
    }
    
}
