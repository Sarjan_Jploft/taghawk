//
//  AddDepositAccountVC.swift
//  TagHawk
//
//  Created by Admin on 5/23/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

struct BankDetailsInput {
    
    //MARK:- Variables
    var rountingNumber : String = ""
    var verifyRouting : String = ""
    var accountNumbr : String = ""
    var verifyAccountNumber : String = ""
    var firstName : String = ""
    var lastName : String = ""
    
    init() {
        
    }
    
    init(rountingNumber : String, verifyRouting : String, accountNumbr : String, verifyAccountNumber : String, firstName : String, lastName : String  ) {
        self.rountingNumber = rountingNumber
        self.verifyRouting = verifyRouting
        self.accountNumbr = accountNumbr
        self.verifyAccountNumber = verifyAccountNumber
        self.firstName = firstName
        self.lastName = lastName
    }
    
}

protocol BankDetailsAdded : class {
    func bankDetailsAdded()
}


class AddDepositAccountVC: BaseVC {
    
    enum AddBankDetailsWhile {
        case cashout
        case adding
    }
    
    enum AddDepositAccountCellConditions : Int {
        case routingNumber = 0
        case accountNumber = 1
        case name = 2
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var topDescriptionLabel: UILabel!
    @IBOutlet weak var depositAccountTableView: UITableView!
    
    //MARK:- Variables
    var details = BankDetailsInput()
    let controler = BankDetailsControler()
    weak var delegate : BankDetailsAdded?
    var addDetailsFrom = AddBankDetailsWhile.adding
    var commingFor = CartVC.CommingFrom.other
    var cashOutBalance : Double = 0.0
    var merchant = MerchantData()
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.saveDetailsDelegate = self
        self.controler.getDetailsDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBActions
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.updateMerchant(details: self.details)
    }
    
}

extension AddDepositAccountVC {
    //MARK:- Set up view
    func setUpSubView(){
        self.configureTableView()
        //        self.populateData()
    }
    
    //MARK:- Populate data
    func populateData(){
        
        if UserProfile.main.bankDetails.accountNumber.isEmpty{ return }
        self.details.accountNumbr = UserProfile.main.bankDetails.accountNumber
        self.details.verifyAccountNumber = UserProfile.main.bankDetails.accountNumber
        self.details.rountingNumber = UserProfile.main.bankDetails.routingNumber
        self.details.verifyRouting = UserProfile.main.bankDetails.routingNumber
        let nameArray = UserProfile.main.bankDetails.accountHolderName.split(separator: " ")
        guard let firstname = nameArray.first else { return }
        self.details.firstName = String(firstname)
        if nameArray.count > 1{ self.details.lastName =  String(nameArray[1]) }
    }
    
    func configureTableView(){
        self.depositAccountTableView.separatorStyle = .none
        self.depositAccountTableView.registerNib(nibName: AddDepositAccountCell.defaultReuseIdentifier)
        self.depositAccountTableView.separatorStyle = .none
        self.depositAccountTableView.delegate = self
        self.depositAccountTableView.dataSource = self
    }
    
}

//MARK:- configure navigation
extension AddDepositAccountVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Bank_Details.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
    
}

//MARK:- tableview datasource and delegates
extension AddDepositAccountVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddDepositAccountCell.defaultReuseIdentifier) as? AddDepositAccountCell else {
            fatalError("FilterVC....\(AddDepositAccountCell.defaultReuseIdentifier) cell not found")
        }
        
        switch indexPath.row {
        case AddDepositAccountCellConditions.routingNumber.rawValue :
            cell.setUpFor = .routingNumber
        case AddDepositAccountCellConditions.accountNumber.rawValue:
            cell.setUpFor = .accountNumber
        case AddDepositAccountCellConditions.name.rawValue:
            cell.setUpFor = .name
        default:
            cell.setUpFor = .routingNumber
        }
        
        cell.setUpView()
        cell.populateData(detail: self.details)
        cell.leftTextField.addTarget(self, action: #selector(textDidChange), for: UIControl.Event.editingChanged)
        cell.rightTextField.addTarget(self, action: #selector(textDidChange), for: UIControl.Event.editingChanged)
        
        return cell
    }
}

//MARK:- textfield delegates
extension AddDepositAccountVC {
    
    @objc func textDidChange(_ textfield : UITextField) {
        
        guard let indexPath = textfield.tableViewIndexPath(self.depositAccountTableView) else { return }
        guard let cell = self.depositAccountTableView.cellForRow(at: indexPath) as? AddDepositAccountCell else { return }
        
        switch cell.setUpFor {
            
        case .routingNumber:
            textfield === cell.leftTextField ? (self.details.rountingNumber = textfield.text ?? "") : (self.details.verifyRouting = textfield.text ?? "")
            
        case .accountNumber:
            textfield === cell.leftTextField ? (self.details.accountNumbr = textfield.text ?? "") : (self.details.verifyAccountNumber = textfield.text ?? "")
            
        case .name:
            textfield === cell.leftTextField ? (self.details.firstName = textfield.text ?? "") : (self.details.lastName = textfield.text ?? "")
            
        }
    }
    
}

//MARK:- Webservices
extension AddDepositAccountVC : SaveBankDetailsProtocol {
    
    func willSaveBankDetail(){
        self.view.showIndicator()
    }
    
    func saveBankDetailsSuccessFully(){
        self.view.hideIndicator()
        self.controler.getBankDetails()
        
    }
    
    func failedTOSaveBankDetails(message : String){
        self.view.hideIndicator()
        
    }
    
    func validateBankDetails(message : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
}

//MARK:- webservices
extension AddDepositAccountVC : GetBankDetailsProtocol{
    
    func willGetBankDetail() {
        self.view.showIndicator()
        
    }
    
    func getBankDetailsSuccessFully(details: BankDetail) {
        self.view.hideIndicator()
        if self.addDetailsFrom == .adding{
            CommonFunctions.showToastMessage(LocalizedString.Account_Added_Successfully.localized)
            self.delegate?.bankDetailsAdded()
            guard let vcs = self.navigationController?.viewControllers else { return }
            for item in vcs {
                if item.isKind(of: AccountInfoAndPaymentHistoryVC.self){
                    guard let historyVC = item as?  AccountInfoAndPaymentHistoryVC else { return }
                    historyVC.accountInfoVc.isRefreshing = false
                    historyVC.accountInfoVc.controler.getBalance()
                    self.navigationController?.popToViewController(item, animated: true)
                }
            }
        }else{
            let vc = CashOutRequiredDataVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
            vc.cashoutBalance = self.cashOutBalance
            vc.merchant = self.merchant
            self.navigationController?.pushViewController(vc, animated: true)
            
            guard let vcs = self.navigationController?.viewControllers else { return }
            for item in vcs {
                if item.isKind(of: AccountInfoAndPaymentHistoryVC.self){
                    guard let historyVC = item as?  AccountInfoAndPaymentHistoryVC else { return }
                    historyVC.accountInfoVc.isRefreshing = false
                    historyVC.accountInfoVc.controler.getBalance()
                }
            }
        }
    }
    
    func failedTOGetBankDetails(message: String) {
        self.view.hideIndicator()
    }
}
