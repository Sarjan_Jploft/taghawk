//
//  SettingsVC.swift
//  TagHawk
//
//  Created by Appinventiv on 01/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import PWSwitch

class SettingsVC : BaseVC {
    
    enum SettingOptions : String {
        case pushNotification = "Push Notifications"
        case payment = "Payment"
        case changePassword = "Change Password"
        case refferAFriend = "Refer to friend"
        case paymentMethods = "Payment Methods"
        case registerAsHawkDriver = "Register as Hawkdriver"
        case faq = "FAQ"
        case termsOfUse = "Terms of Use"
        case privacyPolicy = "Privacy Policy"
        case blockedUsers = "Blocked Users"
    }
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var settingsTableView: UITableView!
    @IBOutlet weak var logoutButton: UIButton!
    
    //MARK:- variables
    let options : [SettingOptions] = [SettingOptions.pushNotification, SettingOptions.changePassword,SettingOptions.refferAFriend, SettingOptions.blockedUsers , SettingOptions.registerAsHawkDriver, SettingOptions.faq, SettingOptions.termsOfUse, SettingOptions.privacyPolicy]
    let controler = SettingsControler()
    
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    
    override func bindControler() {
        super.bindControler()
        controler.logoutDelegate = self
        controler.notificationSettingsDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBActions
    @IBAction func logoutButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = .logout
        vc.delegateWithType = self
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
        
    }
    
}



extension SettingsVC{
    
    func setUpSubView(){
        self.logoutButton.setAttributes(title: LocalizedString.logout.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)
        self.configureTableView()
    }
    
    func configureTableView(){
        self.settingsTableView.registerNib(nibName: SettingsCell.defaultReuseIdentifier)
        self.settingsTableView.separatorStyle = .none
        self.settingsTableView.showsVerticalScrollIndicator = false
        self.settingsTableView.delegate = self
        self.settingsTableView.dataSource = self
    }
    
    func loadUrl(loadFor : LoadUrlVC.LodeUrlType){
        let vc = LoadUrlVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        vc.loadUrlFor = loadFor
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func navigateToChangePassword(){
        
        let vc =  ChangePasswordVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func navigateToRefferAFriend() {
        let vc =  ReferFriendViewController.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
        vc.setUpFor = .refferAFriend
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func openPaymentVc(){
        let vc = PaymentVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openBlockedUserVC(){
        let vc = BlockedUsersVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func toggleChanged(sender : PWSwitch){
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.updateNotificationSettings(isMute: sender.on)
    }
    
}

extension SettingsVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.settings.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }    
}


//MARK:- TableView Datasource and Delegate
extension SettingsVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1:
            let loginType = UserProfile.main.loginType
            if loginType == .facebook { return 0 }
            else { return 70 }
            
        case 4:
            return 0
            
        default:
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingsCell.defaultReuseIdentifier) as? SettingsCell else {
            fatalError("SettingsCell....\(SettingsCell.defaultReuseIdentifier) cell not found") }
        cell.toggleSwitch.addTarget(self, action: #selector(toggleChanged), for: UIControl.Event.valueChanged)
        cell.populatedata(type: self.options[indexPath.row])
        cell.setToggleState()
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 1:
            self.navigateToChangePassword()
            
        case 2:
            navigateToRefferAFriend()
            
        case 3:
            self.openBlockedUserVC()
            
        case 5:
            self.loadUrl(loadFor: LoadUrlVC.LodeUrlType.faq)
            
        case 6:
            self.loadUrl(loadFor: LoadUrlVC.LodeUrlType.terms)
            
        case 7:
            self.loadUrl(loadFor: LoadUrlVC.LodeUrlType.privacy)
            
        default:
            // AppNavigator.shared.actionsOnLogout()
            break
        }
        
    }
    
}


extension SettingsVC : LogoutDelegate {
    
    func willLogout() {
        self.view.showIndicator()
    }
    
    func logoutSuccessFully() {
        
        self.view.hideIndicator()
        FireBaseChat.shared.clearDeviceToken(userID: UserProfile.main.userId)
        AppNavigator.shared.actionsOnLogout()
    }
    
    func failedToLogout(message: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
}



extension SettingsVC : NotifivationSettingsDelegate {
 
    func willChangeNotificationSettings() {
        
    }
    
    func notificationSettingsChangedSuccessFully(isMute: Bool) {
        
    }
    
    func failedToChangeNotificationSettings(isMute: Bool) {
        self.settingsTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.none)
    }
    
}


extension SettingsVC : CustomPopUpDelegateWithType {
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        self.controler.logout()
    }
    
    
}

