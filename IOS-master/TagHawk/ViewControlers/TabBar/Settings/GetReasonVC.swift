//
//  GetReasonVC.swift
//  TagHawk
//
//  Created by Admin on 6/7/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol GetReasonToDeclineBackDelegate : class {
    func getReasonBack(msg : String, history : PaymentHistory)
}

class GetReasonVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var getReasonTextView: IQTextView!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var popUpView: UIView!
    
    //MARK:- Variables
    weak var delegate : GetReasonToDeclineBackDelegate?
    var sellerId : String = ""
    var historyObj = PaymentHistory()
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
        IQKeyboardManager.shared.enable = true
    }
    
    //MARK:- IBOutlets
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        IQKeyboardManager.shared.enable = false
    }
    
    @IBAction func declineButtonTapped(_ sender: UIButton) {
        
        guard let txt = self.self.getReasonTextView.text, !txt.isEmpty else {
            CommonFunctions.showToastMessage(LocalizedString.Please_Enter_Reason.localized)
            return }
        IQKeyboardManager.shared.enable = false
        self.delegate?.getReasonBack(msg: self.getReasonTextView.text ?? "", history: self.historyObj)
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension GetReasonVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.getReasonTextView.delegate = self
        self.popUpView.round(radius: 10)
        self.declineButton.round(radius: 10)
        self.cancelButton.round(radius: 10)
        self.declineButton.setAttributes(title: LocalizedString.Decline.localized, font: AppFonts.Galano_Medium.withSize(16), titleColor: UIColor.white)
        self.cancelButton.setAttributes(title: LocalizedString.cancel.localized, font: AppFonts.Galano_Medium.withSize(16), titleColor: AppColors.appBlueColor, backgroundColor: UIColor.white)
        self.cancelButton.setBorder(width: 1, color: AppColors.appBlueColor)
        self.view.backgroundColor = UIColor.clear
        self.dismissView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.getReasonTextView.round(radius: 10)
        self.getReasonTextView.setBorder(width: 1.0, color: AppColors.textfield_Border)
    }
}

//MARK:- Text view delegates
extension GetReasonVC : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (range.location == 0 && text == " ") || text.containsEmoji {return false}
        return true
    }
    
}
