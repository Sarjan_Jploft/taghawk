//
//  RatingVC.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 27/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class RatingVC: BaseVC {
    
    //MARK:- PROPERTIES
    //=================
    var profileData = UserProfile()
    private let controller = RatingsController()
    private var isrefreshed = false
    private var serviceCalledFirstTime = true
    private let refreshControl = UIRefreshControl()
    private var ratingsArr = [RatingModel]()
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userJoinDateLbl: UILabel!
    @IBOutlet weak var userRatingBtn: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var totalRatingsLbl: UILabel!
    @IBOutlet weak var mainTableView: UITableView! {
        didSet {
            mainTableView.delegate = self
            mainTableView.dataSource = self
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        updateNavigationBar(withTitle: LocalizedString.Reviews.localized, leftButtons: [], rightButtons: [], navBarColor: .white)
        self.leftBarItemImage(change: UIImage(named: "icBackBlack")!, ofVC: self)
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        userImgView.round()
    }
    
}

//MARK:- UITABLEIVIEW DELEGATE AND DATASOURCE
extension RatingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratingsArr.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return dequeueRatingCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if let curCell = tableView.cellForRow(at: indexPath) as? RatingTableCell {
            curCell.replyTextView.flashScrollIndicators()
        }
        if ratingsArr.count >= 9  {
            if indexPath.item == self.ratingsArr.count - 1 && self.nextPage != 0{
                controller.getRatings(sellerId: profileData.id, pageNo: page)
            }
        }
    }
    
    func dequeueRatingCell(_ tableView: UITableView, indexPath: IndexPath) -> RatingTableCell {
        let cell = tableView.dequeueCell(with: RatingTableCell.self)
        cell.delegate = self
        let currentRating = ratingsArr[indexPath.row]
        cell.ownerNameLbl.text = profileData.fullName
        cell.userNameLbl.text = currentRating.fullName
        cell.productNameLbl.text = currentRating.productName
        cell.userImgView.setImage_kf(imageString: currentRating.profilePicture, placeHolderImage: #imageLiteral(resourceName: "icTabProfileUnactive"), loader: true)
        cell.ratingsView.rating = currentRating.rating
        cell.reviewLbl.text = currentRating.comment
        cell.replyTextView.textColor = #colorLiteral(red: 0.07843137255, green: 0.07843137255, blue: 0.07843137255, alpha: 1)
        cell.replyTextView.flashScrollIndicators()
        if currentRating.created != 0 {
            cell.timeLbl.text = Date(timeIntervalSince1970: currentRating.created / 1000).timeAgoSince
        }
        if currentRating.replied.replyComment.isEmpty {
            cell.ownerReplyTimeLbl.isHidden = true
            cell.editReplyBtn.setTitle(RatingTableCell.SectionType.reply.rawValue)
            cell.replySectionStackView.isHidden = true
        } else {
            cell.editReplyBtn.setTitle(RatingTableCell.SectionType.edit.rawValue)
            cell.replyTextView.isEditable = false
            if currentRating.replied.replyComment == RatingTableCell.SectionType.post.rawValue || currentRating.replied.replyComment == RatingTableCell.SectionType.update.rawValue {
                cell.textCountLbl.isHidden = false
                cell.ownerReplyTimeLbl.isHidden = true
            } else {
                cell.textCountLbl.isHidden = true
                cell.ownerReplyTimeLbl.isHidden = false
            }
            cell.replySectionStackView.isHidden = false
            if currentRating.replied.editedStatus == true {
                cell.ownerReplyTimeLbl.text = LocalizedString.Edited.localized + " \(Date(timeIntervalSince1970: currentRating.replied.editedDate / 1000).timeAgoSince)"
            } else {
                cell.ownerReplyTimeLbl.text =  "\(Date(timeIntervalSince1970: currentRating.replied.replyDate / 1000).timeAgoSince)"
            }
        }
        if !currentRating.replied.replyBtnText.isEmpty {
            
            cell.editReplyBtn.setTitle(currentRating.replied.replyBtnText)
            if cell.editReplyBtn.title(for: .normal) != RatingTableCell.SectionType.reply.rawValue {
                cell.replySectionStackView.isHidden = false
            } else {
                cell.replySectionStackView.isHidden = true
            }
            
            switch currentRating.replied.replyBtnText {
                
            case RatingTableCell.SectionType.reply.rawValue:
                cell.replyTextView.isEditable = false
                cell.replyTextView.textColor = #colorLiteral(red: 0.07843137255, green: 0.07843137255, blue: 0.07843137255, alpha: 1)
                cell.replyTextView.font = AppFonts.Galano_Light.withSize(13)
                
                
            case RatingTableCell.SectionType.post.rawValue:
                cell.replyTextView.isEditable = true
                DispatchQueue.delay(0.5) {
                    cell.replyTextView.becomeFirstResponder()
                }
                cell.replyTextView.textColor = .black
                cell.replyTextView.font = AppFonts.Galano_Regular.withSize(14)
                cell.textCountLbl.isHidden = false
                
            case RatingTableCell.SectionType.edit.rawValue:
                cell.replyTextView.resignFirstResponder()
                cell.replyTextView.isEditable = false
                cell.replyTextView.textColor = #colorLiteral(red: 0.07843137255, green: 0.07843137255, blue: 0.07843137255, alpha: 1)
                cell.replyTextView.font = AppFonts.Galano_Light.withSize(13)
                cell.replyTextView.scrollToTop()
                cell.textCountLbl.isHidden = true
                
            case RatingTableCell.SectionType.update.rawValue:
                cell.replyTextView.isEditable = true
                DispatchQueue.delay(0.5) {
                    cell.replyTextView.becomeFirstResponder()
                }
                cell.replyTextView.textColor = .black
                cell.replyTextView.font = AppFonts.Galano_Regular.withSize(14)
                cell.textCountLbl.isHidden = false
                
            default: break
            }
        }
        cell.replyTextView.text = currentRating.replied.replyComment
        
        if currentRating.sellerId == UserProfile.main.userId {
            cell.editReplyBtn.isHidden = false
        } else {
            cell.editReplyBtn.isHidden = true
            if currentRating.replied.replyComment.isEmpty {
                cell.ownerNameReplyView.isHidden = true
                cell.replyMainView.isHidden = true
            } else {
                cell.replyMainView.isHidden = false
                cell.ownerNameReplyView.isHidden = false
            }
        }
        return cell
    }
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension RatingVC {
    
    //MARK:- Init set up
    private func initialSetup(){
        controller.delegate = self
        userImgView.setImage_kf(imageString: profileData.profilePicture, placeHolderImage: #imageLiteral(resourceName: "icTabProfileUnactive"))
        userNameLbl.text = profileData.fullName
        let date = Date(milliseconds: Int(profileData.created)).toString(dateFormat: Date.DateFormat.MMMMyyyy.rawValue)
        userJoinDateLbl.text = "Member Since: " + date
        userRatingBtn.setTitle("\(profileData.rating)")
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.mainTableView)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.mainTableView.addSubview(refreshControl)
        controller.getRatings(sellerId: profileData.id, pageNo: page)
    }
    
    //MARK:- Refresh
    @objc func refresh(){
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.resetPage()
        self.isrefreshed = true
        controller.getRatings(sellerId: profileData.id, pageNo: page)
    }
}

//MARK:- RatingsController
extension RatingVC: RatingsControllerDelegate {
    
    func didGetRatings(totalRating: Double, ratingsModel: [RatingModel], nextPage: Int, totalRatings: Int) {
        
        userRatingBtn.setTitle("\(totalRating.rounded(toPlaces: 1))")
        
        serviceCalledFirstTime = ratingsArr.isEmpty ? true : false
        self.view.hideIndicator()
        
        if self.page == 1{
            self.ratingsArr.removeAll()
            self.mainTableView.reloadData()
        }
        
        if nextPage != 0{
            self.page = nextPage
        }
        
        ratingsArr.append(contentsOf: ratingsModel)
        mainTableView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.mainTableView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
        
        totalRatingsLbl.text = "\(LocalizedString.Reviews.localized) (\(totalRatings))"
        totalRatingsLbl.isHidden = ratingsArr.count == 0 ? true : false
    }
    
    func willGetRatings() {
        if self.page == 1{
            self.shouldDisplayEmptyDataView = false
            self.mainTableView.reloadEmptyDataSet()
            if !isrefreshed {
                self.view.showIndicator()
            }
        }
    }
    
    func failedToGetRatings() {
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = true
        self.mainTableView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
}

extension RatingVC: RatingTableCellDelegate {
    
    func editReplyBtnTapped(_ sender: UIButton, tableCell: RatingTableCell) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        guard let indexPath = mainTableView.indexPath(for: tableCell) else { return }
        
        switch sender.title(for: .normal) {
        case RatingTableCell.SectionType.reply.rawValue:
            ratingsArr[indexPath.row].replied.replyBtnText = RatingTableCell.SectionType.post.rawValue
            
            
        case RatingTableCell.SectionType.post.rawValue:
            tableCell.replyTextView.resignFirstResponder()
            ratingsArr[indexPath.row].replied.replyBtnText = RatingTableCell.SectionType.edit.rawValue
            ratingsArr[indexPath.row].replied.replyComment = tableCell.replyTextView.text ?? ""
            ratingsArr[indexPath.row].replied.replyDate = Double(Date().millisecondsSince1970)
            controller.postReply(ratingId: ratingsArr[indexPath.row].id, replyComment: ratingsArr[indexPath.row].replied.replyComment, action: 1)
            
        case RatingTableCell.SectionType.edit.rawValue:
            ratingsArr[indexPath.row].replied.replyBtnText = RatingTableCell.SectionType.update.rawValue
            
        case RatingTableCell.SectionType.update.rawValue:
            tableCell.replyTextView.resignFirstResponder()
            ratingsArr[indexPath.row].replied.replyComment = tableCell.replyTextView.text ?? ""
            ratingsArr[indexPath.row].replied.editedStatus = true
            ratingsArr[indexPath.row].replied.editedDate = Double(Date().millisecondsSince1970)
            ratingsArr[indexPath.row].replied.replyBtnText = RatingTableCell.SectionType.edit.rawValue
            controller.postReply(ratingId: ratingsArr[indexPath.row].id, replyComment: ratingsArr[indexPath.row].replied.replyComment, action: 2)
            
        default: break
        }
        mainTableView.reloadRows(at: [indexPath], with: .automatic)
    }
}
