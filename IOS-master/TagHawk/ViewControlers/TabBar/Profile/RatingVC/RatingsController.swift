//
//  RatingsController.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 27/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

protocol RatingsControllerDelegate: AnyObject {
    func willGetRatings()
    func didGetRatings(totalRating: Double, ratingsModel: [RatingModel], nextPage: Int, totalRatings: Int)
    func failedToGetRatings()
}

class RatingsController {
    
    weak var delegate: RatingsControllerDelegate?
    
    func getRatings(sellerId: String, pageNo: Int) {
        delegate?.willGetRatings()
        
        let params: JSONDictionary = [ApiKey.sellerId: sellerId, ApiKey.pageNo: pageNo, ApiKey.limit: 10]
        
        WebServices.getRatings(parameters: params, success: { (json) in
            
            var ratingArr = [RatingModel]()
            json[ApiKey.data].arrayValue.forEach({ (dataJson) in
                ratingArr.append(RatingModel(json: dataJson))
            })
            self.delegate?.didGetRatings(totalRating: json[ApiKey.totalRating].doubleValue, ratingsModel: ratingArr, nextPage: json[ApiKey.next_hit].intValue, totalRatings: json[ApiKey.total].intValue)
        }) { (err) -> (Void) in
            self.delegate?.failedToGetRatings()
        }
        
    }
    
    func postReply(ratingId: String, replyComment: String, action: Int) {
        
        let params: JSONDictionary = [ApiKey.ratingId: ratingId, ApiKey.replyComment: replyComment, ApiKey.action: action]
        
        WebServices.postRatingReply(parameters: params, success: { (json) in
            
            print(json)
            
        }) { (err) -> (Void) in
            
        }
        
    }
    
}
