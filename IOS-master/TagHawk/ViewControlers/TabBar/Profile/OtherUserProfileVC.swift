//
//  OtherUserProfileVC.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 26/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//



import UIKit
import Popover
import DropDown

class OtherUserProfileVC: BaseVC {
    
    //MARK:- PROPERTIES
    //=================
    private var controller = UserProfileController()
    private let followControler = FollowingFollowersControler()
    private var userData = UserProfile()
    var userId = ""
    let popover = Popover()
    private let dropDown = DropDown()
    let blockControler = BlockedUserControler()

    //MARK:- IBOUTLETS
    //================
    @IBOutlet var profileItemButtons: [UIButton]!
    @IBOutlet weak var profileView: DACircularProgressView!
    @IBOutlet weak var verifiedButton: UIButton!
    @IBOutlet weak var memberNameLabel: UILabel!
    @IBOutlet weak var memberNameJoiningDateLabel: UILabel!
    @IBOutlet var socialMediaButtons: [UIButton]!
    @IBOutlet weak var userRating: UIButton!
    @IBOutlet weak var followersLbl: UILabel!
    @IBOutlet weak var followingLbl: UILabel!
    @IBOutlet weak var underlinedView: UIView!
    @IBOutlet weak var underlinedViewLeading: NSLayoutConstraint!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var productsScrollView: UIScrollView! {
        didSet {
            productsScrollView.delegate = self
            productsScrollView.isPagingEnabled = true
            productsScrollView.showsHorizontalScrollIndicator = false
            productsScrollView.contentSize = CGSize(width: screenWidth * 2, height: 0)
        }
    }
    
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var chatButton: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.doInitialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SingleChatFireBaseController.shared.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.leftBarItemImage(change: UIImage(named: "icBackBlack")!, ofVC: self)
        self.controller.getUserProfileService(userId: userId)
        self.rightBarItemImage(change: AppImages.menuDots.image, ofVC: self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImageView.cornerRadius = profileImageView.frame.height / 2
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        navigationController?.popViewController(animated: true)
    }
    
    
    override func rightBarButtonTapped(_ sender: UIBarButtonItem) {
        super.rightBarButtonTapped(sender)
        showProfileOptions(sender: sender)
    }
    
    private func doInitialSetup() {
        topView.alpha = 0
        productsScrollView.alpha = 0
        self.controller.delegate = self
        self.controller.productDelegate = self
        self.followControler.followUserDelegate = self
        self.blockControler.blockUserdelegate = self
        self.blockControler.blockUserdelegate = self
        self.blockControler.unblockUserDelegate = self
        self.topView.isHidden = true
        self.productsScrollView.isHidden = true
        addSellingVC()
        addSoldVC()
        initialSetup()
    }
    
    //MARK:- TARGET ACTIONS
    //=====================
    @IBAction func clickProfileItemButtonAction(_ sender: UIButton) {
        view.endEditing(true)
        if sender.tag == 0 {
            UIView.animate(withDuration: 0.4) {
                self.productsScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            }
        } else if sender.tag == 1 {
            UIView.animate(withDuration: 0.4) {
                self.productsScrollView.setContentOffset(CGPoint(x: screenWidth, y: 0), animated: true)
            }
        } else {
            UIView.animate(withDuration: 0.4) {
                self.productsScrollView.setContentOffset(CGPoint(x: 2*screenWidth, y: 0), animated: true)
            }
        }
    }
    
    
    @IBAction func clickSocialMediaIcons(_ sender: UIButton) {
        view.endEditing(true)
    }
    
    @IBAction func clickPopoverVerifyButton(_ sender: UIButton) {
        view.endEditing(true)
    }
    
    @IBAction func followersButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = FollowingFollowersVC.instantiate(fromAppStoryboard: AppStoryboard.Profile)
        vc.commingFor = .othersFollowers
        vc.userId = self.userId
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func followingButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = FollowingFollowersVC.instantiate(fromAppStoryboard: AppStoryboard.Profile)
        vc.commingFor = .othersFollowing
        vc.userId = self.userId
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    @IBAction func chatButtonTapped(_ sender: UIButton) {
        if UserProfile.main.loginType == .none {
            AppNavigator.shared.tabBar?.showLoginSignupPopup()
            return
        }
        
            if self.userData.isBlocked{
                CommonFunctions.showToastMessage(LocalizedString.Unblock_User_To_Initiate_Chat.localized)
                return
            }
        
        followersButton.isUserInteractionEnabled = true
        let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
        scene.vcInstantiated = .userDetail
        scene.otherUserData = userData
        AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
    }
    
    @IBAction func followButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if UserProfile.main.loginType == .none {
            AppNavigator.shared.tabBar?.showLoginSignupPopup()
            return
        }
        
        if !self.userData.isFollowing{
            self.followControler.followUser(user: self.userId)
        }else{
            self.followControler.unfollowUser(user: self.userId)
        }
    }
    
    @IBAction func ratingsBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        let ratingsScene = RatingVC.instantiate(fromAppStoryboard: .Profile)
        ratingsScene.profileData = userData
        ratingsScene.profileData.id = userId
        AppNavigator.shared.parentNavigationControler.pushViewController(ratingsScene, animated: true)
    }
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension OtherUserProfileVC {
    
    private func initialSetup(){
        self.underlinedViewLeading.constant = self.profileItemButtons[0].frame.width/2 - underlinedView.width
        profileView.thicknessRatio = 0.05
        profileView.clockwiseProgress = -1
        self.followButton.setAttributes(title: LocalizedString.Follow.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)
        self.chatButton.setAttributes(title: LocalizedString.Chat.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)

    }
    
    func populateData(userData: UserProfile) {
        self.profileImageView.setImage_kf(imageString: userData.profilePicture, placeHolderImage: UIImage(named: "icTabProfileUnactive")!)
        if userData.profilePicture.isEmpty {
            profileImageView.addNameStartingCredential(name: userData.fullName)
        }
        
        if userData.profileCompleted == 100 {
            self.verifiedButton.setImage(UIImage(named: "icVerifiedSheild"), for: .normal)
        } else {
            self.verifiedButton.setImage(nil, for: .normal)
        }
        
        self.memberNameLabel.text = userData.fullName
        self.memberNameJoiningDateLabel.text = "Member Since:"
        self.followersLbl.attributedText = "\(LocalizedString.Followers.localized): \(userData.followers)".attributeBoldStringWithAnotherColor(stringsToColor: ["\(userData.followers)"], size: 15, strClr: .black, substrClr: .black)
        self.followingLbl.attributedText = "\(LocalizedString.Following.localized): \(userData.following)".attributeBoldStringWithAnotherColor(stringsToColor: ["\(userData.following)"], size: 15, strClr: .black, substrClr: .black)
        self.userRating.setTitle("\(userData.rating)")
        self.socialMediaButtons[0].isHidden = userData.isFacebookLogin ? false : true
        self.socialMediaButtons[1].isHidden = userData.isEmailVerified ? false : true
        self.socialMediaButtons[2].isHidden = userData.isPhoneVerified ? false : true
        self.socialMediaButtons[3].isHidden = userData.officialIdVerified ? false : true
        
        let date = Date(milliseconds: Int(userData.created)).toString(dateFormat: Date.DateFormat.MMMMyyyy.rawValue)
        
        self.memberNameJoiningDateLabel.text = "Member Since: " + date
         self.setFollowBUttonStatus()
        
//        let profileCompletePercent = CGFloat(userData.profileCompleted)/100
//        self.profileView.setProgress(profileCompletePercent, animated: true)
//        self.profileView.progressTintColor = AppColors.appBlueColor
//        self.profileView.trackTintColor = AppColors.grayColor
//        self.profileView.tintColor = AppColors.grayColor
    }
    
    fileprivate func gotoEditProfile() {
        let editProfileVC = EditProfileVC.instantiate(fromAppStoryboard: .Home)
        editProfileVC.userData = self.userData
        self.present(editProfileVC, animated: true, completion: nil)
    }
    
    //add followers vc
    //====================
    private func addSellingVC(){
        let sellingVC = ProfileProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        sellingVC.userId = userId
        sellingVC.productType = .Selling
        sellingVC.addedTo = .otherUserProfile
        productsScrollView.frame = sellingVC.view.frame
        productsScrollView.addSubview(sellingVC.view)
        sellingVC.willMove(toParent: self)
        self.addChild(sellingVC)
        sellingVC.view.frame.size.height = self.productsScrollView.frame.height
        sellingVC.view.frame.origin = CGPoint.zero
    }
    
    //add followers vc
    //====================
    private func addSoldVC(){
        let soldVC = ProfileProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        soldVC.userId = userId
        soldVC.productType = .Sold
        soldVC.addedTo = .otherUserProfile
        productsScrollView.frame = soldVC.view.frame
        productsScrollView.addSubview(soldVC.view)
        soldVC.willMove(toParent: self)
        self.addChild(soldVC)
        soldVC.view.frame.size.height = self.productsScrollView.frame.height
        soldVC.view.frame.origin = CGPoint(x: screenWidth, y: 0)
    }
    
    //add followers vc
    //====================
    private func addSavingVC(){
        let savingVC = ProfileProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        savingVC.userId = userId
        savingVC.productType = .Saved
        savingVC.addedTo = .otherUserProfile
        productsScrollView.frame = savingVC.view.frame
        productsScrollView.addSubview(savingVC.view)
        savingVC.willMove(toParent: self)
        self.addChild(savingVC)
        savingVC.view.frame.size.height = self.productsScrollView.frame.height
        savingVC.view.frame.origin = CGPoint(x: 2*screenWidth, y: 0)
    }
    
    //MARK:- Show profile options
    func showProfileOptions(sender: UIBarButtonItem){
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: -65, y:dropDown.anchorView!.plainView.bounds.height - 25)
        dropDown.cellHeight = 30
        dropDown.width = 90
        self.dropDown.dismissMode = .automatic
     
        self.dropDown.dataSource = [(self.userData.isFollowing ? LocalizedString.Unfollow.localized : LocalizedString.Follow.localized), self.userData.isBlocked ? LocalizedString.Unblock.localized : LocalizedString.Block.localized]
        
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            switch index {
                
            case 0:
                weakSelf.followUnfollowUser()
                
            default:
                weakSelf.userData.isBlocked ? weakSelf.showPopUp(type: RemoveFriendPopUp.RemoveFriendFor.unblockUser) :  weakSelf.showPopUp(type: RemoveFriendPopUp.RemoveFriendFor.block)
            }
        }
    }
    
    //MARK:- Set follow button status
    func setFollowBUttonStatus(){
        let btnTitle = self.userData.isFollowing ? LocalizedString.Unfollow.localized : LocalizedString.Follow.localized
        self.followButton.setTitle(btnTitle)
        self.followersLbl.attributedText = "\(LocalizedString.Followers.localized): \(userData.followers)".attributeBoldStringWithAnotherColor(stringsToColor: ["\(userData.followers)"], size: 15, strClr: .black, substrClr: .black)
    }
    
    //MARK:- Follow unfollow user
    func followUnfollowUser(){
        self.userData.isFollowing ? self.showPopUp(type: .unfollow) : self.followControler.followUser(user: self.userId)
    }
    
    //MARK:- Show popup
    func showPopUp(type : RemoveFriendPopUp.RemoveFriendFor){
        let vc = RemoveFriendPopUp.instantiate(fromAppStoryboard: AppStoryboard.Profile)
        vc.commingFor = type
        vc.user = FollowingFollowersModel(name: self.userData.fullName, profilePicture: self.userData.profilePicture, userId: self.userId)
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}

//MARK:- Webservices
extension OtherUserProfileVC: UserProfileDelegate, UserProductDelegate {
    func productDeleteSuccess() {
        
    }
    
    func productDeleteFailed() {
        
    }
    
    func userProductsFailed(errorType: ApiState) {
        
    }
    
    func willGetProducts() {
        self.view.showIndicator()
    }
    
    func willGetUserProfile() {
        self.view.showIndicator()
    }
    
    func userProfileServiceReturn(userData: UserProfile) {
        self.view.hideIndicator()
        self.userData = userData
        populateData(userData: userData)
        self.topView.isHidden = false
        self.productsScrollView.isHidden = false
        
        UIView.animate(withDuration: 0.3) {
            self.topView.alpha = 1
            self.productsScrollView.alpha = 1
        }
    }
    
    func userProfileServiceFailure(errorType: ApiState) {
        self.view.hideIndicator()
        UIView.animate(withDuration: 0.3) {
            self.topView.alpha = 1
            self.productsScrollView.alpha = 1
        }
        //TODO: - Error message
    }
    
    func userProductServiceReturn(userProduct: [Product], nextPage: Int) {

    }
}

extension OtherUserProfileVC: VerifyProfileDelegate {
    func openEditProfile() {
        popover.dismiss()
        gotoEditProfile()
    }
}

extension OtherUserProfileVC {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == productsScrollView {
            if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
                scrollView.contentOffset.y = 0
            }
        }
        
        underlinedViewLeading.constant = (scrollView.contentOffset.x/2) + (profileItemButtons[0].width/2 - 7.5)
        view.layoutIfNeeded()
    }
    
}

//MARK:- Follow user call back
extension OtherUserProfileVC : FollowUserDelegate {
    
    func willFollowUser(){
        self.view.showIndicator()
        
    }
    
    func followUserSuccessFully(userId : String){
        self.view.hideIndicator()
        self.userData.isFollowing = !self.userData.isFollowing
        userData.followers = self.userData.isFollowing ? userData.followers + 1 : userData.followers - 1
        self.setFollowBUttonStatus()
    }
    
    func failedToFollowUser(message : String){
        self.view.hideIndicator()
        
    }
}

//MARK:- Remove friend delegate
extension OtherUserProfileVC : RemoveFriendPopUpDelegate{
    
    func noButtonTapped(type : RemoveFriendPopUp.RemoveFriendFor){
        
    }
    
    func yesButtonTapped(type : RemoveFriendPopUp.RemoveFriendFor, user : FollowingFollowersModel){
        
        switch type {
        case .unfollow:
            self.followControler.unfollowUser(user: user.userId)
        case .block:
            self.blockControler.blockUser(user: self.userId)
            
        case .unblockUser:
            self.blockControler.unBlockUser(user: self.userId)
        
        default:
            break
        }
    }
}

//MARK:- Block user delegate
extension OtherUserProfileVC  : BlockUserDelegate {
  
    func willRequestBlockUser() {
        self.view.showIndicator()
    }
    
    func blockUserSuccessfull(user : String) {
        self.view.hideIndicator()
        
        SingleChatFireBaseController.shared.blockedUser(userID: UserProfile.main.userId, otherUserID: user)
        CommonFunctions.showToastMessage(LocalizedString.User_Blocked_Successfully.localized)
    }
    
    func failedToBlockUser(message: String) {
        self.view.hideIndicator()
    }
}

//MARK:- unblock user delegate
extension OtherUserProfileVC : UnBlockUserDelegate{
    
    func willRequestUnBlockUser() {
        self.view.showIndicator()
        
    }
    
    func unBlockUserSuccessfull(user: String) {
        self.view.hideIndicator()
        self.userData.isBlocked = false
    }
    
    func failedToUnBlockUser(message: String) {
        self.view.hideIndicator()
        
    }
}

extension OtherUserProfileVC: SingleChatFireBaseControllerDelegate {
    
    func userBlocked(){
        navigationController?.popViewController(animated: true)
    }
}
