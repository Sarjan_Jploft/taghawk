//
//  UserProfileVC.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 19/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Popover

class UserProfileVC: BaseVC {

    //MARK:- IBOUTLETS
    //================
    @IBOutlet var profileItemButtons: [UIButton]!
    @IBOutlet weak var profileView: DACircularProgressView!
    @IBOutlet weak var verifiedButton: UIButton!
    @IBOutlet weak var memberNameLabel: UILabel!
    @IBOutlet weak var followersLbl: UILabel!
    @IBOutlet weak var followingLbl: UILabel!
    @IBOutlet weak var memberNameJoiningDateLabel: UILabel!
    @IBOutlet var socialMediaButtons: [UIButton]!
    @IBOutlet var socialMediaInfoBtns: [UIButton]!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var isVerfiedLabel: UILabel!
    @IBOutlet weak var userRating: UIButton!
    @IBOutlet weak var underlinedView: UIView!
    @IBOutlet weak var underlinedViewLeading: NSLayoutConstraint!
    @IBOutlet var socialInfoImage: [UIImageView]!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var profileHeaderView: UIView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var productsScrollView: UIScrollView! {
        didSet {
            productsScrollView.delegate = self
            productsScrollView.isPagingEnabled = true
            productsScrollView.showsHorizontalScrollIndicator = false
            productsScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            var scrollHeight: CGFloat {
                var height: CGFloat = 0
                productsScrollView.subviews.forEach { (subView) in
                    height += subView.frame.size.height
                }
                return height
            }
            productsScrollView.contentSize = CGSize(width: screenWidth * 3, height: scrollHeight)
        }
    }
    
    //MARK:- Variables
    private var sellingVC : ProfileProductVC!
    private var soldVC : ProfileProductVC!
    var savingVC : ProfileProductVC!
    private var controller = UserProfileController()
    private var userData = UserProfile()
    //    private var userProduct = [Product]()
    var userId = ""
    let popover = Popover()
    private var isLoadedFirstTime = true
    let bankControler = BankDetailsControler()
    
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.doInitialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.rightBarItemImage(change: AppImages.settings.image, ofVC: self)
        self.leftBarItemImage(change: AppImages.blueShare.image, ofVC: self)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        AppNavigator.shared.parentNavigationControler.setNavigationBarHidden(true, animated: false)
        if isLoadedFirstTime {
            isLoadedFirstTime = false
            view.showIndicator()
        }
        self.controller.getUserProfileService(userId: userId)
//        self.controller.getUserProduct(userId: userId, productStatus: 1, page: self.page)
        
        if let selling = self.sellingVC{
            selling.resetPage()
            selling.controller.getUserProduct(userId: userId, productStatus: ProfileProductVC.ProductsType.Selling.rawValue , page: selling.page)
        }
        
        if let sold = self.soldVC{
            sold.resetPage()
            sold.controller.getUserProduct(userId: userId, productStatus: ProfileProductVC.ProductsType.Sold.rawValue , page: sold.page)
        }
        
        if let saved = self.savingVC {
            saved.resetPage()
            saved.controller.getUserProduct(userId: userId, productStatus: ProfileProductVC.ProductsType.Saved.rawValue , page: saved.page)
        }
        
        self.bankControler.getBalance()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImageView.cornerRadius = profileImageView.frame.height / 2
    }
    
    //MARK:- Right button tapped
    override func rightBarButtonTapped(_ sender: UIBarButtonItem) {
        super.rightBarButtonTapped(sender)
        let vc = SettingsVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    //MARK:- Left button tapped
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        let vc =  ReferFriendViewController.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
        vc.setUpFor = .refferAFriend
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Initial set up
    private func doInitialSetup() {
        self.controller.delegate = self
        self.controller.productDelegate = self
        self.bankControler.getBalanceDelegate = self
        addSellingVC()
        addSoldVC()
        addSavingVC()
        initialSetup()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    
    //MARK:- TARGET ACTIONS
    //=====================
    //MARK:- Profile item buttons actions
    @IBAction func clickProfileItemButtonAction(_ sender: UIButton) {
        view.endEditing(true)
        if sender.tag == 0 {
            UIView.animate(withDuration: 0.4) {
                self.productsScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            }
        } else if sender.tag == 1 {
            UIView.animate(withDuration: 0.4) {
                self.productsScrollView.setContentOffset(CGPoint(x: screenWidth, y: 0), animated: true)
            }
        } else {
            UIView.animate(withDuration: 0.4) {
                self.productsScrollView.setContentOffset(CGPoint(x: 2*screenWidth, y: 0), animated: true)
            }
        }
    }
    
    //MARK:- Share button tapped
    @IBAction func clickShareButton(_ sender: UIButton) {
        view.endEditing(true)
        let subject = UserProfile.main.fullName + "'s profie"
        shareUrl(url: userData.shareUrl, subject: subject)
    }
    
    //MARK:- Edit button
    @IBAction func clickEditButton(_ sender: UIButton) {
        view.endEditing(true)
        gotoEditProfile()
    }
    
    //MARK:- Social media button tapped
    @IBAction func clickSocialMediaIcons(_ sender: UIButton) {
        view.endEditing(true)
    }
    
    //MARK:- Social media info icons tapped
    @IBAction func clickSocialMediaInfoIcons(_ sender: UIButton) {
        view.endEditing(true)
        openInfoView(sender)
    }
    
    //MARK:- popover view button tapped
    @IBAction func clickPopoverVerifyButton(_ sender: UIButton) {
        view.endEditing(true)
        if self.userData.profileCompleted != 100 {
            let navHeight = navigationController?.navigationBar.frame.height ?? 44
            var safeAreaHeight: CGFloat = 0.0
            if #available(iOS 11.0, *) {
                safeAreaHeight = AppDelegate.shared.window?.safeAreaInsets.top ?? 44
            }
            let yAxis = sender.frame.maxY + navHeight + safeAreaHeight + 10
            let startPoint = CGPoint(x: sender.frame.midX + 15, y: yAxis)
            let aView = VerifyPopOverView(frame:CGRect(x: 0, y: 0, width: 222, height: 93) )
            aView.delegate = self
            popover.show(aView, point: startPoint)
        }
    }

    //MARK:- Followers button tapped
    @IBAction func followersButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = FollowingFollowersVC.instantiate(fromAppStoryboard: AppStoryboard.Profile)
        vc.commingFor = .followers
    AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    //MARK:- Following button tapped
    @IBAction func followingButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = FollowingFollowersVC.instantiate(fromAppStoryboard: AppStoryboard.Profile)
        vc.commingFor = .following
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    //MARK:- Rating button action
    @IBAction func ratingBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        let ratingsScene = RatingVC.instantiate(fromAppStoryboard: .Profile)
        ratingsScene.profileData = userData
        ratingsScene.profileData.id = UserProfile.main.userId
        AppNavigator.shared.parentNavigationControler.pushViewController(ratingsScene, animated: true)
    }
    
    //MARK:- Amount tapped
    @objc func amountTapped(){
        let vc = AccountInfoAndPaymentHistoryVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
        vc.commingFrom = .feomProfile
        vc.selectedTab = AccountInfoAndPaymentHistoryVC.CurrentlySelectedHomeTab.accountInfo
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension UserProfileVC {
    
    //MARK:- init set up
    private func initialSetup(){
        profileHeaderView.alpha = 0
        productsScrollView.alpha = 0
        self.underlinedViewLeading.constant = self.profileItemButtons[0].frame.width/2 - underlinedView.width
        profileView.thicknessRatio = 0.05
        profileView.clockwiseProgress = -1
        productsScrollView.bounces = false
        productsScrollView.isPagingEnabled = true
        productsScrollView.showsHorizontalScrollIndicator = false
        self.amountLabel.setAttributes(text: "", font: AppFonts.Galano_Medium.withSize(16), textColor: AppColors.appBlueColor, backgroundColor: UIColor.clear)
        let tap = UITapGestureRecognizer(target: self, action: #selector(amountTapped))
        self.amountLabel.addGestureRecognizer(tap)
        self.amountLabel.isUserInteractionEnabled = true
    }
    
    //MARK:- populate data
    func populateData(userData: UserProfile) {
        
        self.profileImageView.setImage_kf(imageString: userData.profilePicture, placeHolderImage: UIImage(named: "icTabProfileUnactive")!)
        if userData.profilePicture.isEmpty {
            profileImageView.addNameStartingCredential(name: userData.fullName)
        } else {
            profileImageView.removeNameStringCredential()
        }
        if userData.profileCompleted == 100 {
            self.verifiedButton.setImage(UIImage(named: "icVerifiedSheild"), for: .normal)
        } else {
            self.verifiedButton.setImage(UIImage(named: "icUnverifySheild"), for: .normal)
        }
        self.memberNameLabel.text = userData.fullName
        self.memberNameJoiningDateLabel.text = "Member Since:"
        self.followersLbl.attributedText = "\(LocalizedString.Followers.localized): \(userData.followers)".attributeBoldStringWithAnotherColor(stringsToColor: ["\(userData.followers)"], size: 15, strClr: .black, substrClr: .black)
        self.followingLbl.attributedText = "\(LocalizedString.Following.localized): \(userData.following)".attributeBoldStringWithAnotherColor(stringsToColor: ["\(userData.following)"], size: 15, strClr: .black, substrClr: .black)
        self.isVerfiedLabel.text = userData.profileCompleted == 1 ? "Verified Seller" : "\(userData.profileCompleted)" + "% complete"
        self.userRating.setTitle("\(userData.rating)")
        self.socialInfoImage[0].isHidden = userData.isFacebookLogin ? true : false
        self.socialInfoImage[1].isHidden = userData.isEmailVerified ? true : false
        self.socialInfoImage[2].isHidden = userData.isPhoneVerified ? true : false
        self.socialInfoImage[3].isHidden = userData.isDocumentsVerified ? true : false
        let date = Date(milliseconds: Int(userData.created)).toString(dateFormat: Date.DateFormat.MMMMyyyy.rawValue)
        self.memberNameJoiningDateLabel.text = "Member Since: " + date
        let profileCompletePercent = CGFloat(userData.profileCompleted)/100
        self.profileView.setProgress(profileCompletePercent, animated: true)
        self.profileView.progressTintColor = AppColors.appBlueColor
        self.profileView.trackTintColor = AppColors.grayColor
        self.profileView.tintColor = AppColors.grayColor
    }

    //MARK:- Navigate to edit profile
    fileprivate func gotoEditProfile() {
        let editProfileVC = EditProfileVC.instantiate(fromAppStoryboard: .Home)
        editProfileVC.userData = self.userData
        self.present(editProfileVC, animated: true, completion: nil)
    }
    
    //MARK:- Open info view
    private func openInfoView(_ sender: UIButton) {
        view.endEditing(true)
        let navHeight = navigationController?.navigationBar.frame.height ?? 44
        var safeAreaHeight: CGFloat = 0.0
        if #available(iOS 11.0, *) {
            safeAreaHeight = AppDelegate.shared.window?.safeAreaInsets.top ?? 44
        }
        let yAxis = sender.frame.maxY + navHeight + safeAreaHeight
        let startPoint = CGPoint(x: sender.frame.midX , y: yAxis)
        let aView = VerifyPopOverView(frame:CGRect(x: 0, y: 0, width: 222, height: 93))
        aView.delegate = self
        
        switch sender.tag {
        case 0:
            if userData.isFacebookLogin { return } else {
                aView.label.text = LocalizedString.verifyFacebookAccount.localized
            }
        case 1:
            if userData.isEmailVerified { return } else {
                aView.label.text = LocalizedString.verifyEmailAddress.localized
            }
        case 2:
            if userData.isPhoneVerified { return } else {
                aView.label.text = LocalizedString.verifyPhoneNumb.localized
            }
        case 3:
            if userData.isDocumentsVerified { return } else {
                aView.label.text = LocalizedString.verifyOfficialID.localized
            }
        default: return
        }
        popover.show(aView, point: startPoint)
    }
    
    //add followers vc
    //====================
    private func addSellingVC(){
        sellingVC = ProfileProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        sellingVC.userId = UserProfile.main.userId
        sellingVC.productType = .Selling
        productsScrollView.frame = sellingVC.view.frame
        productsScrollView.addSubview(sellingVC.view)
        sellingVC.willMove(toParent: self)
        self.addChild(sellingVC)
        sellingVC.view.frame.size.height = self.productsScrollView.frame.height
        sellingVC.view.frame.origin = CGPoint.zero
    }
    
    //add followers vc
    //====================
    private func addSoldVC(){
        soldVC = ProfileProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        soldVC.userId = UserProfile.main.userId
        soldVC.productType = .Sold
        productsScrollView.frame = soldVC.view.frame
        productsScrollView.addSubview(soldVC.view)
        soldVC.willMove(toParent: self)
        self.addChild(soldVC)
        soldVC.view.frame.size.height = self.productsScrollView.frame.height
        soldVC.view.frame.origin = CGPoint(x: screenWidth, y: 0)
    }
    
    //add followers vc
    //====================
    private func addSavingVC(){
        savingVC = ProfileProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        savingVC.userId = UserProfile.main.userId
        savingVC.productType = .Saved
        productsScrollView.frame = savingVC.view.frame
        productsScrollView.addSubview(savingVC.view)
        savingVC.willMove(toParent: self)
        self.addChild(savingVC)
        savingVC.view.frame.size.height = self.productsScrollView.frame.height
        savingVC.view.frame.origin = CGPoint(x: 2*screenWidth, y: 0)
    }
    
    //MARK:- Update promoted product status
    func updatePromotedProductsStatus(promotedProducts : [Product]){
        for item in promotedProducts{
            
            if self.sellingVC == nil || self.soldVC == nil || self.savingVC == nil { return }
           
            if let index = self.sellingVC.userProduct.lastIndex(where: { $0.productId == item.productId}) {
                self.sellingVC.userProduct[index].isPromoted = true
            }
            
            if let index = self.soldVC.userProduct.lastIndex(where: { $0.productId == item.productId}) {
                self.soldVC.userProduct[index].isPromoted = true
            }
            
            if let index = self.savingVC.userProduct.lastIndex(where: { $0.productId == item.productId}) {
                self.savingVC.userProduct[index].isPromoted = true
            }
        }
    }
}

//MARK:- Scrollview delegates
extension UserProfileVC {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == productsScrollView {
            if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
                scrollView.contentOffset.y = 0
            }
        }
        
        underlinedViewLeading.constant = (scrollView.contentOffset.x/3) + (profileItemButtons[0].width/2 - 7.5)
        
        view.layoutIfNeeded()
    }
}

extension UserProfileVC : ProfileUpdatedDelegate {
    
    func profileUpdated(userProfile : UserProfile){
        self.userData = userProfile
        
    }
    
}

//MARK:- Webservices
extension UserProfileVC: UserProfileDelegate, UserProductDelegate {
    
    func willGetUserProfile() {
        
    }
    
    func productDeleteSuccess() {
        
    }
    
    func productDeleteFailed() {
        view.hideIndicator()
    }
    
    func userProductsFailed(errorType: ApiState) {
        productsScrollView.hideIndicator()
    }
    
    func willGetProducts() {
    }
    
    func userProfileServiceReturn(userData: UserProfile) {
        view.hideIndicator()
        UIView.animate(withDuration: 0.3) {
            self.profileHeaderView.alpha = 1
            self.productsScrollView.alpha = 1
        }
        self.userData = userData
        FireBaseChat.shared.updateUserData(userData: self.userData)
        populateData(userData: userData)
    }
    
    func userProfileServiceFailure(errorType: ApiState) {
        UIView.animate(withDuration: 0.3) {
            self.profileHeaderView.alpha = 1
            self.productsScrollView.alpha = 1
        }
        view.hideIndicator()
    }
    
    func userProductServiceReturn(userProduct: [Product], nextPage: Int) {
//        self.userProduct = userProduct
//        populateData(userData: userData)
    }
    
}

extension UserProfileVC: VerifyProfileDelegate {
    func openEditProfile() {
        popover.dismiss()
        gotoEditProfile()
    }
}


extension UserProfileVC : GetBalanceDelegate {
    
    func willGetBalance() {
       
    }
    
    func getBalanceSuccessFully(cashoutBalance: Double, avilableSoonBalance: Double, pendingBalance: Double) {        self.amountLabel.text = " \(LocalizedString.Wallet.localized) $\(cashoutBalance.formatPoints())"
                
    }
    
    func failedToGetBalance(msg: String) {
        CommonFunctions.showToastMessage(msg)
    }
}


