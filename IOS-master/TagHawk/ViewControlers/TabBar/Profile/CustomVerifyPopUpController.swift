//
//  CustomVerifyPopUpController.swift
//  TagHawk
//
//  Created by Appinventiv on 15/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

protocol CustomVerifyPopUpControllerDelegate: class {
    func error(str: String)
}


class CustomVerifyPopUpController {
    
    weak var delegate: CustomVerifyPopUpControllerDelegate?
    
    func validateData(text: String, countryCode: String, vcInstantiated: CustomPhoneVerifyPopUpVCInstantiatedFor) -> Bool{
        
        switch vcInstantiated {
            
        case .email:
            if !validateEmail(email: text){
                return false
            }
        case .phoneNumber:
            if !validatePhoneNumber(phoneNumber: text, countryCode: countryCode){
                CommonFunctions.showToastMessage(LocalizedString.Please_enter_valid_phoneNumber.localized)
                return false
            }
            
        case .ssn:
            if !validateSsn(ssn: text){
                CommonFunctions.showToastMessage(LocalizedString.Please_Enter_Ssn.localized)
                return false
            }
            
        default:
            return true
        }
        return true
    }
    
    func validateEmail(email : String) -> Bool {
        
        if email.isEmpty {
            self.delegate?.error(str: LocalizedString.Please_Enter_Email.localized)
            return false
        }else if email.checkIfInvalid(.email) {
            self.delegate?.error(str: LocalizedString.Please_Enter_A_Valid_Email.localized)
            return false
        }
        return true
    }
    
    func validateSsn(ssn : String) -> Bool {
        
        if ssn.isEmpty {
            self.delegate?.error(str: LocalizedString.Please_Enter_Ssn.localized)
            return false
        } else if ssn.count < 4{
            self.delegate?.error(str: LocalizedString.Please_Enter_Valid_Ssn.rawValue)
            return false
        }
        return true
        
    }
    
    func validatePhoneNumber(phoneNumber: String, countryCode: String) -> Bool{
        
        if countryCode.isEmpty{
            self.delegate?.error(str: LocalizedString.Please_Enter_Email.localized)
            return false
        }
        if phoneNumber.isEmpty{
            self.delegate?.error(str: LocalizedString.Please_Enter_phonenumber.localized)
            return false
        }else if phoneNumber.checkIfInvalid(.mobileNumber){
            self.delegate?.error(str: LocalizedString.Please_enter_valid_phoneNumber.localized)
            return false
        }
        return true
    }
    
}
