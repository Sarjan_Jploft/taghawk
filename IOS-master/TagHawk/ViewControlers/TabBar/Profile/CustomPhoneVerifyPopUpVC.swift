//
//  CustomPhoneVerifyPopUpVC.swift
//  TagHawk
//
//  Created by Vikash on 09/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

enum CustomPhoneVerifyPopUpVCInstantiatedFor {
    case phoneNumber
    case email
    case ssn
}

protocol CustomPhoneVerifyPopUpDelegate: class {
    
    func doneBtnTapped(instantaited: CustomPhoneVerifyPopUpVCInstantiatedFor, updatedValue: String, countryCode: String)
    
}

class CustomPhoneVerifyPopUpVC: UIViewController, UITextFieldDelegate {
    
    //    MARK:- IBOutlets
    //    ================
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var countryCodeButton: UIButton!
    @IBOutlet weak var popUpTitle: UILabel!
    @IBOutlet weak var phoneNumberDescription: UILabel!
    @IBOutlet weak var textFieldOuterView: UIView!
    @IBOutlet weak var countryCodeBtnWidth: NSLayoutConstraint!
    
    //MARK:- variables
    var instantiated: CustomPhoneVerifyPopUpVCInstantiatedFor = .phoneNumber
    let controller = CustomVerifyPopUpController()
    weak var delegate: CustomPhoneVerifyPopUpDelegate?
    var userData: UserProfile!
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    //MARK:- IBActions
    
    //MARK:- Country code button tapped
    
    //MARK:- Country code button tapped
    @IBAction func clickCountryCodeButton(_ sender: UIButton) {
        view.endEditing(true)
        let vc = CountryCodeVC.instantiate(fromAppStoryboard: .Home)
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Cancel button tapped
    @IBAction func clickCancelButton(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Done button tapped
    @IBAction func doneBtntapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if controller.validateData(text: phoneNumberTextField.text ?? "", countryCode: countryCodeButton.titleLabel?.text ?? "", vcInstantiated: instantiated){
            dismiss(animated: true) {[weak self] in
                guard let strongSelf = self else{
                    return
                }
                
                strongSelf.delegate?.doneBtnTapped(instantaited: strongSelf.instantiated , updatedValue: strongSelf.phoneNumberTextField.text ?? "", countryCode: strongSelf.countryCodeButton.titleLabel?.text ?? "")
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " {
            return false
        }
        if let text = textField.text {
            let newLength = text.count + string.count - range.length
            switch instantiated {
                
            case .email: return newLength <= 50
                
            case .phoneNumber: return newLength <= 15
                
            case .ssn : return newLength <= 4
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

extension CustomPhoneVerifyPopUpVC {
    
    //MARK:- Initial setup
    private func initialSetup(){
        
        view.backgroundColor = AppColors.blackColor.withAlphaComponent(0.3)
        popUpTitle.text = instantiated == .phoneNumber ? LocalizedString.verifyPhoneNumber.localized : LocalizedString.verifyEmail.localized
        popUpTitle.font = AppFonts.Galano_Semi_Bold.withSize(17.5)
        popUpTitle.textColor = AppColors.blackLblColor
        phoneNumberDescription.text = instantiated == .phoneNumber ? LocalizedString.verifyYourNumber.localized : LocalizedString.verifyYourEmail.localized
        phoneNumberDescription.font = AppFonts.Galano_Regular.withSize(13)
        phoneNumberDescription.textColor = AppColors.lightGreyTextColor
        phoneNumberTextField.placeholder = instantiated == .phoneNumber ? LocalizedString.enterPhoneNumber.localized : LocalizedString.enterEmptyEmail.localized
        phoneNumberTextField.font = AppFonts.Galano_Regular.withSize(14)
        phoneNumberDescription.textColor = AppColors.blackLblColor
        countryCodeBtnWidth.constant = instantiated == .phoneNumber ? 80 : 0.0
        countryCodeButton.isHidden = instantiated != .phoneNumber
        phoneNumberTextField.delegate = self
        
        self.populateData()
        self.setUpKeyBoard()
    }
    
    //MARK:- Set up keyboard
    func setUpKeyBoard(){
        switch self.instantiated{
        case .phoneNumber, .ssn:
            self.phoneNumberTextField.keyboardType = .numberPad
            
        default:
            self.phoneNumberTextField.keyboardType = .default
            
        }
    }
    
    //MARK:- Populate data
    func populateData(){
        
        switch self.instantiated {
        case .phoneNumber:
            popUpTitle.text = LocalizedString.verifyPhoneNumber.localized
            
            phoneNumberDescription.text = LocalizedString.verifyYourNumber.localized
            
            phoneNumberTextField.placeholder = LocalizedString.enterPhoneNumber.localized
            
            if userData.countryCode.isEmpty{
                //                let countryCode = AppUserDefaults.value(forKey: .countryCode).stringValue
                //                if countryCode.isEmpty{
                //                    CommonFunctions.getCurrentCountryCode()
                //                }
                //                countryCodeButton.setTitle(countryCode)
                countryCodeButton.setTitle("+1")
            }else{
                countryCodeButton.setTitle(userData.countryCode)
            }
            phoneNumberTextField.text = userData.phoneNumber
            
        case .email:
            popUpTitle.text = LocalizedString.verifyEmail.localized
            phoneNumberDescription.text = LocalizedString.verifyYourEmail.localized
            phoneNumberTextField.placeholder = LocalizedString.enterEmptyEmail.localized
            phoneNumberTextField.text = userData.email
            
        case .ssn:
            popUpTitle.text = LocalizedString.Social_Security_Number.localized
            phoneNumberDescription.text = LocalizedString.Enter_Last_4_Digits_Of_SSN.localized
            phoneNumberTextField.placeholder = LocalizedString.Enter_SSN.localized
            phoneNumberTextField.text = userData.ssnNumber
            
        }
    }
}

extension CustomPhoneVerifyPopUpVC: SetContryCodeDelegate {
    func setCountryCode(country_info: JSONDictionary) {
        let code = "+\(country_info["CountryCode"]!)"
        countryCodeButton.setTitle(code)
    }
}
