//
//  RemoveTapPopUp.swift
//  TagHawk
//
//  Created by Appinventiv on 27/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol RemoveFriendPopUpDelegate : class {
    func noButtonTapped(type : RemoveFriendPopUp.RemoveFriendFor)
    func yesButtonTapped(type : RemoveFriendPopUp.RemoveFriendFor, user : FollowingFollowersModel)
}

extension RemoveFriendPopUpDelegate {
   
    func noButtonTapped(type : RemoveFriendPopUp.RemoveFriendFor){}
    func yesButtonTapped(type : RemoveFriendPopUp.RemoveFriendFor, user : FollowingFollowersModel){}
}

class RemoveFriendPopUp : UIViewController {

    enum RemoveFriendFor {
        case unfollow
        case block
        case removeFollower
        case unblockUser
    }
    
    //IBOutlets:-
    @IBOutlet weak var dismisView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var multipleButtonBackView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var buttonGradientImageView: UIImageView!
    
    
    var commingFor = RemoveFriendFor.unfollow
    
    var user = FollowingFollowersModel()
    
    weak var delegate : RemoveFriendPopUpDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
        self.userImageView.round()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func noButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true) {
            self.delegate?.noButtonTapped(type: self.commingFor)
        }
    }
    
    @IBAction func yesButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true) {
            self.delegate?.yesButtonTapped(type: self.commingFor, user: self.user)
        }
    }

}


extension RemoveFriendPopUp{
    
    func setUpSubView(){
        self.userImageView.contentMode = .scaleAspectFill
        self.dismisView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.backgroundColor = UIColor.clear
        self.noButton.round(radius: 10)
        self.yesButton.round(radius: 10)
        self.buttonGradientImageView.round(radius: 10)
        self.noButton.backgroundColor = UIColor.white
        self.noButton.setBorder(width: 0.5, color: AppColors.appBlueColor)
        self.noButton.setAttributes(title: LocalizedString.cancel.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: AppColors.appBlueColor, backgroundColor: UIColor.white)
        self.yesButton.setAttributes(title: LocalizedString.Delete.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
        
        self.headingLabel.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(15), textColor: UIColor.black)
        
        populateData()
    }
    
    func populateData(){
    
        self.userImageView.setImage_kf(imageString: self.user.profilePicture, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        
        switch self.commingFor {
        case .unfollow:
            printDebug("unfollow")
            
        let completeMsg = LocalizedString.Sure_To_Unfollow.localized.replacingOccurrences(of: "name", with: self.user.fullName)
            
           self.descriptionLabel.attributedText = completeMsg.attributedMessageWithHighlitedUserName(stringTobeAttributed: self.user.fullName)
            self.yesButton.setTitle(LocalizedString.Unfollow.localized)
        self.headingLabel.text = ""
      
        case .block:
            let completeMsg = LocalizedString.Sure_To_Block.localized.replacingOccurrences(of: "name", with: self.user.fullName)
            self.descriptionLabel.attributedText = completeMsg.attributedMessageWithHighlitedUserName(stringTobeAttributed: self.user.fullName)
            self.yesButton.setTitle(LocalizedString.Block.localized)
            self.headingLabel.text = "\(LocalizedString.Block) \(self.user.fullName)"
            
        case .removeFollower:
            let completeMsg = LocalizedString.Sure_To_Remove_Follower.localized.replacingOccurrences(of: "name", with: self.user.fullName)
            self.descriptionLabel.attributedText = completeMsg.attributedMessageWithHighlitedUserName(stringTobeAttributed: self.user.fullName)
            self.yesButton.setTitle(LocalizedString.Remove.localized)
            self.headingLabel.text = "\(LocalizedString.Remove_Friend)?"

        case .unblockUser:
            let completeMsg = LocalizedString.Sure_To_Unblock.localized.replacingOccurrences(of: "name", with: self.user.fullName)
            self.descriptionLabel.attributedText = completeMsg.attributedMessageWithHighlitedUserName(stringTobeAttributed: self.user.fullName)
            self.yesButton.setTitle(LocalizedString.Unblock.localized)
            self.headingLabel.text = "\(LocalizedString.Unblock)?"
            
        }
        
    }
    
}
