//
//  ProfileProductVC.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 20/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown


class ProfileProductVC: BaseVC {
    
    //MARK:- PROPERTIES
    //=================
    
    enum AddedTo {
        case otherUserProfile
        case myProfile
    }
    
    enum ProductsType: Int {
        case none = 0
        case Selling
        case Sold
        case Saved
    }
    
    //MARK:- Variables
    var userId = ""
    var productType: ProductsType = .none
    var userProduct = [Product]()
    let controller = UserProfileController()
    private var isrefreshed = false
    private let refreshControl = UIRefreshControl()
    //    private let productOptionsView = ProductOptionsView.instantiateFromNib()
    private var selectedIndex = IndexPath()
    private let dropDown = DropDown()
    private var emptyLabel = UILabel()
    var addedTo = AddedTo.myProfile
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var mainCollView: UICollectionView! {
        didSet {
            mainCollView.delegate   = self
            mainCollView.dataSource = self
            mainCollView.registerCell(with: ProfileCollectionViewCell.self)
            mainCollView.bounces = true
            mainCollView.alwaysBounceVertical = true
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        emptyLabel.frame = mainCollView.frame
        emptyLabel.setupLabelForEmptyView()
        view.addSubview(emptyLabel)
    }
    
    //MARK:- Populate product
    func promoteProduct() {
        let vc = FeatureProductVC.instantiate(fromAppStoryboard: .Home)
        let prod = userProduct[selectedIndex.item].getAddProductDataFromProduct()
        vc.addProductData = prod
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true) {
        }
    }
    
    //MARK;- Edit product
    func editProduct() {
        let vc = AddProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.product = userProduct[selectedIndex.item]
        vc.commingFor = .edit
        self.present(vc, animated: true, completion: nil)
    }

}

//MARK:- UICOLLECTIONVIEW DELEGATE AND DATASOURCE
extension ProfileProductVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueCell(with: ProfileCollectionViewCell.self, indexPath: indexPath)
        if !userProduct[indexPath.item].images.isEmpty{
            cell.productImage.setImage_kf(imageString: userProduct[indexPath.item].images[0].image, placeHolderImage: UIImage())
        }else{
            cell.productImage.image = UIImage()
        }
        if userId != UserProfile.main.userId {
            cell.moreBtn.isHidden = true
        } else {
            cell.moreBtn.isHidden = (productType == .Saved || productType == .Sold) ? true : false
        }
        cell.buttonTapped = {[weak self] sender in
            self?.moreBtnAction(sender, frame: cell.frame)
            self?.selectedIndex = indexPath
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 4
        return CGSize(width: finalContentWidth / 3, height: finalContentWidth / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if userProduct.count >= 9  {
            if indexPath.item == self.userProduct.count - 1 && self.nextPage != 0 {
                controller.getUserProduct(userId: userId, productStatus: productType.rawValue, page: page)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard !userProduct.isEmpty else { return }
        if productType == .Sold { return }
        let productId = userProduct[indexPath.item].productId
        let productScene = ProductDetailVC.instantiate(fromAppStoryboard: .Home)
        productScene.prodId = productId
        productScene.likeStatusBackDeleate = self
        selectedIndex = indexPath
        AppNavigator.shared.parentNavigationControler.pushViewController(productScene, animated: true)
    }
    
    private func moreBtnAction(_ sender: UIButton, frame: CGRect) {
        showDropDown(sender)
    }
    
}

//MARK:- PRIVATE FUNCTIONS
//========================
extension ProfileProductVC {
    
    private func initialSetup(){
        controller.delegate = self
        controller.productDelegate = self
        //        productOptionsView.delegate = self
        //        productOptionsView.size = CGSize(width: 90, height: 90)
        //        productOptionsView.cornerRadius = 5.0
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.mainCollView)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.mainCollView.refreshControl = refreshControl
        self.resetPage()
        if addedTo == .otherUserProfile{
            controller.getUserProduct(userId: userId, productStatus: productType.rawValue, page: page)
        }
    }
    
    //MARK:- Refresh
    @objc func refresh(){
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.resetPage()
        self.isrefreshed = true
        controller.getUserProduct(userId: userId, productStatus: productType.rawValue, page: page)
    }
    
    //MARK:-= show popup
    private func showPopup(type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none, msg : String = ""){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        vc.delegate = self
        vc.msg = msg
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Show drop down
    private func showDropDown(_ sender: UIButton) {
        view.endEditing(true)
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: -65, y:dropDown.anchorView!.plainView.bounds.height - 25)
        dropDown.cellHeight = 30
        dropDown.width = 90
        self.dropDown.dismissMode = .automatic
        self.dropDown.dataSource = [LocalizedString.Promote.localized, LocalizedString.Edit.localized, LocalizedString.Delete.localized]
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let `self` = self else { return }
            
            switch index {
                
            case 0:
                
                let prod = self.userProduct[self.selectedIndex.item]
                
                prod.isPromoted ? CommonFunctions.showToastMessage(LocalizedString.Product_AlreadyPromoted.localized) : self.promoteProduct()
                
            case 1: self.editProduct()
                
            case 2: self.showPopup(type: .deleteProduct)
                
            default: break
                
            }
            
        }
    }
}

//MARK:- Webservices
extension ProfileProductVC:  UserProfileDelegate, UserProductDelegate {
 
    func willGetUserProfile() {
        
    }
    
    func productDeleteSuccess() {
        resetPage()
        userProduct.remove(at: selectedIndex.item)
        emptyLabel.isHidden = userProduct.isEmpty ? false : true
        mainCollView.reloadData()
    }
    
    func productDeleteFailed() {
        
    }
    
    func willGetProducts() {
        if self.page == 1 || self.page == 0{
            self.shouldDisplayEmptyDataView = false
            self.mainCollView.reloadEmptyDataSet()
        }
    }
    
    func userProfileServiceReturn(userData: UserProfile) {
        self.mainCollView.reloadData()
    }
    
    func userProfileServiceFailure(errorType: ApiState) {
    }
    
    func userProductServiceReturn(userProduct: [Product], nextPage: Int) {
        
        self.view.hideIndicator()
        
        if self.page == 1 || self.page == 0{
            self.userProduct.removeAll()
            self.mainCollView.reloadData()
        }
        
        if nextPage != 0{
            self.page = nextPage
        }
        
        self.nextPage = nextPage
        self.userProduct.append(contentsOf: userProduct)
        emptyLabel.isHidden = self.userProduct.isEmpty ? false : true
        self.mainCollView.reloadData()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
    func userProductsFailed(errorType: ApiState) {
        self.shouldDisplayEmptyDataView = true
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
}


extension ProfileProductVC: PostnotherDelegate {
    
    func productPramotedSuccessfull() {
        
    }
    
    func postAnother() {
        
    }
}

extension ProfileProductVC : CustomPopUpDelegateWithType {
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if type == .deleteProduct {
            self.controller.deleteProduct(userProduct[selectedIndex.item].productId)
        }
    }
}

//MARK:- Get like status back
extension ProfileProductVC: GetLikeStatusBack {
    func getLikeStatus(product: Product) {
        if product.isLiked {
            userProduct.insert(product, at: selectedIndex.item)
        } else {
            let products = userProduct.filter { $0.productId != product.productId }
            userProduct = products
        }
        emptyLabel.isHidden = self.userProduct.isEmpty ? false : true
        mainCollView.reloadData()
    }
}


extension UILabel {
    func setupLabelForEmptyView() {
        self.textAlignment = .center
        self.text = LocalizedString.No_Data_Found.localized
        self.font = AppFonts.Galano_Semi_Bold.withSize(18)
        self.textColor = .black
    }
}
