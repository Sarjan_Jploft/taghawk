//
//  CountryCodeVC.swift
//
//
//  Created by Appinventiv on 29/03/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import UIKit

protocol SetContryCodeDelegate {
    func setCountryCode(country_info: JSONDictionary)
}

class CountryCodeVC: UIViewController {
    
    //MARK: Properties
    /*--------------*/
    var countryInfo = JSONDictionaryArray()
    var filteredCountryList = JSONDictionaryArray()
    var delegate: SetContryCodeDelegate!
    
    //MARK: IBOutlets
    /*-------------*/
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var countryCodeTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: View Life Cycle
    /*-------------------*/
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.view.endEditing(true)
    }
    
    //MARK: IBActions
    /*-------------*/
    @IBAction func backbtnTap(_ sender: UIButton) {
        view.endEditing(true)
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Private Functions
    /*---------------------*/
    private func readJson() {
        
        let file = Bundle.main.path(forResource: "countryData", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: file!))
        let jsonData = try? JSONSerialization.jsonObject(with: data!, options: []) as! JSONDictionaryArray
        let sortedList = jsonData!.sorted(by: { (first, second) -> Bool in
            return (first["CountryEnglishName"] as! String).localizedCaseInsensitiveCompare(second["CountryEnglishName"] as! String) == ComparisonResult.orderedAscending})
        self.countryInfo = sortedList
        self.filteredCountryList = sortedList
        self.countryCodeTableView.reloadData()
    }
    
    private func initialSetup(){
        
        navigationTitle.text = LocalizedString.selecteCountry.localized
        navigationTitle.textColor = AppColors.whiteColor
        navigationTitle.font = AppFonts.Galano_Medium.withSize(15)
        navigationView.backgroundColor = AppColors.themeColor
        
        self.countryCodeTableView.delegate = self
        self.countryCodeTableView.dataSource = self
        self.searchBar.delegate = self
        searchBar.tintColor = AppColors.whiteColor
        searchBar.backgroundColor = AppColors.themeColor
        self.readJson()
        let textFieldInsideUISearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideUISearchBar?.textColor = AppColors.whiteColor
        let placeholderLabel       = textFieldInsideUISearchBar?.value(forKey: "placeholderLabel") as? UILabel
        placeholderLabel?.textColor = AppColors.lightGreyTextColor
        placeholderLabel?.font = AppFonts.Galano_Regular.withSize(14)
        textFieldInsideUISearchBar?.font = AppFonts.Galano_Regular.withSize(14)
        self.searchBar.placeholder = LocalizedString.Search.localized
    }
}
//MARK: TextField Delegates
/*-----------------------*/
extension CountryCodeVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        
        let filter = self.countryInfo.filter({ ($0["CountryEnglishName"] as? String ?? "").localizedCaseInsensitiveContains(searchText)
        })
        self.filteredCountryList = filter
        
        if searchText.isEmpty{
            self.filteredCountryList = self.countryInfo
        }
        self.countryCodeTableView.reloadData()
    }
}

//MARK: CollectionView DataSource and Delegates
/*--------------------------------------------*/
extension CountryCodeVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredCountryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeCell", for: indexPath) as! CountryCodeCell
        
        let info = self.filteredCountryList[indexPath.row]
        
        cell.populateView(info: info)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let info = self.filteredCountryList[indexPath.row]
        
        self.delegate.setCountryCode(country_info: info)
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}

class CountryCodeCell: UITableViewCell {
    
    //MARK: IBOutlets
    /*-------------*/
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryCode: UILabel!
    
    //MARK: Cell Life Cycle
    /*-------------------*/
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.clear
        countryCode.textColor = AppColors.blackLblColor
        countryCode.font = AppFonts.Galano_Regular.withSize(14)
    }
    
    func populateView(info: JSONDictionary){
        printDebug(info)
        guard let code = info["CountryCode"] else{return}
        guard let name = info["CountryEnglishName"] as? String else{return}
        self.countryCode.text = "\(name)(+\(code))"
        guard let img = info["CountryFlag"] as? String else{return}
        self.countryFlag.image = UIImage(named: img)
    }
}
