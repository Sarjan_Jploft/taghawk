//
//  ProductOptionsView.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 25/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ProductOptionsViewDelegate: AnyObject {
    func promoteBtnTapped(_ sender: UIButton)
    func editBtnTapped(_ sender: UIButton)
    func deleteBtnTapped(_ sender: UIButton)
}

class ProductOptionsView: UIView {

    weak var delegate: ProductOptionsViewDelegate?
    
    @IBOutlet weak var promoteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
    @IBAction func promoteBtnAction(_ sender: UIButton) {
        delegate?.promoteBtnTapped(sender)
    }
    
    @IBAction func editBtnAction(_ sender: UIButton) {
        delegate?.editBtnTapped(sender)
    }
    
    @IBAction func deleteBtnAction(_ sender: UIButton) {
        delegate?.deleteBtnTapped(sender)
    }
    
}
