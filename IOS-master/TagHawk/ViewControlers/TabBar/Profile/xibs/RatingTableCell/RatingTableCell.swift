//
//  RatingTableCell.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 27/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol RatingTableCellDelegate: AnyObject {
    func editReplyBtnTapped(_ sender: UIButton, tableCell: RatingTableCell)
}

class RatingTableCell: UITableViewCell {

    // MARK:- VARIABLES
    //====================
    
    enum SectionType: String {
        case reply = "Reply"
        case post = "Post"
        case edit = "Edit"
        case update = "Update"
    }
    
    var delegate: RatingTableCellDelegate?
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var ratingsView: FloatRatingView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var reviewLbl: UILabel!
    @IBOutlet weak var replyMainView: UIView!
    @IBOutlet weak var ownerNameReplyView: UIView!
    @IBOutlet weak var ownerNameLbl: UILabel!
    @IBOutlet weak var ownerReplyTimeLbl: UILabel!
    @IBOutlet weak var editReplyBtn: UIButton!
    @IBOutlet weak var replySectionStackView: UIStackView!
    @IBOutlet weak var replyTextView: UITextView!
    @IBOutlet weak var textCountLbl: UILabel!
    
    // MARK:- IBACTIONS
    //====================
    
    @IBAction func editReplyBtnAction(_ sender: UIButton) {

        delegate?.editReplyBtnTapped(sender, tableCell: self)
    }
    
    // MARK:- LIFE CYCLE
    //====================
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetup()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        userImgView.round()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // MARK:- FUNCTIONS
    //====================
    
    
    ///Initial Setup
    private func initialSetup() {
        self.mainView.addShadow(radius: 15, opacity: 0.1)
        ratingsView.editable = false
        ratingsView.type = .halfRatings
        replyTextView.delegate = self
        replyTextView.textColor = #colorLiteral(red: 0.07843137255, green: 0.07843137255, blue: 0.07843137255, alpha: 1)
        replyTextView.font = AppFonts.Galano_Light.withSize(13)
    }
}

extension RatingTableCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text {
            textCountLbl.text = "\(text.count)/300"
            if text.count > 300 {
                textCountLbl.text = "300/300"
                textView.text.removeLast()
            }
        }
        
    }
}
