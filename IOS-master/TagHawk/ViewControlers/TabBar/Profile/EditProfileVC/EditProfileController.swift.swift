//
//  EditProfileController.swift
//  TagHawk
//
//  Created by Appinventiv on 15/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

protocol EditProfileControllerDelegate: class {
    func willRequestUpdateProfile()
    func updateProfileSuccessfully(updatedFor: UpdatedProfile, msg: String, userData : UserProfile)
    func failedToUpdateProfile(message : String)
}

protocol VerifyOtpDelegate: AnyObject {
    func otpVerifySuccess(msg: String)
    func otpVerifyFailed(msg: String)
}

class EditProfileController {
    
    weak var delegate: EditProfileControllerDelegate?
    weak var otpDelegate: VerifyOtpDelegate?
    
    func updateProfile(userData : UserProfile, status: UpdatedProfile,
                       emailPhoneNumber: String = "",
                       countryCode: String = "",
                       facebookId: String = "",
                       officialIds: [String] = [],
                       fullName: String = "",
                       imageURl: String = "",
                       dob : String = "",
                       ssn : String = "",
                       firstName : String = "",
                       lastName : String = "",
                       documentReference : String = "",
                       addressInput : AddAddressVC.AddressInput = AddAddressVC.AddressInput()) {
        
        var params: JSONDictionary = ["status": status.rawValue > 4 ? 0 : status.rawValue]
        
        if status == .address{
            params["status"] = 5
        }
        
        switch status {
            
        case .profile:
            
            params["fullName"] = "\(firstName) \(lastName)"
            if !imageURl.isEmpty{
               params["profilePicture"] = imageURl
            }
            
            if !firstName.isEmpty{
                params[ApiKey.firstName] = firstName
            }
            
            if !lastName.isEmpty{
                params[ApiKey.lastName] = lastName
            }
            
        case .document:
//            params["documents"] = officialIds.joined(separator: ",")
            params[ApiKey.documentReferenceId] = documentReference

        case .email:
            params["email"] = emailPhoneNumber
        case .phone:
            params["countryCode"] = countryCode
            params["phoneNumber"] = emailPhoneNumber
        case .facebook:
            params["facebookId"] = facebookId
            
        case .ssn:
             params[ApiKey.ssnNumber] = ssn
        
        case .dob:
            params[ApiKey.dob] = dob
            
        case .address:
            params[ApiKey.line1] = addressInput.addressLine1
            if !addressInput.addressLine2.isEmpty{ params[ApiKey.line2] = addressInput.addressLine2 }
            params[ApiKey.postalCode] = addressInput.zipCode
            params[ApiKey.city] = addressInput.city
            params[ApiKey.state] = addressInput.state

        }
        
        delegate?.willRequestUpdateProfile()
        
        WebServices.updateProfile(parameters: params,
                                  success: {[weak self](json) in
                                    
        var updatedProfile = userData
                             
            switch status{
            
            case .ssn:
                updatedProfile.ssnNumber = ssn
                
            case .email:
                updatedProfile.email = emailPhoneNumber

            case .profile:
                updatedProfile.profilePicture = imageURl
                updatedProfile.firstName = firstName
                updatedProfile.lastName = lastName
                updatedProfile.fullName = firstName + " " + lastName
            case .phone:
               updatedProfile.phoneNumber = emailPhoneNumber
               updatedProfile.countryCode = countryCode

            case .address:
                updatedProfile.line1 = addressInput.addressLine1
                updatedProfile.line2 = addressInput.addressLine2
                updatedProfile.city = addressInput.city
                updatedProfile.state = addressInput.state
                updatedProfile.zipCode = addressInput.zipCode

            default:
                printDebug("default")
                
            }
                                    
        self?.delegate?.updateProfileSuccessfully(updatedFor: status,
                                                  msg: json[ApiKey.message].stringValue,
                                                  userData: updatedProfile)
        }) {[weak self](error) -> (Void) in
            self?.delegate?.failedToUpdateProfile(message: error.localizedDescription)
        }
    }
    
    func verifyOtp(otp: String) {
        
        let params: JSONDictionary = [ApiKey.otp: otp]
        
        WebServices.verifyOtp(parameters: params, success: { (json) in
            self.otpDelegate?.otpVerifySuccess(msg: json[ApiKey.message].stringValue)
        }) { (err) -> (Void) in
            self.otpDelegate?.otpVerifyFailed(msg: err.localizedDescription)
        }
    }
}
