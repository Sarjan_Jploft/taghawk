//
//  EditProfileVC.swift
//  TagHawk
//
//  Created by Vikash on 07/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Netverify


enum UpdatedProfile: Int {
    case profile = 0
    case facebook = 1
    case email = 2
    case phone = 3
    case document = 4
    case dob = 5
    case ssn = 6
    case address = 7
}


protocol ProfileUpdatedDelegate : class {
    func profileUpdated(userProfile : UserProfile)
}


class EditProfileVC: BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var firstNameTextfield: CustomTextField!
    @IBOutlet weak var lastNameTextfield: CustomTextField!
    @IBOutlet var verifySocialProfileButton: [UIButton]!
    @IBOutlet weak var isVerifiedUserImage: UIImageView!
    @IBOutlet weak var VerifyAccountLbl: UILabel!
    @IBOutlet var verifyAccTypeTitle: [UILabel]!
    @IBOutlet var verifyAccDescription: [UILabel]!
    
    //MARK:- Variables
    var userData: UserProfile!
    private let socialLoginController = SocialLoginController()
    let controller = EditProfileController()
    weak var delegate : ProfileUpdatedDelegate?
    private var isPictureChanged = false
    var netverifyViewController:NetverifyViewController?
    var customUIController:NetverifyUIController?
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.doInitialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImageView.round()
    }
    
    //MARK:- Do initial set up
    private func doInitialSetup() {
        self.profileImageView.setImage_kf(imageString: self.userData.profilePicture, placeHolderImage: UIImage(named: "icTabProfileUnactive")!)
        
        if self.userData.profilePicture.isEmpty {
            self.profileImageView.addNameStartingCredential(name: userData.fullName)
        }
        
        self.firstNameTextfield.text = self.userData.firstName
        self.firstNameTextfield.placeholder = LocalizedString.firstName.localized
        self.firstNameTextfield.delegate = self
        self.firstNameTextfield.keyboardType = .default
        self.firstNameTextfield.returnKeyType = .done
        self.lastNameTextfield.placeholder = LocalizedString.lastName.localized
        self.lastNameTextfield.delegate = self
        
        self.lastNameTextfield.text = self.userData.lastName
        self.lastNameTextfield.keyboardType = .default
        self.lastNameTextfield.returnKeyType = .done
        self.socialLoginController.delegate = self
        controller.delegate = self
        verifyAccDescription[1].text = self.userData.email
        verifyAccDescription[2].text = self.userData.countryCode + "-" + self.userData.phoneNumber
        verifyAccDescription[3].text = "\(self.userData.city), \(self.userData.state)"
        self.verifyAccDescription[3].text = "\(UserProfile.main.line1), \(UserProfile.main.line2), \(UserProfile.main.city), \(UserProfile.main.state), \(UserProfile.main.zipCode)"
        
        if self.userData.profileCompleted == 1 {
            self.isVerifiedUserImage.image = UIImage(named: "icVerifiedSheild")
        } else {
            self.isVerifiedUserImage.image = UIImage(named: "icEditShield")
        }
        
        if !self.userData.isFacebookLogin {
            self.verifySocialProfileButton[0].backgroundColor = AppColors.appBlueColor
            self.verifySocialProfileButton[0].setTitleColor(.white)
            self.verifySocialProfileButton[0].setTitle(LocalizedString.Verify.localized)
        } else {
            self.verifySocialProfileButton[0].backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1)
            self.verifySocialProfileButton[0].setTitleColor(.white)
            self.verifySocialProfileButton[0].setTitle("Verified")
        }
        
        if self.userData.email.isEmpty{
            self.verifySocialProfileButton[1].backgroundColor = AppColors.appBlueColor
            self.verifySocialProfileButton[1].setTitleColor(.white)
            self.verifySocialProfileButton[1].setTitle("Add")
        }else if !self.userData.isEmailVerified {
            self.verifySocialProfileButton[1].backgroundColor = AppColors.appBlueColor
            self.verifySocialProfileButton[1].setTitleColor(.white)
            self.verifySocialProfileButton[1].setTitle("Verify")
        } else {
            self.verifySocialProfileButton[1].backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1)
            self.verifySocialProfileButton[1].setTitleColor(.white)
            self.verifySocialProfileButton[1].setTitle("Verified")
        }
        
        if self.userData.phoneNumber.isEmpty{
            self.verifySocialProfileButton[2].backgroundColor = AppColors.appBlueColor
            self.verifySocialProfileButton[2].setTitleColor(.white)
            self.verifySocialProfileButton[2].setTitle("Add")
        }else if !self.userData.isPhoneVerified {
            self.verifySocialProfileButton[2].backgroundColor = AppColors.appBlueColor
            self.verifySocialProfileButton[2].setTitleColor(.white)
            self.verifySocialProfileButton[2].setTitle("Verify")
        } else {
            self.verifySocialProfileButton[2].backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1)
            self.verifySocialProfileButton[2].setTitleColor(.white)
            self.verifySocialProfileButton[2].setTitle("Verified")
        }
        
        if !self.userData.isDocumentsVerified {
            self.verifySocialProfileButton[3].backgroundColor = AppColors.appBlueColor
            self.verifySocialProfileButton[3].setTitleColor(.white)
            self.verifySocialProfileButton[3].setTitle("Verify")
        } else {
            self.verifySocialProfileButton[3].backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1)
            self.verifySocialProfileButton[3].setTitleColor(.white)
            self.verifySocialProfileButton[3].setTitle("Verified")
        }
        
        if self.userData.city.isEmpty{
            self.verifySocialProfileButton[4].backgroundColor = AppColors.appBlueColor
            self.verifySocialProfileButton[4].setTitleColor(.white)
            self.verifySocialProfileButton[4].setTitle("Add")
        }else{
            self.verifySocialProfileButton[4].backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1)
            self.verifySocialProfileButton[4].setTitleColor(.white)
            self.verifySocialProfileButton[4].setTitle("Edit")
        }
        
        for (index, lbl) in verifyAccTypeTitle.enumerated() {
            lbl.font = AppFonts.Galano_Medium.withSize(12)
            lbl.textColor = AppColors.blackLblColor
            switch index{
                
            case 0: lbl.text = LocalizedString.facebook.localized
                
            case 1: lbl.text = LocalizedString.email.localized
                
            case 2: lbl.text = LocalizedString.phone.localized
                
            case 3: lbl.text = LocalizedString.officialID.localized
                
            default: break
            }
        }
        
        verifyAccDescription[0].text = LocalizedString.signupWithFb.localized
        for lbl in verifyAccDescription {
            lbl.font = AppFonts.Galano_Regular.withSize(11)
            lbl.textColor = AppColors.lightGreyTextColor
        }
    }
    
    //MARK: Upload image
    func uploadLogoImageToAWS(image: UIImage) {
        
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        DispatchQueue.main.async {
            self.view.showIndicator()
        }
        
        image.uploadImageToS3(imageIndex: 0, success: { (imageIndex, success, imageUrl) in
            self.userData.profilePicture = imageUrl
            self.updateProfile()
            self.view.hideIndicator()
        }, progress: { (imageIndex, progress) in
            
        }, failure: { (imageindex, error) in
            CommonFunctions.showToastMessage(error.localizedDescription)
            self.view.hideIndicator()
        })
    }
    
    //MARK:- Cancel button tapped
    @IBAction func clickCancelButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Edit profile image tapped
    @IBAction func clickEditProfileImage(_ sender: UIButton) {
        self.openActionSheetWith(arrOptions: [AppConstants.camera.rawValue, AppConstants.photoLibrary.rawValue], openIn: self) { (actionIndex) in
            
            switch actionIndex {
            case 0:
                self.checkAndOpenCamera(delegate: self)
            case 1:
                self.checkAndOpenLibrary(delegate: self)
            default:
                break;
            }
        }
    }
    
    //MARK:- verify social media button tapped
    @IBAction func clickVerifySocialMediaButton(_ sender: UIButton) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if sender.tag == SocialMedia.facebook.rawValue, !self.userData.isFacebookLogin {
            self.socialLoginController.fbLogin()
        }
        if sender.tag == SocialMedia.document.rawValue, !self.userData.isDocumentsVerified {
            self.showPopup(type: CustomPopUpVC.CustomPopUpFor.documentVerify)
        }
        
        if sender.tag == SocialMedia.phoneNo.rawValue, !self.userData.isPhoneVerified {
            let phoneVerifyVC = CustomPhoneVerifyPopUpVC.instantiate(fromAppStoryboard: .Home)
            phoneVerifyVC.instantiated = .phoneNumber
            phoneVerifyVC.userData = self.userData
            phoneVerifyVC.delegate = self
            phoneVerifyVC.modalTransitionStyle = .coverVertical
            phoneVerifyVC.modalPresentationStyle = .overCurrentContext
            self.present(phoneVerifyVC, animated: true, completion: nil)
        }
        
        if sender.tag == SocialMedia.email.rawValue, !self.userData.isEmailVerified {
            if !userData.email.isEmpty {
                controller.updateProfile(userData: self.userData, status: .email,emailPhoneNumber: userData.email)
            } else {
                let phoneVerifyVC = CustomPhoneVerifyPopUpVC.instantiate(fromAppStoryboard: .Home)
                phoneVerifyVC.userData = self.userData
                phoneVerifyVC.instantiated = .email
                phoneVerifyVC.delegate = self
                phoneVerifyVC.modalTransitionStyle = .coverVertical
                phoneVerifyVC.modalPresentationStyle = .overCurrentContext
                self.present(phoneVerifyVC, animated: true, completion: nil)
            }
        }
        
        if sender.tag == SocialMedia.address.rawValue, !self.userData.isEmailVerified {
            let vc = AddAddressVC.instantiate(fromAppStoryboard: AppStoryboard.PaymentAndShipping)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK:- Update profile button tapped
    @IBAction func clickUpdateProfileButton(_ sender: UIButton) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if isPictureChanged {
            if let image = profileImageView.image {
                uploadLogoImageToAWS(image: image)
            }
        } else {
            updateProfile()
        }
    }
    
    @IBAction func addEditDobButtonTapped(_ sender: UIButton) {
        
    }
    
    //MARK:- Add or edit ssn tapped
    @IBAction func addEditSssButtonTapped(_ sender: UIButton) {
        let phoneVerifyVC = CustomPhoneVerifyPopUpVC.instantiate(fromAppStoryboard: .Home)
        phoneVerifyVC.userData = self.userData
        phoneVerifyVC.instantiated = .ssn
        phoneVerifyVC.delegate = self
        phoneVerifyVC.modalTransitionStyle = .coverVertical
        phoneVerifyVC.modalPresentationStyle = .overCurrentContext
        self.present(phoneVerifyVC, animated: true, completion: nil)
    }
}


extension EditProfileVC : AddressDetailAdded {
    
    func addressDetailsAdded(){
        self.verifyAccDescription[3].text = !UserProfile.main.city.isEmpty ? "\(UserProfile.main.line1), \(UserProfile.main.line2), \(UserProfile.main.city), \(UserProfile.main.state), \(UserProfile.main.zipCode)" : ""
        self.verifySocialProfileButton[4].backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1)
        self.verifySocialProfileButton[4].setTitleColor(.white)
        self.verifySocialProfileButton[4].setTitle("Edit")
    }
    
}

// MARK:- Functions
extension EditProfileVC {
    
    //MARK:- Show otp verification vc
    private func showOtpVerificationVC() {
        let otpScene = VerifyOtpPopupVC.instantiate(fromAppStoryboard: .Profile)
        otpScene.phoneNumberVerify = { [weak self] in
            guard let `self` = self else { return }
            self.verifyAccDescription[2].text = self.userData.countryCode + "-" + self.userData.phoneNumber
            UserProfile.main.countryCode = self.userData.countryCode
            UserProfile.main.phoneNumber = self.userData.phoneNumber
            UserProfile.main.isPhoneVerified = true
            UserProfile.main.saveToUserDefaults()
            self.verifySocialProfileButton[2].backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1)
            self.verifySocialProfileButton[2].setTitleColor(.white)
            self.verifySocialProfileButton[2].setTitle("Verified")
        }
        otpScene.modalPresentationStyle = .overCurrentContext
        self.present(otpScene, animated: true, completion: nil)
    }
    
    //MARK:- update profile
    private func updateProfile() {
        self.controller.updateProfile(userData: self.userData, status: UpdatedProfile.profile, imageURl: self.userData.profilePicture, firstName: self.firstNameTextfield.text ?? "", lastName: self.lastNameTextfield.text ?? "")
    }
}

//MARK:- Textview and imagepicker delegates
extension EditProfileVC: UITextFieldDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[.originalImage] as? UIImage {
            DispatchQueue.main.async {
                self.profileImageView.image = img
                self.profileImageView.removeNameStringCredential()
            }
            self.isPictureChanged = true
            self.profileImageView.round()
            self.view.layoutIfNeeded()
            picker.dismiss(animated: true) {
                
                //                self.uploadLogoImageToAWS(image: img)
            }
        }
    }
    
    func validateInput(isValid: Bool, message: String) {
        print("message", message)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let inputMode = textField.textInputMode else {
            return false
        }
        if inputMode.primaryLanguage == "emoji" || !(inputMode.primaryLanguage != nil) {
            return false
        } else if string == " " {
            return false
        }
        
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                return true
            }
        }
        
        if !string.isAllowed(validationStr: AllowedCharactersValidation.name){
            return false
        }
        
        if let text = textField.text {
            if text.count > 20 {
                return false
            }
        }
        return true
    }
}

//MARK:- webservices call back
extension EditProfileVC : SocialLoginControllerDelegate {
    
    func willLoginSocialAccount(){
        
    }
    
    func socialLoginSuccess(email: String, socialId: String,profilePicture : String, name : String ,socialType: SocialLoginController.LoginType, firstName: String, lastName: String){
        controller.updateProfile(userData: self.userData, status: .facebook,
                                 facebookId: socialId)
    }
    
    func socialLoginFailed(message: String){
        CommonFunctions.showToastMessage(message)
    }
    
    func userNotRegistered(){
        
    }
}

extension EditProfileVC: CustomPhoneVerifyPopUpDelegate {
    
    func doneBtnTapped(instantaited: CustomPhoneVerifyPopUpVCInstantiatedFor,
                       updatedValue : String,
                       countryCode: String) {
        
        switch instantaited {
            
        case .email:
            
            controller.updateProfile(userData: self.userData, status: .email,emailPhoneNumber: updatedValue)
            
        case .phoneNumber:
            
            controller.updateProfile(userData: self.userData,status: .phone, emailPhoneNumber: updatedValue, countryCode: countryCode)
            
        case .ssn:
            
            controller.updateProfile(userData: self.userData,status: UpdatedProfile.ssn, ssn: updatedValue)
            
        }
    }
}

extension EditProfileVC: EditProfileControllerDelegate {
    
    func willRequestUpdateProfile() {
        self.view.showIndicator()
    }
    
    func updateProfileSuccessfully(updatedFor: UpdatedProfile, msg: String, userData : UserProfile) {
        self.view.hideIndicator()
        self.userData = userData
        
        switch updatedFor {
            
        case .email:
            self.userData.email = userData.email
            UserProfile.main.email = userData.email
            UserProfile.main.saveToUserDefaults()
            verifyAccDescription[1].text = userData.email
            self.verifySocialProfileButton[1].setTitle("Verify")
            showPopUp(type: .emailVerified, msg: msg)
            
        case .phone:
            self.userData.countryCode = userData.countryCode
            self.userData.phoneNumber = userData.phoneNumber
            showOtpVerificationVC()
            
        case .document:
            CommonFunctions.showToastMessage(msg)
            
        case .profile:
            UserProfile.main.fullName = userData.fullName
            UserProfile.main.firstName = userData.firstName
            UserProfile.main.lastName = userData.lastName
            self.dismiss(animated: true, completion: nil)
            
        case .address:
            UserProfile.main.line1 = userData.line1
            UserProfile.main.line2 = userData.line2
            UserProfile.main.city = userData.city
            UserProfile.main.state = userData.state
            UserProfile.main.zipCode = userData.zipCode
            
        default: break
            
        }
        
        CommonFunctions.showToastMessage(msg)
        self.delegate?.profileUpdated(userProfile: self.userData)
    }
    
    func failedToUpdateProfile(message: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
}
