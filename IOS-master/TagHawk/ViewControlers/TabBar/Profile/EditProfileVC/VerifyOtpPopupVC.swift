//
//  VerifyOtpPopupVC.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 01/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class VerifyOtpPopupVC: BaseVC {
    
    //MARK:- PROPERTIES
    //=================
    
    private let controller = EditProfileController()
    var phoneNumberVerify: (() -> ())?
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popupView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    //MARK:- TARGET ACTIONS
    //=====================
 
    //MARK:- Cancel button action
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Done button tapped
    @IBAction func doneBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if let text = otpTextField.text {
            if text.count == 4 {
                controller.verifyOtp(otp: text)
            } else {
                CommonFunctions.showToastMessage(LocalizedString.enterValidOtp.localized)
            }
        }
    }
}

//MARK:- PRIVATE FUNCTIONS
//========================
extension VerifyOtpPopupVC {
    
    //MARK:- Set up view
    private func initialSetup(){
        controller.otpDelegate = self
        otpTextField.addTarget(self, action: #selector(textFiledDidChange(_:)), for: .editingChanged)
        otpTextField.keyboardType = .numberPad
        self.headingLabel.setAttributes(text: LocalizedString.OtpVerify.localized, font: AppFonts.Galano_Semi_Bold.withSize(17.5), textColor: UIColor.black)
        self.descriptionLabel.setAttributes(text: LocalizedString.enterVerificationCode.localized,font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
        self.dismissView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.backgroundColor = UIColor.clear
        
        self.doneBtn.round(radius: 10)
        self.cancelBtn.round(radius: 10)
        self.doneBtn.round(radius: 10)
        self.cancelBtn.backgroundColor = UIColor.white
        self.cancelBtn.setBorder(width: 0.5, color: AppColors.blackBorder)
        self.cancelBtn.setAttributes(title: LocalizedString.cancel.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.black, backgroundColor: UIColor.white)
        self.doneBtn.setAttributes(title: LocalizedString.Verify.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
    }
    
    private func dismisViewTapped(){
        self.dismiss(animated: true, completion: nil)
    }
}


extension VerifyOtpPopupVC: UITextFieldDelegate {
    //MARK:- Textfield did change
    @objc func textFiledDidChange(_ textField: UITextField) {
        if textField == otpTextField {
            if let text = textField.text {
                if text.count > 4 {
                    textField.text?.removeLast()
                }
            }
        }
    }
}


//MARK:- Verify otp delegates
extension VerifyOtpPopupVC: VerifyOtpDelegate {
   
    func otpVerifySuccess(msg: String) {
        dismiss(animated: true, completion: nil)
        phoneNumberVerify?()
        CommonFunctions.showToastMessage(msg)
    }
    
    func otpVerifyFailed(msg: String) {
        CommonFunctions.showToastMessage(msg)
    }
}
