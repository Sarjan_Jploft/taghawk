//
//  FollowingFollowersVC.swift
//  TagHawk
//
//  Created by Appinventiv on 20/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

class FollowingFollowersVC : BaseVC {

    enum FollowingFollowersListFor {
        case followers
        case following
        case othersFollowers
        case othersFollowing
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var followingFollowersTableView: UITableView!
    
    //MARK:- variables
    var commingFor = FollowingFollowersListFor.followers
    var users : [FollowingFollowersModel] = []
    private let dropDown = DropDown()
    private let controler = FollowingFollowersControler()
    var userId : String = ""
    let blockControler = BlockedUserControler()

    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    
    override func bindControler() {
        super.bindControler()
        self.controler.followListDelegate = self
        self.controler.followUserDelegate = self
        self.controler.removeUserdelegate = self
        self.blockControler.blockUserdelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }
}

extension FollowingFollowersVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.configureTableView()
        self.requestData()
    }
    
    //MARK:- Request data
    func requestData(){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        switch self.commingFor {
        case .followers, .following:
            self.controler.getFollowingFollowersList(commingFor: self.commingFor, page: page)
        default:
            self.controler.getFollowingFollowersList(userId: self.userId, commingFor: self.commingFor, page: page)
        }
        
    }
    
    //MARK:- Configure tableview
    func configureTableView(){
        self.followingFollowersTableView.registerNib(nibName: FollowersCell.defaultReuseIdentifier)
        self.followingFollowersTableView.separatorStyle = .none
        self.followingFollowersTableView.showsVerticalScrollIndicator = false
        self.configureEmptyDataSet()
        self.followingFollowersTableView.delegate = self
        self.followingFollowersTableView.dataSource = self
    }
    
    //MARK:- Configure empty data set
    func configureEmptyDataSet(){
        self.setEmptyDataView(heading: LocalizedString.Oops_It_Is_Empty.localized, description: "", image: AppImages.noData.image)
        
        self.setEmptyDataViewAttributes(titleFont: AppFonts.Galano_Semi_Bold.withSize(15), descriptionFont: AppFonts.Galano_Regular.withSize(12), spacingInBetween: 10)
        
        self.setDataSourceAndDelegate(forScrollView: self.followingFollowersTableView)
    }
    
    //MARK:- Follow Unfollow button tapped
    @objc func followUnFollowButtonTapped(sender : UIButton){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        guard let indexPath = sender.tableViewIndexPath(self.followingFollowersTableView) else { return }
        
        self.users[indexPath.row].isFollowing ? self.showPopUp(user: self.users[indexPath.row], type: RemoveFriendPopUp.RemoveFriendFor.unfollow) : self.controler.followUser(user: self.users[indexPath.row].userId)
    }
    
    //MARK:- Show popup
    func showPopUp(user : FollowingFollowersModel, type : RemoveFriendPopUp.RemoveFriendFor){
        let vc = RemoveFriendPopUp.instantiate(fromAppStoryboard: AppStoryboard.Profile)
        vc.commingFor = type
        vc.user = user
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Menu button tapped
   @objc func menuButtonTapped(sender : UIButton){
  
    guard let indexPath = sender.tableViewIndexPath(self.followingFollowersTableView) else { return }
    
    self.dropDown.anchorView = sender
    self.dropDown.dismissMode = .onTap
    self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
    self.dropDown.dismissMode = .automatic
    self.dropDown.dataSource = [LocalizedString.Remove.localized,LocalizedString.Block.localized]
    self.dropDown.show()
    self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
        guard let weakSelf = self else { return }
       let popUpFor = index == 0 ? RemoveFriendPopUp.RemoveFriendFor.removeFollower : RemoveFriendPopUp.RemoveFriendFor.block
       weakSelf.showPopUp(user: weakSelf.users[indexPath.row], type: popUpFor)
      }
    }
}

//MARK:- Configure Navigation
extension FollowingFollowersVC {
 
    func configureNavigation(){
        let title = self.commingFor == .followers ? LocalizedString.Followers.localized : LocalizedString.Following.localized
        self.navigationItem.title = title
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
}


//MARK:- TableView Datasource and Delegate
extension FollowingFollowersVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FollowersCell.defaultReuseIdentifier) as? FollowersCell else {
            fatalError("FollowersCell....\(FollowersCell.defaultReuseIdentifier) cell not found")}
        cell.populateData(user: self.users[indexPath.row])
        cell.followUnfollowButton.addTarget(self, action: #selector(followUnFollowButtonTapped), for: UIControl.Event.touchUpInside)
        cell.menuButton.addTarget(self, action: #selector(menuButtonTapped), for: UIControl.Event.touchUpInside)
        cell.menuButton(isHidden: self.commingFor != .followers)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userProfileScene = OtherUserProfileVC.instantiate(fromAppStoryboard: .Profile)
        userProfileScene.userId = self.users[indexPath.row].userId
        navigationController?.pushViewController(userProfileScene, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if users.count >= 9  {
            if indexPath.item == self.users.count - 1 && self.nextPage != 0{
                requestData()
            }
        }
    }
}


//MARK:- Webservices

//MARK:- Following followers callback
extension FollowingFollowersVC : FollowingFollowersListDelegate {

    func willRequestFollowersList(){
        self.view.showIndicator()
    }
    
    func followersFollowingReceivedSuccessFully(users : [FollowingFollowersModel], total : Int){
        
        if self.page == 1{
            self.users.removeAll()
            self.followingFollowersTableView.reloadData()
        }
        
        if nextPage != 0{
            self.page = nextPage
        }
        
        self.view.hideIndicator()
        self.users.append(contentsOf: users)
        self.followingFollowersTableView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.followingFollowersTableView.reloadEmptyDataSet()
    }
    
    func failedToReceiveFollowingFollowers(message : String){
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = true
        self.followingFollowersTableView.reloadEmptyDataSet()
    }
    
}

extension FollowingFollowersVC : FollowUserDelegate {
    
    func willFollowUser(){
        self.view.showIndicator()
        
    }
    
    func followUserSuccessFully(userId : String){
        self.view.hideIndicator()

          if let ind = self.users.index(where: { $0.userId == userId }) {
         
            self.users[ind].isFollowing = !self.users[ind].isFollowing
            
            self.followingFollowersTableView.reloadRows(at: [IndexPath(row: ind, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
      func failedToFollowUser(message: String) {
        self.view.hideIndicator()
        
    }
    
}

//MARK:- Remove followers delegate
extension FollowingFollowersVC : RemoveFollowerDelegate {
    
    func willRequestRemoveUser() {
        self.view.showIndicator()
    }
    
    func removeUserSuccessfull(user: String) {
        self.view.hideIndicator()
        
        if let ind = self.users.index(where: { $0.userId == user }) {
            self.users.remove(at: ind)
            self.followingFollowersTableView.reloadData()
            
            if self.users.isEmpty{
                self.shouldDisplayEmptyDataView = true
                self.followingFollowersTableView.reloadEmptyDataSet()
            }
        }
    }
    
    func failedToRemoveUser(message: String) {
        self.view.hideIndicator()

    }
    
}

//MARK:- Block user delegate
extension FollowingFollowersVC : BlockUserDelegate {
    
    func willRequestBlockUser(){
        self.view.showIndicator()
    }
    
    func blockUserSuccessfull(user : String){
        self.view.hideIndicator()
        SingleChatFireBaseController.shared.blockedUser(userID: UserProfile.main.userId, otherUserID: user)
        
        if let ind = self.users.index(where: { $0.userId == user }) {
            self.users.remove(at: ind)
            self.followingFollowersTableView.reloadData()
            
            if self.users.isEmpty{
                self.shouldDisplayEmptyDataView = true
                self.followingFollowersTableView.reloadEmptyDataSet()
            }
        }
    }
    
    func failedToBlockUser(message : String){
        self.view.hideIndicator()
        
    }
    
}


//MARK:- Remove friend popup
extension FollowingFollowersVC : RemoveFriendPopUpDelegate{
   
    func noButtonTapped(type: RemoveFriendPopUp.RemoveFriendFor) {
        
    }
    
    func yesButtonTapped(type: RemoveFriendPopUp.RemoveFriendFor, user: FollowingFollowersModel) {
        
        if !AppNetworking.isConnectedToInternet {CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        switch  type {
            case .unfollow:
                self.controler.unfollowUser(user: user.userId)
            
        case .removeFollower:
            self.controler.removeFollower(user: user.userId)
            
        case .block:
            self.blockControler.blockUser(user: user.userId)
            
        default:
            break
        }
    }
}
