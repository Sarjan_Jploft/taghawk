//
//  CameraVCViewController.swift
//  TagHawk
//
//  Created by Appinventiv on 21/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import AVFoundation

protocol GetCapturedImages : class {
    func getCapturedImagesBack(images : [(index : Int, stringUrl : String, img : UIImage?)])
}

class CameraVC : BaseVC {
    
    enum CameraFrom {
        case addProduct
        case tabBar
        case addTag
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var nextButton : UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var flipCameraButton: UIButton!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var cameraViewTop: NSLayoutConstraint!
    
    //MARK:- Variables
    var camera : LLSimpleCamera?
    var capturedImages : [(index : Int, stringUrl : String, img : UIImage?)] = []
    weak var delegate : GetCapturedImages?
    var cameraFor = CameraFrom.tabBar
    private var picker: YPImagePicker?
    weak var addProductSuccessFullDelegate : ProductCreatedSuccessFully?
    
    //MARK:- view life cycld
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
        checkCameraAccess()
        AppDelegate.shared.applicationDidBecomeActive = {
            self.checkCameraAccess()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.captureButton.round()
    }
    
    //MARK:- Close button tapped
    @IBAction func closeButton(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Capture button tapped
    @IBAction func captureButton(_ sender: UIButton) {
        view.endEditing(true)
        self.capturePhoto()
    }
    
    //MARK:- Capture button tapped
    @IBAction func flashButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if self.camera?.flash == LLCameraFlashOn{
            self.camera?.updateFlashMode(LLCameraFlashOff)
            self.flashButton.setImage(AppImages.flashOff.image, for: UIControl.State.normal)
        }else{
            self.camera?.updateFlashMode(LLCameraFlashOn)
            self.flashButton.setImage(AppImages.flashOn.image, for: UIControl.State.normal)
        }
    }
    
    //MARK:- Next button tapped
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if self.cameraFor == .tabBar{
            if self.capturedImages.isEmpty {
                CommonFunctions.showToastMessage(LocalizedString.Please_Click_Or_Select_An_Image.localized)
                return }
            let vc = AddProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.addProductData.productImages = self.capturedImages
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.delegate?.getCapturedImagesBack(images: self.capturedImages)
            self.dismiss(animated: true) {
            }
        }
    }
    
    //MARK:- Flip camera button tapped
    @IBAction func flipCameraButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.camera?.togglePosition()
        if self.camera?.position == LLCameraPositionRear{
            self.flashButton.isHidden = false
        }else{
            self.flashButton.isHidden = true
            self.camera?.updateFlashMode(LLCameraFlashOff)
            self.flashButton.setImage(AppImages.flashOn.image, for: UIControl.State.normal)
        }
    }
    
    //MARK:- Gallery button tapped
    @IBAction func galleryButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.openPicker()
    }
}

extension CameraVC {
    
    //AMRK:- Set up view
    func setUpSubView(){
        self.captureButton.round()
        self.captureButton.setBorder(width: 3, color: AppColors.lightGreyTextColor)
        self.nextButton.setAttributes(title: LocalizedString.Next.localized, font: AppFonts.Galano_Bold.withSize(13), titleColor: AppColors.appBlueColor)
        DispatchQueue.main.async {
            self.configureCamera()
        }
        self.configureCollectionView()
    }
    
    //MARK:- Set camera view top
    func setCameraViewTop(){
        
        switch Display.typeIsLike {
        case .iphone6:
            self.cameraViewTop.constant = 75
        case .iphoneX:
            self.cameraViewTop.constant = (screenHeight / 2) - (self.cameraView.frame.height / 2)
            
        default:
            self.cameraViewTop.constant = 75
        }
        
        self.cameraViewTop.constant = 200
        
    }
    
    @objc func imageViewTapped(){
        self.camera?.view.isHidden = false
    }
    
    //MARK:- Configure camera
    func configureCamera(){
        self.camera = LLSimpleCamera(quality: AVCaptureSession.Preset.high.rawValue, position: LLCameraPositionRear, videoEnabled: false)
        self.camera?.attach(to: self, withFrame: CGRect(x: self.cameraView.origin.x, y: self.cameraView.origin.y, width: self.cameraView.width, height: self.cameraView.height))
        self.camera?.fixOrientationAfterCapture = false
        self.camera?.fixOrientationAfterCapture = true
        self.camera?.start()
        self.camera?.updateFlashMode(LLCameraFlashAuto)
    }
    
    //MARK:- Configure collectionview
    func configureCollectionView(){
        self.imagesCollectionView.registerNib(nibName: CapturedImagesCell.defaultReuseIdentifier)
        self.imagesCollectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        self.imagesCollectionView.dataSource = self
        self.imagesCollectionView.delegate = self
    }
    
    //MARK:- Capture photo
    func capturePhoto(){
        
        if self.capturedImages.count >= 10 {
            CommonFunctions.showToastMessage(LocalizedString.Max_Ten_Images_Message.localized)
            return
        }
        
        self.camera?.capture({ (camera, image, nil, error) in
            guard let img = image else {
                printDebug(error?.localizedDescription)
                return }
            if self.capturedImages.count < 10 {
                self.capturedImages.append((index: self.capturedImages.count - 1, stringUrl: "", img: img))
                self.imagesCollectionView.reloadData()
                if self.cameraFor == .addTag {
                    self.delegate?.getCapturedImagesBack(images: self.capturedImages)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }, exactSeenImage: true)
    }
    
    //MARK:- Remove button tapped
    @objc func removeButtobTapped(sender : UIButton){
        guard let indexPath = sender .collectionViewIndexPath(self.imagesCollectionView) else { return }
        self.capturedImages.remove(at: indexPath.item)
        self.imagesCollectionView.reloadData()
    }
    
    
    private func openPicker(){
        var config = YPImagePickerConfiguration()
        config.library.onlySquare = true
        config.library.mediaType = .photo
        config.shouldSaveNewPicturesToAlbum = false
        config.video.compression = AVAssetExportPresetMediumQuality
        config.startOnScreen = .library
        config.screens = [.library]
        config.hidesBottomBar = true
        config.showsCrop = .none
        config.showsFilters = false
        config.icons.hideBackButtonTitle = true
        config.icons.shouldChangeDefaultBackButtonIcon = false
        config.icons.arrowDownIcon = UIImage()
        config.icons.backButtonIcon = AppImages.backBlack.image
        config.hidesStatusBar = true
        config.library.maxNumberOfItems = cameraFor != .addTag ? 10 - self.capturedImages.count : 1
        config.library.skipSelectionsGallery = true
        
        picker = YPImagePicker(configuration: config)
        picker?.didFinishPicking{[weak self](items, cancelled) in
            
            guard let weakSelf = self else{
                return
            }
            
            // when user cancelled a picker
            if cancelled {
                weakSelf.picker?.dismiss(animated: true, completion: nil)
                return
            }
            
            for item in items{
                
                switch item{
                    
                case .photo(let photo):
                    
                    let image1 = photo.image
                    let image2 = photo.modifiedImage
                    let image3 = photo.originalImage
                    if weakSelf.capturedImages.count >= 10 {
                        CommonFunctions.showToastMessage(LocalizedString.Max_Ten_Images_Message.localized)
                    } else {
                        weakSelf.capturedImages.append((index: weakSelf.capturedImages.count - 1, stringUrl: "", img: image1))
                        if weakSelf.cameraFor == .addTag {
                            weakSelf.delegate?.getCapturedImagesBack(images: weakSelf.capturedImages)
                            weakSelf.picker?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                        }
                    }
                    
                case .video(let _):
                    return
                    
                }
            }
            
            weakSelf.imagesCollectionView.reloadData()
            weakSelf.picker?.dismiss(animated: true, completion: nil)
        }
        if let pick = picker{
            present(pick, animated: true, completion: nil)
        }
    }
}

//MARK:- CollectionView Datasource and Delegats
extension CameraVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.capturedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.height - 15  , height: collectionView.frame.height - 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getCapturedImageCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getCapturedImageCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CapturedImagesCell.defaultReuseIdentifier, for: indexPath) as? CapturedImagesCell else { fatalError("Could not dequeue CapturedImagesCell at index \(indexPath) in LoginVC") }
        cell.populateImage(image: self.capturedImages[indexPath.item])
        cell.removeButton.addTarget(self, action: #selector(removeButtobTapped), for: UIControl.Event.touchUpInside)
        cell.cancelButton.addTarget(self, action: #selector(removeButtobTapped), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let browser = SKPhotoBrowser(photos: createWebPhotos(index: indexPath.item))
        browser.initializePageIndex(indexPath.item)
        browser.delegate = self
        present(browser, animated: true, completion: nil)
    }
}

extension CameraVC : SKPhotoBrowserDelegate {
    
    func createWebPhotos(index : Int) -> [SKPhotoProtocol] {
        
        let selectedProduct = [self.capturedImages[index]]
        
        return (0..<selectedProduct.count).map { (i: Int) -> SKPhotoProtocol in
            
            if let img = selectedProduct[i].img{
                let photo = SKPhoto.photoWithImage(img)
                photo.caption = ""
                photo.shouldCachePhotoURLImage = false
                return photo
            }else{
                let productImage = selectedProduct[i].stringUrl
                let photo = SKPhoto.photoWithImageURL( productImage, holder: AppImages.productPlaceholder.image)
                photo.caption = ""
                photo.shouldCachePhotoURLImage = false
                return photo
            }
        }
    }
}

extension CameraVC : ProductCreatedSuccessFully {
    //MARK:- Add product successfully
    func addProductSuccessFully(prod : AddProductData) {
        self.navigationController?.dismiss(animated: true, completion: {
            self.addProductSuccessFullDelegate?.addProductSuccessFully(prod: prod)
            
        })
        
    }
}
