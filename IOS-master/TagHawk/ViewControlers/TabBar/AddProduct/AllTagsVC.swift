//
//  AllTagsVC.swift
//  TagHawk
//
//  Created by Appinventiv on 16/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class AllTagsVC: BaseVC {
    
    //MARK:- PROPERTIES
    //=================
    private var userTags = [Tag]()
    var selectedTags = [Tag]()
    weak var delegate: SetCommunitySelection?
    private var controller = AddProductControler()
    private var isrefreshed = false
    private var serviceCalledFirstTime = true
    private let refreshControl = UIRefreshControl()
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var mainTableView: UITableView! {
        didSet {
            mainTableView.delegate = self
            mainTableView.dataSource = self
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.rightBarTitle(title: LocalizedString.done.localized, ofVC: self)
        self.navigationItem.title = LocalizedString.MyTags.localized
        if serviceCalledFirstTime{
            controller.getuserTags(page: page)
        }
    }
    
    //MARK:- Right button tapped
    override func rightBarButtonTapped(_ sender: UIBarButtonItem) {
        super.rightBarButtonTapped(sender)
        navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- UITABLEIVIEW DELEGATE AND DATASOURCE
extension AllTagsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userTags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: TagTableCell.self)
        cell.titleLbl.text = userTags[indexPath.row].name
        if self.selectedTags.contains(where: { (item) -> Bool in
            item.id == self.userTags[indexPath.item].id
        }){
            cell.selectImgView.image = AppImages.tickWithSquare.image
        }else{
            cell.selectImgView.image = AppImages.unTickWithSquare.image
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let ind = selectedTags.firstIndex(where: { (obj) -> Bool in
            return obj.id == self.userTags[indexPath.item].id
        }){
            self.selectedTags.remove(at: ind)
        } else{
            self.selectedTags.append(self.userTags[indexPath.item])
        }
        
        tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .none)
        self.delegate?.setselectedCommunities(tags: self.selectedTags)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if userTags.count >= 9  {
            if indexPath.item == self.userTags.count - 1 && self.nextPage != 0{
                
                if !AppNetworking.isConnectedToInternet {
                    CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                    return
                }
                
                controller.getuserTags(page: page)
            }
        }
    }
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension AllTagsVC {
    //MARK:- Set up view
    private func initialSetup(){
        controller.delegate = self
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.mainTableView)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.mainTableView.addSubview(refreshControl)
    }
    
    //MARK:- Refresh Table
    @objc func refresh(){
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        self.resetPage()
        self.isrefreshed = true
        controller.getuserTags(page: page)
    }
}

//MARK:- Get tag user delegates
extension AllTagsVC: GetTagUsersDelegate {
    
    //MARK:- Will request to users
    func willRequestTagUsers() {
        if self.page == 1{
            self.shouldDisplayEmptyDataView = false
            self.mainTableView.reloadEmptyDataSet()
            if !isrefreshed {
                self.view.showIndicator()
            }
        }
    }
    
    //MARK:- User tag received successfully
    func userTagsReceivedSuccessFully(tags: [Tag], nextPage: Int) {
        serviceCalledFirstTime = userTags.isEmpty ? true : false
        self.view.hideIndicator()
        
        if self.page == 1{
            self.userTags.removeAll()
            self.mainTableView.reloadData()
        }
        
        if nextPage != 0{
            self.page = nextPage
        }
        
        userTags.append(contentsOf: tags)
        mainTableView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.mainTableView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
    func failedToReceiveTagUsers(message: String) {
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = true
        self.mainTableView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
}

// MARK:- TagTableCell
class TagTableCell: UITableViewCell {
    
    @IBOutlet weak var selectImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
}
