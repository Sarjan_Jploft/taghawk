//
//  FeatureProductVC.swift
//  TagHawk
//
//  Created by Appinventiv on 27/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import StoreKit

struct FeatureProductInput {
    
    var selectedPrice : String = ""
    var isPramoteInTagChecked = false
    var selectedTags : [Tag] = []
    
    init(){
        
    }
    
}

protocol PostnotherDelegate : class {
    func productPramotedSuccessfull()
    func postAnother()
}

class FeatureProductVC : BaseVC {
    
    @IBOutlet weak var featureProductImageView: UIImageView!
    @IBOutlet weak var featureProductTableView: UITableView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var promoteInTagCheckMarkButton: UIButton!
    @IBOutlet weak var postAnotherButton: UIButton!
    @IBOutlet weak var shareItemButton: UIButton!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var doneBtn: UIButton!
    
    
    let plans : [(title : String, price : String)] = [(LocalizedString.One_Day.localized, "3.99"), (LocalizedString.Three_Days.localized, "6.99"), (LocalizedString.Seven_Days.localized, "9.99")]
    var featureProductData = FeatureProductInput()
    private let controler = AddProductControler()
    var addProductData = AddProductData()
    weak var delegate : PostnotherDelegate?
    
    //    var controller = SubscriptionsController()
    
    //    MARK: Array of all products fetched from Appstore.
    
    
    private let purchaseCompletionBlock: ((_ purchasedPID:Set<String>,_ restoredPID:Set<String>,_ failedPID:Set<String>)->Void) = { (purchasedPID,restoredPID,failedPID) in
        print("purchasedPID \(purchasedPID)")
        print("restoredPID \(restoredPID)")
        print("failedPID \(failedPID)")
        if !purchasedPID.isEmpty {
            // SubscriptionVC.getAppReceipt()
        }
    }
    
    private var iapProductIdentifiers:Set<String> = [InAppProducts.ONEDAY.rawValue,InAppProducts.THREEDAYS.rawValue,InAppProducts.SEVENDAYS.rawValue]
    
    private var allIapProductIdentifier :Set<String> = [InAppProducts.ONEDAY.rawValue, InAppProducts.ONEDAYPLUSONECOMMUNITY.rawValue, InAppProducts.ONEDAYPLUSTWOCOMMUNITY.rawValue, InAppProducts.ONEDAYPLUSTHREECOMMUNITY.rawValue, InAppProducts.ONEDAYPLUSFOURCOMMUNITY.rawValue,InAppProducts.ONEDAYPLUSFIVECOMMUNITY.rawValue,InAppProducts.THREEDAYS.rawValue, InAppProducts.THREEDAYSPLUSONECOMMUNITY.rawValue, InAppProducts.THREEDAYSPLUSTWOCOMMUNITY.rawValue, InAppProducts.THREEDAYSPLUSTHREECOMMUNITY.rawValue, InAppProducts.THREEDAYSPLUSFOURCOMMUNITY.rawValue,InAppProducts.THREEDAYSPLUSFIVECOMMUNITY.rawValue,InAppProducts.SEVENDAYS.rawValue,  InAppProducts.SEVENDAYSPLUSONECOMMUNITY.rawValue, InAppProducts.SEVENDAYSPLUSTWOCOMMUNITY.rawValue, InAppProducts.SEVENDAYSPLUSTHREECOMMUNITY.rawValue, InAppProducts.SEVENDAYSPLUSFOURCOMMUNITY.rawValue,InAppProducts.SEVENDAYSPLUSFIVECOMMUNITY.rawValue]
    
    fileprivate var allIapProducts = [SKProduct]()
    fileprivate var inAppProductsToShow = [SKProduct]()
    fileprivate var selectedProduct : SKProduct?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchSubscriptions()
        self.setUpSubView()
        
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.featureDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func shareItemButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        CommonFunctions.shareWithSocialMedia(message: self.addProductData.sharingUrl, vcObj: self)
    }
    
    @IBAction func postAnotherButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true) {
            self.delegate?.postAnother()
        }
    }
    
}

//MARK:- Private functions
extension FeatureProductVC{
    
    func setUpSubView() {
        
        doneBtn.setAttributes(title: LocalizedString.done.localized, font: AppFonts.Galano_Semi_Bold.withSize(17), titleColor: .white, backgroundColor : .clear)
        
        self.shareItemButton.setAttributes(title: LocalizedString.Share_Item.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: AppColors.appBlueColor, backgroundColor : UIColor.white)
        self.postAnotherButton.setAttributes(title: LocalizedString.Post_Another.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: AppColors.appBlueColor, backgroundColor : UIColor.white)
        self.shareItemButton.round(radius: 10)
        self.postAnotherButton.round(radius: 10)
        self.priceLabel.round(radius: 10)
        self.indicatorView.hidesWhenStopped = true
        self.indicatorView.stopAnimating()
        self.configureTableView()
        self.populateData()
        self.featureProductImageView.round(radius: 10)
        //        self.controler.getuserTags(page: 1)
    }
    
    func configureTableView(){
        self.featureProductTableView.registerNib(nibName: SellFasterCell.defaultReuseIdentifier)
        self.featureProductTableView.registerNib(nibName: FeaturePlanCell.defaultReuseIdentifier)
        self.featureProductTableView.registerNib(nibName: FeaturePramoteInCell.defaultReuseIdentifier)
        self.featureProductTableView.separatorStyle = .none
        self.featureProductTableView.dataSource = self
        self.featureProductTableView.delegate = self
    }
    
    @objc func checkButtonTapped(){
        self.featureProductData.isPramoteInTagChecked =  !self.featureProductData.isPramoteInTagChecked
        
        UIView.animate(withDuration: 0.2) {
            self.featureProductTableView.reloadData()
            self.view.layoutIfNeeded()
        }
        
        //        self.featureProductTableView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    private func setSubscriptionsPrice() {
        guard self.inAppProductsToShow.indices.contains(0),
            self.inAppProductsToShow.indices.contains(1),
            self.inAppProductsToShow.indices.contains(2)
            else {return}
    }
    
    @objc func priceButtonTapped(sender : UIButton){
        
        guard let indexPath = sender.tableViewIndexPath(self.featureProductTableView) else { return }
        //self.selectedProduct = self.inAppProductsToShow[indexPath.row - 1]
        
        self.selectedProduct = self.controler.getActualproductFromTappedProduct(allProducts: self.allIapProducts, selectedProduct: self.inAppProductsToShow[indexPath.row - 1], selectedCommunityCount: self.featureProductData.selectedTags.count)
        
        guard let prod = self.selectedProduct else {
            CommonFunctions.showToastMessage(LocalizedString.Please_Select_A_Pack.localized)
            return }
        self.purchaseProd(prod)
        self.featureProductTableView.reloadData()
    }
    
    func populateData() {
        self.featureProductImageView.setImage_kf(imageString: self.addProductData.productImages.first?.stringUrl ?? "", placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        self.priceLabel.text = "  $\(self.addProductData.price)  "
    }
    
}

//MARK:- Tableview datasourse and delegates
extension FeatureProductVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inAppProductsToShow.isEmpty ? 0 : self.inAppProductsToShow.count + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
            
        case 0:
            return 75
            
        case 4:
            
            return self.addProductData.sharedCommunities.isEmpty ? 20 : (self.featureProductData.isPramoteInTagChecked ? 150 : 70)
            
        default:
            return 50
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            return self.getSellFasterCell(tableView, indexPath: indexPath)
            
        case 4:
            return self.getFeaturePromptInCell(tableView, indexPath: indexPath)
            
        default:
            return self.getPlanCell(tableView, indexPath: indexPath)
        }
        
    }
    
    func getSellFasterCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SellFasterCell.defaultReuseIdentifier) as? SellFasterCell else { fatalError("FilterVC....\(SellFasterCell.defaultReuseIdentifier) cell not found") }
        
        return cell
        
    }
    
    func getPlanCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FeaturePlanCell.defaultReuseIdentifier) as? FeaturePlanCell else { fatalError("FilterVC....\(FeaturePlanCell.defaultReuseIdentifier) cell not found") }
        //        cell.populateData(title: self.plans[indexPath.row - 1].title , price: self.plans[indexPath.row - 1].price)
        cell.populateData(product: self.inAppProductsToShow[indexPath.row - 1])
        cell.setSelectedDeselectedState(currentProd: self.inAppProductsToShow[indexPath.row - 1], selectedProduct: self.selectedProduct)
        cell.priceButton.addTarget(self, action: #selector(priceButtonTapped), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    func getFeaturePromptInCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FeaturePramoteInCell.defaultReuseIdentifier) as? FeaturePramoteInCell else { fatalError("FeatureProductVC....\(FeaturePramoteInCell.defaultReuseIdentifier) cell not found") }
        cell.checkButton.isHidden = self.addProductData.sharedCommunities.isEmpty
        cell.communitiesCollectionView.delegate = self
        cell.communitiesCollectionView.dataSource = self
        cell.checkButton.addTarget(self, action: #selector(checkButtonTapped) , for: UIControl.Event.touchUpInside)
        cell.setCheckImage(isPromptSelected: self.featureProductData.isPramoteInTagChecked)
        return cell
    }
}

//MARK:- Collectionview datasource and delegate
extension FeatureProductVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //        return self.addProductData.sharedCommunities.count > 6 ? 6 : self.addProductData.sharedCommunities.count
        return self.addProductData.sharedCommunities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 2
        return CGSize(width: finalContentWidth / 2, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getcommunityCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getcommunityCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SelectCommunityCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? SelectCommunityCollectionViewCell else { fatalError("Could not dequeue SelectCommunityCollectionViewCell at index \(indexPath) in LoginVC") }
        
        cell.populateData(tag: self.addProductData.sharedCommunities[indexPath.item])
        
        if self.featureProductData.selectedTags.contains(where: { (item) -> Bool in
            item.id == self.addProductData.sharedCommunities[indexPath.item].id
        }){
            cell.tickImageView.image = AppImages.check.image
        }else{
            cell.tickImageView.image = AppImages.unCheck.image
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let ind = featureProductData.selectedTags.firstIndex(where: { (obj) -> Bool in
            return obj.id == self.addProductData.sharedCommunities[indexPath.item].id
        }){
            self.featureProductData.selectedTags.remove(at: ind)
        } else{
            
            if self.featureProductData.selectedTags.count == 4{
                CommonFunctions.showToastMessage(LocalizedString.Can_Promote_In_Max_4_Communities.localized)
                return
            }
            self.featureProductData.selectedTags.append(self.addProductData.sharedCommunities[indexPath.item])
        }
        
        collectionView.reloadItems(at: [IndexPath(item: indexPath.item, section: indexPath.section)])
    }
}


// MARK: InAppPurchase Methods
extension FeatureProductVC {
    
    private func fetchSubscriptions() {
        
        self.view.showIndicator()
        self.featureProductTableView.isHidden = true
        IAPController.shared.fetchAvailableProducts(productIdentifiers: self.allIapProductIdentifier, success: { [weak self] (products) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.allIapProducts = products
            
            weakSelf.inAppProductsToShow.append(contentsOf: products.filter { $0.productIdentifier == InAppProducts.ONEDAY.rawValue })
            weakSelf.inAppProductsToShow.append(contentsOf: products.filter { $0.productIdentifier == InAppProducts.THREEDAYS.rawValue })
            weakSelf.inAppProductsToShow.append(contentsOf: products.filter { $0.productIdentifier == InAppProducts.SEVENDAYS.rawValue })
            DispatchQueue.main.async {
                weakSelf.view.hideIndicator()
                weakSelf.featureProductTableView.reloadData()
                weakSelf.featureProductTableView.isHidden = false
            }
           
            
            }, failure: { [weak self] (error) in
                
                guard let weakSelf = self else { return }
                 DispatchQueue.main.async {
                weakSelf.view.hideIndicator()
                }
                //                self?.view.hideIndicator()
                
                if let err = error{
                     DispatchQueue.main.async {
                    let alertController = UIAlertController(title: FETCH_PRODUCT_ERROR, message: err.localizedDescription, preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: OK, style: .cancel, handler: nil)
                    alertController.addAction(alertAction)
                    weakSelf.present(alertController, animated: true, completion: nil)
                    }
                }
        })
    }
    
    //MARK: Get latest app receipt & Update on server
    private func getAppReceipt(_ product: SKProduct){
        
        IAPController.shared.fetchIAPReceipt(sharedSecrete: SHARED_SECRETE, success: {[weak self] (receipt) in
            if let receiptInfo = receipt as? ReceiptInfo{
                
            }
            }, failure: { (error) in
                print(error?.localizedDescription ?? "")
        })
    }
    
    //MARK:- Purchase product
    private func purchaseProd(_ product: SKProduct){
        if !AppNetworking.isConnectedToInternet{ CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized); return }
        self.indicatorView.startAnimating()
        IAPController.shared.purchaseProduct(product: product) {[weak self] (purchasedPID, restoredPID, failedPID) in
            guard let weakSelf = self else { return }
            weakSelf.indicatorView.stopAnimating()
            if !purchasedPID.isEmpty {
                guard let days = product.localizedTitle.first else { return }
                weakSelf.controler.featureProduct(paymentId: weakSelf.addProductData.prodId, productId: weakSelf.addProductData.prodId, days: Int(String(days)) ?? 1, price: product.price.floatValue, sharedCommunities: weakSelf.featureProductData.selectedTags)
            }
        }
    }
}

//MARK:- featured product dellegate
extension FeatureProductVC : FeatureProductDelegate {
    func willRequestFeatureProduct() {
        self.view.showIndicator()
    }
    
    func productFeaturedSuccessFully() {
        self.view.hideIndicator()
        self.dismiss(animated: true) {
        }
    }
    
    func failedToFeatureProduct() {
        self.view.hideIndicator()
        
    }
    
}
