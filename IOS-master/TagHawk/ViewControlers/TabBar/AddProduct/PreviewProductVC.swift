//
//  PreviewProductViC.swift
//  TagHawk
//
//  Created by Appinventiv on 20/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ProductCreatedSuccessFully : class {
    func addProductSuccessFully(prod : AddProductData)
}


class PreviewProductVC : BaseVC {
    
    //MARK:- IbOutlets
    @IBOutlet weak var gradientImgView: UIImageView!
    @IBOutlet weak var productDetailTableView: UITableView!
    @IBOutlet weak var productImageCollectionView: UICollectionView!
    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationTitleLbl: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //MARK:- Variables
    var prodId : String = ""
    private let controler = ProductDetailControler()
    private let addProductControler = AddProductControler()
    weak var likeStatusBackDeleate : GetLikeStatusBack?
    var addProductData = AddProductData()
    weak var delegate : ProductCreatedSuccessFully?
    private var failedCount = 0
    private var successCount = 0
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func bindControler() {
        super.bindControler()
       self.addProductControler.addProductDelegate = self
        self.addProductControler.uploadImagesDelegate = self
    }
    
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBActions
    
    //MARK:- Back button tapped
    @IBAction func bacjButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- post button tapped
    @IBAction func postButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
     
        if !AppNetworking.isConnectedToInternet{
        CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        DispatchQueue.main.async {
            self.view.showIndicator()
        }
        
        for (index,_) in self.addProductData.productImages.enumerated(){
            self.addProductData.productImages[index].index = index
        }
        
        self.addProductControler.uploadImagesWithSequence(allImagesToUpload: self.addProductData.productImages)
        
    }
 
    //MARK:- page control view changed
    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
        self.productImageCollectionView.scrollToItem(at: IndexPath(item: sender.currentPage, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
    }
}

private extension PreviewProductVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        navigationTitleLbl.font = AppFonts.Galano_Semi_Bold.withSize(16)
        navigationTitleLbl.text = addProductData.title
        self.tableHeaderView.frame.size.height = screenWidth
        self.configureTableView()
        self.configureCollectionView()
        self.postButton.round(radius: 10)
        self.postButton.setAttributes(title: LocalizedString.Post.localized, font: AppFonts.Galano_Semi_Bold.withSize(17) , titleColor: UIColor.white, backgroundColor: UIColor.clear)
        self.configurePageControl()
        self.tableViewTop.constant = Display.typeIsLike == .iphoneX ? -44 : -20
        self.configurePageControl()
    }
    
    //MARK:- Configure page control
    func configurePageControl(){
        if self.addProductData.productImages.count <= 1 {
            self.pageControl.isHidden = true
        } else {
            self.pageControl.isHidden = false
            self.pageControl.numberOfPages = self.addProductData.productImages.count
            self.pageControl.currentPage = 0
            self.pageControl.currentPageIndicatorTintColor = AppColors.appBlueColor
            self.pageControl.pageIndicatorTintColor = UIColor.white
        }
    }
    
    //MARK:- Configure tableview
    func configureTableView(){
        self.productDetailTableView.registerNib(nibName: ProduceDetailAndDescriptionCell.defaultReuseIdentifier)
        self.productDetailTableView.registerNib(nibName: ProductDetailMapCell.defaultReuseIdentifier)
        self.productDetailTableView.registerNib(nibName: CellWithLabel.defaultReuseIdentifier)
        self.productDetailTableView.rowHeight = UITableView.automaticDimension
        self.productDetailTableView.separatorStyle = .none
        self.productDetailTableView.estimatedRowHeight = 100
        self.productDetailTableView.delegate = self
        self.productDetailTableView.dataSource = self
    }
    
    //MARK:- Configure collectionview
    func configureCollectionView(){
        self.productImageCollectionView.registerNib(nibName: ProductDetailImageCellColl.defaultReuseIdentifier)
        self.productImageCollectionView.showsHorizontalScrollIndicator = false
        self.productImageCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.productImageCollectionView.isPagingEnabled = true
        self.productImageCollectionView.delegate = self
        self.productImageCollectionView.dataSource = self
    }
    
    func setIsLikedStatus(){
        
    }
    
}

//MARK:- Configure Navigation
private extension PreviewProductVC {
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}

//MARK:- tableview datasource and delegates
extension PreviewProductVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return UITableView.automaticDimension

        case 1:
            return 280
        default:
            
          return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.getDetailAndDescriptionCell(tableView, indexPath: indexPath)
            
        case 1:
            
            return self.getMapCell(tableView, indexPath: indexPath)
            
        default:
            return getCellWithlabel(tableView, indexPath: indexPath)
//            return self.getMapCell(tableView, indexPath: indexPath)
        }
    }
    
    func getDetailAndDescriptionCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProduceDetailAndDescriptionCell.defaultReuseIdentifier) as? ProduceDetailAndDescriptionCell else { fatalError("SettingsVC....\(ProduceDetailAndDescriptionCell.defaultReuseIdentifier) cell not found") }
        cell.setUpFor = .productPreview
        cell.populatePreviewData(product: self.addProductData)
        return cell
        
    }
    
    func getMapCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailMapCell.defaultReuseIdentifier, for: indexPath) as? ProductDetailMapCell else { fatalError("Could not dequeue ProductDetailMapCell at index \(indexPath) in LoginVC") }
        cell.setUpFor = .previewProduct
        cell.populateData(product: self.addProductData)
        return cell
    }
    
    func getCellWithlabel(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellWithLabel.defaultReuseIdentifier) as? CellWithLabel else { fatalError("SettingsVC....\(ProduceDetailAndDescriptionCell.defaultReuseIdentifier) cell not found") }
        cell.setUpFor = .productPreViewCommunity
        let txt = "\n\(self.addProductData.sharedCommunities.map { "-\($0.name)" }.joined(separator: "\n"))"
        cell.populateData(txt: txt)
        cell.cellLabel.textAlignment = .left
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//MARK:- CollectionView Datasource and Delegats
extension PreviewProductVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.addProductData.productImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == self.productImageCollectionView ? 0 : 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width  , height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getProductImageCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductImageCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductDetailImageCellColl.defaultReuseIdentifier, for: indexPath) as? ProductDetailImageCellColl else { fatalError("Could not dequeue ProductDetailImageCellColl at index \(indexPath) in LoginVC") }
//        cell.populateData(imageUrl: self.product.images[indexPath.item].image)
        cell.populateData(image: self.addProductData.productImages[indexPath.item].img)
        return cell
    }
    
    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchedProduceCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? SearchedProduceCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let browser = SKPhotoBrowser(photos: createWebPhotos())
        browser.initializePageIndex(0)
        browser.delegate = self
        present(browser, animated: true, completion: nil)
        
    }
    
}

//MARK: scrolview delegate
extension PreviewProductVC  {

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let factor = scrollView.contentOffset.y / screenWidth
        self.navigationView.backgroundColor = AppColors.appBlueColor.withAlphaComponent(factor)
        self.statusBarView.backgroundColor = AppColors.appBlueColor.withAlphaComponent(factor)
        gradientImgView.alpha = (1 - factor)
        if scrollView != self.productImageCollectionView{ return }
        guard let indexPath =  self.productImageCollectionView.indexPathForItem(at: CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y)) else { return }
        self.pageControl.currentPage = indexPath.item
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if scrollView != self.productImageCollectionView{ return }
        guard let indexPath =  self.productImageCollectionView.indexPathForItem(at: CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y)) else { return }
        self.pageControl.currentPage = indexPath.item
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView != self.productImageCollectionView{ return }
        guard let indexPath =  self.productImageCollectionView.indexPathForItem(at: CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y)) else { return }
        self.pageControl.currentPage = indexPath.item
    }
    
}

//MARK:- Full view of images
//===========================
extension PreviewProductVC : SKPhotoBrowserDelegate {
    
    func createWebPhotos() -> [SKPhotoProtocol] {
        
        return (0..<self.addProductData.productImages.count).map { (i: Int) -> SKPhotoProtocol in
            
            let productImage = self.addProductData.productImages[i].img ?? UIImage()
            
            let photo = SKPhoto.photoWithImage(productImage)
            
            photo.caption = ""
            photo.shouldCachePhotoURLImage = false
            return photo
        }
    }
}

//MARK:- Add product delegates
extension PreviewProductVC : AddProductDelegate{
    
    func willAddProduct(){
        self.view.showIndicator()
    }
    
    func addProductSuccessFully(prodId: String, sharingUrl: String) {
        self.view.hideIndicator()
        self.addProductData.prodId =  prodId
        self.addProductData.sharingUrl = sharingUrl
        self.delegate?.addProductSuccessFully(prod: self.addProductData)
        self.refreshHome()
    }

    func failedToAddproduct(message : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
    
    func validateInput(isValid : Bool, message : String){
        self.view.hideIndicator()
    }
}

//MARK:- Upload image delegate
extension PreviewProductVC : UploadImagesDelegate {
    
    func willStartUploadingImages() {
        self.view.showIndicator()
    }
    
    func imagesUploadedSuccessfullyWithSequence(allImagesStatus: [(index: Int, stringUrl: String, img: UIImage?)]) {
        self.view.hideIndicator()
        
        if allImagesStatus.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.pleaseTryAgain.localized)
            return
        }
        
        self.addProductData.productImages = allImagesStatus
        self.addProductControler.addProduct(data: self.addProductData)
        
    }
    
    func imagesUploadedSuccessfully(allImagesStatus: [Int : (isSuccess: Bool, url: String)]) {
        self.view.hideIndicator()
        
    }
    
}
