//
//  AddProductVC.swift
//  TagHawk
//
//  Created by Appinventiv on 08/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

struct AddProductData {
    
    var prodId = ""
    var title : String = ""
    var category : Category = Category(json: JSON())
    var price : Int = 0
    var isNegotiable : Bool = false
    var condition = SelectedFilters.ProductConditionType.none
    var description : String = ""
    var shippingAvailability : [ShippingAvailability] = []
    var lat : Double = 0
    var long : Double = 0
    var shareToCommunity : [String] = []
    var location : String = ""
    var sharedCommunities : [Tag] = []
    var capturedImages : [(strUrl : String, img : UIImage?)] = []
    var capturedImagesUrl : [String] = []
    var sharingUrl : String = ""
    var shippingData = ShippingWeightData()
    var selectedShippingMode = ShippingMode.none
    
    
    func getProductFromAddProduct()  {
  
    }
    
}


protocol RefreshProductDetails : class {
    
    func refreshProductDetails()
    
}

class AddProductVC : BaseVC {

    //MARK:- IBOutlet
    @IBOutlet weak var addProductTableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var previewButton: UIButton!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var imagesTitleLabel: UILabel!
    
    //MARK:- Variables
    var addProductData = AddProductData()
    var myTags : [Tag] = []
    private let controler = AddProductControler()
    var userTags : [Tag] = []
    weak var delegate : ProductCreatedSuccessFully?
    var product = Product()
    var commingFor = AddProductFor.add
    weak var refreshDelegate : RefreshProductDetails?
    private var allImages : [Int : (isSuccess : Bool, url : String)] = [:]
    weak var addProductSuccessFullDelegate : ProductCreatedSuccessFully?

    var shippingWeightData : [ShippingWeightData] = []
    
    
    enum AddProductFor {
        case edit
        case add
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
        // Do any additional setup after loading the view.
    }

    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
        self.controler.addProductDelegate = self
        self.controler.editProductDelegate = self
        self.controler.uploadImagesDelegate = self
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
       self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func previewButtonTapped(_ sender: UIButton) {
        
        if self.commingFor == .add{
            if !self.controler.validateAddProduct(with: self.addProductData){ return }
            let vc = PreviewProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.addProductData = self.addProductData
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
//            self.present(vc, animated: true, completion: nil)
        }else{
            
            let filteredImages = self.addProductData.capturedImages.filter { $0.img != nil }
            
            let finalImages = filteredImages.map { (item) -> UIImage in
                return item.img ?? UIImage()
            }
            
            if filteredImages.isEmpty{
                self.controler.editProduct(data: self.addProductData)
            }else{
                self.controler.uploadImages(allImagesToUpload: finalImages)
            }
        }
    }
    
    @IBAction func addImageButtonTapped(_ sender: UIButton) {
    
//        self.openPicker()
        
        let vc = CameraVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: true)
        vc.delegate = self
        vc.cameraFor = .addProduct
        vc.capturedImages = self.addProductData.capturedImages
        self.present(nav, animated: true, completion: nil)
    }
}


extension AddProductVC {
    
    func setUpSubView(){
        self.readShippingWeightData()
        self.imagesTitleLabel.setAttributes(text: LocalizedString.Add_Images.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.imagesTitleLabel.attributedText = LocalizedString.Add_Images.localized.attributeStringWithAstric()
        self.configureTableView()
        self.configureCollectionView()
        if self.commingFor != .add{
            self.preSetDataForEditing()
        }
        if self.commingFor == .add{ self.controler.getuserTags(page: 1) }
    }
    
    func configureTableView(){
        
       let previewBtnTitle = commingFor == .add ? LocalizedString.Preview.localized : LocalizedString.Edit.localized
        self.previewButton.setAttributes(title: previewBtnTitle, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)
        self.addProductTableView.registerNib(nibName: AddProductPriceCell.defaultReuseIdentifier)
        self.addProductTableView.registerNib(nibName: AddProductTextFieldCell.defaultReuseIdentifier)
        self.addProductTableView.registerNib(nibName: AddProductDescriptionCell.defaultReuseIdentifier)
        self.addProductTableView.registerNib(nibName: ShareToCommunityCell.defaultReuseIdentifier)
        self.addProductTableView.registerNib(nibName: ShippingAvailabilityCell.defaultReuseIdentifier)
        self.addProductTableView.registerNib(nibName: AddProductLocationCell.defaultReuseIdentifier)
        
        self.addProductTableView.registerNib(nibName: DropDownTableViewCell.defaultReuseIdentifier)
        self.addProductTableView.registerNib(nibName: ConditionCell.defaultReuseIdentifier)
        self.addProductTableView.registerNib(nibName: AddProductWeightCell.defaultReuseIdentifier)

        self.addProductTableView.separatorStyle = .none
        self.addProductTableView.delegate = self
        self.addProductTableView.dataSource = self
        
    }
    
    func configureCollectionView(){
        self.imageCollectionView.registerNib(nibName: CapturedImagesCell.defaultReuseIdentifier)
        self.imageCollectionView.contentInset = UIEdgeInsets(top: 7, left: 0, bottom: 0, right: 20)
        self.imageCollectionView.dataSource = self
        self.imageCollectionView.delegate = self
        self.imageCollectionView.showsHorizontalScrollIndicator = false
    }
    
    @objc func removeButtobTapped(sender : UIButton){
        guard let indexPath = sender .collectionViewIndexPath(self.imageCollectionView) else { return }
        self.addProductData.capturedImages.remove(at: indexPath.item)
        self.imageCollectionView.reloadData()
    }
    
    
    func preSetDataForEditing(){
        self.addProductData = self.product.getAddProductDataFromProduct()
        self.addProductTableView.reloadData()
        
    }
    
    
     func readShippingWeightData() {
        
        let file = Bundle.main.path(forResource: "ShippingData", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: file!))
        guard let jsonData = try? JSONSerialization.jsonObject(with: data!, options: []) as? JSONDictionaryArray, let finalArray = jsonData else { return }
        
        printDebug(finalArray)
    
      self.shippingWeightData = finalArray.map { (dict) -> ShippingWeightData in
            ShippingWeightData(json: JSON(dict))
        }
        
    }
    
}


extension AddProductVC {
    
    func configureNavigation(){
        
    }
    
}

extension AddProductVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
       
            case 0,1,3,5,6:
                 return 100
            
            case 2:
                return 140
            
            case 4:
                return 150
            
            case 7:
            
                let finalCount = (self.userTags.count > 6) ? 6 : self.userTags.count
                var rows : CGFloat = CGFloat(finalCount / 2)
                rows.round(FloatingPointRoundingRule.up)
                
               return self.commingFor == .add ? (self.userTags.isEmpty ? 0 : (rows * 30) + ((rows - 1) * 2) + 66) : 0
            
        case 8:
            
            return self.addProductData.shippingAvailability.contains(ShippingAvailability.shipping) ? 150 : 0
            
            default:
                return 120
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.getTitleCell(tableView, indexPath: indexPath)
            
        case 1:
            return self.getDropDownCell(tableView, indexPath: indexPath)

        case 2:
            return self.getPriceCell(tableView, indexPath: indexPath)

        case 3:
            return self.getDropDownCell(tableView, indexPath: indexPath)
            
        case 4:
            return self.getDescriptionCell(tableView, indexPath: indexPath)
            
        case 5:
            return self.getShippingAvailabilityCell(tableView, indexPath: indexPath)
        case 6:
            return self.getLocationCell(tableView, indexPath: indexPath)

        case 7:
            return self.getShareToCommunityCell(tableView, indexPath: indexPath)
            
        case 8:
           return self.getWeightCell(tableView, indexPath: indexPath)
            
        default:
            return self.getDropDownCell(tableView, indexPath: indexPath)
        }
    }

    func getWeightCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddProductWeightCell.defaultReuseIdentifier) as? AddProductWeightCell else {
            fatalError("FilterVC....\(AddProductWeightCell.defaultReuseIdentifier) cell not found")
        }
        
        cell.delegate = self
        cell.shippingWeightData = self.shippingWeightData
        cell.populateData(mode: self.addProductData.selectedShippingMode, data: self.addProductData.shippingData)
        return cell
    }
    
    func getLocationCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddProductLocationCell.defaultReuseIdentifier) as? AddProductLocationCell else {
            fatalError("FilterVC....\(AddProductLocationCell.defaultReuseIdentifier) cell not found")
        }
        cell.delegate = self
        cell.populateData(loc: self.addProductData.location)
        return cell
    }
    
    func getShareToCommunityCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ShareToCommunityCell.defaultReuseIdentifier) as? ShareToCommunityCell else {
            fatalError("FilterVC....\(ShareToCommunityCell.defaultReuseIdentifier) cell not found")
        }
        
        cell.delegate = self
        cell.userTags = self.userTags
        cell.selectedUserTags = self.addProductData.sharedCommunities
        cell.selectCommunityCollectionView.reloadData()
        
        return cell
    }
    
    func getShippingAvailabilityCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ShippingAvailabilityCell.defaultReuseIdentifier) as? ShippingAvailabilityCell else {
            fatalError("FilterVC....\(ShippingAvailabilityCell.defaultReuseIdentifier) cell not found") }
        cell.delegate = self
        cell.selectDeselectShippingAvailability(shipping: self.addProductData.shippingAvailability)
        return cell
    }
    
    func getDescriptionCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddProductDescriptionCell.defaultReuseIdentifier) as? AddProductDescriptionCell else {
            fatalError("FilterVC....\(AddProductDescriptionCell.defaultReuseIdentifier) cell not found")
        }
        cell.descriptionTextView.returnKeyType = .done
        cell.descriptionTextView.delegate = self
        cell.descriptionTextView.text = self.addProductData.description
        return cell
    }
    
    
    func getTitleCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddProductTextFieldCell.defaultReuseIdentifier) as? AddProductTextFieldCell else {
            fatalError("FilterVC....\(AddProductTextFieldCell.defaultReuseIdentifier) cell not found")
        }
        cell.textField.returnKeyType = .done
        cell.textField.delegate = self
        cell.textField.addTarget(self, action: #selector(textDidChange), for: UIControl.Event.editingChanged)
        cell.textField.text = self.addProductData.title
        
        return cell
    }
    
    func getPriceCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddProductPriceCell.defaultReuseIdentifier) as? AddProductPriceCell else {
            fatalError("FilterVC....\(AddProductPriceCell.defaultReuseIdentifier) cell not found")
        }
        cell.priceTextField.delegate = self
        cell.priceTextField.addTarget(self, action: #selector(textDidChange), for: UIControl.Event.editingChanged)
        cell.delegate = self
        cell.populateData(data: self.addProductData)
        return cell
    }
    
    func getDropDownCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DropDownTableViewCell.defaultReuseIdentifier) as? DropDownTableViewCell else {
            fatalError("FilterVC....\(DropDownTableViewCell.defaultReuseIdentifier) cell not found")
        }
        cell.dropDownView.backgroundColor = AppColors.textfield_Border.withAlphaComponent(0.1)
        switch indexPath.row {
        case 1:
             cell.setUpFor = .productCategory
            cell.categoryDelegate = self
             let labelText = self.addProductData.category.description.isEmpty ? LocalizedString.Select.localized : self.addProductData.category.description
             let labelColor = self.addProductData.category.description.isEmpty ? AppColors.lightGreyTextColor : UIColor.black
             cell.setUpDropDownLabel(labelText: labelText, withColor: labelColor)
            cell.titleLabelTop.constant = 0
            
        default:
            cell.setUpFor = .condition
            cell.conditionDelegate = self
            cell.titleLabelTop.constant = 0

           let labelText = SelectedFilters.shared.getProductConditionString(from: self.addProductData.condition).rawValue
            
            let labelColor = self.addProductData.condition == .none ? AppColors.lightGreyTextColor : UIColor.black
            
            cell.setUpDropDownLabel(labelText: labelText, withColor: labelColor)

        }
        
        return cell
    }
    
    func getDropConditionCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ConditionCell.defaultReuseIdentifier) as? ConditionCell else {
            fatalError("FilterVC....\(ConditionCell.defaultReuseIdentifier) cell not found")
        }
        
        return cell
    }
    
    func getDistanceCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DistanceCell.defaultReuseIdentifier) as? DistanceCell else {
            fatalError("FilterVC....\(DistanceCell.defaultReuseIdentifier) cell not found")
        }
        
        return cell
    }
    
}


//MARK:- CollectionView Datasource and Delegats
extension AddProductVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
        return self.addProductData.capturedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.height - 5 , height: collectionView.frame.height - 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getCapturedImageCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getCapturedImageCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CapturedImagesCell.defaultReuseIdentifier, for: indexPath) as? CapturedImagesCell else { fatalError("Could not dequeue CapturedImagesCell at index \(indexPath) in LoginVC") }
        cell.populateImage(image: self.addProductData.capturedImages[indexPath.item])
        cell.removeButton.addTarget(self, action: #selector(removeButtobTapped), for: UIControl.Event.touchUpInside)
        cell.cancelButton.addTarget(self, action: #selector(removeButtobTapped), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

extension AddProductVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let userEnteredString = textField.text else { return false }
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        
        return self.isCharacterAcceptableInProductTitle(name: userEnteredString, char: string)
        
    }
    
    func isCharacterAcceptableInProductTitle(name : String, char : String)-> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 200{
            return false
        }else{
            return true
        }
    }
    
    @objc func textDidChange(_ textField : UITextField){
        
        guard let txt = textField.text else { return }
        guard let indexPath = textField.tableViewIndexPath(self.addProductTableView) else { return }
        
        switch indexPath.row {
        case 0:
            self.addProductData.title = txt
        default:
            guard let intTxt = Int(txt) else { return }
            self.addProductData.price = intTxt
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}

extension AddProductVC : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard let userEnteredString = textView.text else { return false }

        if (range.location == 0 && text == " ") || text.containsEmoji {return false}
        return self.isCharacterAcceptableForDescription(name: userEnteredString, char: text)
        
    }
    
    func isCharacterAcceptableForDescription(name : String, char : String)-> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 200{
            return false
        }else{
            return true
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text {
            if text.last == "\n" {
                view.endEditing(true)
                textView.text.removeLast()
            }
        }
        self.addProductData.description = textView.text ?? ""
    }
}

extension AddProductVC : GetSelectedCategoryDelegate {
    
    func getSelectedCategory(category: Category) {
        self.addProductData.category = category
        guard let cell = self.addProductTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? DropDownTableViewCell else { return }
        let labelText = self.addProductData.category.description
        let labelColor = UIColor.black
        cell.setUpDropDownLabel(labelText: labelText, withColor: labelColor)
    }
}


extension AddProductVC : GetSelectedConditionDelegate {
    
    func getSelectedCondition(condition: SelectedFilters.ProductConditionType) {
        self.addProductData.condition = condition
        let conditionValue = SelectedFilters.shared.getProductConditionString(from: self.addProductData.condition)
            let labelColor = UIColor.black
        guard let cell = self.addProductTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? DropDownTableViewCell else { return }
        cell.setUpDropDownLabel(labelText: conditionValue.rawValue, withColor: labelColor)
    }
        
}

extension AddProductVC : GetShippingAvailabilityDelegate {
    
    func getShippingAvailability(shipping : ShippingAvailability) {
        
        if let ind = self.addProductData.shippingAvailability.firstIndex(where: { (obj) -> Bool in
            obj == shipping
        }){
            self.addProductData.shippingAvailability.remove(at: ind)
        }else{
             self.addProductData.shippingAvailability.append(shipping)
        }
    
        self.addProductTableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: UITableView.RowAnimation.none)
        self.addProductTableView.reloadRows(at: [IndexPath(row: 8, section: 0)], with: UITableView.RowAnimation.none)

    }

}

extension AddProductVC : MapTextFieldTappedDelegate {
   
    func getSelectedLocation(lat: Double, long: Double, address: String) {
        self.addProductData.lat = lat
        self.addProductData.long = long
        self.addProductData.location = address
    }
    
    func mapFieldTapped(){
        let vc = SelectLocationVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
}

extension AddProductVC : GedAddressBack{
    
    func getAddress(lat : Double, long:Double,addressString:String) {
        self.addProductData.lat = lat
        self.addProductData.long = long
        self.addProductData.location = addressString
        self.addProductTableView.reloadRows(at: [IndexPath(row: 6, section: 0)], with: UITableView.RowAnimation.none)
    }
    
}

extension AddProductVC : FirmPriceValueChanged{
    
    func firmPriceValueChanged(value : Bool) {
        printDebug(value)
        self.addProductData.isNegotiable = !value
    }
    
}

extension AddProductVC : SetCommunitySelection{
    
    func setselectedCommunities(tags : [Tag]) {
        self.addProductData.sharedCommunities = tags
        self.addProductTableView.reloadRows(at: [IndexPath(row: 7, section: 0)], with: UITableView.RowAnimation.none)
    }
    
}

extension AddProductVC : GetTagUsersDelegate {
   
    func willRequestTagUsers(){
        self.view.showIndicator()
        self.addProductTableView.isHidden = true
    }
    
    func userTagsReceivedSuccessFully(tags : [Tag]) {
        self.addProductTableView.isHidden = false
        self.view.hideIndicator()
        self.userTags = tags
        self.addProductTableView.reloadRows(at: [IndexPath(row: 7, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func failedToReceiveTagUsers(message: String) {
        self.view.hideIndicator()

    }
    
}

extension AddProductVC : AddProductDelegate {
  
    func addProductSuccessFully(prodId: String, sharingUrl: String) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func willAddProduct() {
        
    }
    
    func addProductSuccessFully() {
        
    }
    
    func failedToAddproduct(message: String) {
        
    }
    
    func validateInput(isValid: Bool, message: String) {
        CommonFunctions.showToastMessage(message)
    }
    
}


extension AddProductVC : EditProductDelegate {
    
    func willRequestEditProduct() {
        
    }
    
    func editProductSuccessFully() {
        self.refreshDelegate?.refreshProductDetails()
        self.dismiss(animated: true, completion: nil)
    }
    
    func errorOccuredWhileEditProduct(message: String) {
        
    }
    
    func validateInputToEdit(isValid: Bool, message: String) {
        CommonFunctions.showToastMessage(message)
    }
    
}

extension AddProductVC : UploadImagesDelegate {
    
    func willStartUploadingImages() {
        self.view.showIndicator()
    }
    
    func imagesUploadedSuccessfully(allImagesStatus: [Int : (isSuccess: Bool, url: String)]) {
        self.view.hideIndicator()
        printDebug(allImagesStatus)
        
        let keys = Array(allImagesStatus.keys)
        
        for val in keys{
            self.addProductData.capturedImagesUrl.append(allImagesStatus[val]?.url ?? "")
        }
        
        self.controler.editProduct(data: self.addProductData)
    }
}

extension AddProductVC : ProductCreatedSuccessFully{
    
    func addProductSuccessFully(prod: AddProductData) {
         self.delegate?.addProductSuccessFully(prod: prod)
//        self.navigationController?.dismiss(animated: true, completion: {
//
//        })
    }
    
}

extension AddProductVC : GetCapturedImages {
  
    func getCapturedImagesBack(images : [(strUrl : String, img : UIImage?)]) {
        self.addProductData.capturedImages = images
        self.imageCollectionView.reloadData()
    }
    
}


extension AddProductVC : GetSelectedShippingData {
    
    func getSelectedShippingData(data: ShippingWeightData) {
        self.addProductData.shippingData = data
        self.addProductTableView.reloadRows(at: [IndexPath(row: 8, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func getSelectedShippingMode(data: ShippingMode) {
        self.addProductData.selectedShippingMode = data
        self.addProductTableView.reloadRows(at: [IndexPath(row: 8, section: 0)], with: UITableView.RowAnimation.none)

    }
    
    
    
    
}

