//
//  ChatTagListingVC.swift
//  TagHawk
//
//  Created by Appinventiv on 23/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ChatTagListingVC: BaseVC {

    //MARK:- PROPERTIES
    //=================
    private var tagArr = [Tag]()
    private var selectedIndex: Int?
    private let controller = AddProductControler()
    private var isrefreshed = false
    private let refreshControl = UIRefreshControl()
    
    var selectedTag: ((Tag) -> ())?
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var mainTableView: UITableView! {
        didSet {
            mainTableView.delegate = self
            mainTableView.dataSource = self
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.rightBarTitle(title: LocalizedString.done.localized, ofVC: self)
        self.leftBarItemImage(change: AppImages.blackCross.image, ofVC: self)
        self.navigationItem.title = LocalizedString.MyCommunities.localized
    }
    
    //MARK:- Left button tapped
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Right button tapped
    override func rightBarButtonTapped(_ sender: UIBarButtonItem) {
        super.rightBarButtonTapped(sender)
        
        dismiss(animated: true) {
            if let index = self.selectedIndex {
                self.selectedTag?(self.tagArr[index])
            } else {
                CommonFunctions.showToastMessage(LocalizedString.PleaseSelectATag.localized)
                return
            }
        }
    }
}

//MARK:- UITABLEIVIEW DELEGATE AND DATASOURCE
extension ChatTagListingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tagArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: ChatTagListTableCell.self)
        let currentTag = tagArr[indexPath.row]
        cell.separatorView.isHidden = indexPath.row == tagArr.count - 1 ? true : false
        cell.selectedImgView.image = selectedIndex == indexPath.row ? AppImages.tickWithCircle.image : nil
        cell.tagImgView.setImage_kf(imageString: currentTag.tagImageUrl, placeHolderImage: #imageLiteral(resourceName: "icHomePlaceholder"), loader: true)
        cell.titleLbl.text = currentTag.name
        cell.descLbl.text = currentTag.description
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ChatTagListTableCell else { return }
        cell.selectedImgView.image = AppImages.tickWithCircle.image
        selectedIndex = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ChatTagListTableCell else { return }
        cell.selectedImgView.image = nil
        selectedIndex = nil
    }
    
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension ChatTagListingVC {
    
    //MARK:- Init set up
    private func initialSetup(){
        controller.delegate = self
        controller.getuserTags(page: page)
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.mainTableView)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.mainTableView.addSubview(refreshControl)
    }
    
    //MARK:- Refresh
    @objc func refresh(){
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.resetPage()
        self.isrefreshed = true
        controller.getuserTags(page: page)
    }
}

//MARK:- Webservices
extension ChatTagListingVC: GetTagUsersDelegate {
    
    func willRequestTagUsers() {
        if self.page == 1{
            self.shouldDisplayEmptyDataView = false
            self.mainTableView.reloadEmptyDataSet()
            if !isrefreshed {
                self.view.showIndicator()
            }
        }
    }
    
    func userTagsReceivedSuccessFully(tags: [Tag], nextPage: Int) {
        self.view.hideIndicator()
        
        if self.page == 1{
            self.tagArr.removeAll()
            self.mainTableView.reloadData()
        }
        
        if nextPage != 0{
            self.page = nextPage
        }
        
        tagArr.append(contentsOf: tags)
        mainTableView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.mainTableView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
    func failedToReceiveTagUsers(message: String) {
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = true
        self.mainTableView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
    
}



//MARK:- ChatTagListTableCell

class ChatTagListTableCell: UITableViewCell {
    
    @IBOutlet weak var tagImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var selectedImgView: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        tagImgView.contentMode = .scaleAspectFill
        tagImgView.round()
    }
    
}
