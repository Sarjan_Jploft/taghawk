//
//  NotificationsVC.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 13/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class NotificationsVC: BaseVC {

    //MARK:- PROPERTIES
    //=================
    private let controller = NotificationsController()
    private var notificationModel: NotificationModel?
    private var notificationArr = [Notifications]()
    private var isrefreshed = false
    private var serviceCalledFirstTime = true
    private let refreshControl = UIRefreshControl()

    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var mainTableView: UITableView! {
        didSet {
            mainTableView.delegate = self
            mainTableView.dataSource = self
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if serviceCalledFirstTime{
            controller.getNotificationsData(pageNo: page)
        }
    }
    
    //MARK:- TARGET ACTIONS
    //=====================
    
    
}

//MARK:- UITABLEIVIEW DELEGATE AND DATASOURCE
extension NotificationsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: NotificationCell.self)
        let curNotif = notificationArr[indexPath.row]
        cell.titleLbl.attributedText = curNotif.message.attributeStringWithColors(stringToColor: curNotif.name, strClr: AppColors.notificatioTextClr, substrClr: AppColors.notificatioTextClr)
        cell.timeLbl.text = Date(timeIntervalSince1970: curNotif.created / 1000).timeAgoSince
        cell.mainImgView.image = curNotif.readStatus == .read ? AppImages.notificationRead.image : AppImages.notificationUnread.image
         cell.mainImgView.image = AppImages.appIcon.image
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !notificationArr.isEmpty else { return }
        let currentNotif = notificationArr[indexPath.row]
        controller.readNotification(notificationId: currentNotif.id)
        
        switch currentNotif.type {
        case .productLike:
            navigateToProdDetails(id: currentNotif.entityId)
        case .tagJoined:
            navigateToTagDetails(id: currentNotif.entityId)
        case .followed:
            let followersScene = FollowingFollowersVC.instantiate(fromAppStoryboard: .Profile)
            followersScene.commingFor = .followers
            followersScene.userId = UserProfile.main.userId
            AppNavigator.shared.parentNavigationControler.pushViewController(followersScene, animated: true)
        case .productAdded:
            navigateToProdDetails(id: currentNotif.entityId)
        case .inviteCodeUsed:
            CommonFunctions.showToastMessage(LocalizedString.underDevelopment.localized)
        case .tagUpdated:
            navigateToTagDetails(id: currentNotif.entityId)
        case .tagJoinAccepted:
            navigateToTagDetails(id: currentNotif.entityId)
        case .prodAddedInCart:
            navigateToCart(id: currentNotif.entityId)
        case .rating:
            navigateToRating(id: currentNotif.entityId)
        case .orderStatus, .productSold:
            let vc = AccountInfoAndPaymentHistoryVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
            vc.commingFrom = .feomProfile
            vc.selectedTab = AccountInfoAndPaymentHistoryVC.CurrentlySelectedHomeTab.history
            AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
        default: break
//            CommonFunctions.showToastMessage(LocalizedString.underDevelopment.localized)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if notificationArr.count >= 9  {
            if indexPath.item == self.notificationArr.count - 1 && self.nextPage != 0{
                controller.getNotificationsData(pageNo: page)
            }
        }
    }
    
    private func navigateToCart(id: String) {
        let cartScene = CartVC.instantiate(fromAppStoryboard: .Home)
        cartScene.commingFor = .productDetails
        AppNavigator.shared.parentNavigationControler.pushViewController(cartScene, animated: true)
    }
    
    private func navigateToProdDetails(id: String) {
        let productScene = ProductDetailVC.instantiate(fromAppStoryboard: .Home)
        productScene.prodId = id
        AppNavigator.shared.parentNavigationControler.pushViewController(productScene, animated: true)
    }
    
    private func navigateToTagDetails(id: String) {
        let tagScene = TagDetailVC.instantiate(fromAppStoryboard: .AddTag)
        tagScene.tagId = id
        AppNavigator.shared.parentNavigationControler.pushViewController(tagScene, animated: true)
    }
    
    private func navigateToRating(id: String) {
        
    }
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension NotificationsVC {
    
    private func initialSetup(){
        controller.delegate = self
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.mainTableView)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.mainTableView.addSubview(refreshControl)
    }
    
    @objc func refresh(){
        self.resetPage()
        self.isrefreshed = true
        controller.getNotificationsData(pageNo: page)
    }
}


//MARK:- NotificationsControllerDelegate
extension NotificationsVC: NotificationsControllerDelegate {
    func willGetNotifications() {
        if self.page == 1{
            self.shouldDisplayEmptyDataView = false
            self.mainTableView.reloadEmptyDataSet()
            if !isrefreshed {
                self.view.showIndicator()
                
            }
        }
    }
    
    func notificationArray(notifArr: [Notifications], nextPage: Int) {
        
        serviceCalledFirstTime = notificationArr.isEmpty ? true : false
        self.view.hideIndicator()
        
        if self.page == 1{
            self.notificationArr.removeAll()
            self.mainTableView.reloadData()
        }
        
        if nextPage != 0{
            self.page = nextPage
        }

        notificationArr.append(contentsOf: notifArr)
        mainTableView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.mainTableView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
    func notificationReadSuccess() {
        
    }
    
    func notificationSuccess(notifModel: NotificationModel) {
        
    }
    
    func notificationFailed() {
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = true
        self.mainTableView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
}


//MARK:- NotificationCell
class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var mainImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
}
