//
//  MoreInfoVC.swift
//  TagHawk
//
//  Created by Appinventiv on 12/06/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class MoreInfoVC: UIViewController {
    
    //    MARK:- IBOutlets
    //    ================
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var cancellButton: UIButton!
    @IBOutlet weak var infoLbl: UILabel!
    
    //    MARK:- Proporties
    //    =================
    var history: PaymentHistory?
    var status: UserStatus = .none
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    //MARK:- IBActions
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension MoreInfoVC {
    
    //MARK:- Set up view
    fileprivate func initialSetup(){
        infoLbl.font = AppFonts.Galano_Regular.withSize(14)
        populateData()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismisViewtapped))
        self.dismissView.addGestureRecognizer(tap)
        self.dismissView.isUserInteractionEnabled = true
        self.infoLbl.textAlignment = .justified
    }
    
    @objc func dismisViewtapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARKL:- Populate data
    fileprivate func populateData(){
        
        if let paymentData = history {
            var htmlText = ""
            switch paymentData.deliveryStatus {
                
            case .pending:
                
                if status == .seller {
                    htmlText  = LocalizedString.pendingInfo.localized
                }else{
                    htmlText = LocalizedString.pendingInfoBuyer.localized
                }
                infoLbl.text = htmlText.html2String
            case .requestForRefund:
                if status == .seller {
                    htmlText = LocalizedString.requestForRefundSellerInfo.localized
                }else{
                    htmlText = LocalizedString.requestForRefundBuyerInfo.localized
                }
                infoLbl.text = htmlText.html2String
            case .declined, .disputeCanStart:
                if status == .seller {
                    infoLbl.text = ""
                }else{
                    infoLbl.text = LocalizedString.sellerDeclinePaymentFor5Days.localized
                }
                
            case .refundAccepted:
                if status == .seller {
                    infoLbl.text = LocalizedString.sellerReleasePaymentFor5Days.localized
                }else{
                    infoLbl.text = LocalizedString.fiveDaysToReturnProduct.localized
                }
                
            default: return
            }
        }
    }
}
