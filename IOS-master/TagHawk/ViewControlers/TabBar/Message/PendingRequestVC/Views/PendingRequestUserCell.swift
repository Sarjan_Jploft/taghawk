//
//  PendingRequestUserCell.swift
//  TagHawk
//
//  Created by Appinventiv on 22/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol PendingRequectCellDelegate: class {
    func attachBtnTapped(button: UIButton)
    func acceptBtnTapped(button: UIButton)
    func rejectBtnTapped(button: UIButton)
    func messageBtnTapped(button: UIButton)
}

class PendingRequestUserCell: UITableViewCell {

//    MARK:- IBOutlets
//    =================
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var viewDocLbl: UILabel!
    @IBOutlet weak var attachBtn: UIButton!
     @IBOutlet weak var acceptBtn: UIButton!
     @IBOutlet weak var rejectBtn: UIButton!
     @IBOutlet weak var messageBtn: UIButton!
    
    weak var delegate: PendingRequectCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userName.textColor = AppColors.blackLblColor
        userName.font = AppFonts.Galano_Semi_Bold.withSize(11.5)
        viewDocLbl.textColor = AppColors.lightGreyTextColor
        viewDocLbl.font = AppFonts.Galano_Regular.withSize(10)
        viewDocLbl.text = LocalizedString.viewDocument.localized
        userImage.roundCorners()
    }

//    MARK:- IBActions
//    =================
    @IBAction func attachBtnTapped(_ sender: UIButton) {
        delegate?.attachBtnTapped(button: sender)
    }
    @IBAction func acceptBtnTapped(_ sender: UIButton) {
       delegate?.acceptBtnTapped(button: sender)
    }
    @IBAction func rejectBtnTapped(_ sender: UIButton) {
        delegate?.rejectBtnTapped(button: sender)
    }
    @IBAction func messageBtnTapped(_ sender: UIButton) {
        delegate?.messageBtnTapped(button: sender)
    }
    
    func populateData(requests: [PendingRequest], indexPath: IndexPath){
        
        guard !requests.isEmpty else{ return }
        let userData = requests[indexPath.row]
        userImage.setImage_kf(imageString: userData.profilePic, placeHolderImage: AppImages.icChatPlaceholder.image)
        userName.text = userData.userName
    }
}
