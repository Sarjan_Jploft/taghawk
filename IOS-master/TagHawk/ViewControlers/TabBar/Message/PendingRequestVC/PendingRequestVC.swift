//
//  PendingRequestVC.swift
//  TagHawk
//
//  Created by Appinventiv on 22/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

class PendingRequestVC: BaseVC {
    
    //    MARK:- IBOutlets
    //    ================
    @IBOutlet weak var pendingRequestTableView: UITableView!
    var groupInfo: GroupDetail?
    let controller = PendingRequestController()
    var selectedIndexForMessage: IndexPath?
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.shouldDisplayEmptyDataView = true
        self.setEmptyDataView(heading: LocalizedString.Oops_It_Is_Empty.localized, description: "", image: AppImages.noData.image)
        self.setEmptyDataViewAttributes(titleFont: AppFonts.Galano_Semi_Bold.withSize(15), descriptionFont: AppFonts.Galano_Regular.withSize(12), spacingInBetween: 10)
        self.setDataSourceAndDelegate(forScrollView: self.pendingRequestTableView)
        pendingRequestTableView.dataSource = self
        pendingRequestTableView.delegate = self
        controller.delegate = self
        controller.getRequests(tagID: groupInfo?.tagID ?? "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigation()
    }
    
    //MARK:- Left button tapped
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- Configure navigation
extension PendingRequestVC {
    
    private func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.title = (groupInfo?.pendingRequestCount ?? 0) > 0 ? LocalizedString.pendingRquests.localized : LocalizedString.pendingRquest.localized
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
}

//MARK:- TBleview datasource and delegates
extension PendingRequestVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return controller.pendingRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueCell(with: PendingRequestUserCell.self)
        
        cell.delegate = self
        cell.populateData(requests: controller.pendingRequests, indexPath: indexPath)
        return cell
    }
}

extension PendingRequestVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension PendingRequestVC: PendingRequectCellDelegate {
    
    func attachBtnTapped(button: UIButton){
        guard let indexPath = button.tableViewIndexPath(pendingRequestTableView) else{ return }
        let browser = SKPhotoBrowser(photos: createWebPhotos(index: indexPath.row))
        browser.initializePageIndex(indexPath.item)
        browser.delegate = self
        present(browser, animated: true, completion: nil)
    }
    
    func acceptBtnTapped(button: UIButton){
        guard let indexPath = button.tableViewIndexPath(pendingRequestTableView) else{ return }
        
        controller.acceptRejectRequest(tagID: groupInfo?.tagID ?? "",
                                       isAccept: true,
                                       indexPath: indexPath,
                                       requestData: controller.pendingRequests[indexPath.row])
        
        pendingRequestTableView.beginUpdates()
        controller.pendingRequests.remove(at: indexPath.row)
        pendingRequestTableView.deleteRows(at: [indexPath], with: .automatic)
        pendingRequestTableView.endUpdates()
    }
    
    func rejectBtnTapped(button: UIButton){
        guard let indexPath = button.tableViewIndexPath(pendingRequestTableView) else{ return }
        controller.acceptRejectRequest(tagID: groupInfo?.tagID ?? "",
                                       isAccept: false,
                                       indexPath: indexPath,
                                       requestData: controller.pendingRequests[indexPath.row])
        
        pendingRequestTableView.beginUpdates()
        controller.pendingRequests.remove(at: indexPath.row)
        pendingRequestTableView.deleteRows(at: [indexPath], with: .automatic)
        pendingRequestTableView.endUpdates()
    }
    
    func messageBtnTapped(button: UIButton){
        guard let indexPath = button.tableViewIndexPath(pendingRequestTableView) else{ return }
        selectedIndexForMessage = indexPath
        let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
        scene.vcInstantiated = .userDetail
        scene.otherUserData = UserProfile.getUserDataInUsermodel(requestData: controller.pendingRequests[indexPath.row])
        AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
    }
}


extension PendingRequestVC: SKPhotoBrowserDelegate {
    
    func createWebPhotos(index : Int) -> [SKPhotoProtocol] {
        let imagesJSON = controller.pendingRequests[index].documentURl
        var photos: [SKPhoto] = []
        imagesJSON.forEach({photos.append(SKPhoto(url: $0.stringValue))})
        return photos
    }
}

extension PendingRequestVC: PendingRequestControllerDelegate {
    func error(message: String) {
        self.shouldDisplayEmptyDataView = false
        CommonFunctions.showToastMessage(message)
    }
    
    func getPendingRequests() {
        self.shouldDisplayEmptyDataView = true
        pendingRequestTableView.reloadData()
    }
    
    func acceptRejectRequest(index: IndexPath, isAccept: Bool, data: PendingRequest){
        if let groupData = groupInfo{
            GroupChatFireBaseController.shared.reducePendingRequest(grouDetail: groupData)
            if isAccept{
                let tagVal = Tag.getTagData(groupDetail: groupData)
                let userData = UserProfile.getUserDataInUsermodel(requestData: data)
                GroupChatFireBaseController.shared.joinGroup(tagData: tagVal, data: userData)
            }
        }
    }
}
