//
//  PendingRequestController.swift
//  TagHawk
//
//  Created by Appinventiv on 22/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

protocol PendingRequestControllerDelegate: class {
    func error(message: String)
    func getPendingRequests()
    func acceptRejectRequest(index: IndexPath, isAccept: Bool, data: PendingRequest)
}

class PendingRequestController {
    
    weak var delegate: PendingRequestControllerDelegate?
    var pendingRequests: [PendingRequest] = []
    
    func getRequests(tagID: String){
        let params: JSONDictionary = ["communityId": tagID,
                                      "pageNo": 1,
                                      "limit": 100]
        
        WebServices.getPendingRequest(parameters: params, success: {[weak self](json) in
            self?.pendingRequests = PendingRequest.getArrayFromJSON(json: json["data"])
            self?.delegate?.getPendingRequests()
        }) {[weak self](error) -> (Void) in
            self?.delegate?.error(message: error.localizedDescription)
        }
    }
    
    func acceptRejectRequest(tagID: String,
                             isAccept: Bool,
                             indexPath: IndexPath,
                             requestData: PendingRequest){
        
        let params: JSONDictionary = ["communityId": tagID,
                                      "userId": pendingRequests[indexPath.row].userID,
                                      "status": isAccept ? 1 : 2]
        WebServices.acceptRejectRequest(parameters: params, success: {[weak self](json) in
            self?.delegate?.acceptRejectRequest(index: indexPath,
                                                isAccept: isAccept,
                                                data: requestData)
        }){[weak self](error) -> (Void) in
            self?.delegate?.error(message: error.localizedDescription)
        }
    }
}
