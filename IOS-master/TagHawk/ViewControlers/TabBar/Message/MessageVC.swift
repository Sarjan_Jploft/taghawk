//
//  NewFileVC.swift
//  TagHawk
//
//  Created by Appinventiv on 28/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import AMScrollingNavbar

class MessageVC : BaseVC {
    
    //MARK:- Variables
    
    enum CurrentlySelectedMessagesTab {
        case messages
        case notifiacations
    }
    
    //MARK:- variables
    var lastSearchText = ""
    var messagesVC : MessageListingVC!
    private var notificationsVC : NotificationsVC!
    var searchVC: SearchMessageVC!
    private var selectedTab = CurrentlySelectedMessagesTab.messages
    private var isMessagesSelected = true
    private var optionsViewTopLast: CGFloat = 0
    private var lastContentOffset: CGFloat = 0
    private var yLastContentOffset: CGFloat = 0
    
    //MARK:- IBOutlets
    @IBOutlet weak var optionsViewTop: NSLayoutConstraint!
    @IBOutlet weak var navigationViewTop: NSLayoutConstraint!
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var giftsButton: UIButton!
    @IBOutlet weak var messagesButton: UIButton!
    @IBOutlet weak var segmentOuterView: UIView!
    @IBOutlet weak var notificationsButton: UIButton!
    @IBOutlet weak var segmentInnerView: UIView!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var textFieldBackView: UIView!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var currentTabSelectedView: UIView!
    @IBOutlet weak var currentTabSelectedViewLeading: NSLayoutConstraint!
    @IBOutlet weak var homeScrollView: UIScrollView!
    @IBOutlet weak var categoryButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var searchBarRightButton: UIButton!
    @IBOutlet weak var searchBarRightImageView: UIImageView!
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        printDebug("Messages...viewWillAppear")
        self.configureNavigation()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        printDebug("Messages...viewDidAppear")
    }
    
    override func bindControler() {
        super.bindControler()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //        self.segmentOuterView.round()
        
    }
    
    //MARK:- IBActions
    
    //MARK:- Share button tapped
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let pinnedData = messagesVC.controller.roomList.pinnedData
        let normalData = messagesVC.controller.roomList.normalChat
        guard !pinnedData.isEmpty || !normalData.isEmpty else { return }
        searchVC = SearchMessageVC.instantiate(fromAppStoryboard: AppStoryboard.Messages)
        searchVC.chats = pinnedData + normalData
        AppNavigator.shared.parentNavigationControler.pushViewController(searchVC, animated: false)
    }
    
    //MARK:- Message button tapped
    @IBAction func messagesButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.isMessagesSelected = true
        self.selectDeselectTbls()
        self.homeScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    //MARK:- Notification buttpn tapped
    @IBAction func notificationsButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.isMessagesSelected = false
        self.selectDeselectTbls()
        self.homeScrollView.setContentOffset(CGPoint(x: screenWidth, y: 0), animated: true)
    }
    
    //MARK:- Categories button tapped
    @IBAction func categoriesButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        let vc = CategoryVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchBarRightButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if self.selectedTab == .notifiacations { return }
        
    }
    
    //MARK:- private functions
    
    func configureNavigation(){
        AppNavigator.shared.parentNavigationControler.setNavigationBarHidden(true, animated: false)
    }
    
    private func setUpSubView(){
        self.homeScrollView.isScrollEnabled = false
        self.categoryButtonWidth.constant = 0
        self.segmentOuterView.round()
        self.segmentOuterView.backgroundColor = AppColors.selectedSegmentColor
        self.shadowView.addShadow()
        self.shadowView.round()
        self.shadowView.clipsToBounds = false
        self.messagesButton.round()
        self.notificationsButton.round()
        self.textFieldBackView.round(radius: 1.0)
        self.messagesButton.setAttributes(title: LocalizedString.Messages.localized, font: AppFonts.Galano_Medium.withSize(14), titleColor: UIColor.white)
        self.notificationsButton.setAttributes(title: LocalizedString.Notifications.localized, font: AppFonts.Galano_Medium.withSize(14), titleColor: UIColor.white)
        self.searchLabel.setAttributes(text: LocalizedString.Search.localized, font: AppFonts.Galano_Regular.withSize(12) , textColor: AppColors.lightGreyTextColor)
//        self.currentTabSelectedView.backgroundColor = AppColors.selectedSegmentColor
        self.currentTabSelectedView.backgroundColor = AppColors.whiteColor
        self.currentTabSelectedView.round()
        self.homeScrollView.contentSize = CGSize(width: screenWidth * 2, height: self.homeScrollView.frame.height - 64)
        self.messagesButton.backgroundColor = UIColor.clear
        self.notificationsButton.backgroundColor = UIColor.clear
        self.homeScrollView.bounces = false
        self.homeScrollView.delegate = self
        self.homeScrollView.isPagingEnabled = true
        self.homeScrollView.showsHorizontalScrollIndicator = false
        self.selectDeselectTbls()
        self.addMessagesVc()
        self.addNotificationsVc()
    }
    
    //MARK:- Add message vc
    private func addMessagesVc() {
        self.messagesVC = MessageListingVC.instantiate(fromAppStoryboard: AppStoryboard.Messages)
        self.homeScrollView.frame = self.messagesVC.view.frame
        self.homeScrollView.addSubview(self.messagesVC.view)
        self.messagesVC.willMove(toParent: self)
        self.addChild(self.messagesVC)
        self.messagesVC.view.frame.size.height = self.homeScrollView.frame.height
        self.messagesVC.view.frame.origin = CGPoint.zero
    }
    
    //MARK:- Add notification vc
    private func addNotificationsVc() {
        self.notificationsVC = NotificationsVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        self.homeScrollView.frame = self.notificationsVC.view.frame
        self.homeScrollView.addSubview(self.notificationsVC.view)
        self.notificationsVC.willMove(toParent: self)
        self.addChild(self.notificationsVC)
        self.notificationsVC.view.frame.size.height = self.homeScrollView.frame.height
        self.notificationsVC.view.frame.origin = CGPoint(x: screenWidth, y: 0)
    }
    
    func selectDeselectTbls(){
        self.isMessagesSelected ? self.selectMessagesTab() : self.selectNotificationsTab()
    }
    
    //MARK:- Select message tab
    func selectMessagesTab(){
        scrollDownCategoryView()
        self.messagesButton.setTitleColor(UIColor.black)
        self.notificationsButton.setTitleColor(AppColors.whiteColor)
        self.selectedTab = .messages
        //        self.categoryButton.isHidden = false
//        self.currentTabSelectedView.backgroundColor = AppColors.selectedSegmentColor
        self.currentTabSelectedView.backgroundColor = AppColors.whiteColor
        //        self.searchBarRightButton.setImage(AppImages.greySearch.image, for: UIControl.State.normal)
        
        DispatchQueue.delay(0.1) {
            self.searchLabel.text = LocalizedString.Search.localized
        }
        self.searchBarRightImageView.image = AppImages.greySearch.image
        
        self.searchBarRightImageView.tintColor = UIColor.clear
        
        
        UIView.animate(withDuration: 0.3) {
            self.categoryButtonWidth.constant = 0
            self.view.layoutIfNeeded()
            
        }
    }
    
    //MARK:- Select notification tab
    func selectNotificationsTab() {
        scrollUpCategoryView(true)
        self.notificationsButton.setTitleColor(UIColor.black)
        self.messagesButton.setTitleColor(AppColors.whiteColor)
        self.selectedTab = .notifiacations
        //        self.categoryButton.isHidden = true
//        self.currentTabSelectedView.backgroundColor = AppColors.segmentTextColor
        self.currentTabSelectedView.backgroundColor = AppColors.whiteColor
        //        self.searchBarRightButton.setImage(AppImages.cross.image, for: UIControl.State.normal)
        
        if !lastSearchText.isEmpty { self.searchLabel.text = lastSearchText }
        
        if let text = self.searchLabel.text {
            if text == LocalizedString.Search.localized {
                self.searchBarRightImageView.image = AppImages.greySearch.image
            } else {
                self.searchBarRightImageView.image = AppImages.cross.image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            }
        }
        self.searchBarRightImageView.tintColor = UIColor.black
        
        UIView.animate(withDuration: 0.3) {
            self.categoryButtonWidth.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}


extension MessageVC : SelectedCategory  {
    
    func selectCategory(category: Category) {
        
    }
    
    func selectCategory(selectedCategoryId : String, categoryName : String) {
        
    }
    
}

//MARK: scrolview delegate
extension MessageVC  {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.currentTabSelectedViewLeading.constant = (scrollView.contentOffset.x / 2) * self.segmentInnerView.width / screenWidth
        
        if scrollView.contentOffset.x == 0.0{
            self.selectMessagesTab()
        }else if scrollView.contentOffset.x == screenWidth {
            self.selectNotificationsTab()
        }
    }
    
    func scrollUpCategoryView(_ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.38, animations: { () -> Void in
                self.optionsViewTop.constant = -45
                self.view.layoutIfNeeded()
            }, completion: {(success) in
            })
        } else {
            self.optionsViewTop.constant = -45
            self.view.layoutIfNeeded()
        }
    }
    
    func scrollDownCategoryView() {
        UIView.animate(withDuration: 0.76, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            self.optionsViewTop.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension MessageVC : ScrollingDelegate {
    
    func manageHeaderView(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewScroll(_ scrollView: UIScrollView) {
        manageHeaderView(scrollView)
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        manageHeaderView(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        manageHeaderView(scrollView)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        manageHeaderView(scrollView)
    }
}
