//
//  DeleteChatVC.swift
//  TagHawk
//
//  Created by Appinventiv on 14/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol DeleteChatDelegate: class {
    func deleteBtnTapped(index: IndexPath?)
    func exitBtnTapped(index: IndexPath?)
    func blockFromGroup()
}

extension DeleteChatDelegate{
    func deleteBtnTapped(index: IndexPath?){}
    func exitBtnTapped(index: IndexPath?){}
    func blockFromGroup(){}
}
enum DeleteChatVCInstantiated{
    
    case deleteChat
    case exitGroup
    case blockedByAnother
}

class DeleteChatVC: UIViewController {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    
//    MARK:- Proporties
//    =================
    var instantiated: DeleteChatVCInstantiated = .deleteChat
    weak var delegate: DeleteChatDelegate?
    var roomName: String = ""
    var selectedIndexPath: IndexPath?
    var groupName: String = ""
    
//    MARK:- ViewController Life Cycle
//    =================================
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        popupView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
//    MARK:- IBActions
//    ================
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        dismiss(animated: true) {[weak self] in
            if self?.instantiated == .deleteChat {
                self?.delegate?.deleteBtnTapped(index: self?.selectedIndexPath)
            }else if self?.instantiated == .exitGroup{
                self?.delegate?.exitBtnTapped(index: self?.selectedIndexPath)
            }else{
               self?.delegate?.blockFromGroup()
            }
        }
    }
}

extension DeleteChatVC {
    
    private func initialSetup(){
        
        popupTitle.textColor = AppColors.blackLblColor
        popupTitle.font = AppFonts.Galano_Semi_Bold.withSize(17.5)
        descriptionLabel.textColor = AppColors.lightGreyTextColor
        
        switch instantiated {
            
        case .deleteChat:
            popupTitle.text = LocalizedString.deleteChat.localized
            descriptionLabel.text = LocalizedString.areYouSureToDeleteChat.localized + roomName + "?"
            deleteBtn.setTitle(LocalizedString.Delete.localized)
            cancelBtn.isHidden = false
        case .exitGroup:
            popupTitle.text = LocalizedString.existGroup.localized
            descriptionLabel.text = LocalizedString.areYouSureExitGroup.localized + groupName + LocalizedString.qustionMark_Group.localized
            deleteBtn.setTitle(LocalizedString.exit.localized)
            cancelBtn.isHidden = false
        case .blockedByAnother:
            popupTitle.text = LocalizedString.Block.localized
            descriptionLabel.text = LocalizedString.blockedByOwner.localized
            deleteBtn.setTitle(LocalizedString.ok.localized)
            cancelBtn.isHidden = true
        }
                
        descriptionLabel.font = AppFonts.Galano_Regular.withSize(13)
        view.backgroundColor = AppColors.blackColor.withAlphaComponent(0.3)
        cancelBtn.titleLabel?.font = AppFonts.Galano_Semi_Bold.withSize(16)
        cancelBtn.setTitle(LocalizedString.cancel.localized)
        cancelBtn.setTitleColor(AppColors.blackBorder)
        cancelBtn.round(radius: 10)
        cancelBtn.borderWidth = 0.5
        deleteBtn.setTitleColor(AppColors.whiteColor)
        deleteBtn.round(radius: 10)
        deleteBtn.titleLabel?.font = AppFonts.Galano_Semi_Bold.withSize(16)
    }
}
