//
//  ReceiverImageCell.swift
//  TagHawk
//
//  Created by Appinventiv on 03/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ReceiverImageCell: UITableViewCell {

//    MARK:- IBOutlets
//    =================
    @IBOutlet weak var userNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var overLayView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productTypeImage: UIImageView!
    
    weak var delegate: ReceiverCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        overLayView.isHidden = true
        productName.isHidden = true
        userName.font = AppFonts.Galano_Medium.withSize(10)
        productName.font = AppFonts.Galano_Regular.withSize(11)
        messageImage.roundCorner(.allCorners, radius: 10.0)
        overLayView.roundCorner(.allCorners, radius: 10.0)
        productName.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        productName.textColor = AppColors.whiteColor
        timeStamp.font = AppFonts.Galano_Regular.withSize(10)
        timeStamp.textColor = AppColors.lightGreyTextColor
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        userImage.round()
    }
    
    @IBAction func profileBtnTapped(_ sender: UIButton) {
        delegate?.profileBtnTapped(button: sender)
    }
    
    func populateData(data: MessageDetail,
                      roomData: ChatListing?,
                      members: [GroupMembers]){
        
        guard let roomInfo = roomData else{
            return
        }
        
        if roomInfo.chatType == .single{
            userImage.setImage_kf(imageString: roomInfo.roomImage,
                                  placeHolderImage: AppImages.icChatPlaceholder.image,
                                  loader: true)
        }else{
            
            if let userIndex = members.firstIndex(where: {$0.id == data.senderID}){
                
                userImage.setImage_kf(imageString: members[userIndex].image,
                                      placeHolderImage: AppImages.icChatPlaceholder.image,
                                      loader: true)
                userName.text = members[userIndex].name
            }
        }

        userName.isHidden = roomInfo.chatType == .single
        userNameLblHeight.constant = roomInfo.chatType == .single ? 0 : 20.5
        messageImage.setImage_kf(imageString: data.messageText, placeHolderImage: AppImages.productPlaceholder.image)
        timeStamp.isHidden = roomInfo.chatType == .group
        let time = Date(timeIntervalSince1970: data.timeInterval)
        let timeInStr = time.toString(dateFormat: Date.DateFormat.HHmm.rawValue, timeZone: TimeZone.current)
        timeStamp.text = timeInStr
        
        switch data.messageType {
            
        case .image:
            overLayView.isHidden = true
            productName.isHidden = true
        case .shareProduct, .shareCommunity:
            overLayView.isHidden = false
            productName.isHidden = false
            messageImage.setImage_kf(imageString: data.sharedImage,
                                     placeHolderImage: AppImages.productPlaceholder.image)
            productName.text = data.messageText
        default: return
        }
    }
}
