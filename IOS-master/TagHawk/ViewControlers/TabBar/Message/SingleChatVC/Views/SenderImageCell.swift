//
//  SenderImageCell.swift
//  TagHawk
//
//  Created by Appinventiv on 03/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SenderImageCell: UITableViewCell {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var messageStatus: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var overLayView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productTypeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        activityIndicator.isHidden = true
        overLayView.isHidden = true
        productName.isHidden = true
        productName.font = AppFonts.Galano_Regular.withSize(11)
        messageImage.roundCorner(.allCorners, radius: 10.0)
        overLayView.roundCorner(.allCorners, radius: 10.0)
        productName.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        productName.textColor = AppColors.whiteColor
        timeStamp.font = AppFonts.Galano_Regular.withSize(10)
        timeStamp.textColor = AppColors.lightGreyTextColor
        
    }
    
    func populateData(data: MessageDetail, chatType: ChatType){
        
        timeStamp.isHidden = chatType == .group
        let time = Date(timeIntervalSince1970: data.timeInterval)
        let timeInStr = time.toString(dateFormat: Date.DateFormat.HHmm.rawValue, timeZone: TimeZone.current)
        timeStamp.text = timeInStr
        activityIndicator.isHidden = !data.messageText.isEmpty
        if data.messageText.isEmpty{
            activityIndicator.startAnimating()
        }else{
           activityIndicator.stopAnimating()
        }
        messageImage.setImage_kf(imageString: data.messageText, placeHolderImage: AppImages.productPlaceholder.image)
        
        if chatType == .single{
            messageStatus.image = data.messageStatus == .read ? AppImages.icReceiveTick.image : AppImages.icSendingTick.image
        }else{
            messageStatus.isHidden = true
            messageStatus.image = data.readCount >= data.memberCount ? AppImages.icReceiveTick.image : AppImages.icSendingTick.image
        }
        
        switch data.messageType {
            
        case .image:
            overLayView.isHidden = true
            productName.isHidden = true
        case .shareProduct, .shareCommunity:
            overLayView.isHidden = false
            productName.isHidden = false
            messageImage.setImage_kf(imageString: data.sharedImage,
                                     placeHolderImage: AppImages.productPlaceholder.image)
            productName.text = data.messageText
            
        default: return
            
        }
    }
}
