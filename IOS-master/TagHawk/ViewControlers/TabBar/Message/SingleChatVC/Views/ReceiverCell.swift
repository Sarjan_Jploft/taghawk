//
//  ReceiverCell.swift
//  TagHawk
//
//  Created by Appinventiv on 27/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ReceiverCellDelegate: class {
    func profileBtnTapped(button: UIButton)
}

class ReceiverCell: UITableViewCell {

    @IBOutlet weak var userNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var msgText: UILabel!
    @IBOutlet weak var timeStamp: UILabel!
    
    weak var delegate: ReceiverCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userName.font = AppFonts.Galano_Medium.withSize(10)
        msgText.font = AppFonts.Galano_Regular.withSize(13)
        msgText.textColor = AppColors.blackLblColor
        timeStamp.font = AppFonts.Galano_Regular.withSize(10)
        timeStamp.textColor = AppColors.lightGreyTextColor
        outerView.round(radius: 10.0)
        outerView.backgroundColor = AppColors.recieveBackgroundColor
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        userImage.roundCorner(.allCorners, radius: userImage.frame.height / 2)
        
    }

    @IBAction func prodileBtnTapped(_ sender: UIButton) {
        delegate?.profileBtnTapped(button: sender)
    }
    
    
    func populateData(data: MessageDetail,
                      roomData: ChatListing?,
                      members: [GroupMembers]){

        guard let roomInfo = roomData else{
            return
        }
        
        if roomInfo.chatType == .single{
            userImage.setImage_kf(imageString: roomInfo.roomImage,
                                  placeHolderImage: AppImages.icChatPlaceholder.image,
                                  loader: true)
        }else{
            if let userIndex = members.firstIndex(where: {$0.id == data.senderID}){
                
                userImage.setImage_kf(imageString: members[userIndex].image,
                                      placeHolderImage: AppImages.icChatPlaceholder.image,
                                      loader: true)
                userName.text = members[userIndex].name
            }else{
                userImage.setImage_kf(imageString: data.senderImage,
                                      placeHolderImage: AppImages.icChatPlaceholder.image,
                                      loader: true)
                userName.text = data.senderName
            }
        }
        userName.isHidden = roomInfo.chatType == .single
        userNameLblHeight.constant = roomInfo.chatType == .single ? 0 : 20.5
        timeStamp.isHidden = roomInfo.chatType == .group
        let time = Date(timeIntervalSince1970: data.timeInterval)
        let timeInStr = time.toString(dateFormat: Date.DateFormat.HHmm.rawValue, timeZone: TimeZone.current)
        timeStamp.text = timeInStr
        
        if data.messageType == .text {
            msgText.text = data.messageText
        }else{
            let completeText = data.messageText + " is added to the shelf"
            msgText.attributedText = completeText.attributeStringWithColors(stringToColor: data.messageText,
                                                                            strClr: AppColors.lightGreyTextColor,
                                                                            substrClr: AppColors.selectedSegmentColor)
        }
    }
}
