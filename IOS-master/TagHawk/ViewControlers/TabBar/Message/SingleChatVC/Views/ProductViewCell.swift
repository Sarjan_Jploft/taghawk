//
//  ProductViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 03/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

class ProductViewCell: DropDownCell {

    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        productImage.cornerRadius = 5.0
        productImage.borderWidth = 1.0
        productImage.borderColor = AppColors.whiteColor
        productTitle.font = AppFonts.Galano_Regular.withSize(12)
        productTitle.textColor = AppColors.blackLblColor
        optionLabel.font = AppFonts.Galano_Semi_Bold.withSize(12)
        optionLabel.textColor = AppColors.blackLblColor
    }
}
