//
//  SenderCell.swift
//  TagHawk
//
//  Created by Appinventiv on 27/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SenderCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var msgText: UILabel!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var messageStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        msgText.font = AppFonts.Galano_Regular.withSize(13)
        msgText.textColor = AppColors.whiteColor
        timeStamp.font = AppFonts.Galano_Regular.withSize(10)
        timeStamp.textColor = AppColors.lightGreyTextColor
        outerView.round(radius: 10.0)
        outerView.backgroundColor = AppColors.themeColor
    }
    
    func populateData(data: MessageDetail,
                      chatType: ChatType){
       
        timeStamp.isHidden = chatType == .group
        
        let time = Date(timeIntervalSince1970: data.timeInterval)
        let timeInStr = time.toString(dateFormat: Date.DateFormat.HHmm.rawValue, timeZone: TimeZone.current)
        timeStamp.text = timeInStr
        
        if chatType == .single{
           messageStatus.image = data.messageStatus == .read ? AppImages.icReceiveTick.image : AppImages.icSendingTick.image
        }else{
          messageStatus.isHidden = true
          messageStatus.image = data.readCount >= data.memberCount ? AppImages.icReceiveTick.image : AppImages.icSendingTick.image
        }
        
        if data.messageType == .text {
            msgText.text = data.messageText
        }else{
         let completeText = data.messageText + " is added to the shelf"
            msgText.attributedText = completeText.attributeStringWithColors(stringToColor: data.messageText,
                                                                  strClr: AppColors.lightGreyTextColor,
                                                                  substrClr: AppColors.selectedSegmentColor)
        }
        
       contentView.layoutIfNeeded()
    }
}
