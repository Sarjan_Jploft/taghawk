//
//  HeaderCell.swift
//  TagHawk
//
//  Created by Appinventiv on 27/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet weak var headerTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        headerTitle.font = AppFonts.Galano_Regular.withSize(12)
    }
    
    func populateData(data: MessageDetail, groupMembers: [GroupMembers]){
        
        var totalText = ""
        var titleText = ""
        if let userIndex = groupMembers.firstIndex(where: {$0.id == data.senderID}){
            headerTitle.isHidden = false
            titleText = data.senderID == UserProfile.main.userId ? LocalizedString.you.localized : groupMembers[userIndex].name
        }else{
            headerTitle.isHidden = true
            titleText = data.senderID == UserProfile.main.userId ? LocalizedString.you.localized : data.messageText
        }
        
        switch data.messageType{
            
        case .productChangeHeader: return 
            
        case .tagCreatedHeader:
            totalText = titleText + LocalizedString.createdThisTag.localized
            
        case .userJoin, .userRemove, .userLeft:
            if let userIndex = groupMembers.firstIndex(where: {$0.id == data.messageText}){
                headerTitle.isHidden = false
                titleText = data.messageText == UserProfile.main.userId ? LocalizedString.you.localized : groupMembers[userIndex].name
            }else{
                headerTitle.isHidden = true
                titleText = data.messageText == UserProfile.main.userId ? LocalizedString.you.localized : data.messageText
            }
            
            totalText = titleText
            
            switch data.messageType {
                
            case .userJoin: totalText = totalText + LocalizedString.joinedTheTag.localized
                
            case .userRemove: totalText = totalText + LocalizedString.removedFromTag.localized
                
            case .userLeft: totalText = totalText + LocalizedString.leftFromTag.localized
                
            default: return
                
            }
            
        case .ownershipTransfer:
            if let userIndex = groupMembers.firstIndex(where: {$0.id == data.messageText}){
                 headerTitle.isHidden = false
                totalText = LocalizedString.OwnershipTransferTo.localized + (groupMembers[userIndex].name)
                headerTitle.attributedText = totalText.attributeBoldStringWithAnotherColor(stringsToColor: [ (groupMembers[userIndex].name)], size: 12,
                                                                                           strClr: AppColors.lightGreyTextColor,
                                                                                           substrClr: AppColors.selectedSegmentColor)
            }

        case .shelfProduct:
            headerTitle.isHidden = false
            let completeText = titleText + ": " + data.messageText + " is added to the shelf"
            headerTitle.attributedText = completeText.attributeStringWithColors(stringToColor: data.messageText,
                                                                   strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            headerTitle.textColor = AppColors.lightGreyTextColor
            
        case .timeHeader:
             headerTitle.isHidden = false
            let time = Date(timeIntervalSince1970: data.timeInterval)
            let timeInStr = time.toString(dateFormat: Date.DateFormat.HHmm.rawValue,
                                          timeZone: TimeZone.current)
            
            if time.isToday{
                headerTitle.text = LocalizedString.today.localized + ", " + timeInStr
            }else if time.isYesterday{
                headerTitle.text = LocalizedString.yesterday.localized + ", " + timeInStr
            }else{
                let date = time.toString(dateFormat: Date.DateFormat.ddMMM.rawValue,
                                         timeZone: TimeZone.current)
                headerTitle.text = date + ", " + timeInStr
            }
            headerTitle.font = AppFonts.Galano_Medium.withSize(14)
            headerTitle.textColor = AppColors.lightGreyTextColor
            
        default: break
            
        }
        
        if data.messageType != .timeHeader &&  data.messageType != .productChangeHeader && data.messageType != .ownershipTransfer && data.messageType != .shelfProduct {
            headerTitle.isHidden = false
            headerTitle.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
        }
    }
}
