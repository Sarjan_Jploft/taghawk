//
//  SingleChatVC+UItextViewDelegate.swift
//  TagHawk
//
//  Created by Appinventiv on 13/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON
import IQKeyboardManagerSwift


extension SingleChatVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        guard let text = textView.text else {
            return
        }
        
        if text.byRemovingLeadingTrailingWhiteSpaces.isEmpty {
            sendBtn.isEnabled = false
            self.resetFrames()
            return
        }else {
            sendBtn.isEnabled = true
        }
        arrangeTextViewHeight()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let message = messageTextView.text.byRemovingLeadingTrailingWhiteSpaces.trimTrailingWhitespace()
        if message.isEmpty{
            return (text == " ") || (text == "\n") ? false : true
        }
        return true
    }
    
    
    func arrangeTextViewHeight(){
        
        let text = messageTextView.text ?? ""
        let height = text.heightOfText(self.messageTextView.bounds.width - 18, font: AppFonts.Galano_Regular.withSize(14)) + 10
        
        if height > 43 && height < 90 {
            self.textViewHeight.constant = height + 10
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if height < 43{
            resetFrames()
        }
        messageTextView.isScrollEnabled = (height > 90)
    }
    
    func resetFrames() {
        UIView.animate(withDuration: 0.3) {
            self.textViewHeight.constant = 30
            self.view.layoutIfNeeded()
        }
    }
}

extension SingleChatVC: ReceiverCellDelegate {
    
    func profileBtnTapped(button: UIButton){
        
        guard let index = button.tableViewIndexPath(messagesTableView) else { return }
        let data = messageData[index.row]
        let userProfileScene = OtherUserProfileVC.instantiate(fromAppStoryboard: .Profile)
        userProfileScene.userId = data.senderID
        navigationController?.pushViewController(userProfileScene, animated: true)
    }
}

extension SingleChatVC : AttachmentPopupDelegate {
    
    func attachmentBtnTapped(tappedBtn: AttachmentPopUp){
        
        switch tappedBtn {
            
        case .takePhoto: checkAndOpenCamera(delegate: self)
            
        case .gallery: checkAndOpenLibrary(delegate: self)
            
        case .shareProduct: gotoProductList()
            
        case .shareCommunity: gotoTagList()
            
        }
    }
    
    private func gotoTagList() {
        let tagListVC = ChatTagListingVC.instantiate(fromAppStoryboard: .Messages)
        tagListVC.selectedTag = { [weak self] tag in
            
            guard let strongSelf = self else{return}
            
            SingleChatFireBaseController.shared.createSingleChat(roomData: strongSelf.roomInfo,
                                                                 data: strongSelf.productDetail,
                                                                 sharedProduct: nil,
                                                                 sharedTag: tag,
                                                                 userProfile: strongSelf.otherUserData,
                                                                 text: "",
                                                                 msgType: .shareCommunity,
                                                                 vcInstantiated: strongSelf.vcInstantiated)
        }
        let navCon = UINavigationController(rootViewController: tagListVC)
        self.present(navCon, animated: true, completion: nil)
    }
    
    private func gotoProductList() {
        let productListVC = ChatProductListVC.instantiate(fromAppStoryboard: .Messages)
        productListVC.selectedProduct = { [weak self] product in
            
            guard let strongSelf = self else{return}
            
            SingleChatFireBaseController.shared.createSingleChat(roomData: strongSelf.roomInfo,
                                                                 data: strongSelf.productDetail,
                                                                 sharedProduct: product,
                                                                 sharedTag: nil,
                                                                 userProfile: strongSelf.otherUserData,
                                                                 text: "",
                                                                 msgType: .shareProduct,
                                                                 vcInstantiated: strongSelf.vcInstantiated)
        }
        let navCon = UINavigationController(rootViewController: productListVC)
        self.present(navCon, animated: true, completion: nil)
    }
}

extension SingleChatVC: SingleChatFireBaseControllerDelegate {
    
    func addTotalUnreadCount(otherUserData: UserProfile, type: MessageType, text: String, roomInfo: ChatListing?){
        
        switch type {
            
        case .text:
            sendNotification(message: text, otherUserData: otherUserData)
        case .image:
            sendNotification(message: text + " shared a image", otherUserData: otherUserData)
        case .shareProduct:
            sendNotification(message: text + " shared a product", otherUserData: otherUserData)
        case .shareCommunity:
            sendNotification(message: text + " shared a Tag", otherUserData: otherUserData)
        default: return
        }
    }
    
    func getUpdatedRoomData(info: ChatListing){
        
        if roomInfo?.lastMessage.messageID != info.lastMessage.messageID{
            roomInfo?.lastMessage = info.lastMessage
        }else{
            roomInfo = info
        }
    }
    
    func getRoomData(info: ChatListing){
        
        roomInfo = info
        getOtherUserInfo(otherUserID: info.otherUserID)
        if messageData.isEmpty{
            SingleChatFireBaseController.shared.getMessages(roomID: info.roomID,
                                                            timeStamp: info.createdTime)
        }else{
          let msgTimeStamp = messageData.last?.timeStamp ?? Date().unixFirebaseTimestamp
          SingleChatFireBaseController.shared.getNewMessage(msgTimeStamp: msgTimeStamp, roomId: info.roomID)
        }
        
        updateRoomData()
        getUpdatedProductInfo(roomID: roomInfo?.roomID ?? "")
    }
    
    func getOtherUserInfo(otherUserID: String){
        
        SingleChatFireBaseController.shared.getUserInfo(userID: otherUserID, indexPath: IndexPath.init(row: 0, section: 0)) {[weak self](index, userData) in
            guard let strongSelf = self else{ return }
            strongSelf.navigationItem.title = userData.fullName
            strongSelf.roomInfo?.roomName = userData.fullName
            strongSelf.roomInfo?.roomImage = userData.profilePicture
            let visibleCells = strongSelf.messagesTableView.visibleCells
            visibleCells.forEach { (cell) in
//                if let index = cell.tableViewIndexPath(strongSelf.messagesTableView){
                
                    if let receiverCell = cell as? ReceiverCell{
                        receiverCell.userImage.setImage_kf(imageString: strongSelf.roomInfo?.roomImage ?? "",
                                                           placeHolderImage: AppImages.icChatPlaceholder.image,
                                                           loader: true)
                    }
//                }
            }
        }
    }
    

    func getMessageData(messageData: [MessageDetail]){
        
        self.messageData = messageData
        let dropDownDataSource = [LocalizedString.pinToTop.localized, LocalizedString.Mute.localized, LocalizedString.blockUser.localized]

        moreDropDown.dataSource = dropDownDataSource
        messagesTableView.reloadData()
        tableViewScrollToBtm()
        let timeStamp = messageData.isEmpty ? (roomInfo?.createdTime ?? Date().unixFirebaseTimestamp - 2) : (messageData.last?.timeStamp ?? Date().unixFirebaseTimestamp)
        if let roomData = roomInfo {
            SingleChatFireBaseController.shared.getNewMessage(msgTimeStamp: timeStamp,
                                                              roomId: roomData.roomID)
        }
    }
    
    func getNewMessage(messageData: MessageDetail) {

        var isDiminishMessage: Bool = false
        let timeStamp = (self.messageData.isEmpty ? roomInfo?.createdTime : self.messageData.last?.timeStamp) ?? Date().unixFirebaseTimestamp
        
        if messageData.messageType == .image{
            
            isDiminishMessage = timeStamp <= messageData.timeStamp && self.messageData.last?.messageID != messageData.messageID
        }else{
            isDiminishMessage = timeStamp <= messageData.timeStamp && self.messageData.last?.messageID != messageData.messageID
        }
        
        if isDiminishMessage {
            
            if messageData.messageType != .productChangeHeader {
                
                SingleChatFireBaseController.shared.messageStatusChanged(messageData: messageData,
                                                                         roomId: messageData.roomID)
                SingleChatFireBaseController.shared.updateUserReadStatus(messageData: messageData,
                                                                         roomId: messageData.roomID)
                
                if messageData.senderID != UserProfile.main.userId {
                    SingleChatFireBaseController.shared.diminishReadCount(roomID: messageData.roomID,
                                                                          userID: UserProfile.main.userId,
                                                                          isUserBlocked: false,
                                                                          success: {})
                }
            }
            if messageData.messageType == .image,
                messageData.senderID != UserProfile.main.userId{
                self.messageData.append(messageData)
                if self.messageData.count == 1{
                    messagesTableView.reloadData()
                }else{
                    messagesTableView.beginUpdates()
                    messagesTableView.insertRows(at: [IndexPath(row: self.messageData.count - 1, section: 0)], with: .automatic)
                    messagesTableView.endUpdates()
                    tableViewScrollToBtm()
                }
            }else if messageData.messageType != .image{
                self.messageData.append(messageData)
                if self.messageData.count == 1{
                    messagesTableView.reloadData()
                }else{
                    messagesTableView.beginUpdates()
                    messagesTableView.insertRows(at: [IndexPath(row: self.messageData.count - 1, section: 0)], with: .automatic)
                    messagesTableView.endUpdates()
                    tableViewScrollToBtm()
                }
            }
        }
    }

    func tableViewScrollToBtm(){
        messagesTableView.layoutIfNeeded()
        messagesTableView.scrollToBottom(isAnimated: false)
    }
    
    func updateMessageStatus(message: MessageDetail){
        
        if let idx = messageData.firstIndex(where: {$0.messageID == message.messageID}){
            messageData[idx].messageStatus = MessageStatus.read
            messagesTableView.reloadRows(at: [IndexPath(row: idx, section: 0)], with: .automatic)
        }
    }
    
    func anotherUserRemoveRoom() {
        isOtherUseRemoveRoom = true
    }
    
    func getPaginatedData(messages: [MessageDetail]){
        
        isDataLoading = false
        var indexArray: [IndexPath] = []
        if !messages.isEmpty{
            for index in 0...(messages.count - 1){
                indexArray.append(IndexPath(row: index, section: 0))
                messageData.insert(messages[index], at: index)
            }
        }
        messagesTableView.beginUpdates()
        messagesTableView.insertRows(at: indexArray, with: .fade)
        messagesTableView.endUpdates()
    }
    
    func getImageMessage(messageData: MessageDetail){
        
        if let idx = self.messageData.index(where: {!$0.isImageMsgReceived}){
            self.messageData[idx] = messageData
            self.messagesTableView.reloadRows(at: [IndexPath(row: idx, section: 0)], with: .automatic)
            
            SingleChatFireBaseController.shared.messageStatusChanged(messageData: messageData,
                                                                     roomId: messageData.roomID)
            SingleChatFireBaseController.shared.updateUserReadStatus(messageData: messageData,
                                                                     roomId: messageData.roomID)
        }
    }
    
    func userBlocked(){
        view.hideIndicator()
        navigationController?.popToRootViewController(animated: true)
    }
    
    func userBlockedByAnother(roomData: ChatListing){
        if roomData.roomID == (roomInfo?.roomID ?? ""){
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func getUserData(userData: UserProfile, chatType: ChatType){
        
        self.navigationItem.title = userData.fullName
        roomInfo?.roomImage = userData.profilePicture
        roomInfo?.roomName = userData.fullName
        
        if !messageData.isEmpty{
            let visibleCells = messagesTableView.visibleCells
            var visibleIndexPath: [IndexPath] = []
            visibleCells.forEach { (cell) in
                guard let index = cell.tableViewIndexPath(messagesTableView) else{ return}
                if messageData[index.row].senderID != UserProfile.main.userId{
                    visibleIndexPath.append(index)
                }
            }
            messagesTableView.reloadRows(at: visibleIndexPath, with: .none)
        }
    }
    
    func sendNotification(message: String, otherUserData: UserProfile){
        
        if let roomData = roomInfo{
            GroupChatFireBaseController.shared.getRoomData(roomID: roomData.roomID, userID: roomData.otherUserID) {[weak self](chatData) in
                
                self?.otherUserRoomData = chatData
                if !chatData.chatMute{
                    self?.controller.sendNotification(userData: otherUserData,
                                                      roomData: roomData,
                                                      text: message)
                }
            }
        }
    }
    
    func getUpdatedProductInfo(roomID: String){
        
        FireBaseChat.shared.getUpdatedProductInfo(roomID: roomID,
                                                  indexPath: IndexPath(row: 0, section: 0)) {[weak self](productData, indx, roomID) in
                                                    guard let strongSelf = self else{ return }
                                                    if strongSelf.roomInfo?.productData?.productID != productData.productID{
                                                        strongSelf.isGetProductData = true
                                                        strongSelf.roomInfo?.productData = productData
                                                        strongSelf.updateProductData()
                                                    }
        }
    }
}

extension SingleChatVC: DeleteChatDelegate {
    
    func blockFromGroup(){
        navigationController?.popViewController(animated: true)
    }
}


extension SingleChatVC: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[.originalImage] as? UIImage {
            picker.dismiss(animated: true) {
                self.uploadLogoImageToAWS(image: img)
            }
        }
    }
    
    func uploadLogoImageToAWS(image: UIImage) {
        
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        let mesageData = MessageDetail.getBlankImageMessageDetail()
        messageData.append(mesageData)
        self.messagesTableView.beginUpdates()
        self.messagesTableView.insertRows(at: [IndexPath(row: (messageData.count - 1),
                                                         section: 0)], with: .automatic)
        self.messagesTableView.endUpdates()
        tableViewScrollToBtm()
        
        image.uploadImageToS3(imageIndex: (messageData.count - 1),
                              success: {[weak self](imageIndex, success, imageUrl) in
            
            guard let strongSelf = self else{ return }
            
            if success{
                SingleChatFireBaseController.shared.createSingleChat(roomData: strongSelf.roomInfo,
                                                                     data: strongSelf.productDetail,
                                                                     sharedProduct: nil,
                                                                     sharedTag: nil,
                                                                     userProfile: strongSelf.otherUserData,
                                                                     text: imageUrl,
                                                                     msgType: .image,
                                                                     vcInstantiated: strongSelf.vcInstantiated)
            }
        }, progress: { (imageIndex, progress) in
            printDebug("progress:\(progress)")
        }, failure: {[weak self](imageindex, error) in
            CommonFunctions.showToastMessage(error.localizedDescription)
            if let index = self?.messageData.lastIndex(where: {!$0.isImageMsgReceived}){
                self?.messageData.remove(at: index)
                self?.messagesTableView.beginUpdates()
                self?.messagesTableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                self?.messagesTableView.endUpdates()
            }
        })
    }
}

extension SingleChatVC: SingleChatControllerDelegate {
    
    func productHistory(payment: PaymentHistory) {
       self.view.hideIndicator()
        paymentHistory = payment
        updatePaymentStatus()
    }
    
    func willConfirmOrder(){
        self.view.showIndicator()
    }
    
    func confirmOrderSuccessFylly(){
        self.view.hideIndicator()
        paymentHistory?.deliveryStatus = .completed
        updatePaymentStatus()
    }
    
    func failedToConfirmorder(msg : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
    
    func willReleasePayment(){
       self.view.showIndicator()
    }
    
    func paymentReleasedSuccessFully(historyObj : PaymentHistory){
        self.view.hideIndicator()
        paymentHistory = historyObj
        updatePaymentStatus()
    }
    
    func failedToReleasePayment(msg : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
    
    func willRequestRefund(){
        self.view.showIndicator()
    }
    
    func refundreceivedSuccessfully(historyObj : PaymentHistory){
        self.view.hideIndicator()
        paymentHistory = historyObj
        updatePaymentStatus()
    }
    
    func failedToReceiveRefund(msg : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
    
    func willAcceptRefundDelegate(){
        
        self.view.showIndicator()
    }
    
    func refundAcceptedSuccessfully(historyObj : PaymentHistory){
        self.view.hideIndicator()
        paymentHistory = historyObj
        updatePaymentStatus()
    }
    
    func failedToAcceptRefund(msg : String){
        self.view.hideIndicator()
       CommonFunctions.showToastMessage(msg)
    }
    
    func willDeclineRefund(){
        self.view.showIndicator()
    }
    
    func declineSuccessFully(historyObj : PaymentHistory){
        self.view.hideIndicator()
        paymentHistory = historyObj
        updatePaymentStatus()
    }
    
    func failedToDecline(msg : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
    
    func willReleaseRefund(){
        self.view.showIndicator()
    }
    
    func refundReleasedSuccessfully(historyObj : PaymentHistory){
        self.view.hideIndicator()
        paymentHistory = historyObj
        updatePaymentStatus()
    }
    
    func failedToReleaseRefund(msg : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
    
    func willMmakeDespute(){
        self.view.showIndicator()
    }
    
    func desputeMadeSuccessfully(historyObj : PaymentHistory){
        self.view.hideIndicator()
        paymentHistory = historyObj
        updatePaymentStatus()
    }
    
    func failedToMakeDespute(msg : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
    
    func updatePaymentStatus(){
        
        self.updateProductViewData()
    }
    
    private func updateProductViewData(){
        
        if let payment = paymentHistory {
            if !payment._id.isEmpty{
                
                if payment.sellerId == UserProfile.main.userId {
                    if payment.userId == roomInfo?.otherUserID {
                        userStatus = .seller
                        
                        paymentConfirmView.isHidden = false
                        paymentConfirmationLbl.isHidden = false
                    }else{
                        paymentConfirmationLbl.text = ""
                        moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                        refundBtnHeight.constant = CGFloat.leastNormalMagnitude
                        paymentConfirmView.isHidden = true
                        paymentConfirmationLbl.isHidden = true
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = true
                        return
                    }
                }else{
                    if payment.userId == UserProfile.main.userId {
                        userStatus = .buyer
                        paymentConfirmView.isHidden = false
                        paymentConfirmationLbl.isHidden = false
                    }else{
                        paymentConfirmationLbl.text = ""
                        moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                        refundBtnHeight.constant = CGFloat.leastNormalMagnitude
                        paymentConfirmView.isHidden = true
                        paymentConfirmationLbl.isHidden = true
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = true
                        return
                    }
                }
                paymentConfirmViewHeight.constant = 30
                moreInfoBtn.isHidden = true//false
                productStatus.isHidden = false
                //Temp Code
                if userStatus == .seller{
                    paymentConfirmationLbl.text = LocalizedString.contactToBuyerForOtherPay.localized
                }else{
                    paymentConfirmationLbl.text = LocalizedString.contactToSellerForOtherPay.localized
                }
                
                /*
                 use this code when payment api is going on live.
                switch payment.deliveryStatus
                {
                    
                case .pending:
                    if let idx = controller.products.firstIndex(where: {$0.id == payment.productId}){
                        controller.products[idx].productStatus = .pending
                        productDropdown.reloadAllComponents()
                        self.productStatus.textColor = AppColors.appBlueColor
                        self.productStatus.text = controller.products[idx].productStatus.value
                        self.productStatus.font = AppFonts.Galano_Regular.withSize(14)
                        
                    }
                    
                    moreBtnHeight.constant = 30
                    if userStatus == .seller {
                        let pendingText = LocalizedString.PENDINGPICKUPCONFIRMATION.localized
                        let otherText = LocalizedString.contactToBuyerForOtherPay.localized//LocalizedString.inPersonPickupOption.localized
                        
                        //paymentConfirmationLbl.text = (pendingText + otherText)
                        paymentConfirmationLbl.text =  otherText
                        refundBtnHeight.constant = CGFloat.leastNormalMagnitude
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = true
                        refundBtnHeight.constant = CGFloat.leastNormalMagnitude
                    }else{
                        refundBtn.isHidden = false
                        confirmBtn.isHidden = false
                        refundBtn.setTitle(LocalizedString.Refund.localized)
                        confirmBtn.setTitle(LocalizedString.Confirm.localized)
                        refundBtnHeight.constant = 40
                        paymentConfirmationLbl.text = LocalizedString.contactToSellerForOtherPay.localized//LocalizedString.inPersonPickupOption.localized//LocalizedString.InspectAndConfirmProduct.localized
                    }
                    
                case .requestSuccess:
                    moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                    refundBtnHeight.constant = CGFloat.leastNormalMagnitude
                    moreInfoBtn.isHidden = true
                    
                    if let idx = controller.products.firstIndex(where: {$0.id == payment.productId}){
                        controller.products[idx].productStatus = .selling
                        productDropdown.reloadAllComponents()
                        self.productStatus.text = controller.products[idx].productStatus.value
                    }
                    
                    if userStatus == .seller {
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = true
                        refundBtn.setTitle(LocalizedString.Decline.localized)
                        confirmBtn.setTitle(LocalizedString.Confirm.localized)
                        let completeText = LocalizedString.orderComplete.localized
                        paymentConfirmationLbl.text = completeText
                    }else{
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = true
                        confirmBtn.setTitle(LocalizedString.cancelRefundRequest.localized)
                        let completeText = "Order Complete: \(roomInfo?.roomName ?? "") has released the refund"
                        paymentConfirmationLbl.text = completeText
                    }
                    
                case .completed, .disputeCompleted:
                    
                    if payment.deliveryStatus == .completed {
                        
                        if let idx = controller.products.firstIndex(where: {$0.id == payment.productId}){
                            controller.products[idx].productStatus = .sold
                            let product = controller.products[idx]
                            controller.products.remove(at: idx)
                            controller.products.append(product)
                            productDropdown.reloadAllComponents()
                            self.productStatus.text = product.productStatus.value
                            self.productStatus.textColor = AppColors.blackColor
                            self.productStatus.font = AppFonts.Galano_Medium.withSize(15)
                        }
                    }
                    
                    paymentConfirmationLbl.text =  userStatus == .seller ? "\(roomInfo?.roomName ?? "") has released the payment. \n Check your wallet." : "Order Complete \n You have released the payment."
                    paymentConfirmViewHeight.constant = 30
                    moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                    moreInfoBtn.isHidden = true
                    refundBtnHeight.constant = CGFloat.leastNormalMagnitude
                    refundBtn.isHidden = true
                    confirmBtn.isHidden = true
                    
                case .refundAccepted:
                    moreBtnHeight.constant = 30
                    
                    if userStatus == .seller {
                        refundBtnHeight.constant = 40
                        refundBtn.isHidden = false
                        confirmBtn.isHidden = false
                        refundBtn.setTitle(LocalizedString.Decline.localized)
                        confirmBtn.setTitle(LocalizedString.Confirm.localized)
                        let completeText = LocalizedString.acceptRefundRequest.localized //"Order Complete \n You have released the payment."
                        paymentConfirmationLbl.text = completeText
                    }else{
                        refundBtnHeight.constant = 40
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = false
                        confirmBtn.setTitle(LocalizedString.cancelRefundRequest.localized)
                        let completeText = "\(roomInfo?.roomName ?? "")" + " " + LocalizedString.acceptsRefundRequest.localized //"\(chatController.roomData?.roomName ?? "") has released the payment Check your wallet"
                        paymentConfirmationLbl.text = completeText
                    }
                    
                case .requestForRefund:
                    
                    moreBtnHeight.constant = 30
                    if userStatus == .seller {
                        refundBtn.isHidden = false
                        confirmBtn.isHidden = false
                        refundBtn.setTitle(LocalizedString.Decline.localized)
                        confirmBtn.setTitle(LocalizedString.Accept.localized)
                        refundBtnHeight.constant = 40
                        let completeText = "\(roomInfo?.roomName ?? "")" + LocalizedString.initiateRefundRequest.localized
                        paymentConfirmationLbl.text = completeText
                    }else{
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = false
                        confirmBtn.setTitle(LocalizedString.cancelRefundRequest.localized)
                        let completeText = LocalizedString.responseIn5Days.localized
                        paymentConfirmationLbl.text = completeText
                        refundBtnHeight.constant = 40
                    }
                    
                case .declined:
                    
                    if userStatus == .seller {
                        moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                        refundBtnHeight.constant = CGFloat.leastNormalMagnitude
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = true
                        moreInfoBtn.isHidden = true
                        let completeText = "You declined the refund request due to the following reason: \n \(payment.reason)"
                        paymentConfirmationLbl.text = completeText
                    }else{
                        moreBtnHeight.constant = 30
                        refundBtn.isHidden = false
                        confirmBtn.isHidden = false
                        moreInfoBtn.isHidden = false
                        refundBtnHeight.constant = 40
                        refundBtn.setTitle(LocalizedString.Despute.localized)
                        confirmBtn.setTitle(LocalizedString.Cancel_Refund.localized)
                        let completeText = "\(roomInfo?.roomName ?? "") declined the refund request due to the following reason: \n \(payment.reason)"
                        paymentConfirmationLbl.text = completeText
                    }
                    
                case .disputeStarted:
                    moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                    moreInfoBtn.isHidden = true
                    if userStatus == .seller {
                        refundBtn.isHidden = false
                        confirmBtn.isHidden = false
                        refundBtnHeight.constant = 40
                        refundBtn.setTitle(LocalizedString.SubmitStatement.localized)
                        confirmBtn.setTitle(LocalizedString.Accept_Refund.localized)
                        let completeText = "\(roomInfo?.roomName ?? "") has initiated  a dispute on this order. You have 5 days to submit your statement, or choose to accept the refund request."
                        paymentConfirmationLbl.text = completeText
                    }else{
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = false
                        refundBtnHeight.constant = 40
                        confirmBtn.setTitle(LocalizedString.cancelRefundRequest.localized)
                        let completeText = LocalizedString.submittedDisputeStatement.localized
                        paymentConfirmationLbl.text = completeText
                    }
                    
                case .disputeCanStart:
                    
                    if userStatus == .seller {
                        
                        moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                        refundBtnHeight.constant = CGFloat.leastNormalMagnitude
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = true
                        moreInfoBtn.isHidden = true
                        moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                        paymentConfirmationLbl.text = LocalizedString.YouNotRespondWithIn5Days.localized
                        
                    }else{
                        moreBtnHeight.constant = 30
                        refundBtn.isHidden = false
                        confirmBtn.isHidden = false
                        moreInfoBtn.isHidden = false
                        refundBtnHeight.constant = 40
                        refundBtn.setTitle(LocalizedString.Despute.localized)
                        confirmBtn.setTitle(LocalizedString.Cancel_Refund.localized)
                        let completeText = LocalizedString.notRespondWithIn5Days.localized
                        paymentConfirmationLbl.text = (roomInfo?.roomName ?? "") + completeText
                    }
                    
                case .sellarStatementDone:
                    moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                    moreInfoBtn.isHidden = true
                    if userStatus == .seller {
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = false
                        refundBtnHeight.constant = 40
                        confirmBtn.setTitle(LocalizedString.AcceptRefundRequest.localized)
                        let completeText = LocalizedString.submittedDisputeStatement.localized
                        paymentConfirmationLbl.text = completeText
                    }else{
                        refundBtn.isHidden = true
                        confirmBtn.isHidden = false
                        refundBtnHeight.constant = 40
                        confirmBtn.setTitle(LocalizedString.cancelRefundRequest.localized)
                        let completeText = LocalizedString.submittedDisputeStatement.localized
                        paymentConfirmationLbl.text = completeText
                    }
                default: return
                
                }
                */
                
                /*
                if let idx = controller.products.firstIndex(where: {$0.id == payment.productId}){
                    controller.products[idx].productStatus = .pending
                    productDropdown.reloadAllComponents()
                    self.productStatus.textColor = AppColors.appBlueColor
                    self.productStatus.text = controller.products[idx].productStatus.value
                    self.productStatus.font = AppFonts.Galano_Regular.withSize(14)
                   
                    let product = controller.products[idx]
                    self.productStatus.text = product.productStatus.value
                    self.productStatus.textColor = product.productStatus == .sold ? AppColors.blackColor : AppColors.appBlueColor
                    self.productStatus.font = product.productStatus == .sold ? AppFonts.Galano_Medium.withSize(15) : AppFonts.Galano_Regular.withSize(14)
                    
                }
                */
                
                
            }else{
                moreBtnHeight.constant = CGFloat.leastNormalMagnitude
                paymentConfirmViewHeight.constant = CGFloat.leastNormalMagnitude
                refundBtnHeight.constant = CGFloat.leastNormalMagnitude
                paymentConfirmView.isHidden = true
                refundBtn.isHidden = true
                confirmBtn.isHidden = true
                paymentConfirmationLbl.isHidden = true
                paymentConfirmationLbl.text = ""
                productStatus.isHidden = true
            }
        }else{
            moreBtnHeight.constant = CGFloat.leastNormalMagnitude
            paymentConfirmViewHeight.constant = CGFloat.leastNormalMagnitude
            refundBtnHeight.constant = CGFloat.leastNormalMagnitude
            paymentConfirmView.isHidden = true
            refundBtn.isHidden = true
            confirmBtn.isHidden = true
            paymentConfirmationLbl.isHidden = true
            paymentConfirmationLbl.text = ""
            productStatus.isHidden = true
        }
        view.layoutIfNeeded()
    }
    
    
    func getProductData(){
        
        UIView.animate(withDuration: 0.3) {
            self.productViewHeightConstraint.constant = self.controller.products.isEmpty ? 0 : 100
        }
        view.layoutIfNeeded()
        populateProductData(isViewHidden: controller.products.isEmpty)
        guard !controller.products.isEmpty else { return }
        // in product detail we update data in view did load
        if vcInstantiated == .messageList || vcInstantiated == .userDetail{
           updateProductData()
        }else{
            productDropdown.dataSource = controller.products.map({$0.productName})
            productDropdown.reloadAllComponents()
        }
    }
    
    func updateProductData(){
        
        populateProductData(isViewHidden: controller.products.isEmpty)
        var selctedProductId: String = ""
        
        switch vcInstantiated {
            
        case .productDetail:
            selctedProductId = (isGetProductData ? roomInfo?.productData?.productID : productDetail?.productId) ?? ""
            if let product = productDetail,
                let productData = roomInfo?.productData,
                product.productId == productData.productID{
                selctedProductId = product.productId
            }
            
            if isGetProductData,
                let product = productDetail,
                let productData = roomInfo?.productData,
                product.productId != productData.productID{
                productDetail?.productId = roomInfo?.productData?.productID ?? ""
                productDetail?.images[0].thumbImage = roomInfo?.productData?.productImage ?? ""
                productDetail?.title = roomInfo?.productData?.productName ?? ""
                productDetail?.firmPrice = roomInfo?.productData?.productPrice ?? Double.leastNormalMagnitude
            }

        case .messageList, .userDetail:
            
            if let productData = roomInfo?.productData,
                !productData.productID.isEmpty{
                selctedProductId = productData.productID
            }else{
                if let roomInfo = roomInfo,
                    !controller.products.isEmpty{
                    SingleChatFireBaseController.shared.updateProductInfo(product: controller.products.first ?? UserProduct(),
                                                     roomId: roomInfo.roomID,
                                                     otherUserID: roomInfo.otherUserID)
                    if let product = controller.products.first {
                        controller.getProductPaymentHistory(id: product.id)
                    }
                }
            }
        }
        
        if !controller.products.isEmpty {
            let selectedIdx = controller.products.index(where: {$0.id == selctedProductId}) ?? 0
            let selectedData = controller.products[selectedIdx]
            selectedProductID = selectedData.id
            productImage.setImage_kf(imageString: selectedData.images.first?.image ?? "",
                                     placeHolderImage: AppImages.productPlaceholder.image)
            productPrice.text = "$\(selectedData.firmPrice)"
            productTitle.text = selectedData.productName
            
            productStatus.textColor = selectedData.productStatus == .sold ? AppColors.blackColor : AppColors.appBlueColor
            productStatus.font = selectedData.productStatus == .sold ? AppFonts.Galano_Medium.withSize(15) : AppFonts.Galano_Regular.withSize(14)
            productStatus.text = selectedData.productStatus.value
            controller.getProductPaymentHistory(id: selctedProductId)
        }else{
            if let roomInfo = roomInfo,
                let productData = roomInfo.productData{
                productImage.setImage_kf(imageString: productData.productImage,
                                         placeHolderImage: AppImages.productPlaceholder.image)
                productPrice.text = "$\(productData.productPrice)"
                productTitle.text = productData.productName
                selectedProductID = productData.productID
            }
        }
        
        productDropdown.dataSource = controller.products.map({$0.productName})
        productDropdown.reloadAllComponents()
    }
    
    private func getProductHistory(){
        
        switch vcInstantiated {
            
        case .messageList:
            let selctedProductId = roomInfo == nil ? (productDetail?.productId ?? "") : roomInfo?.productData?.productID ?? ""
            controller.getProductPaymentHistory(id: selctedProductId)
       
        case .userDetail:
            if let productData = roomInfo?.productData,
                !productData.productID.isEmpty{
                
                controller.getProductPaymentHistory(id: productData.productID)
            }else{
                if let product = controller.products.first {
                    controller.getProductPaymentHistory(id: product.id)
                }
            }
            
        case .productDetail:
            if let productData = roomInfo?.productData,
                !productData.productID.isEmpty{
                
                controller.getProductPaymentHistory(id: productData.productID)
            }
        }
    }
    
    func getError(error: String){
        view.hideIndicator()
        CommonFunctions.showToastMessage(error)
    }
    
    func blockedUser(otherUserID: String){
        SingleChatFireBaseController.shared.blockedUser(userID: UserProfile.main.userId,
                                   otherUserID: otherUserID)
    }
}

extension SingleChatVC: RemoveFriendPopUpDelegate {
    
    func yesButtonTapped(type: RemoveFriendPopUp.RemoveFriendFor,
                         user: FollowingFollowersModel) {
       
        if type == .block{
            view.showIndicator()
            controller.blockedUser(otherUserID: user.userId)
        }
    }
}

extension SingleChatVC : CustomPopUpDelegateWithType{
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type : CustomPopUpVC.CustomPopUpFor, orderObj : PaymentHistory){
        
        switch type {
            
        case .releasePayment, .cancelRefund:
            self.controller.releasePayment(orderId: orderObj._id)
            
        case .refundRequest:
            if orderObj.sellerId != UserProfile.main.userId {
                self.controller.requestRefund(orderId: orderObj._id)
            }
            
        case .declineRefundRequest:
            if orderObj.sellerId == UserProfile.main.userId {
                // self.controler.declineRefund(orderId: orderObj._id, msg: <#String#>)
            }
            
        case .acceptRefundRequest:
            if orderObj.sellerId == UserProfile.main.userId {
                self.controller.acceptRefund(orderId: orderObj._id)
            }
            
        case .releaseRefund:
            if orderObj.sellerId == UserProfile.main.userId {
                self.controller.releaseRefund(orderId: orderObj._id)
            }
            
        case .despute:
            if userStatus == .buyer {
                self.controller.releasePayment(orderId: orderObj._id)
            }

            
        default:
            break
        }
    }
}

extension SingleChatVC : GetReasonToDeclineBackDelegate{
    
    func getReasonBack(msg : String, history : PaymentHistory){
        if userStatus == .seller {
            self.controller.declineRefund(orderId: history._id, msg: msg)
        }
    }
}

extension SingleChatVC: GetDisputeReasons{
    
    func getDisputeReasonsBack(mssg : String, images: [String], paymentHistory : PaymentHistory){
        
        self.controller.makeDispute(orderId: paymentHistory._id,
                                    msg: mssg,
                                    images: images)
    }
}
