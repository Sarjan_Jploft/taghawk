//
//  SingleChatVC.swift
//  TagHawk
//
//  Created by Appinventiv on 13/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import DropDown

enum SingleChatInstantiateForm{
    case productDetail
    case messageList
    case userDetail
}

enum UserStatus {
    
    case buyer
    case seller
    case none
}

class SingleChatVC: BaseVC {
    
    //    MARK:- Proporties
    //    =================
    var productDetail: Product?
    var otherUserData: UserProfile?
    let moreDropDown = DropDown()
    let productDropdown = DropDown()
    var vcInstantiated: SingleChatInstantiateForm = .productDetail
    let controller = SingleChatController()
    var selectedProductID: String = ""
    var isOtherUseRemoveRoom: Bool = false
    var isFromSearchScreen: Bool = false
    var messageText: String = ""
    var otherUserRoomData: ChatListing?
    var paymentHistory: PaymentHistory?
    var userStatus: UserStatus = .none
    var isGetProductData: Bool = false // value true when once user set the product data through product detail
    var messageData: [MessageDetail] = []
    var roomInfo: ChatListing?
    var isDataLoading: Bool = false
    
    //    MARK:- IBOutlets
    //    =================
    @IBOutlet weak var writeMessageView: UIView!
    @IBOutlet weak var messageTextView: IQTextView!
    @IBOutlet weak var messagesTableView: UITableView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productStatus: UILabel!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productArrowImage: UIImageView!
    @IBOutlet weak var productViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var paymentConfirmView: UIView!
    @IBOutlet weak var paymentConfirmationLbl: UILabel!
    @IBOutlet weak var refundBtn: UIButton!
    @IBOutlet weak var refundBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var moreInfoBtn: UIButton!
    @IBOutlet weak var moreBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentConfirmViewHeight: NSLayoutConstraint!
    
    //    MARK:- ViewController Life Cycle
    //    ================================
    override func viewDidLoad() {
        super.viewDidLoad()
       
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavigation()
        SingleChatFireBaseController.shared.delegate = self
        
        switch vcInstantiated {
            
        case .productDetail:
            controller.getProductPaymentHistory(id: productDetail?.productId ?? "")
            SingleChatFireBaseController.shared.checkRoomExist(otherUserID: productDetail?.userId ?? "")
            
        case .userDetail:
            SingleChatFireBaseController.shared.checkRoomExist(otherUserID: otherUserData?.userId ?? "")
        default:
            SingleChatFireBaseController.shared.checkRoomExist(otherUserID: roomInfo?.otherUserID ?? "")
        }
        self.addKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enable = true
        SingleChatFireBaseController.shared.removeObservers(roomId: roomInfo?.roomID ?? "",
                                                            otherUserId: roomInfo?.otherUserID ?? "")
        self.removeKeyboard()
    }
    
    deinit {
        SingleChatFireBaseController.shared.removeObservers(roomId: roomInfo?.roomID ?? "",
                                                            otherUserId: roomInfo?.otherUserID ?? "")
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func leftBarButtonTapped() {
        self.view.endEditing(true)
        if isFromSearchScreen{
            navigationController?.popToRootViewController(animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func rightBarButtonTapped(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        moreDropDown.anchorView = sender
        moreDropDown.bottomOffset = CGPoint(x: 0, y: moreDropDown.anchorView!.plainView.bounds.height)
        moreDropDown.show()
    }
    
    //    MARK:- IBActions
    //    ================
    @IBAction func sendBtnTapped(_ sender: Any) {
        
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        let message = messageTextView.text.byRemovingLeadingTrailingWhiteSpaces.trimTrailingWhitespace()
        
        if !message.isEmpty{
            SingleChatFireBaseController.shared.createSingleChat(roomData: roomInfo,
                                                                 data: productDetail,
                                                                 sharedProduct: nil,
                                                                 sharedTag: nil,
                                                                 userProfile: otherUserData,
                                                                 text: message,
                                                                 msgType: .text,
                                                                 vcInstantiated: self.vcInstantiated)
        }
        sendBtn.isEnabled = false
        messageTextView.text = ""
        arrangeTextViewHeight()
    }
    
    @IBAction func attachBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        let scene = AttachtmentPopUpVC.instantiate(fromAppStoryboard: .Messages)
        scene.modalTransitionStyle = .coverVertical
        scene.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
        scene.delegate = self
        AppNavigator.shared.parentNavigationControler.present(scene, animated: true, completion: nil)
    }
    
    @IBAction func productViewBtnTapped(_ sender: UIButton) {
        guard !controller.products.isEmpty else{ return }
        let vc = ProductDetailVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.prodId = selectedProductID
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    @IBAction func productBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            productDropdown.show()
        }else{
            productDropdown.hide()
        }
    }
    
    @IBAction func refundBtnTapped(_ sender: UIButton) {
        refundBtnTapped()
    }
    
    @IBAction func confirmBtnTapped(_ sender: UIButton) {
        confirmBtnTapped()
    }
    
    @IBAction func moreInfobtnTapped(_ sender: UIButton) {
        let vc = MoreInfoVC.instantiate(fromAppStoryboard: AppStoryboard.Gift)
        vc.history = paymentHistory
        vc.status = userStatus
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }

    private func confirmBtnTapped(){
        
        if let historyObj = paymentHistory {
            
            switch historyObj.deliveryStatus {
                
            case .pending:
                self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.releasePayment, history: historyObj)
                
            case .requestForRefund, .disputeCanStart:
                if userStatus == .seller{
                   self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.acceptRefundRequest, history: historyObj)
                }else{
                  self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.cancelRefund, history: historyObj)
                }
                
            case .refundAccepted:
                if userStatus == .seller {
                   self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.releaseRefund, history: historyObj)
                }else{
                   self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.cancelRefund, history: historyObj)
                }
                
            case .declined:
                if userStatus == .buyer {
                  //dispute
                    self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.despute, history: historyObj)
                }
                
            case .disputeStarted:
                if userStatus == .seller {
                    self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.acceptRefundRequest, history: historyObj)
                }else{
                  self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.cancelRefund, history: historyObj)
                }
                
            case .sellarStatementDone:
                if userStatus == .seller {
                    self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.acceptRefundRequest, history: historyObj)
                }else{
                    self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.cancelRefund, history: historyObj)
                }
            default: return
                
            }
        }
    }
    
    private func refundBtnTapped(){
        
        if let historyObj = paymentHistory {
            
            switch historyObj.deliveryStatus {
                
            case .pending:
                self.showPopUp(type: CustomPopUpVC.CustomPopUpFor.refundRequest, history: historyObj)
                
            case .requestForRefund, .refundAccepted:
                if userStatus == .seller{
                    self.showDeclinePopUp()
                }
                
            case .declined, .disputeCanStart:
                
                if userStatus == .buyer {
                    
                    let documentVC = DisputePopUpVC.instantiate(fromAppStoryboard: .Settings)
                    documentVC.serviceReturnSuccess = { }
                    documentVC.delegate = self
                    documentVC.paymentHistory = historyObj
                    documentVC.modalPresentationStyle = .overCurrentContext
                    AppNavigator.shared.parentNavigationControler.present(documentVC, animated: true, completion: nil)
                }
                
            case .disputeStarted:
                
                if userStatus == .seller {
                    let documentVC = DisputePopUpVC.instantiate(fromAppStoryboard: .Settings)
                    documentVC.serviceReturnSuccess = { }
                    documentVC.delegate = self
                    documentVC.paymentHistory = historyObj
                    documentVC.modalPresentationStyle = .overCurrentContext
                    AppNavigator.shared.parentNavigationControler.present(documentVC, animated: true, completion: nil)
                }
                
            default:
                break
            }
        }
    }
    
    private func showDeclinePopUp(){
        
        if let history = paymentHistory {
            let vc = GetReasonVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
            vc.delegate = self
            vc.historyObj = history
            vc.modalPresentationStyle = .overCurrentContext
            AppNavigator.shared.parentNavigationControler.present(vc, animated: true, completion: nil)
        }
    }
    
    func showPopUp(type: CustomPopUpVC.CustomPopUpFor, history : PaymentHistory) {
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        vc.paymentHistory = history
        vc.modalPresentationStyle = .overCurrentContext
        AppNavigator.shared.parentNavigationControler.present(vc, animated: true, completion: nil)
    }
    
    private func addKeyboard(){
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: {[weak self] (notification) in
                                                guard let info = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
                                                
                                                guard let strongSelf = self else {return}
                                                
                                                let keyBoardHeight = info.cgRectValue.height
                                                
                                                var safeAreaBottomInset : CGFloat = 0
                                                if #available(iOS 11.0, *) {
                                                    safeAreaBottomInset = strongSelf.view.safeAreaInsets.bottom
                                                } else {
                                                    // Fallback on earlier versions
                                                }
                                                //
                                                UIView.animate(withDuration: 0.1,  delay: 0,
                                                               options: .curveEaseInOut,
                                                               animations: {
                                                                
                                                                if (info.cgRectValue.origin.y) >= screenHeight {
                                                                    strongSelf.textViewBtnConstraint.constant = 0
                                                                } else {
                                                                    
                                                                    strongSelf.textViewBtnConstraint.constant = (keyBoardHeight - safeAreaBottomInset)
                                                                }
//                                                                strongSelf.tableViewScrollToBtm()

                                                                strongSelf.view.layoutIfNeeded()
                                                                
                                                }, completion: nil)
                                                
        })
    }
    
    private func removeKeyboard(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
}

//MARK:- Methods
//==============
extension SingleChatVC {
    
    private func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.rightBarItemImage(change: AppImages.icMore.image, ofVC: self)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
    
    private func initialSetup(){
        
        paymentConfirmView.isHidden = true
        paymentConfirmView.cornerRadius = 10.0
        paymentConfirmView.backgroundColor = AppColors.appBlueColor
        paymentConfirmationLbl.textColor = AppColors.whiteColor
        refundBtnHeight.constant = CGFloat.leastNormalMagnitude
        paymentConfirmViewHeight.constant = CGFloat.leastNormalMagnitude
        refundBtn.layer.cornerRadius = 4.0
        refundBtn.clipsToBounds = true
        refundBtn.titleLabel?.font = AppFonts.Galano_Semi_Bold.withSize(16)
        refundBtn.borderColor = AppColors.blackColor
        refundBtn.borderWidth = 1.0
        refundBtn.isHidden = true
        confirmBtn.isHidden = true
        moreBtnHeight.constant = CGFloat.leastNormalMagnitude
        moreInfoBtn.isHidden = true
        confirmBtn.titleLabel?.font = AppFonts.Galano_Semi_Bold.withSize(16)
        productViewHeightConstraint.constant = 0
        messageTextView.placeholder = LocalizedString.writeYourMsg.localized
        messageTextView.placeholderTextColor = AppColors.textViewPlaceholderColor
        messageTextView.font = AppFonts.Galano_Regular.withSize(14)
        messageTextView.text = ""
        messageTextView.tintColor = AppColors.blackLblColor
        writeMessageView.drawShadow(shadowColor: AppColors.blackLblColor, shadowOpacity: 0.1, shadowPath: nil, shadowRadius: 6.0, cornerRadius: 0.0, offset: CGSize(width: 0, height: -3))
        writeMessageView.clipsToBounds = false
        messageTextView.delegate = self
        productImage.cornerRadius = 5.0
        productImage.borderWidth = 1.0
        productImage.borderColor = AppColors.whiteColor
        productPrice.font = AppFonts.Galano_Semi_Bold.withSize(12)
        productPrice.textColor = AppColors.blackLblColor
        productTitle.font = AppFonts.Galano_Regular.withSize(12)
        productTitle.textColor = AppColors.blackLblColor
        
        productArrowImage.image = AppImages.icFilterDropdown.image
        IQKeyboardManager.shared.enable = false
        
        var otherUserID: String = ""
        
        switch vcInstantiated {
            
        case .productDetail:
            otherUserID = productDetail?.userId ?? ""
            productImage.setImage_kf(imageString: productDetail?.images.first?.image ?? "", placeHolderImage: AppImages.productPlaceholder.image)
            productPrice.text = "$\(productDetail?.firmPrice ?? 0)"
            productTitle.text = productDetail?.title
            let status = ProductStatus(value: productDetail?.productStatus ?? 3)
            productStatus.textColor = status == .sold ? AppColors.blackColor : AppColors.appBlueColor
            productStatus.font = status == .sold ? AppFonts.Galano_Medium.withSize(15) : AppFonts.Galano_Regular.withSize(14)
            productStatus.text = status.value
            self.navigationItem.title = productDetail?.fullName
            controller.getProductDetail(otherUserID: productDetail?.userId ?? "")
            if let productData = productDetail {
                controller.getProductPaymentHistory(id: productData.productId)
            }
            
        case .messageList:
            otherUserID = roomInfo?.otherUserID ?? ""
            productImage.setImage_kf(imageString: roomInfo?.productData?.productImage ?? "", placeHolderImage: AppImages.productPlaceholder.image)
            productPrice.text = "$\(roomInfo?.productData?.productPrice ?? 0.0)"
            productTitle.text = roomInfo?.productData?.productName
            self.navigationItem.title = roomInfo?.roomName
            controller.getProductDetail(otherUserID: roomInfo?.otherUserID ?? "")
            
        case .userDetail:
            otherUserID = otherUserData?.userId ?? ""
            if let data = roomInfo{
                productImage.setImage_kf(imageString: data.productData?.productImage ?? "", placeHolderImage: AppImages.productPlaceholder.image)
                productPrice.text = "$\(data.productData?.productPrice ?? 0.0)"
                productTitle.text = data.productData?.productName
                
                populateProductData(isViewHidden: false)
            }else{
                populateProductData(isViewHidden: true)
            }
            
            self.navigationItem.title = otherUserData?.fullName
            controller.getProductDetail(otherUserID: otherUserData?.userId ?? "")
        }
        
        SingleChatFireBaseController.shared.getUserInfo(userID: otherUserID,
                                                        indexPath: IndexPath(item: 0, section: 0),
                                                        success: {[weak self](index, userData) in
                                                            
                                                            guard let strongSelf = self else{ return }
                                                            strongSelf.navigationItem.title = userData.fullName
                                                            strongSelf.roomInfo?.roomImage = userData.profilePicture
                                                            strongSelf.roomInfo?.roomName = userData.fullName
                                                            
                                                            if !strongSelf.messageData.isEmpty{
                                                                let visibleCells = strongSelf.messagesTableView.visibleCells
                                                                var visibleIndexPath: [IndexPath] = []
                                                                visibleCells.forEach { (cell) in
                                                                    guard let index = cell.tableViewIndexPath(strongSelf.messagesTableView) else{ return}
                                                                    if strongSelf.messageData[index.row].senderID != UserProfile.main.userId{
                                                                        visibleIndexPath.append(index)
                                                                    }
                                                                }
                                                                strongSelf.messagesTableView.reloadRows(at: visibleIndexPath, with: .none)
                                                            }
                                                            
        })
        messagesTableView.delegate = self
        messagesTableView.dataSource = self
        
        controller.delegate = self
        sendBtn.isEnabled = false
        paymentConfirmationLbl.font = AppFonts.Galano_Regular.withSize(14)
        setupMoreDropdown()
        setupProductDropdown()
        if let roomData = roomInfo{
            SingleChatFireBaseController.shared.changeInRoomInfo(roomID: roomData.roomID)
            getUpdatedProductInfo(roomID: roomData.roomID)
        }
    }
    
    func populateProductData(isViewHidden: Bool){
        productView.isHidden = isViewHidden
        productArrowImage.isHidden = isViewHidden
        productImage.isHidden = isViewHidden
        productTitle.isHidden = isViewHidden
        productPrice.isHidden = isViewHidden
        productViewHeightConstraint.constant = isViewHidden ? 0 : 100
    }
    
    fileprivate func setupMoreDropdown(){
        
        let dropDownDataSource = [LocalizedString.pinToTop.localized, LocalizedString.Mute.localized, LocalizedString.blockUser.localized]
        
        moreDropDown.dataSource = dropDownDataSource
        moreDropDown.width = 150
        moreDropDown.backgroundColor = AppColors.whiteColor
        moreDropDown.cellNib = UINib(nibName: PinToTopCell.defaultReuseIdentifier, bundle: nil)

        moreDropDown.customCellConfiguration = {[weak self](index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? PinToTopCell else { return }
            cell.delegate = self
            cell.toggleBtn.tag = index
            cell.toggleBtn.isHidden = !(index == 0 || index == 1)
            cell.toggleBtn.isSelected = index == 0 ? (self?.roomInfo?.pinned ?? false) : (self?.roomInfo?.chatMute ??  false)
        }
        
        moreDropDown.dismissMode = .automatic
        
        moreDropDown.selectionAction = { [weak self] (index: Int, item: String) in
            
            guard let strongSelf = self else { return }
            
            switch index {
                
            case 2:
                let vc = RemoveFriendPopUp.instantiate(fromAppStoryboard: AppStoryboard.Profile)
                vc.commingFor = .block
                let data = FollowingFollowersModel()
                switch strongSelf.vcInstantiated {
                    
                case .productDetail:
                    data.userId = strongSelf.productDetail?.userId ?? ""
                    data.profilePicture = strongSelf.productDetail?.profilePicture ?? ""
                    data.fullName = strongSelf.productDetail?.fullName ?? ""
                case .messageList:
                    data.userId = strongSelf.roomInfo?.otherUserID ?? ""
                    data.profilePicture = strongSelf.roomInfo?.roomImage ?? ""
                    data.fullName = strongSelf.roomInfo?.roomName ?? ""
                case .userDetail:
                    data.userId = strongSelf.otherUserData?.userId ?? ""
                    data.profilePicture = strongSelf.otherUserData?.profilePicture ?? ""
                    data.fullName = strongSelf.otherUserData?.fullName ?? ""
                }
                
                vc.user = data
                vc.delegate = self
                vc.modalPresentationStyle = .overCurrentContext
                strongSelf.navigationController?.present(vc, animated: true, completion: nil)
                
            default: return
            }
        }
    }
    
    fileprivate func setupProductDropdown(){
        
        productDropdown.backgroundColor = AppColors.whiteColor
        productDropdown.anchorView = productView
        productDropdown.cellNib = UINib(nibName: ProductViewCell.defaultReuseIdentifier, bundle: nil)
        
        productDropdown.customCellConfiguration = {[weak self](index: Index, item: String, cell: DropDownCell) -> Void in
            guard let strongSelf = self else{return}
            guard let cell = cell as? ProductViewCell else { return }
            guard !strongSelf.controller.products.isEmpty else{ return }
            let data = strongSelf.controller.products[index]
            cell.productImage.setImage_kf(imageString: data.images.first?.image ?? "", placeHolderImage: AppImages.productPlaceholder.image)
            cell.optionLabel.text = "$\(data.firmPrice)"
            cell.productTitle.text = data.productName
            cell.productStatus.text = data.productStatus.value
            //Temp Code
//            cell.productStatus.text = "Sold"
            cell.productStatus.textColor = data.productStatus == .sold ? AppColors.blackColor : AppColors.appBlueColor
            cell.productStatus.font = data.productStatus == .sold ? AppFonts.Galano_Medium.withSize(15) : AppFonts.Galano_Regular.withSize(14)
            cell.bgView.backgroundColor = data.productStatus == .sold ? AppColors.grayBorderColor : AppColors.whiteColor
        }
        
        productDropdown.bottomOffset = CGPoint(x: 0, y: productDropdown.anchorView!.plainView.bounds.height)
        productDropdown.dismissMode = .automatic
        productDropdown.cellHeight = 60
        
        productDropdown.selectionAction = { [weak self] (index: Int, item: String) in
            
            guard let strongSelf = self else { return }
            if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                return
            }
            let productData = strongSelf.controller.products[index]
            guard strongSelf.selectedProductID != productData.id else{return}
            strongSelf.selectedProductID = productData.id
            
            strongSelf.productImage.setImage_kf(imageString: productData.images.first?.image ?? "", placeHolderImage: AppImages.productPlaceholder.image)
            strongSelf.productPrice.text = "$\(productData.firmPrice)"
            strongSelf.productTitle.text = productData.productName
            strongSelf.productStatus.text = productData.productStatus.value
            strongSelf.productStatus.textColor = productData.productStatus == .sold ? AppColors.blackColor : AppColors.appBlueColor
            strongSelf.productStatus.font = productData.productStatus == .sold ? AppFonts.Galano_Medium.withSize(15) : AppFonts.Galano_Regular.withSize(14)
            
            guard let roomData = self?.roomInfo else{ return }
            
            strongSelf.productDetail = nil
            SingleChatFireBaseController.shared.updateProductInfo(product: productData,
                                                                  roomId: roomData.roomID,
                                                                  otherUserID: roomData.otherUserID)
            SingleChatFireBaseController.shared.addHeader(text: productData.productName,
                                                          roomId: roomData.roomID)
            strongSelf.controller.getProductPaymentHistory(id: productData.id)
        }
    }
    
    func refreshProductStatus(userId : String,
                              sellerId: String,
                              entityId: String){
        
        guard !controller.products.isEmpty else{ return }
        let selectedProduct = controller.products.filter({$0.id == selectedProductID}).first
        
        var otherUserID: String = ""
        switch vcInstantiated{
            
        case .productDetail:
            otherUserID = productDetail?.userId ?? ""
        case .userDetail:
            otherUserID = otherUserData?.userId ?? ""
        case .messageList:
            otherUserID = roomInfo?.otherUserID ?? ""
        }
        
        let isRoomOfOtherUser = (userId == otherUserID) || (sellerId == otherUserID)
        
        if let prod = selectedProduct, prod.id == entityId, isRoomOfOtherUser {
            controller.getProductPaymentHistory(id: entityId)
        }
    }
}

extension SingleChatVC: PinToTopCellDelegate {
    
    func toggleBtnTapped(btn: UIButton) {
        
        guard let info = roomInfo else{ return }
        
        switch btn.tag{
            
        case 0:
            SingleChatFireBaseController.shared.updatePinnedToChat(isPinned: btn.isSelected,
                                                                   roomID: info.roomID)
        case 1:
            SingleChatFireBaseController.shared.updateChatToMute(isMute: btn.isSelected,
                                                                 roomID: info.roomID)
            
        default: return
        }
        moreDropDown.hide()
    }
    
    func updateRoomData(){
        
        if let data = roomInfo{
            
            moreDropDown.customCellConfiguration = {(index: Index, item: String, cell: DropDownCell) -> Void in
                guard let cell = cell as? PinToTopCell else { return }
                cell.delegate = self
                cell.toggleBtn.tag = index
                cell.toggleBtn.isHidden = !(index == 0 || index == 1)
                if index == 0{
                    cell.toggleBtn.isSelected = data.pinned
                }else if index == 1{
                    cell.toggleBtn.isSelected = data.chatMute
                }
                
                // in product detail we update data in view did load
                if self.vcInstantiated == .messageList || self.vcInstantiated == .userDetail{
                    self.updateProductData()
                }
            }
        }
    }
}


