//
//  SingleChatController.swift
//  TagHawk
//
//  Created by Appinventiv on 03/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SingleChatControllerDelegate: class {
 
    func getProductData()
    func getError(error: String)
    func blockedUser(otherUserID: String)
    func productHistory(payment: PaymentHistory)
    
    func willConfirmOrder()
    func confirmOrderSuccessFylly()
    func failedToConfirmorder(msg : String)
    
    func willReleasePayment()
    func paymentReleasedSuccessFully(historyObj : PaymentHistory)
    func failedToReleasePayment(msg : String)
    
    func willRequestRefund()
    func refundreceivedSuccessfully(historyObj : PaymentHistory)
    func failedToReceiveRefund(msg : String)
    
    func willAcceptRefundDelegate()
    func refundAcceptedSuccessfully(historyObj : PaymentHistory)
    func failedToAcceptRefund(msg : String)
    
    func willDeclineRefund()
    func declineSuccessFully(historyObj : PaymentHistory)
    func failedToDecline(msg : String)
    
    func willReleaseRefund()
    func refundReleasedSuccessfully(historyObj : PaymentHistory)
    func failedToReleaseRefund(msg : String)
    
    func willMmakeDespute()
    func desputeMadeSuccessfully(historyObj : PaymentHistory)
    func failedToMakeDespute(msg : String)
}

extension SingleChatControllerDelegate {
    
    func productHistory(product: PaymentHistory){}
    
    func willConfirmOrder(){}
    func confirmOrderSuccessFylly(){}
    func failedToConfirmorder(msg : String){}
    
    func willReleasePayment(){}
    func paymentReleasedSuccessFully(historyObj : PaymentHistory){}
    func failedToReleasePayment(msg : String){}
    
    func willRequestRefund(){}
    func refundreceivedSuccessfully(historyObj : PaymentHistory){}
    func failedToReceiveRefund(msg : String){}
    
    func willAcceptRefundDelegate(){}
    func refundAcceptedSuccessfully(historyObj : PaymentHistory){}
    func failedToAcceptRefund(msg : String){}
    
    func willDeclineRefund(){}
    func declineSuccessFully(historyObj : PaymentHistory){}
    func failedToDecline(msg : String){}
    
    func willReleaseRefund(){}
    func refundReleasedSuccessfully(historyObj : PaymentHistory){}
    func failedToReleaseRefund(msg : String){}
    
    func willMmakeDespute(){}
    func desputeMadeSuccessfully(historyObj : PaymentHistory){}
    func failedToMakeDespute(msg : String){}
}

class SingleChatController {
    
    weak var delegate: SingleChatControllerDelegate?
    var products: [UserProduct] = []
    
    func getProductDetail(otherUserID: String){
        
        var dict = [String: Any]()
        dict["productStatus"] = 4
        dict["pageNo"] = 1
        dict["limit"] = 100
        dict["otherUserId"] = otherUserID
        
        WebServices.getUserProducts(parameters: dict, success: { [weak self] (data) in
            self?.products = UserProduct.returnUserProductArray(jsonArr: data["data"].arrayValue)
            self?.delegate?.getProductData()
        }) { (error) -> (Void) in
            self.delegate?.getError(error: error.localizedDescription)
        }
    }
    
    func blockedUser(otherUserID: String){
        
        let params : JSONDictionary = [ApiKey.userId : otherUserID, ApiKey.action : "3"]
        
        WebServices.removeFriend(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.delegate?.blockedUser(otherUserID: otherUserID)
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.getError(error: error.localizedDescription)
        }
    }
    
    func sendNotification(userData: UserProfile,
                          roomData: ChatListing,
                          text: String){
        
        let payLoadData:JSONDictionary = [ApiKey.entityId: roomData.roomID,
                                          ApiKey.type: "GET_MESSAGE",
                                          "title": UserProfile.main.fullName + " sent you a new message"]
        
        let notificationPayload: JSONDictionary = ["title": UserProfile.main.fullName + " sent you a new message",
                                                   "body": text,
                                                   "sound": "default",
                                                   "badge": userData.totalUnreadCount,
                                                   "roomData": roomData.roomInfo]
        
        let params: JSONDictionary = ["to" : userData.deviceToken,
                                      "content_available": true,
                                      "data": payLoadData,
                                      "notification": notificationPayload,
                                      "collapse_key": "Updates Available",
                                      "priority" : "high"]
        printDebug("Send notification")
        WebServices.sendNotification(parameters: params)
    }
    
    func getProductPaymentHistory(id: String){
        
        let params: JSONDictionary = ["productId" : id]
        WebServices.getProductPaymentHistory(parameters: params,
                                             success: {[weak self](payment) in
            
                                                guard let weakSelf = self else { return }
                                                weakSelf.delegate?.productHistory(payment: payment)
        }) { (error) -> (Void) in
            
        }
    }
    
    func confirmOrder(orderId : String){
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        self.delegate?.willConfirmOrder()
        
        WebServices.confirmOrder(parameters: params, success: {[weak self] (json) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.confirmOrderSuccessFylly()
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToConfirmorder(msg: error.localizedDescription)
        }
    }
    
    func requestRefund(orderId : String){
        
        self.delegate?.willRequestRefund()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        
        WebServices.requestRefund(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.delegate?.refundreceivedSuccessfully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToReceiveRefund(msg: error.localizedDescription)
            
        }
        
    }
    
    
    func releasePayment(orderId : String){
        
        self.delegate?.willRequestRefund()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        
        WebServices.releasePayment(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.delegate?.refundreceivedSuccessfully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToReceiveRefund(msg: error.localizedDescription)
            
        }
    }
    
    func acceptRefund(orderId : String){
        
        self.delegate?.willAcceptRefundDelegate()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        
        WebServices.acceptRefund(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.delegate?.refundAcceptedSuccessfully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToAcceptRefund(msg: error.localizedDescription)
        }
    }
    
    
    func declineRefund(orderId : String, msg : String){
        
        self.delegate?.willDeclineRefund()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId, ApiKey.declineMessage : msg]
        
        WebServices.declineRefund(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.delegate?.declineSuccessFully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToDecline(msg: error.localizedDescription)
        }
    }
    
    func releaseRefund(orderId : String){
        
        self.delegate?.willDeclineRefund()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        
        WebServices.releaseRefund(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.delegate?.declineSuccessFully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToDecline(msg: error.localizedDescription)
        }
    }
    
    
    func makeDispute(orderId : String, msg : String, images : [String]){
        
        var params : JSONDictionary = [ApiKey.orderId : orderId, ApiKey.statement : msg]
        
        let imagesArray = images.map { (strUrl) -> JSONDictionary in
            return [ApiKey.url : strUrl]
        }
        
        params[ApiKey.proof] = JSON(imagesArray)
        
        self.delegate?.willMmakeDespute()
        
        WebServices.makeDispute(parameters: params, success: {[weak self] (json) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.desputeMadeSuccessfully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToMakeDespute(msg: error.localizedDescription)
        }
    }
}
