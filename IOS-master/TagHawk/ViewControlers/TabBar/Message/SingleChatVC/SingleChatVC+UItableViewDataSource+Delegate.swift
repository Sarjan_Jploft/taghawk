//
//  SingleChatVC+UItableViewDataSource+Delegate.swift
//  TagHawk
//
//  Created by Appinventiv on 27/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import UIKit

//MARK:- TableView datasource
extension SingleChatVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let message = messageData[indexPath.row]
        
        switch message.messageType{
            
        case .text:
            if message.senderID == UserProfile.main.userId{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: SenderCell.defaultReuseIdentifier, for: indexPath) as?  SenderCell else{
                    fatalError() }
                cell.populateData(data: message,
                                  chatType: roomInfo?.chatType ?? .single)
                
                return cell
            
            }else{
               
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ReceiverCell.defaultReuseIdentifier, for: indexPath) as?  ReceiverCell else{
                    fatalError()
                }
                
                cell.delegate = self
                cell.populateData(data: message,
                                  roomData: roomInfo,
                                  members: [])
                return cell
            }
            
        case .image, .shareProduct, .shareCommunity:
            if message.senderID == UserProfile.main.userId{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: SenderImageCell.defaultReuseIdentifier, for: indexPath) as?  SenderImageCell else{
                    fatalError()
                }
                cell.populateData(data: message, chatType: roomInfo?.chatType ?? .single)
                
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ReceiverImageCell.defaultReuseIdentifier, for: indexPath) as?  ReceiverImageCell else{
                    fatalError()
                }
                
                cell.populateData(data: message, roomData: roomInfo, members: [])
                
                return cell
            }
            
        case .productChangeHeader,.timeHeader,.userJoin,.userLeft,.userRemove, .tagCreatedHeader:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCell.defaultReuseIdentifier, for: indexPath) as?  HeaderCell else {
                fatalError() }

            cell.populateData(data: message, groupMembers: [])
            
            if message.messageType == .productChangeHeader {
                cell.headerTitle.isHidden = false
                
                let userName = message.senderID == UserProfile.main.userId ? UserProfile.main.fullName : (roomInfo?.roomName ?? "")
                let totalText  = userName + " switches the topic to item " + message.messageText
                cell.headerTitle.attributedText = totalText.attributeStringWithColors(stringToColor: message.messageText,
                                                                                      strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            }
            
            return cell
            
        default: fatalError()
        }        
    }
}

//MARK:- tableView Delegate
extension SingleChatVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard !messageData.isEmpty else{ return }
        let message = messageData[indexPath.item]
        
        switch message.messageType {
            
        case .image:
            let browser = SKPhotoBrowser(photos: createWebPhotos(index: indexPath.item))
            browser.initializePageIndex(indexPath.item)
            browser.delegate = self
            present(browser, animated: true, completion: nil)
            
        case .shareProduct:
            let vc = ProductDetailVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.prodId = message.sharedID
            AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
        case .shareCommunity:
            let tagScene = TagDetailVC.instantiate(fromAppStoryboard: .AddTag)
            tagScene.tagId = message.sharedID
            AppNavigator.shared.parentNavigationControler.pushViewController(tagScene, animated: true)
        default: return
            
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        print("scrollViewDidEndDragging")
        if (messagesTableView.contentOffset.y <= 0)
        {
            if !isDataLoading{
                isDataLoading = true
                
                if !messageData.isEmpty,
                    messageData.count > 90,
                    let info = roomInfo{
                    let timeStamp = messageData.first?.timeStamp ?? Date().unixFirebaseTimestamp
                    SingleChatFireBaseController.shared.getPaginatedData(roomId: info.roomID, msgTimeStamp: timeStamp)
                }
            }
        }
    }
}


extension SingleChatVC : SKPhotoBrowserDelegate {
    
    func createWebPhotos(index : Int) -> [SKPhotoProtocol] {
        
        if let selectedImage = [messageData[index].messageText].first{
            return [SKPhoto(url: selectedImage)]
        }
        return []
    }
    
}
