//
//  SearchMessages+UitableViewDelegate+DataSource.swift
//  TagHawk
//
//  Created by Abhishek on 25/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

//MARk:- tableview datasource and delegates
extension SearchMessageVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueCell(with: SearchMessageCell.self)
        cell.populateData(indexPath: indexPath, chatData: searchedData)
        
        return cell
    }
}

extension SearchMessageVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard !searchedData.isEmpty else{
            return
        }
        
        switch searchedData[indexPath.row].chatType {
            
        case .single:
            
            let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
            scene.vcInstantiated = .messageList
            scene.roomInfo = searchedData[indexPath.row]
            AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
            
        case .group:
            let scene = GroupChatVC.instantiate(fromAppStoryboard: .Messages)
            scene.isFromSearchScreen = true
            scene.roomData = searchedData[indexPath.row]
            AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
        }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
        
        let deleteAction = UIContextualAction(style: .normal, title: nil) {[weak self](ac: UIContextualAction, view: UIView, success: (Bool) -> Void)  in
            self?.intantiateDeleteChat(indexPath: indexPath)
            success(true)
        }
        
        deleteAction.image = AppImages.icChatTrash.image
        deleteAction.backgroundColor = AppColors.deleteBackgroundColor
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    private func intantiateDeleteChat(indexPath: IndexPath){
        
        let data = searchedData[indexPath.row]
        
        if data.chatType == .single {
            deleteChat(roomName: data.roomName, index: indexPath, vcinstantiated: .deleteChat)
        }else if data.chatType == .group && data.otherUserID != UserProfile.main.userId{
            deleteChat(roomName: data.roomName, index: indexPath, vcinstantiated: .exitGroup)
        }else{
            deleteGroup(groupName: data.roomName, index: indexPath)
        }
    }
    
    //MARK:- Delete chat
    private func deleteChat(roomName: String, index: IndexPath, vcinstantiated: DeleteChatVCInstantiated){
        let scene = DeleteChatVC.instantiate(fromAppStoryboard: .Messages)
        scene.modalTransitionStyle = .coverVertical
        scene.modalPresentationStyle = .overCurrentContext
        scene.selectedIndexPath = index
        scene.roomName = roomName
        scene.groupName = roomName
        scene.instantiated = vcinstantiated
        self.definesPresentationContext = true
        scene.delegate = self
        AppNavigator.shared.parentNavigationControler.present(scene, animated: true, completion: nil)
    }
    
    //MARK:- Delete group
    private func deleteGroup(groupName: String, index: IndexPath){
        let scene = UpdateEmailVC.instantiate(fromAppStoryboard: .Messages)
        scene.delegate = self
        scene.groupName = groupName
        scene.modalTransitionStyle = .coverVertical
        scene.modalPresentationStyle = .overCurrentContext
        scene.instantitaed = .deleteGroup
        scene.indexPath = index
        self.definesPresentationContext = true
        AppNavigator.shared.parentNavigationControler.present(scene, animated: true, completion: nil)
    }
}

//MARK:- Update email delegate
extension SearchMessageVC: UpdateEmailVCDeleagte {
    
    func updates(text: String, type: UpdateEmailVCInstantiated, indexPath: IndexPath?){
        if type == .deleteGroup,
            let index = indexPath {
            let data = searchedData[index.row]
            listController.deleteTag(tagID: data.roomID, index: index)
        }
    }
}

extension SearchMessageVC: DeleteChatDelegate {
    
    //MARK:- Delete button tapped
    func deleteBtnTapped(index: IndexPath?) {
        
        if let selectedIndex = index {
            let data = searchedData[selectedIndex.row]
            searchTableView.beginUpdates()
            if let index = chats.index(where: {$0.roomID == data.roomID}){
                self.chats.remove(at: index)
            }
            searchTableView.deleteRows(at: [selectedIndex], with: .automatic)
            searchTableView.endUpdates()
            SingleChatFireBaseController.shared.deleteChat(roomID: data.roomID, otherUserID: data.otherUserID)
        }
    }
    
    //MARK:- Exit button tapped
    func exitBtnTapped(index: IndexPath?){
        
        if let selectedIndex = index {
            let data = searchedData[selectedIndex.row]
            listController.exitGroup(tagID: data.roomID, isPinned: true)
        }
    }
}

extension SearchMessageVC: GroupChatFireBaseControllerDelegate {

    //MARK:- Exit group
    func exitFromGroup(id: String, pinned: Bool){
        searchTableView.beginUpdates()
        if let idx = searchedData.index(where: {$0.roomID == id}){
            searchedData.remove(at: idx)
            searchTableView.deleteRows(at: [IndexPath(row: idx, section: 0)], with: .automatic)
        }
        if let index = searchedData.index(where: {$0.roomID == id}){
            chats.remove(at: index)
        }
        searchTableView.endUpdates()
    }
}

extension SearchMessageVC: GroupDetailControllerDelegate {
    
    //MARK:- Edit tag
    func exitTag(id: String, pinned: Bool){
        GroupChatFireBaseController.shared.exitFromGroup(roomID: id,
                                                         isPinned: pinned)
    }
    
    //MARK:- Tag deleted successfully
    func tagDeleted(indexPath: IndexPath){
        
        searchTableView.beginUpdates()
        let data = searchedData[indexPath.row]
        if let index = chats.index(where: {$0.roomID == data.roomID}){
            self.chats.remove(at: index)
        }
        searchedData.remove(at: indexPath.row)
        searchTableView.deleteRows(at: [indexPath], with: .automatic)
        GroupChatFireBaseController.shared.deleteGroup(roomID: data.roomID, members: data.groupMembers)
        searchTableView.endUpdates()
    }
}

//MARK:- Textfield delegate
extension SearchMessageVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        
        guard let userEnteredString = textField.text else { return false }
        
        return self.isCharacterAcceptableForSearch(txt: userEnteredString, char: string)
        
    }
    
    func isCharacterAcceptableForSearch(txt : String, char : String) -> Bool {
        return char.isBackSpace || txt.count <= 20
    }
    
    @objc func textDidChange(_ textField : UITextField) {
        
        if let searchText = textField.text,
            !searchText.isEmpty{
            let searched = chats.filter({$0.roomName.localizedCaseInsensitiveContains(searchText)})
            searchedData = searched
            searchTableView.reloadData()
        }else{
            searchTableView.reloadData()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

