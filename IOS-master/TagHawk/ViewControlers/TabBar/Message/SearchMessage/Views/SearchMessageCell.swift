//
//  SearchMessageCell.swift
//  TagHawk
//
//  Created by Abhishek on 25/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SearchMessageCell: UITableViewCell {

    //    MARK:- IBOutlets
    //    ================
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var chatMuteImage: UIImageView!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var sepratorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userImage.contentMode = .scaleAspectFill
        userImage.round()
        userName.font = AppFonts.Galano_Semi_Bold.withSize(13)
        userName.textColor = AppColors.blackLblColor
        chatMuteImage.cornerRadius = chatMuteImage.frame.height / 2
        chatMuteImage.borderWidth = 0.5
        chatMuteImage.borderColor = AppColors.grayBorderColor
        chatMuteImage.image = AppImages.icMutedChat.image
        messageText.font = AppFonts.Galano_Regular.withSize(11.5)
        messageText.textColor = AppColors.lightGreyTextColor
        messageTime.font = AppFonts.Galano_Regular.withSize(11)
        messageTime.textColor = AppColors.blackLblColor
        sepratorView.backgroundColor = AppColors.blackColor.withAlphaComponent(0.8)
        productImage.cornerRadius = 10
        productImage.borderWidth = 1.0
        productImage.borderColor = AppColors.grayBorderColor
    }

    //MARK:- Populate data
    func populateData(indexPath: IndexPath, chatData: [ChatListing]){
    
        sepratorView.isHidden = (chatData.count - 1) == indexPath.row ? true : false
        userImage.setImage_kf(imageString: chatData[indexPath.row].roomImage, placeHolderImage: AppImages.icChatPlaceholder.image)
        userName.text = chatData[indexPath.row].roomName.capitalizingFirstLetter()
        productImage.isHidden = chatData[indexPath.row].chatType == .group
        productImage.setImage_kf(imageString: chatData[indexPath.row].productData?.productImage ?? "", placeHolderImage: AppImages.icProductPlaceholder.image)
        
        var titleText = ""
        
        if chatData[indexPath.row].lastMessage.messageType != .image && chatData[indexPath.row].lastMessage.messageType != .text{
            titleText = titleText + (chatData[indexPath.row].lastMessage.senderID == UserProfile.main.userId ? LocalizedString.you.localized : chatData[indexPath.row].lastMessage.messageText)
        }
        
        switch chatData[indexPath.row].lastMessage.messageType {
            
        case .image:
            let totalText = titleText + LocalizedString.sentImage.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
        case .text:
            let totalText = titleText + chatData[indexPath.row].lastMessage.messageText
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
        case .tagCreatedHeader:
            let totalText = titleText + LocalizedString.createdThisTag.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .userLeft:
            let totalText = titleText + LocalizedString.leftFromTag.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .userRemove:
            let totalText = titleText + LocalizedString.removedFromTag.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .userJoin:
            let totalText = titleText + LocalizedString.joinedTheTag.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        default: messageText.text = ""
        }
        
        chatMuteImage.isHidden = !chatData[indexPath.row].chatMute
        let date = Date(timeIntervalSince1970: chatData[indexPath.row].lastMessage.timeInterval)
        messageTime.text = date.timeAgoSince
    }
}
