//
//  SearchMessageVC.swift
//  TagHawk
//
//  Created by Abhishek on 25/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SearchMessageVC: BaseVC {

    //MARK:- IBOutlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
//    MARK:- Proporties
//    =================
    var chats: [ChatListing] = []
    var searchedData: [ChatListing] = []
    let listController = GroupDetailController()
    
    
//    MARK:- ViewController Life Cycle
//    =================================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GroupChatFireBaseController.shared.delegate = self
        self.configureNavigation()
        self.searchTextField.becomeFirstResponder()
    }
    
//    MARK:- IBActions
//    ================
    //MARK:- Back button Tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK:- close button tapped
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        self.searchTextField.text = ""
//        self.controler.searchResults(searchText : "")
    }

}

//MARK:- Setup subview
extension SearchMessageVC{
    
    func setUpSubView(){
        
        listController.delegate = self
        
        self.configureTableView()
        self.searchTextField.delegate = self
        self.searchTextField.placeholder = LocalizedString.Search.localized
        self.searchTextField.addTarget(self, action: #selector(textDidChange), for: UIControl.Event.editingChanged)
//        self.searchTextField.text = self.searchStr
        self.searchTextField.returnKeyType = .search
    }
    
    //MARK:- Configure tableview
    func configureTableView(){
        self.searchTableView.rowHeight = UITableView.automaticDimension
        self.searchTableView.estimatedRowHeight = 50
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
    }
    
    //MARK:- configure navigation
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}
