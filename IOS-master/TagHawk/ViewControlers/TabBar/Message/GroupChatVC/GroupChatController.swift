//
//  GroupChatController.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

protocol GroupChatControllerDelegate: class{
    func getproductData()
    func getError(error: String)
}

class GroupChatController {
    
    var products: [Product] = []
    var totalProductsCount: Int = 0
    weak var delegate: GroupChatControllerDelegate?
    
    //MARK:- Get products
    func getProducts(tagID: String){
        
        let params: JSONDictionary = ["communityId": tagID,
                                      "pageNo": 1,
                                      "limit": 5]
        
        WebServices.getTagProducts(parameters: params, success: {[weak self](json) in
            self?.totalProductsCount = json["total"].intValue
            json["data"].arrayValue.forEach({ (jsonDic) in
                self?.products.append(Product(json: jsonDic))
            })
            self?.delegate?.getproductData()
        }) {[weak self](error) -> (Void) in
            self?.delegate?.getError(error: error.localizedDescription)
        }
    }
    
    //MARK:- Send notification
    func sendNotification(userData: UserProfile,
                          groupInfo: GroupDetail,
                          text: String,
                          roomData: ChatListing){
        
        let payLoadData:JSONDictionary = [ApiKey.entityId: groupInfo.tagID,
                                          ApiKey.type: "GET_MESSAGE",
                                          "title": UserProfile.main.fullName + " sent you a new message"]
        
        let notificationPayload: JSONDictionary = ["title": UserProfile.main.fullName + " sent you a new message",
                                                   "body": text,
                                                   "sound": "default",
                                                   "badge":userData.totalUnreadCount,
                                                   "roomData": roomData.roomInfo]
        
        let params: JSONDictionary = ["to" : userData.deviceToken,
                                      "content_available": true,
                                      "data": payLoadData,
                                      "notification": notificationPayload,
                                      "collapse_key": "Updates Available",
                                      "priority" : "high"]
        
        WebServices.sendNotification(parameters: params)
    }
}
