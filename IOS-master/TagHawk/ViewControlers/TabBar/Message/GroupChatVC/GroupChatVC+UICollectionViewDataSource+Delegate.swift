//
//  GroupChatVC+UICollectionViewDataSource+Delegate.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

//MARK:- UICollectionview datasource
extension GroupChatVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return controller.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductImageCell.defaultReuseIdentifier, for: indexPath) as? ProductImageCell else{
            fatalError()
        }
        
        cell.populateData(indexPath: indexPath,
                          products: controller.products,
                          totalCount: controller.totalProductsCount)
        
        return cell
    }
}

//MARK:- collectionview flow layout delegate
extension GroupChatVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: (collectionView.frame.height), height: (collectionView.frame.height))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
}

//MARK:- collectionview delegate
extension GroupChatVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        proceedToTagShelfVC()
    }
    
    func proceedToTagShelfVC(){
        let tagShelfVC = TagShelfVC.instantiate(fromAppStoryboard: .AddTag)
        tagShelfVC.tagId = roomData?.roomID ?? ""
        tagShelfVC.tagName = roomData?.roomName ?? ""
        navigationController?.pushViewController(tagShelfVC, animated: true)
    }
}

class ProductImageCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var overLayView: UIView!
    @IBOutlet weak var itemCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        itemCount.font = AppFonts.Galano_Medium.withSize(12)
        itemCount.textColor = AppColors.whiteColor
        productImage.round(radius: 10.0)
        productImage.clipsToBounds = true
        overLayView.round(radius: 10.0)
        overLayView.clipsToBounds = true
        productImage.borderColor = AppColors.whiteColor
        productImage.borderWidth = 1.0
        overLayView.backgroundColor = AppColors.blackColor.withAlphaComponent(0.5)
    }
    
    func populateData(indexPath: IndexPath, 
                      products: [Product], totalCount: Int){
        
        guard !products.isEmpty else{ return }
        let productData = products[indexPath.item]
        productImage.setImage_kf(imageString: productData.images.first?.image ?? "", placeHolderImage: AppImages.productPlaceholder.image)
        
        if indexPath.item == 4{
            overLayView.isHidden = false
            itemCount.isHidden = false
            let text = totalCount > 99 ? "\(totalCount)+\n" : "\(totalCount)\n"
            itemCount.text = text + LocalizedString.more.localized
        }else{
            overLayView.isHidden = true
            itemCount.isHidden = true
        }
    }
}
