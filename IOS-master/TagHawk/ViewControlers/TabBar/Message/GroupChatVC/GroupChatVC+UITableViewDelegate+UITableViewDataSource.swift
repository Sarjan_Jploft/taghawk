//
//  GroupChatVC+UITableViewDelegate+UITableViewDataSource.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

//MARK:- Tableview datasource
extension GroupChatVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message = messagesList[indexPath.row]
 
        switch message.messageType{
            
        case .text, .shelfProduct:
            if message.senderID == UserProfile.main.userId{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: SenderCell.defaultReuseIdentifier, for: indexPath) as?  SenderCell else{
                    fatalError() }
                cell.populateData(data: message, chatType: .group)
                
                return cell
                
            }else{
                
                if let idx = membersData.firstIndex(where: {$0.id == message.senderID}){
                    if !membersData[idx].isGetData{
                        membersData[idx].isGetData = true
                        getUserData(userID: message.senderID, index: indexPath)
                    }
                }else{
                    getUserData(userID: message.senderID, index: indexPath)
                }
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ReceiverCell.defaultReuseIdentifier, for: indexPath) as?  ReceiverCell else{
                    fatalError()
                }
                
                cell.delegate = self
                cell.populateData(data: message,
                                  roomData: roomData,
                                  members: self.membersData)
                
                return cell
            }
            
        case .image:
            if message.senderID == UserProfile.main.userId{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: SenderImageCell.defaultReuseIdentifier, for: indexPath) as?  SenderImageCell else{
                    fatalError()
                }
                cell.populateData(data: message, chatType: .group)
                
                return cell
            }else{
                if let idx = membersData.firstIndex(where: {$0.id == message.senderID}){
                    if !membersData[idx].isGetData{
                        membersData[idx].isGetData = true
                        getUserData(userID: message.senderID, index: indexPath)
                    }
                }else{
                    getUserData(userID: message.senderID, index: indexPath)
                }
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ReceiverImageCell.defaultReuseIdentifier, for: indexPath) as?  ReceiverImageCell else{
                    fatalError()
                }
                
                cell.populateData(data: message,
                                  roomData: roomData,
                                  members: self.membersData)
                
                return cell
            }
            
        case .productChangeHeader,.timeHeader,.userJoin,.userLeft,.userRemove,.tagCreatedHeader, .ownershipTransfer:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCell.defaultReuseIdentifier, for: indexPath) as?  HeaderCell else{
                fatalError()
            }

            cell.populateData(data: message,
                              groupMembers: membersData)
            
            switch message.messageType {
                
            case .userJoin, .userRemove, .userLeft, .shelfProduct, .ownershipTransfer, .tagCreatedHeader:
                if let idx = membersData.firstIndex(where: {$0.id == message.messageText}){
                    if !membersData[idx].isGetData{
                        membersData[idx].isGetData = true
                        getUserData(userID: message.senderID, index: indexPath)
                    }
                }else{
                    getUserData(userID: message.senderID, index: indexPath)
            }
                
            default:
                break
            }
            
            return cell
            
        default: fatalError()
        }
    }
    
    //MARK:- get user data
    private func getUserData(userID: String, index: IndexPath){
        
        SingleChatFireBaseController.shared.getUserInfo(userID: userID,
                                         indexPath: index,
                                         success: {[weak self](index, userData) in
                                            
                                            guard let strongSelf = self else{ return }
                                            
                                            if let idx = strongSelf.membersData.firstIndex(where: {$0.id == userData.userId}){
                                                
                                                strongSelf.membersData[idx].image = userData.profilePicture
                                                strongSelf.membersData[idx].name = userData.fullName
                                                strongSelf.membersData[idx].isGetData = true
                                            }else{
                                               var data = GroupMembers()
                                                data.id = userData.userId
                                                data.name = userData.fullName
                                                data.image = userData.profilePicture
                                                data.isGetData = true
                                                strongSelf.membersData.append(data)
                                            }
                                            
                                            let visibleCells = strongSelf.messagesTableView.visibleCells
                                            visibleCells.forEach { (cell) in
                                                
                                                if let index = cell.tableViewIndexPath(strongSelf.messagesTableView){
                                                    strongSelf.populateDataforVisibleCells(indexPath: index, tableViewCell: cell)
                                                }
                                            }
        })
    }
    
    //MARK:- populate data for visible cells
    private func populateDataforVisibleCells(indexPath: IndexPath, tableViewCell: UITableViewCell){
        
        let message = messagesList[indexPath.row]
        
        if let cell = tableViewCell as? ReceiverCell {
           cell.populateData(data: message, roomData: roomData, members: membersData)
        }
        
        if let cell = tableViewCell as? ReceiverImageCell{
            cell.populateData(data: message, roomData: roomData, members: membersData)
        }
        
        if let cell = tableViewCell as? HeaderCell{
            cell.populateData(data: message, groupMembers: membersData)
        }
    }
}

//MARK:- Tableview datasource
extension GroupChatVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard !messagesList.isEmpty else{ return }
        let message = messagesList[indexPath.item]
        if message.messageType == .image{
            let browser = SKPhotoBrowser(photos: createWebPhotos(index: indexPath.item))
            browser.initializePageIndex(indexPath.item)
            browser.delegate = self
            present(browser, animated: true, completion: nil)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        print("scrollViewDidEndDragging")
        if (messagesTableView.contentOffset.y <= 0)
        {
            if !isDataLoading{
                isDataLoading = true
                
                if !messagesList.isEmpty,
                    messagesList.count > 90,
                    let info = roomData{
                    let timeStamp = messagesList.first?.timeStamp ?? Date().unixFirebaseTimestamp
                    GroupChatFireBaseController.shared.getPaginatedData(roomId: info.roomID,
                                                                        msgTimeStamp: timeStamp,
                                                                        createdTime: info.createdTime)
                }
            }
        }
    }
}

extension GroupChatVC : SKPhotoBrowserDelegate {
    
    func createWebPhotos(index : Int) -> [SKPhotoProtocol] {
        
        if let selectedImage = [messagesList[index].messageText].first{
            return [SKPhoto(url: selectedImage)]
        }
        return []
    }
}
