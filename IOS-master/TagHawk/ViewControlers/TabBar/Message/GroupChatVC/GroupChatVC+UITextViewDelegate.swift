//
//  SingleChatVC+UItextViewDelegate.swift
//  TagHawk
//
//  Created by Appinventiv on 13/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

//MARK:- Textview delegate
extension GroupChatVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        guard let text = textView.text else {
            return
        }
        
        let trimmedText = text.byRemovingLeadingTrailingWhiteSpaces.trimTrailingWhitespace()
        
        if trimmedText.isEmpty {
            sendBtn.isEnabled = false
            self.resetFrames()
            return
        }else {
            sendBtn.isEnabled = true
        }
        arrangeTextViewHeight()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let message = messageTextView.text.byRemovingLeadingTrailingWhiteSpaces.trimTrailingWhitespace()
        if message.isEmpty{
            return (text == " ") || (text == "\n") ? false : true
        }
        return true
    }
    
    func arrangeTextViewHeight(){
        
        let text = messageTextView.text ?? ""
        let height = text.heightOfText(self.messageTextView.bounds.width - 18, font: AppFonts.Galano_Regular.withSize(14)) + 10
        
        if height > 43 && height < 90 {
            self.textViewHeight.constant = height + 10
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if height < 43{
            resetFrames()
        }
        messageTextView.isScrollEnabled = (height > 90)
    }
    
    func resetFrames() {
        UIView.animate(withDuration: 0.3) {
            self.textViewHeight.constant = 30
            self.view.layoutIfNeeded()
        }
    }
}

//MARK:- Attachment popup delegate
extension GroupChatVC : AttachmentPopupDelegate {
    
    //MARK:- Attachment button tapped
    func attachmentBtnTapped(tappedBtn: AttachmentPopUp){
        
        switch tappedBtn {
            
        case .takePhoto: checkAndOpenCamera(delegate: self)
            
        case .gallery: checkAndOpenLibrary(delegate: self)
            
        case .shareCommunity: gotoTagList()
            
        case .shareProduct: gotoProductList()
            
        }
    }
    
    private func gotoTagList() {
       
    }
    
    private func gotoProductList() {

    }
}

//MARK:- Image picker delegate
extension GroupChatVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[.originalImage] as? UIImage {
            picker.dismiss(animated: true) {
                self.uploadLogoImageToAWS(image: img)
            }
        }
    }
    
    //MARK:- upload logo
    func uploadLogoImageToAWS(image: UIImage) {
        
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        let mesageData = MessageDetail.getBlankImageMessageDetail()
        mesageData.memberCount = groupInfo?.members.count ?? 1
        self.messagesList.insert(mesageData, at: 0)
        self.messagesTableView.beginUpdates()
        self.messagesTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        self.messagesTableView.endUpdates()
        
        image.uploadImageToS3(imageIndex: 0, success: {[weak self](imageIndex, success, imageUrl) in
            
            if success{
                GroupChatFireBaseController.shared.addmessage(text: imageUrl,
                                                              tagID: self?.groupInfo?.tagID ?? "",
                                                              detail: self?.groupInfo,
                                                              messageType: .image)
            }
            }, progress: { (imageIndex, progress) in
                printDebug("progress:\(progress)")
        }, failure: {[weak self](imageindex, error) in
            CommonFunctions.showToastMessage(error.localizedDescription)
            if let index = self?.messagesList.lastIndex(where: {!$0.isImageMsgReceived}){
                self?.messagesList.remove(at: index)
                self?.messagesTableView.beginUpdates()
                self?.messagesTableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                self?.messagesTableView.endUpdates()
            }
        })
    }
}

//MARK:- Add total user count
extension GroupChatVC: GroupChatFireBaseControllerDelegate {
    
    func addTotalUnreadCount(otherUserData: UserProfile, type: MessageType, text: String, roomInfo: ChatListing?){
        
        let message = type == .text ? text : (text + " shared a image")
        
        if type == .text || type == .image {
            sendNotification(userData: otherUserData,
                             msgType: type,
                             msg: message,
                             roomData: roomInfo)
        }
    }
    
    //MARK:- Room does not exist pop up
    func roomNotExist(){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = .noLongerMember
        vc.delegate = self
        vc.msg = "You are no longer member of this group."
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Get room data
    func getRoomData(info: ChatListing) {
        roomData = info
        GroupChatFireBaseController.shared.changesInGroupDetail(roomID: info.roomID)
        GroupChatFireBaseController.shared.getMessages(roomID: info.roomID, timeStamp: info.createdTime)
        SingleChatFireBaseController.shared.checkUserBlocked()
    }
    
    func userBlockedByAnother(roomData: ChatListing) {
        if roomData.roomID == (groupInfo?.tagID ?? ""){
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func getMessageData(messageData: [MessageDetail]){
        
        messagesList = messageData
        if let info = roomData{
            let timeStamp = !messagesList.isEmpty ? (messagesList.last?.timeStamp ?? Date().unixFirebaseTimestamp) : (Date().unixFirebaseTimestamp - 2)
            GroupChatFireBaseController.shared.getNewMessage(rooomId: info.roomID,
                                                             msgTimeStamp: timeStamp,
                                                             roomCreatedtime: info.createdTime)
        }
        
        messagesTableView.reloadData()
        tableViewScrollToBtm()
    }
    
    func getNewMessage(messageData: MessageDetail){
        
        let timeStamp = !messagesList.isEmpty ? (messagesList.last?.timeStamp ?? Date().unixFirebaseTimestamp) : (Date().unixFirebaseTimestamp - 2)
        guard timeStamp < messageData.timeStamp else{ return }
        
        if messageData.messageType == .text || messageData.messageType == .image{
            
            //            GroupChatFireBaseController.shared.updateReadCount(messsageData: messageData)
            //            GroupChatFireBaseController.shared.checkMessageRead(messsageData: messageData)
            
            if messageData.senderID != UserProfile.main.userId{
                GroupChatFireBaseController.shared.dimisnReadCount(tagID: messageData.roomID)
            }
        }
        
        switch messageData.messageType{
            
        case .image:
            if messageData.senderID != UserProfile.main.userId{
                messagesList.append(messageData)
            }
            
        default:
            messagesList.append(messageData)
        }
        
        if messagesList.isEmpty{
            messagesTableView.reloadData()
        }else{
            messagesTableView.beginUpdates()
            messagesTableView.insertRows(at: [IndexPath(row: (messagesList.count - 1), section: 0)], with: .top)
            messagesTableView.endUpdates()
            tableViewScrollToBtm()
        }
    }
    
    func getPaginatedData(messages: [MessageDetail]){
        
        isDataLoading = false
        var indexArray: [IndexPath] = []
        if !messages.isEmpty{
            for index in 0...(messages.count - 1){
                indexArray.append(IndexPath(row: index, section: 0))
                messagesList.insert(messages[index], at: index)
            }
        }
        messagesTableView.beginUpdates()
        messagesTableView.insertRows(at: indexArray, with: .fade)
        messagesTableView.endUpdates()
    }
    
    func changedInRoomData(detail: GroupDetail){
        
        if groupInfo == nil{
            groupInfo = detail
            membersData = detail.members
            //            detail.members.forEach { (member) in
            //                if member.id != UserProfile.main.userId {
            //                    singleChatController.getUserData(userID: member.id, chatType: .group)
            //                }
            //            }
        }else{
            if groupInfo?.lastMessage.messageID != detail.lastMessage.messageID{
                groupInfo?.lastMessage = detail.lastMessage
            }else{
                groupInfo = detail
            }
        }
        
        screenTitle.text = detail.tagName
        announcementLbl.text = detail.announcement
        if let userData = detail.members.filter({$0.id == UserProfile.main.userId}).first{
            
            UIView.animate(withDuration: 0.3) {
                self.announcementHeight.constant = detail.announcement.isEmpty ? 0 : 50
                self.messageTextView.isHidden = userData.isMute
                self.muteLbl.isHidden = !userData.isMute
                self.sendBtn.isHidden = userData.isMute
                self.attachBtn.isHidden = userData.isMute
            }
            view.layoutIfNeeded()
        }
    }
    
    func messageRead(index: IndexPath){
        
        //        messagesTableView.reloadRows(at: [index], with: .automatic)
    }
    
    func getUserData(userData: UserProfile, chatType: ChatType){
        
        if let idx = groupInfo?.members.firstIndex(where: {$0.id == userData.userId}){
            groupInfo?.members[idx].name = userData.fullName
            groupInfo?.members[idx].image = userData.profilePicture
        }
        
        let visibleCells = messagesTableView.visibleCells
        visibleCells.forEach { (cell) in
            guard let index = cell.tableViewIndexPath(messagesTableView) else{ return }
            if messagesList[index.row].senderID == userData.userId{
                messagesTableView.reloadRows(at: [index], with: .automatic)
            }
        }
    }
    
    func tableViewScrollToBtm(){
        messagesTableView.layoutIfNeeded()
        messagesTableView.scrollToBottom(isAnimated: false)
    }
    
    func getImageMessage(messageData: MessageDetail){
        
        if let idx = self.messagesList.index(where: {!$0.isImageMsgReceived}){
            self.messagesList[idx] = messageData
            self.messagesTableView.reloadRows(at: [IndexPath(row: idx, section: 0)], with: .automatic)
            
            GroupChatFireBaseController.shared.updateReadCount(messsageData: messageData)
            GroupChatFireBaseController.shared.checkMessageRead(messsageData: messageData)
        }
    }
}

extension GroupChatVC: DeleteChatDelegate {
    
    func blockFromGroup(){
        navigationController?.popViewController(animated: true)
    }
}

extension GroupChatVC: GroupChatControllerDelegate {
    
    func getproductData(){
        view.layoutIfNeeded()
        productCollectionView.reloadData()
    }
    
    func getError(error: String){
        CommonFunctions.showToastMessage(error)
    }
}

extension GroupChatVC: ReceiverCellDelegate {
    
    func profileBtnTapped(button: UIButton){
        
        guard let index = button.tableViewIndexPath(messagesTableView) else { return }
        let data = messagesList[index.row]
        let userProfileScene = OtherUserProfileVC.instantiate(fromAppStoryboard: .Profile)
        userProfileScene.userId = data.senderID
        navigationController?.pushViewController(userProfileScene, animated: true)
    }
}

extension GroupChatVC: CustomPopUpDelegate{
    
    func okButtonTapped(){
        navigationController?.popToRootViewController(animated: true)
    }
}
