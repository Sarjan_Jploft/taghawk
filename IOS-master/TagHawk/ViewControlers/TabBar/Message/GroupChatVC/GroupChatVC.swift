//
//  GroupChatVC.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import MarqueeLabel
import DropDown

class GroupChatVC: UIViewController {

//    MARK:- IBOutlets
//    =================
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var navBarOuterView: UIView!
    @IBOutlet weak var writeMessageView: UIView!
    @IBOutlet weak var messageTextView: IQTextView!
    @IBOutlet weak var messagesTableView: UITableView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var shelfBtn: UIButton!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var muteLbl: UILabel!
    @IBOutlet weak var attachBtn: UIButton!
    @IBOutlet weak var announcementView: UIView!
    @IBOutlet weak var announcementLbl: MarqueeLabel!
    @IBOutlet weak var announcementHeight: NSLayoutConstraint!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productViewheight: NSLayoutConstraint!
    @IBOutlet weak var textViewBtnConstraint: NSLayoutConstraint!
    
//    MARK:- Proporties
//    =================
    var roomData: ChatListing?
    var messagesList: [MessageDetail] = []
    let controller = GroupChatController()
    var groupInfo: GroupDetail?
    var membersData: [GroupMembers] = []
    var isFromSearchScreen : Bool = false
    var message: String = ""
    var roomID: String?
    let moreDropDown = DropDown()
    var isDataLoading: Bool = false
    
    
//    MARK:- ViewController Life Cycle
//    ================================
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SingleChatFireBaseController.shared.delegate = self
        GroupChatFireBaseController.shared.delegate = self
        if let info = roomData{
            GroupChatFireBaseController.shared.changesInGroupDetail(roomID: info.roomID)
            GroupChatFireBaseController.shared.getMessages(roomID: info.roomID, timeStamp: info.createdTime)
            SingleChatFireBaseController.shared.checkUserBlocked()
        }else{
          GroupChatFireBaseController.shared.toCheckRoomExist(id: roomID ?? "")
        }
        self.addKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        IQKeyboardManager.shared.enable = true
        SingleChatFireBaseController.shared.removeObservers(roomId: roomData?.roomID ?? "", otherUserId: roomData?.otherUserID ?? "")
        GroupChatFireBaseController.shared.removeObservers()
       self.removeKeyboard()
    }
    
    deinit {
        GroupChatFireBaseController.shared.removeObservers()
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
//    MARK:- IBoutlets
//    ================
    @IBAction func backBtnTapped(_ sender: UIButton) {
        
        if isFromSearchScreen{
            navigationController?.popToRootViewController(animated: true)
        }else{
            AppNavigator.shared.parentNavigationControler.popViewController(animated: true)
//            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:- Send button tapped
    @IBAction func sendBtnTapped(_ sender: UIButton) {
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        let message = messageTextView.text.byRemovingLeadingTrailingWhiteSpaces.trimTrailingWhitespace()
        guard !message.isEmpty else { return }
        self.message = message
        GroupChatFireBaseController.shared.addmessage(text: message,
                                                      tagID: groupInfo?.tagID ?? "",
                                                      detail: groupInfo,
                                                      messageType: .text)
            
        sendBtn.isEnabled = false
        messageTextView.text = ""
        arrangeTextViewHeight()
    }
    
    //MARKl:- Attach button tapped
    @IBAction func attachBtnTapped(_ sender: UIButton) {
        let scene = AttachtmentPopUpVC.instantiate(fromAppStoryboard: .Messages)
        scene.modalTransitionStyle = .coverVertical
        scene.modalPresentationStyle = .overCurrentContext
        scene.vcInstantiatedForChat = false
        self.definesPresentationContext = true
        scene.delegate = self
        AppNavigator.shared.parentNavigationControler.present(scene, animated: true, completion: nil)
    }
    
    //MARK:- Header button tapped
    @IBAction func headerBtnTapped(_ sender: UIButton) {
        
        let scene = GroupDetailVC.instantiate(fromAppStoryboard: .Messages)
        scene.roomInfo = roomData
        scene.groupInfo = self.groupInfo
        navigationController?.pushViewController(scene, animated: true)
    }
    
    //MARK:- More button tapped
    @IBAction func moreBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        moreDropDown.anchorView = sender
        moreDropDown.bottomOffset = CGPoint(x: 0, y: moreDropDown.anchorView!.plainView.bounds.height)
        moreDropDown.show()
    }
    
    //MARK:- Select button tapped
    @IBAction func shelgBtnTapped(_ sender: UIButton) {
        proceedToTagShelfVC()
    }
    
    
    func sendNotification(userData: UserProfile, msgType: MessageType, msg: String, roomData: ChatListing?){
        
        if let data = groupInfo, let roomInfo = roomData{
            if !roomInfo.chatMute{
                // no issue from indexPath
                controller.sendNotification(userData: userData,
                                            groupInfo: data,
                                            text: msg,
                                            roomData: roomInfo)
                
            }
        }
    }
}

//MARK:- Methods
//==============
extension GroupChatVC {
    
    //MARK:- init set up
    private func initialSetup(){
        
        shelfBtn.titleLabel?.font = AppFonts.Galano_Semi_Bold.withSize(15)
        shelfBtn.setTitleColor(AppColors.blackLblColor)
        shelfBtn.setTitle(LocalizedString.shelf.localized)
        screenTitle.font = AppFonts.Galano_Medium.withSize(15)
        screenTitle.textColor = AppColors.blackLblColor
        messageTextView.placeholder = LocalizedString.writeYourMsg.localized
        messageTextView.placeholderTextColor = AppColors.textViewPlaceholderColor
        messageTextView.font = AppFonts.Galano_Regular.withSize(14)
        messageTextView.text = ""
        messageTextView.tintColor = AppColors.blackLblColor
        navBarOuterView.drawShadow(shadowColor: AppColors.blackLblColor, shadowOpacity: 0.1, shadowPath: nil, shadowRadius: 6.0, cornerRadius: 0.0, offset: CGSize(width: 0, height: 3))
        writeMessageView.drawShadow(shadowColor: AppColors.blackLblColor, shadowOpacity: 0.1, shadowPath: nil, shadowRadius: 6.0, cornerRadius: 0.0, offset: CGSize(width: 0, height: -3))
        navBarOuterView.clipsToBounds = false
        writeMessageView.clipsToBounds = false
        messageTextView.delegate = self
        productCollectionView.delegate = self
        productCollectionView.dataSource = self
        
        setupMoreDropdown()
        updateRoomData()
        messagesTableView.delegate = self
        messagesTableView.dataSource = self
        sendBtn.isEnabled = false
        screenTitle.text = roomData?.roomName ?? ""
        if roomData == nil{
            controller.getProducts(tagID: roomID ?? "")
        }else{
          controller.getProducts(tagID: roomData?.roomID ?? "")
        }
        
        
        controller.delegate = self
        navigationController?.setNavigationBarHidden(true, animated: true)
        muteLbl.isHidden = true
        muteLbl.font = AppFonts.Galano_Regular.withSize(12)
        muteLbl.textColor = AppColors.blackLblColor
        muteLbl.text = LocalizedString.sorryCantSendMessage.localized
        announcementLbl.font = AppFonts.Galano_Regular.withSize(14)
        announcementLbl.textColor = AppColors.blackLblColor
        announcementLbl.text = groupInfo?.announcement ?? ""
        announcementView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        productView.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.9294117647, blue: 0.937254902, alpha: 1)
        IQKeyboardManager.shared.enable = false
    }
    
    //MARK:- Add key buard
    private func addKeyboard(){
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: {[weak self] (notification) in
                                                guard let info = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
                                                
                                                guard let strongSelf = self else {return}
                                                
                                                let keyBoardHeight = info.cgRectValue.height
                                                
                                                var safeAreaBottomInset : CGFloat = 0
                                                if #available(iOS 11.0, *) {
                                                    safeAreaBottomInset = strongSelf.view.safeAreaInsets.bottom
                                                } else {
                                                    // Fallback on earlier versions
                                                }
                                                //
                                                
                                                UIView.animate(withDuration: 0.1,  delay: 0,
                                                               options: .curveEaseInOut,
                                                               animations: {
                                                                
                                                                if (info.cgRectValue.origin.y) >= screenHeight {
                                                                    strongSelf.textViewBtnConstraint.constant = 0
                                                                } else {
                                                                    
                                                                    strongSelf.textViewBtnConstraint.constant = (keyBoardHeight - safeAreaBottomInset)
                                                                }
                                                                strongSelf.view.layoutIfNeeded()
                                                }, completion: nil)
                                                
        })
    }
    
    //MARK:- remove keyboard
    private func removeKeyboard(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    //MARK:- Set up more dropdown
    fileprivate func setupMoreDropdown(){
        
        let dropDownDataSource = [LocalizedString.pinToTop.localized, LocalizedString.Mute.localized, LocalizedString.viewGroupProfile.localized]
        
        moreDropDown.dataSource = dropDownDataSource
        moreDropDown.width = 150
        moreDropDown.backgroundColor = AppColors.whiteColor
        moreDropDown.cellNib = UINib(nibName: PinToTopCell.defaultReuseIdentifier, bundle: nil)
        moreDropDown.selectionBackgroundColor = UIColor.clear
        moreDropDown.customCellConfiguration = {[weak self](index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? PinToTopCell else { return }
            cell.selectionStyle = .none
            cell.delegate = self
            cell.toggleBtn.tag = index
            cell.toggleBtn.isHidden = !(index == 0 || index == 1)
            cell.toggleBtn.isSelected = index == 0 ? (self?.roomData?.pinned ?? false) : (self?.roomData?.chatMute ??  false)
        }
        
        moreDropDown.selectionAction = {[weak self](index, value) in
            
            if index == 2{
                let scene = GroupDetailVC.instantiate(fromAppStoryboard: .Messages)
                scene.roomInfo = self?.roomData
                scene.groupInfo = self?.groupInfo
                self?.navigationController?.pushViewController(scene, animated: true)
            }
        }
        
        moreDropDown.dismissMode = .automatic
    }
}

extension GroupChatVC: PinToTopCellDelegate {
    
    //MARK:- Toggle botton tapped
    func toggleBtnTapped(btn: UIButton) {
        
        switch btn.tag{
            
        case 0:
            roomData?.pinned = btn.isSelected
            if let info = roomData{
                GroupChatFireBaseController.shared.pinnedToTop(isPinned: btn.isSelected, roomID: info.roomID)
            }
        case 1:
            roomData?.chatMute = btn.isSelected
            if let info = roomData{
                GroupChatFireBaseController.shared.muteAGroup(isMute: btn.isSelected, roomID: info.roomID)
            }
        default: return
        }
        moreDropDown.hide()
    }
    
    //MARK:- Update room data
    func updateRoomData(){
        
        if let data = roomData{
            
            moreDropDown.customCellConfiguration = {(index: Index, item: String, cell: DropDownCell) -> Void in
                guard let cell = cell as? PinToTopCell else { return }
                cell.delegate = self
                cell.toggleBtn.tag = index
                cell.toggleBtn.isHidden = !(index == 0 || index == 1)
                if index == 0{
                    cell.toggleBtn.isSelected = data.pinned
                }else if index == 1{
                    cell.toggleBtn.isSelected = data.chatMute
                }
            }
        }
    }
}
