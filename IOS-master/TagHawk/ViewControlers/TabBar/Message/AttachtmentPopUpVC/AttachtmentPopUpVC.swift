//
//  AttachtmentPopUpVC.swift
//  TagHawk
//
//  Created by Appinventiv on 14/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

enum AttachmentPopUp {
    case takePhoto
    case gallery
    case shareCommunity
    case shareProduct
}

protocol AttachmentPopupDelegate: class {
    func attachmentBtnTapped(tappedBtn: AttachmentPopUp)
}

class AttachtmentPopUpVC: UIViewController {

//    MARK:- IBOutlets
//    =================
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var takePhotoLbl: UILabel!
    @IBOutlet weak var galleryLbl: UILabel!
    @IBOutlet weak var shareCommunityLbl: UILabel!
    @IBOutlet weak var shareProductLbl: UILabel!
    @IBOutlet weak var shareCommunityStackView: UIStackView!
    @IBOutlet weak var shareProductStackView: UIStackView!
    
//    MARK:- proporties
//    =================
    weak var delegate: AttachmentPopupDelegate?
    var vcInstantiatedForChat: Bool = true
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        popupView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    //MARK:- Touches began
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let touch = touches.first{
             let location = touch.location(in: self.view)
            let fingerRect = CGRect(x: location.x, y: location.y, width: 10, height: 10)
            let popViewFrame = popupView.frame
            if !popViewFrame.intersects(fingerRect){
                dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
//    MARK:- IBActions
//    ================
    @IBAction func takePhotoBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        dismiss(animated: true) {[weak self] in
            self?.delegate?.attachmentBtnTapped(tappedBtn: .takePhoto)
        }
    }
    
    //MARK:- gallery button tapped
    @IBAction func galleryBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        dismiss(animated: true) {[weak self] in
            self?.delegate?.attachmentBtnTapped(tappedBtn: .gallery)
        }
    }
    
    //MARK:- Share community button tapped
    @IBAction func shareCommunityBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        dismiss(animated: true) {[weak self] in
            self?.delegate?.attachmentBtnTapped(tappedBtn: .shareCommunity)
        }
    }
    
    //MARK:- Share product button tapped
    @IBAction func shareProductBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        dismiss(animated: true) {[weak self] in
            self?.delegate?.attachmentBtnTapped(tappedBtn: .shareProduct)
        }
    }
}

extension AttachtmentPopUpVC {
    //MARK:- init set up
    private func initialSetup(){
        view.backgroundColor = AppColors.blackColor.withAlphaComponent(0.3)
        takePhotoLbl.font = AppFonts.Galano_Regular.withSize(13)
        takePhotoLbl.textColor = AppColors.blackLblColor
        takePhotoLbl.text = LocalizedString.TakePhoto.localized
        galleryLbl.font = AppFonts.Galano_Regular.withSize(13)
        galleryLbl.textColor = AppColors.blackLblColor
        galleryLbl.text = LocalizedString.gallery.localized
        shareCommunityLbl.font = AppFonts.Galano_Regular.withSize(13)
        shareCommunityLbl.textColor = AppColors.blackLblColor
        shareCommunityLbl.text = LocalizedString.shareCommunity.localized
        shareProductLbl.font = AppFonts.Galano_Regular.withSize(13)
        shareProductLbl.textColor = AppColors.blackLblColor
        shareProductLbl.text = LocalizedString.shareProduct.localized
        shareCommunityStackView.isHidden = !vcInstantiatedForChat
        shareProductStackView.isHidden = !vcInstantiatedForChat
    }
}
