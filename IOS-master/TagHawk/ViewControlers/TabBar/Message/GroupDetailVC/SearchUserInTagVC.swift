//
//  SearchUserInTagVCViewController.swift
//  TagHawk
//
//  Created by Admin on 5/31/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

class SearchUserInTagVC : BaseVC {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchIntagTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    
    var groupInfo: GroupDetail?
    private var isSearchingMember: Bool = false
    private let userInfoDropDown = DropDown()
    private var selectedMemberIndex: Int?
    private let controller = GroupDetailController()
    private var groupStatus: GroupRemove = .remove
    var currentMemberData: GroupMembers?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        
        controller.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SingleChatFireBaseController.shared.delegate = self
        GroupChatFireBaseController.shared.delegate = self
        self.configureNAvigation()
    }
    
    //MARK:- Back button Tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension SearchUserInTagVC {
    
    
    func setUpSubView(){
        self.searchBar.delegate = self
        self.searchBar.becomeFirstResponder()
        self.configureTablerView()
        self.setupDropdown()
    }
    
    func configureTablerView(){
        self.searchIntagTableView.dataSource = self
        self.searchIntagTableView.delegate = self
        self.searchIntagTableView.separatorStyle = .none
    }
    
    func setupDropdown(){
        userInfoDropDown.backgroundColor = AppColors.whiteColor
        userInfoDropDown.cellHeight = 40
        userInfoDropDown.dismissMode = .onTap
        userInfoDropDown.textColor = AppColors.blackLblColor
        userInfoDropDown.textFont = AppFonts.Galano_Medium.withSize(11)
        userInfoDropDown.selectionAction = { [weak self] (index: Int, item: String) in
            self?.changeMemberStatus(index: index, item: item)
        }
    }
    
    // Change Status by tap on dropdown
    private func changeMemberStatus(index: Int, item: String){
        
        if let idx = selectedMemberIndex, let info = groupInfo{
            
            if index == 0{
                let userProfileScene = OtherUserProfileVC.instantiate(fromAppStoryboard: .Profile)
                userProfileScene.userId = info.members[idx].id
                navigationController?.pushViewController(userProfileScene, animated: true)
                return
            }
            
            if info.members[idx].isBlocked{
                
                groupInfo?.members[idx].isBlocked = false
                GroupChatFireBaseController.shared.blockUnblockUser(isBlock: false,
                                                                    roomID: info.tagID,
                                                                    userID: info.members[idx].id)
                
            }else{
                
                if item == LocalizedString.makeGroupAdmin.localized{
                    groupInfo?.members[idx].type = MemberType.admin
                    GroupChatFireBaseController.shared.changeMemberType(roomID: info.tagID,
                                                                        userID: info.members[idx].id,
                                                                        memberType: MemberType.admin)
                    
                }else if item == LocalizedString.transferOwnership.localized{
                    controller.transferOwnerShip(tagID: info.tagID,
                                                 userID: info.members[idx].id,
                                                 index: idx)
                }else if item == LocalizedString.dismissAsAdmin.localized{
                    groupInfo?.members[idx].type = MemberType.member
                    GroupChatFireBaseController.shared.changeMemberType(roomID: info.tagID,
                                                                        userID: info.members[idx].id,
                                                                        memberType: MemberType.member)
                }else{
                    if item.contains(s: LocalizedString.Block.localized){
                        groupInfo?.members[idx].isBlocked = true
                        let vc = RemoveFriendPopUp.instantiate(fromAppStoryboard: AppStoryboard.Profile)
                        vc.commingFor = .block
                        let data = FollowingFollowersModel()
                        data.userId = groupInfo?.members[idx].id ?? ""
                        data.profilePicture = groupInfo?.members[idx].image ?? ""
                        data.fullName = groupInfo?.members[idx].name ?? ""
                        vc.user = data
                        vc.delegate = self
                        vc.modalPresentationStyle = .overCurrentContext
                        navigationController?.present(vc, animated: true, completion: nil)
                        
                    }else if item.contains(s: LocalizedString.Mute.localized){
                        groupInfo?.members[idx].isMute = true
                        GroupChatFireBaseController.shared.muteUnmuteMember(isMute: true,
                                                                            roomID: info.tagID,
                                                                            userID: info.members[idx].id)
                        
                    }else if item.contains(s: LocalizedString.message.localized){
                        if let index = selectedMemberIndex, let info = groupInfo{
                            let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
                            scene.vcInstantiated = .userDetail
                            scene.otherUserData = UserProfile.getUserDataFromGroupMembers(data: info.members[index])
                            AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
                        }
                    }else if item.contains(s: LocalizedString.unmute.localized){
                        groupInfo?.members[idx].isMute = false
                        GroupChatFireBaseController.shared.muteUnmuteMember(isMute: false,
                                                                            roomID: info.tagID,
                                                                            userID: info.members[idx].id)
                        
                    }else if item.contains(s: LocalizedString.Remove.localized){
                        controller.removeMember(tagID: info.tagID,
                                                userID: info.members[idx].id,
                                                index: idx)
                    }
                }
            }
            
            let indexPath = IndexPath(row: idx, section: 0)
            guard let cell = searchIntagTableView.cellForRow(at: indexPath) as? GroupMemberCell else{
                return
            }
            let data = isSearchingMember ? groupInfo?.searchedMembers : groupInfo?.searchedMembers
            cell.populateData(members: data ?? [], indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let groupData = groupInfo else { return }
        let memberData = isSearchingMember ? groupData.searchedMembers : groupData.members
        guard !memberData.isEmpty else{ return }
        guard let cell = tableView.cellForRow(at: indexPath) as? GroupMemberCell else { return }
        let member = memberData[indexPath.row]
        guard member.id != UserProfile.main.userId else{ return }
        selectedMemberIndex = indexPath.row
        userInfoDropDown.anchorView = cell.userName
        userInfoDropDown.bottomOffset = CGPoint(x: 0, y:userInfoDropDown.anchorView!.plainView.bounds.height)
        
        if let myData = currentMemberData{
            
            switch myData.type{
                
            case .admin:
                if member.type != .owner {
                    let messageText = LocalizedString.message.localized + " " + member.name
                    let blockText = LocalizedString.Block.localized + " " + member.name
                    let muteText = LocalizedString.Mute.localized + " " + member.name
                    userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText, blockText, muteText]
                }else{
                    let messageText = LocalizedString.message.localized + " " + member.name
                    userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText]
                }
            case .owner:
                
                if member.isBlocked{
                    let unblockText = LocalizedString.Unblock.localized + " " + member.name
                    userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, unblockText]
                }else{
                    
                    switch member.type{
                        
                    case .admin:
                        
                        let messageText = LocalizedString.message.localized + " " + member.name
                        let blockText = LocalizedString.Block.localized + " " + member.name
                        let muteText = LocalizedString.Mute.localized + " " + member.name
                        userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText, LocalizedString.dismissAsAdmin.localized, LocalizedString.transferOwnership.localized, blockText, muteText]
                        
                    case .owner: return
                        
                    case .member:
                        let messageText = LocalizedString.message.localized + " " + member.name
                        let blockText = LocalizedString.Block.localized + " " + member.name
                        let removeText = LocalizedString.Remove.localized + " " + member.name
                        let muteText = member.isMute ? LocalizedString.unmute.localized : LocalizedString.Mute.localized + " " + member.name
                        userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText, LocalizedString.makeGroupAdmin.localized, LocalizedString.transferOwnership.localized, blockText, muteText, removeText]
                    }
                }
            case .member:
                let messageText = LocalizedString.message.localized + " " + member.name
                let blockText = LocalizedString.Block.localized + " " + member.name
                userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText, blockText]
            }
        }
        
        userInfoDropDown.reloadAllComponents()
        userInfoDropDown.show()
    }
    
}


extension SearchUserInTagVC {
    
    func configureNAvigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}

extension SearchUserInTagVC : UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let memberData = isSearchingMember ? groupInfo?.searchedMembers : groupInfo?.members
        return memberData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: GroupMemberCell.self)
        let memberData = isSearchingMember ? groupInfo?.searchedMembers : groupInfo?.members
        cell.populateData(members: memberData ?? [], indexPath: indexPath)
        return cell
    }
    
}


extension SearchUserInTagVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        
        isSearchingMember = !searchText.isEmpty
        if !searchText.isEmpty{
            let members = groupInfo?.members.filter({$0.name.localizedCaseInsensitiveContains(searchText)})
            groupInfo?.searchedMembers = members ?? []
            self.searchIntagTableView.reloadData()
        }else{
            self.searchIntagTableView.reloadData()
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        view.endEditing(true)
        self.searchIntagTableView.reloadData()
        // groupDetailTableView.reloadSections(IndexSet(integer: 2), with: .none)
    }
}


extension SearchUserInTagVC: GroupDetailControllerDelegate {
    
    func error(message: String){
        CommonFunctions.showToastMessage(message)
    }
    
    func tagDeleted(indexPath: IndexPath){
        GroupChatFireBaseController.shared.deleteGroup(roomID: groupInfo?.tagID ?? "",
                                                       members: groupInfo?.members ?? [])
    }
    
    func exitTag(id: String, pinned: Bool){
        groupStatus = .exit
        GroupChatFireBaseController.shared.exitFromGroup(roomID: groupInfo?.tagID ?? "", isPinned: pinned)
    }
    
    func transferOwnerShip(otherUserID: String, indexVal: Int){
        
        groupInfo?.members[indexVal].type = MemberType.owner
        GroupChatFireBaseController.shared.changeMemberType(roomID: groupInfo?.tagID ?? "",
                                                            userID: groupInfo?.members[indexVal].id ?? "",
                                                            memberType: MemberType.owner)
        GroupChatFireBaseController.shared.changeMemberType(roomID: groupInfo?.tagID ?? "",
                                                            userID: UserProfile.main.userId,
                                                            memberType: MemberType.member)
        GroupChatFireBaseController.shared.addmessage(text: groupInfo?.members[indexVal].id ?? "",
                                                      tagID: groupInfo?.tagID ?? "",
                                                      detail: groupInfo,
                                                      messageType: .ownershipTransfer)
        
        if let idxx = groupInfo?.members.index(where: {$0.id == UserProfile.main.userId}){
            groupInfo?.members[idxx].type = MemberType.member
            let indexPath1 = IndexPath(row: (idxx + 1), section: 0)
            guard let cell = searchIntagTableView.cellForRow(at: indexPath1) as? GroupMemberCell else{
                return
            }
            searchIntagTableView.beginUpdates()
            searchIntagTableView.endUpdates()
            let data = isSearchingMember ? groupInfo?.searchedMembers : groupInfo?.searchedMembers
            cell.populateData(members: data ?? [], indexPath: indexPath1)
        }
    }
    
    func removeMember(indexVal: Int){
        GroupChatFireBaseController.shared.removeMember(roomID: groupInfo?.tagID ?? "",
                                                        userID: groupInfo?.members[indexVal].id ?? "",
                                                        isForRemoveMember: true,
                                                        success: {
        })
        groupInfo?.members.remove(at: indexVal)
        searchIntagTableView.reloadSections([2], with: .none)
    }
    
    func blockedUser(indexVal: Int){
        SingleChatFireBaseController.shared.blockedUser(userID: UserProfile.main.userId,
                                                        otherUserID: groupInfo?.members[indexVal].id ?? "")
        searchIntagTableView.beginUpdates()
        groupInfo?.members.remove(at: indexVal)
        searchIntagTableView.deleteRows(at: [IndexPath(row: indexVal, section: 0)], with: .fade)
        searchIntagTableView.endUpdates()
    }
}

extension SearchUserInTagVC: RemoveFriendPopUpDelegate {
    
    func yesButtonTapped(type : RemoveFriendPopUp.RemoveFriendFor, user : FollowingFollowersModel){
        
        if type == .block{
            view.showIndicator()
            if let idx = groupInfo?.members.firstIndex(where: {$0.id == user.userId}){
                controller.blockedUser(otherUserID: user.userId, index: idx)
            }
        }
    }
}

extension SearchUserInTagVC: SingleChatFireBaseControllerDelegate{
    func userBlocked(){
        self.view.hideIndicator()
    }
}


extension SearchUserInTagVC : GroupChatFireBaseControllerDelegate {
    
    func userBlockedByAnother(roomData: ChatListing) {
        if roomData.roomID == (groupInfo?.tagID ?? ""){
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func getGroupDetail(detail: GroupDetail, pinned: Bool){
        updateGroupData(tagInfo: detail)
    }
    
    func changedInRoomData(detail: GroupDetail){
        updateGroupData(tagInfo: detail)
    }
    
    func updateGroupData(tagInfo: GroupDetail){
        
        isSearchingMember = false
        groupInfo = tagInfo
        groupInfo?.members.forEach({[weak self](member) in
            SingleChatFireBaseController.shared.getUserInfo(userID: member.id,
                                                            indexPath: IndexPath(row: 0, section: 0),
                                                            success: {[weak self](idx, userData) in
                                                                
                                                                guard let strongSelf = self else{ return }
                                                                
                                                                if let index = strongSelf.groupInfo?.members.firstIndex(where: {$0.id == userData.userId}){
                                                                    strongSelf.groupInfo?.members[index].image = userData.profilePicture
                                                                    strongSelf.groupInfo?.members[index].name = userData.fullName
                                                                    let indexPath = IndexPath(row: index, section: 2)
                                                                    guard let cell = strongSelf.searchIntagTableView.cellForRow(at: indexPath) as? GroupMemberCell else { return }
                                                                    cell.populateData(members: (strongSelf.groupInfo?.members ?? []), indexPath: indexPath)
                                                                }
            })
        })
        currentMemberData = tagInfo.members.filter({$0.id == UserProfile.main.userId}).first
        searchIntagTableView.reloadData()
    }
    
    func exitFromGroup(id: String, pinned: Bool){
        GroupChatFireBaseController.shared.addmessage(text: UserProfile.main.userId,
                                                      tagID: groupInfo?.tagID ?? "",
                                                      detail: groupInfo,
                                                      messageType: .userLeft)
        proceedToMessageListingVC()
    }
    
    func deleteGroup(){
        proceedToMessageListingVC()
    }
    
    func isGroupDeleted(){
        proceedToMessageListingVC()
    }
    
    func proceedToMessageListingVC(){
        navigationController?.popToRootViewController(animated: true)
        
        guard let homeVc = AppNavigator.shared.tabBar?.viewControllers?.first?.children.first as? HomeVC else { return }
        
        let updatedTags = homeVc.homeTagsVc.tags.filter { $0.id != groupInfo?.tagID }
        homeVc.homeTagsVc.tags = updatedTags
        homeVc.homeTagsVc.mapView.clear()
        homeVc.homeTagsVc.clusterManager.clearItems()
        homeVc.homeTagsVc.generateClusterItems()
        
        
        //        let viewControllers = navigationController?.viewControllers ?? []
        //        let selectedController = viewControllers[viewControllers.count - 3]
        //        navigationController?.popToViewController(selectedController, animated: true)
    }
}
