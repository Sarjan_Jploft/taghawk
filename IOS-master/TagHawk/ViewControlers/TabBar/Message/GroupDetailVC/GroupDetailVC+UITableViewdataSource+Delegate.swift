//
//  GroupDetailVC+UITableViewdataSource+Delegate.swift
//  TagHawk
//
//  Created by Appinventiv on 12/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

//MARK:- tableview datasource
extension GroupDetailVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section{
            
        case 0: return 4
            
        case 1:
            guard let data = groupInfo else{
                return 0
            }
            return data.members.isEmpty ? 0 : 1
            
        default:
            guard let data = groupInfo else{
                return 0
            }
            return isSearchingMember ? data.searchedMembers.count : data.members.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section{
            
        case 0:
            switch indexPath.row{
                
            case 0:
                let cell = tableView.dequeueCell(with: GroupDetailCell.self)
                cell.populateData(detail: groupInfo,
                                  roomInfo: roomInfo,
                                  member: currentMemberData)
                cell.delegate = self
                
                return cell
                
            case 1:
                let cell = tableView.dequeueCell(with: TagTypeCell.self)
                cell.populateData(detail: groupInfo, member: currentMemberData)
                cell.delegate = self
                return cell
                
            case 2:
                let cell = tableView.dequeueCell(with: VerificationMethodCell.self)
                cell.populateData(detail: groupInfo, member: currentMemberData)
                cell.delegate = self
                return cell
                
            case 3:
                let cell = tableView.dequeueCell(with: PendingRequestCell.self)
                cell.populateData(groupData: groupInfo)
                return cell
                
            default: fatalError()
            }
            
        case 1:
            let cell = tableView.dequeueCell(with: GroupMemberSearchCell.self)
            cell.searchBar.delegate = self
            if !isSearchingMember{
                cell.searchBar.text = ""
            }
            return cell
            
        case 2:
            
            let cell = tableView.dequeueCell(with: GroupMemberCell.self)
            let memberData = isSearchingMember ? groupInfo?.searchedMembers : groupInfo?.members
            cell.populateData(members: memberData ?? [], indexPath: indexPath)
            
            return cell
            
        default: fatalError()
        }
    }
}

//MARK:- Tableview delegate
extension GroupDetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section{
            
        case 0:
            switch indexPath.row{
            case 0: return UITableView.automaticDimension
                
            case 1: guard let member = currentMemberData else{ return CGFloat.leastNormalMagnitude }
            return member.type == .member ? CGFloat.leastNormalMagnitude : 80
                
            case 2: guard let groupData = groupInfo else { return CGFloat.leastNormalMagnitude }
            guard let member = currentMemberData else{ return CGFloat.leastNormalMagnitude }
            let height = groupData.tagEntrance == .publicType ? CGFloat.leastNormalMagnitude : UITableView.automaticDimension
            return member.type == .member ? CGFloat.leastNormalMagnitude : height
                
            case 3: guard let groupData = groupInfo else { return CGFloat.leastNormalMagnitude }
            guard let memderData = currentMemberData else{ return CGFloat.leastNormalMagnitude }
            guard memderData.type == .owner else { return CGFloat.leastNormalMagnitude}
            return groupData.pendingRequestCount > 0 ? 50 : CGFloat.leastNormalMagnitude
                
            default: return CGFloat.leastNormalMagnitude
                
            }
            
        case 1: return 50
            
        case 2: return 60
            
        default: return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return (section == 0 || section == 2) ? CGFloat.leastNormalMagnitude : 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = tableView.dequeueHeaderFooter(with: MessageHeaderView.self)
        view.headerLeadingConstraint.constant = 20
        view.headerViewLbl.text = LocalizedString.groupMembers.localized + " (" + "\(groupInfo?.members.count ?? 0)" + ")"
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
            
        case 0:
            if indexPath.row == 3{
                let scene = PendingRequestVC.instantiate(fromAppStoryboard: .Messages)
                scene.groupInfo = groupInfo
                navigationController?.pushViewController(scene, animated: true)
            }
        case 2:
            guard let groupData = groupInfo else { return }
            guard !groupData.members.isEmpty else{ return }
            guard let cell = tableView.cellForRow(at: indexPath) as? GroupMemberCell else { return }
            let member = groupData.members[indexPath.row]
            guard member.id != UserProfile.main.userId else{ return }
            selectedMemberIndex = indexPath.row
            userInfoDropDown.anchorView = cell.userName
            userInfoDropDown.bottomOffset = CGPoint(x: 0, y:userInfoDropDown.anchorView!.plainView.bounds.height)
            
            if let myData = currentMemberData{
                
                switch myData.type{
                    
                case .admin:
                    if member.type != .owner {
                        let messageText = LocalizedString.message.localized + " " + member.name
                        let blockText = LocalizedString.Block.localized + " " + member.name
                        let muteText = LocalizedString.Mute.localized + " " + member.name
                        userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText, blockText, muteText]
                    }else{
                        let messageText = LocalizedString.message.localized + " " + member.name
                        userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText]
                    }
                case .owner:
                    
                    if member.isBlocked{
                        let unblockText = LocalizedString.Unblock.localized + " " + member.name
                        userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, unblockText]
                    }else{
                        
                        switch member.type{
                            
                        case .admin:
                            
                            let messageText = LocalizedString.message.localized + " " + member.name
                            
                            let blockText = LocalizedString.Block.localized + " " + member.name
                            let muteText = LocalizedString.Mute.localized + " " + member.name
                            userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText, LocalizedString.dismissAsAdmin.localized, LocalizedString.transferOwnership.localized, blockText, muteText]
                            
                        case .owner: return
                            
                        case .member:
                            let messageText = LocalizedString.message.localized + " " + member.name
                            let blockText = LocalizedString.Block.localized + " " + member.name
                            let removeText = LocalizedString.Remove.localized + " " + member.name
                            let muteText = member.isMute ? LocalizedString.unmute.localized : LocalizedString.Mute.localized + " " + member.name
                            userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText, LocalizedString.makeGroupAdmin.localized, LocalizedString.transferOwnership.localized, blockText, muteText, removeText]
                        }
                    }
                case .member:
                    let messageText = LocalizedString.message.localized + " " + member.name
                    let blockText = LocalizedString.Block.localized + " " + member.name
                    userInfoDropDown.dataSource = [LocalizedString.viewProfile.localized, messageText, blockText]
                }
            }
            
            userInfoDropDown.reloadAllComponents()
            userInfoDropDown.show()
            
        default: return
        }
    }
}

extension GroupDetailVC: GroupChatFireBaseControllerDelegate {
    
    //MARK:- user blocked by another user
    func userBlockedByAnother(roomData: ChatListing) {
        if roomData.roomID == (groupInfo?.tagID ?? ""){
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    //MARK:- get group detail
    func getGroupDetail(detail: GroupDetail, pinned: Bool){
        updateGroupData(tagInfo: detail)
    }
    
    //MARK:- Change in room data
    func changedInRoomData(detail: GroupDetail){
        updateGroupData(tagInfo: detail)
    }
    
    //MARK:- Update group data
    func updateGroupData(tagInfo: GroupDetail){
        
        isSearchingMember = false
        groupInfo = tagInfo
        
        groupInfo?.members.forEach({[weak self](member) in
            SingleChatFireBaseController.shared.getUserInfo(userID: member.id, indexPath: IndexPath.init(item: 0, section: 0), success: {[weak self](index, userData) in
                
                guard let strongSelf = self else{ return }
                
                
                if let index = strongSelf.groupInfo?.members.firstIndex(where: {$0.id == userData.userId}){
                    strongSelf.groupInfo?.members[index].image = userData.profilePicture
                    strongSelf.groupInfo?.members[index].name = userData.fullName
                    let indexPath = IndexPath(row: index, section: 2)
                    guard let cell = strongSelf.groupDetailTableView.cellForRow(at: indexPath) as? GroupMemberCell else { return }
                    cell.populateData(members: (strongSelf.groupInfo?.members ?? []), indexPath: indexPath)
                }
            })
        })
        currentMemberData = tagInfo.members.filter({$0.id == UserProfile.main.userId}).first
        groupDetailTableView.reloadData()
        if currentMemberData?.type == .owner {
            deleteBtn.setTitle(LocalizedString.deleteGroup.localized)
        }else{
            deleteBtn.setTitle(LocalizedString.existGroup.localized)
        }
    }
    
    func exitFromGroup(id: String, pinned: Bool){
        
        proceedToMessageListingVC()
    }
    
    func deleteGroup(){
        proceedToMessageListingVC()
    }
    
    func isGroupDeleted(){
        proceedToMessageListingVC()
    }
    
    //MARK:- Proceed to message listing vc
    func proceedToMessageListingVC(){
        navigationController?.popToRootViewController(animated: true)
        
        guard let homeVc = AppNavigator.shared.tabBar?.viewControllers?.first?.children.first as? HomeVC else { return }
        let updatedTags = homeVc.homeTagsVc.tags.filter { $0.id != groupInfo?.tagID }
        homeVc.homeTagsVc.tags = updatedTags
        homeVc.homeTagsVc.mapView.clear()
        homeVc.homeTagsVc.clusterManager.clearItems()
        homeVc.homeTagsVc.generateClusterItems()
    }
}

//MARK:- UISearch bar delegate
extension GroupDetailVC: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        let vc = SearchUserInTagVC.instantiate(fromAppStoryboard: AppStoryboard.Messages)
        vc.groupInfo = self.groupInfo
        vc.currentMemberData = self.currentMemberData
        self.navigationController?.pushViewController(vc, animated: false)
        
        return false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        
        isSearchingMember = !searchText.isEmpty
        if !searchText.isEmpty{
            let members = groupInfo?.members.filter({$0.name.localizedCaseInsensitiveContains(searchText)})
            groupInfo?.searchedMembers = members ?? []
        }else{
            groupDetailTableView.reloadSections(IndexSet(integer: 2), with: .none)
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        view.endEditing(true)
        groupDetailTableView.reloadSections(IndexSet(integer: 2), with: .none)
    }
}

extension GroupDetailVC: TagTypeCellDelegate {
    
    func switchBtnTapped(type: TagEntrance){
        
        controller.changeGroupDetail(tagID: groupInfo?.tagID ?? "",
                                     text: "",
                                     editTagType: .tagType,
                                     tagType: type,
                                     privateTagType: .documents,
                                     latitude: nil,
                                     longitude: nil)
    }
}

extension GroupDetailVC: GroupDetailCellDelegate {
    
    func descriptionBtnTapped(button: UIButton) {
        openUpdateEmailVC(instantiate: .description)
    }
    
    func editImageBtnTapped(button: UIButton) {
        
        let scene = AttachtmentPopUpVC.instantiate(fromAppStoryboard: .Messages)
        scene.modalTransitionStyle = .coverVertical
        scene.modalPresentationStyle = .overCurrentContext
        scene.vcInstantiatedForChat = false
        self.definesPresentationContext = true
        scene.delegate = self
        AppNavigator.shared.parentNavigationControler.present(scene, animated: true, completion: nil)
    }
    
    func announcementBtnTapped(button: UIButton) {
        openUpdateEmailVC(instantiate: .announcement)
    }
    
    func editBtnTapped(button: UIButton) {
        openUpdateEmailVC(instantiate: .tagTitle)
    }
    
    func pinToTopBtnTapped(button: UIButton) {
        roomInfo?.pinned = button.isSelected
        GroupChatFireBaseController.shared.pinnedToTop(isPinned: button.isSelected, roomID: groupInfo?.tagID ?? "")
    }
    
    func muteTapped(button: UIButton) {
        roomInfo?.chatMute = button.isSelected
        GroupChatFireBaseController.shared.muteAGroup(isMute: button.isSelected, roomID: groupInfo?.tagID ?? "")
    }
    
    func localtionTapped(button: UIButton) {
        let vc = SelectLocationVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
}

extension GroupDetailVC: VerificationMethodCelldelegate {
    
    //MARK:- Document button tapped
    func documentBtnTapped(){
        GroupChatFireBaseController.shared.updateTagType(roomID: groupInfo?.tagID ?? "",
                                                         tagType: .privateType,
                                                         privateTagType: .documents,
                                                         verificationData: "")
    }
    
    //MARK:- Edit button tapped
    func editBtnTapped(isEmail: Bool){
        openUpdateEmailVC(instantiate: isEmail ? .email : .password)
    }
    
    func openUpdateEmailVC(instantiate: UpdateEmailVCInstantiated){
        let scene = UpdateEmailVC.instantiate(fromAppStoryboard: .Messages)
        scene.delegate = self
        scene.groupName = groupInfo?.tagName ?? ""
        scene.modalTransitionStyle = .coverVertical
        scene.modalPresentationStyle = .overCurrentContext
        scene.instantitaed = instantiate
        self.definesPresentationContext = true
        AppNavigator.shared.parentNavigationControler.present(scene, animated: true, completion: nil)
    }
}

extension GroupDetailVC: UpdateEmailVCDeleagte {
    
    func updates(text: String, type: UpdateEmailVCInstantiated, indexPath: IndexPath?) {
        
        switch type {
            
        case .email, .password:
            controller.changeGroupDetail(tagID: groupInfo?.tagID ?? "",
                                         text: text,
                                         editTagType: .tagType,
                                         tagType: .privateType,
                                         privateTagType: (type == .email ? .email : .password),
                                         latitude: nil,
                                         longitude: nil)
            
        case .announcement:
            
            controller.changeGroupDetail(tagID: groupInfo?.tagID ?? "",
                                         text: text,
                                         editTagType: .announcement,
                                         tagType: nil,
                                         privateTagType: .none,
                                         latitude: nil,
                                         longitude: nil)
        case .tagTitle:
            controller.changeGroupDetail(tagID: groupInfo?.tagID ?? "",
                                         text: text,
                                         editTagType: .tagName,
                                         tagType: nil,
                                         privateTagType: .none,
                                         latitude: nil,
                                         longitude: nil)
            
        case .deleteGroup:
            controller.deleteTag(tagID: groupInfo?.tagID ?? "", index: IndexPath(row: 0, section: 0))
            
        case .description:
            controller.changeGroupDetail(tagID: groupInfo?.tagID ?? "",
                                         text: text,
                                         editTagType: .description,
                                         tagType: nil,
                                         privateTagType: .none,
                                         latitude: nil,
                                         longitude: nil)
        }
    }
}

extension GroupDetailVC: DeleteChatDelegate {
    
    //MARK:- Exit button tapped
    func exitBtnTapped(index: IndexPath?){
        controller.exitGroup(tagID: groupInfo?.tagID ?? "", isPinned: false)
    }
    
    func blockFromGroup(){
        navigationController?.popToRootViewController(animated: true)
    }
}

extension GroupDetailVC: GedAddressBack {
    
    func getAddress(lat : Double, long:Double,addressString: String){
        
        LocationManager.shared.reverseGeocodeLocationWithLatLon(latitude: lat, longitude: long) {[weak self](dict, place, error) in
            guard error == nil else{ return }
            self?.controller.changeGroupDetail(tagID: self?.groupInfo?.tagID ?? "",
                                               text: addressString,
                                               editTagType: .location,
                                               tagType: nil,
                                               privateTagType: nil,
                                               latitude: lat,
                                               longitude: long,
                                               city: place?.locality ?? "")
        }
    }
}

extension GroupDetailVC: AttachmentPopupDelegate{
    
    //MARK:- Attachment button tapped
    func attachmentBtnTapped(tappedBtn: AttachmentPopUp){
        
        switch tappedBtn{
            
        case .takePhoto: checkAndOpenCamera(delegate: self)
            
        case .gallery: checkAndOpenLibrary(delegate: self)
            
        default: return
            
        }
    }
}

//MARK:- Image picker delegate
extension GroupDetailVC: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[.originalImage] as? UIImage {
            picker.dismiss(animated: true) {
                self.uploadLogoImageToAWS(image: img)
            }
        }
    }
    
    func uploadLogoImageToAWS(image: UIImage) {
        
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        DispatchQueue.main.async {
            self.view.showIndicator()
        }
        
        image.uploadImageToS3(imageIndex: 0, success: {[weak self](imageIndex, success, imageUrl) in
            
            if success{
                self?.controller.changeGroupDetail(tagID: self?.groupInfo?.tagID ?? "",
                                                   text: imageUrl,
                                                   editTagType: .tagImage,
                                                   tagType: nil,
                                                   privateTagType: nil,
                                                   latitude: nil,
                                                   longitude: nil)
            }
            self?.view.hideIndicator()
            }, progress: { (imageIndex, progress) in
        }, failure: {[weak self](imageindex, error) in
            CommonFunctions.showToastMessage(error.localizedDescription)
            self?.view.hideIndicator()
        })
    }
}


extension GroupDetailVC: GroupDetailControllerDelegate {
    
    func error(message: String){
        CommonFunctions.showToastMessage(message)
    }
    
    func tagEdited(editType: EditTagType, tagData: AddTagResult){
        switch editType {
            
        case .tagEntrance:
            
            groupInfo?.tagEntrance = tagData.tagEntrance
            
            if tagData.tagEntrance == .privateType{
                GroupChatFireBaseController.shared.updateTagType(roomID: groupInfo?.tagID ?? "",
                                                                 tagType: .privateType,
                                                                 privateTagType: tagData.privateTagType,
                                                                 verificationData: tagData.tagData)
            }else{
                GroupChatFireBaseController.shared.changeTagType(roomID: groupInfo?.tagID ?? "",
                                                                 tagType: tagData.tagEntrance)
                groupDetailTableView.reloadSections([0], with: .automatic)
            }
        case .tagImage:
            GroupChatFireBaseController.shared.updateGroupImage(imageUrl: tagData.tagImageURL,
                                                                tagDetail: self.groupInfo)
            
        case .tagName:
            GroupChatFireBaseController.shared.updateTagTitle(roomID: groupInfo?.tagID ?? "",
                                                              title: tagData.name,
                                                              members: groupInfo?.members ?? [])
            
        case .announcement:
            GroupChatFireBaseController.shared.updateAnnouncement(roomID: groupInfo?.tagID ?? "", title: tagData.announcement)
            
        case .location:
            groupInfo?.tagAddress = tagData.tagAddress
            groupInfo?.latitude = tagData.tagLat
            groupInfo?.longitude = tagData.tagLong
            GroupChatFireBaseController.shared.updateTagLocation(roomID: groupInfo?.tagID ?? "", location: tagData.tagAddress, latitude: tagData.tagLat, longitude: tagData.tagLong)
            
        case .description:
            GroupChatFireBaseController.shared.updateDesciption(roomID: groupInfo?.tagID ?? "", title: tagData.description)
            
        case .tagType:
            print("Need to done")
            
        }
    }
    
    func tagDeleted(indexPath: IndexPath){
        GroupChatFireBaseController.shared.deleteGroup(roomID: groupInfo?.tagID ?? "",
                                                       members: groupInfo?.members ?? [])
    }
    
    func exitTag(id: String, pinned: Bool){
        groupStatus = .exit
        GroupChatFireBaseController.shared.exitFromGroup(roomID: groupInfo?.tagID ?? "", isPinned: pinned)
    }
    
    func transferOwnerShip(otherUserID: String, indexVal: Int){
        
        groupInfo?.members[indexVal].type = MemberType.owner
        GroupChatFireBaseController.shared.changeMemberType(roomID: groupInfo?.tagID ?? "",
                                                            userID: groupInfo?.members[indexVal].id ?? "",
                                                            memberType: MemberType.owner)
        GroupChatFireBaseController.shared.changeMemberType(roomID: groupInfo?.tagID ?? "",
                                                            userID: UserProfile.main.userId,
                                                            memberType: MemberType.member)
        GroupChatFireBaseController.shared.addmessage(text: groupInfo?.members[indexVal].id ?? "",
                                                      tagID: groupInfo?.tagID ?? "",
                                                      detail: groupInfo,
                                                      messageType: .ownershipTransfer)
        
        GroupChatFireBaseController.shared.changeOtherUserIDOnRoomInfo(members: groupInfo?.members ?? [],
                                                                       tagID: groupInfo?.tagID ?? "",
                                                                       ownerID: groupInfo?.members[indexVal].id ?? "")
        
        if let idxx = groupInfo?.members.index(where: {$0.id == UserProfile.main.userId}){
            groupInfo?.members[idxx].type = MemberType.member
            let indexPath1 = IndexPath(row: (idxx + 1), section: 2)
            guard let cell = groupDetailTableView.cellForRow(at: indexPath1) as? GroupMemberCell else{
                return
            }
            groupDetailTableView.beginUpdates()
            groupDetailTableView.endUpdates()
            let data = isSearchingMember ? groupInfo?.searchedMembers : groupInfo?.searchedMembers
            cell.populateData(members: data ?? [], indexPath: indexPath1)
        }
    }
    
    //MARK:- Remove member
    func removeMember(indexVal: Int){
        
        GroupChatFireBaseController.shared.removeMember(roomID: groupInfo?.tagID ?? "",
                                                        userID: groupInfo?.members[indexVal].id ?? "",
                                                        isForRemoveMember: true,
                                                        success: {
        })
        groupInfo?.members.remove(at: indexVal)
        groupDetailTableView.reloadSections([2], with: .none)
    }
    
    func blockedUser(indexVal: Int){
        
        SingleChatFireBaseController.shared.blockedUser(userID: UserProfile.main.userId,
                                                        otherUserID: groupInfo?.members[indexVal].id ?? "")
        groupDetailTableView.beginUpdates()
        groupInfo?.members.remove(at: indexVal)
        groupDetailTableView.deleteRows(at: [IndexPath(row: indexVal, section: 2)], with: .fade)
        groupDetailTableView.endUpdates()
    }
}

extension GroupDetailVC: RemoveFriendPopUpDelegate {
    
    func yesButtonTapped(type : RemoveFriendPopUp.RemoveFriendFor, user : FollowingFollowersModel){
        
        if type == .block{
            view.showIndicator()
            if let idx = groupInfo?.members.firstIndex(where: {$0.id == user.userId}){
                controller.blockedUser(otherUserID: user.userId, index: idx)
            }
        }
    }
}

extension GroupDetailVC: SingleChatFireBaseControllerDelegate{
    func userBlocked(){
        self.view.hideIndicator()
    }
}
