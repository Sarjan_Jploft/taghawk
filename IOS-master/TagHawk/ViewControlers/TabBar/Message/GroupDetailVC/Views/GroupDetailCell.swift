//
//  GroupDetailCell.swift
//  TagHawk
//
//  Created by Appinventiv on 15/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol GroupDetailCellDelegate: class {
    func editBtnTapped(button: UIButton)
    func pinToTopBtnTapped(button: UIButton)
    func muteTapped(button: UIButton)
    func localtionTapped(button: UIButton)
    func editImageBtnTapped(button: UIButton)
    func announcementBtnTapped(button: UIButton)
    func descriptionBtnTapped(button: UIButton)
}

class GroupDetailCell: UITableViewCell {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var announcememntLbl: UILabel!
    @IBOutlet weak var announcementDescription: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var editDescriptionBtn: UIButton!
    @IBOutlet weak var pinTopView: UIView!
    @IBOutlet weak var pinOnTopLbl: UILabel!
    @IBOutlet weak var muteLbl: UILabel!
    @IBOutlet weak var localtionLbl: UILabel!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var groupLocation: UILabel!
    @IBOutlet weak var pinToTopBtn: UIButton!
    @IBOutlet weak var muteBtn: UIButton!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var locationImageWidth: NSLayoutConstraint!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var editImageBtn: UIButton!
    @IBOutlet weak var editTagBtn: UIButton!
    @IBOutlet weak var editAnnouncementBtn: UIButton!
    
    
    weak var delegate: GroupDetailCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        groupImage.contentMode = .scaleAspectFill
        groupImage.round()
        groupName.font = AppFonts.Galano_Semi_Bold.withSize(15)
        groupName.textColor = AppColors.textfield_Title
        announcememntLbl.font = AppFonts.Galano_Semi_Bold.withSize(15)
        announcememntLbl.textColor = AppColors.textfield_Title
        announcememntLbl.text = LocalizedString.announcement.localized
        announcementDescription.font = AppFonts.Galano_Regular.withSize(11.5)
        announcementDescription.textColor = AppColors.blackLblColor
        
        descriptionLbl.font = AppFonts.Galano_Semi_Bold.withSize(15)
        descriptionLbl.textColor = AppColors.textfield_Title
        descriptionLbl.text = LocalizedString.Description.localized
        descriptionText.font = AppFonts.Galano_Regular.withSize(11.5)
        descriptionText.textColor = AppColors.blackLblColor
        
        pinTopView.drawShadow(shadowColor: AppColors.blackLblColor, shadowOpacity: 0.1, shadowPath: nil, shadowRadius: 6.0, cornerRadius: 10.0, offset: CGSize(width: 3, height: 3))
        pinTopView.clipsToBounds = false
        pinOnTopLbl.font = AppFonts.Galano_Regular.withSize(14)
        pinOnTopLbl.textColor = AppColors.blackLblColor
        pinOnTopLbl.text = LocalizedString.pinToTop.localized
        muteLbl.font = AppFonts.Galano_Regular.withSize(14)
        muteLbl.textColor = AppColors.blackLblColor
        muteLbl.text = LocalizedString.Mute.localized
        localtionLbl.font = AppFonts.Galano_Semi_Bold.withSize(13)
        localtionLbl.textColor = AppColors.blackLblColor
        localtionLbl.text = LocalizedString.Location.localized
        groupLocation.font = AppFonts.Galano_Regular.withSize(14)
        groupLocation.textColor = AppColors.blackLblColor
    }

//    MARK:- IBActions
//    ================
    @IBAction func editBtnTapped(_ sender: UIButton) {
        delegate?.editBtnTapped(button: sender)
    }
    
    @IBAction func pinOnTapBtnTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.pinToTopBtnTapped(button: sender)
    }
    
    @IBAction func muteBtnTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.muteTapped(button: sender)
    }
    
    @IBAction func locationBtnTapped(_ sender: UIButton) {
       delegate?.localtionTapped(button: sender)
    }
    @IBAction func editImageBtnTapped(_ sender: UIButton) {
        delegate?.editImageBtnTapped(button: sender)
    }
    @IBAction func announcementBtnTapped(_ sender: UIButton) {
        delegate?.announcementBtnTapped(button: sender)
    }
    
    @IBAction func descriptionBtnTapped(_ sender: UIButton) {
        delegate?.descriptionBtnTapped(button: sender)
    }
}

extension GroupDetailCell {
    
    func populateData(detail: GroupDetail?, roomInfo: ChatListing?, member: GroupMembers?){
        
        guard let data = detail else{
            return
        }
        groupImage.setImage_kf(imageString: data.tagImage, placeHolderImage: AppImages.productPlaceholder.image)
        groupName.text = data.tagName
        announcementDescription.text = data.announcement.isEmpty ? "No Announcement" : data.announcement
        descriptionText.text = data.description.isEmpty ? "No Description" : data.description
        pinToTopBtn.isSelected = roomInfo?.pinned ?? false
        muteBtn.isSelected = roomInfo?.chatMute ?? false
        groupLocation.text = data.tagAddress
        if let mem = member {
           editBtn.isHidden = !(mem.type == .owner)
           
            switch mem.type{
                
            case .admin, .member:
                editImageBtn.isHidden = true
                editTagBtn.isHidden = true
                editAnnouncementBtn.isHidden = true
                editDescriptionBtn.isHidden = true
                locationView.drawShadow(shadowColor: AppColors.blackLblColor, shadowOpacity: 0.1, shadowPath: nil, shadowRadius: 6.0, cornerRadius: 10.0, offset: CGSize(width: 3, height: 3))
                locationView.clipsToBounds = false
                locationImage.isHidden = true
                locationImageWidth.constant = 0
                locationBtn.isUserInteractionEnabled = false
            case .owner:
                editImageBtn.isHidden = false
                editTagBtn.isHidden = false
                editDescriptionBtn.isHidden = false
                editAnnouncementBtn.isHidden = false
                locationImage.isHidden = false
                locationImageWidth.constant = 20
                locationBtn.isUserInteractionEnabled = true
                locationView.round(radius: 10)
                locationView.borderWidth = 0.5
                locationView.borderColor = AppColors.grayBorderColor
            }
        }
    }
}
