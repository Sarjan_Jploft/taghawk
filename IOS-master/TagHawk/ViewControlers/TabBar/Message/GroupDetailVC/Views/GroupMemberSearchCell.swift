//
//  GroupMemberSearchCell.swift
//  TagHawk
//
//  Created by Appinventiv on 12/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class GroupMemberSearchCell: UITableViewCell {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
