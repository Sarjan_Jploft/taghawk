//
//  GroupMemberCell.swift
//  TagHawk
//
//  Created by Appinventiv on 12/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class GroupMemberCell: UITableViewCell {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userTypeView: UIView!
    @IBOutlet weak var userTypeLbl: UILabel!
    @IBOutlet weak var userStatusLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userImage.round()
        userName.font = AppFonts.Galano_Medium.withSize(13)
        userName.textColor = AppColors.blackLblColor
        userTypeView.round(radius: userTypeLbl.frame.height / 2)
        userTypeView.backgroundColor = AppColors.selectedSegmentColor.withAlphaComponent(0.4)
        userTypeLbl.font = AppFonts.Galano_Semi_Bold.withSize(10.5)
        userTypeLbl.textColor = AppColors.selectedSegmentColor
        userStatusLbl.font = AppFonts.Galano_Regular.withSize(11)
        userStatusLbl.textColor = AppColors.lightGreyTextColor
        userStatusLbl.text = LocalizedString.BlockedTapToUnblock.localized
        userStatusLbl.isHidden = true
    }
}

extension GroupMemberCell {
    
    func populateData(members: [GroupMembers],
                      indexPath: IndexPath){
        
        guard !members.isEmpty else{ return }
        let membersData = members[indexPath.row]
        userImage.setImage_kf(imageString: membersData.image, placeHolderImage: AppImages.icChatPlaceholder.image)
        
        let isMyDetail = membersData.id == UserProfile.main.userId
        
        if isMyDetail{
            let strToColor = " (" + LocalizedString.you.localized + ")"
            let totalText = membersData.name + strToColor
            userName.attributedText = totalText.attributeStringWithColors(stringToColor: strToColor,
                                                                          strClr: AppColors.blackLblColor,
                                                                          substrClr: AppColors.lightGreyTextColor,
                                                                          strFont: AppFonts.Galano_Medium.withSize(13),
                                                                          strClrFont: AppFonts.Galano_Medium.withSize(13))
        }else{
            userName.text = membersData.name
        }
        
        userTypeLbl.text = membersData.type.localizedValue
        if membersData.type == .member{
            userTypeLbl.text = membersData.isMute ? LocalizedString.Mute.rawValue :  ""
            userTypeLbl.isHidden = !membersData.isMute
            userTypeView.isHidden = !membersData.isMute
        }else{
          userTypeLbl.isHidden = false
          userTypeView.isHidden = false
        }
        userStatusLbl.isHidden = !membersData.isBlocked
        userTypeLbl.textColor = membersData.isBlocked ? AppColors.lightGreyTextColor : AppColors.selectedSegmentColor
    }
}
