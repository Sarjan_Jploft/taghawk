//
//  PendingRequestCell.swift
//  TagHawk
//
//  Created by Appinventiv on 22/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class PendingRequestCell: UITableViewCell {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var pendingRequestLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        outerView.drawShadow(shadowColor: AppColors.blackLblColor, shadowOpacity: 0.1, shadowPath: nil, shadowRadius: 6.0, cornerRadius: 10.0, offset: CGSize(width: 3, height: 3))
        outerView.clipsToBounds = false
    }
    
    func populateData(groupData: GroupDetail?){
        
        guard let data = groupData else{ return }//case pendingRquest = "pendingRquest"
        //case pendingRquests = "pendingRquests"
        let rquestText = data.pendingRequestCount > 1 ? LocalizedString.pendingRquests.localized : LocalizedString.pendingRquest.localized
        let toalText = rquestText + " " + "(\(data.pendingRequestCount))"
        pendingRequestLbl.attributedText = toalText.attributeStringWithColors(stringToColor : "(\(data.pendingRequestCount))",
                                                                              strClr: AppColors.blackColor,
                                                                              substrClr: AppColors.appBlueColor,
                                                                              strFont: AppFonts.Galano_Regular.withSize(14),
                                                                              strClrFont: AppFonts.Galano_Regular.withSize(14))
    }
}
