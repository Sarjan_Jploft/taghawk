//
//  TagTypeCell.swift
//  TagHawk
//
//  Created by Appinventiv on 12/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol TagTypeCellDelegate: class{
    func switchBtnTapped(type: TagEntrance)
}

class TagTypeCell: UITableViewCell {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var tagTypeLbl: UILabel!
    @IBOutlet weak var publicLbl: UILabel!
    @IBOutlet weak var privateLbl: UILabel!
    @IBOutlet weak var tagBtn: UIButton!
    
    weak var delegate: TagTypeCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        tagTypeLbl.text = LocalizedString.tagType.localized
        tagTypeLbl.font = AppFonts.Galano_Semi_Bold.withSize(13)
        tagTypeLbl.textColor = AppColors.blackLblColor
        
        publicLbl.text = LocalizedString.Public.localized
        publicLbl.font = AppFonts.Galano_Regular.withSize(13)
        privateLbl.text = LocalizedString.Private.localized
        privateLbl.font = AppFonts.Galano_Regular.withSize(13)
    }

    @IBAction func tagBtnTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        let privateLblColor = sender.isSelected ? AppColors.selectedSegmentColor : AppColors.lightGreyTextColor
        let publicLblColor = !sender.isSelected ? AppColors.selectedSegmentColor : AppColors.lightGreyTextColor
        privateLbl.textColor = privateLblColor
        publicLbl.textColor = publicLblColor
        delegate?.switchBtnTapped(type: (sender.isSelected ? TagEntrance.privateType : TagEntrance.publicType))
    }
}


extension TagTypeCell {
    
    func populateData(detail: GroupDetail?, member: GroupMembers?){
        
        guard let data = detail else{
            return
        }
        let privateLblColor = data.tagEntrance == .privateType ? AppColors.selectedSegmentColor : AppColors.lightGreyTextColor
        let publicLblColor = data.tagEntrance == .publicType ? AppColors.selectedSegmentColor : AppColors.lightGreyTextColor
        privateLbl.textColor = privateLblColor
        publicLbl.textColor = publicLblColor
        tagBtn.isSelected = data.tagEntrance == .privateType
        tagBtn.isUserInteractionEnabled = member?.type == .owner
    }
}
