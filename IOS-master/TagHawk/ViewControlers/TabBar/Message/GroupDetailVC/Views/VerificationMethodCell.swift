//
//  VerificationMethodCell.swift
//  TagHawk
//
//  Created by Appinventiv on 12/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol VerificationMethodCelldelegate: class {
    func editBtnTapped(isEmail: Bool)
    func documentBtnTapped()
}

class VerificationMethodCell: UITableViewCell {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var verificationMethodLbl: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var emailTickMarkImage: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var emailIDText: UILabel!
    @IBOutlet weak var passwordTickMarkImage: UIImageView!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var passwordText: UILabel!
    @IBOutlet weak var documentTickMarkImage: UIImageView!
    @IBOutlet weak var emailEditImage: UIImageView!
    
    @IBOutlet weak var documentLbl: UILabel!
    @IBOutlet weak var documentText: UILabel!
    
    @IBOutlet weak var emailOuterView: UIView!
    @IBOutlet weak var emailOtrViewHghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailSepratorView: UIView!
    @IBOutlet weak var emailSepratorViewHghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var pswrdViewHghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var pswrdSepratorView: UIView!
    @IBOutlet weak var pswrdSepratorViewHeight: NSLayoutConstraint!
    @IBOutlet weak var documentView: UIView!
    @IBOutlet weak var documentViewHghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordEditImage: UIImageView!
    
    @IBOutlet weak var emailTickMarkWidth: NSLayoutConstraint!
    @IBOutlet weak var emailTickMarkLeading: NSLayoutConstraint!
    @IBOutlet weak var paswrdTickMarkWidth: NSLayoutConstraint!
    @IBOutlet weak var paswrdTickMarkLeading: NSLayoutConstraint!
    @IBOutlet weak var docTickMarkWidth: NSLayoutConstraint!
    @IBOutlet weak var docTickMarkLeading: NSLayoutConstraint!
    
    
    weak var delegate: VerificationMethodCelldelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        verificationMethodLbl.font = AppFonts.Galano_Semi_Bold.withSize(13)
        verificationMethodLbl.textColor = AppColors.blackLblColor
        verificationMethodLbl.text = LocalizedString.verificationMethod.localized
        outerView.round(radius: 10.0)
        outerView.drawShadow()
        documentText.text = LocalizedString.ownerSetAsDocuemntVerification.localized
        
        for lbl in [emailLbl, passwordLbl, documentLbl]{
            lbl?.font = AppFonts.Galano_Regular.withSize(13)
            lbl?.textColor = AppColors.blackLblColor
        }
        emailLbl.text = LocalizedString.email.localized
        passwordLbl.text = LocalizedString.password.localized
        documentLbl.text = LocalizedString.uploadDocument.localized
        for lbl in [emailIDText, passwordText, documentText]{
            lbl?.font = AppFonts.Galano_Regular.withSize(13)
            lbl?.textColor = AppColors.lightGreyTextColor
        }
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
        outerView.drawShadow(shadowColor: AppColors.blackLblColor,
                             shadowOpacity: 0.1,
                             shadowPath: nil,
                             shadowRadius: 6.0,
                             cornerRadius: 10.0,
                             offset: CGSize(width: 3, height: 3))
        outerView.clipsToBounds = false
    }
    
//    MARK:- IBActions
//    =================
    @IBAction func emailEditBtnTapped(_ sender: UIButton) {
        delegate?.editBtnTapped(isEmail: true)
    }
    
    @IBAction func passwordEditBtnTapped(_ sender: UIButton) {
        delegate?.editBtnTapped(isEmail: false)
    }
    
    @IBAction func documentBtnTapped(_ sender: UIButton) {
        delegate?.documentBtnTapped()
    }
}

extension VerificationMethodCell {
    
    func populateData(detail: GroupDetail?, member: GroupMembers?){
        
        guard let data = detail else{
            return
        }
        guard let mem = member else { return }
        
        switch mem.type {
            
        case .owner:
            emailTickMarkWidth.constant = 26
            paswrdTickMarkWidth.constant = 26
            docTickMarkWidth.constant = 26
            emailTickMarkLeading.constant = 15
            paswrdTickMarkLeading.constant = 15
            docTickMarkLeading.constant = 15
            
            switch data.verificationType {
                
            case .email:
                setVerificatonImage(imageView: emailTickMarkImage)
                emailIDText.text = data.verificationData
                passwordText.isHidden = true
                emailIDText.isHidden = false
                documentText.isHidden = true
                emailEditImage.isHidden = false
                passwordEditImage.isHidden = true
            case .password:
                setVerificatonImage(imageView: passwordTickMarkImage)
                passwordText.text = data.verificationData
                passwordText.isHidden = false
                emailIDText.isHidden = true
                documentText.isHidden = true
                passwordEditImage.isHidden = false
                emailEditImage.isHidden = true
            case .documents:
                setVerificatonImage(imageView: documentTickMarkImage)
                emailIDText.isHidden = true
                passwordText.isHidden = true
                documentText.isHidden = true
                emailEditImage.isHidden = true
                passwordEditImage.isHidden = true
            case .none: return
            }
            
        case .admin:
            emailTickMarkImage.isHidden = true
            passwordTickMarkImage.isHidden = true
            documentTickMarkImage.isHidden = true
            emailTickMarkWidth.constant = 0
            paswrdTickMarkWidth.constant = 0
            docTickMarkWidth.constant = 0
            emailTickMarkLeading.constant = 0
            paswrdTickMarkLeading.constant = 0
            docTickMarkLeading.constant = 0
            
            switch data.verificationType {
                
            case .email:
                emailIDText.text = data.verificationData
                manageVerificationViewHeight(consOutlet: emailOtrViewHghtConstraint, outerView: emailOuterView)

            case .password:
                passwordText.text = data.verificationData
                manageVerificationViewHeight(consOutlet: pswrdViewHghtConstraint, outerView: passwordView)
                
            case .documents:
                manageVerificationViewHeight(consOutlet: documentViewHghtConstraint, outerView: documentView)
            case .none: return
            }
        case .member:
            emailIDText.isHidden = true
            passwordText.isHidden = true
        }
    }
    
    private func manageVerificationViewHeight(consOutlet: NSLayoutConstraint, outerView: UIView){
        for layout in [pswrdViewHghtConstraint, emailOtrViewHghtConstraint, documentViewHghtConstraint, pswrdSepratorViewHeight, emailSepratorViewHghtConstraint]{
            layout?.constant = layout === consOutlet ? 66 : 0
        }
        
        for view in [emailOuterView,documentView, passwordView, pswrdSepratorView, emailSepratorView]{
            view?.isHidden = !(view === outerView)
            view?.clipsToBounds = !(view === outerView)
        }
    }
    
    private func setVerificatonImage(imageView: UIImageView){
        
        [emailTickMarkImage, passwordTickMarkImage, documentTickMarkImage].forEach { (markImageView) in
            
            if markImageView === imageView{
               markImageView?.image = AppImages.tickWithCircle.image
            }else{
               markImageView?.image = AppImages.unTickWithCircle.image
            }
        }
    }
}


