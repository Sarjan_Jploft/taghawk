//
//  GroupDetailController.swift
//  TagHawk
//
//  Created by Appinventiv on 19/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

enum EditTagType {
    
    case tagName
    case tagImage
    case announcement
    case location
    case tagType
    case tagEntrance
    case description
}

protocol GroupDetailControllerDelegate: class {
    func tagEdited(editType: EditTagType, tagData: AddTagResult)
    func error(message: String)
    func tagDeleted(indexPath: IndexPath)
    func exitTag(id: String, pinned: Bool)
    func transferOwnerShip(otherUserID: String, indexVal: Int)
    func removeMember(indexVal: Int)
    func productDelete(id: String, pinned: Bool)
    func blockedUser(indexVal: Int)
}

extension GroupDetailControllerDelegate {
    
    func tagEdited(editType: EditTagType, tagData: AddTagResult){}
    func error(message: String){}
    func tagDeleted(indexPath: IndexPath){}
    func exitTag(id: String){}
    func transferOwnerShip(otherUserID: String, indexVal: Int){}
    func removeMember(indexVal: Int){}
    func productDelete(id: String, pinned: Bool){}
    func blockedUser(indexVal: Int){}
}

class GroupDetailController {
    
    weak var delegate: GroupDetailControllerDelegate?
    
    func changeGroupDetail(tagID: String,
                           text: String,
                           editTagType: EditTagType,
                           tagType: TagEntrance?,
                           privateTagType: PrivateTagType?,
                           latitude: Double?,
                           longitude: Double?,
                           city: String = ""){
        
        var params: JSONDictionary = ["communityId": tagID]
        switch editTagType{
            
        case .tagName:
            params["name"] = text
        case .tagImage:
            var dictObj = JSONDictionary()
            dictObj[ApiKey.url] = text
            dictObj[ApiKey.thumbUrl] = text
            params["imageUrl"] = JSON(dictObj)
        case .announcement:
            params["announcement"] = text
        case .location:
            params["address"] = text
            params["lat"] = latitude ?? 0.0
            params["long"] = longitude ?? 0.0
        case .tagEntrance:
            params["type"] = tagType?.rawValue ?? TagEntrance.publicType.rawValue
            if tagType == .privateType, let tag = privateTagType{
                params["joinTagBy"] = tag.rawValue
                if tag == .email{
                    params["email"] = text
                }else if tag == .password{
                    params["password"] = text
                }
            }
            
        case .tagType:
            print("Need to implement")
        case .description:
            params["description"] = text
        }

        WebServices.editTagData(parameters: params, success: {[weak self](json) in
            let data = AddTagResult(json: json["data"])
            self?.delegate?.tagEdited(editType: editTagType, tagData: data)
        }) {[weak self](error) -> (Void) in
            self?.delegate?.error(message: error.localizedDescription)
        }
    }
    
    func deleteTag(tagID: String, index: IndexPath){
        
        let params: JSONDictionary = ["communityId": tagID]
        
        WebServices.deleteTag(parameters: params, success: {[weak self](json) in
            self?.delegate?.tagDeleted(indexPath: index)
        }) {[weak self](error) -> (Void) in
           self?.delegate?.error(message: error.localizedDescription)
        }
    }
    
    func exitGroup(tagID: String, isPinned: Bool){
           let params: JSONDictionary = ["communityId": tagID]
        WebServices.exitGroup(parameters: params, success: {[weak self](json) in
            self?.delegate?.exitTag(id: tagID, pinned: isPinned)
        }) {[weak self](error) -> (Void) in
            self?.delegate?.error(message: error.localizedDescription)
        }
    }
    
    func transferOwnerShip(tagID: String,
                           userID: String,
                           index: Int){
        let params: JSONDictionary = ["communityId": tagID,
                                      "userId": userID]
        WebServices.transferOwnerShip(parameters: params, success: {[weak self](json) in
            self?.delegate?.transferOwnerShip(otherUserID: userID, indexVal: index)
        }) {[weak self](error) -> (Void) in
          self?.delegate?.error(message: error.localizedDescription)
        }
    }
    
    func removeMember(tagID: String, userID: String, index: Int){
        let params: JSONDictionary = ["communityId": tagID,
                                      "userId": userID]
        
        WebServices.removeMember(parameters: params, success: {[weak self](json) in
            self?.delegate?.removeMember(indexVal: index)
        }) {[weak self](error) -> (Void) in
            self?.delegate?.error(message: error.localizedDescription)
        }
    }
    
    func deleteProduct(_ productId: String, isPinned: Bool) {
        let params: JSONDictionary = [ApiKey.productId: productId]
        
        WebServices.deleteProduct(parameters: params, success: {[weak self](json) in

            self?.delegate?.productDelete(id: productId, pinned: isPinned)
        }) {[weak self](err) -> (Void) in
            self?.delegate?.error(message: err.localizedDescription)
        }
    }
    
    func blockedUser(otherUserID: String, index: Int){
        
        let params : JSONDictionary = [ApiKey.userId : otherUserID, ApiKey.action : "3"]
        
        WebServices.removeFriend(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.delegate?.blockedUser(indexVal: index)
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.error(message: error.localizedDescription)
        }
    }
}
