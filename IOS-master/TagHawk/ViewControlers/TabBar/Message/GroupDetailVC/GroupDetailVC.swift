//
//  GroupDetailVC.swift
//  TagHawk
//
//  Created by Appinventiv on 14/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

enum GroupRemove {
    case exit
    case remove
    case delete
}

class GroupDetailVC : BaseVC {
    
    //    MARK:- IBOutlets
    //    ================
    @IBOutlet weak var groupDetailTableView: UITableView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var alertBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    
    //    MARK:- Proporties
    //    =================
    let controller = GroupDetailController()
    var groupInfo: GroupDetail?
    var roomInfo: ChatListing?
    var currentMemberData: GroupMembers?
    let userInfoDropDown = DropDown()
    var selectedMemberIndex: Int?
    var isSearchingMember: Bool = false
    var groupStatus: GroupRemove = .remove
    private let addTagControler = AddTagController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    override func bindControler() {
        addTagControler.reportTagDelegate = self
    }
    
    deinit {
        GroupChatFireBaseController.shared.removeObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SingleChatFireBaseController.shared.delegate = self
        GroupChatFireBaseController.shared.delegate = self
        if let roomInfo = roomInfo{
            if groupInfo == nil{
                GroupChatFireBaseController.shared.getGroupData(groupID: roomInfo.roomID, isPinned: true)
            }else{
                groupInfo?.members.forEach({[weak self](member) in
                    SingleChatFireBaseController.shared.getUserData(userID: member.id, chatType: .group)
                })
            }
            GroupChatFireBaseController.shared.changesInGroupDetail(roomID: roomInfo.roomID)
            SingleChatFireBaseController.shared.checkUserBlocked()
        }
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        GroupChatFireBaseController.shared.removeObservers()
        if let info = roomInfo{
            SingleChatFireBaseController.shared.removeObservers(roomId: info.roomID, otherUserId: info.otherUserID)
        }
        
    }
    
    //    MARK:- IBActions
    //    ================
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func alertBtnTapped(_ sender: UIButton) {
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = .reportTag
        vc.delegateWithType = self
        vc.modalPresentationStyle = .overCurrentContext
        AppNavigator.shared.parentNavigationControler.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func shareBtnTapped(_ sender: UIButton) {
      
        if let info = groupInfo{
            shareUrl(url: info.sharelink)
        }
    }
    
    @IBAction func deleteBtnTapped(_ sender: UIButton) {
        
        guard let data = currentMemberData else{ return }
        
        if data.type == .owner{
            groupStatus = .delete
            openUpdateEmailVC(instantiate: .deleteGroup)
        }else{
            groupStatus = .exit
            let scene = DeleteChatVC.instantiate(fromAppStoryboard: .Messages)
            scene.modalTransitionStyle = .coverVertical
            scene.modalPresentationStyle = .overCurrentContext
            self.definesPresentationContext = true
            scene.instantiated = .exitGroup
            scene.groupName = groupInfo?.tagName ?? ""
            scene.delegate = self
            AppNavigator.shared.parentNavigationControler.present(scene, animated: true, completion: nil)          
        }
    }
    
    private func initialSetup(){
        
        groupDetailTableView.delegate = self
        groupDetailTableView.dataSource = self
        deleteBtn.titleLabel?.font = AppFonts.Galano_Semi_Bold.withSize(16)
        deleteBtn.setTitleColor(AppColors.whiteColor)
        
        controller.delegate = self
        
        groupDetailTableView.registerHeaderFooter(with: MessageHeaderView.self)
        setupDropdown()
    }
    
    func setupDropdown(){
        
        userInfoDropDown.backgroundColor = AppColors.whiteColor
        userInfoDropDown.cellHeight = 40
        userInfoDropDown.dismissMode = .onTap
        userInfoDropDown.textColor = AppColors.blackLblColor
        userInfoDropDown.textFont = AppFonts.Galano_Medium.withSize(11)
        userInfoDropDown.selectionAction = { [weak self] (index: Int, item: String) in
            
            self?.changeMemberStatus(index: index, item: item)
        }
    }
    
    // Change Status by tap on dropdown
    private func changeMemberStatus(index: Int, item: String){
        
        if let idx = selectedMemberIndex, let info = groupInfo{
            
            if index == 0{
                let userProfileScene = OtherUserProfileVC.instantiate(fromAppStoryboard: .Profile)
                userProfileScene.userId = info.members[idx].id
                navigationController?.pushViewController(userProfileScene, animated: true)
                return
            }
            
            if info.members[idx].isBlocked{
                
                groupInfo?.members[idx].isBlocked = false
                GroupChatFireBaseController.shared.blockUnblockUser(isBlock: false,
                                                roomID: info.tagID,
                                                userID: info.members[idx].id)
                
            }else{
                
                if item == LocalizedString.makeGroupAdmin.localized{
                    groupInfo?.members[idx].type = MemberType.admin
                    GroupChatFireBaseController.shared.changeMemberType(roomID: info.tagID,
                                                    userID: info.members[idx].id,
                                                    memberType: MemberType.admin)
                    
                }else if item == LocalizedString.transferOwnership.localized{
                    controller.transferOwnerShip(tagID: info.tagID,
                                                 userID: info.members[idx].id,
                                                 index: idx)
                }else if item == LocalizedString.dismissAsAdmin.localized{
                    groupInfo?.members[idx].type = MemberType.member
                    GroupChatFireBaseController.shared.changeMemberType(roomID: info.tagID,
                                                    userID: info.members[idx].id,
                                                    memberType: MemberType.member)
                }else{
                    if item.contains(s: LocalizedString.Block.localized){
                        let vc = RemoveFriendPopUp.instantiate(fromAppStoryboard: AppStoryboard.Profile)
                        vc.commingFor = .block
                        let data = FollowingFollowersModel()
                        data.userId = groupInfo?.members[idx].id ?? ""
                        data.profilePicture = groupInfo?.members[idx].image ?? ""
                        data.fullName = groupInfo?.members[idx].name ?? ""
                        vc.user = data
                        vc.delegate = self
                        vc.modalPresentationStyle = .overCurrentContext
                        navigationController?.present(vc, animated: true, completion: nil)
                        
                    }else if item.contains(s: LocalizedString.Mute.localized){
                        groupInfo?.members[idx].isMute = true
                        GroupChatFireBaseController.shared.muteUnmuteMember(isMute: true,
                                                        roomID: info.tagID,
                                                        userID: info.members[idx].id)
                        
                    }else if item.contains(s: LocalizedString.message.localized){
                        if let index = selectedMemberIndex, let info = groupInfo{
                            let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
                            scene.vcInstantiated = .userDetail
                            scene.otherUserData = UserProfile.getUserDataFromGroupMembers(data: info.members[index])
                            AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
                        }                        
                    }else if item.contains(s: LocalizedString.unmute.localized){
                        groupInfo?.members[idx].isMute = false
                        GroupChatFireBaseController.shared.muteUnmuteMember(isMute: false,
                                                                            roomID: info.tagID,
                                                                            userID: info.members[idx].id)
                        
                    }else if item.contains(s: LocalizedString.Remove.localized){
                        controller.removeMember(tagID: info.tagID,
                                                userID: info.members[idx].id,
                                                index: idx)
                    }
                }
            }
            
            let indexPath = IndexPath(row: idx, section: 2)
            guard let cell = groupDetailTableView.cellForRow(at: indexPath) as? GroupMemberCell else{
                return
            }
            let data = isSearchingMember ? groupInfo?.searchedMembers : groupInfo?.searchedMembers
            cell.populateData(members: data ?? [], indexPath: indexPath)
        }
    }
}


extension GroupDetailVC : CustomPopUpDelegateWithType{
    
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        if type != .reportTag { return }
        self.addTagControler.reportTag(tagId: self.groupInfo?.tagID ?? "")
    }
}


extension GroupDetailVC : ReportATagDelegate {
    
    func willReportTag() {
        self.view.showIndicator()
        
    }
    
    func tagReportedSuccessFully() {
        self.view.hideIndicator()
        GroupChatFireBaseController.shared.removeMember(roomID: self.groupInfo?.tagID ?? "",
                                    userID: UserProfile.main.userId,
            isForRemoveMember: false,
            success: {[weak self] in
            self?.navigationController?.popToRootViewController(animated: true)
        })
    }
    
    func failedToReportTag(msg: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
    
}
