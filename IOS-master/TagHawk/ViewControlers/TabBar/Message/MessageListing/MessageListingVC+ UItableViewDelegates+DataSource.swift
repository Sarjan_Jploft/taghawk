//
//  MessageVC+ UItableViewDelegates+DataSource.swift
//  TagHawk
//
//  Created by Appinventiv on 13/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import UIKit

//MARK:- Tableview datasource
extension MessageListingVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0: return controller.roomList.pinnedData.count
            
        case 1: return controller.roomList.normalChat.count
            
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: MessageCell.self)
        
        var chatData : [ChatListing] = []
        switch indexPath.section{
            
        case 0:
            chatData = controller.roomList.pinnedData
        default:
            chatData = controller.roomList.normalChat
        }
        let message = chatData[indexPath.row]
        
        switch message.chatType {
            
        case .single:
            if !message.lastMessage.senderID.isEmpty{
                SingleChatFireBaseController.shared.getUserInfo(userID: message.lastMessage.senderID, indexPath: indexPath) {[weak self](indexPath, userData) in
                    
                    guard let strongSelf = self else{ return }
                    guard let messageCell = strongSelf.messageListTableView.cellForRow(at: indexPath) as? MessageCell else{ return }
                    
                    let text = userData.userId == UserProfile.main.userId ? LocalizedString.you.localized : userData.fullName
                    
                    switch indexPath.section{
                        
                    case 0:
                        strongSelf.controller.roomList.pinnedData[indexPath.row].userName = text
                    
                    default:
                        strongSelf.controller.roomList.normalChat[indexPath.row].userName = text
                    }
                    
                    strongSelf.view.hideIndicator()
                    strongSelf.populateData(indexPath: indexPath,
                                            data: strongSelf.controller.roomList,
                                            cell: messageCell)
                }
            }
            
            if !message.otherUserID.isEmpty{
                SingleChatFireBaseController.shared.getUserInfo(userID: message.otherUserID, indexPath: indexPath) {[weak self](indexPath, userData) in
                    
                    guard let strongSelf = self else{ return }
                    guard let messageCell = strongSelf.messageListTableView.cellForRow(at: indexPath) as? MessageCell else{ return }
                    
                    switch indexPath.section{
                        
                    case 0:
                        if strongSelf.controller.roomList.pinnedData[indexPath.row].chatType == .single {
                            strongSelf.controller.roomList.pinnedData[indexPath.row].roomName = userData.fullName
                            strongSelf.controller.roomList.pinnedData[indexPath.row].roomImage = userData.profilePicture
                        }
                        
                    default:
                        if strongSelf.controller.roomList.normalChat[indexPath.row].chatType == .single {
                            strongSelf.controller.roomList.normalChat[indexPath.row].roomName = userData.fullName
                            strongSelf.controller.roomList.normalChat[indexPath.row].roomImage = userData.profilePicture
                        }
                    }
                    
                    strongSelf.view.hideIndicator()
                    strongSelf.populateData(indexPath: indexPath,
                                            data: strongSelf.controller.roomList,
                                            cell: messageCell)
                }
            }
            
            if !message.roomID.isEmpty{
               
                controller.getUpdatedProductInfo(roomID: message.roomID,
                                                 indexPath: indexPath) {[weak self](productInfo, index, roomID) in
                                                    
                                                    guard let strongSelf = self else{ return }
                                                    guard let messageCell = strongSelf.messageListTableView.cellForRow(at: indexPath) as? MessageCell else{ return }
                                                    
                                                    switch index.section {
                                                        
                                                    case 0:
                                                        
                                                        let roomInfo = strongSelf.controller.roomList.pinnedData[index.row]
                                                        
                                                        if roomInfo.chatType == .single,
                                                            roomInfo.roomID == roomID {
                                                            strongSelf.controller.roomList.pinnedData[index.row].productData = productInfo
                                                        }
                                                    default:
                                                        let roomInfo = strongSelf.controller.roomList.normalChat[index.row]
                                                        
                                                        if roomInfo.chatType == .single,
                                                            roomInfo.roomID == roomID {
                                                            strongSelf.controller.roomList.normalChat[index.row].productData = productInfo
                                                        }
                                                    }
                                                    
                                                    strongSelf.populateData(indexPath: indexPath,
                                                                            data: strongSelf.controller.roomList,
                                                                            cell: messageCell)
                }
            }
            
        case .group:
            
            switch message.lastMessage.messageType{
                case .ownershipTransfer, .tagCreatedHeader, .userJoin, .userLeft, .userRemove:
                    
                    SingleChatFireBaseController.shared.getUserInfo(userID: message.lastMessage.messageText, indexPath: indexPath) {[weak self](indexPath, userData) in
                        
                        guard let strongSelf = self else{ return }
                        guard let messageCell = strongSelf.messageListTableView.cellForRow(at: indexPath) as? MessageCell else{ return }
                        
                        let text = userData.userId == UserProfile.main.userId ? LocalizedString.you.localized : userData.fullName
                        
                        switch indexPath.section{
                            
                        case 0:
                            if strongSelf.controller.roomList.pinnedData[indexPath.row].chatType == .group {
                                strongSelf.controller.roomList.pinnedData[indexPath.row].userName = text
                            }
                        default:
                            if strongSelf.controller.roomList.normalChat[indexPath.row].chatType == .group {
                                strongSelf.controller.roomList.normalChat[indexPath.row].userName = text
                            }
                        }
                        
                        strongSelf.view.hideIndicator()
                        strongSelf.populateData(indexPath: indexPath,
                                                data: strongSelf.controller.roomList,
                                                cell: messageCell)
                }
            default:
                break
            }
        }
        
        if controller.isListHavGroupData{
            if !controller.isHeaderWithuserID{
                view.hideIndicator()
                populateData(indexPath: indexPath, data: controller.roomList, cell: cell)
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = tableView.dequeueHeaderFooter(with: MessageHeaderView.self)
        view.headerViewLbl.text = section == 0 ? LocalizedString.pinnedChats.localized : LocalizedString.openChats.localized
        return view
    }
    
    //MARK:- Populate data
   private func populateData(indexPath: IndexPath,
                      data: (pinnedData: [ChatListing], normalChat: [ChatListing]),
                      cell: MessageCell){
        
        var chatData : [ChatListing] = []
        switch indexPath.section{
            
        case 0:
            chatData = data.pinnedData
            
        case 1:
            chatData = data.normalChat
            
        default: return
        }
        
        cell.sepratorView.isHidden = (chatData.count - 1) == indexPath.row ? true : false
        cell.userImage.setImage_kf(imageString: chatData[indexPath.row].roomImage, placeHolderImage: AppImages.icChatPlaceholder.image)
        cell.userName.text = chatData[indexPath.row].roomName.capitalizingFirstLetter()
        cell.productImage.isHidden = chatData[indexPath.row].chatType == .group
        cell.productImage.isHidden = chatData[indexPath.row].productData?.productImage.isEmpty ?? true
        cell.productImage.setImage_kf(imageString: chatData[indexPath.row].productData?.productImage ?? "",
                                 placeHolderImage: AppImages.icProductPlaceholder.image)
        
        var titleText = ""
        
        if chatData[indexPath.row].unreadMessageCount > 0{
            cell.onlineUserView.isHidden = !chatData[indexPath.row].chatMute
            
            cell.newMessageCount.text = chatData[indexPath.row].unreadMessageCount > 99 ? "99+" : "\(chatData[indexPath.row].unreadMessageCount)"
            cell.newMessageCount.isHidden = chatData[indexPath.row].chatMute
            if chatData[indexPath.row].chatType == .group, chatData[indexPath.row].chatMute{
                titleText = "(" + "\(chatData[indexPath.row].unreadMessageCount) " + "News" + ") "
            }
        }else{
            cell.onlineUserView.isHidden = true
            cell.newMessageCount.isHidden = true
        }
        
        if chatData[indexPath.row].lastMessage.messageType != .image && chatData[indexPath.row].lastMessage.messageType != .text{
            titleText = titleText + (chatData[indexPath.row].lastMessage.senderID == UserProfile.main.userId ? LocalizedString.you.localized : chatData[indexPath.row].lastMessage.messageText)
        }
        
        switch chatData[indexPath.row].lastMessage.messageType {
            
        case .image:
            let totalText = titleText + LocalizedString.sentImage.localized
            cell.messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
        case .text:
            let totalText = titleText + chatData[indexPath.row].lastMessage.messageText
            cell.messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
        case .tagCreatedHeader:
            let totalText = chatData[indexPath.row].userName + LocalizedString.createdThisTag.localized
            cell.messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .userLeft:
            let totalText = chatData[indexPath.row].userName + LocalizedString.leftFromTag.localized
            cell.messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .userRemove:
            let totalText = chatData[indexPath.row].userName + LocalizedString.removedFromTag.localized
            cell.messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .userJoin:
            let totalText = chatData[indexPath.row].userName + LocalizedString.joinedTheTag.localized
            cell.messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .ownershipTransfer:
            let totalText = LocalizedString.OwnershipTransferTo.localized + chatData[indexPath.row].userName
            cell.messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .shareProduct, .shareCommunity:
            cell.messageText.text = chatData[indexPath.row].lastMessage.messageType == .shareProduct ? "Shared a product" : "Shared a Tag"
            
        default: cell.messageText.text = ""
            
        }
        
        cell.chatMuteImage.isHidden = !chatData[indexPath.row].chatMute
        let date = Date(timeIntervalSince1970: chatData[indexPath.row].lastMessage.timeInterval)
        cell.messageTime.text = date.timeAgoSince
    }
}

//MARK:- tableview delegates
extension MessageListingVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch section {
            
        case 0: return controller.roomList.pinnedData.isEmpty ? CGFloat.leastNormalMagnitude : 40
            
        case 1: return controller.roomList.normalChat.isEmpty ? CGFloat.leastNormalMagnitude : 40
            
        default: return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
        
        let deleteAction = UIContextualAction(style: .normal, title: nil) {[weak self](ac: UIContextualAction, view: UIView, success: (Bool) -> Void)  in
            self?.intantiateDeleteChat(indexPath: indexPath)
            success(true)
        }
        
        deleteAction.image = AppImages.icChatTrash.image
        deleteAction.backgroundColor = AppColors.deleteBackgroundColor
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let roomData =  indexPath.section == 0 ? controller.roomList.pinnedData : controller.roomList.normalChat
        
        guard !roomData.isEmpty else{
            return
        }
        
        switch roomData[indexPath.row].chatType {
            
        case .single:
            
            let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
            scene.vcInstantiated = .messageList
            scene.roomInfo = roomData[indexPath.row]
            
            if let idx = SingleChatFireBaseController.shared.checkRoomExistIndex(roomId: roomData[indexPath.row].roomID,
                                                                  userId: UserProfile.main.userId){
               SingleChatFireBaseController.shared.usersRoom[idx].isExist = true
            }else{
                let data: (roomID: String, userID: String, isExist: Bool) = (roomID: roomData[indexPath.row].roomID, userID: UserProfile.main.userId, isExist: true)
              SingleChatFireBaseController.shared.usersRoom.append(data)
            }
            
            SingleChatFireBaseController.shared.checkAnotherUserRemoveRoom(roomId: roomData[indexPath.row].roomID, otherUserID: roomData[indexPath.row].otherUserID)
            AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
            
        case .group:
            let scene = GroupChatVC.instantiate(fromAppStoryboard: .Messages)
            scene.roomData = roomData[indexPath.row]
            AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
        }
    }
    
    private func intantiateDeleteChat(indexPath: IndexPath){
        
        var data: ChatListing!
        if indexPath.section == 0{
            data = controller.roomList.pinnedData[indexPath.row]
        }else{
            data = controller.roomList.normalChat[indexPath.row]
        }
        if data.chatType == .single {
            deleteChat(roomName: data.roomName, index: indexPath, vcinstantiated: .deleteChat)
        }else if data.chatType == .group && data.otherUserID != UserProfile.main.userId{
            deleteChat(roomName: data.roomName, index: indexPath, vcinstantiated: .exitGroup)
        }else{
            deleteGroup(groupName: data.roomName, index: indexPath)
        }
    }
    
    private func deleteChat(roomName: String, index: IndexPath, vcinstantiated: DeleteChatVCInstantiated){
        let scene = DeleteChatVC.instantiate(fromAppStoryboard: .Messages)
        scene.modalTransitionStyle = .coverVertical
        scene.modalPresentationStyle = .overCurrentContext
        scene.selectedIndexPath = index
        scene.roomName = roomName
        scene.groupName = roomName
        scene.instantiated = vcinstantiated
        self.definesPresentationContext = true
        scene.delegate = self
        AppNavigator.shared.parentNavigationControler.present(scene, animated: true, completion: nil)
    }
    
    private func deleteGroup(groupName: String, index: IndexPath){
        
        let scene = UpdateEmailVC.instantiate(fromAppStoryboard: .Messages)
        scene.delegate = self
        scene.groupName = groupName
        scene.modalTransitionStyle = .coverVertical
        scene.modalPresentationStyle = .overCurrentContext
        scene.instantitaed = .deleteGroup
        scene.indexPath = index
        self.definesPresentationContext = true
        AppNavigator.shared.parentNavigationControler.present(scene, animated: true, completion: nil)
    }
}

extension MessageListingVC : UpdateEmailVCDeleagte{
    
    func updates(text: String, type: UpdateEmailVCInstantiated, indexPath: IndexPath?){
        
        if type == .deleteGroup,
            let index = indexPath {
            var data: ChatListing!
            if index.section == 0{
                data = controller.roomList.pinnedData[index.row]
            }else{
                data = controller.roomList.normalChat[index.row]
            }
            listController.deleteTag(tagID: data.roomID, index: index)
        }
    }
}

extension MessageListingVC: DeleteChatDelegate {
    
    //MARK:- Delete button tapped
    func deleteBtnTapped(index: IndexPath?) {

        if let selectedIndex = index {
            let chatData: ChatListing?
            
            switch selectedIndex.section{
            case 0:
                messageListTableView.beginUpdates()
                chatData = controller.roomList.pinnedData[selectedIndex.row]
                controller.roomList.pinnedData.remove(at: selectedIndex.row)
            case 1:
                messageListTableView.beginUpdates()
                chatData = controller.roomList.normalChat[selectedIndex.row]
                controller.roomList.normalChat.remove(at: selectedIndex.row)
            default: return
            }
            
            if let data = chatData {
                messageListTableView.deleteRows(at: [selectedIndex], with: .automatic)
                messageListTableView.endUpdates()
                SingleChatFireBaseController.shared.deleteChat(roomID: data.roomID,
                                                otherUserID: data.otherUserID)
                reloadTableView()
            }
        }
    }
    
    //MARK:- Exit button tapped
    func exitBtnTapped(index: IndexPath?){
        
        if let selectedIndex = index {
            if selectedIndex.section == 0{
                let data = controller.roomList.pinnedData[selectedIndex.row]
                listController.exitGroup(tagID: data.roomID, isPinned: true)
            }else{
                let data = controller.roomList.normalChat[selectedIndex.row]
                listController.exitGroup(tagID: data.roomID, isPinned: false)
            }
        }
    }
}

extension MessageListingVC: FireBaseChatDelegate {
    
    //MARK:- Get message listing
    func getMessageListing(){
        
        if controller.roomList.pinnedData.isEmpty && controller.roomList.normalChat.isEmpty{
            self.view.hideIndicator()
            reloadTableView()
        }
        
        if !controller.isListHavGroupData{
            self.view.hideIndicator()
            reloadTableView()
            messageListTableView.reloadData()
        }
    }
    
    //MARK:-
    func reloadTableView(){
        
        self.shouldDisplayEmptyDataView = controller.roomList.pinnedData.isEmpty && controller.roomList.normalChat.isEmpty
        self.messageListTableView.reloadEmptyDataSet()
    }
    
    func getNewMessage(isPinned: Bool){
        
        if isPinned{
            if controller.roomList.pinnedData.isEmpty{
                messageListTableView.reloadData()
            }else{
                messageListTableView.beginUpdates()
                messageListTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                messageListTableView.endUpdates()
            }
        }else{
            messageListTableView.reloadData()
        }
    }
    
    //MARK:- Update room data
    func getUpdatedRoomData(indexpath: IndexPath){
        
        if indexpath.row == 0{
            messageListTableView.reloadRows(at: [indexpath], with: .fade)
        }else{
            messageListTableView.beginUpdates()
            messageListTableView.deleteRows(at: [IndexPath(row: indexpath.row, section: indexpath.section)], with: .fade)
            messageListTableView.insertRows(at: [IndexPath(row: 0, section: indexpath.section)], with: .fade)
            messageListTableView.endUpdates()
        }
    }
}

extension MessageListingVC: GroupChatFireBaseControllerDelegate {
    
    //MARK:- get group details
    func getGroupDetail(detail: GroupDetail, pinned: Bool){
        
        self.shouldDisplayEmptyDataView = controller.roomList.pinnedData.isEmpty && controller.roomList.normalChat.isEmpty
        self.messageListTableView.reloadEmptyDataSet()
        
        if pinned{
            if let idx = controller.roomList.pinnedData.index(where: {$0.roomID == detail.tagID}){
                controller.roomList.pinnedData[idx].roomImage = detail.tagImage
                controller.roomList.pinnedData[idx].roomName = detail.tagName
                controller.roomList.pinnedData[idx].chatType = ChatType.group
                controller.roomList.pinnedData[idx].lastMessage = detail.lastMessage
                controller.roomList.pinnedData[idx].groupMembers = detail.members
                controller.roomList.pinnedData = controller.roomList.pinnedData.sorted(by: {$0.lastMessage.timeInterval > $1.lastMessage.timeInterval})
            }
        }else{
            if let idx = controller.roomList.normalChat.index(where: {$0.roomID == detail.tagID}){
                controller.roomList.normalChat[idx].roomImage = detail.tagImage
                controller.roomList.normalChat[idx].roomName = detail.tagName
                controller.roomList.normalChat[idx].chatType = ChatType.group
                controller.roomList.normalChat[idx].lastMessage = detail.lastMessage
                controller.roomList.normalChat[idx].groupMembers = detail.members
                controller.roomList.normalChat = controller.roomList.normalChat.sorted(by: {$0.lastMessage.timeInterval > $1.lastMessage.timeInterval})
            }
        }
        
        CommonFunctions.delay(delay: 0.3) {[weak self] in
            
            guard let strongSelf = self else{ return }
            strongSelf.view.hideIndicator()
            strongSelf.messageListTableView.reloadData()
        }
    }
    
    //MARK:- Change in room data
    func changedInRoomData(detail: GroupDetail){
        var isDataPinned: Bool = false
        if let idx = controller.roomList.pinnedData.index(where: {$0.roomID == detail.tagID}){
            isDataPinned = true
            let data = controller.roomList.pinnedData[idx]
            data.roomImage = detail.tagImage
            data.roomName = detail.tagName
            data.groupMembers = detail.members
            
            if data.lastMessage.messageID != detail.lastMessage.messageID{
                data.lastMessage = MessageDetail.mergeTwoMessages(oldmessage: data.lastMessage,
                                                                  newMessage: detail.lastMessage)
            }
            
            if data.lastMessage.messageID != detail.lastMessage.messageID && idx != 0{
                controller.roomList.pinnedData.remove(at: idx)
                controller.roomList.pinnedData.insert(data, at: 0)
                messageListTableView.beginUpdates()
                messageListTableView.deleteRows(at: [IndexPath(row: idx, section: 0)], with: .fade)
                messageListTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                messageListTableView.endUpdates()
            }else{
                controller.roomList.pinnedData[idx] = data
                messageListTableView.reloadRows(at: [IndexPath(row: idx, section: 0)], with: .fade)
            }
            
            guard let vc = self.parent as? MessageVC else{ return }
            guard let searchVC = vc.searchVC else{ return }
            if let index = searchVC.chats.index(where: {$0.roomID == detail.tagID}){
                searchVC.chats[index].groupMembers = detail.members
            }
            if let index = searchVC.searchedData.index(where: {$0.roomID == detail.tagID}){
                searchVC.searchedData[index].groupMembers = detail.members
            }
        }
        
        if !isDataPinned{
            if let idx = controller.roomList.normalChat.index(where: {$0.roomID == detail.tagID}){
                let data = controller.roomList.normalChat[idx]
                data.roomImage = detail.tagImage
                data.roomName = detail.tagName
                data.groupMembers = detail.members
                
                if data.lastMessage.messageID != detail.lastMessage.messageID{
                    data.lastMessage = MessageDetail.mergeTwoMessages(oldmessage: data.lastMessage,
                                                                      newMessage: detail.lastMessage)
                }
                if data.lastMessage.messageID != detail.lastMessage.messageID && idx != 0{
                    
                    controller.roomList.normalChat.remove(at: idx)
                    controller.roomList.normalChat.insert(data, at: 0)
                    messageListTableView.beginUpdates()
                    messageListTableView.deleteRows(at: [IndexPath(row: idx, section: 1)], with: .fade)
                    messageListTableView.insertRows(at: [IndexPath(row: 0, section: 1)], with: .fade)
                    messageListTableView.endUpdates()
                }else{
                    controller.roomList.normalChat[idx] = data
                    messageListTableView.reloadData()
                }
            }
        }
    }
    
    //MARK:- Exit from group
    func exitFromGroup(id: String, pinned: Bool){
        messageListTableView.beginUpdates()
        if pinned{
            if let idx = controller.roomList.pinnedData.index(where: {$0.roomID == id}){
                controller.roomList.pinnedData.remove(at: idx)
                messageListTableView.deleteRows(at: [IndexPath(row: idx, section: 0)], with: .automatic)
            }
        }else{
            if let idx = controller.roomList.normalChat.index(where: {$0.roomID == id}){
                controller.roomList.normalChat.remove(at: idx)
                messageListTableView.deleteRows(at: [IndexPath(row: idx, section: 1)], with: .automatic)
            }
        }
        messageListTableView.endUpdates()
        reloadTableView()
    }
}

extension MessageListingVC: GroupDetailControllerDelegate {
    
    func exitTag(id: String, pinned: Bool){
        GroupChatFireBaseController.shared.exitFromGroup(roomID: id, isPinned: pinned)
    }
    
    func tagDeleted(indexPath: IndexPath){
        
        messageListTableView.beginUpdates()
        
        let data: ChatListing!
        if indexPath.section == 0{
            data = controller.roomList.pinnedData[indexPath.row]
            controller.roomList.pinnedData.remove(at: indexPath.row)
        }else{
            data = controller.roomList.normalChat[indexPath.row]
            controller.roomList.normalChat.remove(at: indexPath.row)
        }
        messageListTableView.deleteRows(at: [indexPath], with: .automatic)
        GroupChatFireBaseController.shared.deleteGroup(roomID: data.roomID, members: data.groupMembers)
        
        guard let homeVc = AppNavigator.shared.tabBar?.viewControllers?.first?.children.first as? HomeVC else { return }
        homeVc.homeTagsVc.removeparticulatTagFromMap(tagId: data.roomID)
        
        messageListTableView.endUpdates()
        reloadTableView()
    }
}

extension MessageListingVC: SingleChatFireBaseControllerDelegate {

    //MARK:- User block
    func userBlockedByAnother(roomData: ChatListing) {
        var isPinnedGetData: Bool = false
        
        if let idx = controller.roomList.pinnedData.firstIndex(where: {$0.roomID == roomData.roomID}){
            isPinnedGetData = true
            messageListTableView.beginUpdates()
            controller.roomList.pinnedData.remove(at: idx)
            messageListTableView.deleteRows(at: [IndexPath.init(row: idx, section: 0)], with: .automatic)
            messageListTableView.endUpdates()
        }
        
        if !isPinnedGetData{
            if let idx = controller.roomList.normalChat.firstIndex(where: {$0.roomID == roomData.roomID}){
                messageListTableView.beginUpdates()
                controller.roomList.normalChat.remove(at: idx)
                messageListTableView.deleteRows(at: [IndexPath(row: idx, section: 1)], with: .automatic)
                messageListTableView.endUpdates()
            }
        }
        reloadTableView()
    }
}
