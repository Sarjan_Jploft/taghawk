//
//  MessageCell.swift
//  TagHawk
//
//  Created by Appinventiv on 13/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var onlineUserView: UIView!
    @IBOutlet weak var newMessageCount: UILabel!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var chatMuteImage: UIImageView!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var sepratorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImage.contentMode = .scaleAspectFill
        userImage.round()
        onlineUserView.cornerRadius = onlineUserView.frame.height / 2
        onlineUserView.borderWidth = 1.0
        onlineUserView.borderColor = AppColors.blueBorder
        onlineUserView.backgroundColor = AppColors.appBlueColor
        userName.font = AppFonts.Galano_Bold.withSize(13)
        userName.textColor = AppColors.blackLblColor
        chatMuteImage.cornerRadius = chatMuteImage.frame.height / 2
        chatMuteImage.borderWidth = 0.5
        chatMuteImage.borderColor = AppColors.grayBorderColor
        chatMuteImage.image = AppImages.icMutedChat.image
        newMessageCount.font = AppFonts.Galano_Semi_Bold.withSize(11)
        newMessageCount.textColor = AppColors.selectedSegmentColor
        newMessageCount.cornerRadius = newMessageCount.frame.height / 2
        newMessageCount.borderWidth = 1.5
        newMessageCount.borderColor = AppColors.selectedSegmentColor
        messageText.font = AppFonts.Galano_Regular.withSize(11.5)
        messageText.textColor = AppColors.lightGreyTextColor
        messageTime.font = AppFonts.Galano_Regular.withSize(11)
        messageTime.textColor = AppColors.blackLblColor
        sepratorView.backgroundColor = AppColors.blackColor.withAlphaComponent(0.8)
        productImage.cornerRadius = 10
        productImage.borderWidth = 1.0
        productImage.borderColor = AppColors.grayBorderColor
    }
    
    func populateData(indexPath: IndexPath,
                      data: (pinnedData: [ChatListing], normalChat: [ChatListing])){
        
        var chatData : [ChatListing] = []
        switch indexPath.section{
            
        case 0:
            chatData = data.pinnedData
            
        case 1:
            chatData = data.normalChat
            
        default: return
        }
        
        sepratorView.isHidden = (chatData.count - 1) == indexPath.row ? true : false
        userImage.setImage_kf(imageString: chatData[indexPath.row].roomImage, placeHolderImage: AppImages.icChatPlaceholder.image)
        userName.text = chatData[indexPath.row].roomName.capitalizingFirstLetter()
        productImage.isHidden = chatData[indexPath.row].chatType == .group
        productImage.isHidden = chatData[indexPath.row].productData?.productImage.isEmpty ?? true
        productImage.setImage_kf(imageString: chatData[indexPath.row].productData?.productImage ?? "",
                                 placeHolderImage: AppImages.icProductPlaceholder.image)
        
        var titleText = ""
        
        if chatData[indexPath.row].unreadMessageCount > 0{
            onlineUserView.isHidden = !chatData[indexPath.row].chatMute
            
            newMessageCount.text = chatData[indexPath.row].unreadMessageCount > 99 ? "99+" : "\(chatData[indexPath.row].unreadMessageCount)"
            newMessageCount.isHidden = chatData[indexPath.row].chatMute
            if chatData[indexPath.row].chatType == .group, chatData[indexPath.row].chatMute{
                titleText = "(" + "\(chatData[indexPath.row].unreadMessageCount) " + "News" + ") "
            }
        }else{
            onlineUserView.isHidden = true
            newMessageCount.isHidden = true
        }
        
        if chatData[indexPath.row].lastMessage.messageType != .image && chatData[indexPath.row].lastMessage.messageType != .text{
            titleText = titleText + (chatData[indexPath.row].lastMessage.senderID == UserProfile.main.userId ? LocalizedString.you.localized : chatData[indexPath.row].lastMessage.messageText)
        }

        switch chatData[indexPath.row].lastMessage.messageType {
            
        case .image:
            let totalText = titleText + LocalizedString.sentImage.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
        case .text:
            let totalText = titleText + chatData[indexPath.row].lastMessage.messageText
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: titleText,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
        case .tagCreatedHeader:
            let totalText = chatData[indexPath.row].userName + LocalizedString.createdThisTag.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .userLeft:
            let totalText = chatData[indexPath.row].userName + LocalizedString.leftFromTag.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .userRemove:
            let totalText = chatData[indexPath.row].userName + LocalizedString.removedFromTag.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .userJoin:
            let totalText = chatData[indexPath.row].userName + LocalizedString.joinedTheTag.localized
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .ownershipTransfer:
            let totalText = LocalizedString.OwnershipTransferTo.localized + chatData[indexPath.row].userName
            messageText.attributedText = totalText.attributeStringWithColors(stringToColor: chatData[indexPath.row].userName,
                                                                             strClr: AppColors.lightGreyTextColor, substrClr: AppColors.selectedSegmentColor)
            
        case .shareProduct, .shareCommunity:
            messageText.text = chatData[indexPath.row].lastMessage.messageType == .shareProduct ? "Shared a product" : "Shared a Tag"
            
        default: messageText.text = ""
        }

        chatMuteImage.isHidden = !chatData[indexPath.row].chatMute
        let date = Date(timeIntervalSince1970: chatData[indexPath.row].lastMessage.timeInterval)
        messageTime.text = date.timeAgoSince
    }
}
