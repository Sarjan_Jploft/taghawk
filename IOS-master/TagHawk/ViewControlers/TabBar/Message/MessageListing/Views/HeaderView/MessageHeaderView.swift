//
//  MessageHeaderView.swift
//  TagHawk
//
//  Created by Appinventiv on 13/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class MessageHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var headerViewLbl: UILabel!
    @IBOutlet weak var headerLeadingConstraint: NSLayoutConstraint!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        headerViewLbl.font = AppFonts.Galano_Bold.withSize(14)
        headerViewLbl.textColor = AppColors.blackLblColor
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
