//
//  NewFileVC.swift
//  TagHawk
//
//  Created by Appinventiv on 28/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class MessageListingVC : BaseVC {
    
    //    MARK:- IBOutlet
    //    ===============
    @IBOutlet weak var messageListTableView: UITableView!
    
    //    MARK:- Proporties
    //    =================
    let controller = FireBaseChat()
    let listController = GroupDetailController()
    
    //    MARK:- ViewController Life Cycle
    //    ================================
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GroupChatFireBaseController.shared.delegate = self
        SingleChatFireBaseController.shared.delegate = self
        controller.roomList = ([], [])
        messageListTableView.reloadData()
        SingleChatFireBaseController.shared.checkUserBlocked()
        self.view.hideIndicator()
        self.view.showIndicator()
        controller.getRoomsData(controller: GroupChatFireBaseController.shared,
                                singleController: SingleChatFireBaseController.shared)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        controller.removeObservers()
        SingleChatFireBaseController.shared.removeObservers(roomId: "", otherUserId: "")
        GroupChatFireBaseController.shared.removeObservers()
    }
}

extension MessageListingVC {
   
    //MARK:- init set up
    private func initialSetup(){
        
        listController.delegate = self
        messageListTableView.dataSource = self
        messageListTableView.delegate = self
        controller.delegate = self
        
        self.setEmptyDataView(heading: LocalizedString.oopsItsEmpty.localized,
                              description: LocalizedString.noMessages.localized,
                              image: AppImages.icEmptyMessages.image)
        
        self.setDataSourceAndDelegate(forScrollView: self.messageListTableView)
        registerNibs()
    }
    
    private func registerNibs(){
        messageListTableView.registerHeaderFooter(with: MessageHeaderView.self)
    }
}
