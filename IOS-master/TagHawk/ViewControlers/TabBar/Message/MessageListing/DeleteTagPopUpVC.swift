//
//  DeleteTagPopUpVC.swift
//  TagHawk
//
//  Created by Admin on 6/28/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit


class DeleteTagPopUpVC : BaseVC {
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var captaLabel: UILabel!
    @IBOutlet weak var captaTextField: UITextField!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var popUpView: UIView!

    var instantiated: DeleteChatVCInstantiated = .deleteChat
    weak var delegate: DeleteChatDelegate?
    var selectedIndexPath: IndexPath?
    var groupName: String = ""
    var randomString : String = CommonFunctions.randomString(length: 10)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        dismiss(animated: true) {[weak self] in
            self?.delegate?.deleteBtnTapped(index: self?.selectedIndexPath)
        }
    }
}


extension DeleteTagPopUpVC{
    
    func setUpSubView(){
        
        self.cancelButton.round(radius: 10)
        self.deleteButton.round(radius: 10)
        self.captaLabel.round(radius: 10)
        cancelButton.setBorder(width: 1, color: AppColors.textfield_Border)
        self.headingLabel.setAttributes(text: LocalizedString.Delete_Tag.localized,font: AppFonts.Galano_Semi_Bold.withSize(17.5), textColor: UIColor.black)
        self.cancelButton.setAttributes(title: LocalizedString.cancel.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.black, backgroundColor: UIColor.white)
        self.deleteButton.setAttributes(title: LocalizedString.Delete.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
        self.descriptionLabel.text = LocalizedString.Sure_To_Delete_tag.localized
        self.captaLabel.setAttributes(text: "  \(randomString)  ", font: AppFonts.Galano_Regular.withSize(15), textColor: AppColors.appBlueColor, backgroundColor: AppColors.appBlueColor.withAlphaComponent(0.5))
        self.captaTextField.setBorder(width: 1, color: AppColors.textfield_Border)
        self.captaTextField.round(radius: 10)
        self.captaTextField.textAlignment = .center
        self.captaTextField.placeholder = LocalizedString.Enter_Code.localized
    }
}
