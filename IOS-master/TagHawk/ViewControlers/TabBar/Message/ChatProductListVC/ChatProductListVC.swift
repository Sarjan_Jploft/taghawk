//
//  ChatProductListVC.swift
//  TagHawk
//
//  Created by Appinventiv on 18/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ChatProductListVC: BaseVC {
    
    //MARK:- PROPERTIES
    //=================
    private var products = [Product]()
    private var isrefreshed = false
    private var serviceCalledFirstTime = true
    private let refreshControl = UIRefreshControl()
    private let controller = UserProfileController()
    private var selectedIndex: Int?
    var selectedProduct: ((Product) -> ())?
    
    
    //MARK:- IBOUTLETS
    //================
    
    @IBOutlet weak var mainCollView: UICollectionView! {
        didSet {
            mainCollView.delegate = self
            mainCollView.dataSource = self
            mainCollView.registerCell(with: ProductCollectionViewCell.self)
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.rightBarTitle(title: LocalizedString.done.localized, ofVC: self)
        self.leftBarItemImage(change: AppImages.blackCross.image, ofVC: self)
        self.navigationItem.title = LocalizedString.Products.localized
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        dismiss(animated: true, completion: nil)
    }
    
    override func rightBarButtonTapped(_ sender: UIBarButtonItem) {
        super.rightBarButtonTapped(sender)
        if let index = selectedIndex {
            selectedProduct?(products[index])
        } else {
            CommonFunctions.showToastMessage(LocalizedString.PleaseSelectAProduct.localized)
            return
        }
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- Collectionview datasource and delegates
//MARK:- UICollectionView Delegate and DataSource
extension ChatProductListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HomeHeaserView", for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 10
        return CGSize(width: finalContentWidth / 3, height: finalContentWidth / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getProductCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueCell(with: ProductCollectionViewCell.self, indexPath: indexPath)
        cell.cellFor = .home
        cell.populateData(product: self.products[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ProductCollectionViewCell else { return }
        cell.selectedImgView.isHidden = false
        selectedIndex = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ProductCollectionViewCell else { return }
        cell.selectedImgView.isHidden = true
        selectedIndex = nil
    }
}

//MARK:- PRIVATE FUNCTIONS
//========================
extension ChatProductListVC {
    
    private func initialSetup(){
        controller.productDelegate = self
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.mainCollView)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.mainCollView.addSubview(refreshControl)
        controller.getUserProduct(productStatus: ProfileProductVC.ProductsType.Selling.rawValue, page: page)
        
    }
    
    @objc func refresh(){
        self.resetPage()
        self.isrefreshed = true
        controller.getUserProduct(productStatus: ProfileProductVC.ProductsType.Selling.rawValue, page: page)
    }
}

extension ChatProductListVC: UserProductDelegate {
    
    func productDeleteSuccess() {
        
    }
    
    func productDeleteFailed() {
        
    }
    
    func willGetProducts() {
        if self.page == 1{
            self.shouldDisplayEmptyDataView = false
            self.mainCollView.reloadEmptyDataSet()
            if !isrefreshed {
                self.view.showIndicator()
                
            }
        }
    }
    
    func userProductServiceReturn(userProduct: [Product], nextPage: Int) {
        serviceCalledFirstTime = products.isEmpty ? true : false
        self.view.hideIndicator()
        
        if self.page == 1{
            self.products.removeAll()
            self.mainCollView.reloadData()
        }
        
        if nextPage != 0{
            self.page = nextPage
        }
        
        products.append(contentsOf: userProduct)
        mainCollView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.mainCollView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
    func userProductsFailed(errorType: ApiState) {
        //        CommonFunctions.showToastMessage(errorType)
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = true
        self.mainCollView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
}
