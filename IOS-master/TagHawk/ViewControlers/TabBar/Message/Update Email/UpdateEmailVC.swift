//
//  UpdateEmailVC.swift
//  TagHawk
//
//  Created by Appinventiv on 17/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

enum UpdateEmailVCInstantiated {
    
    case email
    case password
    case announcement
    case tagTitle
    case deleteGroup
    case description
}

protocol UpdateEmailVCDeleagte: class {
    func updates(text: String, type: UpdateEmailVCInstantiated, indexPath: IndexPath?)
}

class UpdateEmailVC: UIViewController {

//    MARK:- IBOutlets
//    ================
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var popUpTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var updateTextField: UITextField!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var btmConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var emailOuterView: UIView!
    @IBOutlet weak var deleteCodeView: UIView!
    @IBOutlet weak var deleteCodeText: UILabel!
    @IBOutlet weak var codeViewHeight: NSLayoutConstraint!
    
    //    MARK:- Proporties
//    =================
    var instantitaed: UpdateEmailVCInstantiated = .email
    weak var delegate: UpdateEmailVCDeleagte?
    var groupName: String = ""
    var indexPath: IndexPath?

    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    //MARK:- IBACtion
    
    //MARK:-  Update button tapped
    @IBAction func updateBtnTapped(_ sender: UIButton) {
        let str = updateTextField.text ?? ""
        guard !str.isEmpty else{
            
            switch instantitaed{
                
            case .email:
                CommonFunctions.showToastMessage(LocalizedString.enterNewEmail.localized)
            case .password:
              CommonFunctions.showToastMessage(LocalizedString.enterNewPassword.localized)
            case .announcement:
                CommonFunctions.showToastMessage(LocalizedString.newAnnouncement.localized)
            case .tagTitle:
                CommonFunctions.showToastMessage(LocalizedString.newTagName.localized)
            case .deleteGroup:
                CommonFunctions.showToastMessage(LocalizedString.codeEmpty.localized)
            case .description:
                CommonFunctions.showToastMessage(LocalizedString.emptyTagDescription.localized)
            }
            return
        }
        if instantitaed == .deleteGroup{
            if str != (deleteCodeText.text ?? ""){
                CommonFunctions.showToastMessage(LocalizedString.enterCorrectCode.localized)
                return
            }
        }
        
        delegate?.updates(text: str, type: instantitaed, indexPath: self.indexPath)
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Cancel button tapped
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func emailTextField(_ sender: UITextField) {
        
    }
}

extension UpdateEmailVC {
    
    //MARK:- initial set up
    private func initialSetup(){
        emailLbl.text = "@"
        emailLbl.font = AppFonts.Galano_Regular.withSize(13)
        emailLbl.textColor = AppColors.blackLblColor
        updateTextField.font = AppFonts.Galano_Regular.withSize(13)
        updateTextField.textColor = AppColors.blackLblColor
        
        popUpTitle.textColor = AppColors.blackLblColor
        
        popUpTitle.font = AppFonts.Galano_Semi_Bold.withSize(17.5)
        descriptionLabel.textColor = AppColors.lightGreyTextColor
        
        descriptionLabel.font = AppFonts.Galano_Regular.withSize(13)
        view.backgroundColor = AppColors.blackColor.withAlphaComponent(0.3)
        cancelBtn.titleLabel?.font = AppFonts.Galano_Semi_Bold.withSize(16)
        cancelBtn.setTitle(LocalizedString.cancel.localized)
        cancelBtn.setTitleColor(AppColors.blackBorder)
        cancelBtn.round(radius: 10)
        cancelBtn.borderWidth = 0.5
        updateBtn.setTitleColor(AppColors.whiteColor)
        updateBtn.round(radius: 10)
        updateBtn.titleLabel?.font = AppFonts.Galano_Semi_Bold.withSize(16)
        updateBtn.setTitle(LocalizedString.Update.localized)
        emailLbl.isHidden = true
        deleteCodeView.isHidden = true
        deleteCodeText.isHidden = true
        deleteCodeView.round(radius: 10)
        deleteCodeView.backgroundColor = AppColors.selectedSegmentColor.withAlphaComponent(0.3)
        deleteCodeText.font = AppFonts.Galano_Medium.withSize(18)
        deleteCodeText.textColor = AppColors.selectedSegmentColor
        codeViewHeight.constant = 0
        switch instantitaed{
            
        case .email:
            emailLbl.isHidden = false
            popUpTitle.text = LocalizedString.editEmail.localized
            descriptionLabel.text = LocalizedString.newVerificationEmail.localized
        case .password:
            popUpTitle.text = LocalizedString.editPassword.localized
            descriptionLabel.text = LocalizedString.newVerificationPassword.localized
            
        case .tagTitle:
            popUpTitle.text = LocalizedString.editTagName.localized
            descriptionLabel.text = LocalizedString.newTagName.localized
            
        case .announcement:
            popUpTitle.text = LocalizedString.editAnnouncement.localized
            descriptionLabel.text = LocalizedString.newAnnouncement.localized
            
        case .deleteGroup:
            codeViewHeight.constant = 46
            updateTextField.placeholder = LocalizedString.enterCode.localized
            popUpTitle.text = LocalizedString.deleteGroup.localized + "!"
            descriptionLabel.text = LocalizedString.areYouSureDeleteGroup.localized + groupName + LocalizedString.qustionMark_Group.localized
            updateBtn.setTitle(LocalizedString.Delete.localized)
            deleteCodeView.isHidden = false
            deleteCodeText.isHidden = false
            deleteCodeText.text = CommonFunctions.randomString(length: 8)
            
        case .description:
            popUpTitle.text = LocalizedString.Description.localized
            descriptionLabel.text = LocalizedString.newDescription.localized

        }
        
        updateTextField.delegate = self
        emailOuterView.round(radius: 10.0)
        emailOuterView.borderWidth = 0.5
        emailOuterView.borderColor = AppColors.grayColor
        view.layoutIfNeeded()
    }
    
    @objc func keyBoardWillShow(notification: Notification){
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            UIView.animate(withDuration: 0.3) {
               self.btmConstraint.constant = keyboardSize.height + 10
            }
            view.layoutIfNeeded()
        }
    }
    
    @objc func keyBoardWillHide(notification: Notification){
        UIView.animate(withDuration: 0.3) {
            self.btmConstraint.constant = 0
        }
        view.layoutIfNeeded()
    }
}

extension UpdateEmailVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
