//
//  ReviewVC.swift
//  TagHawk
//
//  Created by Appinventiv on 09/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ReviewVC: BaseVC {

    //MARK:- PROPERTIES
    //=================
    enum TableCells {
        case productDetail
        case shippingAddress
        case shippingPrice
    }
    
    //MARK:- Variables
    var productModel = CartProduct()
    var shippingModel = ShippingModel()
    var editBtnTap: (() -> ())?
    var shippingCharges: Double = 0.0
    private var tableCells: [TableCells] = [.productDetail, .shippingAddress, .shippingPrice]
    private let controller = ShippingController()
    weak var continueDelegate : ContinueButtonTapped?
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var mainTableView: UITableView! {
        didSet {
            mainTableView.delegate = self
            mainTableView.dataSource = self
            mainTableView.registerCell(with: ProductPriceDetailCell.self)
            mainTableView.registerCell(with: ShippingDetailsCell.self)
            mainTableView.registerCell(with: ShippingPriceDetailCell.self)
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func bindControler() {
        self.controller.labelDelegate = self
    }
    
    //MARK:- TARGET ACTIONS
    //=====================
    @IBAction func continueBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        self.continueDelegate?.continueButtonTapped()
    }
}

//MARK:- UITABLEIVIEW DELEGATE AND DATASOURCE
extension ReviewVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let curCell = tableCells[indexPath.row]
        
        switch curCell {
            
        case .productDetail:
            let cell = tableView.dequeueCell(with: ProductPriceDetailCell.self)
            cell.productImgView.setImage_kf(imageString: productModel.productPicUrl.first ?? "", placeHolderImage: AppImages.productPlaceholder.image)
            cell.nameLbl.text = productModel.productName
            cell.priceLbl.text = "$\(productModel.productPrice.rounded(toPlaces: 2).removeTrailingZero())"
            cell.shippingTypeLbl.text = productModel.shippingAvailibility.stringValue()
            cell.sellerNameLbl.text = "\(LocalizedString.Seller_Name.localized): \(productModel.sellerName)"
            return cell
            
        case .shippingAddress:
            let cell = tableView.dequeueCell(with: ShippingDetailsCell.self)
            cell.fullNameLbl.text = shippingModel.fullName
            cell.addressLbl.text = "\(shippingModel.apartment), \(shippingModel.streetAddress), \(shippingModel.city), \(shippingModel.state),"
            cell.zipCodeLbl.text = LocalizedString.Pincode.localized + ": \(shippingModel.zipcode)"
            cell.phoneLbl.text = LocalizedString.Mobile.localized + ": \(shippingModel.countryCode)-\(shippingModel.phoneNumber)"
            cell.editBtn.addTarget(self, action: #selector(editBtnTapped), for: .touchUpInside)
            return cell
            
        case .shippingPrice:
            let cell = tableView.dequeueCell(with: ShippingPriceDetailCell.self)
            cell.totalMrpLbl.text = "$\(Double(productModel.productPrice).rounded(toPlaces: 2).removeTrailingZero())"
            cell.shippingChargesLbl.text = "\(shippingCharges.rounded(toPlaces: 2).removeTrailingZero())"
            let totalAmount = Double(productModel.productPrice) + shippingCharges
            cell.totalAmtLbl.text = "$\(totalAmount.rounded(toPlaces: 2).removeTrailingZero())"
            return cell
        }
    }
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension ReviewVC {
    
    private func initialSetup(){
        
    }
    
    @objc private func editBtnTapped() {
        editBtnTap?()
    }
}


//MARK:- Webservices
extension ReviewVC : CreateLabelDelegate {
    
    func willCreatelabel() {
        self.view.showIndicator()
    
    }
    
    func labelCreatedSuccessFully() {
        self.view.hideIndicator()
    
    }
    
    func failedToCreateLabel(msg: String) {
        self.view.hideIndicator()
        
    }
    
}

