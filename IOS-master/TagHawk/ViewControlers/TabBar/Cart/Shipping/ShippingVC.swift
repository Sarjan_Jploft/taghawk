//
//  ShippingVC.swift
//  TagHawk
//
//  Created by Appinventiv on 09/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import CoreLocation

class ShippingVC: BaseVC {
    
    //MARK:- PROPERTIES
    //=================
    
    private enum TableCells: Int {
        case fullName = 0
        case streetAddress
        case apartment
        case zipcode
        case city
        case state
        case addressType
        case phoneNumber
        
        func stringValue() -> String {
            switch self {
            case .fullName:
                return LocalizedString.FullName.localized + "*"
            case .streetAddress:
                return LocalizedString.StreetAddress.localized + "*"
            case .apartment:
                return LocalizedString.Apartment.localized
            case .city:
                return LocalizedString.city.localized + "*"
            case .state:
                return LocalizedString.State.localized + "*"
            case .zipcode:
                return LocalizedString.ZipCode.localized + "*"
            case .phoneNumber:
                return LocalizedString.PhoneNumber.localized + "*"
            case .addressType:
                return LocalizedString.AddressType.localized + "*"
            }
        }
    }
    
    //MARK:- variables
    var continueBtnTap: ((ShippingModel) -> ())?
    private var shippingModel = ShippingModel()
    private var tableCells: [TableCells] = [.fullName, .streetAddress, .apartment, .zipcode, .city, .state, .addressType, .phoneNumber]
    
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var mainTableView: UITableView! {
        didSet {
            mainTableView.delegate = self
            mainTableView.dataSource = self
            mainTableView.registerCell(with: AddProductTextFieldCell.self)
            mainTableView.registerCell(with: PhoneNumberCell.self)
            mainTableView.registerCell(with: DropDownTableViewCell.self)
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    //MARK:- TARGET ACTIONS
    //=====================
    
    @IBAction func continueBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        //        if !UserProfile.main.isEmailVerified {
        //            verifyEmailAddress()
        //            return
        //        }
        if validateData() {
            continueBtnTap?(shippingModel)
        }
    }
}

//MARK:- UITABLEIVIEW DELEGATE AND DATASOURCE
extension ShippingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let curCell = tableCells[indexPath.row]
        
        switch curCell {
        case .addressType:
            return getDropDownCell(tableView, indexPath: indexPath)
        case .phoneNumber:
            let cell = tableView.dequeueCell(with: PhoneNumberCell.self)
            cell.titleLbl.attributedText = curCell.stringValue().attributeStringWithAstric()
            cell.phoneNumbTextField.text = shippingModel.phoneNumber
            cell.countryCodeBtn.setTitle(shippingModel.countryCode)
            cell.phoneNumbTextField.delegate = self
            cell.phoneNumbTextField.returnKeyType = .done
            cell.phoneNumbTextField.keyboardType = .numberPad
            cell.countryCodeBtnTap = { [weak self] sender in
                guard let `self` = self else { return }
                let vc = CountryCodeVC.instantiate(fromAppStoryboard: .Home)
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }
            return cell
        default:
            let cell = tableView.dequeueCell(with: AddProductTextFieldCell.self)
            cell.titleLabel.attributedText = curCell.stringValue().attributeStringWithAstric()
            cell.textField.delegate = self
            cell.titleLblTop.constant = 5
            cell.textField.returnKeyType = .next
            cell.textField.keyboardType = .default
            switch curCell {
            case .city:
                cell.textField.isEnabled = false
                cell.textField.text = shippingModel.city
            case .state:
                cell.textField.isEnabled = false
                cell.textField.text = shippingModel.state
            case .fullName:
                cell.titleLblTop.constant = 25
                cell.textField.isEnabled = true
                cell.textField.text = shippingModel.fullName
            case .streetAddress:
                cell.textField.isEnabled = true
                cell.textField.text = shippingModel.streetAddress
            case .apartment:
                cell.textField.isEnabled = true
                cell.textField.text = shippingModel.apartment
            case .zipcode:
                cell.textField.isEnabled = true
                cell.textField.text = shippingModel.zipcode
                cell.textField.keyboardType = .numberPad
            default: break
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let curCell = tableCells[indexPath.row]
        switch curCell {
        case .phoneNumber: return UITableView.automaticDimension
        case .fullName: return 120
        default: return 100
        }
    }
    
    func getDropDownCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DropDownTableViewCell.defaultReuseIdentifier) as? DropDownTableViewCell else {
            fatalError("FilterVC....\(DropDownTableViewCell.defaultReuseIdentifier) cell not found")
        }
        cell.dropDownView.backgroundColor = AppColors.textfield_Border.withAlphaComponent(0.1)
        cell.setUpFor = .addressType
        cell.addressTypeDelegate = self
        let labelText = self.shippingModel.addressType.isEmpty ? LocalizedString.Select.localized : self.shippingModel.addressType
        let labelColor = self.shippingModel.addressType.isEmpty ? AppColors.lightGreyTextColor : UIColor.black
        cell.setUpDropDownLabel(labelText: labelText, withColor: labelColor)
        cell.titleLabelTop.constant = 0
        return cell
    }
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension ShippingVC {
    
    private func initialSetup(){
        
    }
    
    func verifyEmailAddress(){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = .verifyEmailAddress
        vc.delegateWithType = self
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    private func validateData() -> Bool {
        if shippingModel.fullName.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.Please_Enter_Full_Name.localized)
            return false
        }
        if shippingModel.streetAddress.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.PleaseEnterAddress.localized)
            return false
        }
        
        if shippingModel.zipcode.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.PleaseEnterZipCode.localized)
            return false
        }
        
        if shippingModel.zipcode.count < 5 {
            CommonFunctions.showToastMessage(LocalizedString.invalidZipCode.localized)
            return false
        }
        if shippingModel.addressType.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.PleaseSelectAddressType.localized)
        }
        if shippingModel.phoneNumber.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.enterPhoneNumber.localized)
            return false
        }
        if shippingModel.phoneNumber.count < 7 || shippingModel.phoneNumber.count > 13 {
            CommonFunctions.showToastMessage(LocalizedString.Please_enter_valid_phoneNumber.localized)
            return false
        }
        return true
    }
}

extension ShippingVC: SetContryCodeDelegate {
    func setCountryCode(country_info: JSONDictionary) {
        guard let phoneCell = mainTableView.cellForRow(at: IndexPath(row: TableCells.phoneNumber.rawValue, section: 0)) as? PhoneNumberCell else { return }
        let code = "+\(country_info["CountryCode"]!)"
        phoneCell.countryCodeBtn.setTitle(code)
        self.shippingModel.countryCode = code
    }
}

//MARK:- Textfield delegates
extension ShippingVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let cell = mainTableView.cell(forItem: textField) as? AddProductTextFieldCell, let indexPath = mainTableView.indexPath(for: cell) {
            let curCell = tableCells[indexPath.row]
            switch curCell {
            case .zipcode:
                shippingModel.zipcode = textField.text ?? ""
                
                LocationManager.shared.geocodeAddressStringFromGoogle(address: shippingModel.zipcode) { (addressDict) in
                    
                    if addressDict.isEmpty {
                        CommonFunctions.showToastMessage(LocalizedString.invalidZipCode.localized)
                        self.shippingModel.city = ""
                        self.shippingModel.state = ""
                        self.mainTableView.reloadRows(at: [IndexPath(row: TableCells.city.rawValue, section: 0), IndexPath(row: TableCells.state.rawValue, section: 0)], with: .none)
                        
                    } else {
                        self.shippingModel.city = "\(addressDict[ApiKey.city] ?? "")"
                        self.shippingModel.state = "\(addressDict[ApiKey.state] ?? "")"
                        self.shippingModel.stateCode = "\(addressDict[ApiKey.stateCode] ?? "")"
                        self.shippingModel.country = "\(addressDict[ApiKey.country] ?? "")"
                        self.mainTableView.reloadRows(at: [IndexPath(row: TableCells.city.rawValue, section: 0), IndexPath(row: TableCells.state.rawValue, section: 0)], with: .none)
                    }
                    
                }
                
            case .fullName:
                shippingModel.fullName = textField.text ?? ""
            case .streetAddress:
                shippingModel.streetAddress = textField.text ?? ""
            case .apartment:
                shippingModel.apartment = textField.text ?? ""
            default:
                break
            }
        } else {
            shippingModel.phoneNumber = textField.text ?? ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let cell = mainTableView.cell(forItem: textField) as? AddProductTextFieldCell, let indexPath = mainTableView.indexPath(for: cell) {
            let curCell = tableCells[indexPath.row]
            
            if (range.location == 0 && string == " ") || string.containsEmoji {return false}
            
            if string.isBackSpace { return true }
            
            switch curCell {
                
            case .fullName:
                return isCharacterAcceptableForFirstName(name: textField.text ?? "", char: string)
                
            case .apartment:
                if (textField.text ?? "").count > 30 { return false }
                
            case .streetAddress:
                if (textField.text ?? "").count > 50 { return false }
                
            case .phoneNumber:
                if (textField.text ?? "").count > 13 { return false }
                
            case .zipcode:
                if (textField.text ?? "").count > 9 { return false }
            default: break
            }
        }
        return true
    }
    
    func isCharacterAcceptableForFirstName(name : String, char : String) -> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 30{
            return false
        }else{
            return char.isAllowed(validationStr: AllowedCharactersValidation.name)
        }
    }
}

extension ShippingVC: GetAddressTypeDelegate {
    func getAddressType(type: String) {
        shippingModel.addressType = type
        mainTableView.reloadRows(at: [IndexPath(row: TableCells.addressType.rawValue, section: 0)], with: .none)
    }
}

extension ShippingVC: CustomPopUpDelegateWithType {
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        let vc = EditProfileVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.userData = UserProfile.main
        self.present(vc, animated: true, completion: nil)
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        let vc = EditProfileVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.userData = UserProfile.main
        self.present(vc, animated: true, completion: nil)
    }
    
    
}
