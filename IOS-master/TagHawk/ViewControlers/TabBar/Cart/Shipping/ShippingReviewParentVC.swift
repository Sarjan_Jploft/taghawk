//
//  ShippingReviewParentVC.swift
//  TagHawk
//
//  Created by Appinventiv on 09/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ContinueButtonTapped : class {
    func continueButtonTapped()
}

class ShippingReviewParentVC: BaseVC {

    //MARK:- PROPERTIES
    //=================
    var productModel = CartProduct()
    private var shippingModel = ShippingModel()
    private var shippingVC : ShippingVC!
    private var reviewVC : ReviewVC!
    private let controller = ShippingController()
    private var getShipChargesFirstTime = true
    private let bankDetailsControler = BankDetailsControler()
    
    
    //MARK:- IBOUTL
    //================
    @IBOutlet weak var stepNumberView: UIView!
    @IBOutlet weak var firstLineView: UIView!
    @IBOutlet weak var whiteLineView: UIView!
    @IBOutlet weak var whiteLineLeading: NSLayoutConstraint!
    @IBOutlet weak var step1Lbl: UILabel!
    @IBOutlet weak var step2Lbl: UILabel!
    @IBOutlet weak var mainScrollView: UIScrollView! {
        didSet {
            mainScrollView.contentSize = CGSize(width: 2 * screenWidth, height: mainScrollView.height)
            mainScrollView.isPagingEnabled = true
            mainScrollView.isScrollEnabled = false
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.title = LocalizedString.Shipping.localized
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
    
    //MARK:- view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controller.delegate = self
        self.bankDetailsControler.cardDelegate = self
    }
    
    //MARK:- TARGET ACTIONS
    //=====================
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        step1Lbl.round()
        step2Lbl.round()
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        
       if self.mainScrollView.contentOffset.x == 0.0 {
        navigationController?.popViewController(animated: true)
       }else{
            self.mainScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            self.setStatesForFirstTab()
        }
    }
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension ShippingReviewParentVC {
    
    private func initialSetup(){
        controller.delegate = self
        setupViews()
        addShippingVC()
        addReviewVC()
    }
    
    private func setupViews() {
        view.bringSubviewToFront(stepNumberView)
        step1Lbl.font = AppFonts.Galano_Semi_Bold.withSize(17.5)
        step2Lbl.font = AppFonts.Galano_Semi_Bold.withSize(17.5)
        step1Lbl.backgroundColor = AppColors.themeColor
        step1Lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        step2Lbl.textColor = #colorLiteral(red: 0.5843137255, green: 0.6156862745, blue: 0.6705882353, alpha: 1)
        step1Lbl.borderWidth = 0.5
        step2Lbl.borderWidth = 0.5
        step1Lbl.borderColor = AppColors.themeColor
        step2Lbl.borderColor = #colorLiteral(red: 0.8196078431, green: 0.9137254902, blue: 0.9137254902, alpha: 1)
        stepNumberView.addShadow(opacity: 0.3)
    }
    
    private func addShippingVC() {
        shippingVC = ShippingVC.instantiate(fromAppStoryboard: .PaymentAndShipping)
        shippingVC.continueBtnTap = { [weak self] (shipModel) in
            guard let `self` = self else { return }
            self.mainScrollView.setContentOffset(CGPoint(x: screenWidth, y: 0), animated: true)
            `self`.setStatesForSecondTab()
            self.shippingModel = shipModel
            self.reviewVC.shippingModel = shipModel
            self.reviewVC.productModel = self.productModel
            self.getShippingCharges()
            self.reviewVC.mainTableView.reloadData()
        }
        shippingVC.view.frame = mainScrollView.bounds
        mainScrollView.addSubview(shippingVC.view)
        self.addChild(shippingVC)
        shippingVC.didMove(toParent: self)
    }
    
    //MARK:- Add review
    private func addReviewVC() {
        reviewVC = ReviewVC.instantiate(fromAppStoryboard: .PaymentAndShipping)
        reviewVC.editBtnTap = { [weak self] in
            guard let `self` = self else { return }
            self.mainScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            self.setStatesForFirstTab()
        }
        reviewVC.continueDelegate = self
        reviewVC.view.frame = mainScrollView.bounds
        reviewVC.view.origin.x = screenWidth
        mainScrollView.addSubview(reviewVC.view)
        self.addChild(reviewVC)
        reviewVC.didMove(toParent: self)
    }
    
    private func getShippingCharges() {
        controller.submitShippingInfo(prodId: productModel.productId, model: shippingModel)
    }
    
}

//MARK:- Webservices Delegates
extension ShippingReviewParentVC: ShippingControllerDelegate {
  
    func willRequestShippingCharges(){
        self.view.showIndicator()
    }
    
    func getShippingCharges(_ charge: Double) {
        reviewVC.shippingCharges = charge.rounded(toPlaces: 2)
        reviewVC.mainTableView.reloadData()
        self.view.hideIndicator()
    }
    
    func shippingChargesFailed() {
        self.view.hideIndicator()

    }
}

// MARK : Scrollview delegates
extension ShippingReviewParentVC  {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentXCoord = scrollView.contentOffset.x
        whiteLineLeading.constant = currentXCoord/12
        
        if currentXCoord == 0 && currentXCoord < screenWidth {
            setStatesForFirstTab()
        } else if currentXCoord >= screenWidth {
            setStatesForSecondTab()
        }
    }
    
    func setStatesForFirstTab(){
        UIView.animate(withDuration: 0.3) {
            self.navigationItem.title = LocalizedString.Shipping.localized
            self.step1Lbl.backgroundColor = AppColors.themeColor
            self.step2Lbl.backgroundColor = .white
            self.step1Lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.step2Lbl.textColor = #colorLiteral(red: 0.5843137255, green: 0.6156862745, blue: 0.6705882353, alpha: 1)
            self.step1Lbl.borderWidth = 0.5
            self.step2Lbl.borderWidth = 0.5
            self.step1Lbl.borderColor = AppColors.themeColor
            self.step2Lbl.borderColor = #colorLiteral(red: 0.8196078431, green: 0.9137254902, blue: 0.9137254902, alpha: 1)
        }
    }
    
    func setStatesForSecondTab(){
        UIView.animate(withDuration: 0.3) {
            if self.getShipChargesFirstTime {
                self.getShipChargesFirstTime = false }
            self.navigationItem.title = LocalizedString.Review.localized
            self.step1Lbl.backgroundColor = .white
            self.step2Lbl.backgroundColor = AppColors.themeColor
            self.step1Lbl.textColor = AppColors.themeColor
            self.step2Lbl.textColor = .white
            self.step1Lbl.borderWidth = 0.5
            self.step2Lbl.borderWidth = 0.5
            self.step1Lbl.borderColor = AppColors.themeColor
            self.step2Lbl.borderColor = AppColors.themeColor
        }
    }
}

//MARK:-  Webservices
extension ShippingReviewParentVC : GetCardsDelegate{
    
    func willgetCards(){
        self.view.showIndicator()
    }
    
    func cardsReceivedSuccessFully(cards : [Card]) {
        self.view.hideIndicator()
        
        if cards.isEmpty {
            let vc = TransactionDetaildVC.instantiate(fromAppStoryboard: AppStoryboard.PaymentAndShipping)
            vc.products = [self.productModel]
            vc.shippingCharges = self.reviewVC.shippingCharges
            vc.shippingModel = self.shippingModel
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = MySavedCardsVC.instantiate(fromAppStoryboard: AppStoryboard.PaymentAndShipping)
            vc.cards = cards
            vc.products = [self.productModel]
            vc.shippingCharges = self.reviewVC.shippingCharges
            vc.shippingModel = self.shippingModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func failedToReceiveCards(message : String){
        self.view.hideIndicator()
    }
}


extension ShippingReviewParentVC : ContinueButtonTapped {
    func continueButtonTapped() {
        self.bankDetailsControler.getCatds()
    }
}
