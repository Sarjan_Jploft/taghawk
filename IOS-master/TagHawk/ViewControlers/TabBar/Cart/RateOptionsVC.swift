//
//  RateOptionsVC.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 01/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class RateOptionsVC: BaseVC {
    
    //MARK:- PROPERTIES
    //=================
    var rateModel = SellerProductModel()
    private let controller = RateOptionsController()
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var mainImgView: UIImageView!
    @IBOutlet weak var prodTitleLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var ratingsView: FloatRatingView!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var placeHolderLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainImgView.round()
        popupView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- TARGET ACTIONS
    //=====================
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        controller.sendRatings(sellerId: rateModel.sellerId, prodId: rateModel.prodId, rating: ratingsView.rating, comment: descTextView.text)
    }
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension RateOptionsVC {
    
    //MARK:- Setup view
    private func initialSetup(){
        controller.delegate = self
        ratingsView.type = .halfRatings
        mainImgView.setImage_kf(imageString: rateModel.profilePicture, placeHolderImage: #imageLiteral(resourceName: "icTabProfileUnactive"))
        prodTitleLbl.text = rateModel.title
        nameLbl.text = rateModel.fullName
        descTextView.delegate = self
        self.dismissView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.backgroundColor = UIColor.clear
        self.doneBtn.round(radius: 10)
        self.cancelBtn.round(radius: 10)
        self.doneBtn.round(radius: 10)
        self.cancelBtn.backgroundColor = UIColor.white
        self.cancelBtn.setBorder(width: 0.5, color: AppColors.blackBorder)
        self.cancelBtn.setAttributes(title: LocalizedString.cancel.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.black, backgroundColor: UIColor.white)
        self.doneBtn.setAttributes(title: LocalizedString.Send.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: UIColor.clear)
    }
}

//MARK:p- textfield delegates
extension RateOptionsVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text {
            if text.isEmpty {
                placeHolderLbl.isHidden = false
            } else {
                placeHolderLbl.isHidden = true
            }
            countLbl.text = "\(text.count)/300"
            if text.count > 300 {
                countLbl.text = "300/300"
                textView.text.removeLast()
            }
        }
    }
}


extension RateOptionsVC: RateOptionsControllerDelegate {
    
    func ratedSuccessfully() {
        self.dismiss(animated: true) {
            CommonFunctions.showToastMessage(LocalizedString.ratedSellerSuccessfully.localized)
        }
    }
    
}
