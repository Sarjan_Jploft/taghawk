//
//  TransactionDetaildVC.swift
//  TagHawk
//
//  Created by Appinventiv on 28/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Stripe
import SwiftyJSON

struct TransactionDetails {
    
    var cardNumber : String = ""
    var expirey : String = ""
    var cvv : String = ""
    var name : String = ""
    var amount : Double = 0
    var currency : String = ""
    var source : String = ""
    var expireyMonth : Int = 0
    var expireyYear : Int = 0
    var saveCard = false
    var brand : String = ""
    var isApplePay = false

    init(){
        
    }
    
}

protocol ProductAlreadySoldDelegate : class {
    func productAlreadySold(productIds : [String])
}

class TransactionDetaildVC : BaseVC  {

    //MARK:- IBOutlets
    @IBOutlet weak var transactionDetailsTableView: UITableView!
    @IBOutlet weak var applePayButton: UIButton!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var payButtonImageView: UIImageView!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    @IBOutlet weak var saveCardButton: UIButton!
    @IBOutlet weak var saveCardLabel: UILabel!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var rightSepratorView: UIView!
    @IBOutlet weak var leftSepratorView: UIView!
    
    @IBOutlet weak var ViewPopUpShowError: UIView!
    
    //Variables
    var transactionData = TransactionDetails()
    private let controler = TransactionDetailsControler()
    private var messages: [MessageDetail] = []
    var products : [CartProduct] = []
    weak var productAlreadySold : ProductAlreadySoldDelegate?
    var shippingCharges = 0.0
    var shippingModel = ShippingModel()
    var addDetailsWhile = AddDepositAccountVC.AddBankDetailsWhile.adding
    var commingFor = CartVC.CommingFrom.other
    var cashOutBalance : Double = 0.0
    var merchant = MerchantData()

    //MARK:- Variables
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.sendtokenDelegate = self
        self.controler.saveCardDelegate = self
        self.controler.addDebitCardDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SingleChatFireBaseController.shared.delegate = self
        self.configureNavigation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SingleChatFireBaseController.shared.removeObservers(roomId: "", otherUserId: "")
    }
    
  
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBActions
    //MARK:- pay button tapped
    @IBAction func payButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.view.endEditing(true)
        if !AppNetworking.isConnectedToInternet{
        CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if self.commingFor == .whereTDepositVc {
            self.payWithCustumCard()
        }else{
            self.controler.getTotalAmount(products: self.products, shippingCharges: self.shippingCharges) <= 0 ? self.controler.chargeForZeroPrice(details: self.transactionData, products: self.products) : self.payWithCustumCard()
        }
    }
    
    //MARK:- Apple pay button tapped
    @IBAction func applePayButtonTapped(_ sender: UIButton) {
        
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet{ CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        
        
        //Temp comment due to payment error
        //self.getTokenForApplePay()
    }
    
    
    
    @IBAction func btnChatClicked(_ sender: UIButton) {
        
        if !AppNetworking.isConnectedToInternet{
        CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
//        self.removeFromParent()
//        self.view.removeFromSuperview()
        
//        self.controler.getTotalAmount(products: self.products, shippingCharges: self.shippingCharges) <= 0 ? self.controler.chargeForZeroPrice(details: self.transactionData, products: self.products) : self.payWithCustumCard()
        
        self.controler.chargeForZeroPrice(details: self.transactionData, products: self.products)
        
        
    }
    
    
    
    
    
    //MARK:- Save card button tapped
    @IBAction func saveCardButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.checkBoxImageView.image = self.transactionData.saveCard ? AppImages.unCheck.image : AppImages.check.image
        self.transactionData.saveCard = !self.transactionData.saveCard
    }
    
}

extension TransactionDetaildVC  {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.payButton.setAttributes(title: LocalizedString.Pay.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)
        self.payButton.round(radius: 10)
        self.payButtonImageView.round(radius: 10)
        self.configureTableView()
        self.checkBoxImageView.image = AppImages.unCheck.image
        self.transactionData.amount = self.controler.getTotalAmount(products: self.products, shippingCharges: self.shippingCharges)
        self.saveCardButton.isHidden = self.commingFor == .whereTDepositVc
        self.saveCardLabel.isHidden = self.commingFor == .whereTDepositVc
        self.applePayButton.isHidden = self.commingFor == .whereTDepositVc
        self.orLabel.isHidden = self.commingFor == .whereTDepositVc
        self.leftSepratorView.isHidden = self.commingFor == .whereTDepositVc
        self.rightSepratorView.isHidden = self.commingFor == .whereTDepositVc
        self.checkBoxImageView.isHidden = self.commingFor == .whereTDepositVc
        setPayButtonTitle()
        
        //Temp code due to Payment Error
        //self.applePayButton.isHidden = true
        self.payButton.isHidden = true
        self.checkBoxImageView.isHidden = true
        self.saveCardLabel.isHidden = true
        self.orLabel.isHidden = true
        self.rightSepratorView.isHidden = true
        self.leftSepratorView.isHidden = true
    }
    
    //MARK:- Set pay button title
    func setPayButtonTitle(){
        if self.commingFor == .whereTDepositVc{
            self.payButton.setTitle(self.addDetailsWhile == .adding ? LocalizedString.Add.localized : LocalizedString.Next.localized)
        }else{
            self.payButton.setTitle(LocalizedString.Pay.localized)
        }
    }
    
    //MARK:- Configure tableview
    func configureTableView(){
        self.transactionDetailsTableView.separatorStyle = .none
        self.transactionDetailsTableView.registerNib(nibName: PaymentCell.defaultReuseIdentifier)
        self.transactionDetailsTableView.dataSource = self
        self.transactionDetailsTableView.delegate = self
    }
    
    //MARK:- Show popup
    func productPurchasedSuccessfullyPopUp(){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = .productPurchasedSuccessfully
        vc.delegateWithType = self
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }

    //MARK:- Pop to view controler
    func popVC(){
        if let productData = products.first {
            var product = Product()
            var productImage = ProductImage(json: JSON())
            product.fullName = productData.sellerName
            productImage.image = productData.productPicUrl.first ?? ""
            productImage.thumbImage = productData.productPicUrl.first ?? ""
            product.images = [productImage]
            product.userId = productData.sellerId
            product.productId = productData.productId
            product.title = productData.productName
            product.firmPrice = productData.productPrice
            let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
            scene.productDetail = product
            scene.selectedProductID = product.productId
            scene.isFromSearchScreen = true
            scene.vcInstantiated = .productDetail
            navigationController?.pushViewController(scene, animated: true)
        }
    }
}

//MARK:- Configure Navigation
extension TransactionDetaildVC {

    //MARK:- Configure Navigation
func configureNavigation(){
   self.navigationItem.title = self.commingFor == .whereTDepositVc ? LocalizedString.Add_Debit_Card.localized : LocalizedString.Payment.localized
    self.navigationController?.setNavigationBarHidden(false, animated: false)
    self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
  }
}

//MAR:- Tableview datasource and delegates
extension TransactionDetaildVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentCell.defaultReuseIdentifier) as? PaymentCell else {
            fatalError("FilterVC....\(PaymentCell.defaultReuseIdentifier) cell not found")
        }
        
        cell.delegate = self
        
        switch indexPath.row {
           
            case 0:
                cell.setUpFor = .cardNumber
            
            case 1:
                cell.setUpFor = .expireyAndCvv
            
        default:
                cell.setUpFor = .name
      
        }
        cell.setUpLayOut()
        return cell
    }
}

//MARK:- Textfield delegates
extension TransactionDetaildVC : PaymentCellProtocol {
 
    func texfieldDidChange(type : PaymentCell.PaymentCellSetUpFor, textField : UITextField){
        
        guard let txt = textField.text else { return }
       
        switch type {
      
            case .cardNumber: self.transactionData.cardNumber = txt
            
            case .expireyAndCvv:
                
        guard let cell = self.transactionDetailsTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? PaymentCell else { return }
        
        if textField == cell.expireyTextField{
            
            if txt.count == 1{
                if txt != "1" && txt != "0"{
                    self.transactionData.expirey = "0\(txt)"
                    textField.text = "0\(txt)"
                }
            } else if txt.count == 2{
                self.transactionData.expirey = "\(txt)/"
                textField.text = "\(txt)/"
            }else if txt.count == 3 && !txt.contains(s: "/"){
                var currentTxt = txt
                currentTxt.insert("/", at: txt.index(txt.startIndex, offsetBy: 2))
                textField.text = currentTxt
                self.transactionData.expirey = currentTxt
            }
      
            self.transactionData.expirey = txt

        }else{
            self.transactionData.cvv = txt

       }
     
        case .name: self.transactionData.name = txt
            
        }
        
    }
    
    func textFieldShouldReturn(type : PaymentCell.PaymentCellSetUpFor){
        
    }
    
}

//MARK:- In app purchase delegates
extension TransactionDetaildVC : PKPaymentAuthorizationViewControllerDelegate  {
    
    func payWithCustumCard(){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if !self.controler.validateTransactionDetails(details: self.transactionData) { return }

        let cardParams = STPCardParams()
        cardParams.number = self.transactionData.cardNumber
        
        let expireyArray = self.transactionData.expirey.components(separatedBy: "/")
        
        cardParams.expMonth = UInt(expireyArray[0]) ?? 0
        cardParams.expYear = UInt(expireyArray[1]) ?? 0
        cardParams.cvc = self.transactionData.cvv
        cardParams.currency = "USD"
        self.view.showIndicator()
   
        STPAPIClient.shared().createToken(withCard: cardParams) {[weak self] (token: STPToken?, error: Error?) in
            self?.view.hideIndicator()
            if error != nil {
                printDebug(error?.localizedDescription)
                CommonFunctions.showToastMessage(error?.localizedDescription ?? "")
                return
            }
            
            guard let token = token, error == nil else { return }
            
            printDebug(token.tokenId)
            
            guard let weakSelf = self else { return }
            
            guard let crd = token.card else { return }
            
            weakSelf.transactionData.source = token.tokenId
            weakSelf.transactionData.amount = weakSelf.controler.getTotalAmount(products: weakSelf.products, shippingCharges: weakSelf.shippingCharges)
            
            weakSelf.transactionData.expireyYear = Int(crd.expYear)
            weakSelf.transactionData.expireyMonth = Int(crd.expMonth)
             weakSelf.transactionData.isApplePay = false

            if self?.commingFor == .whereTDepositVc{
                weakSelf.controler.addDebitCard(details: weakSelf.transactionData)
            }else{
                weakSelf.controler.containsShippingProduct(products: weakSelf.products) ? weakSelf.controler.sendToken(details: weakSelf.transactionData, products: weakSelf.products, shippingInfo: self?.shippingModel)  : weakSelf.controler.sendToken(details: weakSelf.transactionData, products: weakSelf.products)
            }
        }
    }
    
    
    func getTokenForApplePay() {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if !Stripe.deviceSupportsApplePay() {
            CommonFunctions.showToastMessage("Your device doesn't supported Apple Pay.")
            return
        }
        
        let paymentRequest = PKPaymentRequest()
        paymentRequest.merchantIdentifier = "merchant.taghawkmerchant.app"
        paymentRequest.countryCode = "US"
        paymentRequest.currencyCode = "USD"
        
        if #available(iOS 12.0, *) {
            paymentRequest.supportedNetworks = [.visa, .masterCard, .amex, .maestro]
        } else {
            paymentRequest.supportedNetworks = [.visa, .masterCard, .amex]
        }
        
        paymentRequest.merchantCapabilities = .capability3DS
        //        guard let productCheckoutData = self.productCheckoutData else {return}
        
        var paymentSummery = [PKPaymentSummaryItem]()
        
        let total = NSDecimalNumber(value: self.controler.getTotalAmount(products: self.products, shippingCharges: self.shippingCharges))
        
        paymentSummery.append(PKPaymentSummaryItem(label: "Product", amount: total))
        
        paymentRequest.paymentSummaryItems = paymentSummery
        if Stripe.canSubmitPaymentRequest(paymentRequest) {
            let paymentAuthorizationViewController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)!
            paymentAuthorizationViewController.delegate = self
            self.present(paymentAuthorizationViewController, animated: true, completion: nil)
        } else {
            CommonFunctions.showToastMessage("can't make payment with apple pay")
        }
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
//        self.dismiss(animated: true, completion: nil)
    }
    
    @available(iOS 11.0, *)
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        
        STPAPIClient.shared().createToken(with: payment) {[weak self] (token, error) in
            
            if error != nil {
                printDebug(error?.localizedDescription)
                CommonFunctions.showToastMessage(error?.localizedDescription ?? "")
                return
            }
            
            guard let token = token, error == nil else { return }
            
            printDebug(token.tokenId)
            
            guard let weakSelf = self else { return }
            
            guard let crd = token.card else { return }
            
            weakSelf.transactionData.source = token.tokenId
//            weakSelf.transactionData.cardNumber = crd.last4
            weakSelf.transactionData.amount = weakSelf.controler.getTotalAmount(products: weakSelf.products, shippingCharges: weakSelf.shippingCharges)
            weakSelf.transactionData.expireyYear = Int(crd.expYear)
            weakSelf.transactionData.expireyMonth = Int(crd.expMonth)
//            weakSelf.transactionData.name = crd.name ?? ""
//            weakSelf.transactionData.productId = weakSelf.productId
//            weakSelf.transactionData.sellerId = weakSelf.sellarId
            weakSelf.transactionData.isApplePay = true

            weakSelf.controler.containsShippingProduct(products: weakSelf.products) ? weakSelf.controler.sendToken(details: weakSelf.transactionData, products: weakSelf.products, shippingInfo: self?.shippingModel)  : weakSelf.controler.sendToken(details: weakSelf.transactionData, products: weakSelf.products)
            
            controller.dismiss(animated: true, completion: {
                
            })
        }
    }
}

extension TransactionDetaildVC : SendTokenDelegate {
    
    
    func validateTransactionDetails(message: String) {
        CommonFunctions.showToastMessage(message)
    }
    
    func willRequestToken() {
        self.view.showIndicator()
    }
    
    func productAlreadySold(productIds: [String], message: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
       // self.productAlreadySold?.productAlreadySold(productIds: productIds)
        self.popToCart(productIds: productIds)
    }
    
    func popToCart(productIds: [String]){
        guard let vcs = self.navigationController?.viewControllers else { return }
        for itm in vcs {
            if itm.isKind(of: CartVC.self){
                guard let cartVc = itm as? CartVC else { return }
                cartVc.productAlreadySold(productIds: productIds)
                self.navigationController?.popToViewController(itm, animated: true)
            }
        }
    }
    
    func tokenSentSuccessFully(data : TransactionDetails) {
        self.view.hideIndicator()
        
        // add message while product sold successfully
        
        products.forEach {(productData) in
            
            let roomId = SingleChatFireBaseController.shared.createRoomID(firstId: UserProfile.main.userId,
                                                                          secondId: productData.sellerId)
            
            printDebug("roomIdds:\(roomId)")
            printDebug("productData.sellerId:\(productData.sellerId)")
            printDebug("UserProfile.main.userId:\(UserProfile.main.userId)")
            
            SingleChatFireBaseController.shared.isRoomExist(userID: productData.sellerId,
                                                            roomID: roomId, roomData: {[weak self](roomInfo) in
                                                                
                                                                guard let strongSelf = self else{ return }
                                                                
                                                                var product = Product()
                                                                var productImage = ProductImage(json: JSON())
                                                                product.title = productData.productName
                                                                productImage.image = productData.productPicUrl.first ?? ""
                                                                productImage.thumbImage = productData.productPicUrl.first ?? ""
                                                                product.images = [productImage]
                                                                product.productId = productData.productId
                                                                product.firmPrice = productData.productPrice
                                                                
                                                                product.userId = productData.sellerId
                                                                product.fullName = productData.sellerName
                                                                
                                                                let msg = LocalizedString.relaseThePaymentFor.localized + (productData.productName)
                                                                
                                                                SingleChatFireBaseController.shared.getNewMessage(msgTimeStamp: Date().unixFirebaseTimestamp, roomId: roomId)
                                                                SingleChatFireBaseController.shared.createSingleChat(roomData: nil,
                                                                                                                     data: product,
                                                                                                                     sharedProduct: nil,
                                                                                                                     sharedTag: nil,
                                                                                                                     userProfile: nil,
                                                                                                                     text: msg,
                                                                                                                     msgType: .text,
                                                                                                                     vcInstantiated: .productDetail)
            })
        }
        
        if self.transactionData.saveCard && !self.transactionData.isApplePay{
            self.controler.saveCard(details: self.transactionData)
        }else{
            //self.productPurchasedSuccessfullyPopUp()
        }
    }
    
    func failedToSendToken(message: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
}

extension TransactionDetaildVC : SaveCardDelegate {
    
    func willSaveCard(){
        self.view.showIndicator()
    }
    
    func saveCardSuccessFully(){
        self.view.hideIndicator()
//        self.productPurchasedSuccessfullyPopUp()
    }
    
    func failedToSaveCard(message : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
}

extension TransactionDetaildVC : CustomPopUpDelegateWithType{
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        self.popVC()
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
    }
}

extension TransactionDetaildVC: SingleChatFireBaseControllerDelegate {
    
    func getNewMessage(messageData: MessageDetail){
        
        if !messages.contains(where: {$0.messageID == messageData.messageID}){
            messages.append(messageData)
        }
        
        if messages.count == products.count, let productInfo = products.first {
            
            let roomID = SingleChatFireBaseController.shared.createRoomID(firstId: UserProfile.main.userId,
                                                                          secondId: productInfo.sellerId)
            
            let isUserHavRoom = SingleChatFireBaseController.shared.checkRoomExistence(roomID: roomID, userID: UserProfile.main.userId)
            let isOtherUserHavRoom = SingleChatFireBaseController.shared.checkRoomExistence(roomID: roomID, userID: productInfo.sellerId)
            
            if isUserHavRoom && isOtherUserHavRoom{
                
                var userProduct = UserProduct()
                userProduct.id = productInfo.productId
                userProduct.firmPrice = productInfo.productPrice
                userProduct.productName = productInfo.productName
                
                var productImage = ProductImage(json: JSON())
                productImage.image = products.first?.productPicUrl.first ?? ""
                productImage.thumbImage = products.first?.productPicUrl.first ?? ""
                userProduct.images = [productImage]
                SingleChatFireBaseController.shared.updateProductInfo(product: userProduct,
                                                                      roomId: roomID,
                                                                      otherUserID: productInfo.sellerId)
//                self.productPurchasedSuccessfullyPopUp()
                self.popVC()
            }
        }
        //
        //
        //        let data = SingleChatFireBaseController.shared.usersRoom.filter({$0.isExist == false})
        //        if data.isEmpty{
        //            if let productInfo = products.first,
        //                let firstMessage = messages.last,
        //                firstMessage.senderID == UserProfile.main.userId{
        //
        //
        //            }
        //
        //        }
    }
}


extension TransactionDetaildVC : AddDebitCardDelegate{
    
    func willAddDebitCard() {
        self.view.showIndicator()
        
    }
    
    func debitCardAddedSuccessFully() {
        self.view.hideIndicator()
        
        if self.addDetailsWhile == .cashout{
            let vc = CashOutRequiredDataVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
            vc.cashoutBalance = self.cashOutBalance
            self.navigationController?.pushViewController(vc, animated: true)
            
            guard let vcs = self.navigationController?.viewControllers else { return }
            for itm in vcs {
                if itm.isKind(of: AccountInfoAndPaymentHistoryVC.self){
                    guard let historyVC = itm as?  AccountInfoAndPaymentHistoryVC else { return }
                    historyVC.accountInfoVc.isRefreshing = false
                    historyVC.accountInfoVc.controler.getBalance()
                }
            }
            
        }else{
            CommonFunctions.showToastMessage(LocalizedString.Card_Added_Successfully.localized)
                guard let vcs = self.navigationController?.viewControllers else { return }
                for itm in vcs {
                    if itm.isKind(of: AccountInfoAndPaymentHistoryVC.self){
                        guard let historyVC = itm as?  AccountInfoAndPaymentHistoryVC else { return }
                        historyVC.accountInfoVc.controler.getBalance()
                        self.navigationController?.popToViewController(itm, animated: true)
                    }
            }
        }
    }
    
    func failedToAddDebitCard(msg: String) {
        CommonFunctions.showToastMessage(msg)
        self.view.hideIndicator()
    }
}

