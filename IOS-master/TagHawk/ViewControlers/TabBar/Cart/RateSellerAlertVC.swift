//
//  RateSellerAlertVC.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 01/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class RateSellerAlertVC: BaseVC {
    
    //MARK:- PROPERTIES
    //=================
    var closeBtnTap: (() -> ())?
    var rateBtnTap: (() -> ())?
    var headingText = ""
    var descriptionText = NSAttributedString()
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var rateBtn: UIButton!
    
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popupView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    //MARK:- TARGET ACTIONS
    //=====================
    @IBAction func closeBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        dismisViewTapped()
        closeBtnTap?()
    }
    
    @IBAction func rateBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        rateBtnTap?()
    }
}

//MARK:- PRIVATE FUNCTIONS
//========================
extension RateSellerAlertVC {
    
    private func initialSetup(){
        self.headingLabel.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(17.5), textColor: UIColor.black)
        self.descriptionLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
        self.headingLabel.text = headingText
        self.descriptionLabel.attributedText = descriptionText
        self.dismissView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.backgroundColor = UIColor.clear
        
    }
    
    private func dismisViewTapped(){
        self.dismiss(animated: true, completion: nil)
    }
}
