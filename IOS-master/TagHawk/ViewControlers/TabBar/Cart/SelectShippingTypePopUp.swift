//
//  SelectShippingTypePopUp.swift
//  TagHawk
//
//  Created by Appinventiv on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol GetSelectedShippingAvailability : class {
    func getSelectedShippingAvailability(shipping : ShippingAvailability)
}

class SelectShippingTypePopUp : BaseVC {

    //MARK:- IBOutlets
    @IBOutlet weak var dismisView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var pickUpButton: UIButton!
    @IBOutlet weak var deliveryButton: UIButton!
    @IBOutlet weak var shippingButton: UIButton!
    
    //MARK:- Variables
    weak var delegate : GetSelectedShippingAvailability?
    var selectedShippingAvailability : ShippingAvailability?
    var availableShipping : [ShippingAvailability] = []
    
    //NARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- IBActions
    @IBAction func pickUpButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.selectDeselectShippingAvailability(shipping: ShippingAvailability.pickUp)
        self.delegate?.getSelectedShippingAvailability(shipping: .pickUp)
        self.dismiss(animated: true) {
        }
    }
    
    @IBAction func deliveryButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.selectDeselectShippingAvailability(shipping: ShippingAvailability.deliver)
        self.delegate?.getSelectedShippingAvailability(shipping: .deliver)
        self.dismiss(animated: true) {
        }
    }
    
    @IBAction func shippingButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.selectDeselectShippingAvailability(shipping: ShippingAvailability.shipping)
        self.delegate?.getSelectedShippingAvailability(shipping: .shipping)
        self.dismiss(animated: true) {
        }
    }
}

extension SelectShippingTypePopUp {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.headingLabel.setAttributes(text: LocalizedString.Please_Select_Shipping_Type.localized , font: AppFonts.Galano_Semi_Bold.withSize(17.5), textColor: UIColor.black)
        self.dismisView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.backgroundColor = UIColor.clear
       let pickUpTextColor =  self.availableShipping.contains(ShippingAvailability.pickUp) ? UIColor.black : AppColors.lightGreyTextColor
        self.pickUpButton.setAttributes(title: LocalizedString.Pickup.localized, font: AppFonts.Galano_Medium.withSize(13), titleColor: pickUpTextColor, backgroundColor: UIColor.white)
        let deliveryTextColor =  self.availableShipping.contains(ShippingAvailability.deliver) ? UIColor.black : AppColors.lightGreyTextColor
        self.deliveryButton.setAttributes(title: LocalizedString.Delivery.localized, font: AppFonts.Galano_Medium.withSize(13), titleColor: deliveryTextColor, backgroundColor: UIColor.white)
        let shippingTextColor =  self.availableShipping.contains(ShippingAvailability.shipping) ? UIColor.black : AppColors.lightGreyTextColor
        self.shippingButton.setAttributes(title: LocalizedString.FedEx.localized, font: AppFonts.Galano_Medium.withSize(13), titleColor: shippingTextColor, backgroundColor: UIColor.white)
        self.pickUpButton.round(radius: 10)
        self.deliveryButton.round(radius: 10)
        self.shippingButton.round(radius: 10)
        self.pickUpButton.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.deliveryButton.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.shippingButton.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.pickUpButton.isEnabled = self.availableShipping.contains(ShippingAvailability.pickUp)
        self.deliveryButton.isEnabled = self.availableShipping.contains(ShippingAvailability.deliver)
        self.shippingButton.isEnabled = self.availableShipping.contains(ShippingAvailability.shipping)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismisViewTapped))
        self.dismisView.addGestureRecognizer(tap)
    }
    
    @objc func dismisViewTapped(){
        self.dismiss(animated: true, completion: nil)
   }
    
    //MARK:- Select deselect shipping avilability
    func selectDeselectShippingAvailability(shipping : ShippingAvailability){
        
        switch shipping {
        case .pickUp:
            
            let deliveryTextColor =  self.availableShipping.contains(ShippingAvailability.deliver) ? UIColor.black : AppColors.lightGreyTextColor
            let shippingTextColor =  self.availableShipping.contains(ShippingAvailability.shipping) ? UIColor.black : AppColors.lightGreyTextColor
            
            self.pickUpButton.backgroundColor = AppColors.appBlueColor
            self.pickUpButton.setTitleColor(UIColor.white)
            
            self.deliveryButton.backgroundColor = UIColor.white
            self.deliveryButton.setTitleColor(deliveryTextColor)
            
            self.shippingButton.backgroundColor = UIColor.white
            self.shippingButton.setTitleColor(shippingTextColor)
            
           self.selectedShippingAvailability = ShippingAvailability.pickUp
            
        case .deliver:
            
            let pickUpTextColor =  self.availableShipping.contains(ShippingAvailability.pickUp) ? UIColor.black : AppColors.lightGreyTextColor
            let shippingTextColor =  self.availableShipping.contains(ShippingAvailability.shipping) ? UIColor.black : AppColors.lightGreyTextColor

            self.pickUpButton.backgroundColor = UIColor.white
            self.pickUpButton.setTitleColor(pickUpTextColor)
            
            self.deliveryButton.backgroundColor = AppColors.appBlueColor
            self.deliveryButton.setTitleColor(UIColor.white)
            
            self.shippingButton.backgroundColor = UIColor.white
            self.shippingButton.setTitleColor(shippingTextColor)
            
            self.selectedShippingAvailability = ShippingAvailability.deliver

            
        case .shipping:
           
            let pickUpTextColor =  self.availableShipping.contains(ShippingAvailability.pickUp) ? UIColor.black : AppColors.lightGreyTextColor
            let deliveryTextColor =  self.availableShipping.contains(ShippingAvailability.deliver) ? UIColor.black : AppColors.lightGreyTextColor


            self.pickUpButton.backgroundColor = UIColor.white
            self.pickUpButton.setTitleColor(pickUpTextColor)
            
            self.deliveryButton.backgroundColor = UIColor.white
            self.deliveryButton.setTitleColor(deliveryTextColor)
            
            self.shippingButton.backgroundColor = AppColors.appBlueColor
            self.shippingButton.setTitleColor(UIColor.white)
            
            self.selectedShippingAvailability = ShippingAvailability.shipping

            
        }
    }
}
