//
//  PaymentProcessingPopUpViewController.swift
//  TagHawk
//
//  Created by Appinventiv on 12/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class PaymentProcessingPopUp : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var dismisView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}

extension PaymentProcessingPopUp{
    
    //MARK:- Set up view
    func setUpSubView(){
        self.titleLabel.setAttributes(text: LocalizedString.Payment_Processing.localized, font: AppFonts.Galano_Semi_Bold.withSize(17.5), textColor: UIColor.black)
        self.descriptionLabel.setAttributes(text: LocalizedString.Please_Do_Not_Go_Back.localized, font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
        self.dismisView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.backgroundColor = UIColor.clear
    }
    
}
