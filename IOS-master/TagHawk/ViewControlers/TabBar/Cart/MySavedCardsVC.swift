//
//  MySavedCardsVC.swift
//  TagHawk
//
//  Created by Appinventiv on 09/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Stripe
import SwiftyJSON


class MySavedCardsVC : BaseVC {
    
    @IBOutlet weak var applePayButton: UIButton!
    @IBOutlet weak var addCardButton: UIButton!
    @IBOutlet weak var cardsTableView: UITableView!
    
    var transactionData = TransactionDetails()
    private let controler = BankDetailsControler()
    private let transactionControler = TransactionDetailsControler()
    var cards : [Card] = []
    private var messages: [MessageDetail] = []
    //    var totalAmmount = 1
    //    var productId : String = ""
    //    var sellarId : String = ""
    var commingFor = CartVC.CommingFrom.other
    var detail = TransactionDetails()
    var products : [CartProduct] = []
    weak var productAlreadySold : ProductAlreadySoldDelegate?
    var shippingCharges = 0.0
    var shippingModel = ShippingModel()
    
    
    //MARK:- View lifr cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        SingleChatFireBaseController.shared.removeObservers(roomId: "", otherUserId: "")
    }
    
    override func bindControler() {
        super.bindControler()
        
        self.controler.cardDelegate = self
        self.transactionControler.sendtokenDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SingleChatFireBaseController.shared.delegate = self
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func applePayButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.getTokenForApplePay()
    }
    
    
    @IBAction func addCardButton(_ sender: UIButton) {
        let vc = TransactionDetaildVC.instantiate(fromAppStoryboard: AppStoryboard.PaymentAndShipping)
        vc.products = self.products
        vc.shippingModel = self.shippingModel
        vc.shippingCharges = self.shippingCharges
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MySavedCardsVC{
    
    func setUpSubView(){
        
        self.addCardButton.setAttributes(title: LocalizedString.Add_Card.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)
        self.configureTableView()
        
    }
    
    func configureTableView(){
        self.cardsTableView.separatorStyle = .none
        self.cardsTableView.registerNib(nibName: CardCell.defaultReuseIdentifier)
        self.cardsTableView.delegate = self
        self.cardsTableView.dataSource = self
    }
    
    func productPurchasedSuccessfullyPopUp(){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = .productPurchasedSuccessfully
        vc.delegateWithType = self
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func popVC(){
        if let productData = products.first {
            var product = Product()
            var productImage = ProductImage(json: JSON())
            product.fullName = productData.sellerName
            productImage.image = productData.productPicUrl.first ?? ""
            productImage.thumbImage = productData.productPicUrl.first ?? ""
            product.images = [productImage]
            product.userId = productData.sellerId
            product.productId = productData.productId
            product.title = productData.productName
            product.firmPrice = productData.productPrice
            let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
            scene.productDetail = product
            scene.selectedProductID = product.productId
            scene.isFromSearchScreen = true
            scene.vcInstantiated = .productDetail
            navigationController?.pushViewController(scene, animated: true)
        }
    }
    
    func showPopup(type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none, msg : String = ""){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        vc.msg = msg
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func popToCart(productIds: [String]){
        guard let vcs = self.navigationController?.viewControllers else { return }
        for itm in vcs {
            if itm.isKind(of: CartVC.self){
                guard let cartVc = itm as? CartVC else { return }
                cartVc.productAlreadySold(productIds: productIds)
                self.navigationController?.popToViewController(itm, animated: true)
            }
        }
    }
}

extension MySavedCardsVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Payment.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
    
}

extension MySavedCardsVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CardCell.defaultReuseIdentifier) as? CardCell else {
            fatalError("FilterVC....\(CardCell.defaultReuseIdentifier) cell not found")
        }
        cell.populateData(card: self.cards[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        detail.source = self.cards[0].id
        detail.amount = self.transactionControler.getTotalAmount(products: self.products, shippingCharges: self.shippingCharges)
        //        detail.productId = self.productId
        //        detail.sellerId = self.sellarId
        
        self.showPopup(type: CustomPopUpVC.CustomPopUpFor.sureToPayFromParticularCard, msg: "\(LocalizedString.Sure_To_Pay_Using.localized) xxxx\(self.cards[indexPath.row].last4)")
        
    }
    
}

extension MySavedCardsVC : PKPaymentAuthorizationViewControllerDelegate  {
    
    func getTokenForApplePay() {
        if !Stripe.deviceSupportsApplePay() {
            CommonFunctions.showToastMessage("Your device doesn't supported Apple Pay.")
            return
        }
        
        let paymentRequest = PKPaymentRequest()
        paymentRequest.merchantIdentifier = "merchant.taghawkmerchant.app"
        paymentRequest.countryCode = "US"
        paymentRequest.currencyCode = "USD"
        
        if #available(iOS 12.0, *) {
            paymentRequest.supportedNetworks = [.visa, .masterCard, .amex, .maestro]
        } else {
            paymentRequest.supportedNetworks = [.visa, .masterCard, .amex]
        }
        
        paymentRequest.merchantCapabilities = .capability3DS
        //        guard let productCheckoutData = self.productCheckoutData else {return}
        
        var paymentSummery = [PKPaymentSummaryItem]()
        
        let total = NSDecimalNumber(value: self.transactionControler.getTotalAmount(products: self.products, shippingCharges: self.shippingCharges))
        
        paymentSummery.append(PKPaymentSummaryItem(label: "Product", amount: total))
        
        paymentRequest.paymentSummaryItems = paymentSummery
        if Stripe.canSubmitPaymentRequest(paymentRequest) {
            let paymentAuthorizationViewController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)!
            paymentAuthorizationViewController.delegate = self
            self.present(paymentAuthorizationViewController, animated: true, completion: nil)
        } else {
            CommonFunctions.showToastMessage("can't make payment with apple pay")
        }
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @available(iOS 11.0, *)
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        
        STPAPIClient.shared().createToken(with: payment) {[weak self] (token, error) in
            
            if error != nil {
                printDebug(error?.localizedDescription)
                CommonFunctions.showToastMessage(error?.localizedDescription ?? "")
                return
            }
            
            guard let token = token, error == nil else { return }
            
            printDebug(token.tokenId)
            
            guard let weakSelf = self else { return }
            
            guard let crd = token.card else { return }
            
            weakSelf.transactionData.source = token.tokenId
            //            weakSelf.transactionData.cardNumber = crd.last4
            weakSelf.transactionData.amount = weakSelf.transactionControler.getTotalAmount(products: weakSelf.products, shippingCharges: weakSelf.shippingCharges)
            weakSelf.transactionData.expireyYear = Int(crd.expYear)
            weakSelf.transactionData.expireyMonth = Int(crd.expMonth)
            weakSelf.transactionData.isApplePay = true
            weakSelf.transactionControler.containsShippingProduct(products: weakSelf.products) ? weakSelf.transactionControler.sendToken(details: weakSelf.transactionData, products: weakSelf.products, shippingInfo: self?.shippingModel)  : weakSelf.transactionControler.sendToken(details: weakSelf.transactionData, products: weakSelf.products)
            controller.dismiss(animated: true, completion: {
                
            })
        }
    }
}

extension MySavedCardsVC : GetCardsDelegate{
    
    func willgetCards(){
        self.view.showIndicator()
        
    }
    
    func cardsReceivedSuccessFully(cards : [Card]){
        self.view.hideIndicator()
        self.cards = cards
        self.cardsTableView.reloadData()
    }
    
    func failedToReceiveCards(message : String){
        self.view.hideIndicator()
    }
}

extension MySavedCardsVC : SendTokenDelegate {
    
    func validateTransactionDetails(message: String) {
        CommonFunctions.showToastMessage(message)
    }
    
    func willRequestToken() {
        self.view.showIndicator()
    }
    
    func productAlreadySold(productIds: [String], message: String) {
        CommonFunctions.showToastMessage(message)
        //        self.productAlreadySold?.productAlreadySold(productIds: productIds)
        self.popToCart(productIds: productIds)
    }
    
    
    func tokenSentSuccessFully(data : TransactionDetails) {
        self.view.hideIndicator()
        
        products.forEach {(productData) in
            
            let roomId = SingleChatFireBaseController.shared.createRoomID(firstId: UserProfile.main.userId,
                                                                          secondId: productData.sellerId)
            
            printDebug("roomIdds:\(roomId)")
            printDebug("productData.sellerId:\(productData.sellerId)")
            printDebug("productData.productId:\(productData.productId)")
            printDebug("UserProfile.main.userId:\(UserProfile.main.userId)")
            
            SingleChatFireBaseController.shared.isRoomExist(userID: productData.sellerId,
                                                            roomID: roomId,
                                                            roomData: {(roomInfo) in
                                                                
                                                                var product = Product()
                                                                var productImage = ProductImage(json: JSON())
                                                                product.title = productData.productName
                                                                productImage.image = productData.productPicUrl.first ?? ""
                                                                productImage.thumbImage = productData.productPicUrl.first ?? ""
                                                                product.images = [productImage]
                                                                product.productId = productData.productId
                                                                
                                                                product.firmPrice = productData.productPrice
                                                                
                                                                product.userId = productData.sellerId
                                                                product.fullName = productData.sellerName
                                                                
                                                                let msg = LocalizedString.relaseThePaymentFor.localized + (productData.productName)
                                                                let roomID = SingleChatFireBaseController.shared.createRoomID(firstId: product.userId, secondId: UserProfile.main.userId)
                                                                SingleChatFireBaseController.shared.getNewMessage(msgTimeStamp: Date().unixFirebaseTimestamp, roomId: roomID)
                                                                SingleChatFireBaseController.shared.createSingleChat(roomData: nil,
                                                                                                                     data: product,
                                                                                                                     sharedProduct: nil,
                                                                                                                     sharedTag: nil,
                                                                                                                     userProfile: nil,
                                                                                                                     text: msg,
                                                                                                                     msgType: .text,
                                                                                                                     vcInstantiated: .productDetail)
                                                                
            })
        }
    }
    
    func failedToSendToken(message: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
}

extension MySavedCardsVC : CustomPopUpDelegateWithType{
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        self.popVC()
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if self.transactionControler.getTotalAmount(products: self.products, shippingCharges: self.shippingCharges) <= 0{
            self.transactionControler.chargeForZeroPrice(details: self.transactionData, products: self.products)
        }else{
            self.transactionControler.containsShippingProduct(products: self.products) ? self.transactionControler.sendToken(details: self.detail, products: self.products, shippingInfo: self.shippingModel)  : self.transactionControler.sendToken(details: self.detail, products: self.products)
        }
    }
}

extension MySavedCardsVC: SingleChatFireBaseControllerDelegate {
    
    func getNewMessage(messageData: MessageDetail){
        
        if !messages.contains(where: {$0.messageID == messageData.messageID}){
            messages.append(messageData)
        }
        
        if messages.count == products.count, let productInfo = products.first {
           
            let roomID = SingleChatFireBaseController.shared.createRoomID(firstId: UserProfile.main.userId,
                                                                          secondId: productInfo.sellerId)
            
            let isUserHavRoom = SingleChatFireBaseController.shared.checkRoomExistence(roomID: roomID, userID: UserProfile.main.userId)
            let isOtherUserHavRoom = SingleChatFireBaseController.shared.checkRoomExistence(roomID: roomID, userID: productInfo.sellerId)
            
            if isUserHavRoom && isOtherUserHavRoom{
                
                var userProduct = UserProduct()
                userProduct.id = productInfo.productId
                userProduct.firmPrice = productInfo.productPrice
                userProduct.productName = productInfo.productName
                
                var productImage = ProductImage(json: JSON())
                productImage.image = products.first?.productPicUrl.first ?? ""
                productImage.thumbImage = products.first?.productPicUrl.first ?? ""
                userProduct.images = [productImage]
                SingleChatFireBaseController.shared.updateProductInfo(product: userProduct,
                                                                      roomId: roomID,
                                                                      otherUserID: productInfo.sellerId)
                self.productPurchasedSuccessfullyPopUp()
            }
        }
//        
//        
//        let data = SingleChatFireBaseController.shared.usersRoom.filter({$0.isExist == false})
//        if data.isEmpty{
//            if let productInfo = products.first,
//                let firstMessage = messages.last,
//                firstMessage.senderID == UserProfile.main.userId{
//                
//                
//            }
//            
//        }
    }
}
