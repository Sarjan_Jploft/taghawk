//
//  CartVC.swift
//  TagHawk
//
//  Created by Appinventiv on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import AMScrollingNavbar

class CartVC : BaseVC {
    
    enum CommingFrom {
        case productDetails
        case whereTDepositVc
        case other
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var cartCollectionView: UICollectionView!
    @IBOutlet weak var checkOutButton: UIButton!
    
    @IBOutlet weak var viewErrorPopUp: UIView!
    
    
    
    //MARK:- Variables
    private var products : [CartProduct] = []
    private let cartControler = CartControler()
    var commingFor = CommingFrom.other
    let controler = BankDetailsControler()
    
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.cartControler.getCartDelegate = self
        self.cartControler.deleteProductDelegate = self
        self.controler.cardDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBActions
    
    //MARK:- Checkout button tapped
    @IBAction func checkOutButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if self.products.isEmpty { return }
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        let shippingProd = self.products.filter { $0.shippingAvailibility == .shipping}
        
        if !shippingProd.isEmpty{
            guard let firstProd = shippingProd.first else { return }
            let shippingReviewScene = ShippingReviewParentVC.instantiate(fromAppStoryboard: .PaymentAndShipping)
            shippingReviewScene.productModel = firstProd
            navigationController?.pushViewController(shippingReviewScene, animated: true)
            return
        }
        self.controler.getCatds()
    }
    
    
    
    //MARK:- Buy with Apple Pay Clicked
    //Temp Code
    
    @IBAction func btnApplePayClicked(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            
            self.viewErrorPopUp.isHidden = false
            
        }, completion: nil)
        
        
        
        
    }
    
    
    
    
    @IBAction func btnChatClicked(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                   
                   self.viewErrorPopUp.isHidden = true
                   
               }, completion: nil)
               
        
        
    }
    
    
    
    
    
    //MARK:- Action when product is already sold
    func productAlreadySold(productIds : [String]) {
        for item in productIds {
            if let idx = self.products.index(where: {$0.productId == item}){
                self.products[idx].productStatus = .sold
            }
        }
        self.cartCollectionView.reloadData()
    }
}

//MARK:- Private Functions
private extension CartVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.checkOutButton.setAttributes(title: LocalizedString.Checkout.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)
        self.checkOutButton.round(radius: 10)
        self.checkOutButton.isHidden = true
        self.configureCollectionView()
    }
    
    //MARK:- configure tableview
    func configureCollectionView(){
        self.cartCollectionView.registerNib(nibName: CartCollectionViewCell.defaultReuseIdentifier)
        self.cartCollectionView.registerNib(nibName: ApplyPromoceodeCell.defaultReuseIdentifier)
        self.cartCollectionView.registerNib(nibName: CartPriceDetailCell.defaultReuseIdentifier)
        self.cartCollectionView.showsVerticalScrollIndicator = false
        self.cartCollectionView.delegate = self
        self.cartCollectionView.dataSource = self
        self.setEmptyDataView(heading: LocalizedString.Oops_It_Is_Empty.localized, description: LocalizedString.No_Products_Available_In_Your_Cart.localized, image: AppImages.noData.image)
        self.setDataSourceAndDelegate(forScrollView: self.cartCollectionView)
        self.cartControler.getCart()
    }
    
    //MARK:- Delete button tapped
    @objc func deleteProductFromCart(sender : UIButton){
        guard let indexPath = sender.collectionViewIndexPath(self.cartCollectionView) else { return }
        self.displayPopUp(for: CustomPopUpVC.CustomPopUpFor.sureToDeleteProductFromCart, prod: self.products[indexPath.item])
    }
    
    //MARK:- Display popup
    func displayPopUp(for type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none, prod : CartProduct) {
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        vc.prod = prod
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
}

//MARK:- Configure Navigation
private extension CartVC {
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Cart.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
        self.configureNavigationScrolling(scrollView: self.cartCollectionView)
    }
}

//MARK:- CollectionView Datasource and Delegats
extension CartVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.isEmpty ? 0 : self.products.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.item {
            
        case self.products.count:
            
            return CGSize(width: screenWidth , height : 120)
            
        default:
            return CGSize(width: screenWidth , height: 106)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.item {
            
        case self.products.count:
            
            return self.getPriceDetailCell(collectionView, cellForItemAt: indexPath)
            
        default:
            return self.getProductCell(collectionView, cellForItemAt: indexPath)
        }
    }
    
    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CartCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? CartCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        cell.populateData(prod: self.products[indexPath.item])
        cell.deleteButton.addTarget(self, action: #selector(deleteProductFromCart), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    func getPromoCodeCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ApplyPromoceodeCell.defaultReuseIdentifier, for: indexPath) as? ApplyPromoceodeCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        
        return cell
    }
    
    func getPriceDetailCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CartPriceDetailCell.defaultReuseIdentifier, for: indexPath) as? CartPriceDetailCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        cell.populateData(prod: self.products)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}



//MARK:- POP UP DELEGATES
extension CartVC : CustomPopUpDelegateWithType {
    
    func okTapped(type : CustomPopUpVC.CustomPopUpFor, product : CartProduct?){
        
    }
    
    func noButtonTapped(type : CustomPopUpVC.CustomPopUpFor, product : CartProduct?){
        
    }
    
    func yesButtonTapped(type : CustomPopUpVC.CustomPopUpFor, product : CartProduct?){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        switch type {
            
        case .sureToDeleteProductFromCart:
            self.cartControler.deleteSingleProductFromCart(product: product ?? CartProduct())
            
        default:
            break
            
        }
    }
}


//Webservices callback
extension CartVC : GetCartDelegate {
    
    func willRequestCart(){
        self.view.showIndicator()
    }
    
    func cartReceivedSuccessFully(products : [CartProduct]){
        self.view.hideIndicator()
        self.products = products
        self.cartCollectionView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.cartCollectionView.reloadEmptyDataSet()
        if !self.products.isEmpty {
            self.checkOutButton.isHidden = false
        }
        
        if products.count == 1 {
            if let prod = products.first, prod.shippingAvailibility == .shipping {
                checkOutButton.setTitle(LocalizedString.AddShippingDetails.localized)
            }
        }
    }
    
    func errorOccuredWhileReceivingCart(message : String){
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = true
        self.cartCollectionView.reloadEmptyDataSet()
    }
}
//WARK:- Webservice call backs
extension CartVC : DeleteProduct{
    
    func willRequestProductDelete(){
        self.view.showIndicator()
    }
    
    func productDeletedSuccessFully(prod : CartProduct){
        self.view.hideIndicator()
        self.products = self.products.filter { $0.productId != prod.productId }
        self.cartCollectionView.reloadData()
        if self.products.isEmpty {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func errorOccuredWhileDeletingProduct(message : String){
        self.view.hideIndicator()
        
    }
}


extension CartVC : GetCardsDelegate{
    
    func willgetCards(){
        self.view.showIndicator()
        
    }
    
    func cardsReceivedSuccessFully(cards : [Card]){
        self.view.hideIndicator()
//        if cards.isEmpty {
            let vc = TransactionDetaildVC.instantiate(fromAppStoryboard: AppStoryboard.PaymentAndShipping)
            vc.products = self.products
            vc.commingFor = self.commingFor
            var price : Double = 0
            for item in self.products{ price += item.productPrice }
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                vc.view.alpha = 1
            }) { (success) in
                
                vc.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)//self.view.bounds;
                
//                let topViewController = UIApplication.shared.keyWindow?.rootViewController
//                vc.willMove(toParent: topViewController)
//                topViewController?.view.addSubview(vc.view)
//                topViewController?.addChild(vc)
//                vc.didMove(toParent: topViewController)
                
                
                vc.willMove(toParent: self)
                self.view.addSubview(vc.view)
                self.addChild(vc)
                vc.didMove(toParent: self)
                
            }
            
           
            
            
           // self.navigationController?.pushViewController(vc, animated: true)
            
//        }else{
//            let vc = MySavedCardsVC.instantiate(fromAppStoryboard: AppStoryboard.PaymentAndShipping)
//            vc.commingFor = self.commingFor
//            vc.cards = cards
//            vc.products = self.products
//            var price : Double = 0
//            for item in self.products{ price += item.productPrice }
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    func failedToReceiveCards(message : String){
        self.view.hideIndicator()
        
    }
}

