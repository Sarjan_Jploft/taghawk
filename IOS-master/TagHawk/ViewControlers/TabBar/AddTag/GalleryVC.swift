//
//  GalleryVC.swift
//  Surface
//
//  Created by Surface on 12/07/18.
//  Copyright © 2018 Surface. All rights reserved.
//

import UIKit
import Photos
import AVKit


struct Media : Equatable {
    let asset : PHAsset
    var zoomScale : CGFloat?
    var contentOffset : CGPoint?
    var image : UIImage!
    
    static func ==(lhs:Media,rhs:Media) -> Bool {
        return lhs.asset == rhs.asset
    }
}

protocol GalleryDelegate {
    func passMediaObject(media: [Media])
}

class GalleryVC: BaseVC {
   
    //MARK:- PROPERTIES
    //=================
    var uploadFor = UploadDocumentFor.joinTag
    let numberOfRows: CGFloat = 3
    let sideMargin  : CGFloat = 2
    
    var isImageSelected: Bool = false
    var imageArray = [UIImage]()
//    var isInititaedFromPayment: Bool = false
    
    var mediaDelegate: GalleryDelegate?
    
    let cachingImageManager = PHCachingImageManager()
    var assets: [PHAsset] = [] {
        willSet {
            cachingImageManager.stopCachingImagesForAllAssets()
        }
        didSet {
            cachingImageManager.startCachingImages(for: self.assets,
                                                   targetSize: PHImageManagerMaximumSize,
                                                   contentMode: .aspectFill,
                                                   options: nil
            )
        }
    }
    
    var selectedMediaArray : [Media] = []
    
    var selectedMedia : Media? = nil {
        willSet(newValue) {
            if let selectedMedia = newValue{
                let manager = PHImageManager.default()
                DispatchQueue.main.async {
                    self.galleryCollection.allowsSelection = false
                }
                manager.requestImage(for: selectedMedia.asset,
                                     targetSize: PHImageManagerMaximumSize,
                                     contentMode: .aspectFill,
                                     options: nil) {[weak self] (result, _) in
                                        DispatchQueue.main.async {
                                            self?.galleryCollection.allowsSelection = true
                                        }
                }
            }
        }
    }
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var galleryCollection: UICollectionView!
    
    @IBAction func clickCancelButton(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickDoneButton(_ sender: UIButton) {
        view.endEditing(true)
        
        if self.uploadFor == .uploadPassport && self.selectedMediaArray.count < 2{
            CommonFunctions.showToastMessage(LocalizedString.Please_Select_Both_First_And_Last.localized)
            return
        }
        
        self.mediaDelegate?.passMediaObject(media: self.selectedMediaArray)
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- VIEW LIFE CYCLE
//======================
extension GalleryVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    
}

//MARK:- PRIVATE FUNCTIONS
//========================
extension GalleryVC {
    
    private func initialSetup(){
        configureCollectionView()
        fetchAssestsFromPhotoLibrary()
    }
    
    private func configureCollectionView(){
        self.galleryCollection.register(UINib(nibName: "PhotoCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCollectionCell")
        galleryCollection.dataSource = self
        galleryCollection.delegate      = self
        galleryCollection.allowsMultipleSelection = false
    }

    private func fetchAssestsFromPhotoLibrary(){
        
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status
            {
            case .restricted:
                print("restricted")
                //self.alertPromptToAllowAccessViaSetting("AppMessages.Youve_been_restricted_from_using_the_library_on_this_device_Without_camera_access_this_feature_wont_work.localized")
                
            case .denied:
                print("denied")
                //self.alertPromptToAllowAccessViaSetting("AppMessages.Please_change_your_privacy_setting_from_the_Settings_app_and_allow_access_to_library_for.localized")
                
            case .authorized,.notDetermined:

                let fetchOptions = PHFetchOptions()
                let allPhotos = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                
                let allVideos = PHAsset.fetchAssets(with: .video, options: fetchOptions)
                
                if allPhotos.count != 0{
                    self.assets = allPhotos.objects(at: IndexSet(0...(allPhotos.count - 1))).reversed()
                }
                
                if allVideos.count != 0 {
                    self.assets.append(contentsOf: allVideos.objects(at: IndexSet(0...(allVideos.count - 1))).reversed())
                }
                
                self.assets.sort(by: { (asset1, asset2) -> Bool in
                    return asset1.creationDate! > asset2.creationDate!
                })

                print("Found \(self.assets.count) images")
                
                DispatchQueue.main.async {
                    self.galleryCollection.reloadData()
                }
            }
        }
    }
}

//MARK:- IBACTIONS
//================
extension GalleryVC {
    
}

//MARK:- CollectionView DataSource & Delegates
//============================================
extension GalleryVC: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionCell", for: indexPath) as? PhotoCollectionCell else {
            fatalError("PhotoCollectionCell is not found")
        }

        let asset = assets[indexPath.item]
        if asset.mediaType == .video {
            cell.videoImage.isHidden = false
        }else {
            cell.videoImage.isHidden = true
        }

        let manager = PHImageManager.default()
        
        manager.requestImage(for: asset,
                             targetSize: CGSize(width: collectionView.frame.width/3,
                                                height: collectionView.frame.width/3),
                             contentMode: .aspectFit,
                             options: nil) { (result, _) in
                                cell.mainImageView.isUserInteractionEnabled = true
                                cell.mainImageView.image = result
                                if self.selectedMediaArray.contains(where: {$0.asset == asset}) {
//
                                    cell.mainImageView.alpha = 0.5
                                    cell.selectedImage.isHidden = false
                                    
                                } else {
                                    cell.mainImageView.alpha = 1
                                    cell.selectedImage.isHidden = true
                                }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let media = self.selectedMediaArray.first(where: {$0.asset == assets[indexPath.item]}){
            if let selectedMedia = selectedMedia, selectedMedia.asset == assets[indexPath.item] {
                self.selectedMediaArray.removeObject(media)
                if let lastItem = self.selectedMediaArray.last {
                    self.selectedMedia = lastItem
                }else if !assets.isEmpty {
                    self.selectedMedia = Media(asset: assets[0], zoomScale: nil, contentOffset: nil, image: nil)
                }
            } else {
                self.selectedMedia = media
            }
        } else {
            
            if self.uploadFor == .uploadPassport {
                if selectedMediaArray.count >= 2 {
                    //showToastMessage("AppMessages.PickImageValidation.localized")
                    return
                }
            }else{
                if selectedMediaArray.count >= 5 {
                    //showToastMessage("AppMessages.PickImageValidation.localized")
                    return
                }
            }
            
            
            if assets[indexPath.item].mediaType == .video {
                if selectedMediaArray.contains(where: {$0.asset.mediaType == .video}) {
                  //  showToastMessage("AppMessages.PickImageValidation.localized)"
                    return
                }
            }

            let media = Media(asset: assets[indexPath.item], zoomScale: nil, contentOffset: nil, image: nil)
            self.selectedMediaArray.append(media)
            self.selectedMedia = media
            self.isImageSelected = true
           
        }
        collectionView.reloadItems(at: [indexPath])
    }
    
    /// Size for Item
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let bothSideMargin = sideMargin*2
        let interSpaces = (sideMargin/2)*((numberOfRows-1)*2)
        let totalSpaces = bothSideMargin+interSpaces
        
        let mainWidth = UIScreen.main.bounds.width - totalSpaces
        let width = mainWidth/numberOfRows
        
        return CGSize(width: width, height: width)
    }
    
    /// Edge Insets for Section
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 5,
                            left: sideMargin,
                            bottom: 5,
                            right: sideMargin)
    }
    
    /// Minimum Inter Item Spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return sideMargin/2
    }
    
    /// Minimum Line Spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return sideMargin
    }

}

//MARK :- CROPPER FUNCTIONS
//=========================
extension GalleryVC {
   

    
//    func processMedia(completion: @escaping ([UIImage],AVAsset?)->()){
//
//        let images = selectedMediaArray.filter({$0.asset.mediaType == .image}).filter({$0.image != nil}).map { (media) -> UIImage in
//            return cropImage(image: media.image, zoomScale: media.zoomScale, contentOffset: media.contentOffset ?? CGPoint.zero)
//        }
//        if let video = selectedMediaArray.filter({$0.asset.mediaType == .video}).first {
//            self.cachingImageManager.requestAVAsset(forVideo: video.asset, options: nil) { (avAsset, _, _) in
//                if let aAsset = avAsset {
//                    DispatchQueue.main.async {
//                        completion(images,aAsset)
//                    }
//                }
//            }
//        }else{
//            completion(images,nil)
//        }
//    }
}

//MARK:- Cell Class
//=================
class ImageCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var videoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.contentMode = .scaleAspectFill
    }
    
}
