//
//  TagPaymentTypePopup.swift
//  TagHawk
//
//  Created by Appinventiv on 16/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol TagPaymentTypePopupDelegate : class {
    func getSelectedTagpaymentType(type : TagPaymentTypePopup.PayForTagUsing)
}

class TagPaymentTypePopup : BaseVC {

    enum PayForTagUsing : String {
        case credittPoints = "POINT"
        case inApp = "PAYMENT"
        case none = ""
    }
    
    //MARK:- PROPERTIES
    //=================
    
    weak var delegate: TagPaymentTypePopupDelegate?
    
    //MARK:- IBOUTLETS
    //================
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var dismisView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    
    var createtagUsing = PayForTagUsing.credittPoints
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popupView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- TARGET ACTIONS
    //=====================
    
    @IBAction func leftBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.getSelectedTagpaymentType(type: TagPaymentTypePopup.PayForTagUsing.credittPoints)
        }
    }
    
    @IBAction func rightBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.getSelectedTagpaymentType(type: TagPaymentTypePopup.PayForTagUsing.inApp)
        }
    }
    
}


//MARK:- PRIVATE FUNCTIONS
//========================
extension TagPaymentTypePopup {
    
    private func initialSetup(){
        self.titleLbl.setAttributes(text: LocalizedString.SelectPaymentMode.localized , font: AppFonts.Galano_Semi_Bold.withSize(17.5), textColor: UIColor.black)
        
        
        self.dismisView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.backgroundColor = UIColor.clear
        
        
        self.leftBtn.round(radius: 10)
        self.rightBtn.round(radius: 10)
        
        self.leftBtn.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.rightBtn.setBorder(width: 0.5, color: AppColors.textfield_Border)
        
        self.leftBtn.setAttributes(title: LocalizedString.Points.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
        
        self.rightBtn.setAttributes(title: LocalizedString.Payment.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)

        let tap = UITapGestureRecognizer(target: self, action: #selector(dismisViewTapped))
        self.dismisView.addGestureRecognizer(tap)
        self.dismisView.isUserInteractionEnabled = true
    }
    
    @objc func dismisViewTapped(){
        self.dismiss(animated: true) {
            
        }
    }
    
}
