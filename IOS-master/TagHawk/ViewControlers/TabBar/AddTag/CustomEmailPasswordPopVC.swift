//
//  CustomEmailPasswordPopVC.swift
//  TagHawk
//
//  Created by Vikash on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol CustomEmailPasswordPopVCDelegate: class {
    
    func suusscefullyAddTag(isEmail: Bool)
}

class CustomEmailPasswordPopVC: UIViewController, JoinTagDelegate {
   
    //MARK: - Variables
    var isEmailVerify: Bool = true
    var tagData = Tag()
    weak var delegate: CustomEmailPasswordPopVCDelegate?
    
    //MARK: - IBOutlets
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var gmailLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var popupView: UIView!
    
    var controller = JoinTagController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.doInitialSetup()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popupView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    private func doInitialSetup() {
        textField.returnKeyType = .done
        textField.delegate = self
        self.gmailLabel.isHidden = self.isEmailVerify ? false : true
        self.textField.placeholder = self.isEmailVerify ? "Enter Email" : "Enter Password"
       // self.dismissView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.backgroundColor = UIColor.clear
        self.gmailLabel.text = "@" + self.tagData.email
        var descriptionText = "You are applying for " + "\(self.tagData.name)" + " Please enter your "
        descriptionText = self.isEmailVerify ? descriptionText + "Email" : descriptionText + "Password"
        self.descriptionLabel.text = descriptionText
        self.controller.delegate = self
    }
    
    @IBAction func clickCancelButton(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickApplyButton(_ sender: UIButton) {
        view.endEditing(true)
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if !(textField.text?.isEmpty)! {
            if self.isEmailVerify {
                let email = (self.textField.text ?? "") + "@" + self.tagData.email
                self.controller.addTagService(addTag: self.tagData, emailPassword: email, joinTagBy: PrivateTagType.email.rawValue, documents: [String]())
            } else {
                 self.controller.addTagService(addTag: self.tagData, emailPassword: self.textField.text!, joinTagBy: PrivateTagType.password.rawValue, documents: [String]())
            }
        }
        
    }
    
    func willJoinTag() {
        self.view.showIndicator()
    }
    
    func joinTagListFailure(errorType: ApiState, message: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
    
    func joinTagListServiceReturn(msg: String, tagType: PrivateTagType) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
        self.delegate?.suusscefullyAddTag(isEmail: isEmailVerify)
        self.dismiss(animated: true, completion: nil)
    }
}

extension CustomEmailPasswordPopVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
