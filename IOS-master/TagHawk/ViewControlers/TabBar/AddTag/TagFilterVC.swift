//
//  TagFilterVC.swift
//  TagHawk
//
//  Created by Appinventiv on 13/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import AMScrollingNavbar

class TagFilterVC : BaseVC {
    
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var applyButton: UIButton!

    //MARK:- Variable
    weak var delegate : ApplyFilter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func bindControler() {
        super.bindControler()
    }
    
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func rightBarButtonTapped(_ sender: UIBarButtonItem) {
        super.rightBarButtonTapped(sender)
        SelectedFilters.shared.resetTagFilters()
        self.delegate?.applyFilter()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
       
        if SelectedFilters.shared.selectedFilterOnTag.lat == 0{
            SelectedFilters.shared.selectedFilterOnTag.lat = LocationManager.shared.latitude
            SelectedFilters.shared.selectedFilterOnTag.long = LocationManager.shared.longitude
        }
       
        SelectedFilters.shared.copySelectedTagToAppliedForTagFilter()
        self.delegate?.applyFilter()
        self.navigationController?.popViewController(animated: true)
    }
}

private extension TagFilterVC {
    
    func setUpSubView(){
        SelectedFilters.shared.copyAppliedToSelectedForTagFilter()
        self.configureTableView()
    }
    
    func configureTableView(){
        self.filterTableView.registerNib(nibName: DropDownTableViewCell.defaultReuseIdentifier)
        self.filterTableView.registerNib(nibName: DistanceCell.defaultReuseIdentifier)
        self.filterTableView.registerNib(nibName: FilterMapCell.defaultReuseIdentifier)
        self.filterTableView.separatorStyle = .none
        self.filterTableView.delegate = self
        self.filterTableView.dataSource = self
    }
}


private extension TagFilterVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Filter.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image , ofVC: self)
        self.rightBarTitle(title: LocalizedString.Reset.localized, ofVC: self)
        
        if let navController = self.navigationController as? ScrollingNavigationController, let tabBar = AppNavigator.shared.tabBar?.tabBar {
            navController.followScrollView(self.filterTableView  , delay: 0, scrollSpeedFactor: 2, followers: [NavigationBarFollower(view: tabBar, direction: .scrollDown)])
            navController.scrollingNavbarDelegate = self
            navController.expandOnActive = false
        }
    }
}


extension TagFilterVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
            
        case 0:
            return 340
            
        default:
            
            return 115
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.getFilterMapCell(tableView, indexPath: indexPath)
            
        case 1:
            
             return self.getDistanceCell(tableView, indexPath: indexPath)
           
        default:
            return self.getDropDownCell(tableView, indexPath: indexPath)
        }
    }
    
    
    func getDistanceCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DistanceCell.defaultReuseIdentifier) as? DistanceCell else {
            fatalError("FilterVC....\(DistanceCell.defaultReuseIdentifier) cell not found")
        }
        
        cell.distanceSelectionFor = .tagFilter
        cell.setSelectedIndexInSlider()
      
        return cell
    }
    
    func getFilterMapCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterMapCell.defaultReuseIdentifier) as? FilterMapCell else {
            fatalError("FilterVC....\(FilterMapCell.defaultReuseIdentifier) cell not found")
        }
        cell.setUpFor = .tagFilter
        cell.delegate = self
       
        if SelectedFilters.shared.selectedFilterOnTag.lat == 0.0{
            cell.populateData(addressStr: SelectedFilters.shared.selectedFilterOnTag.locationStr, lat: LocationManager.shared.latitude, long: LocationManager.shared.longitude)
        }else{
            cell.populateData(addressStr: SelectedFilters.shared.selectedFilterOnTag.locationStr, lat: SelectedFilters.shared.selectedFilterOnTag.lat, long: SelectedFilters.shared.selectedFilterOnTag.long)
        }
        
        return cell
    }
    
    func getDropDownCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DropDownTableViewCell.defaultReuseIdentifier) as? DropDownTableViewCell else { fatalError("FilterVC....\(DropDownTableViewCell.defaultReuseIdentifier) cell not found") }
        
        switch indexPath.row {
       
        case 2:
            cell.setUpFor = .tagType
            cell.tagTypeDelegate = self
           
//            let labelText = SelectedFilters.shared.selectedFilterOnTag.tagType == .all ? LocalizedString.Select.localized : SelectedFilters.shared.getTagTypeStringValue(from: SelectedFilters.shared.selectedFilterOnTag.tagType).rawValue
            
//          let labelText = SelectedFilters.shared.getTagTypeStringValue(from: SelectedFilters.shared.selectedFilterOnTag.tagType).rawValue
            
            let labelText = SelectedFilters.shared.getEntraceTypeStringValue(from: SelectedFilters.shared.selectedFilterOnTag.tagEntrance).rawValue
            
            let labelColor = SelectedFilters.shared.selectedFilterOnTag.tagType == .all ? AppColors.lightGreyTextColor : UIColor.black
            cell.setUpDropDownLabel(labelText: labelText, withColor: labelColor)

        default:
            cell.setUpFor = .tagName
            
        }
    
        return cell
    }
}

extension TagFilterVC : MapTextFieldTappedDelegate {
    
    func getSelectedLocation(lat: Double, long: Double, address: String, city: String, state: String) {
        
    }
    
    func mapFieldTapped(){
        let vc = SelectLocationVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
}

extension TagFilterVC : GetSelectedTagType {
    
    func getSelectedTagType(type : SelectedFilters.TagType) {
        SelectedFilters.shared.selectedFilterOnTag.tagType = type
        
        guard let cell = self.filterTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? DropDownTableViewCell else { return }
        
        let labelText = SelectedFilters.shared.selectedFilterOnTag.tagType == .none ? LocalizedString.Select.localized : SelectedFilters.shared.getTagTypeStringValue(from: SelectedFilters.shared.selectedFilterOnTag.tagType).rawValue
        
        let labelColor = SelectedFilters.shared.selectedFilterOnTag.tagType == .none ? AppColors.lightGreyTextColor : UIColor.black
        
        cell.setUpDropDownLabel(labelText: labelText, withColor: labelColor)
        
    }
    
}

extension TagFilterVC : GedAddressBack {
    
    func getAddress(lat : Double, long : Double,addressString:String){
        SelectedFilters.shared.selectedFilterOnTag.locationStr = addressString
        SelectedFilters.shared.selectedFilterOnTag.lat = lat
        SelectedFilters.shared.selectedFilterOnTag.long = long
        DispatchQueue.main.async {
            self.filterTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
}

