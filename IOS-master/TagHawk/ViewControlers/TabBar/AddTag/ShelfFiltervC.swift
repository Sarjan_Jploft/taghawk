//
//  ShelfFiltervC.swift
//  TagHawk
//
//  Created by Admin on 5/15/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import GooglePlaces
import AMScrollingNavbar


class ShelfFiltervC : BaseVC {
    
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var verifySellorButton: UIButton!
    @IBOutlet weak var verifiedSellorTickImageView: UIImageView!
    @IBOutlet weak var verifiedSellorLabel: UILabel!
    
    //MARK:- Variable
    weak var delegate : ApplyFilter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func bindControler() {
        super.bindControler()
        
    }
    
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func rightBarButtonTapped(_ sender: UIBarButtonItem) {
        super.rightBarButtonTapped(sender)
        SelectedFilters.shared.resetShelfFilter()
        self.delegate?.applyFilter()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if SelectedFilters.shared.selectedFilterOnShelf.lat == 0{
            SelectedFilters.shared.selectedFilterOnShelf.lat = LocationManager.shared.latitude
            SelectedFilters.shared.selectedFilterOnShelf.long = LocationManager.shared.longitude
        }
    
        SelectedFilters.shared.copySelectedShelfToAyylyedFilter()
        self.delegate?.applyFilter()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func verifiyButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.verifiedSellorTickImageView.image =  SelectedFilters.shared.selectedFilterOnShelf.verifiedSellorsOnly ? AppImages.unTickWithSquare.image : AppImages.tickWithSquare.image
        SelectedFilters.shared.selectedFilterOnShelf.verifiedSellorsOnly = !SelectedFilters.shared.selectedFilterOnShelf.verifiedSellorsOnly
    }
}


private extension ShelfFiltervC{
    
    func setUpSubView(){
        self.verifiedSellorTickImageView.image =  SelectedFilters.shared.selectedFilterOnShelf.verifiedSellorsOnly ? AppImages.tickWithSquare.image : AppImages.unTickWithSquare.image
        SelectedFilters.shared.copyAppliedShelfFiltersToCopiedFilter()
        self.configureTableView()
    }
    
    func configureTableView(){
        self.filterTableView.registerNib(nibName: PriceRangeCell.defaultReuseIdentifier)
        self.filterTableView.registerNib(nibName: DropDownTableViewCell.defaultReuseIdentifier)
        self.filterTableView.registerNib(nibName: ConditionCell.defaultReuseIdentifier)
        self.filterTableView.registerNib(nibName: DistanceCell.defaultReuseIdentifier)
        self.filterTableView.registerNib(nibName: FilterMapCell.defaultReuseIdentifier)
        self.filterTableView.registerNib(nibName: SellarRatingCell.defaultReuseIdentifier)
        self.filterTableView.separatorStyle = .none
        self.filterTableView.delegate = self
        self.filterTableView.dataSource = self
    }
    
}

private extension ShelfFiltervC{
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.Filter.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
        self.rightBarTitle(title: LocalizedString.Reset.localized, ofVC: self)
        
        if let navController = self.navigationController as? ScrollingNavigationController, let tabBar = AppNavigator.shared.tabBar?.tabBar {
            
            navController.followScrollView(self.filterTableView  , delay: 0, scrollSpeedFactor: 2, followers: [NavigationBarFollower(view: tabBar, direction: .scrollDown)])
            
            navController.scrollingNavbarDelegate = self
            navController.expandOnActive = false
        }
    }
}



extension ShelfFiltervC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
            
        case 0:
            return 340
            
        case 1, 2, 3:
            return 115
            
        case 4:
            return 181
            
        case 5:
            return 105
        default:
            return 120
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.getFilterMapCell(tableView, indexPath: indexPath)
            
        case 1:
            return self.getDistanceCell(tableView, indexPath: indexPath)
            
        case 2:
            return self.getPriceRangeCell(tableView, indexPath: indexPath)
            
        case 3:
            return self.getDropDownCell(tableView, indexPath: indexPath)
            
        case 4:
            return self.getDropConditionCell(tableView, indexPath: indexPath)
            
        case 5:
            return self.getSellorRatingCell(tableView, indexPath: indexPath)
            
        default:
            return self.getDropDownCell(tableView, indexPath: indexPath)
        }
    }
    
    func getPriceRangeCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PriceRangeCell.defaultReuseIdentifier) as? PriceRangeCell else {
            fatalError("FilterVC....\(PriceRangeCell.defaultReuseIdentifier) cell not found")
        }
        cell.setupFor = .shelfFilter
        return cell
    }
    
    func getDropDownCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DropDownTableViewCell.defaultReuseIdentifier) as? DropDownTableViewCell else {
            fatalError("FilterVC....\(DropDownTableViewCell.defaultReuseIdentifier) cell not found")
        }
        
        cell.setUpFor = .postedWithin
        cell.postedWithInDelegate = self
        
        let labelText = SelectedFilters.shared.selectedFilterOnShelf.postedWithin == .none ? LocalizedString.Select.localized : SelectedFilters.shared.getPostedWithInString(postedWithin: SelectedFilters.shared.selectedFilterOnShelf.postedWithin)
        
        let labelColor = SelectedFilters.shared.selectedFilterOnShelf.postedWithin == .none ? AppColors.lightGreyTextColor : UIColor.black
        
        cell.setUpDropDownLabel(labelText: labelText, withColor: labelColor)
        
        return cell
    }
    
    func getDropConditionCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ConditionCell.defaultReuseIdentifier) as? ConditionCell else {
            fatalError("FilterVC....\(ConditionCell.defaultReuseIdentifier) cell not found")
        }
        cell.setUpFor = .shelfilter
        
        return cell
    }
    
    func getDistanceCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DistanceCell.defaultReuseIdentifier) as? DistanceCell else {
            fatalError("FilterVC....\(DistanceCell.defaultReuseIdentifier) cell not found")
        }
        cell.distanceSelectionFor = .shelfFilter
        cell.setSelectedIndexInSlider()
        return cell
    }
    
    func getFilterMapCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterMapCell.defaultReuseIdentifier) as? FilterMapCell else {
            fatalError("FilterVC....\(FilterMapCell.defaultReuseIdentifier) cell not found")
        }
        
        cell.delegate = self
        cell.populateData(addressStr: SelectedFilters.shared.selectedFilterOnShelf.locationStr, lat: SelectedFilters.shared.selectedFilterOnShelf.lat, long: SelectedFilters.shared.selectedFilterOnTag.long)
    
        if SelectedFilters.shared.selectedFilterOnShelf.lat == 0.0{
            cell.populateData(addressStr: SelectedFilters.shared.selectedFilterOnShelf.locationStr, lat: LocationManager.shared.latitude, long: LocationManager.shared.longitude)
        }else{
            cell.populateData(addressStr: SelectedFilters.shared.selectedFilterOnShelf.locationStr, lat: SelectedFilters.shared.selectedFilterOnShelf.lat, long: SelectedFilters.shared.selectedFilterOnShelf.long)
        }
        
        
        return cell
    }
    
    func getSellorRatingCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SellarRatingCell.defaultReuseIdentifier) as? SellarRatingCell else { fatalError("FilterVC....\(SellarRatingCell.defaultReuseIdentifier) cell not found") }
        cell.setUpFor = .shelfFilter
        return cell
    }
}

extension ShelfFiltervC : GetPostedWithinDelegate {
    func getPostedWithIn(postedWithin: SelectedFilters.PostedWithin) {
        SelectedFilters.shared.selectedFilterOnShelf.postedWithin = postedWithin
        
        guard let cell = self.filterTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? DropDownTableViewCell else { return }
        let labelText = SelectedFilters.shared.selectedFilterOnShelf.postedWithin == .none ? LocalizedString.Select.localized : SelectedFilters.shared.getPostedWithInString(postedWithin: SelectedFilters.shared.selectedFilterOnShelf.postedWithin)
        
        let labelColor = SelectedFilters.shared.selectedFilterOnShelf.postedWithin == .none ? AppColors.lightGreyTextColor : UIColor.black
        
        cell.setUpDropDownLabel(labelText: labelText, withColor: labelColor)
        
    }
}

extension ShelfFiltervC : MapTextFieldTappedDelegate {
    
    func getSelectedLocation(lat: Double, long: Double, address: String, city: String, state: String) {
        
    }
    
    func mapFieldTapped(){
        let vc = SelectLocationVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
}

extension ShelfFiltervC : GedAddressBack {
    
    func getAddress(lat : Double, long : Double,addressString:String) {
        SelectedFilters.shared.selectedFilterOnShelf.locationStr = addressString
        SelectedFilters.shared.selectedFilterOnShelf.lat = lat
        SelectedFilters.shared.selectedFilterOnShelf.long = long
        DispatchQueue.delay(0.3) {
            self.filterTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.automatic)
        }
    }
}

