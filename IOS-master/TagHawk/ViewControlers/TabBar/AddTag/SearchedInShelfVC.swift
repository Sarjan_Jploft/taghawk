//
//  SearchedInShelfVC.swift
//  TagHawk
//
//  Created by Admin on 5/16/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//


import UIKit
import DropDown

class SearchedInShelfVC: BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var searchProductCollectionView: UICollectionView!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var sortingButton: UIButton!
    @IBOutlet weak var sortingTypeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var categoryButton: UIButton!
    
    
    //MARK:- Variables
    private var isItemSelected = true
    private var products : [Product] = []
    var searchKeyWord : String = "hello"
    private let controller = TagProductsController()
    private var recognizer : UITapGestureRecognizer?
    var titleView = UILabel()
    private let dropDown = DropDown()
    var tagId : String = ""
    var tagName : String = ""
    private let searchDelegate = SearchedProductsControler()
    
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controller.delegate = self
        self.searchDelegate.likeDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
        self.setSortingText()
    }
    
    //MARK:- Back button Tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        SelectedFilters.shared.appliedFiltersOnItems = FilterValues()
        guard let vcs = self.navigationController?.viewControllers else { return }
        self.refreshTagShelf()
        for item in vcs {
            if item.isKind(of: TagDetailVC.self){
                SelectedFilters.shared.resetItemsFilter()
                self.navigationController?.popToViewController(item, animated: true)
            }else if item.isKind(of: TagShelfVC.self){
                SelectedFilters.shared.resetItemsFilter()
                self.navigationController?.popToViewController(item, animated: true)
            }
        }
    }
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = FilterVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }

    @IBAction func searchButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = SearchVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.searchFrom = .searchProducts
        vc.searchStr = ""
        vc.selectedCategory = SelectedFilters.shared.appliedCategoryInShelf
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func categoryButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = CategoryVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        vc.selectionFrom = .searchProducts
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func cartButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = CartVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Private functions
private extension SearchedInShelfVC{
    
    func setUpSubView(){
        
        self.sortingTypeLabel.setAttributes(text: LocalizedString.Newest.localized, font: AppFonts.Galano_Medium.withSize(13), textColor: UIColor.black)
        self.configureCollectionView()
        
        self.controller.getTagProducts(self.tagId , pageNo: self.page, categoryId: SelectedFilters.shared.appliedCategoryInShelf.categoryId, searchText: self.searchKeyWord, currentProducts: self.products)
        
        self.searchLabel.setAttributes(text: LocalizedString.Search.localized, font: AppFonts.Galano_Regular.withSize(12) , textColor: AppColors.lightGreyTextColor)
        
        self.titleLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleTapped))
        titleLabel.isUserInteractionEnabled = true
        titleLabel.addGestureRecognizer(tap)
        
        //        self.titleLabel.isHidden = searchKeyWord.isEmpty
        //        self.searchBackView.isHidden = !searchKeyWord.isEmpty
        //
        //        self.titleLabel.text = self.searchKeyWord
        self.searchBackView.isHidden = false
        self.titleLabel.isHidden = false
        self.setSearchLabelText()
        
        //        if !self.searchKeyWord.isEmpty{
        //            self.searchLabel.text = self.searchKeyWord
        //        }else if !self.selectedCategory.description.isEmpty{
        //            self.searchLabel.text =  "\(LocalizedString.Search_In.localized) \(self.selectedCategory.description)"
        //        }else{
        //          self.searchLabel.text = LocalizedString.Search.localized
        //        }
        
        
        
        //        self.searchLabel.text = self.selectedCategory.description.isEmpty ? LocalizedString.Search.localized : "\(LocalizedString.Search_In.localized) \(self.selectedCategory.description)"
        
    }
    
    func isSearchApplied() -> Bool {
        return !self.searchKeyWord.isEmpty
    }
    
    func setSearchLabelText(){
        if !self.searchKeyWord.isEmpty && !SelectedFilters.shared.appliedCategoryInShelf.description.isEmpty{
            self.searchLabel.attributedText = "\(self.searchKeyWord) in \(SelectedFilters.shared.appliedCategoryInShelf.description)".attributeStringWithAnotherColor(stringToColor: self.searchKeyWord)
            
            //            self.searchLabel.text = "\(self.searchKeyWord) in \(self.selectedCategory.description)"
        }else if !self.searchKeyWord.isEmpty && SelectedFilters.shared.appliedCategoryInShelf.description.isEmpty{
            //            self.searchLabel.text = self.searchKeyWord
            
            self.searchLabel.attributedText = "\(self.searchKeyWord) in \(self.tagName)".attributeStringWithAnotherColor(stringToColor: self.searchKeyWord)
            
        }else if self.searchKeyWord.isEmpty && !SelectedFilters.shared.appliedCategoryInShelf.description.isEmpty{
            self.searchLabel.text = "\(LocalizedString.Search.localized) in \(SelectedFilters.shared.appliedCategoryInShelf.description)"
        }
    }
    
    //MARK:- Configure Collectionview
    func configureCollectionView(){
        self.searchProductCollectionView.registerNib(nibName: SearchedProduceCollectionViewCell.defaultReuseIdentifier)
        self.searchProductCollectionView.registerNib(nibName: ProductCollectionViewCell.defaultReuseIdentifier)
        self.searchProductCollectionView.register(UINib(nibName: "HomeHeaserView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeHeaserView.reuseIdentifier)
        self.searchProductCollectionView.showsHorizontalScrollIndicator = false
        self.searchProductCollectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.searchProductCollectionView)
        self.searchProductCollectionView.delegate = self
        self.searchProductCollectionView.dataSource = self
    }
    
    func setUpSortingDropDown(){
        
    }
}

//MARK:- Configure Navigation
private extension SearchedInShelfVC {
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //   self.title = self.searchKeyWord
        //        initNavigationItemTitleView(title: self.searchKeyWord)
        //        self.configureLeftBarButtons()
        //        self.configureRightBarButton()
    }
    
    func configureLeftBarButtons() {
        let leftButton = UIBarButtonItem(image: AppImages.backBlack.image , style: .plain, target: self, action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    
    func initNavigationItemTitleView(title : String) {
        titleView.text = title
        titleView.font = AppFonts.Galano_Medium.withSize(15)
        titleView.textColor = UIColor.black
        let width = titleView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)).width
        titleView.frame = CGRect(origin:CGPoint.zero, size:CGSize(width: width, height: 500))
        self.navigationItem.titleView = titleView
        recognizer = UITapGestureRecognizer(target: self, action: #selector(titleTapped))
        titleView.isUserInteractionEnabled = true
        titleView.removeGestureRecognizer(recognizer ?? UITapGestureRecognizer())
        titleView.addGestureRecognizer(recognizer ?? UITapGestureRecognizer())
    }
    
    @objc private func titleTapped() {
        let vc = SearchVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.searchFrom = .searchProducts
        vc.searchStr = self.searchKeyWord
        vc.selectedCategory = SelectedFilters.shared.appliedCategoryInShelf
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func likeButtonTapped(sender : UIButton){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        guard let indexPath = sender.collectionViewIndexPath(self.searchProductCollectionView) else { return }
        
        self.searchDelegate.likeUnlikeProduct(product: self.products[indexPath.item])
        self.products[indexPath.item].isLiked =  !self.products[indexPath.item].isLiked
        self.searchProductCollectionView.reloadItems(at: [indexPath])
    }
    
    @IBAction func sortingButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
        self.dropDown.dismissMode = .automatic
        
        self.dropDown.dataSource = [LocalizedString.Newest.localized, LocalizedString.Closest.localized, LocalizedString.Price_High_To_Low.localized, LocalizedString.Price_Low_To_High.localized]
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            
            guard let weakSelf = self else { return }
            
            switch index {
                
            case 0:
                SelectedFilters.shared.appliedSortingOnProducts = .newest
                
            case 1:
                SelectedFilters.shared.appliedSortingOnProducts = .closest
                
            case 2:
                SelectedFilters.shared.appliedSortingOnProducts = .priceHightToLow
                
            case 3:
                SelectedFilters.shared.appliedSortingOnProducts = .priceLowToHigh
                
            default:
                SelectedFilters.shared.appliedSortingOnProducts = .newest
            }
            
            weakSelf.setSortingText()
            weakSelf.products.removeAll()
            weakSelf.searchProductCollectionView.reloadData()
            weakSelf.resetPage()
            weakSelf.controller.getTagProducts(weakSelf.tagId, pageNo: weakSelf.page, categoryId: SelectedFilters.shared.appliedCategoryInShelf.categoryId, searchText: weakSelf.searchKeyWord, currentProducts: weakSelf.products)
            
        }
    }
    
    func setSortingText(){
        self.sortingTypeLabel.text = SelectedFilters.shared.setSortingAppliedText(sorting: SelectedFilters.shared.appliedSortingOnProducts)
    }
    
    func getSizeWhenSearchingApplied(_ collectionView: UICollectionView) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 10
        return CGSize(width: finalContentWidth / 2, height: (finalContentWidth / 2) + 47)
    }
    
    func getSizeWhenSearchingNotApplied(_ collectionView: UICollectionView) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 2
        return CGSize(width: finalContentWidth / 3, height: finalContentWidth / 3)
    }
    
}

//MARK:- CollectionView Datasource and Delegats
extension SearchedInShelfVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HomeHeaserView", for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return self.isSearchApplied() ? CGSize(width: collectionView.frame.width, height: 0) : CGSize(width: collectionView.frame.width, height: 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.isSearchApplied() ? 10 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return self.isSearchApplied() ? 10 : 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return self.isSearchApplied() ? self.getSizeWhenSearchingApplied(collectionView) : self.getSizeWhenSearchingNotApplied(collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return self.isSearchApplied() ? self.getProductCellWhenSearchingApplied(collectionView, cellForItemAt: indexPath) : self.getProductCellWhenSearchingNotApplied(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductCellWhenSearchingNotApplied(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ProductCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        cell.cellFor = .home
        cell.populateData(product: self.products[indexPath.item])
        return cell
    }
    
    func getProductCellWhenSearchingApplied(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchedProduceCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? SearchedProduceCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        cell.dropShadow()
        cell.populateData(product: self.products[indexPath.item])
        cell.likeButton.addTarget(self, action: #selector(likeButtonTapped), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.products.count >= 9  {
            if indexPath.item == self.products.count - 1 && self.nextPage != 0{
                self.controller.getTagProducts(self.tagId, pageNo: self.page, categoryId: SelectedFilters.shared.appliedCategoryInShelf.categoryId, searchText: self.searchKeyWord, currentProducts: self.products)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ProductDetailVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.prodId = self.products[indexPath.item].productId
        vc.likeStatusBackDeleate = self
        vc.prodReportedDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SearchedInShelfVC : SelectedCategory  {
    
    func selectCategory(category: Category) {
        SelectedFilters.shared.appliedCategoryInShelf = category
        self.setSearchLabelText()
        self.resetPage()
        self.products.removeAll()
        self.searchProductCollectionView.reloadData()
        self.controller.getTagProducts(self.tagId, pageNo: self.page, categoryId: SelectedFilters.shared.appliedCategoryInShelf.categoryId, searchText: self.searchKeyWord, currentProducts: self.products)
    }
    
}

extension SearchedInShelfVC : SearchDelegate {
    func getSearchData(text: String) {
        self.searchKeyWord = text
        self.titleView.text = text
        self.titleLabel.text = text
        self.setSearchLabelText()
        //        self.titleLabel.isHidden = false
        //        self.searchBackView.isHidden = true
        self.resetPage()
        self.products.removeAll()
        self.searchProductCollectionView.reloadData()
        self.controller.getTagProducts(self.tagId, pageNo: self.page, categoryId: SelectedFilters.shared.appliedCategoryInShelf.categoryId, searchText: self.searchKeyWord, currentProducts: self.products)

    }
}

extension SearchedInShelfVC : GetLikeStatusBack {
    
    func getLikeStatus(product: Product) {
        if let index = self.products.firstIndex(where: { (obj) -> Bool in
            return obj.productId == product.productId
        }){
            self.products[index].isLiked = product.isLiked
            self.searchProductCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
        }
    }
    
}

extension SearchedInShelfVC  : LikeUnlikeDelegate {
    
    func willRequestLikeUnlikeProduct() {
        
    }
    
    func productLikeUnlikeSuccessFull(product: Product) {
        
    }
    
    func failedToLikeUnlikeProduct(message: String, product: Product) {
        
    }
}


extension SearchedInShelfVC : TagProductsControllerDelegate{
    func willGetProducts() {
        if self.page == 1{
            self.shouldDisplayEmptyDataView = false
            self.searchProductCollectionView.reloadEmptyDataSet()
            self.view.showIndicator()
        }
    }
    
    func getProductSuccess(prodArr: [Product], nextPage: Int) {
        self.view.hideIndicator()
        
        if self.page != 0{
            self.page = nextPage
        }
        self.nextPage = nextPage
        self.products.append(contentsOf: prodArr)
        self.searchProductCollectionView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.searchProductCollectionView.reloadEmptyDataSet()
    }
    
    func getProductFailure(_ message: String) {
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = false
        self.searchProductCollectionView.reloadEmptyDataSet()
        CommonFunctions.showToastMessage(message)
    }
    
}

extension SearchedInShelfVC : ApplyFilter {
    
    func applyFilter(){
        self.resetPage()
        self.products.removeAll()
        self.searchProductCollectionView.reloadData()
        self.controller.getTagProducts(self.tagId, pageNo: self.page, categoryId: SelectedFilters.shared.appliedCategoryInShelf.categoryId, searchText: self.searchKeyWord, currentProducts: self.products)
    }
    
}



extension SearchedInShelfVC : ProductReportedDelegate{
    
    func productReportedSuccessFully(prodId : String){
        
        if let index = self.products.lastIndex(where: { $0.productId == prodId}){
            self.products.remove(at: index)
            self.shouldDisplayEmptyDataView = true
            self.searchProductCollectionView.reloadData()
            self.searchProductCollectionView.reloadEmptyDataSet()
        }
    }
    
}
