//
//  ReferFriendViewController.swift
//  TagHawk
//
//  Created by Vikash on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ReferFriendViewController: BaseVC {

    enum ShareCodeFor {
        case tagCreate
        case refferAFriend
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var shareCodeLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    
    
    var addTagResult: AddTagResult = AddTagResult()
    var cancelBtnTap: (() -> ())?
    var setUpFor = ShareCodeFor.tagCreate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.doInitialSetup()
    }

    private func doInitialSetup() {
        
        if setUpFor == .refferAFriend{
            self.titleLabel.text = LocalizedString.Reffer_To_Friend.localized
            self.headingLabel.text = LocalizedString.Reffer_Your_Neighbours_To_Join_TagHawk.localized
            self.descriptionLabel.text = LocalizedString.Share_The_Following_Refferal_Code.localized
            self.shareCodeLabel.text = UserProfile.main.invitationCode
        }else{
            self.descriptionLabel.text = self.addTagResult.description
            self.shareCodeLabel.text = self.addTagResult.shareCode
            self.headingLabel.text = LocalizedString.Invite_Your_Neighbours_To_Join_Tag.localized
            self.titleLabel.text = LocalizedString.Invite_Friend.localized
        }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func clickCancelButton(_ sender: Any) {
        self.dismiss(animated: true)
        cancelBtnTap?()
    }

    @IBAction func clickShareButton(_ sender: UIButton) {
        view.endEditing(true)
        
        if self.setUpFor == .refferAFriend{
            let subject = LocalizedString.Reffer_To_Friend.localized
            self.shareUrl(url: "https://www.apple.com/in/ios/app-store/", subject: subject)
        }else{
            let subject = addTagResult.name + " Community's link"
            self.shareUrl(url: self.addTagResult.deepLinkUrl, subject: subject)
        }
    }
    
}
