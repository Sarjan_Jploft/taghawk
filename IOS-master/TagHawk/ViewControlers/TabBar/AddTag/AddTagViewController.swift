//
//  AddTagViewController.swift
//  TagHawk
//
//  Created by Vikash on 04/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import StoreKit


enum TagEntrance: Int {
    case privateType = 1
    case publicType = 2
    
    init(type: Int){
        
        switch type{
            
        case TagEntrance.publicType.rawValue: self = TagEntrance.publicType
        case TagEntrance.privateType.rawValue: self = TagEntrance.privateType
        default: self = TagEntrance.publicType
            
        }
    }
}

enum TagType: Int {
    case apartmentType = 1
    case universitiesType = 2
    case organizationType = 3
    case clubType = 4
    case otherType = 5
    
    init(type: Int){
        
        switch type{
            
        case TagType.apartmentType.rawValue: self = TagType.apartmentType
        case TagType.universitiesType.rawValue: self = TagType.universitiesType
        case TagType.organizationType.rawValue: self = TagType.organizationType
        case TagType.clubType.rawValue: self = TagType.clubType
        case TagType.otherType.rawValue: self = TagType.otherType
        default: self = TagType.otherType
            
        }
    }
}

enum PrivateTagType: Int {
    case email = 1
    case password = 2
    case documents = 3
    case none = 0
    
    init(type: Int){
        
        switch type{
            
        case PrivateTagType.email.rawValue: self = PrivateTagType.email
        case PrivateTagType.password.rawValue: self = PrivateTagType.password
        case PrivateTagType.documents.rawValue: self = PrivateTagType.documents
        case PrivateTagType.none.rawValue: self = PrivateTagType.none
            
        default: self = PrivateTagType.none
        }
    }
}

enum AddDetailsProgress: Int {
    case firstProgress = 0
    case secondProgress = 1
}

struct AddTagData {
    
    var tagName = ""
    var imageUrl: String?
    var thumbUrl: String?
    var tagType: Int?
    var tagEntrance: Int?
    var privateTagType = PrivateTagType.email.rawValue
    var email = ""
    var password = ""
    var documents = ""
    var description = ""
    var location = ""
    var city = ""
    var lat: Double = 0.0
    var long: Double = 0.0
    var announcement = ""
    var fundraising = ""
    var goalAmount = ""
    var paymentType = TagPaymentTypePopup.PayForTagUsing.none
    var paymentId : String = ""
    var documentType: String = ""
}


class AddTagViewController : BaseVC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tagNameTextField: UITextField!
    @IBOutlet weak var descriptionBorderView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var uploadLogoLabel: UILabel!
    @IBOutlet weak var tagTypeLabel: UILabel!
    @IBOutlet weak var tagEntraceLabel: UILabel!
    @IBOutlet weak var tagTypeBtn: UIButton!
    @IBOutlet weak var tagEntraceBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var selectTagTypeLabel: UILabel!
    @IBOutlet weak var selectTagEntranceLabel: UILabel!
    @IBOutlet var tagSelectionType: [UIButton]!
    @IBOutlet weak var descriptionTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagTypeTextField: UITextField!
    @IBOutlet weak var tagPrivateView: UIView!
    @IBOutlet weak var logoButtonImage: UIButton!
    @IBOutlet weak var firstView: UIScrollView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var emailAtLabel: UILabel!
    @IBOutlet weak var emailTextfieldLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var announcementLbl: UILabel!
    @IBOutlet weak var announcementBorderView: UIView!
    @IBOutlet weak var announcementTextView: UITextView!
    @IBOutlet weak var announcementPlaceholderLbl: UILabel!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var indicatorBackView: UIView!
    @IBOutlet weak var imageUpoadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var payUsingLbl: UILabel!
    @IBOutlet weak var selectPayTypeLbl: UILabel!
    @IBOutlet weak var selectPayTypeBtn: UIButton!
    @IBOutlet weak var payTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentDescLbl: UILabel!
    @IBOutlet weak var payLblBottomToDone: NSLayoutConstraint!
    @IBOutlet weak var tagNameLabel: UILabel!
    @IBOutlet weak var locationTitleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var lblTagFundraisingEvent: UILabel!
    
    
    @IBOutlet weak var txtViewFundraising: UITextView!
    @IBOutlet weak var lblFundRaisingPlaceholder: UILabel!
    
    @IBOutlet weak var txtGoalAmount: UITextField!
    
    //Temp Change
    @IBOutlet weak var NSLayoutHeightLblGoal: NSLayoutConstraint!
    @IBOutlet weak var NSLayoutHeightViewGoal: NSLayoutConstraint!
    @IBOutlet weak var NSLayoutHeightViewTagType: NSLayoutConstraint!
    @IBOutlet weak var NSLayoutHeightTagPrivate: NSLayoutConstraint!
    
    
    
    
    //Variables
    private let dropDown = DropDown()
    var tagData = AddTagData()
    let controller = AddTagController()
    private var currentProgress = AddDetailsProgress.firstProgress.rawValue
    var isEditingTag = false
    var tagId = ""
    var referVcCancelBtnTap: (() -> ())?
    var tagCreateSuccess: ((Tag) -> ())?
    var iapProduct = SKProduct()
    private var isImageUpdated = false
    
    
    //MARK: - View's Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.doInitialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        GroupChatFireBaseController.shared.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.leftBarItemImage(change: UIImage(named: "icSearchClose")!, ofVC: self)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.logoImage.round()
            self.logoImage.borderWidth = 1.0
            self.logoImage.borderColor = #colorLiteral(red: 0.5843137255, green: 0.6156862745, blue: 0.6823529412, alpha: 1)
        }
    }
    
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        pop()
    }
    
    //MARK: - Methods
    private func doInitialSetup() {
        
        self.uploadLogoLabel.setAttributes(text: "\(LocalizedString.uploadLogo.localized)*", font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        
        self.uploadLogoLabel.attributedText = "\(LocalizedString.uploadLogo.localized)*".attributeStringWithAstric()
        
        //Temp Change
        self.tagEntraceLabel.setAttributes(text: "\(LocalizedString.tagEntrace.localized)*", font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        self.tagEntraceLabel.attributedText = "\(LocalizedString.tagEntrace.localized)*".attributeStringWithAstric()
        /*
        self.tagTypeLabel.setAttributes(text: "\(LocalizedString.tagType.localized)*", font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        self.tagTypeLabel.attributedText = "\(LocalizedString.tagType.localized)*".attributeStringWithAstric()
        */
        
        self.tagTypeLabel.text = ""
        
        
        
        
        
        
        self.descriptionLabel.setAttributes(text: "\(LocalizedString.Tag_Description.localized)*", font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        self.descriptionLabel.attributedText = "\(LocalizedString.Tag_Description.localized)*".attributeStringWithAstric()
        
        self.tagNameLabel.setAttributes(text: "\(LocalizedString.Tag_Name.localized)*", font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        
        self.tagNameLabel.attributedText = "\(LocalizedString.Tag_Name.localized)*".attributeStringWithAstric()

        self.locationTitleLabel.setAttributes(text: "\(LocalizedString.Location.localized)*", font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        self.locationTitleLabel.attributedText = "\(LocalizedString.Location.localized)*".attributeStringWithAstric()
        
        
        //Temp Change
      /*
         self.lblTagFundraisingEvent.setAttributes(text: "\(LocalizedString.TagFundRaising.localized)*", font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        self.lblTagFundraisingEvent.attributedText = "\(LocalizedString.TagFundRaising.localized)".attributeStringWithAstric()
         
         // *When a product is sold from your tag, the buyer has the option to donate to your fundraising event
         
         
        */
        
        self.lblTagFundraisingEvent.text = ""
        self.NSLayoutHeightLblGoal.constant = 0
        self.NSLayoutHeightViewGoal.constant = 0
        self.NSLayoutHeightViewTagType.constant = 0
        self.NSLayoutHeightTagPrivate.constant = 0
        
        
        
        
        
        
        self.announcementLbl.setAttributes(text: "\(LocalizedString.Tag_Announcement.localized)*", font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        self.announcementLbl.attributedText = "\(LocalizedString.Tag_Announcement.localized)*".attributeStringWithAstric()
        
        self.payUsingLbl.setAttributes(text: "\(LocalizedString.Pay_Using.localized)*", font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
        self.payUsingLbl.attributedText = "\(LocalizedString.Pay_Using.localized)*".attributeStringWithAstric()
        
        self.paymentDescLbl.text = LocalizedString.Create_Tag_In_Either_Way.localized
        
        imageUpoadIndicator.isHidden = true
        imageUpoadIndicator.hidesWhenStopped = true
        self.tagPrivateView.isHidden = true
//        self.descriptionTopConstraint.constant = 130//20
//        self.descriptionTopConstraint.constant = 20
        self.firstView.isHidden = false
        tagNameTextField.delegate = self
        tagTypeTextField.returnKeyType = .next
        self.locationView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.mapFieldTapped)))
        self.controller.delegate = self
        self.tagTypeTextField.placeholder = "Request Email Domain to join the Tag"
        
        DispatchQueue.main.async {
            self.logoImage.round()
        }
        
        if tagData.location.isEmpty {
            getAddressFromReverceGeoCoding(lat: LocationManager.shared.latitude, long: LocationManager.shared.longitude)
        } else {
            locationLabel.text = tagData.location
        }
        
        if isEditingTag { setupForEditing() } else {
            self.setNavigationBar(withTitle: "Create Your Tag", firstLeftButtonImage: UIImage(named: "icClose"))
        }
        
        self.indicatorView.hidesWhenStopped = true
        self.indicatorView.stopAnimating()
        self.indicatorBackView.isHidden = true
        self.firstView.isHidden = true
        self.fetchSubscriptions()
    }
    
    override func onClickFirstLeftNavigationBarButton(_ sender: UIButton) {
        view.endEditing(true)
        self.pop()
    }
    
    func setUpDropDownForTagType(){
        
        // This is done as we need enum values and rawvalues both
        //        let tagTypes = [SelectedFilters.TagType.privateTag, SelectedFilters.TagType.publicTag]
        let tagTypes = [SelectedFilters.TagType.apartment, SelectedFilters.TagType.universities,SelectedFilters.TagType.organization,SelectedFilters.TagType.club,SelectedFilters.TagType.other]
        
        self.dropDown.dataSource = tagTypes.map { SelectedFilters.shared.getTagTypeStringValue(from: $0).rawValue }
        
        self.dropDown.show()
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            if index == 0 {
                weakSelf.tagPrivateView.isHidden = false
                //                weakSelf.descriptionTopConstraint.constant = 320//200
                //                weakSelf.descriptionTopConstraint.constant = 200
                weakSelf.NSLayoutHeightTagPrivate.constant = 170
            } else {
                weakSelf.tagPrivateView.isHidden = true
                //                weakSelf.descriptionTopConstraint.constant = 130//20
                //                weakSelf.descriptionTopConstraint.constant = 20
                weakSelf.NSLayoutHeightTagPrivate.constant = 0
                
            }
            //Temp Comment
            self?.tagData.tagType = index + 1
            weakSelf.selectTagTypeLabel.text = item
            self?.selectTagTypeLabel.textColor = UIColor.black
            
        }
    }
    
    func setUpDropDownForEntranceType(){
        
        // This is done as we need enum values and rawvalues both
        let EntranceTypes = [SelectedFilters.TagEntrance.privateTag, SelectedFilters.TagEntrance.publicTag]
        
        
        self.dropDown.dataSource = EntranceTypes.map { SelectedFilters.shared.getEntraceTypeStringValue(from: $0).rawValue }
        
        self.dropDown.show()
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            if index == 0 {
                weakSelf.tagPrivateView.isHidden = false
//                weakSelf.descriptionTopConstraint.constant = 320//200
//                weakSelf.descriptionTopConstraint.constant = 320
                weakSelf.NSLayoutHeightTagPrivate.constant = 170
            } else {
                weakSelf.tagPrivateView.isHidden = true
               // weakSelf.descriptionTopConstraint.constant = 130//20
//                weakSelf.descriptionTopConstraint.constant = 20
                 weakSelf.NSLayoutHeightTagPrivate.constant = 0
            }
            //Temp Comment
            self?.tagData.tagEntrance = index + 1
//            self?.tagData.tagType = index + 1
            weakSelf.selectTagEntranceLabel.text = item
            self?.selectTagEntranceLabel.textColor = UIColor.black

            //print(self?.tagData.tagEntrance)
            if self?.tagData.tagEntrance == 1 && self?.emailAtLabel.isHidden == false{
                self?.tagData.privateTagType = PrivateTagType.email.rawValue
            }
            
        }
    }
    
    
    
    func setUpDropDownForPayType(){
        
        // This is done as we need enum values and rawvalues both
        let payTypes = [LocalizedString.Reward_Points.localized, LocalizedString.Payment.localized]
        
        self.dropDown.dataSource = payTypes
        
        self.dropDown.show()
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            self?.tagData.paymentType = index == 0 ? .credittPoints : .inApp
            self?.selectPayTypeLbl.text = item
            self?.selectPayTypeLbl.textColor = UIColor.black
            
        }
    }
    
    @objc func mapFieldTapped() {
        let vc = SelectLocationVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    private func setupForEditing() {
        
//        payLblBottomToDone.constant = 0
        paymentDescLbl.isHidden = true
        payUsingLbl.isHidden = true
        payTypeViewHeight.constant = 0
        
        self.setNavigationBar(withTitle: "Edit Tag", firstLeftButtonImage: UIImage(named: "icClose"))
        
        logoButtonImage.setImage(nil, for: .normal)
        logoImage.setImage_kf(imageString: tagData.imageUrl ?? "", placeHolderImage: #imageLiteral(resourceName: "icAdddetailUploadPh"), loader: true)
        tagNameTextField.text = tagData.tagName
        txtGoalAmount.text = tagData.goalAmount
        selectTagTypeLabel.textColor = UIColor.black
        selectTagEntranceLabel.textColor = UIColor.black
        //Temp data  tagData.tagEntrance
        
        if let type = tagData.tagEntrance {
            if type == 1 {
                //Temp change
//                selectTagTypeLabel.text = LocalizedString.Private.localized
                selectTagEntranceLabel.text = LocalizedString.Private.localized
                tagPrivateView.isHidden = false
//                descriptionTopConstraint.constant = 320//200
//                descriptionTopConstraint.constant = 200
                self.NSLayoutHeightTagPrivate.constant = 170
                
                let privateTagType = PrivateTagType(rawValue: tagData.privateTagType)
                if let tagType = privateTagType {
                    switch tagType {
                    case .email:
                        self.emailAtLabel.isHidden = false
                        self.tagTypeTextField.text = tagData.email
                        self.tagSelectionType[0].backgroundColor = UIColor(red: 43/255, green: 206/255, blue: 253/255, alpha: 1.0)
                        self.tagSelectionType[0].setTitleColor(.white)
                        self.tagSelectionType[0].layer.borderColor = UIColor.clear.cgColor
                        self.tagSelectionType[0].layer.borderWidth = 0.5
                        self.tagTypeTextField.placeholder = "Request Email Domain to join the Tag"
                        
                        
                    case .password:
                        self.emailAtLabel.isHidden = true
                        self.tagTypeTextField.text = tagData.password
                        self.emailTextfieldLeadingConstraint.constant = 10
                        self.tagData.privateTagType = PrivateTagType.password.rawValue
                        self.tagSelectionType[1].backgroundColor = UIColor(red: 43/255, green: 206/255, blue: 253/255, alpha: 1.0)
                        self.tagSelectionType[1].setTitleColor(.white)
                        self.tagSelectionType[1].layer.borderColor = UIColor.clear.cgColor
                        self.tagSelectionType[1].layer.borderWidth = 0.5
                        self.tagSelectionType[0].backgroundColor = UIColor.clear
                        self.tagSelectionType[0].setTitleColor(.black)
                        self.tagSelectionType[0].layer.borderColor = UIColor(red: 209/255, green: 223/255, blue: 233/255, alpha: 1.0).cgColor
                        self.tagSelectionType[0].layer.borderWidth = 0.5
                        self.tagTypeTextField.placeholder = "Request password to join the Tag"
                        
                    case .documents:
                        self.emailAtLabel.isHidden = true
                        self.emailTextfieldLeadingConstraint.constant = 10
                        self.tagTypeTextField.text = tagData.documentType
                        self.tagData.privateTagType = PrivateTagType.documents.rawValue
                        self.tagSelectionType[2].backgroundColor = UIColor(red: 43/255, green: 206/255, blue: 253/255, alpha: 1.0)
                        self.tagSelectionType[2].setTitleColor(.white)
                        self.tagSelectionType[2].layer.borderColor = UIColor.clear.cgColor
                        self.tagSelectionType[2].layer.borderWidth = 0.5
                        self.tagSelectionType[0].backgroundColor = UIColor.clear
                        self.tagSelectionType[0].setTitleColor(.black)
                        self.tagSelectionType[0].layer.borderColor = UIColor(red: 209/255, green: 223/255, blue: 233/255, alpha: 1.0).cgColor
                        self.tagSelectionType[0].layer.borderWidth = 0.5
                        self.tagTypeTextField.placeholder = "Request Documents to join the Tag"
                        
                    case .none:
                        self.emailAtLabel.isHidden = false
                        self.tagTypeTextField.text = tagData.email
                        self.tagSelectionType[0].backgroundColor = UIColor(red: 43/255, green: 206/255, blue: 253/255, alpha: 1.0)
                        self.tagSelectionType[0].setTitleColor(.white)
                        self.tagSelectionType[0].layer.borderColor = UIColor.clear.cgColor
                        self.tagSelectionType[0].layer.borderWidth = 0.5
                    }
                }
                
            } else {
                //Temp change
//                selectTagTypeLabel.text = LocalizedString.Public.localized
                selectTagEntranceLabel.text = LocalizedString.Public.localized
                tagPrivateView.isHidden = true
//                descriptionTopConstraint.constant = 130//20
//                descriptionTopConstraint.constant = 20
                self.NSLayoutHeightTagPrivate.constant = 0
            }
        }
        
        placeholderLabel.isHidden = true
        announcementPlaceholderLbl.isHidden = true
        lblFundRaisingPlaceholder.isHidden = true
        descriptionTextView.text = tagData.description
        announcementTextView.text = tagData.announcement
        txtViewFundraising.text = tagData.fundraising
    }
    
    //MARK: - IBActions
    @IBAction func clickPhotoUpload(_ sender: UIButton) {
        view.endEditing(true)
        
        let vc = CameraVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: true)
        vc.delegate = self
        vc.cameraFor = .addTag
//        vc.capturedImages = self.addProductData.capturedImages
        self.present(nav, animated: true, completion: nil)
        
//        self.openActionSheetWith(arrOptions: [AppConstants.camera.rawValue, AppConstants.photoLibrary.rawValue], openIn: self) { (actionIndex) in
//
//            switch actionIndex {
//            case 0:
//                self.checkAndOpenCamera(delegate: self)
//            case 1:
//                self.checkAndOpenLibrary(delegate: self)
//            default:
//                break;
//            }
//        }
    }
    
    @IBAction func clickSelectTagTypeButton(_ sender: UIButton) {
        view.endEditing(true)
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
        self.dropDown.dismissMode = .automatic
        self.setUpDropDownForTagType()
    }
    
    @IBAction func clickSelectTagEntraceButton(_ sender: UIButton) {
        view.endEditing(true)
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
        self.dropDown.dismissMode = .automatic
        self.setUpDropDownForEntranceType()
    }
    
    
    fileprivate func createUpdateTag() {
        
        if isEditingTag {
            
            self.controller.editTagService(addTag: self.tagData, tagId: tagId) {
                
            }
            
        } else {
            if tagData.paymentType == .credittPoints{
                self.controller.addTagService(addTag: self.tagData) {
                }
                
            }else{
                
                self.purchaseProd(self.iapProduct)
            }
            
            
            //            let paymentMethodVC = TagPaymentTypePopup.instantiate(fromAppStoryboard: .AddTag)
            //            paymentMethodVC.delegate = self
            //            paymentMethodVC.modalPresentationStyle = .overCurrentContext
            //            self.present(paymentMethodVC, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func clickNextButton(_ sender: UIButton) {
        
        if !isEditingTag && tagData.paymentType == .none {
            CommonFunctions.showToastMessage(LocalizedString.PleaseSelectPayType.localized)
            return
        }
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        view.endEditing(true)
        if self.controller.validateAddProduct(with: self.tagData) {
            
            if isImageUpdated {
                if let image = logoImage.image {
                    uploadLogoImageToAWS(image: image)
                }
                
            } else {
                
                createUpdateTag()
            }
        }
    }
    
    @IBAction func clickPrivateTagTypeButton(_ sender: UIButton) {
        view.endEditing(true)
        
        self.tagTypeTextField.text = ""
        self.tagData.email = ""
        self.tagData.password = ""
        self.tagData.documents =  ""
        
        switch sender.tag {
        case 0:
            self.tagData.privateTagType = PrivateTagType.email.rawValue
            self.emailTextfieldLeadingConstraint.constant = 30
            self.emailAtLabel.isHidden = false
            self.tagSelectionType[0].backgroundColor = UIColor(red: 43/255, green: 206/255, blue: 253/255, alpha: 1.0)
            self.tagSelectionType[0].setTitleColor(.white)
            self.tagSelectionType[0].layer.borderColor = UIColor.clear.cgColor
            self.tagSelectionType[0].layer.borderWidth = 0.5
            self.tagSelectionType[1].backgroundColor = UIColor.clear
            self.tagSelectionType[1].setTitleColor(.black)
            self.tagSelectionType[1].layer.borderColor = UIColor(red: 209/255, green: 223/255, blue: 233/255, alpha: 1.0).cgColor
            self.tagSelectionType[1].layer.borderWidth = 0.5
            self.tagSelectionType[2].backgroundColor = UIColor.clear
            self.tagSelectionType[2].setTitleColor(.black)
            self.tagSelectionType[2].layer.borderColor = UIColor(red: 209/255, green: 223/255, blue: 233/255, alpha: 1.0).cgColor
            self.tagSelectionType[2].layer.borderWidth = 0.5
            self.tagTypeTextField.placeholder = "Request Email Domain to join the Tag"
            
        case 1:
            self.emailAtLabel.isHidden = true
            self.emailTextfieldLeadingConstraint.constant = 10
            self.tagData.privateTagType = PrivateTagType.password.rawValue
            self.tagSelectionType[1].backgroundColor = UIColor(red: 43/255, green: 206/255, blue: 253/255, alpha: 1.0)
            self.tagSelectionType[1].setTitleColor(.white)
            self.tagSelectionType[1].layer.borderColor = UIColor.clear.cgColor
            self.tagSelectionType[1].layer.borderWidth = 0.5
            self.tagSelectionType[0].backgroundColor = UIColor.clear
            self.tagSelectionType[0].setTitleColor(.black)
            self.tagSelectionType[0].layer.borderColor = UIColor(red: 209/255, green: 223/255, blue: 233/255, alpha: 1.0).cgColor
            self.tagSelectionType[0].layer.borderWidth = 0.5
            self.tagSelectionType[2].backgroundColor = UIColor.clear
            self.tagSelectionType[2].setTitleColor(.black)
            self.tagSelectionType[2].layer.borderColor = UIColor(red: 209/255, green: 223/255, blue: 233/255, alpha: 1.0).cgColor
            self.tagSelectionType[2].layer.borderWidth = 0.5
            self.tagTypeTextField.placeholder = "Request password to join the Tag"
            
        default:
            self.emailAtLabel.isHidden = true
            self.emailTextfieldLeadingConstraint.constant = 10
            self.tagData.privateTagType = PrivateTagType.documents.rawValue
            self.tagSelectionType[2].backgroundColor = UIColor(red: 43/255, green: 206/255, blue: 253/255, alpha: 1.0)
            self.tagSelectionType[2].setTitleColor(.white)
            self.tagSelectionType[2].layer.borderColor = UIColor.clear.cgColor
            self.tagSelectionType[2].layer.borderWidth = 0.5
            self.tagSelectionType[0].backgroundColor = UIColor.clear
            self.tagSelectionType[0].setTitleColor(.black)
            self.tagSelectionType[0].layer.borderColor = UIColor(red: 209/255, green: 223/255, blue: 233/255, alpha: 1.0).cgColor
            self.tagSelectionType[0].layer.borderWidth = 0.5
            self.tagSelectionType[1].backgroundColor = UIColor.clear
            self.tagSelectionType[1].setTitleColor(.black)
            self.tagSelectionType[1].layer.borderColor = UIColor(red: 209/255, green: 223/255, blue: 233/255, alpha: 1.0).cgColor
            self.tagSelectionType[1].layer.borderWidth = 0.5
            self.tagTypeTextField.placeholder = "Request Documents to join the Tag"
        }
    }
    
    @IBAction func currentLocationTapped(_ sender: UIButton) {
        view.endEditing(true)
        LocationManager.shared.startUpdatingLocation { (loc, error) in
            guard let location = loc else { return }
            
            self.getAddressFromReverceGeoCoding(lat: location.coordinate.latitude, long: location.coordinate.longitude)
            LocationManager.shared.stopLocationManger()
        }
    }
    
    func getAddressFromReverceGeoCoding(lat : Double, long : Double){
        LocationManager.shared.reverseGeocodeLocationWithLatLon(latitude: lat, longitude: long) { (dict, placeMarker, error) in
            guard let data = dict else { return }
            guard let add = data[ApiKey.formattedAddress] as? String else { return }
            self.locationLabel.text = add
            self.tagData.location = add
            self.tagData.lat = lat
            self.tagData.long = long
            self.tagData.city = data[ApiKey.locality] as? String ?? ""
        }
    }
    
    
    func uploadLogoImageToAWS(image: UIImage) {
        
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        imageUpoadIndicator.isHidden = false
        imageUpoadIndicator.startAnimating()
        view.showIndicator()
        
        image.uploadImageToS3WithUtility(imageIndex: 0, success: { (progress, success, imageUrl) in
            self.tagData.imageUrl = imageUrl
            self.view.hideIndicator()
            self.isImageUpdated = false
            self.createUpdateTag()

            self.imageUpoadIndicator.stopAnimating()
        }, progress: { (imageIndex, progress) in
            
            
        },failure: { (imageindex, error) in
            
            CommonFunctions.showToastMessage(error.localizedDescription)
            self.imageUpoadIndicator.stopAnimating()
        })
    }
    
    @IBAction func selectPayTypeBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
        self.dropDown.dismissMode = .automatic
        self.setUpDropDownForPayType()
    }
}

extension AddTagViewController: UITextFieldDelegate,
    UITextViewDelegate,
    UIImagePickerControllerDelegate,
AddTagDelegate, GedAddressBack {
    
    func willAddTag() {
        self.view.showIndicator()
    }
    
    func addTagListServiceReturn(addTagResult: AddTagResult,
                                 msg: String,
                                 tag: Tag) {
        self.view.hideIndicator()
        if !isEditingTag {
            tagCreateSuccess?(tag)
            GroupChatFireBaseController.shared.addGroup(tagData: addTagResult,
                                                        msg: UserProfile.main.userId)
            let referVC = ReferFriendViewController.instantiate(fromAppStoryboard: .AddTag)
            referVC.addTagResult = addTagResult
            referVC.cancelBtnTap = { [weak self] in
                guard let `self` = self else { return }
                if self.isEditingTag {
                    self.referVcCancelBtnTap?()
                    self.pop()
                }
            }
            self.present(referVC, animated: true) {
                if !self.isEditingTag {
                    self.pop()
                }
            }
        }else{
           GroupChatFireBaseController.shared.updateGroupInfo(tagData: addTagResult)
            self.referVcCancelBtnTap?()
        }
        
        CommonFunctions.showToastMessage(msg)
    }
    
    func addTagListFailure(errorType: ApiState, message: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
    
    func getAddress(lat: Double, long: Double, addressString: String) {
        self.locationLabel.text = addressString
        self.tagData.location = addressString
        self.tagData.lat = lat
        self.tagData.long = long
        LocationManager.shared.reverseGeocodeLocationWithLatLon(latitude: lat, longitude: long) { (dict, place, err) in
            self.tagData.city = place?.locality ?? ""
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tagNameTextField {
            view.endEditing(true)
            DispatchQueue.delay(0.2) {
                //                self.clickSelectTagTypeButton(self.tagTypeBtn)
                self.clickSelectTagEntraceButton(self.tagEntraceBtn)
            }
        }else if textField == tagTypeTextField {
//            textField.resignFirstResponder()
//            descriptionTextView.becomeFirstResponder()
            view.endEditing(true)
            DispatchQueue.delay(0.2) {
                //                self.clickSelectTagTypeButton(self.tagTypeBtn)
                self.clickSelectTagTypeButton(self.tagTypeBtn)
            }
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == descriptionTextView {
            self.placeholderLabel.isHidden = true
        }else if textView == txtViewFundraising{
            self.lblFundRaisingPlaceholder.isHidden = true
        }else {
            announcementPlaceholderLbl.isHidden = true
        }
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView == descriptionTextView {
            self.tagData.description = textView.text
        }else if textView == txtViewFundraising{
            self.tagData.fundraising = textView.text
        }else {
            self.tagData.announcement = textView.text
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == descriptionTextView {
            self.placeholderLabel.isHidden = textView.text.isEmpty ? false : true
        }else if textView == txtViewFundraising {
            self.lblFundRaisingPlaceholder.isHidden = textView.text.isEmpty ? false : true
        } else {
            announcementPlaceholderLbl.isHidden = textView.text.isEmpty ? false : true
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == descriptionTextView {
            if textView.text.count > 250 {
                textView.text.removeLast()
            }
        }else if textView == txtViewFundraising {
            if textView.text.count > 250 {
                textView.text.removeLast()
            }
        } else {
            if textView.text.count > 250 {
                textView.text.removeLast()
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard let userEnteredString = textView.text else { return false }
        if ((range.location == 0) && (text == " " || text == "\n")) || text.containsEmoji {return false}
    
         if textView == descriptionTextView {
            return self.isCharacterAcceptableForDescription(name: userEnteredString, char: text)
         }else{
            return self.isCharacterAcceptableForAnnouncement(name: userEnteredString, char: text)
        }
        
        
    }
    
    func isCharacterAcceptableForDescription(name : String, char : String)-> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 1000{
            return false
        }else{
            return char.isAllowed(validationStr: AllowedCharactersValidation.description)
        }
    }
    
    func isCharacterAcceptableForAnnouncement(name : String, char : String)-> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 1000{
            return false
        }else{
            return true
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[.originalImage] as? UIImage {
            picker.dismiss(animated: true) {
                
                self.uploadLogoImageToAWS(image: img)
            }
        }
    }
    
    func validateInput(isValid: Bool, message: String) {
        print("message", message)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.tagNameTextField {
            self.tagData.tagName = textField.text ?? ""
        } else {
            if self.tagData.privateTagType == PrivateTagType.email.rawValue {
                self.tagData.email = textField.text ?? ""
            } else if self.tagData.privateTagType == PrivateTagType.password.rawValue {
                self.tagData.password = textField.text ?? ""
            } else {
                self.tagData.documents = textField.text ?? ""
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let inputMode = textField.textInputMode else {
            return false
        }
        if inputMode.primaryLanguage == "emoji" || !(inputMode.primaryLanguage != nil) {
            return false
        }
        
        if textField == self.tagNameTextField {
            
            guard let txt = textField.text else { return false}
    
            if let text = textField.text {
                if text.count > 50 {
                    textField.text?.removeLast()
                }
            }
            
            self.tagData.tagName = txt
            return string.isAllowed(validationStr: AllowedCharactersValidation.name)
            
        } else {
            if self.tagData.privateTagType == PrivateTagType.email.rawValue {
                self.tagData.email = textField.text ?? ""
            } else if self.tagData.privateTagType == PrivateTagType.password.rawValue {
                self.tagData.password = textField.text ?? ""
            } else {
                self.tagData.documents = textField.text ?? ""
            }
        }
        return true
    }
}

extension AddTagViewController : TagPaymentTypePopupDelegate {
    
    func getSelectedTagpaymentType(type : TagPaymentTypePopup.PayForTagUsing){
        printDebug(type)
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if type == .credittPoints{
            self.tagData.paymentType = .credittPoints
            self.controller.addTagService(addTag: self.tagData) {
                
            }
            
        }else{
            self.tagData.paymentType = .inApp
            self.purchaseProd(self.iapProduct)
        }
    }
}

extension AddTagViewController : GetCapturedImages {
    
    func getCapturedImagesBack(images : [(index: Int, stringUrl: String, img: UIImage?)]) {
        if let firstImage = images.first?.img {
            self.logoButtonImage.setImage(nil, for: .normal)
            self.tagData.imageUrl = images.first?.stringUrl
            self.logoImage.image = firstImage
            isImageUpdated = true
        }
    }
}

extension AddTagViewController: GroupChatFireBaseControllerDelegate{
    
    func updateTagSuccessfully(){
        navigationController?.popViewController(animated: true)
    }
}
