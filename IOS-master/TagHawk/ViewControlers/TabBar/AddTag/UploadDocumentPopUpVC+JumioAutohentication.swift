//
//  UploadDocumentPopUpVC+JumioAutohentication.swift
//  TagHawk
//
//  Created by Appinventiv on 01/05/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Netverify
import NetverifyFace

extension UploadDocumentPopUpVC {
        
    func createAuthenticationController(withCustomUI:Bool = false, referenceId: String) {
        
        //Prevent SDK to be initialized on Jailbroken devices
        if JumioDeviceInfo.isJailbrokenDevice() {
            return
        }
        
        // Setup the Configuration for Authentication
        let config: AuthenticationConfiguration = createAuthenticationConfiguration(withCustomUI: withCustomUI, referenceId: referenceId)
        
        
        //Perform the following call as soon as your app’s view controller is initialized. Create the AuthenticationController instance by providing your Configuration with required API token, API secret and a delegate object.
        do {
            try ObjcExceptionHelper.catchException {
                self.authenticationController = AuthenticationController(configuration: config)
            }
        } catch {
            let err = error as NSError
            self.showAlert(withTitle: err.localizedDescription, message: err.userInfo[NSLocalizedFailureReasonErrorKey] as? String ?? "")
        }
    }
    
    
    func createAuthenticationConfiguration(withCustomUI:Bool, referenceId: String) -> AuthenticationConfiguration {
        let config = AuthenticationConfiguration()
        
        //Provide your API token
        config.apiToken = "ed000985-93d5-4837-91c8-3a266c2c83b5"
        //Provide you API secret
        config.apiSecret = "yIMeTuvMkeNCUCffNeT2sf904Gr2v6qT"
        
        //Set the delegate that implements AuthenticationControllerDelegate
        config.delegate = self
        
        //Use the following property to reference the Authentication transaction to a specific Netverify user identity
        //config.enrollmentTransactionReference = "ENROLLMENT_TRANSACTION_REFERENCE"
        config.enrollmentTransactionReference = referenceId
        //Set the dataCenter; default is JumioDataCenterUS
        //config.dataCenter = JumioDataCenterEU
        
//        if (withCustomUI) {
//            config.authenticationScanViewControllerDelegate = self
//
//            JumioBaseView.jumioAppearance().disableBlur = true
//            JumioBaseView.jumioAppearance().foregroundColor = UIColor.white
//            JumioBaseView.jumioAppearance().backgroundColor = UIColor.init(red: 44/250.0, green: 152/250.0, blue: 240/250.0, alpha: 1.0)
//        }
        
        //You can also set a customer identifier (max. 100 characters). Note: The customer ID should not contain sensitive data like PII (Personally Identifiable Information) or account login.
        //config.userReference = "USER_REFERENCE"
        
        //Callback URL (max. 255 characters) for the confirmation after the authentication is completed. This setting overrides your Jumio account settings.
        //config.callbackUrl = "https://www.example.com"
        
        //Configure your desired status bar style
        //config.statusBarStyle = UIStatusBarStyleLightContent
        
        //Localizing labels
        //All label texts and button titles can be changed and localized using the Localizable-Authentication.strings file. Just adapt the values to your required language and use this file in your app.
        
        //Customizing look and feel
        //The SDK can be customized via UIAppearance to fit your application’s look and feel.
        //Please note, that only the below listed UIAppearance selectors are supported and taken into consideration. Experimenting with other UIAppearance or not UIAppearance selectors may cause unexpected behaviour or crashes both in the SDK or in your application. This is best avoided.
        
        // - Navigation bar: tint color, title color, title image
        //UINavigationBar.jumioAppearance().tintColor = UIColor.yellow
        //UINavigationBar.jumioAppearance().barTintColor = UIColor.red
        //UINavigationBar.jumioAppearance().titleTextAttributes = [kCTForegroundColorAttributeName: UIColor.white] as [NSAttributedStringKey : Any]
        
        //AuthenticationNavigationBarTitleImageView.jumioAppearance().titleImage = UIImage.init(named: "<your-navigation-bar-title-image>")
        
        // - Custom general appearance - deactivate blur
        //JumioBaseView.jumioAppearance().disableBlur = true
        
        // - Custom general appearance - background color
        //JumioBaseView.jumioAppearance().backgroundColor = UIColor.lightGray
        
        // - Custom general appearance - foreground color (text-elements and icons)
        //JumioBaseView.jumioAppearance().foregroundColor = UIColor.red
        
        // - Custom Positive Button Background Colors, custom class has to be imported (the same applies to JumioNegativeButton)
        //JumioPositiveButton.jumioAppearance().setBackgroundColor(UIColor.cyan, for: UIControlState.normal)
        //JumioPositiveButton.jumioAppearance().setBackgroundColor(UIColor.blue, for: UIControlState.highlighted)
        
        //Custom Positive Button Background Image, custom class has to be imported
        //JumioPositiveButton.jumioAppearance().setBackgroundImage(UIImage.init(named: "<your-custom-image>"), for: UIControlState.normal)
        //JumioPositiveButton.jumioAppearance().setBackgroundImage(UIImage.init(named: "<your-custom-image>"), for: UIControlState.highlighted)
        
        //Custom Positive Button Title Colors, custom class has to be imported
        //JumioPositiveButton.jumioAppearance().setTitleColor(UIColor.gray, for: UIControlState.normal)
        //JumioPositiveButton.jumioAppearance().setTitleColor(UIColor.magenta, for: UIControlState.highlighted)
        
        //Custom Positive Button Title Colors, custom class has to be imported
        //JumioPositiveButton.jumioAppearance().borderColor = UIColor.green
        
        // Color for the face oval outline
        //JumioScanOverlayView.jumioAppearance().faceOvalColor = UIColor.orange
        // Color for the progress bars
        //JumioScanOverlayView.jumioAppearance().faceProgressColor = UIColor.purple
        // Color for the background of the feedback view
        //JumioScanOverlayView.jumioAppearance().faceFeedbackBackgroundColor = UIColor.yellow
        // Color for the text of the feedback view
        //JumioScanOverlayView.jumioAppearance().faceFeedbackTextColor = UIColor.brown
        
        // - Custom general appearance - font
        //The font has to be loaded upfront within the mainBundle before initializing the SDK
        //JumioBaseView.jumioAppearance().customLightFontName = "<your-font-name-loaded-in-your-app>"
        //JumioBaseView.jumioAppearance().customRegularFontName = "<your-font-name-loaded-in-your-app>"
        //JumioBaseView.jumioAppearance().customMediumFontName = "<your-font-name-loaded-in-your-app>"
        //JumioBaseView.jumioAppearance().customBoldFontName = "<your-font-name-loaded-in-your-app>"
        //JumioBaseView.jumioAppearance().customItalicFontName = "<your-font-name-loaded-in-your-app>"
        
        //You can get the current SDK version using the method below.
        //print("\(AuthenticationController.sdkVersion())")
        
        return config
    }
    
}

extension UploadDocumentPopUpVC: AuthenticationControllerDelegate {

    func authenticationController(_ authenticationController: AuthenticationController, didFinishInitializingScanViewController scanViewController: AuthenticationScanViewController) {
        print("AuthenticationController did finish initializing")
        self.authenticationScanViewController = scanViewController
        self.present(self.authenticationScanViewController!, animated: true, completion: nil)
    }
    
    /**
     * Implement the following delegate method to receive the final AuthenticationResult.
     * Dismiss the SDK view in your app once you received the result.
     * @param authenticationController the AuthenticationController instance
     * @param authenticationResult contains final authentication result (success or failed)
     * @param transactionReference the unique identifier of the scan session
     **/
    func authenticationController(_ authenticationController: AuthenticationController, didFinishWith authenticationResult: AuthenticationResult, transactionReference: String) {
        print("AuthenticationController finished successfully with transaction reference: \(transactionReference)")
        self.view.hideIndicator()
        
        var message = ""
        switch authenticationResult {
        case .success:
            message = "Authentication process was successful"
            break
        default:
            message = "Authentication process failed"
            break
        }
        
        self.authenticationScanViewController!.dismiss(animated: true, completion: {
            print(message)
            self.showAlert(withTitle: "Authentication Mobile SDK", message: message as String)
            self.authenticationController?.destroy()
            self.authenticationController = nil
        })
    }
    
    /**
     * Implement the following delegate method for successful scans and user cancellation notifications.
     * Dismiss the SDK view in your app once you received the result.
     * @param authenticationController the AuthenticationController instance
     * @param error holds more detailed information about the error reason
     * @param transactionReference the unique identifier of the scan session
     **/
    func authenticationController(_ authenticationController: AuthenticationController, didFinishWithError error: AuthenticationError, transactionReference: String?) {
        let message = "AuthenticationController finished with error: \(error.message) transactionReference: \(transactionReference ?? "")"
        print(message)
        self.view.hideIndicator()
        
        //Dismiss the SDK
        let errorCompletion = {
            self.showAlert(withTitle: "Authentication Mobile SDK", message: message)
            self.authenticationController?.destroy()
            self.authenticationController = nil
        }
        
        if let maybeAuthenticationScanViewController = self.authenticationScanViewController {
            maybeAuthenticationScanViewController.dismiss(animated: true, completion: errorCompletion)
        } else {
            errorCompletion()
        }
    }
    
}
