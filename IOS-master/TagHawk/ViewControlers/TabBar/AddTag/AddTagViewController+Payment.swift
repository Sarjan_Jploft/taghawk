//
//  AddTagViewController+Payment.swift
//  TagHawk
//
//  Created by Appinventiv on 16/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import StoreKit

extension AddTagViewController {
    
     func fetchSubscriptions() {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        self.view.showIndicator()
        IAPController.shared.fetchAvailableProducts(productIdentifiers: [createTagIdentifier], success: { [weak self] (products) in
            
            guard let weakSelf = self else { return }
            guard let firstProd = products.first else { return }
            weakSelf.iapProduct = firstProd
            
            DispatchQueue.main.async {
                weakSelf.view.hideIndicator()
                weakSelf.firstView.isHidden = false
            }
            

            }, failure: { [weak self] (error) in
                guard let weakSelf = self else { return }
                
                 DispatchQueue.main.async {
                weakSelf.view.hideIndicator()
                }
                //                self?.view.hideIndicator()
                
                if let err = error{
                     DispatchQueue.main.async {
                    let alertController = UIAlertController(title: FETCH_PRODUCT_ERROR, message: err.localizedDescription, preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: OK, style: .cancel, handler: nil)
                    alertController.addAction(alertAction)
                    weakSelf.present(alertController, animated: true, completion: nil)
                    }
                }
        })
    }
    
     func purchaseProd(_ product: SKProduct){
        if !AppNetworking.isConnectedToInternet{ CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized); return }
        
        self.indicatorView.startAnimating()
        self.indicatorBackView.isHidden = false

        IAPController.shared.purchaseProduct(product: product) {[weak self] (purchasedPID, restoredPID, failedPID) in
            
            guard let weakSelf = self else { return }
            weakSelf.indicatorView.stopAnimating()
            weakSelf.indicatorBackView.isHidden = true

            if !purchasedPID.isEmpty  {
//                self?.getAppReceipt()
            guard let firstId = purchasedPID.first else { return }
                
            weakSelf.tagData.paymentId = firstId

                weakSelf.controller.addTagService(addTag: weakSelf.tagData) {

               }
            }
        }
    }

//MARK: Get latest app receipt & Update on server
      private func getAppReceipt(){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        DispatchQueue.main.async {
            self.indicatorView.startAnimating()
            self.indicatorBackView.isHidden = false
        }
        
        IAPController.shared.fetchIAPReceipt(sharedSecrete: SHARED_SECRETE, success: {[weak self] (receipt) in
            let receiptInfo = receipt as! ReceiptInfo
           
            guard let weakSelf = self else { return }
         
            DispatchQueue.main.async {
                weakSelf.indicatorView.stopAnimating()
                weakSelf.indicatorBackView.isHidden = true
            }
           
            if let encryptedReceipt = receiptInfo[ApiKey.receipt] as? JSONDictionary {
             
                if let inApp = encryptedReceipt[ApiKey.in_app] as? JSONDictionaryArray, let firstItem = inApp.first, let transId = firstItem[ApiKey.transaction_id] as? String {
                    
                    weakSelf.tagData.paymentId = transId
                    
                    weakSelf.controller.addTagService(addTag: weakSelf.tagData) {
                        
                    }
                    
                }
                
            }
            
        }, failure: {[weak self] (error) in
           // preVc.view.hideIndicator()
            guard let weakSelf = self else { return }
             DispatchQueue.main.async {
                weakSelf.indicatorView.stopAnimating()
                weakSelf.indicatorBackView.isHidden = true
                CommonFunctions.showToastMessage(error?.localizedDescription ?? "")
            }
            
            printDebug(error?.localizedDescription)
        })
    }
}
