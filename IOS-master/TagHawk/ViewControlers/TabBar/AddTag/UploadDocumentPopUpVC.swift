//
//  UploadDocumentPopUpVC.swift
//  TagHawk
//
//  Created by Vikash on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Photos
import Netverify
import NetverifyFace



protocol DocumentDelegate : class {
    func officialIdDocuments(imageUrl: [String])
}

protocol GetImagesBackDelegate : class {
    func getImagesBack(images : [UIImage])
}

enum UploadDocumentFor {
    case joinTag
    case uploadPassport
}

class UploadDocumentPopUpVC: StartViewController {

    @IBOutlet weak var documentCollectionView: UICollectionView!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var popUpView: UIView!
    
    var selectedMediaArray = [Media]()
    var tagData = Tag()
    var controller = JoinTagController()
    var images = [UIImage]()
    var imageUrl = [String]()
//    var isFromEditProfile: Bool = false
    var serviceReturnSuccess: (() -> ())?
    weak var delegate : DocumentDelegate?
    var documentVerificationViewController: DocumentVerificationViewController?
    var authenticationController: AuthenticationController?
    var activityIndicatorView: UIActivityIndicatorView?
    var authenticationScanViewController: UIViewController?
    var uploadFor = UploadDocumentFor.joinTag
//    var isInititedFromPayment: Bool = false
    weak var getImagesBackDelegate : GetImagesBackDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        controller.delegate = self
        // Do any additional setup after loading the view.
        
        if self.uploadFor == .joinTag{
            var descriptionText = "You are applying for " + "\(self.tagData.name)" + " Please upload your "
            descriptionText =  descriptionText + "\(self.tagData.document_type)"
            self.descriptionLabel.text = descriptionText
            self.controller.delegate = self
            self.applyButton.setTitle("Apply")
            self.topLabel.text = LocalizedString.Apply_Community.localized
        }else{
            self.descriptionLabel.text = LocalizedString.Upload_The_First_Page.localized
            self.topLabel.text = LocalizedString.Upload_Passport.localized

        }

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popUpView.roundCorner([.topLeft, .topRight], radius: 25)
    }
    
    //MARK: - IBActions
    @IBAction func clickApplyButton(_ sender: UIButton) {
        view.endEditing(true)
        if selectedMediaArray.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.Please_Select_An_Image.localized)
            return
        }
        
        if self.uploadFor == .uploadPassport{
             uploadIdentity()
        }else{
           uploadDocsToS3()
        }
        
        
//        self.createDocumentVerificationController()
//
//        if let documentVC = self.documentVerificationViewController {
//
//
//
//            self.present(documentVC, animated: true, completion: nil)
//        } else {
//
//            AlertController.alert(title: "DocumentVerification Mobile SDK", message: "DocumentVerificationViewController is nil", buttons: ["Ok"], tapBlock: { (_, index) in
//
//            })
//
//        }
        
    }
    
    @IBAction func clickCancelButton(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickUploadDocumentsButton(_ sender: UIButton) {
        view.endEditing(true)
        let galleryVC = GalleryVC.instantiate(fromAppStoryboard: .AddTag)
        galleryVC.uploadFor = self.uploadFor
        galleryVC.mediaDelegate = self
        self.present(galleryVC, animated: true, completion: {
            if self.uploadFor == .uploadPassport {
               CommonFunctions.showToastMessage(LocalizedString.MaxTwoImages.localized)
            }else{
              CommonFunctions.showToastMessage(LocalizedString.MaxFiveImages.localized)
            }
        })
    }
    
    @objc func removeButtobTapped(sender : UIButton){
        guard let indexPath = sender .collectionViewIndexPath(documentCollectionView) else { return }
        self.selectedMediaArray.remove(at: indexPath.item)
        self.documentCollectionView.reloadData()
    }
}

//MARK: - Extension CollectionView's delegates
extension UploadDocumentPopUpVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, GalleryDelegate, JoinTagDelegate {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedMediaArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let documentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentCollectionViewCell", for: indexPath) as? DocumentCollectionViewCell
        
        let asset = self.selectedMediaArray[indexPath.row].asset
        let manager = PHImageManager.default()
        
        manager.requestImage(for: asset,
                             targetSize: CGSize(width: collectionView.frame.width/3,
                                                height: collectionView.frame.width/3),
                             contentMode: .aspectFit,
                             options: nil) {(result, _) in
                               
                                documentCell?.documentImage.image = result
                                documentCell?.outerView.layer.cornerRadius = 20
                                documentCell?.outerView.layer.borderColor = AppColors.textViewPlaceholderColor.cgColor
                                documentCell?.outerView.layer.borderWidth = 0.5
        }
        
        documentCell?.crossBtn.addTarget(self, action: #selector(removeButtobTapped), for: UIControl.Event.touchUpInside)
        
        return documentCell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func passMediaObject(media: [Media]) {

        self.selectedMediaArray = media
        self.documentCollectionView.reloadData()
        
    }
    
    func joinTagListServiceReturn(msg: String, tagType: PrivateTagType) {
        CommonFunctions.showToastMessage(msg)
        serviceReturnSuccess?()
        self.view.hideIndicator()
        self.dismiss(animated: true, completion: nil)
    }

    
    func willJoinTag() {
        
    }
    
    func joinTagListFailure(errorType: ApiState, message: String) {
         self.view.hideIndicator()
    }
    
    func uploadDocsToS3() {
        self.images.removeAll()
        self.imageUrl.removeAll()
        
        for i in 0..<self.selectedMediaArray.count {
            let asset = self.selectedMediaArray[i].asset
            let manager = PHImageManager.default()
            let options = PHImageRequestOptions()
           options.deliveryMode = .highQualityFormat
           options.isSynchronous = true
            
            manager.requestImage(for: asset,
                                 targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight),
                                 contentMode: .aspectFit,
                                 options: options) { (result, _) in
                                    if let image = result {
                                        self.images.append(image)
                                        
                                    }
            }
        }
        
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        
        DispatchQueue.main.async {
            self.view.showIndicator()
        }
        
        for (index,item) in self.images.enumerated() {
            
            item.uploadImageToS3WithUtility(imageIndex: index, compressionRatio: 0.4,success: { (imageIndex, success, imageUrl) in
                self.imageUrl.append(imageUrl)
                if self.imageUrl.count == self.selectedMediaArray.count {
                    self.uploadDocsToServer()
                }
                self.view.hideIndicator()
            }, progress: { (imageIndex, progress) in
                
            }) { (imageIndex, errer) in
                self.view.hideIndicator()
            }
        }
    }
    
    func uploadDocsToServer() {
        DispatchQueue.main.async {
            self.view.showIndicator()
        }
        
            self.controller.addTagService(addTag: self.tagData, emailPassword: "", joinTagBy: PrivateTagType.documents.rawValue, documents: self.imageUrl)
        
    }
    
    func uploadIdentity(){
        self.images.removeAll()
        self.imageUrl.removeAll()
        
        for i in 0..<self.selectedMediaArray.count {
            let asset = self.selectedMediaArray[i].asset
            let manager = PHImageManager.default()
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.isSynchronous = true
            
            manager.requestImage(for: asset,
                                 targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight),
                                 contentMode: .aspectFit,
                                 options: options) { (result, _) in
                                    if let image = result {
                                        self.images.append(image)
                                        
                                    }
            }
        }
        
        if !AppNetworking.isConnectedToInternet{
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return }
        self.getImagesBackDelegate?.getImagesBack(images: self.images)
        self.dismiss(animated: true, completion: nil)
        
//        let data = self.images.map({$0.pngData()})
//        controller.uploadData(imgData: data)
    }
}

