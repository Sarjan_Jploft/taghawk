//
//  TagShelfVC.swift
//  TagHawk
//
//  Created by Appinventiv on 18/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

class TagShelfVC : BaseVC {
    
    
    enum productViewType {
        case listView
        case gridView
    }
    
    
 
    //MARK:- PROPERTIES
    //=================
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var segmentOuterView: UIView!
    @IBOutlet weak var textFieldBackView: UIView!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var sortingButton: UIButton!
    @IBOutlet weak var sortingTypeLabel: UILabel!
    @IBOutlet weak var sortingArrowImageView: UIImageView!
    @IBOutlet weak var searchBarRightButton: UIButton!
    @IBOutlet weak var searchBarRightImageView: UIImageView!
    
    
    var productView = productViewType.gridView
    private var products = [Product]()
    private var isrefreshed = false
    private let refreshControl = UIRefreshControl()
    private let dropDown = DropDown()
    private let controller = TagProductsController()
    let controler = HomeControler()
    var tagId : String = ""
    var tagName : String = ""
    
    //MARK:- IBOUTLETS
    //================
    
    @IBOutlet weak var mainCollView: UICollectionView! {
        didSet {
            mainCollView.delegate = self
            mainCollView.dataSource = self
//            mainCollView.registerCell(with: ProductCollectionViewCell.self)
            mainCollView.registerNib(nibName: ProductCollectionViewCellGridView.defaultReuseIdentifier)
            mainCollView.registerNib(nibName: ProductCollectionViewCellListView.defaultReuseIdentifier)
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
        self.setSortingText()
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TARGET ACTIONS
    
    
     //MARK:- Like button tapped
        @objc func likeButtonTapped(_ sender: UIButton) {
            view.endEditing(true)
            
            //        if self.product.isCreatedByMe{
            //            let vc = AddProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            //            vc.product = self.product
            //            vc.commingFor = .edit
            //            //vc.refreshDelegate = self
            //            self.present(vc, animated: true, completion: nil)
            //}else{
            
            if !AppNetworking.isConnectedToInternet {
                CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                return
            }
            
    //        self.product = self.products[sender.tag]
            self.controler.likeUnlikeProduct(product: self.products[sender.tag])
    //        self.product.isLiked = !self.product.isLiked
            self.products[sender.tag].isLiked = !self.products[sender.tag].isLiked
            self.setIsLikedStatus(self.products[sender.tag],sender)
            //self.likeStatusBackDeleate?.getLikeStatus(product: self.product)
            //}
            
        }
        
        //MARK:- Set Like Status
        func setIsLikedStatus(_ productItem:Product,_ likeButtonAdd:UIButton){
            if !productItem.isCreatedByMe {
                likeButtonAdd.setImage(productItem.isLiked ? AppImages.filledHeart.image : AppImages.emptyHeart.image, for: UIControl.State.normal)
            }
        }
        
    
    //=====================
    
     //MARK:- Horizontal vertical Show button tapped
        @IBAction func hoziVerShowButtonTapped(_ sender: UIButton) {
            view.endEditing(true)
            
            sender.isSelected = !sender.isSelected
            
            if sender.isSelected{
                self.productView = productViewType.listView
            }else{
                self.productView = productViewType.gridView
            }
    //        homeItemsVc.products.removeAll()
            self.mainCollView.reloadData()
        }
    
    
    
    //MARK:- Categories button tapped
    @IBAction func categoriesButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = CategoryVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.selectionFrom = .shelf
        vc.delegate = self
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    @IBAction func cartButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if UserProfile.main.loginType == .none {
            AppNavigator.shared.tabBar?.showLoginSignupPopup()
            return
        }
        
        let vc = CartVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = FilterVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func sortingButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
        self.dropDown.dismissMode = .automatic
        
        let dataSource = [LocalizedString.Newest.localized, LocalizedString.Closest.localized, LocalizedString.Price_High_To_Low.localized, LocalizedString.Price_Low_To_High.localized]
        self.dropDown.dataSource = dataSource
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            weakSelf.handleItemsSorting(index : index)
        }
    }
    
    @IBAction func searchBarRightButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc = SearchVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.searchFrom = .shelf
        vc.tagId = self.tagId
        vc.tagName = self.tagName
        vc.searchTagsDelegate = self
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: false)
    }
    
    //MARK:- Back button Tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        SelectedFilters.shared.appliedCategoryInShelf = Category()
        self.refreshHome()
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- PRIVATE FUNCTIONS
//========================
extension TagShelfVC {
    
    private func initialSetup(){
        self.searchLabel.setAttributes(text: LocalizedString.Search.localized, font: AppFonts.Galano_Medium.withSize(14) , textColor: AppColors.lightGreyTextColor)
        self.sortingTypeLabel.setAttributes(text: LocalizedString.Newest.localized, font: AppFonts.Galano_Medium.withSize(13), textColor: UIColor.black)
        controller.delegate = self
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.mainCollView)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.mainCollView.addSubview(refreshControl)
        self.mainCollView.alwaysBounceVertical = true
        self.mainCollView.contentInset = UIEdgeInsets(top: 10, left: 4, bottom: 10, right: 4)
        self.setSearchLabelText()
        controller.getTagProducts(tagId, pageNo: page, currentProducts: self.products)
        
        
        
        
    }
    
    @objc func refresh(){
        self.resetPage()
        self.isrefreshed = true
        self.controller.getTagProducts(self.tagId, pageNo: self.page, categoryId: SelectedFilters.shared.appliedCategoryInShelf.categoryId, currentProducts: self.products)
    }
    
    func handleItemsSorting(index : Int){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        switch index {
            
        case 0:
            SelectedFilters.shared.appliedSortingOnProducts = .newest
            
        case 1:
            SelectedFilters.shared.appliedSortingOnProducts = .closest
            
        case 2:
            SelectedFilters.shared.appliedSortingOnProducts = .priceHightToLow
            
        case 3:
            SelectedFilters.shared.appliedSortingOnProducts = .priceLowToHigh
            
        default:
            SelectedFilters.shared.appliedSortingOnProducts = .newest
            
        }
        
        self.setSortingText()
        self.products.removeAll()
        self.mainCollView.reloadData()
        self.resetPage()
        self.controller.getTagProducts(self.tagId, pageNo: self.page, categoryId: SelectedFilters.shared.appliedCategoryInShelf.categoryId, currentProducts: self.products)
    }
    
    func setSortingText(){
        self.sortingTypeLabel.text = SelectedFilters.shared.setSortingAppliedText(sorting: SelectedFilters.shared.appliedSortingOnProducts)
    }
    
    func setSearchLabelText(){
        if !SelectedFilters.shared.appliedCategoryInShelf.description.isEmpty{
            self.searchLabel.attributedText = "\(LocalizedString.Search_In.localized) \(SelectedFilters.shared.appliedCategoryInShelf.description)".attributeStringWithAnotherColor(stringToColor: SelectedFilters.shared.appliedCategoryInShelf.description)
        }else{
            self.searchLabel.text = "\(LocalizedString.Search_In.localized) \(self.tagName)"
        }
    }
}

//MARK:- UICollectionView Delegate and DataSource
extension TagShelfVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HomeHeaserView", for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 2
       // return CGSize(width: finalContentWidth / 2, height: finalContentWidth / 2)
        
        if self.productView == .gridView{
            
            return CGSize(width: finalContentWidth / 2, height: finalContentWidth / 2 + 68)
        }else{
            
            return CGSize(width: finalContentWidth, height: 138.0)//finalContentWidth / 2 - 20)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getProductCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
//        let cell = collectionView.dequeueCell(with: ProductCollectionViewCell.self, indexPath: indexPath)
//        cell.cellFor = .home
//        cell.populateData(product: self.products[indexPath.item])
//        return cell
        
        
        if self.productView == .gridView{
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCellGridView.defaultReuseIdentifier, for: indexPath) as? ProductCollectionViewCellGridView else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
            cell.cellFor = .home
            cell.populateData(product: self.products[indexPath.item])
            cell.btnLike.tag = indexPath.item
            cell.btnLike.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
            return cell
            
        }else{
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCellListView.defaultReuseIdentifier, for: indexPath) as? ProductCollectionViewCellListView else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
            cell.cellFor = .home
            cell.populateData(product: self.products[indexPath.item])
            cell.btnLike.tag = indexPath.item
            cell.btnLike.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
            return cell
            
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ProductDetailVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.prodId = self.products[indexPath.item].productId
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if products.count >= 14 {
            if indexPath.item == self.products.count - 1 && self.nextPage != 0{
                controller.getTagProducts(tagId, pageNo: page, currentProducts: self.products)
            }
        }
    }
}



extension TagShelfVC: TagProductsControllerDelegate {
    func willGetProducts() {
        if self.page == 1{
            self.shouldDisplayEmptyDataView = false
            self.mainCollView.reloadEmptyDataSet()
            if !isrefreshed {
                self.view.showIndicator()
            }
        }
    }
    
    func getProductSuccess(prodArr: [Product], nextPage: Int) {
        self.view.hideIndicator()
        
        if self.page == 1{
            self.products.removeAll()
            self.mainCollView.reloadData()
        }
        
        if nextPage != 0{
            self.page = nextPage
        }
        self.nextPage = nextPage
        products.append(contentsOf: prodArr)
        mainCollView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.mainCollView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
    func getProductFailure(_ message: String) {
        CommonFunctions.showToastMessage(message)
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = true
        self.mainCollView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
}

extension TagShelfVC : SelectedCategory  {
    
    func selectCategory(category: Category) {
        self.resetPage()
        self.products.removeAll()
        self.mainCollView.reloadData()
        SelectedFilters.shared.appliedCategoryInShelf = category
        self.setSearchLabelText()
        self.controller.getTagProducts(self.tagId, pageNo: self.page, categoryId: category.categoryId, currentProducts: self.products)
    }
    
    func selectCategory(selectedCategoryId : String, categoryName : String) {
        
    }
}


extension TagShelfVC : ApplyFilter {
    
    func applyFilter(){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        self.resetPage()
        self.products.removeAll()
        self.mainCollView.reloadData()
        controller.getTagProducts(tagId, pageNo: page, currentProducts: self.products)
    }
    
}


extension TagShelfVC : GetSearchedTags {
    
    func getSearchedTags(tags: [Tag]) {
        
    }
    
    func getSearchedTitle(title: String) {
        
    }
    
}
