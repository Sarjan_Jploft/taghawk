//
//  TagDetailVC.swift
//  TagHawk
//
//  Created by Appinventiv on 18/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Netverify

enum JoinTagStatus: Int {
    case INITIAL = 0
    case ACTIVE = 1
    case REJECT = 2
    case REMOVE = 3
    case PENDING = 4
}

class TagDetailVC : BaseVC {
    
    //MARK:- IbOutlets
    @IBOutlet weak var productDetailTableView: UITableView!
    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var tagImageView: UIImageView!
    @IBOutlet weak var numberOfMembersLabel: UILabel!
    @IBOutlet weak var numberOfMembersBackView: UIView!
    @IBOutlet weak var publicOrPrivateLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var chatBtn: UIButton!
    
    
    //MARK:- Variables
    var tag = Tag()
    var tagId : String = ""
    weak var likeStatusBackDeleate : GetLikeStatusBack?
    private let controler = TagDetailControler()
    let joinController = JoinTagController()
    var netverifyViewController:NetverifyViewController?
    var customUIController:NetverifyUIController?
    private var roomInfo: ChatListing?
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setUpSubView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tagImageView.round()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SingleChatFireBaseController.shared.delegate = self
        GroupChatFireBaseController.shared.delegate = self
        self.configureNavigation()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        SingleChatFireBaseController.shared.removeObservers(roomId: "", otherUserId: "")
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
        self.joinController.delegate = self
        self.joinController.cancelDelegate = self
    }
    
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func rightBarButtonTapped(_ sender: UIBarButtonItem) {
        let subject = tag.name + " Tag's link"
        self.shareUrl(url: self.tag.link, subject: subject)
//        self.controler.shareProduct(product: self.product)
//        CommonFunctions.shareWithSocialMedia(message: self.product.sharingUrl, vcObj: self)
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        
        if UserProfile.main.loginType == .none {
            AppNavigator.shared.tabBar?.showLoginSignupPopup()
            return
        }
        
        
        if tag.isCreatedByMe {
            let addTagScene = AddTagViewController.instantiate(fromAppStoryboard: .AddTag)
            addTagScene.tagData = tag.getAddTagData()
            addTagScene.tagId = tag.id
            addTagScene.isEditingTag = true
            addTagScene.referVcCancelBtnTap = { [weak self] in
                self?.controler.getTagDetail(id: self?.tagId ?? "")
            }
            navigationController?.pushViewController(addTagScene, animated: true)
            return
        }
        
        switch tag.requestStatus {
            
        case JoinTagStatus.ACTIVE.rawValue:
                let scene = GroupChatVC.instantiate(fromAppStoryboard: .Messages)
                scene.roomID = tag.id
                scene.roomData = roomInfo
                navigationController?.pushViewController(scene, animated: true)
        case JoinTagStatus.PENDING.rawValue:
            if self.tag.entrance == .privateTag{
                CommonFunctions.showToastMessage(LocalizedString.yourApplicationPending.localized)
                self.showPopup(type: CustomPopUpVC.CustomPopUpFor.cancelPendingRequest)
                return
            }
        case JoinTagStatus.REJECT.rawValue, JoinTagStatus.REMOVE.rawValue: return
            
        default:
            
            switch self.tag.entrance{
                
            case .privateTag:
                switch self.tag.joinTagBy {
                    
                case PrivateTagType.email.rawValue:
                    let emailVC = CustomEmailPasswordPopVC.instantiate(fromAppStoryboard: .AddTag)
                    emailVC.isEmailVerify = true
                    emailVC.delegate = self
                    emailVC.tagData = self.tag
                    emailVC.modalPresentationStyle = .overCurrentContext
                    self.present(emailVC, animated: true, completion: nil)
                    
                case PrivateTagType.password.rawValue:
                    let passwordVC = CustomEmailPasswordPopVC.instantiate(fromAppStoryboard: .AddTag)
                    passwordVC.isEmailVerify = false
                    passwordVC.delegate = self
                    passwordVC.tagData = self.tag
                    passwordVC.modalPresentationStyle = .overCurrentContext
                    self.present(passwordVC, animated: true, completion: nil)
                    
                case PrivateTagType.documents.rawValue:
                    let documentVC = UploadDocumentPopUpVC.instantiate(fromAppStoryboard: .AddTag)
                    documentVC.serviceReturnSuccess = {
                        sender.setTitle(LocalizedString.Pending.localized)
                        self.tag.requestStatus = JoinTagStatus.PENDING.rawValue
                        GroupChatFireBaseController.shared.increasePendingRequestCount(roomId: self.tag.id)
                    }
                    documentVC.tagData = self.tag
                    documentVC.modalPresentationStyle = .overCurrentContext
                    self.present(documentVC, animated: true, completion: nil)
                    
//                    self.showPopup(type: CustomPopUpVC.CustomPopUpFor.documentVerify,msg: LocalizedString.Upload_Document_To_Apply_For_Tag.localized.replacingOccurrences(of: "tagname", with: self.tag.name))
                    
                default: return
                    
                }
                
            case .publicTag:
                joinController.addTagService(addTag: tag,
                                             emailPassword: "",
                                             joinTagBy: 0,
                                             documents: [])
                
            default: return
            }
        }
    }
    
    @IBAction func chatBtnAction(_ sender: UIButton) {
        let scene = GroupChatVC.instantiate(fromAppStoryboard: .Messages)
        scene.roomID = tag.id
        scene.roomData = roomInfo
        navigationController?.pushViewController(scene, animated: true)
    }
}

extension TagDetailVC: CustomEmailPasswordPopVCDelegate{
    
    func suusscefullyAddTag(isEmail: Bool){
        
        if !isEmail{
            
            applyButton.setTitle(LocalizedString.Chat.localized)
            self.tag.requestStatus = JoinTagStatus.ACTIVE.rawValue
            self.tag.members += 1
            let membersStrin = self.tag.members == 1 ? LocalizedString.Member.localized : LocalizedString.Members.localized
            self.numberOfMembersLabel.text = "\(self.tag.members) \(membersStrin)"
            GroupChatFireBaseController.shared.joinGroup(tagData: tag,
                                                         data: UserProfile.main)
        }else{
            applyButton.setTitle(LocalizedString.Pending.localized)
            self.tag.requestStatus = JoinTagStatus.PENDING.rawValue
        }
    }
}

extension TagDetailVC {
    
    func setUpSubView(){
        
//        self.tableHeaderView.frame.size.height = screenWidth
        
        self.tagImageView.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.publicOrPrivateLabel.setAttributes(text: LocalizedString.Private.localized, font : AppFonts.Galano_Semi_Bold.withSize(14) , textColor: AppColors.appBlueColor)
//        self.numberOfMembersBackView.drawShadow()
//        self.numberOfMembersBackView.clipsToBounds = false
        
        self.numberOfMembersBackView.setBorder(width: 0.5, color: AppColors.textfield_Border)
        
        self.numberOfMembersBackView.round(radius: 10)
        
        self.configureTableView()
        self.applyButton.round(radius: 10)
        self.applyButton.setAttributes(title: LocalizedString.Apply.localized, font: AppFonts.Galano_Semi_Bold.withSize(16) , titleColor: UIColor.white, backgroundColor: UIColor.clear)
        
        chatBtn.isHidden = true
        self.chatBtn.round(radius: 10)
        self.chatBtn.setAttributes(title: LocalizedString.Chat.localized, font: AppFonts.Galano_Semi_Bold.withSize(16) , titleColor: UIColor.white, backgroundColor: UIColor.clear)
        self.controler.getTagDetail(id: self.tagId)
        
        
        SingleChatFireBaseController.shared.checkUserBlocked()
    }
    
    func updateDataFromPush(data: Tag){
        
        applyButton.setTitle(LocalizedString.Chat.localized)
        tag.members += 1
        let membersStrin = tag.members == 1 ? LocalizedString.Member.localized : LocalizedString.Members.localized
        numberOfMembersLabel.text = "\(tag.members) \(membersStrin)"
        tag.requestStatus = JoinTagStatus.ACTIVE.rawValue
        GroupChatFireBaseController.shared.joinGroup(tagData: data,
                                                     data: UserProfile.main)
    }
    
    func updatedChatStatus(id: String){
        
        if tag.id == id{
            applyButton.setTitle(LocalizedString.Chat.localized)
            tag.members += 1
            let membersStrin = tag.members == 1 ? LocalizedString.Member.localized : LocalizedString.Members.localized
            numberOfMembersLabel.text = "\(tag.members) \(membersStrin)"
            tag.requestStatus = JoinTagStatus.ACTIVE.rawValue
//            chatController.toCheckRoomExist(id: tag.id)
        }
    }
    
    func configureTableView(){
        
        self.productDetailTableView.registerNib(nibName: TagDetailAndDescriptionCellTableViewCell.defaultReuseIdentifier)
        
        //..........
        
        self.productDetailTableView.registerNib(nibName: ProduceDetailAndDescriptionCell.defaultReuseIdentifier)
        self.productDetailTableView.registerNib(nibName: ProductDetailRatingCell.defaultReuseIdentifier)
        self.productDetailTableView.registerNib(nibName: ProductDetailMapCell.defaultReuseIdentifier)
        self.productDetailTableView.registerNib(nibName: SimilatProductsCell.defaultReuseIdentifier)
        self.productDetailTableView.rowHeight = UITableView.automaticDimension
        self.productDetailTableView.separatorStyle = .none
        self.productDetailTableView.estimatedRowHeight = 100
        self.productDetailTableView.delegate = self
        self.productDetailTableView.dataSource = self
    }
 
    func populateHeaderData(){
        self.navigationItem.title = self.tag.name
        self.tagImageView.setImage_kf(imageString: self.tag.tagImageUrl, placeHolderImage: AppImages.productPlaceholder.image)
        let membersStrin = self.tag.members == 1 ? LocalizedString.Member.localized : LocalizedString.Members.localized
        self.numberOfMembersLabel.text = "\(self.tag.members) \(membersStrin)"
        //Temp Code*
//        self.publicOrPrivateLabel.text = SelectedFilters.shared.getTagTypeStringValue(from: self.tag.type).rawValue
        self.publicOrPrivateLabel.text = SelectedFilters.shared.getEntraceTypeStringValue(from: self.tag.entrance).rawValue
       
        if self.tag.requestStatus == JoinTagStatus.ACTIVE.rawValue {
            self.applyButton.setTitle(LocalizedString.Chat.localized)
            
        } else if self.tag.requestStatus == JoinTagStatus.REJECT.rawValue {
            if tag.entrance == .publicTag {
                self.applyButton.setTitle(LocalizedString.Join.localized)
            } else {
                self.applyButton.setTitle(LocalizedString.Apply.localized)
            }
        } else if self.tag.requestStatus == JoinTagStatus.PENDING.rawValue {
             self.applyButton.setTitle(LocalizedString.Pending.localized)
        }
        if tag.entrance == .publicTag {
            self.applyButton.setTitle(LocalizedString.Join.localized)
            if tag.requestStatus == JoinTagStatus.ACTIVE.rawValue {
                self.applyButton.setTitle(LocalizedString.Chat.localized)
            }
        }
        if tag.isCreatedByMe {
            self.applyButton.setTitle(LocalizedString.Edit.localized)
            chatBtn.isHidden = false
            GroupChatFireBaseController.shared.toCheckRoomExist(id: tag.id)
        }
    }
    
    func showPopup(type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none, msg : String = ""){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        vc.msg = msg
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
}

//MARK:- Configure Navigation
private extension TagDetailVC {
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.rightBarItemImage(change: AppImages.share.image, ofVC: self)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
        self.navigationItem.title = self.tag.name
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
}


extension TagDetailVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            
            return UITableView.automaticDimension
            
        default:
//            before
//            let heightOfOneProd = (screenWidth - 35) / 3
//            let multiplier : CGFloat = self.tag.products.count > 3 ? 2 : 1
//            let totalCellHeight = heightOfOneProd * multiplier
//            let calculatedHeight = self.tag.products.isEmpty ? 0 : totalCellHeight + 90
//            return self.tag.products.isEmpty ? 100 : calculatedHeight
            
//            after
            let heightOfOneProd = (screenWidth - 35) / 2
            let multiplier : CGFloat = self.tag.products.count > 2 ? 2 : 1
            let totalCellHeight = heightOfOneProd * multiplier
            let calculatedHeight = self.tag.products.isEmpty ? 0 : totalCellHeight + 200
            return self.tag.products.isEmpty ? 100 : calculatedHeight

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.getDetailAndDescriptionCell(tableView, indexPath: indexPath)
     
        default:
            return self.getSimilarProductsCell(tableView, indexPath: indexPath)
        }
    }
    
    func getDetailAndDescriptionCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TagDetailAndDescriptionCellTableViewCell.defaultReuseIdentifier) as? TagDetailAndDescriptionCellTableViewCell else { fatalError("SettingsVC....\(ProduceDetailAndDescriptionCell.defaultReuseIdentifier) cell not found") }
        cell.populateData(tag: self.tag)
        return cell
        
    }

    func getSimilarProductsCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SimilatProductsCell.defaultReuseIdentifier, for: indexPath) as? SimilatProductsCell else { fatalError("Could not dequeue SimilatProductsCell at index \(indexPath) in LoginVC") }
        cell.delegate = self
        cell.setUpFor = .tagProducts
        cell.products = self.tag.products
        cell.viewAllButton.isHidden = self.tag.products.count > 5 ? false : true
        cell.similarProductsCollectionView.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


extension TagDetailVC : SimilarProductTappedDelegate {
    
    
    func viewAllBtnTapped(_ sender: UIButton) {
        let tagShelfVC = TagShelfVC.instantiate(fromAppStoryboard: .AddTag)
        tagShelfVC.tagId = tag.id
        tagShelfVC.tagName = self.tag.name
        navigationController?.pushViewController(tagShelfVC, animated: true)
    }
    
    func similarProductTapped(product : Product) {
        let vc = ProductDetailVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.prodId = product.productId
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension TagDetailVC : TagDetaulControlerDelegate {
   
    func willRequestTagDetail() {
        self.view.showIndicator()
        self.productDetailTableView.isHidden = true
        self.buttonContainerView.isHidden = true
    }
    
    func tagDetailReceivedSuccessFully(tag : Tag) {
        self.view.hideIndicator()
        self.tag = tag
        
        if self.tag.isCreatedByMe{
           GroupChatFireBaseController.shared.toCheckRoomExist(id: self.tag.id)
        }else{
            switch self.tag.requestStatus {
                
            case JoinTagStatus.ACTIVE.rawValue:
                GroupChatFireBaseController.shared.toCheckRoomExist(id: self.tag.id)
            case JoinTagStatus.PENDING.rawValue: break
                
            case JoinTagStatus.REJECT.rawValue: break
                
            case JoinTagStatus.REMOVE.rawValue: break
                
            default: break
            }
        }        
        
        self.populateHeaderData()
        self.productDetailTableView.reloadData()
        self.productDetailTableView.isHidden = false
        self.buttonContainerView.isHidden = false
    }

    func failedToReceiveTagDetail(message: String) {
         self.view.hideIndicator()
        showPopup(type: .notFound, msg: message)
    }
}

extension TagDetailVC: JoinTagDelegate {
   
    func willJoinTag(){
        self.view.showIndicator()
    }
    
    func joinTagListServiceReturn(msg: String, tagType: PrivateTagType) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)

        if tagType == .documents {
            self.applyButton.setTitle(LocalizedString.Pending.localized)
            self.tag.requestStatus = JoinTagStatus.PENDING.rawValue
            GroupChatFireBaseController.shared.increasePendingRequestCount(roomId: self.tag.id)
        }else{
            self.tag.requestStatus = JoinTagStatus.ACTIVE.rawValue
            GroupChatFireBaseController.shared.joinGroup(tagData: tag, data: UserProfile.main)
        }
    }
    
    func joinTagListFailure(errorType: ApiState, message : String) {
        CommonFunctions.showToastMessage(message)
    }
}

extension TagDetailVC: CustomPopUpDelegateWithType {
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
       
        if type == .notFound {
            navigationController?.popViewController(animated: true)
        }
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
    
        if type == .cancelPendingRequest{
            
            self.joinController.cancelTagRequest(tagID: self.tag.id, userId: UserProfile.main.userId)
            
        }else{
            self.createNetverifyController()
            if let netverifyVC = self.netverifyViewController {
                self.present(netverifyVC, animated: true, completion: nil)
            } else {
                
                _ = AlertController.alert(title: "Netverify Mobile SDK", message: "NetverifyViewController is nil", buttons: [LocalizedString.ok.localized]) { (_, index) in
                    
                }
            }
          }
        }
}

extension TagDetailVC: GroupChatFireBaseControllerDelegate {
    
    func getRoomData() {
        
      applyButton.setTitle(LocalizedString.Chat.localized)
      tag.requestStatus = 1
    }
    
    func roomExist(){
        if !tag.isCreatedByMe{
            self.applyButton.setTitle(LocalizedString.Chat.localized)
        }
    }
    
    func roomNotExist(){
        GroupChatFireBaseController.shared.joinGroup(tagData: tag, data: UserProfile.main)
    }
}

extension TagDetailVC: SingleChatFireBaseControllerDelegate {
    
    func userBlockedByAnother(roomData: ChatListing){
        if roomData.roomID == tag.id{
            showPopup(type: .notFound, msg: "You are blocked by owner.")
        }
    }
    
    func getRoomData(info: ChatListing) {
        roomInfo = info
        if !tag.isCreatedByMe{
            self.applyButton.setTitle(LocalizedString.Chat.localized)
        }
    }
}


extension TagDetailVC : CancelPendingRequestDelegate {
    
    func willCancelPendingRequest(){
        self.view.showIndicator()
    }
    
    func cancelPendingRequestSuccessfully(){
        self.view.hideIndicator()
        self.applyButton.setTitle(LocalizedString.Apply.localized)
        self.tag.requestStatus = JoinTagStatus.INITIAL.rawValue
    }
    
    func failedToCancelPendingRequest(msg : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(msg)
    }
    
}
