//
//  TagDetailVC+NetVerify.swift
//  TagHawk
//
//  Created by Admin on 5/3/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import Netverify

extension TagDetailVC : NetverifyViewControllerDelegate {
    
    func createNetverifyController() -> Void {
        
        //prevent SDK to be initialized on Jailbroken devices
        if JumioDeviceInfo.isJailbrokenDevice() {
            return
        }
        
        //Setup the Configuration for Netverify
        let config:NetverifyConfiguration = createNetverifyConfiguration()
        //Set the delegate that implements NetverifyViewControllerDelegate
        config.delegate = self

        //Perform the following call as soon as your app’s view controller is initialized. Create the NetverifyViewController instance by providing your Configuration with required API token, API secret and a delegate object.
        
        do {
            try ObjcExceptionHelper.catchException {
                self.netverifyViewController = NetverifyViewController(configuration: config)
            }
        } catch {
            let err = error as NSError
           _ = AlertController.alert(title: err.localizedDescription, message: err.userInfo[NSLocalizedFailureReasonErrorKey] as? String ?? "", buttons: [LocalizedString.ok.localized]) { (_, index) in
                
            }
        }
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            self.netverifyViewController?.modalPresentationStyle = UIModalPresentationStyle.formSheet;  // For iPad, present from sheet
        }
    }
    
    func createNetverifyConfiguration() -> NetverifyConfiguration {
        let config:NetverifyConfiguration = NetverifyConfiguration()
        config.apiToken = "ed000985-93d5-4837-91c8-3a266c2c83b5"
        config.apiSecret = "yIMeTuvMkeNCUCffNeT2sf904Gr2v6qT"
        config.enableIdentityVerification = false
        return config
    }
    
    /**
     * Implement the following delegate method for SDK initialization.
     * @param netverifyViewController The NetverifyViewController instance
     * @param error The error describing the cause of the problematic situation, only set if initializing failed
     **/
    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didFinishInitializingWithError error: NetverifyError?) {
        print("NetverifyViewController did finish initializing")
    }
    
    /**
     * Implement the following delegate method for successful scans.
     * Dismiss the SDK view in your app once you received the result.
     * @param netverifyViewController The NetverifyViewController instance
     * @param documentData The NetverifyDocumentData of the scanned document
     * @param scanReference The scanReference of the scan
     **/
    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didFinishWith documentData: NetverifyDocumentData, scanReference: String) {
        print("NetverifyViewController finished successfully with scan reference: %@", scanReference)
        // Share the scan reference for the Authentication SDK
        
        self.joinController.addTagService(addTag: self.tag, emailPassword: "", joinTagBy: PrivateTagType.documents.rawValue, documents: [], jumioRefrenceId: scanReference)
        
        self.dismiss(animated: true, completion: {
            self.netverifyViewController?.destroy()
            self.netverifyViewController = nil
        })
    }
    
    /**
     * Implement the following delegate method for successful scans and user cancellation notifications. Dismiss the SDK view in your app once you received the result.
     * @param netverifyViewController The NetverifyViewController
     * @param error The error describing the cause of the problematic situation
     * @param scanReference The scanReference of the scan attempt
     **/
    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didCancelWithError error: NetverifyError?, scanReference: String?) {
        print("NetverifyViewController cancelled with error: \(error?.message ?? "") scanReference: \(scanReference ?? "")")
        
        //Dismiss the SDK
        self.dismiss(animated: true) {
            self.netverifyViewController?.destroy()
            self.netverifyViewController = nil
        }
    }
    
}
