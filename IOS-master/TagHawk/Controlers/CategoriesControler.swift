//
//  CategoriesControler.swift
//  TagHawk
//
//  Created by Appinventiv on 24/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol GetCategoriesDelegate : class{
    func willRequestCategories()
    func categoriesReceivedSuccessFully(categories : [Category])
    func failedToReceiveCategories()
}

class CategoriesControler {

    weak var delegate : GetCategoriesDelegate?
    
    //MARK:- get categories webservices
    func getCategories(){
        
        self.delegate?.willRequestCategories()
        
        WebServices.getCategories(parameters: [:], success: { (categories) in
            AppSharedData.shared.categories = categories
            self.delegate?.categoriesReceivedSuccessFully(categories: categories)
            
        }) { (error) -> (Void) in
            self.delegate?.failedToReceiveCategories()
        }
        
    }
    
}
