//
//  RateOptionsController.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 01/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

protocol RateOptionsControllerDelegate: AnyObject {
    func ratedSuccessfully()
}

class RateOptionsController {
    
    weak var delegate: RateOptionsControllerDelegate?
    
    //MARK:- Send rating service
    func sendRatings(sellerId: String, prodId: String, rating: Double, comment: String) {
        
        let params: JSONDictionary = [ApiKey.sellerId: sellerId, ApiKey.productId: prodId, ApiKey.rating: rating, ApiKey.comment: comment]
        
        WebServices.postRating(parameters: params, success: { (json) in
            
            self.delegate?.ratedSuccessfully()
            
        }) { (err) -> (Void) in
            
        }
    }
}
