    //
    //  AddProductControler.swift
    //  TagHawk
    //
    //  Created by Appinventiv on 20/02/19.
    //  Copyright © 2019 TagHawk. All rights reserved.
    //
    
    import UIKit
    import SwiftyJSON
    import StoreKit
    
    
    protocol GetTagUsersDelegate : class {
        func willRequestTagUsers()
        func userTagsReceivedSuccessFully(tags : [Tag], nextPage: Int)
        func failedToReceiveTagUsers(message : String)
    }
    
    protocol AddProductDelegate : class{
        func willAddProduct()
        func addProductSuccessFully(prodId : String, sharingUrl : String)
        func failedToAddproduct(message : String)
        func validateInput(isValid : Bool, message : String)
    }
    
    protocol FeatureProductDelegate : class {
        
        func willRequestFeatureProduct()
        func productFeaturedSuccessFully()
        func failedToFeatureProduct()
        
    }
    
    protocol EditProductDelegate : class {
        
        func willRequestEditProduct()
        func editProductSuccessFully()
        func errorOccuredWhileEditProduct(message : String)
        func validateInputToEdit(isValid : Bool, message : String)
        
    }
    
    
    protocol UploadImagesDelegate : class {
        
        func willStartUploadingImages()
        func imagesUploadedSuccessfullyWithSequence(allImagesStatus : [(index : Int, stringUrl : String, img : UIImage?)])
        func imagesUploadedSuccessfully(allImagesStatus : [Int : (isSuccess : Bool, url : String)])
        
    }
    
    class AddProductControler : NSObject {
        
        weak var delegate : GetTagUsersDelegate?
        weak var addProductDelegate : AddProductDelegate?
        weak var featureDelegate : FeatureProductDelegate?
        weak var editProductDelegate : EditProductDelegate?
        weak var uploadImagesDelegate : UploadImagesDelegate?
        
        
        //MARK:- Validate add product
        func validateAddProduct(with data : AddProductData,validateFor : AddProductVC.AddProductFor = .add) -> Bool {
            
            if data.productImages.isEmpty {
                self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Please_Select_An_Image.localized)
                return false
            }
            
            if data.description.isEmpty {
                self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.PleaseEnterDescription.localized)
                return false
            }
            
            if data.title.isEmpty{
                
                validateFor == .add ? self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Please_Enter_Product_Title.localized) : self.editProductDelegate?.validateInputToEdit(isValid: false, message: LocalizedString.Please_Enter_Product_Title.localized)
                return false
            }
            
            if data.category.categoryId.isEmpty{
                
                validateFor == .add ? self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Please_Enter_Product_Category.localized) : self.editProductDelegate?.validateInputToEdit(isValid: false, message: LocalizedString.Please_Enter_Product_Category.localized)
                
                return false
            }
            
            if data.price > 0 && data.price < 0.8 {
                self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.invalidPrice.localized)
                return false
            }
            
            if data.price == 0 && data.shippingAvailability.contains(.shipping) {
                self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.cannotBeShippedWith0.localized)
                return false
            }
            
            if data.condition == .none{
                self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Please_Select_Product_Condition.localized)
                return false
            }
            
            if data.shippingAvailability.isEmpty{
                self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Please_Select_Shipping_Availability.localized)
                return false
            }
            
            if data.location.isEmpty || data.lat == 0 || data.long == 0 {
                self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Please_Select_Location.localized)
                return false
            }
            
            if data.shippingAvailability.contains(ShippingAvailability.shipping){
                
                if data.shippingData.weight.isEmpty{
                    self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Please_Selecte_Product_Weight.localized)
                    return false
                }
                
                if data.selectedShippingMode == .none{
                    self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Please_Select_Shipping_Mode.localized)
                    return false
                }
                
                if data.selectedShippingMode == .fedex && !data.shippingData.isAvailableInFedex {
                    self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Selected_weight_Not_Avail_With_Shipping_Mode.localized)
                    return false
                }
                
                if data.selectedShippingMode == .usps && !data.shippingData.isAvailableInUsps {
                    self.addProductDelegate?.validateInput(isValid: false, message: LocalizedString.Selected_weight_Not_Avail_With_Shipping_Mode.localized)
                    return false
                }
            }
            
            return true
        }
        
        //MARK:- get user tag
        func getuserTags(page : Int){
            
            self.delegate?.willRequestTagUsers()
            
            let params : JSONDictionary = [ApiKey.pageNo : page, ApiKey.limit : "50"]
            
            WebServices.getuserTags(parameters: params, success: {[weak self] (json) in
                
                guard let weakSelf = self else { return }
                weakSelf.delegate?.userTagsReceivedSuccessFully(tags: json[ApiKey.data][ApiKey.data].arrayValue.map { Tag(json: $0) }, nextPage: json[ApiKey.next_hit].intValue)
                
            }) {[weak self] (error) -> (Void) in
                guard let weakSelf = self else { return }
                weakSelf.delegate?.failedToReceiveTagUsers(message: error.localizedDescription)
            }
        }
        
        //MARK:- Add product service
        func addProduct(data : AddProductData){
            
            self.addProductDelegate?.willAddProduct()
            var params : JSONDictionary = [ApiKey.title : data.title]
            params[ApiKey.productCategoryId] = data.category.categoryId
            params[ApiKey.firmPrice] = data.price
//            params[ApiKey.isNegotiable] = data.isNegotiable ? "false" : "true"
            params[ApiKey.isNegotiable] = data.isNegotiable ? "true" : "false"
            params[ApiKey.isTransactionCost] = data.isTransactionCost ? "true" : "false"
            params[ApiKey.condition] = data.condition.rawValue
            params[ApiKey.description] = data.description
            params[ApiKey.city] = data.city
            params[ApiKey.state] = data.state
            let shippingAvailabilityValues = data.shippingAvailability.map { "\($0.rawValue)" }
            
            params[ApiKey.shippingAvailibility] = shippingAvailabilityValues.joined(separator: ",")
            
            params[ApiKey.location] = data.location
            if !data.sharedCommunities.isEmpty{
                params[ApiKey.sharedCommunities] = data.sharedCommunities.map { $0.id }.joined(separator: ",")
            }
            
            params[ApiKey.lat] = data.lat
            params[ApiKey.long] = data.long
            
            let images = data.productImages.filter { !$0.stringUrl.isEmpty }
            
            let dictObj = images.map { (item) -> JSONDictionary in
                var dict = JSONDictionary()
                dict[ApiKey.url] = item.stringUrl
                dict[ApiKey.thumbUrl] = item.stringUrl
                return dict
            }
            
            params[ApiKey.images] = JSON(dictObj)
            
            if data.shippingAvailability.contains(ShippingAvailability.shipping){
                
                params[ApiKey.shippingPrice] = data.selectedShippingMode == .fedex ? "\(data.shippingData.fedexPrice)" : "\(data.shippingData.uspsPrice)"
                params[ApiKey.weight] = data.shippingData.weight
                params[ApiKey.shippingType] = data.selectedShippingMode.rawValue
            }
            
            WebServices.addProduct(parameters: params, success: {[weak self ] (json) in
                
                let id = json[ApiKey.data][ApiKey._id].stringValue
                let sharingUrl = json[ApiKey.data][ApiKey.link].stringValue
                guard let weakSelf = self else { return }
                weakSelf.addProductDelegate?.addProductSuccessFully(prodId: id, sharingUrl: sharingUrl)
                
            }) {[weak self] (error) -> (Void) in
                guard let weakSelf = self else { return }
                weakSelf.addProductDelegate?.failedToAddproduct(message: error.localizedDescription)
            }
        }
        
        //MARK:- Edit product service
        func editProduct(data : AddProductData){
            
            if !self.validateAddProduct(with: data, validateFor: AddProductVC.AddProductFor.edit){
                return
            }
            
            self.editProductDelegate?.willRequestEditProduct()
            
            var params : JSONDictionary = [ApiKey.title : data.title]
            params[ApiKey.productId] = data.prodId
            params[ApiKey.productCategoryId] = data.category.categoryId
            params[ApiKey.firmPrice] = data.price
            params[ApiKey.isNegotiable] = data.isNegotiable ? "true" : "false"
            params[ApiKey.condition] = data.condition.rawValue
            params[ApiKey.description] = data.description
            
            let shippingAvailabilityValues = data.shippingAvailability.map { "\($0.rawValue)" }
            
            params[ApiKey.shippingAvailibility] = shippingAvailabilityValues.joined(separator: ",")
            
            params[ApiKey.location] = data.location
            params[ApiKey.city] = data.city
            params[ApiKey.state] = data.state
            
            
            if !data.sharedCommunities.isEmpty{
                params[ApiKey.sharedCommunities] = data.sharedCommunities.map { $0.id }.joined(separator: ",")
            }
            
            params[ApiKey.lat] = data.lat
            params[ApiKey.long] = data.long
            
            if data.shippingAvailability.contains(ShippingAvailability.shipping){
                
                params[ApiKey.shippingPrice] = data.selectedShippingMode == .fedex ? "\(data.shippingData.fedexPrice)" : "\(data.shippingData.uspsPrice)"
                params[ApiKey.weight] = data.shippingData.weight
                params[ApiKey.shippingType] = data.selectedShippingMode.rawValue
            }
            
            let images = data.productImages.filter { !$0.stringUrl.isEmpty }
            
            let dictObj = images.map { (item) -> JSONDictionary in
                var dict = JSONDictionary()
                dict[ApiKey.url] = item.stringUrl
                dict[ApiKey.thumbUrl] = item.stringUrl
                return dict
            }
            
            params[ApiKey.images] = JSON(dictObj)
            
            WebServices.editProduct(parameters: params, success: {[weak self] (json) in
                
                guard let weakSelf = self else { return }
                weakSelf.editProductDelegate?.editProductSuccessFully()
                
            }) {[weak self] (error) -> (Void) in
                
                guard let weakSelf = self else { return }
                weakSelf.editProductDelegate?.errorOccuredWhileEditProduct(message: error.localizedDescription)
            }
        }
        
        
        func uploadImagesWithSequence(allImagesToUpload : [(index : Int, stringUrl : String, img : UIImage?)]){
            
            var successCount = 0
            var failedCount = 0
            var allImagesStatus = allImagesToUpload
            let existingImagesCount = allImagesToUpload.filter { !$0.stringUrl.isEmpty }.count
            
            self.uploadImagesDelegate?.willStartUploadingImages()
            
            for (index,item) in allImagesStatus.enumerated() {
                
                allImagesStatus[index].index = index
                
                if let image =  item.img {
                    
                    image.uploadImageToS3WithUtility(imageIndex: index, success: {[weak self] (imageIndex, success, imageUrl) in
                        
                        successCount += 1
                        
                        
                        if let ind = allImagesStatus.firstIndex(where: { $0.index == imageIndex}){
                            allImagesStatus[ind].stringUrl = imageUrl
                        }
                        
                        //                allImagesStatus[imageIndex] = (index : Int, stringUrl : imageUrl, img : nil?)
                        
                        //    self.addProductData.capturedImagesUrl.append(imageUrl)
                        
                        if failedCount + successCount + existingImagesCount == allImagesToUpload.count{
                            guard let weakSelf = self else { return }
                            weakSelf.uploadImagesDelegate?.imagesUploadedSuccessfullyWithSequence(allImagesStatus: allImagesStatus)
                        }
                        
                        }, progress: { (imageIndex, progress) in
                            
                    }) {[weak self] (imageIndex, errer) in
                        failedCount += 1
                        if let index = allImagesStatus.lastIndex(where: { $0.index == imageIndex}){
                            allImagesStatus[index].stringUrl = ""
                        }
                        
                        if failedCount + successCount + existingImagesCount == allImagesToUpload.count{
                            guard let weakSelf = self else { return }
                            weakSelf.uploadImagesDelegate?.imagesUploadedSuccessfullyWithSequence(allImagesStatus: allImagesStatus)
                        }
                    }
                }
            }
        }
        
        
        func uploadImages(allImagesToUpload : [UIImage]){
            
            var successCount = 0
            var failedCount = 0
            var allImagesStatus : [Int : (isSuccess : Bool, url : String)] = [:]
            
            self.uploadImagesDelegate?.willStartUploadingImages()
            
            for (index,item) in allImagesToUpload.enumerated() {
                
                item.uploadImageToS3WithUtility(imageIndex: index, success: {[weak self] (imageIndex, success, imageUrl) in
                    successCount += 1
                    allImagesStatus[imageIndex] = (success, imageUrl)
                    
                    //    self.addProductData.capturedImagesUrl.append(imageUrl)
                    
                    if failedCount + successCount == allImagesToUpload.count{
                        // self.view.hideIndicator()
                        //  self.addProductControler.addProduct(data: self.addProductData)
                        guard let weakSelf = self else { return }
                        weakSelf.uploadImagesDelegate?.imagesUploadedSuccessfully(allImagesStatus: allImagesStatus)
                    }
                    
                    }, progress: { (imageIndex, progress) in
                        
                        
                }) {[weak self] (imageIndex, errer) in
                    failedCount += 1
                    allImagesStatus[imageIndex] = (false, "")
                    
                    if failedCount + successCount == allImagesToUpload.count{
                        guard let weakSelf = self else { return }
                        weakSelf.uploadImagesDelegate?.imagesUploadedSuccessfully(allImagesStatus: allImagesStatus)
                    }
                }
            }
        }
        
        
    }
    
    //MARK:- Featured Product
    extension AddProductControler {
        
        func getActualproductFromTappedProduct(allProducts : [SKProduct], selectedProduct : SKProduct, selectedCommunityCount : Int) -> SKProduct? {
            
            switch selectedProduct.productIdentifier {
                
            case InAppProducts.ONEDAY.rawValue:
                return self.getActualProductForOneDay(allProducts: allProducts, selectedCommunityCount: selectedCommunityCount)
                
            case InAppProducts.THREEDAYS.rawValue:
                return self.getActualProductForThreeDays(allProducts: allProducts, selectedCommunityCount: selectedCommunityCount)
            case InAppProducts.SEVENDAYS.rawValue:
                return self.getActualProductForSevenDays(allProducts: allProducts, selectedCommunityCount: selectedCommunityCount)
                
            default:
                return nil
            }
        }
        
        func getActualProductForOneDay(allProducts : [SKProduct], selectedCommunityCount : Int) -> SKProduct? {
            
            switch selectedCommunityCount {
            case 0:
                
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.ONEDAY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 1:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.ONEDAYPLUSONECOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 2:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.ONEDAYPLUSTWOCOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 3:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.ONEDAYPLUSTHREECOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 4:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.ONEDAYPLUSFOURCOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            default:
                return nil
            }
            
        }
        
        func getActualProductForThreeDays(allProducts : [SKProduct], selectedCommunityCount : Int) -> SKProduct? {
            
            switch selectedCommunityCount {
            case 0:
                
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.THREEDAYS.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 1:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.THREEDAYSPLUSONECOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 2:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.THREEDAYSPLUSTWOCOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 3:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.THREEDAYSPLUSTHREECOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 4:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.THREEDAYSPLUSFOURCOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            default:
                return nil
            }
        }
        
        func getActualProductForSevenDays(allProducts : [SKProduct], selectedCommunityCount : Int) -> SKProduct? {
            
            switch selectedCommunityCount {
            case 0:
                
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.SEVENDAYS.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 1:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.SEVENDAYSPLUSONECOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 2:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.SEVENDAYSPLUSTWOCOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 3:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.SEVENDAYSPLUSTHREECOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            case 4:
                let prod = allProducts.filter { $0.productIdentifier == InAppProducts.SEVENDAYSPLUSFOURCOMMUNITY.rawValue }
                return !prod.isEmpty ? prod[0] : nil
                
            default:
                return nil
            }
        }
        
        func featureProduct(paymentId : String, productId : String, days : Int, price : Float, sharedCommunities : [Tag]){
            
            self.featureDelegate?.willRequestFeatureProduct()
            
            var params : JSONDictionary = [ApiKey.productId : productId, ApiKey.paymentId : paymentId, ApiKey.price : price, ApiKey.days : days]
            
            if !sharedCommunities.isEmpty{
                let communities = sharedCommunities.map({ (obj) -> String in
                    return obj.id
                })
                
                params[ApiKey.sharedCommunities] = communities.joined(separator: ",")
                
            }
            
            WebServices.featureProduct(parameters: params, success: {[weak self ] (json) in
                
                guard let weakSelf = self else { return }
                weakSelf.featureDelegate?.productFeaturedSuccessFully()
                
            }) { (error) -> (Void) in
                printDebug(error)
                self.featureDelegate?.failedToFeatureProduct()
            }
        }
        
    }
