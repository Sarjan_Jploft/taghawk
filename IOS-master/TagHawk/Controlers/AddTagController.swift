//
//  AddTagController.swift
//  TagHawk
//
//  Created by Vikash on 05/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

enum ApiState: String {
    case start, pending, complete, failed, noInternet, completeWithNoData
}

protocol AddTagDelegate : class{
    func willAddTag()
    func validateInput(isValid : Bool, message : String)
    func addTagListServiceReturn(addTagResult: AddTagResult, msg: String, tag: Tag)
    func addTagListFailure(errorType: ApiState, message : String)
}

protocol  ReportATagDelegate : class {
    
    func willReportTag()
    func tagReportedSuccessFully()
    func failedToReportTag(msg : String)
    
}

class AddTagController : NSObject {
    
    weak var delegate : AddTagDelegate?
    weak var reportTagDelegate : ReportATagDelegate?
    
    //MARK:- Validate add tag
    func validateAddProduct(with data : AddTagData) -> Bool {
        
        if data.imageUrl == nil {
            self.delegate?.validateInput(isValid: false, message: LocalizedString.selectLogoImage.localized)
            CommonFunctions.showToastMessage(LocalizedString.selectLogoImage.localized)
            return false
        }
        
        if data.tagName.isEmpty {
            self.delegate?.validateInput(isValid: false, message: LocalizedString.selectTagName.localized)
            CommonFunctions.showToastMessage(LocalizedString.selectTagName.localized)
            return false
        }
        
//        if data.tagType == nil {
//            self.delegate?.validateInput(isValid: false, message: LocalizedString.selectTagType.localized)
//            CommonFunctions.showToastMessage(LocalizedString.selectTagType.localized)
//            return false
//        }
        
        if data.tagEntrance == nil {
            self.delegate?.validateInput(isValid: false, message: LocalizedString.selectTagEntrance.localized)
            CommonFunctions.showToastMessage(LocalizedString.selectTagEntrance.localized)
            return false
        }
        
        if data.description.isEmpty {
            self.delegate?.validateInput(isValid: false, message: LocalizedString.selectDescription.localized)
            CommonFunctions.showToastMessage(LocalizedString.selectDescription.localized)
            return false
        }
        
        if data.description.count <= 3 {
            self.delegate?.validateInput(isValid: false, message: LocalizedString.validDescription.localized)
            CommonFunctions.showToastMessage(LocalizedString.validDescription.localized)
            return false
        }
        
        if data.location.isEmpty  {
            self.delegate?.validateInput(isValid: false, message: LocalizedString.selectLocation.localized)
            CommonFunctions.showToastMessage(LocalizedString.selectLocation.localized)
            return false
        }
        
        if data.tagEntrance == TagEntrance.privateType.rawValue {
            
            let email = "abc@" + data.email
            if data.privateTagType == PrivateTagType.email.rawValue && data.email.isEmpty {
                self.delegate?.validateInput(isValid: false, message: LocalizedString.Please_Enter_Email.localized)
                CommonFunctions.showToastMessage(LocalizedString.Please_Enter_Email.localized)
                return false
            }  else if data.privateTagType == PrivateTagType.email.rawValue && (email.checkIfValid(.email) || email.checkIfValid(.emailWithTwoDomains)) {
                self.delegate?.validateInput(isValid: true, message: LocalizedString.Please_Enter_A_Valid_Email.localized)
//                CommonFunctions.showToastMessage(LocalizedString.Please_Enter_A_Valid_Email.localized)
                return true
            }else if data.privateTagType == PrivateTagType.email.rawValue {
                self.delegate?.validateInput(isValid: false, message: LocalizedString.Please_Enter_A_Valid_Email.localized)
                CommonFunctions.showToastMessage(LocalizedString.Please_Enter_A_Valid_Email.localized)
                return false
            }
            
            if data.privateTagType == PrivateTagType.password.rawValue && data.password.isEmpty {
                self.delegate?.validateInput(isValid: false, message: LocalizedString.Please_Enter_Password.localized)
                CommonFunctions.showToastMessage(LocalizedString.Please_Enter_Password.localized)
                return false
            }  else if data.privateTagType == PrivateTagType.password.rawValue && !data.password.checkIfValid(.password) {
                self.delegate?.validateInput(isValid: false, message: LocalizedString.Password_must_Be_Atleast_6_Characters.localized)
                CommonFunctions.showToastMessage(LocalizedString.Password_must_Be_Atleast_6_Characters.localized)
                return false
            }
            
            if data.privateTagType == PrivateTagType.documents.rawValue && data.documents.isEmpty {
                self.delegate?.validateInput(isValid: false, message: LocalizedString.enterDocuments.localized)
                CommonFunctions.showToastMessage(LocalizedString.enterDocuments.localized)
                return false
            }
            
        }
        
        
        return true
   }
    
    //MARK:- Add tag webservice
    func addTagService(addTag: AddTagData, completion: @escaping () -> ()) {
        
        var dict = getDictFromTag(addTag)
   
        dict[ApiKey.createdUsing] = addTag.paymentType.rawValue
        
        if addTag.paymentType == .inApp{
            dict[ApiKey.paymentId] = addTag.paymentId
        }
        
        self.delegate?.willAddTag()
        
        WebServices.addTagDetails(parameters: dict, success: { [weak self] (data) in

            let addTagResult = AddTagResult(json: data["data"])
            let tag = Tag.init(json: data["data"])
            self?.delegate?.addTagListServiceReturn(addTagResult: addTagResult,
                                                    msg: data[ApiKey.message].stringValue, tag: tag)

        }) { (error) -> (Void) in
            if error.code == NSURLErrorNotConnectedToInternet {
                self.delegate?.addTagListFailure(errorType: .noInternet, message: error.localizedDescription)
            } else {
                
                self.delegate?.addTagListFailure(errorType: .failed, message: error.localizedDescription)
            }
        }
    }
    
    func editTagService(addTag: AddTagData, tagId: String, completion: @escaping () -> ()) {
        
        var dict = getDictFromTag(addTag)
        dict[ApiKey.communityId] = tagId
       
        self.delegate?.willAddTag()
      
        WebServices.editTagDetails(parameters: dict, success: { [weak self] (data) in
            let addTagResult = AddTagResult.init(json: data["data"])
            let tag = Tag(json: data["data"])
            self?.delegate?.addTagListServiceReturn(addTagResult: addTagResult, msg: data[ApiKey.message].stringValue,
                                                    tag: tag)
        }) { (error) -> (Void) in
            if error.code == NSURLErrorNotConnectedToInternet {
                self.delegate?.addTagListFailure(errorType: .noInternet, message: error.localizedDescription)
            } else {
                self.delegate?.addTagListFailure(errorType: .failed, message: error.localizedDescription)
            }
        }
    }
    
    private func getDictFromTag(_ addTag: AddTagData) -> JSONDictionary {
        var dict = [String: Any]()
        dict["name"] = addTag.tagName
        //dict["type2"] = addTag.tagType
        dict["type"] = addTag.tagEntrance
        if addTag.tagEntrance == 1 {
            dict["joinTagBy"] = addTag.privateTagType
            if addTag.privateTagType == PrivateTagType.email.rawValue {
                    dict["email"] = addTag.email
            } else if addTag.privateTagType == PrivateTagType.password.rawValue {
                dict["password"] = addTag.password
            } else {
                dict["document_type"] = addTag.documents
            }
        }
        dict["pointsCharged"] = 100
        dict["description"] = addTag.description
        if !addTag.announcement.isEmpty {
            dict["announcement"] = addTag.announcement
        }
        dict["address"] = addTag.location
        dict["city"] = addTag.city
        dict["lat"] = "0"
        dict["long"] = "0"
        dict["lat1"] = "\(addTag.lat)"
        dict["long1"] = "\(addTag.long)"
        
        var dictObj = JSONDictionary()
        dictObj[ApiKey.url] = addTag.imageUrl
        dictObj[ApiKey.thumbUrl] = addTag.imageUrl
        
        dict["imageUrl"] = JSON(dictObj)
        return dict
    }
    
    
    func reportTag(tagId : String) {
        
        let params : JSONDictionary = [ApiKey.communityId : tagId]
        
        self.reportTagDelegate?.willReportTag()
        
        WebServices.reportATag(parameters: params, success: {[weak self] (json) in
            
            self?.reportTagDelegate?.tagReportedSuccessFully()
            
        }) {[weak self] (error) -> (Void) in
            self?.reportTagDelegate?.failedToReportTag(msg: error.localizedDescription)
        }
        
    }
    
}
