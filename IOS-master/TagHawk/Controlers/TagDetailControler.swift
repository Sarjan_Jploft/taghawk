//
//  TagDetailControler.swift
//  TagHawk
//
//  Created by Appinventiv on 18/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol TagDetaulControlerDelegate : class {
    func willRequestTagDetail()
    func tagDetailReceivedSuccessFully(tag : Tag)
    func failedToReceiveTagDetail(message : String)
}


class TagDetailControler: NSObject {

    weak var delegate : TagDetaulControlerDelegate?
    
    //MARK:- get tag detail
    func getTagDetail(id : String){
        
        self.delegate?.willRequestTagDetail()
        
        let params : JSONDictionary = [ApiKey.communityId : id, ApiKey.limit : 6]
        
        WebServices.getTagDetail(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.delegate?.tagDetailReceivedSuccessFully(tag: Tag(json: json[ApiKey.data]))
            
        }) { [weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToReceiveTagDetail(message: error.localizedDescription)
        }
    }
    

    
}
