//
//  FollowingFollowersControler.swift
//  TagHawk
//
//  Created by Appinventiv on 25/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol FollowUserDelegate : class {
    func willFollowUser()
    func followUserSuccessFully(userId : String)
    func failedToFollowUser(message : String)
}

protocol FollowingFollowersListDelegate : class {
    func willRequestFollowersList()
    func followersFollowingReceivedSuccessFully(users : [FollowingFollowersModel], total : Int)
    func failedToReceiveFollowingFollowers(message : String)
}


protocol RemoveFollowerDelegate : class {
    func willRequestRemoveUser()
    func removeUserSuccessfull(user : String)
    func failedToRemoveUser(message : String)
}


class FollowingFollowersControler {

    weak var followUserDelegate : FollowUserDelegate?
    weak var followListDelegate : FollowingFollowersListDelegate?
    weak var removeUserdelegate : RemoveFollowerDelegate?

    //MARK:- Follow user webservice
    func followUser(user : String){
        
        let params : JSONDictionary = [ApiKey.userId : user]
       
        self.followUserDelegate?.willFollowUser()
        
        WebServices.followUser(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.followUserDelegate?.followUserSuccessFully(userId: user)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.followUserDelegate?.failedToFollowUser(message: error.localizedDescription)
        }
    }
    
    func getFollowingFollowersList(userId : String = "", commingFor : FollowingFollowersVC.FollowingFollowersListFor, page: Int){
        
        self.followListDelegate?.willRequestFollowersList()
        
        var params : JSONDictionary = [ApiKey.pageNo : page, ApiKey.limit : 10]
       
        if !userId.isEmpty{
            params[ApiKey.userId] = userId
        }
        
        params[ApiKey.type] = commingFor == .followers || commingFor == .othersFollowers ? "1" : "2"
        
        WebServices.getFollowingFollowersList(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
          let users = json[ApiKey.data].arrayValue.map({ (obj) -> FollowingFollowersModel in
                FollowingFollowersModel(json: obj)
            })
            weakSelf.followListDelegate?.followersFollowingReceivedSuccessFully(users: users, total: json[ApiKey.next_hit].intValue)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
        weakSelf.followListDelegate?.failedToReceiveFollowingFollowers(message: error.localizedDescription)
        }
    }
    
    func unfollowUser(user : String){
        
        let params : JSONDictionary = [ApiKey.userId : user, ApiKey.action : "1"]
        self.followUserDelegate?.willFollowUser()
        
        WebServices.removeFriend(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.followUserDelegate?.followUserSuccessFully(userId: user)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.followUserDelegate?.failedToFollowUser(message: error.localizedDescription)
        }
    }
    

    func removeFollower(user : String){
        
        let params : JSONDictionary = [ApiKey.userId : user, ApiKey.action : "2"]
        self.removeUserdelegate?.willRequestRemoveUser()
        
        WebServices.removeFriend(parameters: params, success: {[weak self] (json) in
            guard let weakSelf = self else { return }
            weakSelf.removeUserdelegate?.removeUserSuccessfull(user: user)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.removeUserdelegate?.failedToRemoveUser(message: error.localizedDescription)
        }
    }
    
}
