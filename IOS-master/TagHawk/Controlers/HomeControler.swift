//
//  HomeControler.swift
//  TagHawk
//
//  Created by Appinventiv on 29/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol GetProductsDelegate : class {
    func willRequestProducts()
    func productsReceivedSuccessFully(products : [Product], nextPage : Int)
    func failedToReceiveProducts(message : String)
}

protocol GetTagsDelegate : class {
    func willRequestCommunities()
    func communitiesReceivedSuccessfully(comunities : [Tag])
    func failedToReceiveCommunities()
}

protocol CreateCustomerDelegate : class {
    
    func willrequestCreteCustomer()
    func customerCreatedSuccessFully(customerId : String)
    func failedToCreateCustomer()
    
}

protocol GetRateProductData: AnyObject {
    func rateDataSuccess(sellerData: SellerProductModel)
    func denyRatingSuccess()
}

protocol CreateMerchantDelegate : class {
    func willrequestCreteMerchant()
    func merchantCreatedSuccessFully(customerId : String, from : AddDepositAccountVC.AddBankDetailsWhile)
    func failedToCreateMerchant(message : String)
}

protocol GetCurrentVersionDelegate : class {
    func willGetCurrentVersion()
    func getCurrentVersionSuccessfully(version : Int, type : VersionType)
    func failedToUpdateVersion(message : String)
}

enum VersionType : String {
    case force = "FORCE"
    case normal = "NORMAL"
    case skip = "SKIP"
}

class HomeControler : BaseControler {

    weak var delegate : GetProductsDelegate?
    weak var tagDelegate : GetTagsDelegate?
    weak var customerDelegate : CreateCustomerDelegate?
    weak var rateProductDelegate: GetRateProductData?
    weak var createMerchangtDelegate : CreateMerchantDelegate?
    weak var currentVersionDelegate : GetCurrentVersionDelegate?
    
    //MARK:- Update device token service
    func updateDeviceToken() {
        if DeviceDetail.fcmToken.isEmpty { return }
        let params: JSONDictionary = [ApiKey.deviceToken: DeviceDetail.fcmToken]
        
        WebServices.updateDeviceToken(parameters: params, success: { (json) in
            FireBaseChat.updateToken()
            let sellerProdModel = SellerProductModel(json: json[ApiKey.ratingData])
            self.rateProductDelegate?.rateDataSuccess(sellerData: sellerProdModel)
            
        }) { (err) -> (Void) in
            
        }
    }
    
    //MARK:- get products service
    func getProducts(page : Int,currentProducts : [Product] , categoryId : String = ""){
        
        if SelectedFilters.shared.appliedFiltersOnItems.lat == 0.0 && SelectedFilters.shared.appliedFiltersOnItems.long == 0.0 {
            
            SelectedFilters.shared.appliedFiltersOnItems.lat = LocationManager.shared.latitude
            
            SelectedFilters.shared.appliedFiltersOnItems.long = LocationManager.shared.longitude
        }
       
        self.delegate?.willRequestProducts()
        
        var params : JSONDictionary = [ApiKey.pageNo : page, ApiKey.limit : "15"]
        
        if !categoryId.isEmpty{
            params[ApiKey.productCategoryId] = categoryId
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.fromPrice > 0{
            params[ApiKey.priceFrom] = SelectedFilters.shared.appliedFiltersOnItems.fromPrice
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.toPrice > 0{
            params[ApiKey.priceTo] = SelectedFilters.shared.appliedFiltersOnItems.toPrice
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.lat != 0{
            params[ApiKey.lat1] = String(SelectedFilters.shared.appliedFiltersOnItems.lat)
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.long != 0{
            
            params[ApiKey.long1] = String(SelectedFilters.shared.appliedFiltersOnItems.long)
        }
        
        if !SelectedFilters.shared.appliedFiltersOnItems.condiotions.isEmpty{
            let conditionsArray = SelectedFilters.shared.appliedFiltersOnItems.condiotions.map { "\($0.rawValue)" }
        
            let cond = conditionsArray.joined(separator: ",")
            
              params[ApiKey.condition] = cond
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.distance != 5{
            params[ApiKey.distance] = SelectedFilters.shared.getMilesForIndex(index: SelectedFilters.shared.appliedFiltersOnItems.distance)
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.postedWithin != .none{
            params[ApiKey.postedWithIn] = SelectedFilters.shared.appliedFiltersOnItems.postedWithin.rawValue
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.rating != 0 {
            params[ApiKey.sellerRating] = SelectedFilters.shared.appliedFiltersOnItems.rating
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.verifiedSellorsOnly {
            params[ApiKey.sellerVerified] = SelectedFilters.shared.appliedFiltersOnItems.verifiedSellorsOnly ? "true" : "false"
        }
        
        switch SelectedFilters.shared.appliedSortingOnProducts {
            case .newest:
                params[ApiKey.sortBy] = "created"
                params[ApiKey.sortOrder] = "-1"
            
            case .priceLowToHigh:
                params[ApiKey.sortBy] = "firmPrice"
                params[ApiKey.sortOrder] = "1"

            case .priceHightToLow:
                params[ApiKey.sortBy] = "firmPrice"
                params[ApiKey.sortOrder] = "-1"
            
        default:
            break
        }
        
        let allPromotedProducts = currentProducts.filter { $0.isPromoted }
       
        if !allPromotedProducts.isEmpty && page > 1{
            params[ApiKey.promotedProductIds] = allPromotedProducts.map { $0.productId }.joined(separator: ",")
        }
        
//        params[ApiKey.sellerVerified] = SelectedFilters.shared.appliedFilters.verifiedSellorsOnly
        
        WebServices.getProducts(parameters: params, success: { (json) in
            
            self.delegate?.productsReceivedSuccessFully(products: json[ApiKey.data].arrayValue.map { Product(json: $0) }, nextPage: json[ApiKey.next_hit].intValue)
            
        }) { (error) -> (Void) in
        
            self.delegate?.failedToReceiveProducts(message: error.localizedDescription)
        
        }
    }
    
    //MARK:- get tags
    func getTagsData() {
        
        self.tagDelegate?.willRequestCommunities()
        
        var params : JSONDictionary = [:]
        
        if SelectedFilters.shared.appliedFiltersOnTag.lat != 0{
            params[ApiKey.lat1] = String(SelectedFilters.shared.appliedFiltersOnTag.lat)
        }
        
        if SelectedFilters.shared.appliedFiltersOnTag.long != 0{
            params[ApiKey.long1] = String(SelectedFilters.shared.appliedFiltersOnTag.long)
        }
        
        if SelectedFilters.shared.appliedFiltersOnTag.distance != 5{
            params[ApiKey.distance] = SelectedFilters.shared.getMilesForIndex(index: SelectedFilters.shared.appliedFiltersOnTag.distance)
        }
        
        if SelectedFilters.shared.appliedFiltersOnTag.tagType != .all {
            params[ApiKey.tagType] = SelectedFilters.shared.appliedFiltersOnTag.tagType.rawValue
        }
        
        if SelectedFilters.shared.appliedFiltersOnTag.members != .allMembers {
            params[ApiKey.memberSize] = SelectedFilters.shared.appliedFiltersOnTag.members.rawValue
        }
        
//        ApiKey.lat : lat, ApiKey.long : long
        WebServices.getTagData(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
        weakSelf.tagDelegate?.communitiesReceivedSuccessfully(comunities: json[ApiKey.data].arrayValue.map { Tag(json: $0) })
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.tagDelegate?.failedToReceiveCommunities()
            
        }
    }
    
    //MARK:- Create customer webservice
    func createCustomer(){
    
        customerDelegate?.willrequestCreteCustomer()
        
        WebServices.createCusomer(parameters: [:], success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
       
            UserProfile.main.stripeCustomerId = json[ApiKey.data].stringValue
            UserProfile.main.saveToUserDefaults()
        weakSelf.customerDelegate?.customerCreatedSuccessFully(customerId: json[ApiKey.data].stringValue)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.customerDelegate?.failedToCreateCustomer()
        }
    }
    
    func denyRating(sellerId: String, prodId: String) {
        
        let params: JSONDictionary = [ApiKey.sellerId: sellerId, ApiKey.productId: prodId]
        
        WebServices.denyRating(parameters: params, success: { (json) in
            
            self.rateProductDelegate?.denyRatingSuccess()
            
        }) { (err) -> (Void) in
            
        }
    }
    
    //MARK:- Create merchant webservice
    func createMerchant(from : AddDepositAccountVC.AddBankDetailsWhile){
        
        let params : JSONDictionary = [ApiKey.ip : DeviceDetail.ipAddress ?? "176.154.154.154"]
        
        createMerchangtDelegate?.willrequestCreteMerchant()
        
        WebServices.createMerchant(parameters: params, success: {[weak self] (json) in
            
            UserProfile.main.stripeMerchantId = json[ApiKey.data].stringValue
            UserProfile.main.saveToUserDefaults()
            self?.createMerchangtDelegate?.merchantCreatedSuccessFully(customerId: json[ApiKey.data].stringValue, from: from)
            
        }) {[weak self] (error) -> (Void) in
           self?.createMerchangtDelegate?.failedToCreateMerchant(message: error.localizedDescription)
        }
    }
    
    //MARK:- get common data
    func getCommonData(){
        self.currentVersionDelegate?.willGetCurrentVersion()
        WebServices.getCommonData(parameters: [:], success: {[weak self] (json) in
            printDebug(json)
            guard let weakSelf = self else { return }
        
            let versionInfo = json[ApiKey.data][ApiKey.versionInfo]
            weakSelf.currentVersionDelegate?.getCurrentVersionSuccessfully(version: versionInfo[ApiKey.versionName].intValue, type: VersionType(rawValue: versionInfo[ApiKey.type].stringValue) ?? VersionType.force)
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.currentVersionDelegate?.failedToUpdateVersion(message: error.localizedDescription)
        }
        
    }
    
}

