//
//  ResrePasswordControler.swift
//  TagHawk
//
//  Created by Appinventiv on 26/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ResetPasswordDelegate : class {
    func willRequestResetPassword()
    func resetPasswordSuccessfully()
    func failedToResetPassword(message : String)
    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState)
}

class ResetPasswordControler  {

    weak var delegate : ResetPasswordDelegate?
    
    //MARK:- Validate input
    func areAllFieldsVerified(userInput : UserInput) -> Bool {
        
        if !validatePassword(password: userInput.password){
            return false
        }
        
        if !validateConfirmPassword(password: userInput.password, confirmPassword: userInput.confirmPassword){
            return false
        }
        
        return true
    }
    
    func validatePassword(password : String) -> Bool{
        if password.isEmpty {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.password, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_Password.localized.localized))
            return false
        }else if password.count < 6 {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.password, textFieldState: TextFieldState.invalid(LocalizedString.Password_must_Be_Atleast_6_Characters.localized.localized))
            
            return false
        }else{
            return true
        }
    }
    
    func validateConfirmPassword(password : String, confirmPassword : String) -> Bool{
        if confirmPassword.isEmpty {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.confirmPassword, textFieldState: TextFieldState.invalid(LocalizedString.Please_Confirm_Your_Password.localized.localized))
            return false
        }else if confirmPassword.count < 6 {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.confirmPassword, textFieldState: TextFieldState.invalid(LocalizedString.Password_must_Be_Atleast_6_Characters.localized.localized))
            
            return false
        } else if password != confirmPassword {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.confirmPassword, textFieldState: TextFieldState.invalid(LocalizedString.Confirm_Password_Same_New_Password.localized.localized))
            return false
        }
        else{
            return true
        }
    }
    
    
    //MARK:- Reset password
    func resetPassword(userInput : UserInput, token : String){
        
        if !self.areAllFieldsVerified(userInput: userInput){
            return
        }
        
        self.delegate?.willRequestResetPassword()
        
        let params : JSONDictionary = [ApiKey.password : userInput.password, ApiKey.accessToken : token]
        
        WebServices.resetPassword(parameters: params, success: { (data) in
            
            self.delegate?.resetPasswordSuccessfully()
            
        }) { (error) -> (Void) in
            self.delegate?.failedToResetPassword(message: error.localizedDescription)
        }
        
    }
    
}
