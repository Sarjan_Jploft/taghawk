//
//  DeleteTagPopUpControler.swift
//  TagHawk
//
//  Created by Admin on 6/28/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ValidateDeletetagDelegate : class {
    func validate(msg : String)
}

class DeleteTagPopUpControler {

    weak var delegate : ValidateDeletetagDelegate?
    
    func validateDeleteTag(code : String, capta : String) -> Bool {
        
        if code.isEmpty{
            self.delegate?.validate(msg: LocalizedString.Please_Enter_Code.localized)
            return false
        }
        
        if code != capta{
            self.delegate?.validate(msg: LocalizedString.Please_Enter_Valid_Code.localized)
            return false
        }
        
        return true
    }
    
}

