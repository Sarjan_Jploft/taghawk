//
//  AddAddressControler.swift
//  TagHawk
//
//  Created by Admin on 8/9/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ValidateAddress : class {
    func validateAddress(message : String)
}

class AddAddressControler {

    weak var delegate : ValidateAddress?
    
    func validateAddressInput(addressInput : AddAddressVC.AddressInput) -> Bool {
        
        if addressInput.addressLine1.isEmpty {
            delegate?.validateAddress(message: LocalizedString.Please_Enter_Addressline_1.localized)
            return false
        }else if addressInput.zipCode.isEmpty{
            delegate?.validateAddress(message: LocalizedString.Please_Enter_Zipcode.localized)
            return false
        }else if addressInput.city.isEmpty || addressInput.state.isEmpty {
            delegate?.validateAddress(message: LocalizedString.Please_Enter_Valid_Zipcode.localized)
            return false
        }else{
            return true
        }
    }
}
