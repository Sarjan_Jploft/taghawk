//
//  SignUpControler.swift
//  TagHawk
//
//  Created by Appinventiv on 22/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol SignUpControllerDelegate: class {
    func willSignUp()
    func signUpSuccess(user: UserProfile)
    func signUpFailed(message: String)
    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState)
}


class SignUpControler {

    weak var delegate: SignUpControllerDelegate?

    //MARK:- Validate signup
    func areAllFieldsVerified(userInput : UserInput) -> Bool {
        
        if !validateFullName(name : userInput.firstName, nameType: .firstName){
            return false
        }
        
        if !validateFullName(name : userInput.lastName, nameType: .lastName){
            return false
        }

        
        if !validateEmail(email : userInput.email){
            return false
        }
        
        if !validatePassword(password: userInput.password){
            return false
        }
    
        return true
    }
    
    func validateFullName(name : String, nameType: TagHawkTextFieldTableViewCell.TextFieldType) -> Bool {
        
        if name.isEmpty {
            self.delegate?.invalidInput(textFieldType: nameType, textFieldState: TextFieldState.invalid(nameType == .firstName ? LocalizedString.PleaseEnterFirstName.localized : LocalizedString.PleaseEnterLastName.localized))
            return false
        } else if name.count < 1 {
            self.delegate?.invalidInput(textFieldType: nameType, textFieldState: TextFieldState.invalid(nameType == .firstName ? LocalizedString.FirstnameMustBeAtLeast2Characters.localized : LocalizedString.LastnameMustBeAtLeast2Characters.localized))
            return false
        }else{
            self.delegate?.invalidInput(textFieldType: nameType, textFieldState: TextFieldState.valid)
            return true
        }
    }
    
    func validateEmail(email : String) -> Bool {
        
        if email.isEmpty {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_Email.localized))
            return false
        } else if !email.checkIfInvalid(.email) || !email.checkIfInvalid(.emailWithTwoDomains) {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.valid)
            return true
        } else{
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_A_Valid_Email.localized))
            return false
        }
    }
    
    func validatePassword(password : String) -> Bool{
        if password.isEmpty {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.password, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_Password.localized.localized))
            return false
        }else if password.count < 6 {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.password, textFieldState: TextFieldState.invalid(LocalizedString.Password_must_Be_Atleast_6_Characters.localized.localized))

            return false
        }else{
            return true
        }
    }
    
    //MARK:- Sign up service
    func signUp(userInput : UserInput) {
        
        var userModel = userInput
        userModel.fullName = userInput.firstName + " " + userInput.lastName
        
        if !areAllFieldsVerified(userInput: userModel){
            return
        }
        
        self.delegate?.willSignUp()
        
        var params : JSONDictionary = [ApiKey.fullName : userModel.fullName, ApiKey.firstName: userModel.firstName, ApiKey.lastName: userModel.lastName, ApiKey.email : userModel.email, ApiKey.password : userModel.password, ApiKey.deviceId : DeviceDetail.deviceId, ApiKey.userType : "2", ApiKey.deviceToken : DeviceDetail.fcmToken]
        
        if !userInput.invitationCode.isEmpty {
            params[ApiKey.invitationCode] = userInput.invitationCode
        }
        
        WebServices.signUp(parameters: params, success: { (user) in
            self.delegate?.signUpSuccess(user: user)
            FireBaseChat.shared.createUserNode(data: user)
        }) { (error) -> (Void) in
            self.delegate?.signUpFailed(message: error.localizedDescription)
        }
    }
}
