//
//  SearchedProductsControler.swift
//  TagHawk
//
//  Created by Appinventiv on 30/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol SearchProductsDelegate : class {
    func willRequestProducts()
    func productsReceivedSuccessFully(products : [Product], nextPage : Int)
    func failedToReceiveProducts(message : String)
}


class SearchedProductsControler : BaseControler {

    weak var delegate : SearchProductsDelegate?
    
    //MARK:- get products
    func getProducts(categoryId : String = "",searchText : String = "", page : Int){
        
        self.delegate?.willRequestProducts()
        
        var params : JSONDictionary = [ApiKey.pageNo : page, ApiKey.limit : "10"]
     
        if !categoryId.isEmpty{
            params[ApiKey.productCategoryId] = categoryId
        }
        
        if !searchText.isEmpty{ params[ApiKey.searchKey] = searchText }
        
        if SelectedFilters.shared.appliedFiltersOnItems.fromPrice > 0{
            params[ApiKey.priceFrom] = SelectedFilters.shared.appliedFiltersOnItems.fromPrice
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.toPrice > 0{
            params[ApiKey.priceTo] = SelectedFilters.shared.appliedFiltersOnItems.toPrice
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.lat != 0{
            params[ApiKey.lat1] = String(SelectedFilters.shared.appliedFiltersOnItems.lat)
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.long != 0{
            
            params[ApiKey.long1] = String(SelectedFilters.shared.appliedFiltersOnItems.long)
        }
        
        if !SelectedFilters.shared.appliedFiltersOnItems.condiotions.isEmpty{
            let conditionsArray = SelectedFilters.shared.appliedFiltersOnItems.condiotions.map { "\($0.rawValue)" }
            
            let cond = conditionsArray.joined(separator: ",")
            
            params[ApiKey.condition] = cond
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.distance != 5{
            params[ApiKey.distance] = SelectedFilters.shared.getMilesForIndex(index: SelectedFilters.shared.appliedFiltersOnItems.distance)
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.postedWithin != .none{
            params[ApiKey.postedWithIn] = SelectedFilters.shared.appliedFiltersOnItems.postedWithin.rawValue
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.rating != 0 {
            params[ApiKey.sellerRating] = SelectedFilters.shared.appliedFiltersOnItems.rating
        }
        
        if SelectedFilters.shared.appliedFiltersOnTag.verifiedSellorsOnly {
            params[ApiKey.sellerVerified] = SelectedFilters.shared.appliedFiltersOnTag.verifiedSellorsOnly
        }
        
        
        switch SelectedFilters.shared.appliedSortingOnProducts {
        case .newest:
            params[ApiKey.sortBy] = "created"
            params[ApiKey.sortOrder] = "-1"

        case .priceLowToHigh:
            params[ApiKey.sortBy] = "firmPrice"
            params[ApiKey.sortOrder] = "1"
            
        case .priceHightToLow:
            params[ApiKey.sortBy] = "firmPrice"
            params[ApiKey.sortOrder] = "-1"
            
        default:
            break
        }
        
        
        WebServices.getProducts(parameters: params, success: { (json) in
            
            self.delegate?.productsReceivedSuccessFully(products: json[ApiKey.data].arrayValue.map { Product(json: $0) },nextPage: json[ApiKey.next_hit].intValue)
            
        }) { (error) -> (Void) in
            
            self.delegate?.failedToReceiveProducts(message: error.localizedDescription)
            
        }
        
    }
    
}
