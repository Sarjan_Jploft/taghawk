//
//  LoginControler.swift
//  TagHawk
//
//  Created by Appinventiv on 22/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol LoginControllerDelegate: class {
    
    func willLogin()
    func loginSuccess(user: UserProfile)
    func loginFailed(message: String)
    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState)
}

class LoginControler  {

    weak var delegate: LoginControllerDelegate?
    
    //MARK:- CValidation for login
    func areAllFieldsVerified(userInput : UserInput) -> Bool {
        
        if !validateEmail(email : userInput.email){
            return false
        }
        
        if !validatePassword(password: userInput.password){
            return false
        }
        
        return true
    }
    
    func validateEmail(email : String) -> Bool {
        
        if email.isEmpty {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_Email.localized))
            return false
        }else if !email.checkIfInvalid(.email) || !email.checkIfInvalid(.emailWithTwoDomains) {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.valid)
            return true
        }else{
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_A_Valid_Email.localized))
            return false
        }
    }
    
    func validatePassword(password : String) -> Bool{
        if password.isEmpty {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.password, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_Password.localized.localized))
            return false
        }else if password.count < 6 {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.password, textFieldState: TextFieldState.invalid(LocalizedString.Password_must_Be_Atleast_6_Characters.localized.localized))
            
            return false
        }else{
            return true
        }
    }

    
    /// Login webservice
    func login(userInput : UserInput) {
        
        if !areAllFieldsVerified(userInput: userInput){
            return
        }
        
        self.delegate?.willLogin()
        
        let params : JSONDictionary = [ApiKey.email : userInput.email, ApiKey.password : userInput.password, ApiKey.deviceId : DeviceDetail.deviceId, ApiKey.userType : "2", ApiKey.deviceToken : DeviceDetail.fcmToken]
    
        WebServices.login(parameters: params, success: {[weak self] (user) in
            self?.delegate?.loginSuccess(user: user)
            FireBaseChat.shared.updateUserData(userData: user)
        }) {[weak self] (error) -> (Void) in
            
            printDebug(error.localizedDescription)
            
            self?.delegate?.loginFailed(message: error.localizedDescription)
        }
    }
    
    //MARK:- Login as a guest
    func loginAsAGuest() {
        
        self.delegate?.willLogin()

        let params : JSONDictionary = [ApiKey.deviceId : DeviceDetail.deviceId, ApiKey.deviceToken : DeviceDetail.fcmToken]
        
        WebServices.guestLogin(parameters: params, success: { (user) in
            
            self.delegate?.loginSuccess(user: user)
            
        }) { (error) -> (Void) in
            self.delegate?.loginFailed(message: error.localizedDescription)
        }
    }
}
