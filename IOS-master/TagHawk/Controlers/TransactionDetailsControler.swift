//
//  TransactionDetailsControler.swift
//  TagHawk
//
//  Created by Appinventiv on 29/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SendTokenDelegate : class{
    func willRequestToken()
    func tokenSentSuccessFully(data : TransactionDetails)
    func productAlreadySold(productIds : [String], message : String)
    func failedToSendToken(message : String)
    func validateTransactionDetails(message : String)
}

protocol ZeroPriceChargeDelegate : class {
   // func willRequestZeroCharge
}

protocol SaveCardDelegate : class {
    func willSaveCard()
    func saveCardSuccessFully()
    func failedToSaveCard(message : String)
}

protocol AddDebitCardDelegate : class {
    func willAddDebitCard()
    func debitCardAddedSuccessFully()
    func failedToAddDebitCard(msg : String)
}

class TransactionDetailsControler {
    
    weak var sendtokenDelegate : SendTokenDelegate?
    weak var saveCardDelegate : SaveCardDelegate?
    weak var addDebitCardDelegate : AddDebitCardDelegate?
    
    //MARK:- Validate transaction details
    func validateTransactionDetails(details : TransactionDetails) -> Bool{
    
        if details.cardNumber.isEmpty{
            self.sendtokenDelegate?.validateTransactionDetails(message: LocalizedString.Please_Enter_Card_Number.localized)
            return false
        }else if details.cardNumber.count < 11{
            self.sendtokenDelegate?.validateTransactionDetails(message: LocalizedString.Please_Entera_Valid_Card_Number.localized)
            return false
        }else if details.expirey.isEmpty{
            self.sendtokenDelegate?.validateTransactionDetails(message: LocalizedString.Please_Enter_Expirey_Date.localized.localized)
            return false
        }else if details.expirey.count < 5{
            self.sendtokenDelegate?.validateTransactionDetails(message: LocalizedString.Please_Enter_A_Valid_Expirey_Date.localized.localized)
            return false
        } else if details.cvv.isEmpty{
            self.sendtokenDelegate?.validateTransactionDetails(message: LocalizedString.Pleasd_Enter_Cvv.localized.localized)
            return false
        }else if details.cvv.count < 3{
            self.sendtokenDelegate?.validateTransactionDetails(message: LocalizedString.Please_Enter_A_Valid_Cvv.localized.localized)
            return false
        } else if details.name.isEmpty{
            self.sendtokenDelegate?.validateTransactionDetails(message: LocalizedString.Please_Enter_Card_Holder_Name.localized.localized)
            return false
        } else if details.saveCard && details.amount <= 0{
            self.sendtokenDelegate?.validateTransactionDetails(message: LocalizedString.Card_Not_Saved_With_0_Price.localized.localized)
            return false
        }
   
        return true
        
    }
    
    //MARK:- Get total amount
    func getTotalAmount(products : [CartProduct], shippingCharges : Double) -> Double{
        var price : Double = 0
        for item in products {
           price += item.productPrice
     }
        return price + shippingCharges
    }
    
    //MARK:- Contains shipping product
    func containsShippingProduct(products : [CartProduct]) -> Bool {
        return products.contains { (prod) -> Bool in
            return prod.shippingAvailibility == ShippingAvailability.shipping
        }
    }
    
    //MARK:- Send token
    func sendToken(details : TransactionDetails, products : [CartProduct], shippingInfo : ShippingModel? = nil) {
    
        sendtokenDelegate?.willRequestToken()

        var params : JSONDictionary = [:]
        
        params[ApiKey.source] = details.source
        params[ApiKey.amount] = "\(details.amount.rounded(toPlaces: 2).removeTrailingZero())"
        params[ApiKey.currency] = "usd"
        let allProducts = products.map { (prod) -> JSONDictionary in
            var dict = JSONDictionary()
            dict[ApiKey.productId] = prod.productId
            dict[ApiKey.sellerId] = prod.sellerId
            dict[ApiKey.price] = prod.productPrice
            return dict
        }
        
          params[ApiKey.products] = JSON(allProducts)
        
        if let shipInfo = shippingInfo{
            var shipReq = shipInfo.toJSON()
            shipReq[ApiKey.type] = "FEDEX"
            shipReq[ApiKey.country] = nil
           params[ApiKey.shipTo] = JSON(shipReq)
        }
        
        WebServices.sendToken(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            let code = json[ApiKey.code].intValue
            let msg = json[ApiKey.message].stringValue
            
            switch code {
            
            case ApiCode.success:
                weakSelf.sendtokenDelegate?.tokenSentSuccessFully(data: details)
                
            case ApiCode.produceSold:
                let soldProds = json[ApiKey.data].arrayValue.map { $0.stringValue }
                
              weakSelf.sendtokenDelegate?.productAlreadySold(productIds: soldProds, message: msg)
                
            default:
                break
            }
            
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
          weakSelf.sendtokenDelegate?.failedToSendToken(message: error.localizedDescription)
        }
    }
    
    
    //MARK:- Charge for zero products
    func chargeForZeroPrice(details : TransactionDetails, products : [CartProduct]) {
        
        sendtokenDelegate?.willRequestToken()
        
        var params : JSONDictionary = [:]
        
        let allProducts = products.map { (prod) -> JSONDictionary in
            var dict = JSONDictionary()
            dict[ApiKey.productId] = prod.productId
            dict[ApiKey.sellerId] = prod.sellerId
            return dict
        }
        
        params[ApiKey.products] = JSON(allProducts)
        
        WebServices.chargeForZeroPrice(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            let code = json[ApiKey.code].intValue
            let msg = json[ApiKey.message].stringValue
            
            switch code {
                
            case ApiCode.success:
                weakSelf.sendtokenDelegate?.tokenSentSuccessFully(data: details)
                
            case ApiCode.produceSold:
                let soldProds = json[ApiKey.data].arrayValue.map { $0.stringValue }
                
                weakSelf.sendtokenDelegate?.productAlreadySold(productIds: soldProds, message: msg)
                
            default:
                weakSelf.sendtokenDelegate?.failedToSendToken(message: msg)
            }
            
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.sendtokenDelegate?.failedToSendToken(message: error.localizedDescription)
        }
    }
    
    //MARK:- Save card
    func saveCard(details : TransactionDetails){
       
        let params : JSONDictionary = [
            ApiKey.customerId : UserProfile.main.stripeCustomerId,
            ApiKey.cardToken : details.source,
            ApiKey.cardHolderName : details.name,
            ApiKey.cardNumber : details.cardNumber,
//            ApiKey.cardType : details.brand,
            ApiKey.expMonth : "\(details.expireyMonth)",
            ApiKey.currency : "usd",
            ApiKey.expYear : "\(details.expireyYear)",
            ApiKey.isDefault : 0
        ]
        
        saveCardDelegate?.willSaveCard()
        
        WebServices.saveCard(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.saveCardDelegate?.saveCardSuccessFully()
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.saveCardDelegate?.failedToSaveCard(message: error.localizedDescription)
        }
    }
    
    //MARK:- Add debit card
    func addDebitCard(details : TransactionDetails){
       
        let params : JSONDictionary = [ApiKey.token : details.source, ApiKey.name : details.name]
        
        self.addDebitCardDelegate?.willAddDebitCard()
        
        WebServices.addDebitCard(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.addDebitCardDelegate?.debitCardAddedSuccessFully()
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.addDebitCardDelegate?.failedToAddDebitCard(msg: error.localizedDescription)
        }
    }
}
