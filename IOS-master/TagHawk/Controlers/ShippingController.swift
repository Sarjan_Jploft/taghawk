//
//  ShippingController.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ShippingControllerDelegate: AnyObject {
    func willRequestShippingCharges()
    func getShippingCharges(_ charge: Double)
    func shippingChargesFailed()
}

protocol CreateLabelDelegate : class {
    func willCreatelabel()
    func labelCreatedSuccessFully()
    func failedToCreateLabel(msg : String)
}

class ShippingController {
    
    weak var delegate: ShippingControllerDelegate?
    weak var labelDelegate : CreateLabelDelegate?
    
    //MARK:- Submit shipping info
    func submitShippingInfo(prodId: String, model: ShippingModel) {
        
        let shipModel = model.toJSON()
        let params: JSONDictionary = [ApiKey.productId: prodId, ApiKey.shipTo: JSON(shipModel)]
        
        self.delegate?.willRequestShippingCharges()
      
        WebServices.getShippingRates(parameters: params, success: { (json) in
            
            let totalCharge = json[ApiKey.data][ApiKey.total_charge][ApiKey.amount].doubleValue.rounded(toPlaces: 2)
            self.delegate?.getShippingCharges(totalCharge.rounded(toPlaces: 2))
            
        }) { (err) -> (Void) in
            self.delegate?.shippingChargesFailed()
        }
    }
    
    //Create label
    func createLabel(cartProduct : CartProduct, shipping : ShippingModel){
        
        let shipTo : JSONDictionary = [ApiKey.contact_name : shipping.fullName, ApiKey.email : UserProfile.main.email, ApiKey.city : shipping.city, ApiKey.postal_code : shipping.zipcode, ApiKey.street1 : shipping.streetAddress, ApiKey.state : shipping.stateCode, ApiKey.phone : shipping.phoneNumber, ApiKey.type : shipping.addressType]
        
        let params : JSONDictionary = [ApiKey.productId : cartProduct.productId, ApiKey.shipTo : JSON(shipTo)]
        
        self.labelDelegate?.willCreatelabel()
        
        WebServices.createLAbel(parameters: params, success: {[weak self] (json) in
            
            self?.labelDelegate?.labelCreatedSuccessFully()
            
        }) {[weak self] (error) -> (Void) in
            
            self?.labelDelegate?.failedToCreateLabel(msg: error.localizedDescription)
            
        }
        
    }
    
}
