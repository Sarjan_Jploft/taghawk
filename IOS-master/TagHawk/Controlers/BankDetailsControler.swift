//
//  AddBankDetailsControler.swift
//  TagHawk
//
//  Created by Appinventiv on 02/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit


protocol SaveBankDetailsProtocol : class {
    func willSaveBankDetail()
    func saveBankDetailsSuccessFully()
    func failedTOSaveBankDetails(message : String)
    func validateBankDetails(message : String)
}

protocol GetBankDetailsProtocol : class {
    func willGetBankDetail()
    func getBankDetailsSuccessFully(details : BankDetail)
    func failedTOGetBankDetails(message : String)
}


protocol GetCardsDelegate : class {
    func willgetCards()
    func cardsReceivedSuccessFully(cards : [Card])
    func failedToReceiveCards(message : String)
}

protocol DeleteCardDelegate : class {
    func willDeleteCard()
    func deleteCardSuccessFully(card : Card)
    func failedToDeleteCard(message : String)
}

protocol CashoutDelegate : class {
    func willCashOut()
    func cashoutSuccessFull(msg : String)
    func failedToCashOut(msg : String)
}

protocol GetBalanceDelegate : class {
    func willGetBalance()
    func getBalanceSuccessFully(cashoutBalance : Double,avilableSoonBalance : Double ,pendingBalance : Double)
    func failedToGetBalance(msg : String)
}

protocol UpdateMerchantDelegate : class {
    func willUpdateMerchant()
    func updateMerchantSuccessFully(merchant : MerchantData, shouldHandleCashoutFlow : Bool)
    func failedToUpdateMerchant(msg : String)
}


class BankDetailsControler {
    
    weak var getDetailsDelegate : GetBankDetailsProtocol?
    weak var saveDetailsDelegate : SaveBankDetailsProtocol?
    weak var cardDelegate : GetCardsDelegate?
    weak var deleteCardDelegate : DeleteCardDelegate?
    weak var cashOutDelegate : CashoutDelegate?
    weak var getBalanceDelegate : GetBalanceDelegate?
    weak var updateMerchantDelegate : UpdateMerchantDelegate?
    
    
    func getBankDetails() {
        
        self.getDetailsDelegate?.willGetBankDetail()
        
        let params : JSONDictionary = [:]
        
        WebServices.getBankData(parameters: params, success: {[weak self] (json) in
            
            UserProfile.main.bankDetails = BankDetail(json: json[ApiKey.data])
            UserProfile.main.balance = UserProfile.main.bankDetails.balance
            UserProfile.main.saveToUserDefaults()
            self?.getDetailsDelegate?.getBankDetailsSuccessFully(details: BankDetail(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            
            self?.getDetailsDelegate?.failedTOGetBankDetails(message: error.localizedDescription)
            
        }
        
    }
    
    
    func validateBankDetails(details : BankDetailsInput) -> Bool{
        
        if details.rountingNumber.isEmpty {
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.Please_Enter_Routing_Number.localized)
            return false
        }
        
        if details.rountingNumber.count < 9 {
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.Please_Enter_Valid_Routing_Number.localized)
            return false
        }
        
        if details.verifyRouting.isEmpty {
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.Please_Verify_Your_Routing_Number.localized)
            return false
        }
        
        if details.rountingNumber != details.verifyRouting{
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.Verify_Routing_Same_As_Routing.localized)
            return false
        }
        
        if details.accountNumbr.isEmpty {
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.Please_Enter_Account_Number.localized)
            return false
        }
        
        if details.accountNumbr.count < 12 {
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.Please_Enter_Valid_Account_Number.localized)
            return false
        }
        
        if details.verifyAccountNumber.isEmpty {
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.Please_Verify_Your_Account_Number.localized)
            return false
        }
        
        if details.accountNumbr != details.verifyAccountNumber {
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.Verify_Account_Same_As_Account.localized)
            return false
        }
        
        if details.firstName.isEmpty{
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.enterFirstName.localized)
            return false
        }
        
        if details.firstName.isEmpty{
            self.saveDetailsDelegate?.validateBankDetails(message: LocalizedString.enterLastName.localized)
            return false
        }
        
        return true
    }
    
    
    //MARK:- Get cards
    func getCatds(){
        
        let params : JSONDictionary = [ApiKey.customerId : UserProfile.main.stripeCustomerId]
        
        self.cardDelegate?.willgetCards()
        
        WebServices.getCards(parameters: params, success: { (json) in
            
            let cards = (json)[ApiKey.data][ApiKey.data].arrayValue.map { Card(json: $0) }
            self.cardDelegate?.cardsReceivedSuccessFully(cards: cards)
            
        }) { (error) -> (Void) in
            self.cardDelegate?.failedToReceiveCards(message: error.localizedDescription)
        }
    }
    
    
    //MARK:- derelte card
    func deleteCard(card : Card){
        
        let params : JSONDictionary = [ApiKey.cardId : card.id, ApiKey.customerId : card.customer]
        
        deleteCardDelegate?.willDeleteCard()
        
        WebServices.deleteCard(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.deleteCardDelegate?.deleteCardSuccessFully(card: card)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.deleteCardDelegate?.failedToDeleteCard(message: error.localizedDescription)
            
        }
    }
    
    //MARK:- Pay out webservice
    func payOut(amt : Double){
        let params : JSONDictionary = [ApiKey.method : "standard", ApiKey.amount : amt, ApiKey.sourceType : "card"]
        self.cashOutDelegate?.willCashOut()
        WebServices.payOut(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.cashOutDelegate?.cashoutSuccessFull(msg: json[ApiKey.message].stringValue)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.cashOutDelegate?.failedToCashOut(msg: error.localizedDescription)
            
        }
        
    }
    
    //MARK:- Get balance service
    func getBalance(){
        
        let params : JSONDictionary = [:]
        
        self.getBalanceDelegate?.willGetBalance()
        
        WebServices.getBalance(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            let data = json[ApiKey.data]
            let pendingBalance = data[ApiKey.pendingBalance].doubleValue.rounded(toPlaces: 2)
            let cashoutBalance = data[ApiKey.availableBalance].doubleValue.rounded(toPlaces: 2)
            let availableSoonBalance = data[ApiKey.availableSoonBalance].doubleValue.rounded(toPlaces: 2)
            
            weakSelf.getBalanceDelegate?.getBalanceSuccessFully(cashoutBalance: cashoutBalance, avilableSoonBalance: availableSoonBalance, pendingBalance: pendingBalance)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.getBalanceDelegate?.failedToGetBalance(msg: error.localizedDescription)
        }
    }
    
    //MARK:- Save bank details
    func saveBankDetails(details : BankDetailsInput){
        
        if !self.validateBankDetails(details: details) {
            return
        }
        
        self.saveDetailsDelegate?.willSaveBankDetail()
        
        let params : JSONDictionary = [ApiKey.routingNumber : details.rountingNumber, ApiKey.accountNumber : details.accountNumbr, ApiKey.accountHolderName : "\(details.firstName) \(details.lastName)"]
        
        WebServices.saveBankDetails(parameters: params, success: {[weak self] (json) in
            
            self?.saveDetailsDelegate?.saveBankDetailsSuccessFully()
            
        }) {[weak self] (error) -> (Void) in
            
            self?.saveDetailsDelegate?.failedTOSaveBankDetails(message: error.localizedDescription)
            
        }
    }
    
    //MARK:- Update merchant
    func updateMerchant(details : BankDetailsInput){
        
        if !self.validateBankDetails(details: details) {
            return
        }
        
        self.saveDetailsDelegate?.willSaveBankDetail()
        
        let params : JSONDictionary = [ApiKey.routingNumber : details.rountingNumber, ApiKey.accountNumber : details.accountNumbr, ApiKey.accountHolderName : "\(details.firstName) \(details.lastName)"]
        
        WebServices.updateMerchant(parameters: params, success: {[weak self] (json) in
            
            self?.saveDetailsDelegate?.saveBankDetailsSuccessFully()
            
        }) {[weak self] (error) -> (Void) in
            
            self?.saveDetailsDelegate?.failedTOSaveBankDetails(message: error.localizedDescription)
            
        }
    }
    
    //MARK:- get merchant status
    func getMerchantStatus(shouldHandleCashoutFlow : Bool = false){
        let params : JSONDictionary = [:]
        self.updateMerchantDelegate?.willUpdateMerchant()
        WebServices.getMerchantStatus(parameters: params, success: {[weak self] (json) in
            guard let weakSelf = self else { return }
            weakSelf.updateMerchantDelegate?.updateMerchantSuccessFully(merchant: MerchantData(json: json[ApiKey.data]), shouldHandleCashoutFlow: shouldHandleCashoutFlow)
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.updateMerchantDelegate?.failedToUpdateMerchant(msg: error.localizedDescription)
        }
    }
}
