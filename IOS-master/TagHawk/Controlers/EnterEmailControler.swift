//
//  EnterEmailControler.swift
//  TagHawk
//
//  Created by Appinventiv on 04/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol EnterEmailDelegate : class {
    
    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState)

}

class EnterEmailControler {

    weak var delegate : EnterEmailDelegate?
    
    
    
    func validateEmail(email : String) -> Bool {
        
        if email.isEmpty {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_Email.localized))
            return false
        }else if email.checkIfInvalid(.email) {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_A_Valid_Email.localized))
            return false
        }else{
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.valid)
            return true
        }
    }
}
