//
//  CartControler.swift
//  TagHawk
//
//  Created by Appinventiv on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol AddToCartDelegate : class {
    
    func willRequestAddProduct()
    func addProductSuccessFully()
    func errorOccuredWhileAddingProduct(errorType : CartControler.AddToCartErrorType, message : String)
    
}


protocol GetCartDelegate : class {
    
    func willRequestCart()
    func cartReceivedSuccessFully(products : [CartProduct])
    func errorOccuredWhileReceivingCart(message : String)
    
}


protocol RemoveAllProductsFromCartDelegate : class {
    func willRemoveAllProducts()
    func allProductsRemovedSuccessFully(message : String)
    func errorOccuredInRemovingProducts(messahe : String)
}


protocol DeleteProduct : class {
    func willRequestProductDelete()
    func productDeletedSuccessFully(prod : CartProduct)
    func errorOccuredWhileDeletingProduct(message : String)
}

class CartControler  {

    weak var addProductDelegate : AddToCartDelegate?
    weak var getCartDelegate : GetCartDelegate?
    weak var removeAllDelegate :RemoveAllProductsFromCartDelegate?
    weak var deleteProductDelegate : DeleteProduct?
    
    enum AddToCartErrorType : Int{
        case alreadyAdded = 400
        case shippingProductAlreadyAdded = 211
        case none = 0
    }

    //MARK:- Add to cart delegate
    func addToCart(product : Product, shipping : ShippingAvailability){
        
        self.addProductDelegate?.willRequestAddProduct()
        
        let params :JSONDictionary = [ApiKey.productId : product.productId, ApiKey.action : "1", ApiKey.shippingAvailibility : "\(shipping.rawValue)", ApiKey.sellerId: product.userId]
        
        WebServices.addToCart(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.addProductDelegate?.addProductSuccessFully()
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            
            switch error.code {
                
            case AddToCartErrorType.alreadyAdded.rawValue :
            weakSelf.addProductDelegate?.errorOccuredWhileAddingProduct(errorType: AddToCartErrorType.alreadyAdded, message: error.localizedDescription)
                
            case AddToCartErrorType.shippingProductAlreadyAdded.rawValue:
                weakSelf.addProductDelegate?.errorOccuredWhileAddingProduct(errorType: AddToCartErrorType.shippingProductAlreadyAdded, message: error.localizedDescription)

            default:
            weakSelf.addProductDelegate?.errorOccuredWhileAddingProduct(errorType: AddToCartErrorType.none, message: error.localizedDescription)
                
            }
        }
    }
    
    //MARK:- Get cart
    func getCart(){
        self.getCartDelegate?.willRequestCart()
        let params : JSONDictionary = [:]
        
        WebServices.getCart(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.getCartDelegate?.cartReceivedSuccessFully(products: json[ApiKey.data].arrayValue.map { CartProduct(json: $0) })

            
        }) {[weak self] (error) -> (Void) in
            
            guard let weakSelf = self else { return }
        weakSelf.getCartDelegate?.errorOccuredWhileReceivingCart(message: error.localizedDescription)
            
        }
    }
    
    //MARK:- remove all products from cart
    func removeAllProductsFromCart(){
        
        self.removeAllDelegate?.willRemoveAllProducts()
        
        let params :JSONDictionary = [ApiKey.action : "2"]
        
        WebServices.addToCart(parameters: params, success: {[weak self] (json) in
            
        guard let weakSelf = self else { return }
            
            weakSelf.removeAllDelegate?.allProductsRemovedSuccessFully(message: json[ApiKey.message].stringValue)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.removeAllDelegate?.errorOccuredInRemovingProducts(messahe: error.localizedDescription)
        }
    }
    
    
    func deleteSingleProductFromCart(product : CartProduct){
        
        self.deleteProductDelegate?.productDeletedSuccessFully(prod: product)
        
        let params :JSONDictionary = [ApiKey.productId : product.productId, ApiKey.action : "0"]

        WebServices.addToCart(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
        weakSelf.deleteProductDelegate?.productDeletedSuccessFully(prod: product)
            
        }) {[weak self] (error) -> (Void) in
            
            guard let weakSelf = self else { return }
            weakSelf.deleteProductDelegate?.errorOccuredWhileDeletingProduct(message: error.localizedDescription)
            
        }
        
    }
    
}
