//
//  BaseControler.swift
//  TagHawk
//
//  Created by Appinventiv on 07/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol LikeUnlikeDelegate : class {
    func willRequestLikeUnlikeProduct()
    func productLikeUnlikeSuccessFull(product : Product)
    func failedToLikeUnlikeProduct(message : String, product : Product)
}

protocol ReportProductDelegate : class {
    func willReportProduct()
    func productReportedSuccessFully(product : Product, msg: String)
    func failedToReportProduct()
}

class BaseControler  {

    weak var likeDelegate : LikeUnlikeDelegate?
    weak var reportProductDelegate : ReportProductDelegate?
    
    //MARK:- Like unlike products
    func likeUnlikeProduct(product : Product) {
        
        self.likeDelegate?.willRequestLikeUnlikeProduct()
    
        let params : JSONDictionary = [ApiKey.productId : product.productId, ApiKey.status : product.isLiked ? "0" : "1"]
        
        WebServices.likeUnlikeProduct(parameters: params, success: { [weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.likeDelegate?.productLikeUnlikeSuccessFull(product: product)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.likeDelegate?.failedToLikeUnlikeProduct(message: error.localizedDescription, product: product)
        }
    }
    
    //MARK:- Share product
    func shareProduct(product : Product){
        
        let params : JSONDictionary = [ApiKey.productId : product.productId]
        
        WebServices.shareProduct(parameters: params, success: { (json) in
            
        }) { (error) -> (Void) in
            
        }
    }
    
    //MARK:- Report Product
    func reportProduct(product : Product, reason: String) {
        
        self.reportProductDelegate?.willReportProduct()
        
        let params : JSONDictionary = [ApiKey.productId : product.productId, ApiKey.reason: reason]
        
        WebServices.reportProduct(parameters: params, success: { [weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            let msg = json[ApiKey.message].stringValue
            weakSelf.reportProductDelegate?.productReportedSuccessFully(product: product, msg: msg)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.reportProductDelegate?.failedToReportProduct()
        }
    }
    
}
