//
//  ProductDetailControler.swift
//  TagHawk
//
//  Created by Appinventiv on 31/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ProductDetailDelagate : class {
    
    func willRequestProductDetail()
    func productNotFound(msg: String)
    func productDetailReceivedSuccessFilly(product : Product)
    func failedToReceiveProducts(message : String)
    func productDeleteSuccess()
    func productDeleteFailed()
    
}

class ProductDetailControler : BaseControler  {

    weak var delegate : ProductDetailDelagate?
    
    //MARK:- Get product details
    func getProductDetails(id : String){
        
        self.delegate?.willRequestProductDetail()
        
        let params : JSONDictionary = [ApiKey.id : id]
        
        WebServices.getProductDetails(parameters: params, success: { (json) in
        
            self.delegate?.productDetailReceivedSuccessFilly(product: Product(json: json[ApiKey.data]))
            
        }) { (error) -> (Void) in
            
            if error.code == ApiCode.notFound {
                self.delegate?.productNotFound(msg: error.localizedDescription)
            }
            self.delegate?.failedToReceiveProducts(message: error.localizedDescription)
            
        }
        
    }
    
    func deleteProduct(_ productId: String) {
        let params: JSONDictionary = [ApiKey.productId: productId]
        WebServices.deleteProduct(parameters: params, success: { (json) in
            
            print(json)
            self.delegate?.productDeleteSuccess()
            
        }) { (err) -> (Void) in
            self.delegate?.productDeleteFailed()
        }
    }
    
}
