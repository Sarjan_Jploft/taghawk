//
//  SelectLocationControler.swift
//  TagHawk
//
//  Created by Appinventiv on 06/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit


protocol GetPlacesDelegate : class {
    func willGetPlaces()
    func placesReceivedSuccessFully(placesStringArray : [String], placeIdArray : [String])
    func failedToReceivePlace()
}

protocol GetCordinatesDelegate : class {
    func willGetCordinates()
    func cordinatesReceivedSuccessFully(lat : Double, long : Double)
    func failedToReceiveCordinates()
}

class SelectLocationControler  {

    weak var delegate : GetPlacesDelegate?
    weak var cordinateDelegate : GetCordinatesDelegate?
    
    //MARK:- get places
    func getPlaces(txt : String){
        
        self.delegate?.willGetPlaces()
        
        previousRequest?.cancel()
        
        WebServices.getPlaces(txt: txt, success: {[weak self ] (sucess, data) in
            guard let weakSelf = self else { return }

            if sucess{
              
                let placeStr = data.map({ (item) -> String in

                    guard let desc = item["description"] as? String else { return ""}
                    return desc
                })
                
                let placeId = data.map({ (item) -> String in
                    guard let placeId = item["place_id"] as? String else { return ""}
                    return placeId
                })
                    guard let weakSelf = self else { return }
                weakSelf.delegate?.placesReceivedSuccessFully(placesStringArray: placeStr, placeIdArray: placeId)
                
            }else{
              weakSelf.delegate?.failedToReceivePlace()
            }
            
        }) {[weak self ] (error) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToReceivePlace()
       
        }
    }
    
    //MARK:- get cordinates
    func getCordinates(placeId : String){
        self.cordinateDelegate?.willGetCordinates()
        WebServices.getCordinates(placeId: placeId, success: {[weak self] (sucess, lat , long) in
            guard let weakSelf = self else { return }

            if sucess{
            weakSelf.cordinateDelegate?.cordinatesReceivedSuccessFully(lat: lat ?? 0, long: long ?? 0)
           
            }else{
                weakSelf.cordinateDelegate?.failedToReceiveCordinates()
            }
        }) {[weak self] (error) in
            guard let weakSelf = self else { return }
            weakSelf.cordinateDelegate?.failedToReceiveCordinates()
        }
    }
}
