//
//  ForgotPasswordControler.swift
//  TagHawk
//
//  Created by Appinventiv on 25/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ForgotPasswordDelegate : class {
    func willRequestForgotPassword()
    func forgotPasswordSuccessfully()
    func failedToForgotPassword(message : String)
    func invalidInput(textFieldType : TagHawkTextFieldTableViewCell.TextFieldType, textFieldState : TextFieldState)
}

class ForgotPasswordControler {

    weak var delegate : ForgotPasswordDelegate?
    
    func validateEmail(email : String) -> Bool {
        
        if email.isEmpty {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_Email.localized))
            return false
        }else if email.checkIfInvalid(.email) {
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.invalid(LocalizedString.Please_Enter_A_Valid_Email.localized))
            return false
        }else{
            self.delegate?.invalidInput(textFieldType: TagHawkTextFieldTableViewCell.TextFieldType.email, textFieldState: TextFieldState.valid)
            return true
        }
        
    }
    
    func forgotPassword(emil : String, userType : UserProfile.UserType){

        
        if !validateEmail(email : emil){
            return
        }
        
        self.delegate?.willRequestForgotPassword()
        
        let params : JSONDictionary = [ApiKey.email : emil, ApiKey.userType : userType.rawValue]
        
        WebServices.forgotPassword(parameters: params, success: { (json) in
            
            self.delegate?.forgotPasswordSuccessfully()
            
        }) { (error) -> (Void) in
            self.delegate?.failedToForgotPassword(message: error.localizedDescription)
        }
    }
}
