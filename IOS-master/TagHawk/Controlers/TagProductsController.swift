//
//  TagProductsController.swift
//  TagHawk
//
//  Created by Appinventiv on 18/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

protocol TagProductsControllerDelegate: AnyObject {
    func willGetProducts()
    func getProductSuccess(prodArr: [Product], nextPage: Int)
    func getProductFailure(_ message: String)
}

class TagProductsController {
    
    weak var delegate: TagProductsControllerDelegate?
    
    //MARK:- Get tag products
    func getTagProducts(_ tagId: String, pageNo: Int, categoryId : String = "", searchText : String = "", currentProducts : [Product]) {
        
        if SelectedFilters.shared.appliedFiltersOnItems.lat == 0.0 && SelectedFilters.shared.appliedFiltersOnItems.long == 0.0 {
            
            SelectedFilters.shared.appliedFiltersOnItems.lat = LocationManager.shared.latitude
            SelectedFilters.shared.appliedFiltersOnItems.long = LocationManager.shared.longitude
        }
        
        var params : JSONDictionary = [ApiKey.pageNo : pageNo, ApiKey.limit : 15, ApiKey.communityId: tagId,]
        
        if !searchText.isEmpty{ params[ApiKey.searchKey] = searchText }
        
        if !categoryId.isEmpty{
            params[ApiKey.productCategoryId] = categoryId
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.fromPrice > 0{
            params[ApiKey.priceFrom] = SelectedFilters.shared.appliedFiltersOnItems.fromPrice
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.toPrice > 0{
            params[ApiKey.priceTo] = SelectedFilters.shared.appliedFiltersOnItems.toPrice
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.lat != 0{
            params[ApiKey.lat1] = String(SelectedFilters.shared.appliedFiltersOnItems.lat)
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.long != 0{
            
            params[ApiKey.long1] = String(SelectedFilters.shared.appliedFiltersOnItems.long)
        }
        
        if !SelectedFilters.shared.appliedFiltersOnItems.condiotions.isEmpty{
            let conditionsArray = SelectedFilters.shared.appliedFiltersOnItems.condiotions.map { "\($0.rawValue)" }
            
            let cond = conditionsArray.joined(separator: ",")
            
            params[ApiKey.condition] = cond
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.distance != 5{
            params[ApiKey.distance] = SelectedFilters.shared.getMilesForIndex(index: SelectedFilters.shared.appliedFiltersOnItems.distance)
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.postedWithin != .none{
            params[ApiKey.postedWithIn] = SelectedFilters.shared.appliedFiltersOnItems.postedWithin.rawValue
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.rating != 0 {
            params[ApiKey.sellerRating] = SelectedFilters.shared.appliedFiltersOnItems.rating
        }
        
        //Note This is done due to an issue occuring at server end when we are sending boolean value it is becomming integer at server end and when we are sending true and false as string it is becomming boolean at server end.
        
        if SelectedFilters.shared.appliedFiltersOnItems.verifiedSellorsOnly {
            params[ApiKey.sellerVerified] = SelectedFilters.shared.appliedFiltersOnItems.verifiedSellorsOnly ? "true" : "false"
        }
        
        switch SelectedFilters.shared.appliedSortingOnProducts {
        case .newest:
            params[ApiKey.sortBy] = "created"
            params[ApiKey.sortOrder] = "-1"
            
        case .priceLowToHigh:
            params[ApiKey.sortBy] = "firmPrice"
            params[ApiKey.sortOrder] = "1"
            
        case .priceHightToLow:
            params[ApiKey.sortBy] = "firmPrice"
            params[ApiKey.sortOrder] = "-1"
            
        default:
            break
        }
        
        let allPromotedProducts = currentProducts.filter { $0.isPromoted }
        
        if !allPromotedProducts.isEmpty && pageNo > 1{
            params[ApiKey.promotedProductIds] = allPromotedProducts.map { $0.productId }.joined(separator: ",")
        }
        
        delegate?.willGetProducts()
        
        
        WebServices.getTagProducts(parameters: params, success: { (json) in
            
            var prodArr = [Product]()
            json[ApiKey.data].arrayValue.forEach({ (dataJson) in
                prodArr.append(Product(json: dataJson))
            })
            
            self.delegate?.getProductSuccess(prodArr: prodArr, nextPage: json[ApiKey.next_hit].intValue)
            
        }) { (err) -> (Void) in
            self.delegate?.getProductFailure(err.localizedDescription)
        }
    }
}
