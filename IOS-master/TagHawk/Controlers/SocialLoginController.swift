//
//  LandingController.swift
//  Zing
//
//  Created by Bhavneet Singh on 22/02/18.
//  Copyright © 2018 Appinventiv. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SocialLoginControllerDelegate: NSObjectProtocol {
    func willLoginSocialAccount()
    func socialLoginSuccess(email: String, socialId: String,profilePicture : String, name : String ,socialType: SocialLoginController.LoginType, firstName: String, lastName: String)
    func socialLoginFailed(message: String)
    func userNotRegistered()
}

protocol SocialLoginServiceDelegate : class {
    func willLoginSocialLoginService()
    func loginSuccessForSocialServicerModel(user : UserProfile)
    func socialLoginServiceFailed(message: String)
    func invalidInputFromSocialAccount(message : String)
    func getEmail()
}

protocol CheckSocialLoginDelegate : class {
    func willCheckSocialLogin()
    func checkFacebookLoginSuccessFull(isExist : Bool)
    func failedToCheckFacebookLogin(message : String)
}

class SocialLoginController: NSObject {
    
    enum LoginType : String{
        case normal = "0"
        case facebook = "1"
    }
    
    weak var delegate: SocialLoginControllerDelegate?
    weak var socialServiceDelegate : SocialLoginServiceDelegate?
    weak var checkFbLoginDelegate : CheckSocialLoginDelegate?
    
    func fbLogin() {
        self.delegate?.willLoginSocialAccount()
        FacebookController.shared.getFacebookUserInfo(fromViewController: sharedAppDelegate.window!.rootViewController!, success: { [weak self] (user) in
            printDebug("Facebook Success")
            
            guard let strongSelf = self else { return }
            
            var params: JSONDictionary = ["facebookId": user.id]
            if !user.email.isEmpty { params["email"] = user.email }
            
            strongSelf.delegate?.socialLoginSuccess(email: user.email, socialId: user.id, profilePicture: user.picture?.absoluteString ?? "", name: user.name, socialType: SocialLoginController.LoginType.facebook, firstName: user.first_name, lastName: user.last_name)
        

        }) { [weak self] (error) in
            guard let err = error, let strongSelf = self else { return }
            strongSelf.delegate?.socialLoginFailed(message: err.localizedDescription)
        }
    }
    
    
    func validateSocialLgin(userinput : UserInput) -> Bool{
        if userinput.fbId.isEmpty{
            self.socialServiceDelegate?.invalidInputFromSocialAccount(message: LocalizedString.Social_Id_not_received.rawValue)
            return false
        }
        
        return true
    }
}

extension SocialLoginController{
   
    
    func checkSocialLogin(email : String, fbId : String){
     
        var params : JSONDictionary = [:]
        
        params[ApiKey.socialId] = fbId
        if !email.isEmpty { params[ApiKey.email] = email }
        
        self.checkFbLoginDelegate?.willCheckSocialLogin()
        
        WebServices.checkSocialLogin(parameters: params, success: {[weak self] (json) in
            
            self?.checkFbLoginDelegate?.checkFacebookLoginSuccessFull(isExist: json[ApiKey.data][ApiKey.isExist].boolValue)
            
        }) { (error) -> (Void) in
            self.checkFbLoginDelegate?.failedToCheckFacebookLogin(message: error.localizedDescription)
        }
    }
    
    func sociialLogin(userInput : UserInput){
        
        if !validateSocialLgin(userinput: userInput){
            return
        }
        
        self.socialServiceDelegate?.willLoginSocialLoginService()
        
        var params : JSONDictionary = [ApiKey.socialId : userInput.fbId,ApiKey.profilePicture : userInput.profileImage, ApiKey.deviceId : DeviceDetail.deviceId, ApiKey.userType : "2", ApiKey.deviceToken : DeviceDetail.fcmToken, ApiKey.socialLoginType : LoginType.facebook.rawValue, ApiKey.fullName : userInput.fullName, ApiKey.firstName: userInput.firstName, ApiKey.lastName: userInput.lastName]
        
        if !userInput.email.isEmpty{
            params[ApiKey.email] = userInput.email
        }
        
        printDebug("social params....\(params)")
        
        WebServices.socialLogin(parameters: params, success: { [weak self] (user) in
            FireBaseChat.shared.createUserNode(data: user)
            self?.socialServiceDelegate?.loginSuccessForSocialServicerModel(user: user)
        }) { [weak self] (error) -> (Void) in
           
            if error.code == ApiCode.emptyEmail{
                self?.socialServiceDelegate?.getEmail()
            }else{
                self?.socialServiceDelegate?.socialLoginServiceFailed(message: error.localizedDescription)

            }
        }
    }
}
