//
//  GiftControler.swift
//  TagHawk
//
//  Created by Admin on 5/21/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol GetPromotionPackagesDelegate : class {
    func willRequestPromotionPackages()
    func promotionPackagesReceivedSuccessFully(plans : [GiftPlan], totalPoints : Int)
    func failedToReceivePromotionPackages(msg : String)
}

protocol PromoteSelectedProductsDelegate : class {
    func vaildatePromotion(msg : String)
    func willPromoteProduct()
    func productsPromotedSuccessfully(msg : String)
    func failedToPromoteProduct(msg : String)
}

class GiftControler {

    weak var packageListDelegate : GetPromotionPackagesDelegate?
    weak var productDelegate: UserProductDelegate?
    weak var promotionDelegate : PromoteSelectedProductsDelegate?
    
    //MARK:- get promotion packages
    func getPromotionPackages(){
        
        self.packageListDelegate?.willRequestPromotionPackages()
        
        WebServices.getPromotionPackages(parameters: [:], success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.packageListDelegate?.promotionPackagesReceivedSuccessFully(plans: json[ApiKey.data].arrayValue.map { GiftPlan(json: $0) }, totalPoints: json[ApiKey.rewardPoint].intValue)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.packageListDelegate?.failedToReceivePromotionPackages(msg: error.localizedDescription)
        }
    }
    
    //MARK:- Get user products
    func getUserProduct(userId: String = "", productStatus: Int, page: Int) {
        
        productDelegate?.willGetProducts()
        
        var dict = [String: Any]()
        dict[ApiKey.productStatus] = productStatus
        dict[ApiKey.pageNo] = page
        dict[ApiKey.limit] = 18
        
        if !userId.isEmpty {
            dict[ApiKey.userId] = userId
        }
        
        dict[ApiKey.isPromoted] = "false"
        
        WebServices.getUserProducts(parameters: dict, success: { [weak self] (data) in
            var productArr = [Product]()
            // Temp Change
            let dict = data.dictionaryValue
            let dataTemp = dict["data"]!.arrayValue
            
            for json in dataTemp
            {
                productArr.append(Product(json: json))
            }
            
            /*
            dataTemp.arrayValue.forEach({ (json) in
                productArr.append(Product(json: json))
            })
            */
            self?.productDelegate?.userProductServiceReturn(userProduct: productArr, nextPage: data[ApiKey.next_hit].intValue)
        }) { (error) -> (Void) in
            if error.code == NSURLErrorNotConnectedToInternet {
                self.productDelegate?.userProductsFailed(errorType: .noInternet)
            } else {
                self.productDelegate?.userProductsFailed(errorType: .failed)
            }
        }
    }

    //MARK:- Validate for promotion products
    func validateForPromotionProducts(productsToPromote : [Product], selectedGiftPlan : GiftPlan, totalPoints : Int) -> Bool {
        
        if productsToPromote.isEmpty {
            self.promotionDelegate?.vaildatePromotion(msg: LocalizedString.PleaseSelectAProduct.localized)
            return false
        }
        
        return true
    }
    
    //MARK:- Promote selected products
    func promoteSelectedProducts(productsToPromote : [Product], selectedGiftPlan : GiftPlan, totalPoints : Int){
        
        if !self.validateForPromotionProducts(productsToPromote: productsToPromote, selectedGiftPlan: selectedGiftPlan, totalPoints: totalPoints) { return }
        
        let params : JSONDictionary = [ApiKey.days : selectedGiftPlan.days, ApiKey.price : selectedGiftPlan.rewardPoint * productsToPromote.count, ApiKey.products : productsToPromote.map { $0.productId }.joined(separator: ",")]
     
        self.promotionDelegate?.willPromoteProduct()
        
        WebServices.promoteSpecificProducts(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.promotionDelegate?.productsPromotedSuccessfully(msg: json[ApiKey.message].stringValue)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.promotionDelegate?.failedToPromoteProduct(msg: error.localizedDescription)
        }
    }
    
}
