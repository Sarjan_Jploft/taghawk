//
//  PaymentHistoryControler.swift
//  TagHawk
//
//  Created by Admin on 6/5/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol confirmOrder : class {
    func willConfirmOrder()
    func confirmOrderSuccessFylly()
    func failedToConfirmorder(msg : String)
}

protocol PaymentHistoryDelegate : class {
    func willRequestPaymentHistory()
    func paymentHistoryReceivedSuccessFully(history : [PaymentHistory])
    func failedToReceivePaymentHistory(msg : String)
}

protocol ReleasepaymentDelegate : class {
    func willReleasePayment()
    func paymentReleasedSuccessFully(historyObj : PaymentHistory)
    func failedToReleasePayment(msg : String)
}

protocol RequestRefundDelegate : class {
    func willRequestRefund()
    func refundreceivedSuccessfully(historyObj : PaymentHistory)
    func failedToReceiveRefund(msg : String)
}

protocol AcceptRefundDelegate : class {
    func willAcceptRefundDelegate()
    func refundAcceptedSuccessfully(historyObj : PaymentHistory)
    func failedToAcceptRefund(msg : String)
}

protocol DeclineRefundDelegate : class {
    func willDeclineRefund()
    func declineSuccessFully(historyObj : PaymentHistory)
    func failedToDecline(msg : String)
}

protocol ReleaseRefundDelegate : class {
    func willReleaseRefund()
    func refundReleasedSuccessfully(historyObj : PaymentHistory)
    func failedToReleaseRefund(msg : String)
}

protocol MakeDesputeProtocol : class {
    func willMmakeDespute()
    func desputeMadeSuccessfully(historyObj : PaymentHistory)
    func failedToMakeDespute(msg : String)
}

protocol GetPaymentHistoryStatusDelegate : class {
    func willGetPaymentHistoryStatus()
    func paymentHistoryStatusSuccessfully(history : PaymentHistory)
    func failedToReceivePaymentHistoryStatus(msg : String)
}

class PaymentHistoryControler  {
    
    //MARK:- variables
    weak var historyDelegate : PaymentHistoryDelegate?
    weak var refundDelegate : RequestRefundDelegate?
    weak var confirmOrderDelegate : confirmOrder?
    weak var releasePaymentDelegate : ReleasepaymentDelegate?
    weak var acceptRefundDelegate : AcceptRefundDelegate?
    weak var declineRefundDelegate : DeclineRefundDelegate?
    weak var releaseRefundDelegate : ReleaseRefundDelegate?
    weak var desputeDelegate : MakeDesputeProtocol?
    weak var getHistoryStatus : GetPaymentHistoryStatusDelegate?
    
    //MARK:- get payment history
    func getPaymentHistory(){
        
        let patams : JSONDictionary = [:]
        
        self.historyDelegate?.willRequestPaymentHistory()
        
        WebServices.getPaymentHistory(parameters: patams, success: { (json) in
            
            self.historyDelegate?.paymentHistoryReceivedSuccessFully(history: json[ApiKey.data].arrayValue.map { PaymentHistory(json: $0) })
            
        }) { (error) -> (Void) in
            self.historyDelegate?.failedToReceivePaymentHistory(msg: error.localizedDescription)
        }
    }
    
    //MARK:- Confirm order service
    func confirmOrder(orderId : String){
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        self.confirmOrderDelegate?.willConfirmOrder()
        
        WebServices.confirmOrder(parameters: params, success: {[weak self] (json) in
            guard let weakSelf = self else { return }
            weakSelf.confirmOrderDelegate?.confirmOrderSuccessFylly()
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.confirmOrderDelegate?.failedToConfirmorder(msg: error.localizedDescription)
        }
    }
    
    //MARK:- Request refund service
    func requestRefund(orderId : String){
        
        self.refundDelegate?.willRequestRefund()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        
        WebServices.requestRefund(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.refundDelegate?.refundreceivedSuccessfully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.refundDelegate?.failedToReceiveRefund(msg: error.localizedDescription)
            
        }
    }
    
    //MARK:- Release payment webservice
    func releasePayment(orderId : String){
        
        self.refundDelegate?.willRequestRefund()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        
        WebServices.releasePayment(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.refundDelegate?.refundreceivedSuccessfully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.refundDelegate?.failedToReceiveRefund(msg: error.localizedDescription)
            
        }
        
    }
    
    //MARK:- Accept refund
    func acceptRefund(orderId : String){
        
        self.acceptRefundDelegate?.willAcceptRefundDelegate()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        
        WebServices.acceptRefund(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.acceptRefundDelegate?.refundAcceptedSuccessfully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.acceptRefundDelegate?.failedToAcceptRefund(msg: error.localizedDescription)
        }
    }
    
    //MARK:- Decline refund
    func declineRefund(orderId : String, msg : String){
        
        self.declineRefundDelegate?.willDeclineRefund()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId, ApiKey.declineMessage : msg]
        
        WebServices.declineRefund(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.declineRefundDelegate?.declineSuccessFully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.declineRefundDelegate?.failedToDecline(msg: error.localizedDescription)
        }
    }
    
    //MARK:- release refund
    func releaseRefund(orderId : String){
        
        self.declineRefundDelegate?.willDeclineRefund()
        
        let params : JSONDictionary = [ApiKey.orderId : orderId]
        
        WebServices.releaseRefund(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.declineRefundDelegate?.declineSuccessFully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.declineRefundDelegate?.failedToDecline(msg: error.localizedDescription)
        }
    }
    
    //MAKE:- Make dispute
    func makeDispute(orderId : String, msg : String, images : [String]){
        
        var params : JSONDictionary = [ApiKey.orderId : orderId, ApiKey.statement : msg]
        
        var imagesArray = images.map { (strUrl) -> JSONDictionary in
            return [ApiKey.url : strUrl]
        }
        
        params[ApiKey.proof] = JSON(imagesArray)
        
        self.desputeDelegate?.willMmakeDespute()
        
        WebServices.makeDispute(parameters: params, success: {[weak self] (json) in
            guard let weakSelf = self else { return }
            weakSelf.desputeDelegate?.desputeMadeSuccessfully(historyObj: PaymentHistory(json: json[ApiKey.data]))
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.desputeDelegate?.failedToMakeDespute(msg: error.localizedDescription)
        }
    }
    
    //MARK:- get products payment history
    func getProductPaymentHistory(id: String){
        
        self.getHistoryStatus?.willGetPaymentHistoryStatus()
        
        let params: JSONDictionary = [ApiKey.productId : id]
        
        WebServices.getProductPaymentHistory(parameters: params, success: {[weak self](payment) in
            
            guard let weakSelf = self else { return }
            weakSelf.getHistoryStatus?.paymentHistoryStatusSuccessfully(history: payment)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.getHistoryStatus?.failedToReceivePaymentHistoryStatus(msg: error.localizedDescription)
        }
    }
    
    
    
}
