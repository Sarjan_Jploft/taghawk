//
//  NotificationsController.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 13/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

protocol NotificationsControllerDelegate: AnyObject {
    func willGetNotifications()
    func notificationSuccess(notifModel: NotificationModel)
    func notificationArray(notifArr: [Notifications], nextPage: Int)
    func notificationReadSuccess()
    func notificationFailed()
}

class NotificationsController {
    
    weak var delegate: NotificationsControllerDelegate?
    
    func getNotificationsData(pageNo: Int) {
        
        delegate?.willGetNotifications()
        let params: JSONDictionary = [ApiKey.pageNo: pageNo, ApiKey.limit: 10]
        WebServices.commonGetAPI(parameters: params, endPoint: .notificationList, loader: true, success: { (json) in
            
            let notificationModel = NotificationModel(json: json)
            self.delegate?.notificationArray(notifArr: notificationModel.notificationArr, nextPage: json[ApiKey.next_hit].intValue)
            self.delegate?.notificationSuccess(notifModel: notificationModel)
            
        }) { (err) -> (Void) in
            self.delegate?.notificationFailed()
        }
    }
    
    func readNotification(notificationId: String) {
        let params: JSONDictionary = [ApiKey.notificationId: notificationId]
        WebServices.readNotification(parameters: params, success: { (json) in
            
            self.delegate?.notificationReadSuccess()
            
        }) { (err) -> (Void) in
            
        }
    }
    
}
