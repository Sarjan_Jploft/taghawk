//
//  SearchControler.swift
//  TagHawk
//
//  Created by Appinventiv on 30/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol SearchResultDelegate : class {
    func willRequestSearchResult()
    func searchResultReceivedSuccessfully(result : [SearchResult])
    func searchResultForTagsReceivedSuccessfully(result : [Tag])
    func failedToReceiveSearchResult(message : String)
}

class SearchControler {

    weak var delegate : SearchResultDelegate?
    
    //MARK:- Search results webservice
    func searchResults(searchText : String, categoryId : String = ""){
        
        self.delegate?.willRequestSearchResult()
        
        var params : JSONDictionary = [ApiKey.userId : UserProfile.main.userId]
        
        if !categoryId.isEmpty{
            params[ApiKey.productCategoryId] = categoryId
        }
        
        if !searchText.isEmpty{
            params[ApiKey.searchKey] = searchText
        }
        
        let filters = SelectedFilters.shared.appliedFiltersOnItems
        
        if filters.fromPrice > 0{
            params[ApiKey.priceFrom] = filters.fromPrice
        }
        
        if filters.toPrice > 0{
            params[ApiKey.priceTo] = filters.toPrice
        }
        
        if filters.lat != 0 {
            params[ApiKey.lat1] = String(filters.lat)
        }
        
        if filters.long != 0{
            params[ApiKey.long1] = String(filters.long)
        }
        
        if !filters.condiotions.isEmpty{
            params[ApiKey.condition] = filters.condiotions.map { $0.rawValue }
        }
        
        if filters.distance != 5{
            params[ApiKey.distance] = SelectedFilters.shared.getMilesForIndex(index: filters.distance)
        }
        
        if filters.postedWithin != .none{
            params[ApiKey.postedWithIn] = filters.postedWithin.rawValue
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.rating != 0 {
            params[ApiKey.sellerRating] = filters.rating
        }
        
        if filters.verifiedSellorsOnly {
            params[ApiKey.sellerVerified] = SelectedFilters.shared.appliedFiltersOnTag.verifiedSellorsOnly
        }
        
        WebServices.getSuggestions(parameters: params, success: { (json) in
            
            let results = json[ApiKey.data].arrayValue
            
            self.delegate?.searchResultReceivedSuccessfully(result: results.map { SearchResult(json: $0) } )
            
        }) { (error) -> (Void) in
            self.delegate?.failedToReceiveSearchResult(message : error.localizedDescription)
        }
        
    }
    
    //MARK:- Search results for tags
    func searchResultsForTags(searchText : String){
        
        self.delegate?.willRequestSearchResult()
        
        var params : JSONDictionary = [:]
        
        if !searchText.isEmpty{
            params[ApiKey.name] = searchText
        }
        
        let filters = SelectedFilters.shared.appliedFiltersOnTag
        
        if filters.lat > 0{
            params[ApiKey.lat1] = String(filters.lat)
        }
        
        if filters.long > 0{
            params[ApiKey.long1] = String(filters.long)
        }
        
        if filters.distance != 5{
            params[ApiKey.distance] = SelectedFilters.shared.getMilesForIndex(index: filters.distance)
        }
        
        if filters.tagType != .none {
            params[ApiKey.tagType] = filters.tagType.rawValue
        }
        
        WebServices.getTagSuggestions(parameters: params, success: { (json) in
            
            let results = json[ApiKey.data].arrayValue
            self.delegate?.searchResultForTagsReceivedSuccessfully(result: results.map { Tag(json: $0) })
            
        }) { (error) -> (Void) in
            self.delegate?.failedToReceiveSearchResult(message : error.localizedDescription)
        }
        
    }
    
    //MARK:- Sarch result for shelf
    func searchResultsForShelf(tagId : String , searchText : String, categoryId : String = ""){
        
        self.delegate?.willRequestSearchResult()
        
        var params : JSONDictionary = [ApiKey.userId : UserProfile.main.userId, ApiKey.communityId : tagId]
        
        if !categoryId.isEmpty{
            params[ApiKey.productCategoryId] = categoryId
        }
        
        if !searchText.isEmpty{
            params[ApiKey.searchKey] = searchText
        }
        
        let filters = SelectedFilters.shared.appliedFiltersOnItems
        
        if filters.fromPrice > 0{
            params[ApiKey.priceFrom] = filters.fromPrice
        }
        
        if filters.toPrice > 0{
            params[ApiKey.priceTo] = filters.toPrice
        }
        
        if filters.lat > 0{
            params[ApiKey.lat1] = String(filters.lat)
        }
        
        if filters.long > 0{
            params[ApiKey.long1] = String(filters.long)
        }
        
        if !filters.condiotions.isEmpty{
            params[ApiKey.condition] = filters.condiotions.map { $0.rawValue }
        }
        
        if filters.distance != 5{
            params[ApiKey.distance] = SelectedFilters.shared.getMilesForIndex(index: filters.distance)
        }
        
        if filters.postedWithin != .none{
            params[ApiKey.postedWithIn] = filters.postedWithin.rawValue
        }
        
        if SelectedFilters.shared.appliedFiltersOnItems.rating != 0 {
            params[ApiKey.sellerRating] = filters.rating
        }
        
        if filters.verifiedSellorsOnly {
            params[ApiKey.sellerVerified] = SelectedFilters.shared.appliedFiltersOnItems.verifiedSellorsOnly
        }
        
        WebServices.getSuggestions(parameters: params, success: { (json) in
            
            let results = json[ApiKey.data].arrayValue
            
            self.delegate?.searchResultReceivedSuccessfully(result: results.map { SearchResult(json: $0) } )
            
        }) { (error) -> (Void) in
            self.delegate?.failedToReceiveSearchResult(message : error.localizedDescription)
        }
        
    }
    
}
