//
//  BlockedUserControler.swift
//  TagHawk
//
//  Created by Appinventiv on 12/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol GetBlockUsersList : class {
    func willGetBlockUsers()
    func blockedUsersReceivedSuccessfully(users : [FollowingFollowersModel], nextPage : Int)
    func failedToGetBlockedUsers(message : String)
}

protocol UnBlockUserDelegate : class {
    func willRequestUnBlockUser()
    func unBlockUserSuccessfull(user : String)
    func failedToUnBlockUser(message : String)
}

protocol BlockUserDelegate : class {
    func willRequestBlockUser()
    func blockUserSuccessfull(user : String)
    func failedToBlockUser(message : String)
}


class BlockedUserControler {

    weak var delegate : GetBlockUsersList?
    weak var unblockUserDelegate : UnBlockUserDelegate?
    weak var blockUserdelegate : BlockUserDelegate?

    //MARK:- Get block user list
    func getBlockedUserList(page : Int){
        
        self.delegate?.willGetBlockUsers()
        
        let params : JSONDictionary = [ApiKey.pageNo : page, ApiKey.limit : 10]
        
        WebServices.blockUserList(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.delegate?.blockedUsersReceivedSuccessfully(users: json[ApiKey.data][ApiKey.data].arrayValue.map { FollowingFollowersModel(json: $0) }, nextPage: json[ApiKey.next_hit].intValue)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToGetBlockedUsers(message: error.localizedDescription)
        }
    }
    
    //MARK:- Block user
    func blockUser(user : String){
        
        let params : JSONDictionary = [ApiKey.userId : user, ApiKey.action : "3"]
        self.blockUserdelegate?.willRequestBlockUser()
        
        WebServices.removeFriend(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.blockUserdelegate?.blockUserSuccessfull(user: user)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.blockUserdelegate?.failedToBlockUser(message: error.localizedDescription)
        }
    }
    
    //MARK:- Unblock user
    func unBlockUser(user : String){
        
        let params : JSONDictionary = [ApiKey.userId : user, ApiKey.action : "4"]
        self.unblockUserDelegate?.willRequestUnBlockUser()
        
        WebServices.removeFriend(parameters: params, success: {[weak self] (json) in
            guard let weakSelf = self else { return }
            weakSelf.unblockUserDelegate?.unBlockUserSuccessfull(user: user)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.unblockUserDelegate?.failedToUnBlockUser(message: error.localizedDescription)
        }
    }
}
