//
//  SettingsControler.swift
//  TagHawk
//
//  Created by Appinventiv on 06/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol LogoutDelegate : class {
    func willLogout()
    func logoutSuccessFully()
    func failedToLogout(message : String)
}

protocol GetHtmlContentDelegate : class {
    func willGetHtmlContent()
    func htmlContentReceivedSuccessFully(htmlString : String)
    func failedToreceiveHtmlContent(message : String)
}

protocol NotifivationSettingsDelegate : class{
    func willChangeNotificationSettings()
    func notificationSettingsChangedSuccessFully(isMute : Bool)
    func failedToChangeNotificationSettings(isMute : Bool)
}

class SettingsControler {

    weak var logoutDelegate : LogoutDelegate?
    weak var notificationSettingsDelegate : NotifivationSettingsDelegate?
    weak var gethtmlDelegate : GetHtmlContentDelegate?
    
    //MARK:- Log out service
    func logout(){
        
        self.logoutDelegate?.willLogout()
        
        let params : JSONDictionary = [ApiKey.deviceId : DeviceDetail.deviceId]
        
        WebServices.logout(parameters: params, success: {[weak self] (json) in
            
            self?.logoutDelegate?.logoutSuccessFully()
            
        }) {[weak self] (error) -> (Void) in
            self?.logoutDelegate?.failedToLogout(message: error.localizedDescription)
        }
        
    }
    
    //MARK:- Update notification settings
    func updateNotificationSettings(isMute : Bool){
        
        let params : JSONDictionary = [ApiKey.isMute : isMute ? 0 : 1]
        
        WebServices.notificationSettings(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
           
            UserProfile.main.isNotificationMuted = !isMute
            UserProfile.main.saveToUserDefaults()
        weakSelf.notificationSettingsDelegate?.notificationSettingsChangedSuccessFully(isMute: UserProfile.main.isNotificationMuted)
            
        }) {[weak self] (error) -> (Void) in
            self?.notificationSettingsDelegate?.failedToChangeNotificationSettings(isMute: !isMute)
            
        }
        
    }
    
    //MARK:- View html content
    func viewHtmlContent( type : LoadUrlVC.LodeUrlType){
        
        let patams : JSONDictionary = [ApiKey.entrance : type.rawValue]
        
        self.gethtmlDelegate?.willGetHtmlContent()
        
        WebServices.viewHtmlContent(parameters: patams, success: {[weak self] (json) in
            guard let weakSelf = self else { return }
        weakSelf.gethtmlDelegate?.htmlContentReceivedSuccessFully(htmlString: json[ApiKey.data][ApiKey.description].stringValue)
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.gethtmlDelegate?.failedToreceiveHtmlContent(message: error.localizedDescription)
            
        }
    }
    
    
}
