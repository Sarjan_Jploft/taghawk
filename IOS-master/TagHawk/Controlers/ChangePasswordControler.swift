//
//  ChangePasswordControler.swift
//  TagHawk
//
//  Created by Appinventiv on 06/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ChangePasswordControllerDelegate: class {
    
    func willChangePassword()
    func changePasswordSuccess()
    func failedToChangePassword(message: String)
    func invalidInput(textFieldType : ChangePasswordCell.PasswordFieldType, textFieldState : TextFieldState)
}

class ChangePasswordControler  {

    weak var delegate : ChangePasswordControllerDelegate?
    
    //MARK:- Validate change password
    func validateChangePassword(userInput : ChangePasswordInput) -> Bool {
        
        if userInput.oldPass.isEmpty {
            self.delegate?.invalidInput(textFieldType: ChangePasswordCell.PasswordFieldType.oldPassword, textFieldState: TextFieldState.invalid(LocalizedString.enterOldPassword.localized.localized))
            return false
        }
        
        if userInput.oldPass.count < 6{
            self.delegate?.invalidInput(textFieldType: ChangePasswordCell.PasswordFieldType.oldPassword, textFieldState: TextFieldState.invalid(LocalizedString.oldPasswordInvalidLength.localized.localized))
            return false
        }
        
        if userInput.newPass.isEmpty {
            self.delegate?.invalidInput(textFieldType: ChangePasswordCell.PasswordFieldType.newPassword, textFieldState: TextFieldState.invalid(LocalizedString.enterNewPassword.localized.localized))
            return false
        }
        
        if userInput.newPass.count < 6{
            self.delegate?.invalidInput(textFieldType: ChangePasswordCell.PasswordFieldType.newPassword, textFieldState: TextFieldState.invalid(LocalizedString.newPasswordInvalidLength.localized.localized))
            return false
        }
        
        if userInput.newPass == userInput.oldPass {
            self.delegate?.invalidInput(textFieldType: ChangePasswordCell.PasswordFieldType.newPassword, textFieldState: TextFieldState.invalid(LocalizedString.NewPasswordSameOld.localized.localized))
            return false
        }
        
        if userInput.confirmPass.isEmpty {
            self.delegate?.invalidInput(textFieldType: ChangePasswordCell.PasswordFieldType.confirmPassword, textFieldState: TextFieldState.invalid(LocalizedString.enterConfirmPassword.localized.localized))
            return false
        }
        
        if userInput.confirmPass != userInput.newPass{
            self.delegate?.invalidInput(textFieldType: ChangePasswordCell.PasswordFieldType.confirmPassword, textFieldState: TextFieldState.invalid(LocalizedString.Confirm_Password_Same_New_Password.localized.localized))
            return false
        }
        
        return true
    }
    
    //mARK:- Change password service
    func changePassword(userInput : ChangePasswordInput){
        if  !self.validateChangePassword(userInput: userInput){
            return
        }
        
        self.delegate?.willChangePassword()
        
        let params : JSONDictionary = [ApiKey.oldPassword : userInput.oldPass, ApiKey.password : userInput.newPass]
        
        WebServices.changePassword(parameters: params, success: {[weak self] (json) in
            
            self?.delegate?.changePasswordSuccess()
            
        }) {[weak self] (error) -> (Void) in
            
            self?.delegate?.failedToChangePassword(message: error.localizedDescription)
            
        }
        
    }
    
}
