//
//  JoinTagController.swift
//  TagHawk
//
//  Created by Vikash on 07/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol JoinTagDelegate : class{
    func willJoinTag()
    func joinTagListServiceReturn(msg: String, tagType : PrivateTagType)
    func joinTagListFailure(errorType: ApiState, message : String)
}

protocol CancelPendingRequestDelegate : class{
    func willCancelPendingRequest()
    func cancelPendingRequestSuccessfully()
    func failedToCancelPendingRequest(msg : String)
}

class JoinTagController  {
    
    weak var delegate : JoinTagDelegate?
    weak var cancelDelegate : CancelPendingRequestDelegate?

    //MARK:- Add tag service
    func addTagService(addTag: Tag, emailPassword: String, joinTagBy: Int, documents: [String], jumioRefrenceId : String = "") {
        
        var dict = [String: Any]()
        dict["communityId"] = addTag.id
        dict["joinTagBy"] = joinTagBy
        if joinTagBy == PrivateTagType.documents.rawValue {
            dict["documentUrl"] = documents.joined(separator: ",")
//            dict["jumioIdScanReference"] = jumioRefrenceId
        } else {
            dict["requestParameter"] = emailPassword
        }
        
        if addTag.entrance == .publicTag {
            dict.removeAll()
            dict["communityId"] = addTag.id
        }
        
        self.delegate?.willJoinTag()
        
        WebServices.joinTagDetails(parameters: dict, success: { [weak self] (data) in
            
            self?.delegate?.joinTagListServiceReturn(msg: data[ApiKey.message].stringValue, tagType: PrivateTagType(rawValue: joinTagBy) ?? PrivateTagType.none)
        }) { (error) -> (Void) in
            if error.code == NSURLErrorNotConnectedToInternet {
                self.delegate?.joinTagListFailure(errorType: .noInternet, message: LocalizedString.pleaseCheckInternetConnection.localized)
            } else {
                self.delegate?.joinTagListFailure(errorType: .failed, message: error.localizedDescription)
            }
        }
    }
    
    //MARK:- Upload data
    func uploadData(imgData: [Data?]){
        
        let name = "\(Int(Date().timeIntervalSince1970)).jpg"
        var params: [UploadFileParameter] = []
        
        imgData.forEach { (optionalData) in
            if let unwrappedData = optionalData {
                let data = UploadFileParameter(fileName: name, key: "file", data: unwrappedData, mimeType: "image/jpeg")
                params.append(data)
            }
        }
        
        WebServices.uploadIdentity(parameters: params, success: { (json) in
            
        }) { (error) -> (Void) in
           self.delegate?.joinTagListFailure(errorType: .failed, message: error.localizedDescription)
        }
    }
    
    
    //MARK::- Cancel tag request
    func cancelTagRequest(tagID: String, userId : String){
        self.cancelDelegate?.willCancelPendingRequest()
        let params: JSONDictionary = [ApiKey.communityId : tagID, ApiKey.userId : userId , ApiKey.status : 3]
        
        WebServices.acceptRejectRequest(parameters: params, success: {[weak self](json) in
            guard let weakSelf = self else { return }
            weakSelf.cancelDelegate?.cancelPendingRequestSuccessfully()
        }){[weak self](error) -> (Void) in
            //            self?.delegate?.error(message: error.localizedDescription)
            guard let weakSelf = self else { return }
            weakSelf.cancelDelegate?.failedToCancelPendingRequest(msg: error.localizedDescription)
        }
    }
    
}
