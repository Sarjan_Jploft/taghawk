//
//  CashOutRequiredDataControler.swift
//  TagHawk
//
//  Created by Admin on 6/14/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit


protocol  UpdateCashoutDataDelegate : class {
    func willUpdateCashOutData()
    func cashOutDataUpdatedSuccessFully(updateFor : UpdatedProfile, valueToUpdate : String, countryCode : String)
    func failedToUpdateCashOutData(msg : String)
}

protocol UploadIdentitiDelegate : class {
    func willUploadIdentiry()
    func uploadIdedtitySuccessFull()
    func failedToUploadIdentity(msg : String)
}

class CashOutRequiredDataControler {

    weak var delegate : UpdateCashoutDataDelegate?
    weak var uploadIdentityDelegate : UploadIdentitiDelegate?
    
    func updateCashoutDetails(updateFor : UpdatedProfile, valueToUpdate : String, countryCode : String = ""){
        
        var params : JSONDictionary = [ApiKey.status :  updateFor.rawValue > 4 ? 0 : updateFor.rawValue]
        
        switch updateFor {
            
        case .dob:
            params[ApiKey.dob] = valueToUpdate
            
        case .ssn:
            params[ApiKey.ssnNumber] = valueToUpdate
            
        case .phone:
            params[ApiKey.countryCode] = countryCode
            params[ApiKey.phoneNumber] = valueToUpdate
            
        default:
            break
            
        }
        
        delegate?.willUpdateCashOutData()
        
        WebServices.updateProfile(parameters: params, success: {[weak self] (json) in
            
            guard let weakSelf = self else { return }
            weakSelf.delegate?.cashOutDataUpdatedSuccessFully(updateFor: updateFor, valueToUpdate: valueToUpdate, countryCode: countryCode)
            
        }) {[weak self] (error) -> (Void) in
        
            guard let weakSelf = self else { return }
            weakSelf.delegate?.failedToUpdateCashOutData(msg: error.localizedDescription)
       
        }
    }
    
    func uploadData(imgData: [UIImage]){
        
        var params: [UploadFileParameter] = []
        
        self.uploadIdentityDelegate?.willUploadIdentiry()
        
        for (index,item) in imgData.enumerated(){
            let key = index == 0 ? "front" : "back"
            let name = "\(Int(Date().timeIntervalSince1970)).jpeg"
            if let unwrappedData = item.jpegData(compressionQuality: 0.8){
                params.append(UploadFileParameter(fileName: name, key: key, data: unwrappedData, mimeType: "image/jpeg"))
            }
        }
    
        WebServices.uploadIdentity(parameters: params, success: {[weak self] (json) in
            
            printDebug(json)
            guard let weakSelf = self else { return }
            UserProfile.main.passport = true
            UserProfile.main.saveToUserDefaults()
            weakSelf.uploadIdentityDelegate?.uploadIdedtitySuccessFull()
            
        }) {[weak self] (error) -> (Void) in
            guard let weakSelf = self else { return }
            weakSelf.uploadIdentityDelegate?.failedToUploadIdentity(msg: error.localizedDescription)
            
        }
    }
}
