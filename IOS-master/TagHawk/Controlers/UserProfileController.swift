//
//  UserProfileController.swift
//  TagHawk
//
//  Created by Vikash on 08/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

import SwiftyJSON

protocol UserProfileDelegate : class{
    func willGetUserProfile()
    func userProfileServiceReturn(userData: UserProfile)
    func userProfileServiceFailure(errorType: ApiState)
}

protocol UserProductDelegate: AnyObject {
    func willGetProducts()
    func userProductServiceReturn(userProduct: [Product], nextPage: Int)
    func userProductsFailed(errorType: ApiState)
    func productDeleteSuccess()
    func productDeleteFailed()
}

class UserProfileController : NSObject {
    
    weak var delegate : UserProfileDelegate?
    weak var productDelegate: UserProductDelegate?
    
    //MARK:- User profile service
    func getUserProfileService(userId: String = "") {
        
        delegate?.willGetUserProfile()
        
        var params: JSONDictionary = [:]
        
        if !userId.isEmpty {
            params[ApiKey.userId] = userId
        }
     
        WebServices.getUserProfile(parameters: params, success: { (result) in
             let profile = UserProfile(json: result)
             self.delegate?.userProfileServiceReturn(userData: profile)
        }) { (error) -> (Void) in
            if error.code == NSURLErrorNotConnectedToInternet {
                self.delegate?.userProfileServiceFailure(errorType: .noInternet)
            } else {
                self.delegate?.userProfileServiceFailure(errorType: .failed)
            }
        }
    }
    
    //MARK:- Get user product
    func getUserProduct(userId: String = "", productStatus: Int, page: Int) {
        
        productDelegate?.willGetProducts()
        
        var dict = [String: Any]()
        dict[ApiKey.productStatus] = productStatus
        dict[ApiKey.pageNo] = page
        dict[ApiKey.limit] = 10
        
        if !userId.isEmpty {
            dict[ApiKey.userId] = userId
        }
        
        WebServices.getUserProducts(parameters: dict, success: { [weak self] (json) in
            var productArr = [Product]()
            let data = json[ApiKey.data]
            data.arrayValue.forEach({ (json) in
                productArr.append(Product(json: json))
            })
            self?.productDelegate?.userProductServiceReturn(userProduct: productArr, nextPage: json[ApiKey.next_hit].intValue)
        }) { (error) -> (Void) in
            if error.code == NSURLErrorNotConnectedToInternet {
               self.productDelegate?.userProductsFailed(errorType: .noInternet)
            } else {
               self.productDelegate?.userProductsFailed(errorType: .failed)
            }
        }
    }
    
    //MARK:- Delete product
    func deleteProduct(_ productId: String) {
        let params: JSONDictionary = [ApiKey.productId: productId]
        WebServices.deleteProduct(parameters: params, success: { (json) in
            
            print(json)
            self.productDelegate?.productDeleteSuccess()
            
        }) { (err) -> (Void) in
            self.productDelegate?.productDeleteFailed()
        }
    }
    
}
