//
//  UIButton+Extension.swift
//  TagHawk
//
//  Created by Appinventiv on 18/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    
    func setAttributes(title : String, font : UIFont, titleColor : UIColor, backgroundColor : UIColor = UIColor.clear){
        self.setTitle(title)
        self.backgroundColor = backgroundColor
        self.setTitleColor(titleColor)
        self.titleLabel?.font = font
    }
    
    func setTitleColor(_ titleColor : UIColor){
        self.setTitleColor(titleColor, for: UIControl.State.normal)
    }
    
    func setTitle(_ title : String){
        self.setTitle(title, for: UIControl.State.normal)
    }
}
