//
//  Double+Extension.swift
//  TagHawk
//
//  Created by Admin on 6/3/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

extension Double {
    
    func removeTrailingZero() -> String {
        return String(format: "%g", self)
    }
    
    
    func formatPoints() ->String{
        let thousandNum = self/1000
        let millionNum = self/1000000
        if self >= 1000 && self < 1000000{
            if(floor(thousandNum) == thousandNum){
                return("\(Int(thousandNum))k")
            }
            
            return("\(thousandNum.rounded(toPlaces: 2))k")
        }
        if self > 1000000{
            if(floor(millionNum) == millionNum){
                return("\(Int(thousandNum))k")
            }
            return ("\(millionNum.rounded(toPlaces: 2))M")
        }
        else{
            if(floor(self) == self){
                return ("\(Int(self))")
            }
            return ("\(self)")
        }
        
    }
    
}
