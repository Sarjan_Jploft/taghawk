//
//  NavigationBar.swift
//  Geora
//
//  Created by Appinventiv on 03/01/19.
//  Copyright © 2019 Appinventiv. All rights reserved.
//

import Foundation
import UIKit


extension UINavigationBar {
    
    func imageControllerNavColor(){
        
        self.titleTextAttributes = [NSAttributedString.Key.foregroundColor: AppColors.whiteColor,
                                    NSAttributedString.Key.font: AppFonts.Galano_Medium.withSize(17)]
        self.isTranslucent = false
        self.tintColor = AppColors.whiteColor
        self.barTintColor = AppColors.appBlueColor
    }
}
