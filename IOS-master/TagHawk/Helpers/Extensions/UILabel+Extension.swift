//
//  UILabel+Extension.swift
//  TagHawk
//
//  Created by Appinventiv on 18/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func setAttributes(text : String = "", font : UIFont, textColor : UIColor, backgroundColor : UIColor = UIColor.clear){
        self.backgroundColor = backgroundColor
        self.text = text
        self.textColor = textColor
        self.font = font
    }
    
    func setAttributes(attributedText : NSAttributedString, font : UIFont, textColor : UIColor, backgroundColor : UIColor = UIColor.clear){
        self.backgroundColor = backgroundColor
        self.attributedText = attributedText
        self.textColor = textColor
        self.font = font
    }
    
    
}
