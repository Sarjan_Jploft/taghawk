//
//  UITextViewExtension.swift
//  JobCaster
//
//  Created by Appinventiv on 25/08/17.
//  Copyright © 2017 AppInventiv. All rights reserved.
//

import UIKit

// MARK: - Methods
public extension UITextView {
    
    /// SwifterSwift: Scroll to the bottom of text view
    public func scrollToBottom() {
        let range = NSMakeRange((text as NSString).length - 1, 1)
        scrollRangeToVisible(range)
    }
    
    /// SwifterSwift: Scroll to the top of text view
    public func scrollToTop() {
        let range = NSMakeRange(0, 1)
        scrollRangeToVisible(range)
    }
    
    /// Check Empty Text View
    public func validate() -> Bool {
        
        guard let text =  self.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
                return false
        }
        return true
    }
}


//TO Disable Paste/Cut on a TextView
var key: Void?

class UITextViewAdditions: NSObject {
    var copyCutPasteDisabled: Bool = false
}

extension UITextView {
    
    var copyCutPasteDisabled: Bool {
        get {
            return self.getAdditions().copyCutPasteDisabled
        } set {
            self.getAdditions().copyCutPasteDisabled = newValue
        }
    }
    
    private func getAdditions() -> UITextViewAdditions {
        var additions = objc_getAssociatedObject(self, &key) as? UITextViewAdditions
        if additions == nil {
            additions = UITextViewAdditions()
            objc_setAssociatedObject(self, &key, additions!, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
        return additions!
    }
    
    open override func target(forAction action: Selector, withSender sender: Any?) -> Any? {
        let condition1 = (action == #selector(UIResponderStandardEditActions.paste(_:)) && self.copyCutPasteDisabled)
//        let condition2 = (action == #selector(UIResponderStandardEditActions.cut(_:)) && self.copyCutPasteDisabled)
//        let condition3 = (action == #selector(UIResponderStandardEditActions.copy(_:)) && self.copyCutPasteDisabled)
//        let condition4 = (action == #selector(UIResponderStandardEditActions.select(_:)) && self.copyCutPasteDisabled)
//        let condition5 = (action == #selector(UIResponderStandardEditActions.selectAll(_:)) && self.copyCutPasteDisabled)
        
//        if condition1 || condition2 || condition3 || condition4 || condition5 {
//            return nil
//        }
        return super.target(forAction: action, withSender: sender)
    }
}


// MARK: Rishabh
extension UITextView {
    
    func centerVertically() {
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(1, topOffset)
        contentOffset.y = -positiveTopOffset
    }
    
}
