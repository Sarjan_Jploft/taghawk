//
//  StringExtention.swift
//  WoshApp
//
//  Created by Saurabh Shukla on 19/09/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//


import Foundation
import UIKit

extension String {
    
    ///Removes all spaces from the string
    var removeSpaces:String{
        return self.replacingOccurrences(of: " ", with: "")
    }

    ///Removes all HTML Tags from the string
    var removeHTMLTags: String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }

    ///Returns a localized string
    var localized:String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    ///Removes leading and trailing white spaces from the string
    var byRemovingLeadingTrailingWhiteSpaces:String {
        
        let spaceSet = CharacterSet.whitespaces
        return self.trimmingCharacters(in: spaceSet)
    }

 var containsEmoji: Bool {
        
        return !unicodeScalars.filter { $0.isEmoji }.isEmpty
    }
    
    var isBackSpace : Bool {
        let char = self.cString(using: String.Encoding.utf8)!
        return strcmp(char, "\\b") == -92
    }
    
    public func trimTrailingWhitespace() -> String {
        if let trailingWs = self.range(of: "\\s+$", options: .regularExpression) {
            return self.replacingCharacters(in: trailingWs, with: "")
        } else {
            return self
        }
    }
    ///Returns 'true' if the string is any (file, directory or remote etc) url otherwise returns 'false'
    var isAnyUrl:Bool{
        return (URL(string:self) != nil)
    }
    
    ///Returns the json object if the string can be converted into it, otherwise returns 'nil'
    var jsonObject:Any? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                debugPrint(error.localizedDescription)
            }
        }
        return nil
    }
    
    ///Returns the base64Encoded string
    var base64Encoded:String {
        
        return Data(self.utf8).base64EncodedString()
    }
    
    ///Returns the string decoded from base64Encoded string
    var base64Decoded:String? {
        
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }

    func heightOfText(_ width: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func widthOfText(_ height: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.width
    }

     func attributeStringWithAstric() -> NSAttributedString{
        
        let attributedString = NSMutableAttributedString(string:self)
        
        let range1 = (self as NSString).range(of: self)
        // attributedString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: range1)
       attributedString.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: range1)
        
        //if main_string.contains("(should be 18 years or above from curent date)"){
        let range2 = (self as NSString).range(of: "*")
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.red, NSAttributedString.Key.font : AppFonts.Galano_Regular.withSize(15)], range: range2)
        return attributedString
    }
    
    
    
    func attributeStringWithAnotherColor(stringToColor : String) -> NSAttributedString{
        
        let attributedString = NSMutableAttributedString(string:self)
        
        let range1 = (self as NSString).range(of: self)
        // attributedString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: range1)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : AppColors.lightGreyTextColor], range: range1)
        
        //if main_string.contains("(should be 18 years or above from curent date)"){
        let range2 = (self as NSString).range(of: stringToColor)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : AppColors.appBlueColor], range: range2)
        
        return attributedString
    }
    
    func attributeBoldStringWithAnotherColor(stringsToColor : [String],
                                             size: CGFloat = 13,
                                             strClr: UIColor = AppColors.blackColor,
                                             substrClr: UIColor = AppColors.blackColor) -> NSAttributedString{
        
        let attributedString = NSMutableAttributedString(string:self)
        
        let range1 = (self as NSString).range(of: self)
        // attributedString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: range1)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : strClr, .font: AppFonts.Galano_Regular.withSize(size)], range: range1)
        
        //if main_string.contains("(should be 18 years or above from curent date)"){
        stringsToColor.forEach { (stringToColor) in
            let range2 = (self as NSString).range(of: stringToColor)
            attributedString.addAttributes([NSAttributedString.Key.foregroundColor : substrClr, .font: AppFonts.Galano_Semi_Bold.withSize(size)], range: range2)
        }
        
        return attributedString
    }
    
    func attributeBoldStringWithUnderline(stringsToUnderLine : [String], size: CGFloat = 13, strColor: UIColor = AppColors.appBlueColor, font : UIFont) -> NSAttributedString{
        
        let attributedString = NSMutableAttributedString(string:self)
        
        let range1 = (self as NSString).range(of: self)
        
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : strColor, .font: font], range: range1)
        
        stringsToUnderLine.forEach { (str) in
            let range2 = (self as NSString).range(of: str)
            attributedString.addAttributes([NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.underlineColor : strColor , .font: font], range: range2)
        }
        
        return attributedString
    }
    
    
    func attributeStringWithColors(stringToColor : String,
                                   strClr: UIColor,
                                   substrClr: UIColor,
                                   strFont: UIFont = AppFonts.Galano_Regular.withSize(12),
                                   strClrFont: UIFont = AppFonts.Galano_Semi_Bold.withSize(12)) -> NSAttributedString{
        
        let attributedString = NSMutableAttributedString(string:self)
        
        let range1 = (self as NSString).range(of: self)
        // attributedString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: range1)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : strClr, NSAttributedString.Key.font: strFont], range: range1)
        
        //if main_string.contains("(should be 18 years or above from curent date)"){
        let range2 = (self as NSString).range(of: stringToColor)
        
        
        attributedString.addAttributes([NSAttributedString.Key.font: strClrFont ,NSAttributedString.Key.foregroundColor : substrClr], range: range2)
        
        return attributedString
    }
    
    
    func attributedMessageWithHighlitedUserName(stringTobeAttributed : String) -> NSAttributedString{
        
        let attributedString = NSMutableAttributedString(string:self)
        
        let range1 = (self as NSString).range(of: self)
        // attributedString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: range1)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black, NSAttributedString.Key.font: AppFonts.Galano_Semi_Bold.withSize(15)], range: range1)
        
        //if main_string.contains("(should be 18 years or above from curent date)"){
        let range2 = (self as NSString).range(of: stringTobeAttributed)
        attributedString.addAttributes([NSAttributedString.Key.font: AppFonts.Galano_Bold.withSize(15) ,NSAttributedString.Key.foregroundColor : UIColor.black], range: range2)
        
        return attributedString
    }
    

    
    func localizedString(lang:String) ->String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        printDebug(path)
        let bundle = Bundle(path: path!)
        printDebug(bundle)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    ///Returns 'true' if string contains the substring, otherwise returns 'false'
    func contains(s: String) -> Bool
    {
        return self.range(of: s) != nil ? true : false
    }
    
    ///Replaces occurances of a string with the given another string
    func replace(string: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: string, with: withString, options: String.CompareOptions.literal, range: nil)
    }
    
    ///Converts the string into 'Date' if possible, based on the given date format and timezone. otherwise returns nil
    func toDate(dateFormat:String,timeZone:TimeZone = TimeZone.current)->Date?{
        
        let frmtr = DateFormatter()
//        frmtr.locale = Locale(identifier: "en_US_POSIX")
        frmtr.dateFormat = dateFormat
//        frmtr.timeZone = timeZone
        return frmtr.date(from: self)
    }

    func toDateText(inputDateFormat: String, outputDateFormat: String, timeZone: TimeZone = TimeZone.current) -> String{
        
        if let date = self.toDate(dateFormat: inputDateFormat, timeZone: timeZone) {
            return date.toString(dateFormat: outputDateFormat, timeZone: timeZone)
        } else {
            return ""
        }
    }

    func checkIfValid(_ validityExression : ValidityExression) -> Bool {
        
        let regEx = validityExression.rawValue
        
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)

        return test.evaluate(with: self)
        
//        return self.matches(pattern: regEx)
    }
    
    func checkIfInvalid(_ validityExression : ValidityExression) -> Bool {
        
        return !self.checkIfValid(validityExression)
    }
    
    func matches(pattern: String) -> Bool {
        return range(of: pattern,
                     options: String.CompareOptions.regularExpression,
                     range: nil, locale: nil) != nil
    }

    func isAllowed(validationStr : AllowedCharactersValidation) -> Bool {
        let characterset = CharacterSet(charactersIn: validationStr.rawValue)
        if self.rangeOfCharacter(from: characterset.inverted) != nil {
            return false
        }else{
            return true
        }
    }

    ///Capitalize the very first letter of the sentence.
    var capitalizedFirst: String {
        guard !isEmpty else { return self }
        var result = self
        let substr1 = String(self[startIndex]).uppercased()
        result.replaceSubrange(...startIndex, with: substr1)
        return result
    }
    
    func stringByAppendingPathComponent(path: String) -> String {
        
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
    
    func addLine(spacing : CGFloat) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string : self)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = spacing
    attributedString.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: NSMakeRange(0, attributedString.length))
        return attributedString
    }
    
    func capitalizingFirstLetter() -> String {
        let arrSubs = components(separatedBy: " ")
        var newStr = ""
        for (index, val) in arrSubs.enumerated() {
            newStr += index == (arrSubs.count - 1) ? val.capitalizedFirst : (val + " ").capitalizedFirst
        }
        return newStr
    }
    
}

extension String {
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? self.removeHTMLTags
    }
}

enum ValidityExression : String {
    
    case userName = "^[a-zA-z]{1,}+[a-zA-z0-9!@#$%&*]{2,15}"
    case emailWithTwoDomains = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]+\\.[A-Za-z]{2,}"
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,}"
    case mobileNumber = "^[0-9]{6,20}$"
    case password = "^[a-zA-Z0-9!@#$%&*]{6,}"
//    case name = "^[a-zA-Z]"
    
//    case name = "^$|^[a-zA-Z]+$"
    
    case name = "[A-Za-z]+[[\\s][A-Za-z]+]*"
    
    case webUrl = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
}

enum AllowedCharactersValidation : String {
    
    case name = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ \\b"
    case productTitle = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ \\b"
    case description = "!@#$%^&*()_-+=:;'<,>.?/{}[]|0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ \\b\n"
    case cardNumber = "0123456789"
    case expirey = "0123456789/"
    case amount = "0123456789."
}



//MARK:- String Extensions
//MARK:-
extension UnicodeScalar {
    
    var isEmoji: Bool {
        
        switch value {
        case 0x3030, 0x00AE, 0x00A9, // Special Characters
        0x1D000 ... 0x1F77F, // Emoticons
        0x2100 ... 0x27BF, // Misc symbols and Dingbats
        0xFE00 ... 0xFE0F, // Variation Selectors
        0x1F900 ... 0x1F9FF: // Supplemental Symbols and Pictographs
            return true
            
        default: return false
        }
    }
    
    var isZeroWidthJoiner: Bool {
        
        return value == 8205
    }
}


extension NSAttributedString{
    func concatinate(attributedString : NSAttributedString) -> NSMutableAttributedString {
        let combination = NSMutableAttributedString()
        combination.append(self)
        combination.append(attributedString)
        return combination
    }
}
