//
//  Globals.swift
//  Onboarding
//
//  Created by Appinventiv on 22/08/16.
//  Copyright © 2016 Gurdeep Singh. All rights reserved.
//

import Foundation
import UIKit



let sharedAppDelegate = UIApplication.shared.delegate as! AppDelegate
let screenSize  = UIScreen.main.bounds.size
let screenWidth  = UIScreen.main.bounds.size.width
let screenHeight = UIScreen.main.bounds.size.height
let statusBarHeight = UIApplication.shared.statusBarFrame.height


/// Print Debug
func printDebug<T>(_ obj : T) {
    #if DEBUG
        print(obj)
    #endif
}

/// Is Simulator or Device
var isSimulatorDevice: Bool {

    var isSimulator = false
    #if arch(i386) || arch(x86_64)
        //simulator
        isSimulator = true
    #endif
    return isSimulator
}

/// Is this iPhone X or not
func isDeviceIsIphoneX() -> Bool {
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 1136: return false
        case 1334: return false
        case 2208: return false
        case 2436: return true
        default: return false
        }
    }
    return false
}

enum StripeInfo : String{
//    case apiKey = "pk_test_JdfLDVECnEPfFxJTJR1zRrMn00Ry7gkZVX"
    
//    stripe key
//    const STRIP_KEY_P = "pk_test_FOXoFBKrNB90C9zlCQU1aAQF";
//    const STRIP_KEY_S = "sk_test_S5EI4E0a9ml0IV4dxbeBaJZ4";
    
//    case apiKey = "pk_test_FOXoFBKrNB90C9zlCQU1aAQF"
  
    case apiKey = "pk_live_Kax3WilXTomLpwkQB6mYwe26"
    case merchantId = "merchant.com.taghawk"
    
}
