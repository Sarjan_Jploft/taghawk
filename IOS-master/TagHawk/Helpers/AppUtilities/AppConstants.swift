//
//  AppConstants.swift
//  NewProject
//
//  Created by Bhavneet Singh on 10/08/18.
//  Copyright © 2018 Bhavneet Singh. All rights reserved.
//

import Foundation
enum AppConstants: String {

    case cancel = "Cancel"
    case okButtonTitle = "OK"
    case photoLibrary = "Choose from Gallery"
    case camera = "Take Photo"
    case fireBaseServerKey = "key=AAAAZpnw4Bk:APA91bHfgm6-TFqNdKmZ6agBci5hbCMLmg-oj6m7EVCVWsP3FIcxK5imcquFuP3EDsb3F0ubOcyymQEYYsL0PFLSZfq4m8WzpcB5HQH5HH-6E7vgTyJU8z7q0QDHjt7cR3FgoQ85r_20"
}

enum AppConstantKeys : String {
    case fbId = "fb446906379222664"
    case BranchKey = "key_live_edQPu4fKVhnzIFKwGW7Piklcuzn80Hi9"
}

let appVersion : Int = 1

