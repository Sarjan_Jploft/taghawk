//
//  AppSharedData.swift
//  TagHawk
//
//  Created by Appinventiv on 22/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class AppSharedData {


    static let shared = AppSharedData()

    var categories: [Category] = [] {
        didSet {
            let dicArr: JSONDictionaryArray = self.categories.map({ $0.getDictionary })
            AppUserDefaults.save(value: dicArr, forKey: .categoriesData)
        }
    }
    
    
     init(){
        
    }
    
}
