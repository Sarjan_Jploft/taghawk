//
//  AppFonts.swift
//  DannApp
//
//  Created by Aakash Srivastav on 20/04/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit


enum AppFonts : String {
    
    case Galano_Light = "GalanoGrotesque-Light"
    case Galano_Regular = "GalanoGrotesque-Regular"
    case Galano_Bold = "GalanoGrotesque-Bold"
    case Galano_Black = "GalanoGrotesque-Black"
    case Galano_Extra_Light = "GalanoGrotesque-ExtraLight"
    case Galano_Medium = "GalanoGrotesque-Medium"
    case Galano_Heavy = "GalanoGrotesque-Heavy"
    case Galano_Semi_Bold = "GalanoGrotesque-SemiBold"
    case Galano_Extra_Bold = "GalanoGrotesque-ExtraBold"

    case SFUI_Text_Bold         = "SF-UI-Text-Bold_0"
    case SourceSansPro_Semibold = "SourceSansPro-Semibold"
}

extension AppFonts {
    
    func withSize(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: self.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    func withDefaultSize() -> UIFont {
        return UIFont(name: self.rawValue, size: 12.0) ?? UIFont.systemFont(ofSize: 12.0)
    }
    
}


extension UIFont {
    var bold: UIFont {
        return with(traits: .traitBold)
    } // bold
    
    var italic: UIFont {
        return with(traits: .traitItalic)
    } // italic
    
    var boldItalic: UIFont {
        return with(traits: [.traitBold, .traitItalic])
    } // boldItalic
    
    
    func with(traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
        guard let descriptor = self.fontDescriptor.withSymbolicTraits(traits) else {
            return self
        } // guard
        
        return UIFont(descriptor: descriptor, size: 0)
    } // with(traits:)
}
