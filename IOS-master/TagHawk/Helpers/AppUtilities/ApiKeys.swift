//
//  UserInputKeys.swift
//  DittoFashionMarketBeta
//
//  Created by Appinventiv Technologies on 05/02/18.
//  Copyright © 2018 Bhavneet Singh. All rights reserved.
//

import Foundation

//MARK:- Api Keys
//=======================
enum ApiKey {
    
    static var fullName: String { return "fullName" }
    static var email: String { return "email" }
    static var password: String { return "password" }
    static var deviceId: String { return "deviceId" }
    static var userType: String { return "userType" }
    static var deviceToken: String { return "deviceToken" }
    static var username: String { return "username" }
    static var apiKey: String { return "api_key" }

    static var invitationCode : String { return "invitationCode" }
    static var refreshToken : String { return "refreshToken" }
    static var accessToken : String { return "accessToken" }
    static var authorization : String { return "Authorization" }
    static var data : String { return "data" }

    static var _id : String { return "_id" }
    static var id : String { return "id" }

    static var imageUrl : String { return "imageUrl" }
    static var created : String { return "created" }
    static var description : String { return "description" }

    static var socialLoginType : String { return "socialLoginType" }
    static var socialId : String { return "socialId" }
    static var profilePicture : String { return "profilePicture" }
    static var pageNo : String { return "pageNo" }
    static var limit : String { return "limit" }
    static var location : String { return "location" }
    static var type : String { return "type2" }
    static var entrance : String { return "type" }
    static var coordinates : String { return "coordinates" }
    static var firmPrice : String { return "firmPrice" }
    static var sellerVerified : String { return "sellerVerified" }
    static var sellerRating : String { return "sellerRating" }
    static var title : String { return "title" }
    static var images : String { return "images" }
    static var thumbUrl : String { return "thumbUrl" }
    static var url : String { return "url" }
    static var count : String { return "count" }
    static var searchKey : String { return "searchKey" }
    static var productLongitude : String { return "productLongitude" }
    static var productLatitude : String { return "productLatitude" }
    static var userLongitude : String { return "userLongitude" }
    static var userLatitude : String { return "userLatitude" }
    static var productAddress : String { return "productAddress" }
    static var rating : String { return "rating" }
    static var ratingData : String { return "ratingData" }
    static var condition : String { return "condition" }
    static var shippingAvailibility : String { return "shippingAvailibility" }
    static var similarProducts : String { return "similarProducts" }
    static var followings : String { return "followings" }
    static var following : String { return "following" }

    static var followers : String { return "followers" }
    static var userCreated : String { return "userCreated" }
    static var isLiked : String { return "isLiked" }
    static var isCreatedByMe : String { return "isCreatedByMe" }
    static var announcement: String { return "announcement" }

    static var isNegotiable : String { return "isNegotiable" }
    static var isTransactionCost: String { return "isTransactionCost" } 
    static var productCategoryId : String { return "productCategoryId" }
    static var userId : String { return "userId" }
    static var next_hit : String { return "next_hit" }
    static var distance : String { return "distance" }
    static var postedWithIn : String { return "postedWithIn" }
    static var adminEmail : String { return "adminEmail" }
    static var adminName : String { return "adminName" }
    static var ownerName : String { return "ownerName" }
    static var tagLatitude : String { return "tagLatitude" }
    static var tagLongitude : String { return "tagLongitude" }
    static var tagAddress : String { return "tagAddress" }
    static var tagImageUrl : String { return "tagImageUrl" }
    static var members : String { return "members" }
    static var isMember : String { return "isMember" }
    static var tagType : String { return "tagType" }
    static var link : String { return "link" }
    static var communityId : String { return "communityId" }
    static var productInfo : String { return "productInfo" }
    static var sharedCommunities : String { return "sharedCommunities" }
    static var memberSize : String { return "memberSize" }
    static var paymentId : String { return "paymentId" }
    static var days : String { return "days" }
    static var price : String { return "price" }
    static var joinTagBy : String { return "joinTagBy" }
    static var productStatus : String { return "productStatus" }
    static var isPromoted: String { return "isPromoted" }
    static var accountHolderType : String { return "account_holder_type" }
     static var accountDetails : String { return "accountDetails" }
    static var bankDetail : String { return "bankDetail" }

    
    //.............
    
    
    static var status: String { return "status" }
    static var code: String { return "statusCode" }
    static var result: String { return "RESULT" }
    static var message: String { return "message" }
    static var Authorization: String { return "Authorization" }
    static var user_id: String { return "user_id" }
    static var name: String { return "name" }
    static var first_name: String { return "first_name" }
    static var last_name: String { return "last_name" }
    static var firstName: String { return "firstName" }
    static var lastName: String { return "lastName" }
    static var confirmPassword: String { return "confirmPassword" }
    static var gender: String { return "gender" }
    static var phone: String { return "phone" }
    static var dob: String { return "dob" }
    static var address: String { return "address" }
    static var user_lat: String { return "user_lat" }
    static var user_long: String { return "user_long" }
    static var country_id: String { return "country_id" }
    static var state_id: String { return "state_id" }
    static var city_id: String { return "city_id" }
    static var device_id: String { return "device_id" }
    static var platform: String { return "platform" }
    
    static var token: String { return "token" }
    static var resetSuccess: String { return "resetSuccess" }
    static var oldpassword: String { return "oldpassword" }
    static var oldPassword: String { return "oldPassword" }
    static var priceFrom: String { return "priceFrom" }
    static var priceTo: String { return "priceTo" }
    static var lat: String { return "lat" }
    static var long: String { return "long" }
    static var lat1 : String { return "lat1" }
    static var long1 : String { return "long1" }

    static var productId: String { return "productId" }
    static var reason = "reason"
    static var sortBy = "sortBy"
    static var sortOrder = "sortOrder"
    static var formattedAddress = "formattedAddress"
    static var action = "action"
    static var locality = "locality"
    static var subLocality = "subLocality"
    static var country = "country"
    static var administrativeArea = "administrativeArea"


    static var sellerId = "sellerId"

    static var productDescription = "productDescription"
    static var productPrice = "productPrice"
    static var productName = "productName"
    static var totalCount = "totalCount"
    static var sellerName = "sellerName"
    static var productPicUrl = "productPicUrl"

    //MARK:- NotificationsVC
    static var time = "time"
    static var total = "total"
    static var page = "page"
    static var total_page = "total_page"
    static var entityId = "entityId"
    static var readStatus = "readStatus"
    static var receiverId = "receiverId"
    static var createdAt = "createdAt"
    static var updatedAt = "updatedAt"
    static var notificationId = "notificationId"
    static var senderId = "senderId"
    static let requestStatus = "requestStatus"
    static var isFollowing = "isFollowing"
    static var isFollower = "isFollower"
    static var amount = "amount"
    static var currency = "currency"
    static var source = "source"
    static var shipping = "shipping"
    static var otp = "otp"
    
    //MARK:- Ratings
    static var ratingId = "ratingId"
    static var replyComment = "replyComment"
    static var editedStatus = "editedStatus"
    static var replyDate = "replyDate"
    static var editedDate = "editedDate"
    static var comment = "comment"
    static var replied = "replied"

    static var accountNumber = "account_number"
    static var accountHolderName = "account_holder_name"
    static var routingNumber = "routing_number"
    static var ip = "ip"
    static var ssnNumber = "ssnNumber"
    static var stripeMerchantId = "stripeMerchantId"
    static var stripeCustomerId = "stripeCustomerId"
    static var countryCode = "countryCode"
    static var isFacebookLogin = "isFacebookLogin"
    static var isEmailVerified = "isEmailVerified"
    static var isDocumentsVerified = "isDocumentsVerified"
    static var profileCompleted = "profileCompleted"
    static var isPhoneVerified = "isPhoneVerified"
    static var phoneNumber = "phoneNumber"
    static var addressType = "addressType"
    static var govtId = "govtId"
    static var documents = "documents"
    static var shareUrl = "shareUrl"
    static var officialIdVerified = "officialIdVerified"
    static var customerId = "customerId"
    static var brand = "brand"
    static var expYear = "expYear"
    static var expMonth = "expMonth"
    static var last4 = "last4"
    static var customer = "customer"
    static var cardId = "cardId"
    static var cardToken = "cardToken"
    static var cardHolderName = "cardHolderName"
    static var cardNumber = "cardNumber"
    static var cardFingerPrint = "cardFingerPrint"
    static var cardType = "cardType"
    static var isDefault = "isDefault"
    static var isMute = "isMute"
    
    static var streetAddress = "streetAddress"
    static var apartment = "apartment"
    static var city = "city"
    static var state = "state"
    static var stateCode = "stateCode"
    static var zipcode = "zipcode"
    static var shipTo = "ship_to"
    static var contact_name = "contact_name"
    static var street1 = "street1"
    static var postal_code = "postal_code"
    static var residence_type = "residence_type"
    static var total_charge = "total_charge"

    static var weight = "weight"
    static var fedexPrice = "fedexPrice"
    static var uspsPrice = "uspsPrice"
    static var isAvailableInFedex = "isAvailableInFedex"
    static var isAvailableInUsps = "isAvailableInUsps"
    static var shippingPrice = "shippingPrice"
    static var shippingType = "shippingType"
    static var products = "products"

    static var results = "results"
    static var address_components = "address_components"
    static var types = "types"
    static var administrative_area_level_2 = "administrative_area_level_2"
    static var administrative_area_level_1 = "administrative_area_level_1"
    static var long_name = "long_name"
    static var short_name = "short_name"
    static var createdUsing = "createdUsing"
    static let latestReceipt = "latest_receipt"
    static let receipt = "receipt"
    static let in_app = "in_app"
    static let transaction_id = "transaction_id"
    static var loginType = "loginType"
    static var totalRating = "totalRating"
    static var isBlocked = "isBlocked"
    static var tagJoinData = "tagJoinData"
    static var balance = "balance"
    static var documentReferenceId = "documentReferenceId"
    static var isActive = "isActive"
    static var rewardPoint = "rewardPoint"
    static var day = "day"
    static var promotedProductIds = "promotedProductIds"
    static var deliveryDate = "deliveryDate"
    static var disputeId = "deliveryDate"
    static var refundDate = "refundDate"
    static var netPrice = "netPrice"
    static var refundRequestDate = "refundRequestDate"
    static var disputeDate = "disputeDate"
    static var deliveryStatus = "deliveryStatus"
    static var refundId = "refundId"
    static var disputeCompleteDate = "disputeCompleteDate"
    static var purchasedDate = "purchasedDate"
    static var orderId = "orderId"
    static var declineMessage = "declineMessage"
    static var statement = "statement"
    static var proof = "proof"
    static var cashOutBalance = "cashOutBalance"
    static var pendingBalance = "pendingBalance"
    static var object = "object"
    static var externalAccounts = "external_accounts"
    static var payoutsEnabled = "payouts_enabled"
    static var method = "method"
    static var sourceType = "source_type"
    static var destination = "destination"
    static var gcm_message_id = "gcm.message_id"
    static var versionDesc = "versionDesc"
    static var versionName = "versionName"
    static var versionInfo = "versionInfo"
    static var availableBalance = "availableBalance"
    static var availableSoonBalance = "availableSoonBalance"
    static var postalCode = "postal_code"
    static var line1 = "line1"
    static var line2 = "line2"
    static let isAddressVerified = "isAddressVerified"
    static let passport = "passport"
    static let ssnlast4Provided = "ssn_last_4_provided"
    static let verified = "verified"
    static let verification = "verification"
    static let isExist = "isExist"
}


//MARK:- Api Code
//=======================
enum ApiCode {
    
    static var success: Int { return 200 } // Success
    static var unauthorizedRequest: Int { return 206 } // Unauthorized request
    static var headerMissing: Int { return 207 } // Header is missing
    static var phoneNumberAlreadyExist: Int { return 208 } // Phone number alredy exists
    static var requiredParametersMissing: Int { return 418 } // Required Parameter Missing or Invalid
    static var fileUploadFailed: Int { return 421 } // File Upload Failed
    static var pleaseTryAgain: Int { return 500 } // Please try again
    static var tokenExpired: Int { return 401 } // Token expired refresh token needed to be generated
    static var emptyEmail : Int { return 411 }
    static var notFound: Int { return 404 }
    static var produceSold: Int { return 415 }
    static var userDeleted : Int { return 204 }
    
    
}
