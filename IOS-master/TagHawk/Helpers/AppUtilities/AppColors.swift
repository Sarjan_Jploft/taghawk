//
//  AppColors.swift
//  DannApp
//
//  Created by Aakash Srivastav on 20/04/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit

enum AppColors {
    
    static var navigationBarColor: UIColor                  { return #colorLiteral(red: 0.2235294118, green: 0.8117647059, blue: 0.9843137255, alpha: 1) } // rgb 57 207 253
    static var themeColor: UIColor                  { return #colorLiteral(red: 0, green: 0.9215686275, blue: 0.9960784314, alpha: 1) } // rgb 81 148 117
    static var whiteColor: UIColor                  { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) } // rgb 255 255 255
    static var textfield_Border: UIColor            { return #colorLiteral(red: 0.8196078431, green: 0.8745098039, blue: 0.9137254902, alpha: 1) } // rgb 153 153 153
    static var textfield_Text: UIColor              { return #colorLiteral(red: 0.1884121193, green: 0.1884121193, blue: 0.1884121193, alpha: 1) } // rgb 48 48 48
    static var textfield_Title: UIColor             { return #colorLiteral(red: 0.1884121193, green: 0.1884121193, blue: 0.1884121193, alpha: 1) } // rgb 48 48 48
    static var textfield_Error: UIColor             { return #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1) } // rgb 133 28 13
    static var loaderColor: UIColor                 { return #colorLiteral(red: 0.3176470588, green: 0.5803921569, blue: 0.4588235294, alpha: 1) } // rgb 81 148 117
    static var loaderBkgroundColor: UIColor         { return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.145708476) } // rgb 0 0 0 alpha 0.5
    static var titleNavColor: UIColor               { return #colorLiteral(red: 0.3176470588, green: 0.5803921569, blue: 0.4588235294, alpha: 1) } // rgb 81 148 117
    static var selectedSegmentColor: UIColor               { return #colorLiteral(red: 0.168627451, green: 0.8078431373, blue: 0.9921568627, alpha: 1) } // rgb 43 206 253
    static var segmentTextColor: UIColor               { return #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1) } // rgb 63 251 234

    static var notificatioTextClr: UIColor          { return  #colorLiteral(red: 0.137254902, green: 0.137254902, blue: 0.137254902, alpha: 1) }

    static var grayColor : UIColor       { return #colorLiteral(red: 0.7450980392, green: 0.7607843137, blue: 0.8039215686, alpha: 1) } //
    static var recieveBackgroundColor : UIColor       { return #colorLiteral(red: 0.968627451, green: 0.9764705882, blue: 0.9803921569, alpha: 1) } // 247 249 250
    
    static var blackBorder : UIColor       { return #colorLiteral(red: 0.2235294118, green: 0.231372549, blue: 0.2588235294, alpha: 1) } //
    // rgb 57 59 66
    
    //.....................
    
    static var lightGreyTextColor : UIColor   { return #colorLiteral(red: 0.5843137255, green: 0.6156862745, blue: 0.6705882353, alpha: 1) } // 149 157 171
    
    static var blueTextColor : UIColor   { return #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5921568627, alpha: 1) }
    
    static var faceBookBackColor : UIColor   { return #colorLiteral(red: 0.8431372549, green: 0.8549019608, blue: 0.8745098039, alpha: 1) }
    
    static var appBlueColor : UIColor   { return #colorLiteral(red: 0.168627451, green: 0.8078431373, blue: 0.9921568627, alpha: 1) }
    
    static var pickShadow : UIColor   { return #colorLiteral(red: 0.9882352941, green: 0.3333333333, blue: 0.4705882353, alpha: 1) }
    
    static var blueBorder : UIColor   { return #colorLiteral(red: 0.768627451, green: 0.9490196078, blue: 1, alpha: 1) }
    static var blackLblColor : UIColor   { return #colorLiteral(red: 0.137254902, green: 0.137254902, blue: 0.137254902, alpha: 1) } // 35 35 35
    static var grayBorderColor : UIColor   { return #colorLiteral(red: 0.7137254902, green: 0.7450980392, blue: 0.8039215686, alpha: 1) } // 182 190 205
    static var blackColor : UIColor   { return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) } // 0 0 0
    static var deleteBackgroundColor : UIColor   { return #colorLiteral(red: 0.9764705882, green: 0.3921568627, blue: 0.4588235294, alpha: 1) } // 249 100 117
    static var textViewPlaceholderColor : UIColor   { return #colorLiteral(red: 0.7960784314, green: 0.831372549, blue: 0.8901960784, alpha: 1) } // 203 212 227
    static var stripeColor : UIColor   { return #colorLiteral(red: 0.4039215686, green: 0.4470588235, blue: 0.8941176471, alpha: 1) } // 203 212 227

    
    
}
