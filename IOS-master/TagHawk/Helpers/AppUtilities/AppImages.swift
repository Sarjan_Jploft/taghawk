//
//  AppImages.swift
//  TagHawk
//
//  Created by Appinventiv on 21/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import UIKit


enum AppImages {
    
    case customerLogo
    case hawkDriverLogo
    case movingCompanyLogo
    case backWhite
    case itemsInactiv
    case tagsInactiv
    case itemsActiv
    case tagsActiv
    
    case homeSelected
    case homeDeselected
    case hawkDriverSelected
    case hawkDriverdeselected
    case messageSelected
    case messageDeselected
    case profileSelected
    case profileDeselected
    case productPlaceholder
    case wallet
    case walletSelected
    case backBlack
    case gifts
    case categories
    case filledHeart
    case emptyHeart
    case facebook
    case phone
    case google
    case star
    case share
    case rectangularPlaceholder
    case alert
    case blueMarker
    case dollar
    case location
    case tickWithCircle
    case unTickWithCircle
    case tickWithSquare
    case unTickWithSquare
    case cross
    case whiteStar
    case blackStar
    case sliderBlueCircle
    case sliderGrayCircle
    case greenMarker
    case bubble
    case reportWhite
    case shareWhite
    case flashOn
    case flashOff
    case dummy
    case check
    case greySearch
    case whiteimg
    case blackCross
    case noData
    case icSendingTick
    case icReceiveTick

    case notificationRead
    case notificationUnread
    case edit
    
    case unCheck
    case icMutedChat
    case icChatTrash

    case menuDots
    case settings

    case icChatPlaceholder
    case icProductPlaceholder
    case icFilterDropdown
    
    case amex
    case master
    case discover
    case visa
    case icMore
    case icEmptyMessages
    case blueShare
    case currentLocation
    case appIcon
    
}


extension AppImages{
    
     var image : UIImage {
        
        switch self {
            
        case .customerLogo : return UIImage(named: "icCustomer") ?? UIImage()
            
        case .hawkDriverLogo : return UIImage(named: "icDriver") ?? UIImage()
            
        case .movingCompanyLogo : return UIImage(named: "icCompany") ?? UIImage()
            
        case .backWhite : return UIImage(named: "icBackWhite") ?? UIImage()
        
//        case .itemsActiv: return UIImage(named: "icItemsActiv") ?? UIImage()
            
        case .itemsActiv: return UIImage(named: "squaresSelected") ?? UIImage()
            
//        case .itemsInactiv: return UIImage(named: "icItemsUnactive") ?? UIImage()
            
        case .itemsInactiv: return UIImage(named: "icItemsActiv") ?? UIImage()

//        case .tagsActiv: return UIImage(named: "icTagsActive") ?? UIImage()
            
        case .tagsActiv: return UIImage(named: "locationSelected") ?? UIImage()
            
//        case .tagsInactiv: return UIImage(named: "icTagsUnactive") ?? UIImage()
            
        case .tagsInactiv: return UIImage(named: "icTagsActive") ?? UIImage()

        case .homeSelected: return UIImage(named: "icTabHomeActive") ?? UIImage()
        case .homeDeselected: return UIImage(named: "icHomeUnactive") ?? UIImage()
            
        case .hawkDriverSelected : return UIImage(named: "icTabDriverActive") ?? UIImage()
        case .hawkDriverdeselected : return UIImage(named: "icTabDriverUnactive") ?? UIImage()
            
        case .messageSelected: return UIImage(named: "icTabChatActive") ?? UIImage()
        case .messageDeselected: return UIImage(named: "icTabChatUnactive") ?? UIImage()
            
        case .profileSelected : return UIImage(named: "icTabProfileActive") ?? UIImage()
        case .profileDeselected: return UIImage(named: "icTabProfileUnactive") ?? UIImage()
            
        case .productPlaceholder: return UIImage(named: "icHomePlaceholder") ?? UIImage()
            
        case .backBlack : return UIImage(named: "icBackBlack") ?? UIImage()
        
        case .categories : return UIImage(named: "icCategory") ?? UIImage()

        case .gifts : return UIImage(named: "icGift") ?? UIImage()

        case .share : return UIImage(named: "icShare") ?? UIImage()
            
        case .google : return UIImage(named: "icDetailGoogle") ?? UIImage()
            
        case .facebook : return UIImage(named: "icDetailFacebook") ?? UIImage()
            
        case .phone : return UIImage(named: "icDetailFacebook") ?? UIImage()

        case .emptyHeart : return UIImage(named: "icLikeUnfill-1") ?? UIImage()
            
        case .filledHeart : return UIImage(named: "icDetailLikeFill") ?? UIImage()
            
        case .star: return UIImage(named: "icDetailUserRating") ?? UIImage()
            
        case .rectangularPlaceholder: return UIImage(named: "icDetailImgPlaceholder") ?? UIImage()
            
        case .alert: return UIImage(named: "icAlert") ?? UIImage()
        
        case .blueMarker: return UIImage(named: "icMapPin") ?? UIImage()
            
        case .dollar: return UIImage(named: "icDollar") ?? UIImage()
      
        case .location : return UIImage(named: "icCurrentLocation") ?? UIImage()
            
        case .tickWithCircle: return UIImage(named: "icRadioTick") ?? UIImage()
            
        case .unTickWithCircle: return UIImage(named: "icRadioBox") ?? UIImage()
            
        case .tickWithSquare: return UIImage(named: "icCheckBox") ?? UIImage()
            
        case .unTickWithSquare: return UIImage(named: "icCheck") ?? UIImage()
            
        case .cross: return UIImage(named: "icClose") ?? UIImage()
            
        case .whiteStar: return UIImage(named: "icFilterRatingWhite") ?? UIImage()
            
        case .blackStar: return UIImage(named: "icRatingBlack") ?? UIImage()
            
        case .sliderBlueCircle: return UIImage(named: "icBlueCircle") ?? UIImage()
            
        case .sliderGrayCircle: return UIImage(named: "icGreyCircle") ?? UIImage()
            
        case .greenMarker: return UIImage(named: "icCommunityGreen") ?? UIImage()
        
        case .bubble: return UIImage(named: "icInfoBubble9Patch") ?? UIImage()
            
        case .reportWhite: return UIImage(named: "icReportWhite") ?? UIImage()
            
        case .shareWhite: return UIImage(named: "icShareWhite") ?? UIImage()

        case .flashOff: return UIImage(named: "icCameraFlashlightOff") ?? UIImage()
            
        case .flashOn: return UIImage(named: "icCameraFlashlightAuto") ?? UIImage()
            
        case .check: return UIImage(named: "icCheckBox") ?? UIImage()
        
            case .icChatTrash: return UIImage(named: "icChatTrash") ?? UIImage()
            
        case .unCheck:
            return UIImage(named: "icCheck") ?? UIImage()
            
        case .greySearch:
            return UIImage(named: "icResultsSearch_2") ?? UIImage()
            
        case .whiteimg:
            return UIImage(named: "whiteimg") ?? UIImage()

        case .blackCross:
            return UIImage(named: "icSearchClose") ?? UIImage()
            
        case .icMutedChat:
            return UIImage(named: "icMutedChat") ?? UIImage()
            
        case .noData:
            return UIImage(named: "icEmptyProduct") ?? UIImage()
            
        case .edit: return UIImage(named: "icDetailEdit") ?? UIImage()

            
        case .icSendingTick: return UIImage(named: "icSendingTick") ?? UIImage()
            
        case .icReceiveTick: return UIImage(named: "icReceiveTick") ?? UIImage()
        case .icChatPlaceholder: return UIImage(named: "icChatPlaceholder") ?? UIImage()
        case .icProductPlaceholder: return UIImage(named: "icProductPlaceholder") ?? UIImage()
        case .icFilterDropdown: return UIImage(named: "icFilterDropdown") ?? UIImage()
            
        case .notificationRead: return UIImage(named: "icNotificationRead") ?? UIImage()
            
        case .notificationUnread: return UIImage(named: "icNotificationUnread") ?? UIImage()
            
        case .menuDots: return UIImage(named: "icFollowMore") ?? UIImage()

        case .settings: return UIImage(named: "icSettings") ?? UIImage()

        case .amex: return UIImage(named: "icAmericanExpress") ?? UIImage()
            
        case .visa: return UIImage(named: "icVisa") ?? UIImage()

        case .master: return UIImage(named: "icMastercard") ?? UIImage()

        case .discover: return UIImage(named: "icDiscover") ?? UIImage()
            
        case .icMore: return UIImage(named: "icMore") ?? UIImage()
        case .icEmptyMessages: return UIImage(named: "icEmptyMessages") ?? UIImage()

        case .blueShare : return UIImage(named: "icDriveShare") ?? UIImage()

        case .currentLocation : return UIImage(named: "icMyLocation") ?? UIImage()
        
        case .appIcon:
            return UIImage(named: "icDummyAppicon") ?? UIImage()
            
        case .wallet:
             return UIImage(named: "wallet") ?? UIImage()
            
        case .walletSelected:
            return UIImage(named: "wallet_selected") ?? UIImage()
            
          default: return UIImage()
            
            
        }
        
    }
}
