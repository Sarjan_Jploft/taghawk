
import UIKit
import CoreLocation
import MapKit
import Contacts
import SwiftyJSON

typealias LMCompletionHandler = ((_ info:NSDictionary?, _ placemark:CLPlacemark?, _ error:Error?)->Void)?
typealias LMLocationCompletionHandler = ((_ location:CLLocation?, _ error:Error?)->())

//Todo: Keep completion handler differerent for all services, otherwise only one will work
enum GeoCodingType{
    case geocoding
    case reverseGeocoding
}

class LocationManager: NSObject,CLLocationManagerDelegate {
    
    /* Private variables */
    fileprivate var locationCompletionHandler:LMLocationCompletionHandler?
    fileprivate var locationStatus = NSLocalizedString("Calibrating", comment: "")// to pass in handler
    fileprivate var locationManager: CLLocationManager!
    fileprivate var verboseMessage = NSLocalizedString("Calibrating", comment: "")
    
    fileprivate let verboseMessageDictionary = [CLAuthorizationStatus.notDetermined:NSLocalizedString("You have not yet made a choice with regards to this application.", comment: ""),
                                                CLAuthorizationStatus.restricted:NSLocalizedString("This application is not authorized to use location services. Due to active restrictions on location services, the user cannot change this status, and may not have personally denied authorization.", comment: ""),
                                                CLAuthorizationStatus.denied:NSLocalizedString("You have explicitly denied authorization for this application, or location services are disabled in Settings.", comment: ""),
                                                CLAuthorizationStatus.authorizedAlways:NSLocalizedString("App is Authorized to always use location services.", comment: ""),CLAuthorizationStatus.authorizedWhenInUse:NSLocalizedString("You have granted authorization to use your location only when the app is visible to you.", comment: "")]
    
    fileprivate let defaultError = NSError(code: 701, localizedDescription: "Something bad happened") as Error
    weak var delegate:LocationManagerDelegate? = nil
    
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    
    var latitudeAsString:String = ""
    var longitudeAsString:String = ""
    
    var lastKnownLatitude:Double = 0.0
    var lastKnownLongitude:Double = 0.0
    
    var lastKnownLatitudeAsString:String = ""
    var lastKnownLongitudeAsString:String = ""
    
    var keepLastKnownLocation:Bool = true
    var hasLastKnownLocation:Bool = true
    
    var moniterRegion:Bool = true
    
    var showVerboseMessage = false
    
    var isRunning = false
    var coordinate:CLLocationCoordinate2D?{
        return locationManager.location?.coordinate
    }
    var location:CLLocation?{
        return locationManager.location
    }
    
    class var shared : LocationManager {
        struct Static {
            static let instance : LocationManager = LocationManager()
        }
        return Static.instance
    }
    
    fileprivate override init(){
        
        super.init()
        
        if moniterRegion {
            moniterRegion = !CLLocationManager.significantLocationChangeMonitoringAvailable()
        }
    }
    
    fileprivate func resetLatLon(){
        
        latitude = 0.0
        longitude = 0.0
        
        latitudeAsString = ""
        longitudeAsString = ""
        
        // save data for widgit
        if let userDefaults = UserDefaults(suiteName: "group.com.watchourown.Watchourown.beta") {
            userDefaults.set(self.longitude as AnyObject, forKey: "longitude")
            userDefaults.set(self.latitude as AnyObject, forKey: "latitude")
            userDefaults.synchronize()
        }
    }
    
    fileprivate func resetLastKnownLatLon(){
        
        hasLastKnownLocation = false
        
        lastKnownLatitude = 0.0
        lastKnownLongitude = 0.0
        
        lastKnownLatitudeAsString = ""
        lastKnownLongitudeAsString = ""
        
        // save data for wigidt
        if let userDefaults = UserDefaults(suiteName: "group.com.watchourown.Watchourown.beta") {
            userDefaults.set(self.longitude as AnyObject, forKey: "longitude")
            userDefaults.set(self.latitude as AnyObject, forKey: "latitude")
            userDefaults.synchronize()
        }
    }
    
    //    func startUpdatingLocationWithCompletionHandler(_ completionHandler:((_ latitude:Double, _ longitude:Double, _ status:String, _ verboseMessage:String, _ error:String?)->())? = nil){
    //
    //        self.locationCompletionHandler = completionHandler
    //        initLocationManager()
    //    }
    
    func startUpdatingLocation(_ completionHandler:((_ location:CLLocation?, _ error:Error?)->())? = nil){
        self.locationCompletionHandler = completionHandler
        checkLocationPermissionAndPerformSpecificAction()
    }
    
    func checkLocationPermissionAndPerformSpecificAction(){
        
        if CLLocationManager.locationServicesEnabled() {
        
        switch CLLocationManager.authorizationStatus() {
           
            case .denied:
                self.goToSettingsPopUp(status: .denied)
                
                self.locationCompletionHandler?(CLLocation(latitude: 0.0, longitude: 0.0), nil)
          
        case .restricted:
                self.goToSettingsPopUp(status: .restricted)
                self.locationCompletionHandler?(CLLocation(latitude: 0.0, longitude: 0.0), nil)
            
        case .notDetermined:
                initLocationManager()

            case .authorizedWhenInUse:
                initLocationManager()

            case .authorizedAlways:
                initLocationManager()

           }
        }
    }
    
    func goToSettingsPopUp(status : CLAuthorizationStatus){
        
       _ = AlertController.alert(title: "Location Permission", message: verboseMessageDictionary[status] ?? "", buttons: ["Cancel", "Settings"]) { (_, index) in
        
        if index == 1{
            
            guard let bundleId = Bundle.main.bundleIdentifier else { return }
            
            guard let settingsUrl = URL(string: "\(UIApplication.openSettingsURLString)&path=LOCATION/\(bundleId)") else { return }
            
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        }
            
        }
        
    }
    
    func stopUpdatingLocation(){
        if locationManager == nil { return }
        if !moniterRegion {
            locationManager.stopUpdatingLocation()
        } else {
            locationManager.stopMonitoringSignificantLocationChanges()
        }
    }
    
    fileprivate func initLocationManager() {
        
        // App might be unreliable if someone changes autoupdate status in between and stops it
    
            self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.allowsBackgroundLocationUpdates = false
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        startLocationManger()
    }
    
    fileprivate func startLocationManger() {
        
        if !moniterRegion {
            
            self.locationManager.startUpdatingLocation()
        } else {
            
            self.locationManager.startMonitoringSignificantLocationChanges()
        }
        
        isRunning = true
    }
    
     func stopLocationManger() {
        
        stopUpdatingLocation()
        isRunning = false
    }
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        stopLocationManger()
        
        resetLatLon()
        
        if !keepLastKnownLocation {
            
            resetLastKnownLatLon()
        }
        
        locationCompletionHandler?(manager.location, error)
        
        if (delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerReceivedError(_:))))! {
            delegate?.locationManagerReceivedError!(error.localizedDescription as String)
        }
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let arrayOfLocation = locations as NSArray
        let location = arrayOfLocation.lastObject as! CLLocation
        let coordLatLon = location.coordinate
        
        latitude  = coordLatLon.latitude
        longitude = coordLatLon.longitude
        
        // save data for wigdit
        if let userDefaults = UserDefaults(suiteName: "group.com.watchourown.Watchourown.beta") {
            userDefaults.set(self.longitude as AnyObject, forKey: "longitude")
            userDefaults.set(self.latitude as AnyObject, forKey: "latitude")
            userDefaults.synchronize()
        }
        
        latitudeAsString  = coordLatLon.latitude.description
        longitudeAsString = coordLatLon.longitude.description
        
        
        locationCompletionHandler?(location, nil)
        //            locationCompletionHandler = nil;
        
        lastKnownLatitude = coordLatLon.latitude
        lastKnownLongitude = coordLatLon.longitude
        
        lastKnownLatitudeAsString = coordLatLon.latitude.description
        lastKnownLongitudeAsString = coordLatLon.longitude.description
        
        hasLastKnownLocation = true
        
        if delegate != nil {
            if (delegate?.responds(to: #selector(LocationManagerDelegate.locationFoundGetAsString(_:longitude:))))! {
                delegate?.locationFoundGetAsString!(latitudeAsString as String,longitude:longitudeAsString as String)
            }
            if (delegate?.responds(to: #selector(LocationManagerDelegate.locationFound(_:longitude:))))! {
                delegate?.locationFound(latitude,longitude:longitude)
            }
        }
    }
    
    internal func locationManager(_ manager: CLLocationManager,
                                  didChangeAuthorization status: CLAuthorizationStatus) {
        var hasAuthorised = false
        let verboseKey = status
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = NSLocalizedString("Restricted Access", comment: "")
        case CLAuthorizationStatus.denied:
            locationStatus = NSLocalizedString("Denied access", comment: "")
        case CLAuthorizationStatus.notDetermined:
            locationStatus = NSLocalizedString("Not determined", comment: "")
        default:
            locationStatus = NSLocalizedString("Allowed access", comment: "")
            hasAuthorised = true
        }
        
        verboseMessage = verboseMessageDictionary[verboseKey]!
        
        if hasAuthorised {
            startLocationManger()
        } else {
            
            resetLatLon()
            if !(locationStatus == NSLocalizedString("Denied access", comment: "")) {
                
                var verbose = ""
                if showVerboseMessage {
                    
                    verbose = verboseMessage
                    
                    if (delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerVerboseMessage(_:))))! {
                        
                        delegate?.locationManagerVerboseMessage!(verbose as String)
                    }
                }
                
//                locationCompletionHandler?(CLLocation(latitude: latitude, longitude: longitude) ,nil)
            }else{
                locationCompletionHandler?(CLLocation(latitude: latitude, longitude: longitude) ,nil)
            }
            if (delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerStatus(_:))))! {
                delegate?.locationManagerStatus!(locationStatus as String)
            }
        }
    }
    
    func reverseGeocodeLocationWithLatLon(latitude:Double, longitude: Double,onCompletionHandler:LMCompletionHandler){
        
        let location:CLLocation = CLLocation(latitude:latitude, longitude: longitude)
        
        reverseGeocodeLocationWithCoordinates(location, onCompletionHandler: onCompletionHandler)
    }
    
    func reverseGeocodeLocationWithCoordinates(_ location:CLLocation, onCompletionHandler:LMCompletionHandler){
        
        reverseGocode(location,onCompletionHandler:onCompletionHandler)
    }
    
    fileprivate func reverseGocode(_ location:CLLocation,onCompletionHandler:LMCompletionHandler){
        
        let geocoder: CLGeocoder = CLGeocoder()
        
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
            
            if error != nil {
                onCompletionHandler?(nil,nil, error)
            } else {
                
                if let placemark = placemarks?[0] {
                    let address = AddressParser()
                    address.parseAppleLocationData(placemark)
                    let addressDict = address.getAddressDictionary()
                    onCompletionHandler?(addressDict,placemark,nil)
                } else {
                    onCompletionHandler?(nil,nil, self.defaultError)
                    return
                }
            }
        })
    }
    
    func geocodeAddressString(address:String, onCompletionHandler:LMCompletionHandler){
        
        geoCodeAddress(address,onCompletionHandler:onCompletionHandler)
    }
    
    func geocodeAddressStringFromGoogle(address: String, addressDictionary : @escaping (JSONDictionary) -> () ) {
        let urlString = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address)&sensor=true&key=AIzaSyAd1acN1DZiiIg4EqBJR4-rxZIC96Rr47s"
        
        AppNetworking.GET(endPoint: urlString, success: { (json) in
            
            var addressDict = JSONDictionary()
            var city = ""
            if let results = json[ApiKey.results].arrayValue.first {
                let addressComponents = results[ApiKey.address_components].arrayValue
                addressComponents.forEach({ (json) in
                    if json[ApiKey.types].arrayValue.contains(JSON(ApiKey.locality)) {
                        addressDict[ApiKey.city] = json[ApiKey.long_name].stringValue
                    }
                    if json[ApiKey.types].arrayValue.contains(JSON(ApiKey.administrative_area_level_2)) {
                        city = json[ApiKey.long_name].stringValue
                    }
                    
                    if json[ApiKey.types].arrayValue.contains(JSON(ApiKey.administrative_area_level_1)) {
                        addressDict[ApiKey.state] = json[ApiKey.long_name].stringValue
                        addressDict[ApiKey.stateCode] = json[ApiKey.short_name].stringValue
                    }
                    if json[ApiKey.types].arrayValue.contains(JSON(ApiKey.country)) {
                        addressDict[ApiKey.country] = json[ApiKey.long_name].stringValue
                    }
                })
            }
            if "\(addressDict[ApiKey.city] ?? "")".isEmpty {
                addressDict[ApiKey.city] = city
            }
            addressDictionary(addressDict)
            
        }) { (err) in
            addressDictionary(JSONDictionary())
        }
    }
    
    private func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    fileprivate func geoCodeAddress(_ address:String,onCompletionHandler:LMCompletionHandler){
        
        let geocoder = CLGeocoder()
        
//        let add = CNMutablePostalAddress()
//        add.postalCode = address
//
//
//        geocoder.geocodePostalAddress(add) { (placemark, err) in
//            print(placemark)
//        }
//
//        geocoder.geocodePostalAddress(add, preferredLocale: NSLocale.current) { (placemark, err) in
//            print(placemark)
//        }
        
        geocoder.geocodeAddressString(address as String) { (placemarks: [CLPlacemark]?, error : Error?) -> Void in
            if error != nil {

                onCompletionHandler?(nil,nil,error)
            } else {
                if let placemark = placemarks?[0] {
                    let address = AddressParser()
                    address.parseAppleLocationData(placemark)
                    let addressDict = address.getAddressDictionary()
                    onCompletionHandler?(addressDict,placemark,nil)
                } else {
                    onCompletionHandler?(nil,nil, self.defaultError)
                }
            }
        }
    }
    
    func geocodeUsingGoogleAddressString(address:String, onCompletionHandler:LMCompletionHandler){
        
        geoCodeUsignGoogleAddress(address,onCompletionHandler:onCompletionHandler)
    }
    
    fileprivate func geoCodeUsignGoogleAddress(_ address:String,onCompletionHandler:LMCompletionHandler){
        
//        var urlString = "http://maps.googleapis.com/maps/api/geocode/json?address=\(address)" as String
//        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed).unwrapped
        
//        performOperationForURL(urlString, type: GeoCodingType.geocoding, onCompletionHandler: onCompletionHandler)
    }
    
    func reverseGeocodeLocationUsingGoogleWithLatLon(latitude:Double, longitude: Double,onCompletionHandler:LMCompletionHandler){
        
        reverseGocodeUsingGoogle(latitude: latitude, longitude: longitude,onCompletionHandler:onCompletionHandler)
    }
    
    func reverseGeocodeLocationUsingGoogleWithCoordinates(_ coord:CLLocation, onCompletionHandler:LMCompletionHandler){
        
        reverseGeocodeLocationUsingGoogleWithLatLon(latitude: coord.coordinate.latitude, longitude: coord.coordinate.longitude, onCompletionHandler: onCompletionHandler)
    }
    
    fileprivate func reverseGocodeUsingGoogle(latitude:Double, longitude: Double,onCompletionHandler:LMCompletionHandler){
        
        var urlString = "http://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)" as String
//        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed).unwrapped
//
//
//        performOperationForURL(urlString, type: GeoCodingType.reverseGeocoding,onCompletionHandler:onCompletionHandler)
    }
    
    
    fileprivate func performOperationForURL(_ urlString:String,type:GeoCodingType,onCompletionHandler:LMCompletionHandler){
        
        let url:URL? = URL(string:urlString as String)
        
        let request:URLRequest = URLRequest(url:url!)
        
        let queue:OperationQueue = OperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request,queue:queue,completionHandler:{[weak self] response,data,error in
            
            if error != nil {
                
                onCompletionHandler?(nil, nil, error)
                
            } else {
                
                let kStatus = "status"
                let kOK = "ok"
                //let dataAsString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
                
                let jsonResult: NSDictionary = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                
                var status = jsonResult.value(forKey: kStatus) as! String
                status = status.lowercased() as String
                
                if status.isEqual(kOK) {
                    
                    let address = AddressParser()
                    
                    address.parseGoogleLocationData(jsonResult)
                    
                    let addressDict = address.getAddressDictionary()
                    let placemark:CLPlacemark = address.getPlacemark()
                    
                    onCompletionHandler?(addressDict,placemark,nil)
                    
                }
                else {
                    
                    //status = (status.componentsSeparatedByString("_") as NSArray).componentsJoinedByString(" ").capitalizedString
                    onCompletionHandler?(nil,nil,self?.defaultError)
                }
            }
        })
    }
    
}

@objc protocol LocationManagerDelegate : NSObjectProtocol
{
    func locationFound(_ latitude:Double, longitude:Double)
    @objc optional func locationFoundGetAsString(_ latitude:String, longitude:String)
    @objc optional func locationManagerStatus(_ status:String)
    @objc optional func locationManagerReceivedError(_ error:String)
    @objc optional func locationManagerVerboseMessage(_ message:String)
}

private class AddressParser: NSObject{
    
    fileprivate var latitude = String()
    fileprivate var longitude  = String()
    fileprivate var streetNumber = String()
    fileprivate var route = String()
    fileprivate var locality = String()
    fileprivate var subLocality = String()
    fileprivate var formattedAddress = String()
    fileprivate var administrativeArea = String()
    fileprivate var administrativeAreaCode = String()
    fileprivate var subAdministrativeArea = String()
    fileprivate var postalCode = String()
    fileprivate var country = String()
    fileprivate var subThoroughfare = String()
    fileprivate var thoroughfare = String()
    fileprivate var ISOcountryCode = String()
    fileprivate var state = String()
    
    
    override init() {
        
        super.init()
    }
    
    fileprivate func getAddressDictionary()-> NSDictionary {
        
        let addressDict = NSMutableDictionary()
        
        addressDict.setValue(latitude, forKey: "latitude")
        addressDict.setValue(longitude, forKey: "longitude")
        addressDict.setValue(streetNumber, forKey: "streetNumber")
        addressDict.setValue(locality, forKey: "locality")
        addressDict.setValue(subLocality, forKey: "subLocality")
        addressDict.setValue(administrativeArea, forKey: "administrativeArea")
        addressDict.setValue(postalCode, forKey: "postalCode")
        addressDict.setValue(country, forKey: "country")
        addressDict.setValue(formattedAddress, forKey: "formattedAddress")
        
        return addressDict
    }
    
    fileprivate func parseAppleLocationData(_ placemark:CLPlacemark) {
        
        let addressLines = placemark.addressDictionary?["FormattedAddressLines"]
        self.streetNumber = (placemark.thoroughfare != nil ? placemark.thoroughfare : "")!
        self.locality = (placemark.locality != nil ? placemark.locality : "")!
        self.postalCode = (placemark.postalCode != nil ? placemark.postalCode : "")!
        self.subLocality = (placemark.subLocality != nil ? placemark.subLocality : "")!
        self.administrativeArea = (placemark.administrativeArea != nil ? placemark.administrativeArea : "")!
        self.country = (placemark.country != nil ?  placemark.country : "")!
        self.longitude = placemark.location!.coordinate.longitude.description as String;
        self.latitude = placemark.location!.coordinate.latitude.description as String
        
        if (addressLines! as AnyObject).count>0 {
            self.formattedAddress = (addressLines! as AnyObject).componentsJoined(by: ", ")
        } else {
            self.formattedAddress = ""
        }
        
        // save data for widgit
        if let userDefaults = UserDefaults(suiteName: "group.com.watchourown.Watchourown.beta") {
            userDefaults.set(self.longitude as AnyObject, forKey: "longitude")
            userDefaults.set(self.latitude as AnyObject, forKey: "latitude")
            userDefaults.synchronize()
        }
    }
    
    fileprivate func parseGoogleLocationData(_ resultDict:NSDictionary) {
        
        let locationDict = (resultDict.value(forKey: "results") as! NSArray).firstObject as! NSDictionary
        
        let formattedAddrs = locationDict.object(forKey: "formatted_address") as! String
        
        let geometry = locationDict.object(forKey: "geometry") as! NSDictionary
        let location = geometry.object(forKey: "location") as! NSDictionary
        let lat = location.object(forKey: "lat") as! Double
        let lng = location.object(forKey: "lng") as! Double
        
        self.latitude = lat.description as String
        self.longitude = lng.description as String
        
        // save data for widgit
        if let userDefaults = UserDefaults(suiteName: "group.com.watchourown.Watchourown.beta") {
            userDefaults.set(self.longitude as AnyObject, forKey: "longitude")
            userDefaults.set(self.latitude as AnyObject, forKey: "latitude")
            userDefaults.synchronize()
        }
        
        let addressComponents = locationDict.object(forKey: "address_components") as! NSArray
        
        self.subThoroughfare = component("street_number", inArray: addressComponents, ofType: "long_name")
        self.thoroughfare = component("route", inArray: addressComponents, ofType: "long_name")
        self.streetNumber = self.subThoroughfare
        self.locality = component("locality", inArray: addressComponents, ofType: "long_name")
        self.postalCode = component("postal_code", inArray: addressComponents, ofType: "long_name")
        self.route = component("route", inArray: addressComponents, ofType: "long_name")
        self.subLocality = component("subLocality", inArray: addressComponents, ofType: "long_name")
        self.administrativeArea = component("administrative_area_level_1", inArray: addressComponents, ofType: "long_name")
        self.administrativeAreaCode = component("administrative_area_level_1", inArray: addressComponents, ofType: "short_name")
        self.subAdministrativeArea = component("administrative_area_level_2", inArray: addressComponents, ofType: "long_name")
        self.country =  component("country", inArray: addressComponents, ofType: "long_name")
        self.ISOcountryCode =  component("country", inArray: addressComponents, ofType: "short_name")
        self.formattedAddress = formattedAddrs;
    }
    
    fileprivate func component(_ component:String,inArray:NSArray,ofType:String) -> String {
        
        let indexArr = inArray.filter({
            
            let objDict:NSDictionary = $0 as! NSDictionary
            let types:NSArray = objDict.object(forKey: "types") as! NSArray
            let type = types.firstObject as! String
            return type.isEqual(component as String)
        })
        
        if let objct = indexArr.first as? NSDictionary{
            
            let type = (objct.value(forKey: ofType as String)!) as! String
            if NSString(string:type).length > 0 {
                return type
            }
            
        }
        return ""
    }
    
    fileprivate func getPlacemark() -> CLPlacemark {
        
        let addressDict = NSMutableDictionary()
        
        let formattedAddressArray = self.formattedAddress.components(separatedBy: ", ") as Array
        
        let kSubAdministrativeArea = "SubAdministrativeArea"
        let kSubLocality           = "SubLocality"
        let kState                 = "State"
        let kStreet                = "Street"
        let kThoroughfare          = "Thoroughfare"
        let kFormattedAddressLines = "FormattedAddressLines"
        let kSubThoroughfare       = "SubThoroughfare"
        let kPostCodeExtension     = "PostCodeExtension"
        let kCity                  = "City"
        let kZIP                   = "ZIP"
        let kCountry               = "Country"
        let kCountryCode           = "CountryCode"
        
        addressDict.setObject(self.subAdministrativeArea, forKey: kSubAdministrativeArea as NSCopying)
        addressDict.setObject(self.subLocality, forKey: kSubLocality as NSCopying)
        addressDict.setObject(self.administrativeAreaCode, forKey: kState as NSCopying)
        
        //addressDict.setObject(formattedAddressArray.first as! String, forKey: kStreet)
        
        addressDict.setObject(formattedAddressArray.first!, forKey: kStreet as NSCopying)
        
        addressDict.setObject(self.thoroughfare, forKey: kThoroughfare as NSCopying)
        addressDict.setObject(formattedAddressArray, forKey: kFormattedAddressLines as NSCopying)
        addressDict.setObject(self.subThoroughfare, forKey: kSubThoroughfare as NSCopying)
        addressDict.setObject("", forKey: kPostCodeExtension as NSCopying)
        addressDict.setObject(self.locality, forKey: kCity as NSCopying)
        
        addressDict.setObject(self.postalCode, forKey: kZIP as NSCopying)
        addressDict.setObject(self.country, forKey: kCountry as NSCopying)
        addressDict.setObject(self.ISOcountryCode, forKey: kCountryCode as NSCopying)
        
        
        let lat = Double(self.latitude) ?? 0.0
        let lng = Double(self.longitude) ?? 0.0
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        
        let d = addressDict as NSDictionary as! [AnyHashable: Any]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary:  d as? [String:Any])
        return (placemark as CLPlacemark)
    }
}
