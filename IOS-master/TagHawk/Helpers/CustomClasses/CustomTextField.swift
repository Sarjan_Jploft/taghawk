//
//  CustomTextField.swift
//  Dr.BindraApp
//
//  Created by Appniventiv on 31/12/18.
//  Copyright © 2018 Dr.Bindra. All rights reserved.
//

import Foundation
import MaterialComponents.MaterialTextFields


// MARK:- CUSTOM TEXTFIELD DELEGATE
//===================================
protocol CustomTextFieldDelegate: UITextFieldDelegate {
    func textFieldDidChange(_ textField: UITextField)
}

enum TextFieldType2 {
    case none,email, password, mobileNumber, firstName, lastName
}

enum TextFieldState {
    case valid
    case invalid(String)
    case none
    
    var associatedValue: String {
        switch self {
        case .invalid(let str):
            return str
        default:
            return ""
        }
    }
}

class CustomTextField: MDCTextField {
    
    weak var textFieldDelegate: CustomTextFieldDelegate!
    var customTextFieldController: MDCTextInputControllerOutlined!
    var textFieldState : TextFieldState = .none
    
    
    
//    MDCTextInputControllerOutlined
    
    
    var textFieldType : TextFieldType2 = .none {
        didSet {
            self.setupTextField()
            //self.addTargets()
        }
    }
    
    var error : String? = "" {
        didSet {
            
            if let er = error, !er.isEmpty{
                self.customTextFieldController.setErrorText(error, errorAccessibilityValue: nil)
            }else{
                self.customTextFieldController.setErrorText(nil, errorAccessibilityValue: nil)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupTextField()
//        self.addTargets()
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupTextField()
//        self.addTargets()
    }

    func setupTextField() {
        
        self.customTextFieldController = MDCTextInputControllerOutlined(textInput: self)
        
        self.customTextFieldController.floatingPlaceholderActiveColor = AppColors.appBlueColor
        self.customTextFieldController.floatingPlaceholderNormalColor = AppColors.lightGreyTextColor
        
         self.customTextFieldController.inlinePlaceholderColor = AppColors.lightGreyTextColor
        
        self.customTextFieldController.errorColor = AppColors.textfield_Error
        self.customTextFieldController.normalColor = AppColors.textfield_Border

        self.customTextFieldController.textInputFont = AppFonts.Galano_Regular.withSize(14)
        self.customTextFieldController.inlinePlaceholderFont = AppFonts.Galano_Regular.withSize(14)

        self.customTextFieldController.activeColor = AppColors.themeColor
        self.customTextFieldController.disabledColor = AppColors.textfield_Border

        customTextFieldController.roundedCorners = UIRectCorner.topLeft.union(.topRight).union(UIRectCorner.bottomLeft).union(UIRectCorner.bottomRight)
        
        
        //        UIRectCorner.topLeft.union(.topRight).union(UIRectCorner.bottomLeft).union(UIRectCorner.bottomRight)
    }
    
    func addTargets() {
        switch self.textFieldType {
        case .email:
            self.keyboardType = .emailAddress
        case .mobileNumber:
            self.keyboardType = .phonePad
        default:
            self.keyboardType = .default
        }
        self.delegate = self
        
        self.addTarget(self, action: #selector(self.texfieldDidEndEditing(_:)), for: .editingDidEnd)
        self.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)
        self.addTarget(self, action: #selector(self.texfieldDidBeginEditing(_:)), for: .editingDidBegin)
    }
}

extension CustomTextField : UITextFieldDelegate {
    
    @objc private func texfieldDidEndEditing(_ textfield: UITextField) {
        self.textFieldDelegate?.textFieldDidEndEditing?(textfield)
    }
    
    @objc private func texfieldDidChange(_ textfield: UITextField) {
        self.textFieldDelegate?.textFieldDidChange(textfield)
    }
    
    @objc private func texfieldDidBeginEditing(_ textfield: UITextField) {
//        self.error = ""
        self.textFieldDelegate?.textFieldDidBeginEditing?(textfield)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.textFieldDelegate?.textFieldShouldReturn?(textField) ?? true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let inputMode = textField.textInputMode else {
            return false
        }
        if inputMode.primaryLanguage == "emoji" || !(inputMode.primaryLanguage != nil) {
            return false
        } else if string == " " {
            return false
        }
        
        guard let userEnteredString = textField.text else { return false }
        let newString = (userEnteredString as NSString).replacingCharacters(in: range, with: string) as NSString
        switch self.textFieldType {
        case .firstName, .lastName:
            
            let ACCEPTABLE_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            if string != filtered {
                return false
            } else if newString.length > 100 {
                return false
            }
        case .email:
            if newString.length > 100 {
                return false
            }
        case .password:
            if newString.length > 100 {
                return false
            }
        case .mobileNumber:
            let ACCEPTABLE_CHARACTERS = "0123456789"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            if string != filtered {
                return false
            } else if newString.length > 100 {
                return false
            }
        default:
            break
        }
        return self.textFieldDelegate?.textField?(textField,shouldChangeCharactersIn:range, replacementString:string) ?? true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return self.textFieldDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
}

