//
//  CommonClasses.swift
//  DittoFashionMarketBeta
//
//  Created by Bhavneet Singh on 23/11/17.
//  Copyright © 2017 Bhavneet Singh. All rights reserved.
//

import UIKit

import NVActivityIndicatorView
import MobileCoreServices
import AVFoundation

class CommonFunctions {
    
    /// Delay Functions
    class func delay(delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure()
            
        }
    }
    
    /// Show Action Sheet With Actions Array
    class func showActionSheetWithActionArray(_ title: String?, message: String?,
                                              viewController: UIViewController,
                                              alertActionArray : [UIAlertAction],
                                              preferredStyle: UIAlertController.Style)  {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        alertActionArray.forEach{ alert.addAction($0) }
        
        DispatchQueue.mainQueueAsync {
            viewController.present(alert, animated: true, completion: nil)
        }
    }

    /// Show Activity Loader
    class func showActivityLoader() {
//        DispatchQueue.mainQueueAsync {
//            if let vc = AppDelegate.shared.window?.rootViewController {
//                vc.startNYLoader()
//            }
//        }
    }
    
    /// Hide Activity Loader
    class func hideActivityLoader() {
//        DispatchQueue.mainQueueAsync {
//            if let vc = AppDelegate.shared.window?.rootViewController {
//                vc.stopAnimating()
//            }
//        }
    }
}


//MARK:- Manage Toast message
extension CommonFunctions{
    
    class func showToastMessage(_ text: String, delay: TimeInterval = 0.2, duration: TimeInterval = Delay.short) {
        if let currentToast = ToastCenter.default.currentToast {
            currentToast.cancel()
        }
        let toast = Toast(text: text)
        toast.show()
    }
    
    class func showToastSuccessMessage(_ text: String, delay: TimeInterval = 0.2, duration: TimeInterval = Delay.short) {
        if let currentToast = ToastCenter.default.currentToast {
            currentToast.cancel()
        }
        
        let toast = Toast(text: text)
        
        //        toast.view.backgroundColor = AppColors.themeBlue.color
        toast.show()
    }
    
    
    class func shareWithSocialMedia(message:String,vcObj:UIViewController, sub: String = "TagHawk")
    {
        
        var sharingItems = [AnyObject]()
        
        // sharingItems.append(sub as AnyObject)
        sharingItems.append(message as AnyObject)
        
        let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        
        activityViewController.setValue(sub, forKey: "Subject")
        
            var activitiesToExclude : [String] = []
            
            activitiesToExclude = [UIActivity.ActivityType.airDrop.rawValue, UIActivity.ActivityType.print.rawValue, UIActivity.ActivityType.copyToPasteboard.rawValue, UIActivity.ActivityType.assignToContact.rawValue, UIActivity.ActivityType.saveToCameraRoll.rawValue, UIActivity.ActivityType.addToReadingList.rawValue, UIActivity.ActivityType.openInIBooks.rawValue]

        if Display.phone {
            vcObj.present(activityViewController, animated: true, completion: nil)
        }
    }

    
    // Get Country Code through Locale
    // ===============================
    class func getCurrentCountryCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            printDebug("countryCode: \(countryCode)")
            if AppUserDefaults.value(forKey: .countryCode).stringValue != countryCode {
                
                let file = Bundle.main.path(forResource: "countryData", ofType: "json")
                
                do{
                    let data = try Data(contentsOf: URL(fileURLWithPath: file!))
                    let jsonData = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionaryArray
                    
                    if let currentCountry = jsonData?.filter({ (obj) -> Bool in
                        if let country_name = obj["ISOCode"] as? String {
                            printDebug("ISOCode: \(country_name)")
                            return countryCode == country_name ? true : false
                        } else {
                            return false
                        }
                    }) {
                        if let phoneCode = currentCountry.first?["CountryCode"] as? Int {
                            AppUserDefaults.save(value:  "+\(phoneCode)", forKey: .countryCode)
                        }
                    }
                }catch(let error){
                    printDebug(error.localizedDescription)
                }
            }
        }
    }
    
    class func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
}
