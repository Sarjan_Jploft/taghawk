//
//  GroupChatFireBaseController.swift
//  TagHawk
//
//  Created by Appinventiv on 08/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol GroupChatFireBaseControllerDelegate: SingleChatFireBaseControllerDelegate {
    
    func getGroupDetail(detail: GroupDetail, pinned: Bool)
    func roomExist()
    func changedInRoomData(detail: GroupDetail)
    func exitFromGroup(id: String, pinned: Bool)
    func deleteGroup()
    func isGroupDeleted()
    func groupInfoUpdated()
    func messageRead(index: IndexPath)
    func roomNotExist()
    func updateTagSuccessfully()
}

extension GroupChatFireBaseControllerDelegate {
    
    func getGroupDetail(detail: GroupDetail, pinned: Bool){}
    func roomExist(){}
    func changedInRoomData(detail: GroupDetail){}
    func exitFromGroup(id: String, pinned: Bool){}
    func deleteGroup(){}
    func isGroupDeleted(){}
    func groupInfoUpdated(){}
    func messageRead(index: IndexPath){}
    func roomNotExist(){}
    func updateTagSuccessfully(){}
}

class GroupChatFireBaseController {
    
    static let shared = GroupChatFireBaseController()
    weak var delegate: GroupChatFireBaseControllerDelegate?
    var handler = [(String, String, UInt)]()
    let paginationData: UInt = 100
    var newMessageHandler: UInt?
    
    //    MARK:- Remove Observers
    //    =======================
    func removeObservers(){
        
        handler.forEach { (handlerValue) in
            
            DataBaseReference.shared.dataBase
                .child(handlerValue.0)
                .child(handlerValue.1).removeObserver(withHandle: handlerValue.2)
        }
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId).removeAllObservers()
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue).removeAllObservers()
    }
    
    //    MARK:- Add Group
    //    =================
    func addGroup(tagData: AddTagResult,
                  msg: String){
        
        let timeInterval = Date().unixFirebaseTimestamp
        let jsonDic: JSONDictionary = [FireBaseKeys.roomId.rawValue: tagData.id,
                                       FireBaseKeys.pinned.rawValue: false,
                                       FireBaseKeys.chatMute.rawValue: false,
                                       FireBaseKeys.mute.rawValue: false,
                                       FireBaseKeys.chatType.rawValue: ChatType.group.rawValue,
                                       FireBaseKeys.userType.rawValue: UserType.normal.rawValue,
                                       FireBaseKeys.otherUserId.rawValue: UserProfile.main.userId,
                                       FireBaseKeys.unreadMessageCount.rawValue: 0,
                                       FireBaseKeys.createdTimeStamp.rawValue: timeInterval,
                                       FireBaseKeys.roomName.rawValue: tagData.name,
                                       FireBaseKeys.roomImage.rawValue: tagData.tagImageURL]
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(tagData.id)
            .setValue(jsonDic)
        let roomData = ChatListing(json: JSON(jsonDic))
        delegate?.getRoomData(info: roomData)
        addGroupOnGroupNode(roomInfo: roomData,
                            tagData: tagData,
                            interval: timeInterval,
                            message: msg)
    }
    
    //    MARK:- Add group on TagDetail Node
    //    ==================================
    private func addGroupOnGroupNode(roomInfo: ChatListing,
                                     tagData: AddTagResult,
                                     interval: Int,
                                     message: String){
        
        let userDetail : JSONDictionary = [FireBaseKeys.memberId.rawValue: UserProfile.main.userId,
                                           FireBaseKeys.memberName.rawValue: UserProfile.main.fullName,
                                           FireBaseKeys.blocked.rawValue: false,
                                           FireBaseKeys.mute.rawValue: false,
                                           FireBaseKeys.memberType.rawValue: MemberType.owner.rawValue,
                                           FireBaseKeys.memberImage.rawValue: UserProfile.main.profilePicture]
        
        let members = [UserProfile.main.userId: userDetail]
        
        var jsonDic: JSONDictionary = [FireBaseKeys.tagId.rawValue: tagData.id,
                                       FireBaseKeys.tagImageUrl.rawValue: tagData.tagImageURL,
                                       FireBaseKeys.tagName.rawValue: tagData.name,
                                       FireBaseKeys.tagAddress.rawValue: tagData.tagAddress,
                                       FireBaseKeys.description.rawValue: tagData.description,
                                       FireBaseKeys.tagType.rawValue: tagData.tagType.rawValue,
                                       FireBaseKeys.tagLongitude.rawValue: tagData.tagLong,
                                       FireBaseKeys.tagLatitude.rawValue: tagData.tagLat,
                                       FireBaseKeys.shareCode.rawValue: tagData.shareCode,
                                       FireBaseKeys.shareLink.rawValue: tagData.deepLinkUrl,
                                       FireBaseKeys.verificationType.rawValue: tagData.privateTagType.rawValue,
                                       FireBaseKeys.verificationData.rawValue: tagData.tagData,
                                       FireBaseNode.members.rawValue: members,
                                       FireBaseKeys.announcement.rawValue: tagData.announcement,
                                       FireBaseKeys.ownerId.rawValue: UserProfile.main.userId,
                                       FireBaseKeys.pendingRequestCount.rawValue: 0]
        
        let messageData : JSONDictionary = [FireBaseKeys.roomId.rawValue: tagData.id,
                                            FireBaseKeys.messageText.rawValue: message,
                                            FireBaseKeys.senderId.rawValue: UserProfile.main.userId,
                                            FireBaseKeys.senderName.rawValue: UserProfile.main.fullName,
                                            FireBaseKeys.senderImage.rawValue: UserProfile.main.profilePicture,
                                            FireBaseKeys.timeStamp.rawValue: interval,
                                            FireBaseKeys.messageStatus.rawValue: MessageStatus.delivered.rawValue,
                                            FireBaseKeys.messageType.rawValue: MessageType.tagCreatedHeader.rawValue]
        
        jsonDic[FireBaseNode.lastMessage.rawValue] = messageData
        
        
        // create a tag created header
        addmessage(text: message,
                   tagID: roomInfo.roomID,
                   detail: nil,
                   messageType: .tagCreatedHeader)
        
        addTagsOnUserNode(id: tagData.id)
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(tagData.id)
            .setValue(jsonDic)
    }
    
    //    MARK:- add Tags on User node
    //    ============================
    func addTagsOnUserNode(id: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(UserProfile.main.userId).observeSingleEvent(of: .value) { (snapShot) in
                
                if snapShot.exists(), let value = snapShot.value{
                    var userData = UserProfile(withFireBase: JSON(value))
                    let text = userData.myTags.isEmpty ? id : ",\(id)"
                    userData.myTags.append(text)
                    
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.users.rawValue)
                        .child(UserProfile.main.userId).updateChildValues([FireBaseKeys.myTags.rawValue: userData.myTags])
                }
        }
    }
    
    //    MARK:- Update group Info
    //    =========================
    func updateGroupInfo(tagData: AddTagResult){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(tagData.id).observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                if snapShot.exists(), let value = snapShot.value{
                    
                    let groupInfo = GroupDetail(json: JSON(value))
                    groupInfo?.members.forEach({ (member) in
                        DataBaseReference.shared.dataBase
                            .child(FireBaseNode.roomGroupIds.rawValue)
                            .child(member.id)
                            .child(tagData.id)
                            .updateChildValues([FireBaseKeys.roomImage.rawValue: tagData.tagImageURL,
                                                FireBaseKeys.roomName.rawValue: tagData.name])
                    })
                    
                    let jsonDic: JSONDictionary = [FireBaseKeys.tagImageUrl.rawValue: tagData.tagImageURL,
                                                   FireBaseKeys.tagName.rawValue: tagData.name,
                                                   FireBaseKeys.tagAddress.rawValue: tagData.tagAddress,
                                                   FireBaseKeys.description.rawValue: tagData.description,
                                                   FireBaseKeys.tagType.rawValue: tagData.tagType.rawValue,
                                                   FireBaseKeys.tagLongitude.rawValue: tagData.tagLong,
                                                   FireBaseKeys.tagLatitude.rawValue: tagData.tagLat,
                                                   FireBaseKeys.shareCode.rawValue: tagData.shareCode,
                                                   FireBaseKeys.shareLink.rawValue: tagData.deepLinkUrl,
                                                   FireBaseKeys.verificationType.rawValue: tagData.privateTagType.rawValue,
                                                   FireBaseKeys.verificationData.rawValue: tagData.tagData,
                                                   FireBaseKeys.announcement.rawValue: tagData.announcement]
                    
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.tagsDetail.rawValue)
                        .child(tagData.id)
                        .updateChildValues(jsonDic)
                    
                    self?.delegate?.updateTagSuccessfully()
                }
        }
    }
    
    //    MARK:- Check if room exist
    //    ==========================
    func toCheckRoomExist(id: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(id)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                guard let strongSelf = self else{
                    return
                }
                if snapShot.exists(),
                    let value = snapShot.value{
                    let roomInfo = ChatListing(json: JSON(value))
                    strongSelf.delegate?.getRoomData(info: roomInfo)
                }else{
                   strongSelf.delegate?.roomNotExist()
                }
        }
    }
    
    //    MARK:- Join A Member
    //    =====================
    func joinGroup(tagData: Tag, data: UserProfile){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(tagData.id)
            .child(FireBaseNode.members.rawValue)
            .child(data.userId)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                guard let strongSelf = self else{
                    return
                }
                
                if !snapShot.exists(){
                    
                    let userDetail : JSONDictionary = [FireBaseKeys.memberId.rawValue: data.userId,
                                                       FireBaseKeys.memberName.rawValue: data.fullName,
                                                       FireBaseKeys.blocked.rawValue: false,
                                                       FireBaseKeys.mute.rawValue: false,
                                                       FireBaseKeys.memberType.rawValue: MemberType.member.rawValue,
                                                       FireBaseKeys.memberImage.rawValue: data.profilePicture]
                    
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.tagsDetail.rawValue)
                        .child(tagData.id)
                        .child(FireBaseNode.members.rawValue)
                        .child(data.userId)
                        .setValue(userDetail)
                    strongSelf.addGroupInRoomIDNode(tag: tagData,
                                                    userData: data)
                }else{
                    let userDetail : JSONDictionary = [FireBaseKeys.fullName.rawValue: data.fullName,
                                                       FireBaseKeys.memberImage.rawValue: data.profilePicture]
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.tagsDetail.rawValue)
                        .child(tagData.id)
                        .child(FireBaseNode.members.rawValue)
                        .child(data.userId)
                        .updateChildValues(userDetail)
                }
        }
    }
    
    //    MARK:-
    
    //    MARK:- Add Group Node on RoomID Node
    //    =====================================
    func addGroupInRoomIDNode(tag: Tag,
                              userData: UserProfile){
        
        let timeInterval = Date().unixFirebaseTimestamp
        let jsonDic: JSONDictionary = [FireBaseKeys.roomId.rawValue: tag.id,
                                       FireBaseKeys.pinned.rawValue: false,
                                       FireBaseKeys.chatMute.rawValue: false,
                                       FireBaseKeys.mute.rawValue: false,
                                       FireBaseKeys.chatType.rawValue: ChatType.group.rawValue,
                                       FireBaseKeys.userType.rawValue: UserType.normal.rawValue,
                                       FireBaseKeys.unreadMessageCount.rawValue: 0,
                                       FireBaseKeys.createdTimeStamp.rawValue: timeInterval,
                                       FireBaseKeys.roomName.rawValue: tag.name,
                                       FireBaseKeys.roomImage.rawValue: tag.tagImageUrl]
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(userData.userId)
            .child(tag.id)
            .setValue(jsonDic)
        let roomInfo = ChatListing(json: JSON(jsonDic))
        delegate?.getRoomData(info: roomInfo)
        addmessage(text: userData.userId,
                   tagID: roomInfo.roomID,
                   detail: nil,
                   messageType: .userJoin)
    }
    
    //    MARK:- Get Group Data
    //    ========================
    func getGroupData(groupID: String, isPinned: Bool){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(groupID)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                printDebug("GroupDetail: \(snapShot)")
                if snapShot.exists(),
                    let value = snapShot.value{
                    if let data = GroupDetail(json: JSON(value)){
                        self?.delegate?.getGroupDetail(detail: data, pinned: isPinned)
                    }
                }
        }
    }
    
    //    MARK:- Add Messages to node
    //    ===========================
    func addmessage(text: String,
                    tagID: String,
                    detail: GroupDetail?,
                    messageType: MessageType){
        
        switch messageType {
            
        case .tagCreatedHeader:
            addMessageOnNode(msgType: .timeHeader,
                             msgText: text,
                             taggID: tagID,
                             groupInfo: detail,
                             timeeStamp: Date().unixFirebaseTimestamp)
            
        case .image, .text, .ownershipTransfer, .userLeft, .userRemove:
            if let info = detail{
                let previousMsgTime = Date(timeIntervalSince1970: info.lastMessage.timeInterval)
                let timeStamp = Date().unixFirebaseTimestamp
                let time = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
                
                if previousMsgTime.isYesterday && time.isToday{
                    
                    addMessageOnNode(msgType: .timeHeader,
                                     msgText: text,
                                     taggID: tagID,
                                     groupInfo: detail,
                                     timeeStamp: timeStamp)
                }else{
                    let timeDifference = Date(timeIntervalSince1970: info.lastMessage.timeInterval).timeDifferenceInMin
                    if timeDifference > 10{
                        addMessageOnNode(msgType: .timeHeader,
                                         msgText: text,
                                         taggID: tagID,
                                         groupInfo: detail,
                                         timeeStamp: Date().unixFirebaseTimestamp)
                    }
                }
            }
            
        default: break
        }
        
        addMessageOnNode(msgType: messageType,
                         msgText: text,
                         taggID: tagID,
                         groupInfo: detail,
                         timeeStamp: Date().unixFirebaseTimestamp)
    }
    
    private func addMessageOnNode(msgType: MessageType,
                                  msgText: String,
                                  taggID: String,
                                  groupInfo: GroupDetail?,
                                  timeeStamp: Int){
        
        var messageData : JSONDictionary = [FireBaseKeys.roomId.rawValue: taggID,
                                            FireBaseKeys.messageText.rawValue: msgText,
                                            FireBaseKeys.senderId.rawValue: UserProfile.main.userId,
                                            FireBaseKeys.senderName.rawValue: UserProfile.main.fullName,
                                            FireBaseKeys.senderImage.rawValue: UserProfile.main.profilePicture,
                                            FireBaseKeys.timeStamp.rawValue: timeeStamp,
                                            FireBaseKeys.messageStatus.rawValue: MessageStatus.delivered.rawValue,
                                            FireBaseKeys.messageType.rawValue: msgType.rawValue,
                                            FireBaseKeys.memberCount.rawValue: groupInfo?.members.count ?? 0,
                                            FireBaseKeys.readCount.rawValue: 1]
        
        let messageNode = DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
        let messageAutoID = messageNode.child(taggID).childByAutoId()
        messageData[FireBaseKeys.messageId.rawValue] = messageAutoID.key
        messageAutoID.setValue(messageData)
        
        if msgType == .image{
            let message = MessageDetail(json: JSON(messageData))
            delegate?.getImageMessage(messageData: message)
        }
        
        switch msgType {
            
        case .image, .text, .tagCreatedHeader,.userJoin,.userLeft,.userRemove, .ownershipTransfer:
            updateLastMessage(messageData: messageData, roomID: taggID)
            //            if messageType == .image || messageType == .text{
            updateReadCount(tagInfo: groupInfo,
                            messagetType: msgType,
                            msg: msgText)
        //            }
        default: break
        }
    }
    
    //    MARK:- Update Last Message
    //    ===========================
    func updateLastMessage(messageData: JSONDictionary, roomID: String){
        
        let groupNode = DataBaseReference.shared.dataBase.child(FireBaseNode.tagsDetail.rawValue)
        groupNode.child(roomID)
            .child(FireBaseNode.lastMessage.rawValue)
            .setValue(messageData)
    }
    
    //    MARK:- Get New Message
    //    ======================
    func getNewMessage(rooomId: String, msgTimeStamp: Int, roomCreatedtime: Int){
        
            if msgTimeStamp > roomCreatedtime{
                
                let handlerValue = DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
                    .child(rooomId)
                    .queryOrdered(byChild: FireBaseKeys.timeStamp.rawValue)
                    .queryStarting(atValue: msgTimeStamp)
                    .observe(.childAdded) {[weak self](snapshot) in
                        printDebug(snapshot)
                        if snapshot.exists(),
                            let value = snapshot.value {
                            let jsonData = JSON(value)
                            let message = MessageDetail(json: jsonData)
                            self?.delegate?.getNewMessage(messageData: message)
                        }
                }
                
                handler.append((FireBaseNode.messages.rawValue, rooomId, handlerValue))
            }
    }
    
    //    MARK:- Get Messages
    //    ===================
    func getMessages(roomID: String, timeStamp: Int){
        
            DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
                .child(roomID)
                .queryOrdered(byChild: FireBaseKeys.timeStamp.rawValue)
                .queryEnding(atValue: Date().unixFirebaseTimestamp)
                .queryLimited(toLast: paginationData)
                .queryStarting(atValue: timeStamp)
                .observeSingleEvent(of: .value) {[weak self](snapshot) in
                    
                    printDebug(snapshot)
                    
                    if snapshot.exists(),
                        let value = snapshot.value {
                        let messagesDictionary = JSON(value).dictionaryValue
                        let messagesIDs = Array(messagesDictionary.keys)
                        var messages: [MessageDetail] = []
                        messagesIDs.forEach {(ids) in
                            let messageDetail = MessageDetail(json: messagesDictionary[ids] ?? JSON())
                            messages.append(messageDetail)
                            if messageDetail.readCount < messageDetail.memberCount{
                                self?.updateReadCount(messsageData: messageDetail)
                                self?.checkMessageRead(messsageData: messageDetail)
                            }
                        }
                        
                        let messageList = messages.sorted(by: {$0.timeStamp < $1.timeStamp})
                        self?.delegate?.getMessageData(messageData: messageList)
                    }
                    self?.dimisnReadCount(tagID: roomID)
            }
    }
    
    //    MARK:- Changes in GroupDetail
    //    =============================
    func changesInGroupDetail(roomID: String){
        
        let handlerValue = DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID)
            .observe(.value) {[weak self](snapShot) in
                printDebug("snapShot: \(snapShot)")
                if snapShot.exists(),
                    let value = snapShot.value{
                    if let roomDetail = GroupDetail(json: JSON(value)){
                        self?.delegate?.changedInRoomData(detail: roomDetail)
                    }
                }
        }
        
        handler.append((FireBaseNode.tagsDetail.rawValue, roomID, handlerValue))
    }
    
    //    MARK:- Blocked User
    //    ====================
    func blockUnblockUser(isBlock: Bool, roomID: String, userID: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID)
            .child(FireBaseNode.members.rawValue)
            .child(userID)
            .child(FireBaseKeys.blocked.rawValue).setValue(isBlock)
    }
    
    //    MARK:- Change Member Type Status
    //    =================================
    func changeMemberType(roomID: String, userID: String, memberType: MemberType){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID)
            .child(FireBaseNode.members.rawValue)
            .child(userID)
            .child(FireBaseKeys.memberType.rawValue).setValue(memberType.rawValue)
        
        if memberType == .owner{
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.tagsDetail.rawValue)
                .child(roomID).updateChildValues([FireBaseKeys.ownerId.rawValue: userID])
            updateTagsOnUserID(id: userID, tagID: roomID)
            updateTagsOnUserID(id: UserProfile.main.userId, tagID: roomID)
        }
    }
    
    func changeOtherUserIDOnRoomInfo(members: [GroupMembers], tagID: String, ownerID: String){
        
        members.forEach { (mem) in
            
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.roomGroupIds.rawValue)
                .child(mem.id)
                .child(tagID).updateChildValues([FireBaseKeys.otherUserId.rawValue: ownerID])
            
        }
    }
    
//    MARK:- update Tags From UserID
//    =============================
    func updateTagsOnUserID(id: String, tagID: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(id).observeSingleEvent(of: .value) { (snapShot) in
                
                if snapShot.exists(), let value = snapShot.value {
                    let data = UserProfile(withFireBase: JSON(value))
                    
                    if data.userId == UserProfile.main.userId {
                        var remainingTags = data.myTags.components(separatedBy: ",")
                        if let idx = remainingTags.firstIndex(where: {$0 == tagID}){
                            remainingTags.remove(at: idx)
                        }
                        DataBaseReference.shared.dataBase
                            .child(FireBaseNode.users.rawValue)
                            .child(id)
                            .updateChildValues([FireBaseKeys.myTags.rawValue: remainingTags.joined(separator: ",")])
                    }else{
                        let tags = data.myTags.isEmpty ? tagID : data.myTags + "," + tagID
                        DataBaseReference.shared.dataBase
                            .child(FireBaseNode.users.rawValue)
                            .child(id)
                            .updateChildValues([FireBaseKeys.myTags.rawValue: tags])
                    }
                }
        }
    }
    
    
    //    MARK:- Mute a Member
    //    =====================
    func muteUnmuteMember(isMute: Bool,
                          roomID: String,
                          userID: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(userID)
            .child(roomID)
            .child(FireBaseKeys.mute.rawValue).setValue(isMute)
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID)
            .child(FireBaseNode.members.rawValue)
            .child(userID)
            .child(FireBaseKeys.mute.rawValue).setValue(isMute)
    }
    
    //    MARK:- Change Message Type
    //    ==========================
    func changeTagType(roomID: String, tagType: TagEntrance){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID)
            .child(FireBaseKeys.tagEntrance.rawValue).setValue(tagType.rawValue)
        
        if tagType == .publicType{
            
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.tagsDetail.rawValue)
                .child(roomID)
                .child(FireBaseKeys.verificationType.rawValue).setValue(PrivateTagType.none.rawValue)
            
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.tagsDetail.rawValue)
                .child(roomID)
                .child(FireBaseKeys.verificationData.rawValue).setValue("")
        }
    }
    
    //    MARK:- mute A Group
    //    ====================
    func muteAGroup(isMute: Bool, roomID: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomID)
            .child(FireBaseKeys.chatMute.rawValue).setValue(isMute)
    }
    
    //    MARK:- Pinned To Top a Group
    //    ============================
    func pinnedToTop(isPinned: Bool, roomID: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomID)
            .child(FireBaseKeys.pinned.rawValue).setValue(isPinned)
    }
    
    //    MARK:- Delete Group
    //    ====================
    func deleteGroup(roomID: String, members: [GroupMembers]){
        
        for mem in members {
            
            printDebug("mem1:\(mem.id)")
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.roomGroupIds.rawValue)
                .child(mem.id)
                .child(roomID)
                .observeSingleEvent(of: .value) {[weak self](snapShot) in
                    
                    guard let strongSelf = self else{ return }
                    if snapShot.exists(),
                        let value = snapShot.value{
                        
                        printDebug("key:\(snapShot.key)")
                        printDebug("jsonData:\(JSON(value))")
                        let jsonData = JSON(value)
                        let roominfo = ChatListing(json: jsonData)
                        
                        printDebug("mem2:\(mem.id)")
                        strongSelf.updateTotalReadCount(isReduce: true,
                                                        readMessagesCount: roominfo.unreadMessageCount,
                                                        roomData: roominfo,
                                                        userID: mem.id)
                        
                        DataBaseReference.shared.dataBase
                            .child(FireBaseNode.roomGroupIds.rawValue)
                            .child(mem.id)
                            .child(roomID)
                            .removeValue()
                        
                    }
            }
        }
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.messages.rawValue)
            .child(roomID).removeValue()
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID).removeValue {[weak self](error, refrence) in
                self?.delegate?.deleteGroup()
        }
    }
    
    //    MARK:- Exit From Group
    //    =======================
    func exitFromGroup(roomID: String,
                       isPinned: Bool){
        
        addmessage(text: UserProfile.main.userId,
                   tagID: roomID,
                   detail: nil,
                   messageType: .userLeft)
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomID)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                guard let strongSelf = self else{ return }
                if snapShot.exists(),
                    let value = snapShot.value{
                    let jsonData = JSON(value)
                    let roominfo = ChatListing(json: jsonData)
                    
                    strongSelf.updateTotalReadCount(isReduce: true,
                                                    readMessagesCount: roominfo.unreadMessageCount,
                                                    roomData: roominfo,
                                                    userID: UserProfile.main.userId)
                    
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.roomGroupIds.rawValue)
                        .child(UserProfile.main.userId)
                        .child(roomID).removeValue(completionBlock: { (error, refrence) in
                            
                            DataBaseReference.shared.dataBase
                                .child(FireBaseNode.tagsDetail.rawValue)
                                .child(roomID)
                                .child(FireBaseNode.members.rawValue)
                                .child(UserProfile.main.userId).removeValue {[weak self](error, refrence) in
                                    self?.delegate?.exitFromGroup(id: roomID, pinned: isPinned)
                            }
                        })
                }
        }
    }
    
    
    //    MARK:- Update tag Type.
    //    ======================
    func updateTagType(roomID: String,
                       tagType: TagEntrance,
                       privateTagType: PrivateTagType,
                       verificationData: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID)
            .child(FireBaseKeys.tagType.rawValue).setValue(tagType.rawValue)
        
        if tagType == .privateType{
            
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.tagsDetail.rawValue)
                .child(roomID)
                .child(FireBaseKeys.verificationType.rawValue).setValue(privateTagType.rawValue)
            
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.tagsDetail.rawValue)
                .child(roomID)
                .child(FireBaseKeys.verificationData.rawValue).setValue(verificationData)
        }
    }
    
    //    MARK:- Update Tag Title
    //    =======================
    func updateTagTitle(roomID: String, title: String, members: [GroupMembers]){
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID).updateChildValues([FireBaseKeys.tagName.rawValue: title])
        
        members.forEach { (mem) in
            
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.roomGroupIds.rawValue)
                .child(mem.id)
                .child(roomID)
                .updateChildValues([FireBaseKeys.roomName.rawValue: title])
        }
    }
    
    //    MARK:- Update Announcement
    //    =========================
    func updateAnnouncement(roomID: String, title: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID).updateChildValues([FireBaseKeys.announcement.rawValue: title])
    }
    
    //    MARK:- Update Description
    //    =========================
    func updateDesciption(roomID: String, title: String){
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID).updateChildValues([FireBaseKeys.description.rawValue: title])
    }
    
    //    MARK:- Update Tag Location
    //    ==========================
    func updateTagLocation(roomID: String, location: String, latitude: Double, longitude: Double){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomID).updateChildValues([FireBaseKeys.tagAddress.rawValue: location,
                                              FireBaseKeys.tagLatitude.rawValue: latitude,
                                              FireBaseKeys.tagLongitude.rawValue: longitude])
    }
    
    //    MARK:- Update Group Image
    //    =========================
    func updateGroupImage(imageUrl: String, tagDetail: GroupDetail?){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(tagDetail?.tagID ?? "").updateChildValues([FireBaseKeys.tagImageUrl.rawValue: imageUrl])
        
        tagDetail?.members.forEach { (mem) in
            
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.roomGroupIds.rawValue)
                .child(mem.id)
                .child(tagDetail?.tagID ?? "")
                .updateChildValues([FireBaseKeys.roomImage.rawValue: imageUrl])
        }
    }
    
    //    MARK:- Update Read Count
    //    =========================
    func updateReadCount(messsageData: MessageDetail){
        
        if messsageData.senderID != UserProfile.main.userId{
            
            DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
                .child(messsageData.roomID)
                .child(messsageData.messageID)
                .observeSingleEvent(of: .value, with: {(snapShot) in
                    
                    if snapShot.exists(), let value = snapShot.value{
                        let message = MessageDetail(json: JSON(value))
                        DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
                            .child(messsageData.roomID)
                            .child(messsageData.messageID)
                            .updateChildValues([FireBaseKeys.readCount.rawValue: (message.readCount + 1)])
                    }
                })
        }
    }
    
    //    MARK:- Any Change In Message
    //    =============================
    func checkMessageRead(messsageData: MessageDetail){
        
//        if messsageData.senderID == UserProfile.main.userId{
//
//            let handlerValue = DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
//                .child(messsageData.roomID)
//                .child(messsageData.messageID)
//                .observe(.value) {[weak self](snapShot) in
//
//                    if snapShot.exists(),
//                        let value = snapShot.value{
//
//                        let message = MessageDetail(json: JSON(value))
//                        if message.memberCount <= message.readCount{
//
//                            if let index = self?.messages.index(where: {$0.messageID == message.messageID}){
//                                self?.messages[index] = message
//                                self?.delegate?.messageRead(index: IndexPath(row: index, section: 0))
//                            }
//                        }
//                    }
//            }
//
//            handler.append((FireBaseNode.messages.rawValue, messsageData.roomID, handlerValue))
//        }
    }
    
    //    MARK:- UPDATE READ COUNT
    //    =========================
    func updateReadCount(tagInfo: GroupDetail?,
                         messagetType: MessageType = .text,
                         msg: String = ""){
        
        tagInfo?.members.forEach({ (member) in
            
            if member.id != UserProfile.main.userId{
                
                printDebug("Before ID: \(member.id)")
                DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
                    .child(member.id)
                    .child(tagInfo?.tagID ?? "")
                    .observeSingleEvent(of: .value, with: {[weak self](snapShot) in
                        
                        guard let strongSelf = self else{ return }
                        printDebug("updateReadCount: \(snapShot)")
                        if snapShot.exists(),
                            let value = snapShot.value{
                            let jsonData = JSON(value)
                            let roomInfo = ChatListing(json: jsonData)
                            printDebug("After ID: \(member.id)")
                            DataBaseReference.shared.dataBase
                                .child(FireBaseNode.roomGroupIds.rawValue)
                                .child(member.id)
                                .child(tagInfo?.tagID ?? "")
                                .updateChildValues([FireBaseKeys.unreadMessageCount.rawValue: (roomInfo.unreadMessageCount + 1)])
                            roomInfo.unreadMessageCount = roomInfo.unreadMessageCount + 1
                            strongSelf.updateTotalReadCount(isReduce: false,
                                                            readMessagesCount: 0,
                                                            roomData: roomInfo,
                                                            userID: member.id,
                                                            messageType: messagetType,
                                                            message: msg)
                        }
                    })
            }
        })
    }
    
    //    MARK:- Update total Read Count
    //    ==============================
    func updateTotalReadCount(isReduce: Bool,
                              readMessagesCount: Int,
                              roomData: ChatListing,
                              userID: String,
                              messageType: MessageType = .text,
                              message: String = ""){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(userID)
            .observeSingleEvent(of: .value) { (snapShot) in
                
                printDebug("updatetotalReadCount: \(snapShot)")
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    let userData = UserProfile(withFireBase: JSON(value))
                    var count: Int = 0
                    if isReduce{
                        let remainigCount = userData.totalUnreadCount - readMessagesCount
                        count = remainigCount > 0 ? remainigCount : 0
                    }else{
                        count = (userData.totalUnreadCount + 1)
                        self.delegate?.addTotalUnreadCount(otherUserData: userData,
                                                           type: messageType,
                                                           text: message,
                                                           roomInfo: roomData)
                        
                    }
                    
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.users.rawValue)
                        .child(userID)
                        .updateChildValues([FireBaseKeys.totalUnreadCount.rawValue: count])
                }
        }
    }
    
    //    MARK:- reduce a read Count
    //    ==========================
    func dimisnReadCount(tagID: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(tagID)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                guard let strongSelf = self else { return }
                
                if snapShot.exists(), let value = snapShot.value{
                    
                    let roomInfo = ChatListing(json: JSON(value))
                    strongSelf.updateTotalReadCount(isReduce: true,
                                                    readMessagesCount: roomInfo.unreadMessageCount,
                                                    roomData: roomInfo,
                                                    userID: UserProfile.main.userId)
                    
                    strongSelf.reduceReadCount(tagID: tagID)
                }
        }
    }
    
    //    MARK:- DIMINISH READ COUNT
    //    ==========================
    func reduceReadCount(tagID: String){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(tagID).updateChildValues([FireBaseKeys.unreadMessageCount.rawValue: 0])
    }
    
    //MARK:- Increase Pending Request Count
    //=====================================
    func increasePendingRequestCount(roomId: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(roomId).observeSingleEvent(of: .value) { (snapShot) in
                
                if snapShot.exists(), let value = snapShot.value{
                    if let detail = GroupDetail(json: JSON(value)){
                        DataBaseReference.shared.dataBase
                            .child(FireBaseNode.tagsDetail.rawValue)
                            .child(roomId).updateChildValues([FireBaseKeys.pendingRequestCount.rawValue: (detail.pendingRequestCount + 1)])
                    }
                }
        }
    }
    
    //    MARK:- Reduce Pending Request
    //    ==============================
    func reducePendingRequest(grouDetail: GroupDetail){
        
        if grouDetail.pendingRequestCount > 0 {
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.tagsDetail.rawValue)
                .child(grouDetail.tagID).updateChildValues([FireBaseKeys.pendingRequestCount.rawValue: (grouDetail.pendingRequestCount - 1)])
        }
    }
    
    //    MARK:- Get Paginated Data
    //    ==========================
    func getPaginatedData(roomId: String, msgTimeStamp: Int, createdTime: Int){

            if msgTimeStamp > createdTime{
                
                DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
                    .child(roomId)
                    .queryOrdered(byChild: FireBaseKeys.timeStamp.rawValue)
                    .queryEnding(atValue: msgTimeStamp)
                    .queryLimited(toLast: paginationData)
                    .observeSingleEvent(of: .value) {[weak self](snapshot) in
                        
                        guard let strongSelf = self else{
                            return
                        }
                        
                        printDebug("PaginatedData: \(snapshot)")
                        if snapshot.exists(),
                            let value = snapshot.value{
                            let messagesDictionary = JSON(value).dictionaryValue
                            let messagesIDs = Array(messagesDictionary.keys)
                            var messages: [MessageDetail] = []
                            
                            messagesIDs.forEach {(ids) in
                                let messageDetail = MessageDetail(json: messagesDictionary[ids] ?? JSON())
                                if msgTimeStamp > messageDetail.timeStamp{
                                    messages.append(messageDetail)
                                    
                                    if messageDetail.readCount < messageDetail.memberCount{
                                        strongSelf.updateReadCount(messsageData: messageDetail)
                                        strongSelf.checkMessageRead(messsageData: messageDetail)
                                    }
                                }
                            }
                            let sortedData = messages.sorted(by: {$0.timeStamp > $1.timeStamp})
                            strongSelf.delegate?.getPaginatedData(messages: sortedData)
//                            var indexArray: [IndexPath] = []
//                            if !sortedData.isEmpty{
//                                for index in 0...(sortedData.count - 1){
//                                    indexArray.append(IndexPath(row: index, section: 0))
//                                    strongSelf.messages.insert(sortedData[index], at: index)
//                                }
//                            }
                        }
                        self?.dimisnReadCount(tagID: roomId)
                }
            }
    }

    func getRoomData(roomID: String,
                     userID: String,
                     roomInfo: @escaping (_ info: ChatListing) -> ()){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
        .child(userID)
            .child(roomID).observeSingleEvent(of: .value) { (snapShot) in
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    let roomData = ChatListing(json: JSON(value))
                    roomInfo(roomData)
                }
        }
    }
    
    func removeMember(roomID: String,
                      userID: String,
                      isForRemoveMember: Bool,
                      success: @escaping (() -> ())){

        if isForRemoveMember{
            addmessage(text: userID,
                       tagID: roomID,
                       detail: nil,
                       messageType: .userRemove)
        }

        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(userID)
            .child(roomID)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                guard let strongSelf = self else{ return }
                if snapShot.exists(),
                    let value = snapShot.value{
                    let jsonData = JSON(value)
                    let roominfo = ChatListing(json: jsonData)
                    
                    strongSelf.updateTotalReadCount(isReduce: true,
                                                    readMessagesCount: roominfo.unreadMessageCount,
                                                    roomData: roominfo,
                                                    userID: userID)
                    
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.roomGroupIds.rawValue)
                        .child(userID)
                        .child(roomID).removeValue(completionBlock: {(error, refrence) in
                            
                            DataBaseReference.shared.dataBase
                                .child(FireBaseNode.tagsDetail.rawValue)
                                .child(roomID)
                                .child(FireBaseNode.members.rawValue)
                                .child(userID).removeValue {(error, refrence) in
                                   
                                   success()
                            }
                        })
                }
        }
    }
}



