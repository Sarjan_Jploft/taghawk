//
//  FireBaseKeys.swift
//  TagHawk
//
//  Created by Appinventiv on 19/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation

enum FireBaseNode: String {
    case users
    case roomGroupIds
    case lastMessage
    case messages
    case members
    case productInfo
    case tagsDetail
}

enum FireBaseKeys: String {
    
    case email
    case fullName
    case userId
    case invitationCode
    case refreshToken
    case accessToken
    case profilePicture
    case myTags
    case deviceToken
    case deviceType
    
    case roomId
    case roomImage
    case roomName
    case pinned
    case chatMute
    case productImage
    case chatType
    case userType
    case deleteChat
    case mute
    
    case lastUpdatedTimeStamp
    case messageId
    case messageText
    case senderId
    case receiverId
    case timeStamp
    case senderName
    case senderImage
    case messageStatus
    case otherUserId
    case messageReadTime
    case createdTimeStamp
    case messageType
    case productName
    case productId
    case productPrice
    case unreadMessageCount
    case pendingRequestCount
    case totalUnreadCount
    case shareId
    case shareImage
    
    case tagId
    case description
    case tagType
    case tagEntrance
    case tagStatus
    case tagName
    case tagAddress
    case tagImageUrl
    case tagLongitude
    case tagLatitude
    case shareCode
    case shareLink
    case verificationType
    case verificationData
    case blocked
    case memberType
    case memberId
    case memberName
    case memberImage
    case announcement
    case ownerId
    case memberCount
    case readCount
    
}

