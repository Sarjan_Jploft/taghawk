//
//  FireBaseChat.swift
//  TagHawk
//
//  Created by Appinventiv on 18/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth
import Firebase
import SwiftyJSON

protocol FireBaseChatDelegate: class {
    
    func error(msg: Error)
    func getMessageListing()
    func getNewMessage(isPinned: Bool)
    func getUpdatedRoomData(indexpath: IndexPath)
    func getTotalCount()
}

extension FireBaseChatDelegate {
    func error(msg: Error){}
    func getMessageListing(){}
    func getNewMessage(isPinned: Bool){}
    func getUpdatedRoomData(indexpath: IndexPath){}
    func getTotalCount(){}
}

final class DataBaseReference {
    
    static let shared = DataBaseReference()
    let dataBase = Database.database().reference()
//    let dataBase:DatabaseReference! = nil
    
    
    
}

extension Date {
    
    var unixFirebaseTimestamp: Int {
        return Int(ServerValue.timestamp()[".sv"] as! String) ?? self.millisecondsSince1970
    }
}

final class FireBaseChat {
    
    //    MARK:- Proporties
    //    =================
    static let shared = FireBaseChat()
    private let auth = Auth.auth()
    weak var delegate: FireBaseChatDelegate?
    var handler = [(String, String, UInt)]()
    var userData: UserProfile?
    var roomList: (pinnedData: [ChatListing], normalChat: [ChatListing]) = ([], [])
    var isListHavGroupData: Bool = false
    var isHeaderWithuserID: Bool = false
    
    
    func removerObservers(){
     
        handler.forEach { (handlerData) in
            DataBaseReference.shared.dataBase
                .child(handlerData.0)
                .child(handlerData.1).removeObserver(withHandle: handlerData.2)
        }
    }

    func createUserNode(data: UserProfile){
        var userDic = data.fireBaseDictionary
        userDic[FireBaseKeys.totalUnreadCount.rawValue] = 0
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(data.userId)
            .setValue(userDic)
    }
    
    private func updateUserData(data: UserProfile){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(data.userId)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                guard let strongSelf = self else { return }
                if snapShot.exists(), let value = snapShot.value{
                   let userData = UserProfile(withFireBase: JSON(value))
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.users.rawValue)
                        .child(data.userId)
                        .updateChildValues(userData.fireBaseDictionary)
                }else{
                  strongSelf.createUserNode(data: data)
                }
        }
    }
    
    //    MARK:- Update Device Token
    //    ==========================
    static func updateToken(){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(UserProfile.main.userId)
            .updateChildValues([FireBaseKeys.deviceToken.rawValue: DeviceDetail.fcmToken])
    }
    
    //MARK:- Get Rooms Data
    //    =================
    func getRoomsData(controller: GroupChatFireBaseController,
                      singleController: SingleChatFireBaseController){
       
        isListHavGroupData = false
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .observeSingleEvent(of: .value) {[weak self](snapshot) in
            
                if snapshot.exists(),
                    let value = snapshot.value{
                    let jsonDic = JSON(value).dictionaryValue
                    printDebug("jsonData:\(jsonDic)")
                    var pinChat: [ChatListing] = []
                    var normChat: [ChatListing] = []
                    let roomIDs = Array(jsonDic.keys)
                    roomIDs.forEach {(id) in
                        let chatData = ChatListing(json: jsonDic[id] ?? JSON())
                        if chatData.chatType == .group{
                            self?.isListHavGroupData = true
                            controller.getGroupData(groupID: chatData.roomID,
                                                    isPinned: chatData.pinned)
                            controller.changesInGroupDetail(roomID: chatData.roomID)
                            
                            switch chatData.lastMessage.messageType{
                                
                            case .ownershipTransfer, .tagCreatedHeader, .userJoin, .userLeft, .userRemove:
                                self?.isHeaderWithuserID = true
                            default:
                                break
                            }
                        }
                        self?.getLatesMessage()
                        if chatData.pinned{
                            pinChat.append(chatData)
                        }else{
                            normChat.append(chatData)
                        }
                    }
                    
                    let data = (pinnedData: pinChat.sorted(by: {$0.lastMessage.timeInterval > $1.lastMessage.timeInterval}),
                                normalChat: normChat.sorted(by: {$0.lastMessage.timeInterval > $1.lastMessage.timeInterval}))
                    self?.roomList = data
                }else{
                    self?.roomList = ([], [])
                }
                self?.delegate?.getMessageListing()
                self?.getNewRooms(timeStamp: Date().unixFirebaseTimestamp,
                                  groupController: controller)
        }
    }
    
    func removeObservers(){
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId).removeAllObservers()
        DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue).removeAllObservers()
    }
    
    //MARK:- Get new room
    //    =====================
    func getNewRooms(timeStamp: Int,
                     groupController: GroupChatFireBaseController){
        
        let handlerValue = DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .queryOrdered(byChild: FireBaseKeys.createdTimeStamp.rawValue)
            .queryStarting(atValue: timeStamp)
            .observe(.childAdded) {[weak self](snapShot) in
                
                guard let strongSelf = self else{
                    return
                }
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    let chatData = ChatListing(json: JSON(value))
                    if chatData.chatType == .group{
                        groupController.getGroupData(groupID: chatData.roomID,
                                                     isPinned: chatData.pinned)
                    }
                    
                    strongSelf.getLatesMessage()
                    
                    if !chatData.pinned{
                        if strongSelf.roomList.normalChat.isEmpty{
                            strongSelf.roomList.normalChat.insert(chatData, at: 0)
                        }else{
                            if strongSelf.roomList.normalChat.first?.createdTime ?? Date().millisecondsSince1970 < chatData.createdTime{
                                strongSelf.roomList.normalChat.insert(chatData, at: 0)
                            }
                        }
                        strongSelf.delegate?.getNewMessage(isPinned: chatData.pinned)
                    }
                }
        }
        
        handler.append((FireBaseNode.roomGroupIds.rawValue, UserProfile.main.userId, handlerValue))
    }
    
    //MARK:- get latest message corresponding to room
    //    ================================================
    func getLatesMessage(){
        
        let handlerValue = DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .observe(.childChanged) {[weak self](snapShot) in
                guard let strongSelf = self else{
                    return
                }
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    let chatData = ChatListing(json: JSON(value))
                    
                    if chatData.pinned{
                        if let idx = strongSelf.roomList.pinnedData.index(where: {$0.roomID == chatData.roomID}){
                            
                            if chatData.chatType == .group{
                                let data = strongSelf.roomList.pinnedData[idx]
                                chatData.lastMessage = data.lastMessage
                            }
                            let data = strongSelf.roomList.pinnedData[idx]
                            chatData.roomName = data.roomName
                            chatData.roomImage = data.roomImage
                            if idx != 0{
                                strongSelf.roomList.pinnedData.remove(at: idx)
                                strongSelf.roomList.pinnedData.insert(chatData, at: 0)
                            }else{
                                strongSelf.roomList.pinnedData[0] = chatData
                            }
                            strongSelf.delegate?.getUpdatedRoomData(indexpath: IndexPath(row: idx, section: 0))
                        }
                    }else{
                        if let idx = strongSelf.roomList.normalChat.index(where: {$0.roomID == chatData.roomID}){
                            if chatData.chatType == .group{
                                let data = strongSelf.roomList.normalChat[idx]
                                chatData.lastMessage = data.lastMessage
                            }
                            
                            let data = strongSelf.roomList.normalChat[idx]
                            chatData.roomName = data.roomName
                            chatData.roomImage = data.roomImage
                            
                            if idx != 0{
                                strongSelf.roomList.normalChat.remove(at: idx)
                                strongSelf.roomList.normalChat.insert(chatData, at: 0)
                            }else{
                                strongSelf.roomList.normalChat[0] = chatData
                            }
                            strongSelf.delegate?.getUpdatedRoomData(indexpath: IndexPath(row: idx, section: 1))
                        }
                    }
                }
        }
        
        handler.append((FireBaseNode.roomGroupIds.rawValue, UserProfile.main.userId, handlerValue))
    }
    
    func getTotalUnreadMessage(){
        
        let handlerValue = DataBaseReference.shared.dataBase.child(FireBaseNode.users.rawValue)
            .child(UserProfile.main.userId)
            .observe(.value) {[weak self](snapShot) in
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    self?.userData = UserProfile(withFireBase: JSON(value))
                    self?.delegate?.getTotalCount()
                }
        }
        
        handler.append((FireBaseNode.users.rawValue, UserProfile.main.userId, handlerValue))
    }
    
    func updateUserData(userData: UserProfile){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.users.rawValue)
            .child(UserProfile.main.userId)
            .updateChildValues([FireBaseKeys.fullName.rawValue: userData.fullName,
                                FireBaseKeys.profilePicture.rawValue: userData.profilePicture,
                                FireBaseKeys.email.rawValue: userData.email,
                                FireBaseKeys.userId.rawValue: UserProfile.main.userId])
    }
    
    private func getRoomsData(updatedUserData: UserProfile){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                guard let strongSelf = self else { return }
                if snapShot.exists(),
                    let value = snapShot.value{
                    let jsonData = JSON(value).dictionaryValue
                    let keys = Array(jsonData.keys)
                    keys.forEach({ (roomID) in
                        let roomData = ChatListing(json: jsonData[roomID] ?? JSON())
                        if roomData.chatType == .single{
                            strongSelf.updateRoomMemberData(userID: UserProfile.main.userId,
                                                            roomID: roomData.roomID,
                                                            userData: updatedUserData)
                            strongSelf.updateRoomMemberData(userID: roomData.otherUserID,
                                                            roomID: roomData.roomID,
                                                            userData: updatedUserData)
                        }else{
                            strongSelf.updateTagMemberData(tagID: roomData.roomID,
                                                           userData: updatedUserData)
                        }
                    })
                }
        }
    }
    
    private func updateRoomMemberData(userID: String,
                                      roomID: String,
                                      userData: UserProfile){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
            .child(userID)
            .child(roomID)
            .child(FireBaseNode.members.rawValue)
            .child(UserProfile.main.userId).updateChildValues([FireBaseKeys.fullName.rawValue: userData.fullName])
        if userID != UserProfile.main.userId{
            DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
                .child(userID)
                .child(roomID).updateChildValues([FireBaseKeys.roomName.rawValue: userData.fullName,
                                                  FireBaseKeys.roomImage.rawValue: userData.profilePicture])
        }
    }
    
    private func updateTagMemberData(tagID: String,
                                     userData: UserProfile){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.tagsDetail.rawValue)
            .child(tagID)
            .child(FireBaseNode.members.rawValue)
            .child(UserProfile.main.userId).updateChildValues([FireBaseKeys.memberName.rawValue: userData.fullName,
                                                               FireBaseKeys.memberImage.rawValue: userData.profilePicture])
        
    }
    
    func clearDeviceToken(userID: String){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.users.rawValue)
            .child(userID)
            .updateChildValues([FireBaseKeys.accessToken.rawValue: "",
                                FireBaseKeys.deviceToken.rawValue: ""])
    }
    
    func getUpdatedProductInfo(roomID: String,
                               indexPath: IndexPath,
                               info: @escaping ((_ productInfo: ProductData, _ index: IndexPath, _ rooId: String) -> Void)){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomID)
            .child(FireBaseNode.productInfo.rawValue)
            .observe(.value) { (snapShot) in
                
                print(snapShot)
                
                if snapShot.exists(),
                    let value = snapShot.value {
                    let data = ProductData(json: JSON(value))
                    info(data, indexPath, roomID)
                }
        }
    }
}
