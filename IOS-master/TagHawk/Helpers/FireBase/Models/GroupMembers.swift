//
//  Groupmembers.swift
//  TagHawk
//
//  Created by Appinventiv on 08/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

enum MemberType: Int {
    case owner = 1
    case admin = 2
    case member = 3
    
    init(rawString: Int){
        
        switch rawString {
        case MemberType.owner.rawValue: self = MemberType.owner
        case MemberType.admin.rawValue: self = MemberType.admin
        default: self = MemberType.member
        }
    }
    
    var localizedValue: String {
        switch self{
            
        case .admin: return LocalizedString.admin.localized
            
        case .owner: return LocalizedString.owner.localized
            
        case .member: return ""
            
        }
    }
}

struct GroupMembers {
    
    var id: String = ""
    var name: String = ""
    var isBlocked: Bool = false
    var isMute: Bool = false
    var image: String = ""
    var type: MemberType = .member
    var isGetData: Bool = false
    
    init() {
        
    }
    
    init(json: JSON) {
        
        id = json[FireBaseKeys.memberId.rawValue].stringValue
        name = json[FireBaseKeys.memberName.rawValue].stringValue
        isBlocked = json[FireBaseKeys.blocked.rawValue].boolValue
        isMute = json[FireBaseKeys.mute.rawValue].boolValue
        type = MemberType.init(rawString: json[FireBaseKeys.memberType.rawValue].intValue)
        image = json[FireBaseKeys.memberImage.rawValue].stringValue
    }
    
    static func arrayfromJSON(json: JSON) -> [GroupMembers]{
        let jsonDic = json.dictionaryValue
        let keys = Array(jsonDic.keys)
        
        var members: [GroupMembers] = []
        keys.forEach { (memID) in
            members.append(GroupMembers(json: jsonDic[memID] ?? JSON()))
        }
        return members
    }
}
