//
//  GroupDetail.swift
//  TagHawk
//
//  Created by Appinventiv on 08/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct GroupDetail {
    let tagID: String
    let tagImage: String
    let tagName: String
    var tagAddress: String
    var description: String
    var tagType: TagType
    var tagEntrance:TagEntrance
    var latitude: Double
    var longitude: Double
    let shareCode: String
    let sharelink: String
    let verificationType: PrivateTagType
    let verificationData: String
    var members: [GroupMembers] = []
    var searchedMembers: [GroupMembers] = []
    var lastMessage: MessageDetail
    let ownerID: String
    let announcement: String
    let pendingRequestCount: Int
    
    init?(json: JSON) {
        let id = json[FireBaseKeys.tagId.rawValue].stringValue
        guard !id.isEmpty else{ return nil }
        tagID = id
        tagImage = json[FireBaseKeys.tagImageUrl.rawValue].stringValue
        tagName = json[FireBaseKeys.tagName.rawValue].stringValue
        tagAddress = json[FireBaseKeys.tagAddress.rawValue].stringValue
        description = json[FireBaseKeys.description.rawValue].stringValue
        tagType = TagType.init(type: json[FireBaseKeys.tagType.rawValue].intValue)
        tagEntrance = TagEntrance.init(type: json[FireBaseKeys.tagEntrance.rawValue].intValue)
        longitude = json[FireBaseKeys.tagLongitude.rawValue].doubleValue
        latitude = json[FireBaseKeys.tagLatitude.rawValue].doubleValue
        shareCode = json[FireBaseKeys.shareCode.rawValue].stringValue
        sharelink = json[FireBaseKeys.shareLink.rawValue].stringValue
        verificationType = PrivateTagType(type: json[FireBaseKeys.verificationType.rawValue].intValue)
        verificationData = json[FireBaseKeys.verificationData.rawValue].stringValue
        members = GroupMembers.arrayfromJSON(json: json[FireBaseNode.members.rawValue])
        lastMessage = MessageDetail(json: json[FireBaseNode.lastMessage.rawValue])
        ownerID = json[FireBaseKeys.ownerId.rawValue].stringValue
        announcement = json[FireBaseKeys.announcement.rawValue].stringValue
        pendingRequestCount = json[FireBaseKeys.pendingRequestCount.rawValue].intValue
    }
}
