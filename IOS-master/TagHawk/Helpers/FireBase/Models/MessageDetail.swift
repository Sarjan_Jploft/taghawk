//
//  MessageDetail.swift
//  TagHawk
//
//  Created by Appinventiv on 25/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

enum MessageStatus: String {
    
    case delivered
    case read
    
    init(status: String){
        
        switch status {
            
        case MessageStatus.delivered.rawValue: self = MessageStatus.delivered
        case MessageStatus.read.rawValue: self = MessageStatus.read
            
        default: self = MessageStatus.delivered
        }
    }
}

enum MessageType: String {
    
    case text
    case image
    case productChangeHeader
    case userJoin
    case userRemove
    case userLeft
    case timeHeader
    case tagCreatedHeader
    case ownershipTransfer
    case shareProduct
    case shareCommunity
    case shelfProduct
    
    init(type: String){
        
        switch type {
            
        case MessageType.text.rawValue: self = MessageType.text
        case MessageType.image.rawValue: self = MessageType.image
        case MessageType.productChangeHeader.rawValue: self = MessageType.productChangeHeader
        case MessageType.userJoin.rawValue: self = MessageType.userJoin
        case MessageType.userRemove.rawValue: self = MessageType.userRemove
        case MessageType.userLeft.rawValue: self = MessageType.userLeft
        case MessageType.timeHeader.rawValue: self = MessageType.timeHeader
        case MessageType.tagCreatedHeader.rawValue: self = MessageType.tagCreatedHeader
        case MessageType.ownershipTransfer.rawValue: self = MessageType.ownershipTransfer
        case MessageType.shareProduct.rawValue: self = MessageType.shareProduct
        case MessageType.shareCommunity.rawValue: self = MessageType.shareCommunity
        case MessageType.shelfProduct.rawValue: self = MessageType.shelfProduct
            
        default: self = MessageType.text
        }
    }
}

class MessageDetail {
    let roomID: String
    var messageID: String
    var messageText: String
    var senderID: String
    var senderName: String
    var senderImage: String
    var timeStamp: Int
    var timeInterval: TimeInterval
    var messageStatus: MessageStatus
    var messageType: MessageType
    var readCount: Int
    var memberCount: Int
    var isImageMsgReceived: Bool = true
    var sharedID: String
    var sharedImage: String
    
    init(json: JSON) {
        roomID = json[FireBaseKeys.roomId.rawValue].stringValue
        messageID = json[FireBaseKeys.messageId.rawValue].stringValue
        messageText = json[FireBaseKeys.messageText.rawValue].stringValue
        senderID = json[FireBaseKeys.senderId.rawValue].stringValue
        timeStamp = json[FireBaseKeys.timeStamp.rawValue].intValue
        timeInterval = TimeInterval(timeStamp/1000)
        messageStatus = MessageStatus(status: json[FireBaseKeys.messageStatus.rawValue].stringValue)
        messageType = MessageType(type: json[FireBaseKeys.messageType.rawValue].stringValue)
        senderName = json[FireBaseKeys.senderName.rawValue].stringValue
        senderImage = json[FireBaseKeys.senderImage.rawValue].stringValue
        readCount = json[FireBaseKeys.readCount.rawValue].intValue
        memberCount = json[FireBaseKeys.memberCount.rawValue].intValue
        sharedID = json[FireBaseKeys.shareId.rawValue].stringValue
        sharedImage = json[FireBaseKeys.shareImage.rawValue].stringValue
    }
    
    static func fireBaseMessagesInArray(data: JSON) -> [MessageDetail]{
        let messagesDictionary = data.dictionaryValue
        let messagesIDs = Array(messagesDictionary.keys)
        var messages: [MessageDetail] = []
        messagesIDs.forEach { (ids) in
           messages.append(MessageDetail(json: messagesDictionary[ids] ?? JSON()))
        }
        return messages.sorted(by: {$0.timeStamp > $1.timeStamp})
    }
    
    static func getBlankImageMessageDetail() -> MessageDetail{
        
        let data = MessageDetail(json: JSON())
        data.senderID = UserProfile.main.userId
        data.senderName = UserProfile.main.fullName
        data.senderImage = UserProfile.main.profilePicture
        data.messageType = MessageType.image
        data.isImageMsgReceived = false
        data.timeStamp = Date().unixFirebaseTimestamp
        data.timeInterval = TimeInterval(data.timeStamp/1000)
        data.readCount = 1
        return data
    }
    
    var dictionary: JSONDictionary {
        
        let dic: JSONDictionary = [FireBaseKeys.roomId.rawValue: roomID,
                                   FireBaseKeys.messageId.rawValue: messageID,
                                   FireBaseKeys.messageText.rawValue: messageText,
                                   FireBaseKeys.senderId.rawValue: senderID,
                                   FireBaseKeys.timeStamp.rawValue: timeStamp,
                                   FireBaseKeys.messageStatus.rawValue: messageStatus.rawValue,
                                   FireBaseKeys.messageType.rawValue: messageType.rawValue,
                                   FireBaseKeys.senderName.rawValue: senderName,
                                   FireBaseKeys.senderImage.rawValue: senderImage,
                                   FireBaseKeys.readCount.rawValue: readCount,
                                   FireBaseKeys.memberCount.rawValue: memberCount,
                                   FireBaseKeys.shareId.rawValue: sharedID,
                                   FireBaseKeys.shareImage.rawValue: sharedImage]
        
        return dic
    }
    
    static func mergeTwoMessages(oldmessage: MessageDetail,
                                 newMessage: MessageDetail) -> MessageDetail{
        
        let message = oldmessage
        message.messageID = newMessage.messageID
        message.messageStatus = newMessage.messageStatus
        message.messageText = newMessage.messageText
        message.senderID = newMessage.senderID
        message.timeStamp = newMessage.timeStamp
        message.timeInterval = newMessage.timeInterval
        message.messageType = newMessage.messageType
        message.readCount = newMessage.readCount
        message.memberCount = newMessage.memberCount
        message.isImageMsgReceived = newMessage.isImageMsgReceived
        message.sharedID = newMessage.sharedID
        message.sharedImage = newMessage.sharedImage
        return message
    }
}
