//
//  SingleChatDetail.swift
//  TagHawk
//
//  Created by Appinventiv on 25/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

enum ChatType: String {
    case single
    case group
    
    init(type: String){
        
        switch type{
            
        case ChatType.single.rawValue: self = ChatType.single
        case ChatType.group.rawValue: self = ChatType.group
            
        default: self = ChatType.single
        }
    }
}

enum UserType : String {
    case guest = "1"
    case normal = "2"
    case hawkDriver = "3"
    case movingCompany = "4"
    
    init(type: String){
        
        switch type{
            
        case UserType.guest.rawValue: self = UserType.guest
        case UserType.normal.rawValue: self = UserType.normal
        case UserType.hawkDriver.rawValue: self = UserType.hawkDriver
        case UserType.movingCompany.rawValue: self = UserType.movingCompany
            
        default: self = UserType.normal
        }
    }
}


class ChatListing {

    let roomID: String
    var roomImage: String //other userImage in case of singleChat
    var roomName: String //Other userName in case of single chat
    var pinned: Bool
    var chatMute: Bool
    var userMute: Bool
    var lastMessage: MessageDetail
    var chatType: ChatType
    let userType: UserType
    let members: [Members]
    var groupMembers: [GroupMembers] = []
    let lastChatDeleteTimeStamp: Double
    var messageReadTime: TimeInterval
    let otherUserID: String
    var productData: ProductData?
    var unreadMessageCount: Int
    let createdTime: Int
    var userName: String = ""

    init(json: JSON) {
        
        roomID = json[FireBaseKeys.roomId.rawValue].stringValue
        roomImage = json[FireBaseKeys.roomImage.rawValue].stringValue
        roomName = json[FireBaseKeys.roomName.rawValue].stringValue
        pinned = json[FireBaseKeys.pinned.rawValue].boolValue
        chatMute = json[FireBaseKeys.chatMute.rawValue].boolValue
        userMute = json[FireBaseKeys.mute.rawValue].boolValue
        chatType = ChatType(type: json[FireBaseKeys.chatType.rawValue].stringValue)
        userType = UserType(type: json[FireBaseKeys.userType.rawValue].stringValue)
        lastMessage = MessageDetail(json: json[FireBaseNode.lastMessage.rawValue])
        members = Members.getMembersArray(memberInJSON: json[FireBaseNode.members.rawValue])
        lastChatDeleteTimeStamp = json[FireBaseKeys.deleteChat.rawValue].doubleValue
        messageReadTime = json[FireBaseKeys.messageReadTime.rawValue].doubleValue
        otherUserID = json[FireBaseKeys.otherUserId.rawValue].stringValue
        productData = ProductData(json: json[FireBaseNode.productInfo.rawValue])
        unreadMessageCount = json[FireBaseKeys.unreadMessageCount.rawValue].intValue
        createdTime = json[FireBaseKeys.createdTimeStamp.rawValue].intValue
    }
    
    static func instantiateRoomfromProductDetail(productData: Product) -> JSONDictionary{
        
        let dic: JSONDictionary = [FireBaseKeys.roomImage.rawValue: productData.profilePicture,
                                   FireBaseKeys.roomName.rawValue: productData.fullName,
                                   FireBaseKeys.pinned.rawValue: false,
                                   FireBaseKeys.chatMute.rawValue: false,
                                   FireBaseKeys.productImage.rawValue: productData.images.first?.image ?? "",
                                   FireBaseKeys.chatType.rawValue: ChatType.single.rawValue,
                                   FireBaseKeys.userType.rawValue: UserType.normal.rawValue,
                                   FireBaseKeys.otherUserId.rawValue: productData.userId,
                                   FireBaseKeys.unreadMessageCount.rawValue: 0]

        
        return dic
    }
    
    var roomInfo: JSONDictionary {
        
        var memberDic: JSONDictionary = [:]
        members.forEach { (mem) in
            memberDic[mem.userID] = mem.dictionary
        }

        let dic: JSONDictionary = [FireBaseKeys.roomId.rawValue: roomID,
                                   FireBaseKeys.roomImage.rawValue: roomImage,
                                   FireBaseKeys.roomName.rawValue: roomName,
                                   FireBaseKeys.pinned.rawValue: pinned,
                                   FireBaseKeys.chatMute.rawValue: chatMute,
                                   FireBaseKeys.mute.rawValue: userMute,
                                   FireBaseNode.lastMessage.rawValue: lastMessage.dictionary,
                                   FireBaseKeys.chatType.rawValue: chatType.rawValue,
                                   FireBaseKeys.userType.rawValue: userType.rawValue,
                                   FireBaseNode.members.rawValue: memberDic,
                                   FireBaseKeys.deleteChat.rawValue: lastChatDeleteTimeStamp,
                                   FireBaseKeys.messageReadTime.rawValue: messageReadTime,
                                   FireBaseKeys.otherUserId.rawValue: UserProfile.main.userId,
                                   FireBaseNode.productInfo.rawValue: productData?.dictionary ?? [:],
                                   FireBaseKeys.unreadMessageCount.rawValue: unreadMessageCount,
                                   FireBaseKeys.createdTimeStamp.rawValue: createdTime]
        
        return dic
    }
}

class Members {
    
    let userID: String
    let fullName: String
    
    init(json: JSON) {
        userID = json[FireBaseKeys.userId.rawValue].stringValue
        fullName = json[FireBaseKeys.fullName.rawValue].stringValue
    }
    
    class func getMembersArray(memberInJSON: JSON) -> [Members]{
        
        let dictionary = memberInJSON.dictionaryValue
        let keys = Array(dictionary.keys)
        var members: [Members] = []
        keys.forEach { (ids) in
            members.append(Members(json: dictionary[ids] ?? JSON()))
        }
        return members
    }
    
    var dictionary: JSONDictionary {
        let dic: JSONDictionary = [FireBaseKeys.userId.rawValue: userID,
                                   FireBaseKeys.fullName.rawValue: fullName]
        return dic
    }
}

class ProductData {
    
    let productName: String
    let productImage: String
    let productID: String
    let productPrice: Double
    
    init(json: JSON){
        productName = json[FireBaseKeys.productName.rawValue].stringValue
        productImage = json[FireBaseKeys.productImage.rawValue].stringValue
        productID = json[FireBaseKeys.productId.rawValue].stringValue
        productPrice = json[FireBaseKeys.productPrice.rawValue].doubleValue
    }
    
    var dictionary: JSONDictionary {
        
        let dic: JSONDictionary = [FireBaseKeys.productName.rawValue: productName,
                                   FireBaseKeys.productImage.rawValue: productImage,
                                   FireBaseKeys.productId.rawValue: productID,
                                   FireBaseKeys.productPrice.rawValue: productPrice]
        return dic
    }
    
    static func productInfoInDictionary(productData: Product) -> JSONDictionary{
        
        let dic: JSONDictionary = [FireBaseKeys.productName.rawValue: productData.title,
                                   FireBaseKeys.productImage.rawValue: productData.images.first?.thumbImage ?? "",
                                   FireBaseKeys.productId.rawValue: productData.productId,
                                   FireBaseKeys.productPrice.rawValue: Double(productData.firmPrice)]
        
        return dic
    }
    
    static func productDataInDictionary(productData: UserProduct) -> JSONDictionary{
        
        let dic: JSONDictionary = [FireBaseKeys.productName.rawValue: productData.productName,
                                   FireBaseKeys.productImage.rawValue: productData.images.first?.thumbImage ?? "",
                                   FireBaseKeys.productId.rawValue: productData.id,
                                   FireBaseKeys.productPrice.rawValue: productData.firmPrice]
        
        return dic
    }
}
