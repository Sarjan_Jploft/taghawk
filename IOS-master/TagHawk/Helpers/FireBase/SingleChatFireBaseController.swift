//
//  SingleChatFireBaseController.swift
//  TagHawk
//
//  Created by Appinventiv on 25/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase
import SwiftyJSON

protocol SingleChatFireBaseControllerDelegate: class {
    func getRoomData(info: ChatListing)
    func getMessageData(messageData: [MessageDetail])
    func getNewMessage(messageData: MessageDetail)
    func deleteRoom()
    func getImageMessage(messageData: MessageDetail)
    func updateMessageStatus(message: MessageDetail)
    func anotherUserRemoveRoom()
    func getPaginatedData(messages: [MessageDetail])
    func userBlocked()
    func userBlockedByAnother(roomData: ChatListing)
    func getUserData(userData: UserProfile, chatType: ChatType)
    func getUpdatedRoomData(info: ChatListing)
    func addTotalUnreadCount(otherUserData: UserProfile, type: MessageType, text: String, roomInfo: ChatListing?)
}

extension SingleChatFireBaseControllerDelegate {
    
    func getRoomData(info: ChatListing){}
    func getMessageData(messageData: [MessageDetail]){}
    func getNewMessage(messageData: MessageDetail){}
    func deleteRoom(){}
    func getImageMessage(messageData: MessageDetail){}
    func updateMessageStatus(message: MessageDetail){}
    func anotherUserRemoveRoom(){}
    func getPaginatedData(messages: [MessageDetail]){}
    func userBlocked(){}
    func userBlockedByAnother(roomData: ChatListing){}
    func getUserData(userData: UserProfile, chatType: ChatType){}
    func getUpdatedRoomData(info: ChatListing){}
    func addTotalUnreadCount(otherUserData: UserProfile, type: MessageType, text: String, roomInfo: ChatListing?){}
}

class SingleChatFireBaseController {
    
//    MARK:- Proporties
//    ==================
    static var shared = SingleChatFireBaseController()
    
    weak var delegate: SingleChatFireBaseControllerDelegate?
    let paginationData: UInt = 100
    var usersRoom = [(roomID: String, userID: String, isExist: Bool)]()
    var userDataHandler: UInt?
    var handler = [(String, String, UInt)]()
    
    func removeObservers(roomId: String, otherUserId: String){
        removeAllObservers(roomID: roomId, otherUserID: otherUserId)
    }
    
    
//    MARK:- Methods
//    ===============
    
    // MARK:- Create RoomID
//    ======================
    func createRoomID(firstId: String, secondId: String) -> String {
        let f = min(firstId, secondId)
        let s = f == firstId ? secondId : firstId
        return "\(f)-\(s)"
    }
    
//    MARK:- Create Room for both user
//    ================================
    func createSingleChat(roomData: ChatListing?,
                          data: Product?,
                          sharedProduct: Product?,
                          sharedTag: Tag?,
                          userProfile: UserProfile?,
                          text: String,
                          msgType: MessageType,
                          vcInstantiated: SingleChatInstantiateForm){
        
        let timeInterval = Date().unixFirebaseTimestamp
    
        var isOtherUserHavRoom: Bool = false
        var roomID: String = ""
        var otherUserID: String = ""
        switch vcInstantiated {
            
        case .messageList:
            guard let data = roomData else{ return }
            otherUserID = data.otherUserID
            roomID = createRoomID(firstId: UserProfile.main.userId, secondId: data.otherUserID)
            isOtherUserHavRoom = checkRoomExistence(roomID: roomID, userID: data.otherUserID)
            
        case .productDetail:
            if let productData = data{
               otherUserID = productData.userId
            }else{
               otherUserID = roomData?.otherUserID ?? ""
            }
            roomID = createRoomID(firstId: UserProfile.main.userId, secondId: otherUserID)
            isOtherUserHavRoom = checkRoomExistence(roomID: roomID, userID: otherUserID)
            
        case .userDetail:
            guard let userData = userProfile else{ return }
            otherUserID = userData.userId
            roomID = createRoomID(firstId: UserProfile.main.userId, secondId: userData.userId)
            isOtherUserHavRoom = checkRoomExistence(roomID: roomID, userID: userData.userId)
        }
        
        let myRoomExist = checkRoomExistence(roomID: roomID, userID: UserProfile.main.userId)
        
        if !myRoomExist{
            isRoomExist(userID: UserProfile.main.userId, roomID: roomID) {(chatData) in
                if chatData.roomID.isEmpty {
                    self.createRoomForUser(room: roomData,
                                           productData: data,
                                           shareProduct: sharedProduct,
                                           shareTag: sharedTag,
                                           userData: userProfile,
                                           msg: text,
                                           msgType: msgType,
                                           timeInterVal: timeInterval,
                                           instantited: vcInstantiated)
                }else{
                    if !isOtherUserHavRoom{
                        self.createRoomForUser2(roomInfo: chatData,
                                                productData: data,
                                                productShared: sharedProduct,
                                                tagShared: sharedTag,
                                                userData: userProfile,
                                                text: text,
                                                msgType: msgType,
                                                timeInterVal: timeInterval,
                                                vcInstantited: vcInstantiated)
                    }else{
                        self.updateProductInfo(instantitaed: vcInstantiated,
                                               product: data,
                                               roomInfo: roomData)
                        self.addmessage(roomID: chatData.roomID,
                                        otherUserId: chatData.otherUserID,
                                        text: text,
                                        productShared: sharedProduct,
                                        tagShared: sharedTag,
                                        messageType: msgType)
                        
                    }
                }
            }
        }else{
            if !isOtherUserHavRoom{
                createRoomForUser2(roomInfo: roomData,
                                   productData: data,
                                   productShared: sharedProduct,
                                   tagShared: sharedTag,
                                   userData: userProfile,
                                   text: text,
                                   msgType: msgType,
                                   timeInterVal: timeInterval,
                                   vcInstantited: vcInstantiated)
            }else{
                updateProductInfo(instantitaed: vcInstantiated,
                                  product: data,
                                  roomInfo: roomData)
                addmessage(roomID: roomID,
                           otherUserId: otherUserID,
                           text: text,
                           productShared: sharedProduct,
                           tagShared: sharedTag,
                           messageType: msgType)

            }
        }
    }
    
    private func updateProductInfo(instantitaed: SingleChatInstantiateForm,
                                   product: Product?,
                                   roomInfo: ChatListing?){
        
        if instantitaed == .productDetail,
            let productInfo = product,
            let roomData = roomInfo,
            roomData.productData?.productID != productInfo.productId{
            var productData = UserProduct()
            productData.id = productInfo.productId
            productData.productName = productInfo.title
            productData.firmPrice = productInfo.firmPrice
            productData.images = productInfo.images
            SingleChatFireBaseController.shared.updateProductInfo(product: productData,
                                                                  roomId: roomData.roomID,
                                                                  otherUserID: roomData.otherUserID)
            SingleChatFireBaseController.shared.addHeader(text: productInfo.title,
                                                          roomId: roomData.roomID)
        }
    }
    
    
//    MARK:- chatID is product owner id in case of single chat
//    ========================================================
    func checkRoomExist(otherUserID: String){
        
        let roomId = createRoomID(firstId: otherUserID,
                                  secondId: UserProfile.main.userId)
        
        isRoomExist(userID: UserProfile.main.userId,
                    roomID: roomId,
                    roomData: {[weak self] chatData in
                    
                        guard let strongSelf = self else { return }
                        
                        if !chatData.roomID.isEmpty{
                            strongSelf.changeInRoomInfo(roomID: roomId)
                            strongSelf.checkUserBlocked()
                        }
        })
        
        isRoomExist(userID: otherUserID,
                    roomID: roomId,
                    roomData: { chatData in
                        
        })
        
        checkUserDeleteRoom(userID: UserProfile.main.userId, roomID: roomId)
        checkUserDeleteRoom(userID: otherUserID, roomID: roomId)
    }
    
    // get index room existence Array in room Array
    func checkRoomExistIndex(roomId: String, userId: String) -> Int?{
        
        if let idx = usersRoom.firstIndex(where: {$0.roomID == roomId && $0.userID == userId}){
            return idx
        }
        return nil
    }
    
    // check room existence
    func checkRoomExistence(roomID: String, userID: String) -> Bool {
        
        if let idx = checkRoomExistIndex(roomId: roomID, userId: userID){
            return self.usersRoom[idx].isExist
        }
        return false
    }
    
    func isRoomExist(userID: String,
                     roomID: String,
                     roomData: @escaping ((_ info: ChatListing) ->())){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(userID).child(roomID)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                guard let strongSelf = self else { return }
                
                if let idx = strongSelf.checkRoomExistIndex(roomId: roomID, userId: userID){
                    strongSelf.usersRoom[idx].isExist = snapShot.exists()
                }else{
                    let data: (roomID: String, userID: String, isExist: Bool) = (roomID: roomID, userID: userID, isExist: snapShot.exists())
                    strongSelf.usersRoom.append(data)
                }
                
                if snapShot.exists(), let value = snapShot.value {
                    let roomInfo = ChatListing(json: JSON(value))
                    if userID != UserProfile.main.userId {
                       roomData(roomInfo)
                    }else{
                        self?.delegate?.getRoomData(info: roomInfo)
                        roomData(roomInfo)
                    }
                }else{
                    roomData(ChatListing(json: JSON()))
                }
        }
    }
    
    private func createRoomForUser(room: ChatListing?,
                                   productData: Product?,
                                   shareProduct: Product?,
                                   shareTag: Tag?,
                                   userData: UserProfile?,
                                   msg: String,
                                   msgType: MessageType,
                                   timeInterVal: Int,
                                   instantited: SingleChatInstantiateForm){
        
        var roomID: String = ""
        var roomData: JSONDictionary = [:]
        var otherUserID: String = ""
        var members: JSONDictionary = [UserProfile.main.userId: [FireBaseKeys.userId.rawValue: UserProfile.main.userId,
                                                                 FireBaseKeys.fullName.rawValue: UserProfile.main.fullName]]
        switch instantited {
            
        case .productDetail:
            
            if let data = productData {
                roomID = createRoomID(firstId: UserProfile.main.userId,
                                      secondId: data.userId)
                otherUserID = data.userId
                roomData = ChatListing.instantiateRoomfromProductDetail(productData: data)
                let productInfo = ProductData.productInfoInDictionary(productData: data)
                members[data.userId] = [FireBaseKeys.userId.rawValue: data.userId,
                                        FireBaseKeys.fullName.rawValue: data.fullName]
                roomData[FireBaseNode.productInfo.rawValue] = productInfo
            }
        case .userDetail:
            if let data = userData {
                otherUserID = userData?.userId ?? ""
                roomID = createRoomID(firstId: UserProfile.main.userId,
                                      secondId: userData?.userId ?? "")
                roomData = data.roomDictionary
                members[data.userId] = [FireBaseKeys.userId.rawValue: data.userId,
                                        FireBaseKeys.fullName.rawValue: data.fullName]
                roomData[FireBaseNode.productInfo.rawValue] = [:]
            }
        case .messageList:
            otherUserID = room?.otherUserID ?? ""
        }
        
        roomData[FireBaseKeys.createdTimeStamp.rawValue] = timeInterVal
        roomData[FireBaseKeys.roomId.rawValue] = roomID
        roomData[FireBaseNode.members.rawValue] = members
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomID).setValue(roomData)
        
        let roomInfo = ChatListing(json: JSON(roomData))
        self.delegate?.getRoomData(info: roomInfo)
        
        if let idx = checkRoomExistIndex(roomId: roomID, userId: UserProfile.main.userId){
            usersRoom[idx].isExist = true
        }else{
            let data: (roomID: String, userID: String, isExist: Bool) = (roomID: roomID, userID: UserProfile.main.userId, isExist: true)
            usersRoom.append(data)
        }
        
        self.changeInRoomInfo(roomID: roomInfo.roomID)
        self.checkUserBlocked()
        
        let isOtherUserRoomExist = checkRoomExistence(roomID: roomID, userID: otherUserID)
        
        if !isOtherUserRoomExist{
            isRoomExist(userID: otherUserID,
                        roomID: roomID) {[weak self](chatData) in
                            guard let strongSelf = self else{ return }
                            
                            if chatData.roomID.isEmpty{
                                strongSelf.createRoomForUser2(roomInfo: roomInfo,
                                                              productData: productData,
                                                              productShared: shareProduct,
                                                              tagShared: shareTag,
                                                              userData: userData,
                                                              text: msg,
                                                              msgType: msgType,
                                                              timeInterVal: timeInterVal,
                                                              vcInstantited: instantited)
                            }else{
                                strongSelf.updateProductInfo(instantitaed: instantited,
                                                             product: productData,
                                                             roomInfo: roomInfo)
                                strongSelf.addmessage(roomID: chatData.roomID,
                                                      otherUserId: roomInfo.otherUserID,
                                                      text: msg,
                                                      productShared: shareProduct,
                                                      tagShared: shareTag,
                                                      messageType: msgType)
                            }
            }
        }else{
            self.updateProductInfo(instantitaed: instantited,
                                   product: productData,
                                   roomInfo: roomInfo)
            self.addmessage(roomID: roomID,
                            otherUserId: roomInfo.otherUserID,
                            text: msg,
                            productShared: shareProduct,
                            tagShared: shareTag,
                            messageType: msgType)
//            self.getNewMessage()
        }
    }
    
    //MARK:- Create Room on another user node
//    =======================================
    private func createRoomForUser2(roomInfo: ChatListing?,
                                    productData: Product?,
                                    productShared: Product?,
                                    tagShared: Tag?,
                                    userData: UserProfile?,
                                    text: String,
                                    msgType: MessageType,
                                    timeInterVal: Int,
                                    vcInstantited: SingleChatInstantiateForm){
        
        var members: JSONDictionary = [:]
        var productInfo: JSONDictionary = [:]
        var roomID: String = ""
        var otherUserID: String = ""
        
        switch vcInstantited {
            
        case .productDetail:
            
            members[UserProfile.main.userId] = [FireBaseKeys.userId.rawValue: UserProfile.main.userId,
                                                FireBaseKeys.fullName.rawValue: UserProfile.main.fullName]
            
            if let data = productData{
                
                otherUserID = data.userId
                
                members[data.userId] = [FireBaseKeys.userId.rawValue: data.userId,
                                        FireBaseKeys.fullName.rawValue: data.fullName]
                productInfo = ProductData.productInfoInDictionary(productData: data)
                roomID = createRoomID(firstId: UserProfile.main.userId, secondId: data.userId)
            }else{
                roomID = roomInfo?.roomID ?? ""
                otherUserID = roomInfo?.otherUserID ?? ""
                productInfo = roomInfo?.productData?.dictionary ?? [:]
                let data = roomInfo?.members.filter({$0.userID != UserProfile.main.userId}).first
                members[roomInfo?.otherUserID ?? ""] = [FireBaseKeys.userId.rawValue: data?.userID ?? "",
                                                        FireBaseKeys.fullName.rawValue: data?.fullName ?? ""]
            }
        case .userDetail:
            guard let info = roomInfo else{
                return
            }
            
            members[UserProfile.main.userId] = [FireBaseKeys.userId.rawValue: UserProfile.main.userId,
                                                FireBaseKeys.fullName.rawValue: UserProfile.main.fullName]
            members[info.otherUserID] = [FireBaseKeys.userId.rawValue: info.otherUserID,
                                         FireBaseKeys.fullName.rawValue: info.roomName]
            productInfo = [FireBaseKeys.productName.rawValue: info.productData?.productName ?? "",
                           FireBaseKeys.productImage.rawValue: info.productData?.productImage ?? "",
                           FireBaseKeys.productId.rawValue: info.productData?.productID ?? "" ,
                           FireBaseKeys.productPrice.rawValue: info.productData?.productPrice ?? 0.0]
            roomID = info.roomID
            otherUserID = info.otherUserID
        case .messageList:
            guard let info = roomInfo else{
                return
            }
            otherUserID = info.otherUserID
            roomID = createRoomID(firstId: UserProfile.main.userId, secondId: otherUserID)
            
            members[UserProfile.main.userId] = [FireBaseKeys.userId.rawValue: UserProfile.main.userId,
                                                FireBaseKeys.fullName.rawValue: UserProfile.main.fullName]
            members[info.otherUserID] = [FireBaseKeys.userId.rawValue: info.otherUserID,
                                         FireBaseKeys.fullName.rawValue: info.roomName]
            
            productInfo = [FireBaseKeys.productName.rawValue: info.productData?.productName ?? "",
                           FireBaseKeys.productImage.rawValue: info.productData?.productImage ?? "",
                           FireBaseKeys.productId.rawValue: info.productData?.productID ?? "" ,
                           FireBaseKeys.productPrice.rawValue: info.productData?.productPrice ?? 0.0]
            roomID = info.roomID
            otherUserID = info.otherUserID
        }
    
        let roomData: JSONDictionary = [FireBaseKeys.roomId.rawValue: roomID,
                                        FireBaseKeys.roomImage.rawValue: UserProfile.main.profilePicture,
                                        FireBaseKeys.roomName.rawValue: UserProfile.main.fullName,
                                        FireBaseKeys.otherUserId.rawValue: UserProfile.main.userId,
                                        FireBaseKeys.pinned.rawValue: false,
                                        FireBaseKeys.chatMute.rawValue: false,
                                        FireBaseKeys.chatType.rawValue: ChatType.single.rawValue,
                                        FireBaseKeys.userType.rawValue: UserType.normal.rawValue,
                                        FireBaseKeys.messageReadTime.rawValue: timeInterVal,
                                        FireBaseKeys.createdTimeStamp.rawValue: timeInterVal,
                                        FireBaseNode.members.rawValue: members,
                                        FireBaseNode.productInfo.rawValue: productInfo,
                                        FireBaseKeys.unreadMessageCount.rawValue: 0]
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(otherUserID)
            .child(roomID)
            .setValue(roomData)
        
        let isOtherUserRoomExist = checkRoomExistence(roomID: roomID, userID: otherUserID)
        
        if !isOtherUserRoomExist{
            self.updateProductInfo(instantitaed: vcInstantited,
                                   product: productData,
                                   roomInfo: roomInfo)
            
            self.addmessage(roomID: roomID,
                            otherUserId: otherUserID,
                            text: text,
                            productShared: productShared,
                            tagShared: tagShared,
                            messageType: msgType)
//            self.getNewMessage()
        }
        
        if let idx = checkRoomExistIndex(roomId: roomID, userId: otherUserID){
            usersRoom[idx].isExist = true
        }else{
            let data: (roomID: String, userID: String, isExist: Bool) = (roomID: roomID, userID: otherUserID, isExist: true)
            usersRoom.append(data)
        }
    }

//    MARK:- Update RoomData
//    ======================
    private func updateRoomData(roomData: ChatListing?,
                                product: Product){
        
        guard let data = roomData else{
            return
        }
        
        let updatedRoomDataAtUser1 = [FireBaseKeys.roomImage.rawValue: product.profilePicture,
                                      FireBaseKeys.roomName.rawValue: product.fullName,
                                      FireBaseKeys.productImage.rawValue: product.images.first?.image ?? "",
                                      FireBaseKeys.chatType.rawValue: ChatType.single.rawValue]
        
        let updatedRoomDataAtUser2 = [FireBaseKeys.roomImage.rawValue: UserProfile.main.profilePicture,
                                      FireBaseKeys.roomName.rawValue: UserProfile.main.fullName,
                                      FireBaseKeys.productImage.rawValue: product.images.first?.image ?? "",
                                      FireBaseKeys.chatType.rawValue: ChatType.single.rawValue]
        
        let groupNode = DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
        
        let isUserRoomExist = checkRoomExistence(roomID: data.roomID, userID: UserProfile.main.userId)
        
        if isUserRoomExist{
            groupNode.child(UserProfile.main.userId)
                .child(data.roomID)
                .updateChildValues(updatedRoomDataAtUser1)
        }
        
        let isOtherUserRoomExist = checkRoomExistence(roomID: data.roomID, userID: product.userId)
        
        if isOtherUserRoomExist{
            groupNode.child(product.userId)
                .child(data.roomID)
                .updateChildValues(updatedRoomDataAtUser2)
        }
    }
    
    func addmessage(roomID: String,
                    otherUserId: String,
                    text: String,
                    productShared: Product?,
                    tagShared: Tag?,
                    messageType: MessageType){

        let messageNode = DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
        let messageAutoID = messageNode.child(roomID).childByAutoId()
        
        var messageData : JSONDictionary = [FireBaseKeys.roomId.rawValue: roomID,
                                            FireBaseKeys.senderId.rawValue: UserProfile.main.userId,
                                            FireBaseKeys.senderName.rawValue: UserProfile.main.fullName,
                                            FireBaseKeys.senderImage.rawValue: UserProfile.main.profilePicture,
                                            FireBaseKeys.timeStamp.rawValue: Date().unixFirebaseTimestamp,
                                            FireBaseKeys.messageStatus.rawValue: MessageStatus.delivered.rawValue,
                                            FireBaseKeys.messageType.rawValue: messageType.rawValue]
        
        messageData[FireBaseKeys.messageId.rawValue] = messageAutoID.key ?? ""
        
        switch messageType {
            
        case .shareProduct:
            messageData[FireBaseKeys.messageText.rawValue] = productShared?.title ?? ""
            messageData[FireBaseKeys.shareId.rawValue] = productShared?.productId
            messageData[FireBaseKeys.shareImage.rawValue] = productShared?.images.first?.image ?? ""
        case .shareCommunity:
            messageData[FireBaseKeys.messageText.rawValue] = tagShared?.name ?? ""
            messageData[FireBaseKeys.shareId.rawValue] = tagShared?.id ?? ""
            messageData[FireBaseKeys.shareImage.rawValue] = tagShared?.tagImageUrl ?? ""
        default:
            messageData[FireBaseKeys.messageText.rawValue] = text
            messageData[FireBaseKeys.shareId.rawValue] = ""
            messageData[FireBaseKeys.shareImage.rawValue] = ""
        }
        
        updateReadCount(otherUserID: otherUserId, roomID: roomID)
        updateTotalReadCount(isReduce: false,
                             readMessagesCount: 0,
                             userID: otherUserId,
                             messageType: messageType,
                             msg: text)
        
        if messageType != .productChangeHeader{
            updateLastMessage(roomId: roomID,
                              messageData: messageData,
                              userID: UserProfile.main.userId)
            updateLastMessage(roomId: roomID,
                              messageData: messageData,
                              userID: otherUserId)
        }
       
        
        messageAutoID.setValue(messageData)
        
        if messageType == .image {
            let message = MessageDetail(json: JSON(messageData))
            message.isImageMsgReceived = false
            delegate?.getImageMessage(messageData: message)
        }
    }
    
    //MARK:- add time message on message node to show header of time
//    ==============================================================
    func addHeader(text: String, roomId: String){

        let messageNode = DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
        let messageAutoID = messageNode.child(roomId).childByAutoId()
        
        var messageData : JSONDictionary = [FireBaseKeys.roomId.rawValue: roomId,
                                            FireBaseKeys.messageText.rawValue: text,
                                            FireBaseKeys.senderId.rawValue: UserProfile.main.userId,
                                            FireBaseKeys.senderName.rawValue: UserProfile.main.fullName,
                                            FireBaseKeys.senderImage.rawValue: UserProfile.main.profilePicture,
                                            FireBaseKeys.timeStamp.rawValue: Date().unixFirebaseTimestamp,
                                            FireBaseKeys.messageStatus.rawValue: MessageStatus.delivered.rawValue,
                                            FireBaseKeys.messageType.rawValue: MessageType.productChangeHeader.rawValue]
        
        messageData[FireBaseKeys.messageId.rawValue] = messageAutoID.key
        messageAutoID.setValue(messageData)
    }
    
    //MARK:- update last message node
//    ===============================
    private func updateLastMessage(roomId: String,
                                   messageData: JSONDictionary,
                                   userID: String){
        
        let roomIDNode = DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(userID)
            .child(roomId)
            .observeSingleEvent(of: .value) {(snapshot) in
                
                if !snapshot.exists(){
                    roomIDNode.child(userID)
                        .child(roomId)
                        .child(FireBaseNode.lastMessage.rawValue).setValue(messageData)
                }else{
                    roomIDNode.child(userID)
                        .child(roomId)
                        .child(FireBaseNode.lastMessage.rawValue).updateChildValues(messageData)
                }
        }
    }
    
    //MARK:-  Update Pinned to chat value
//    ====================================
    func updatePinnedToChat(isPinned: Bool, roomID: String){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomID)
            .child(FireBaseKeys.pinned.rawValue)
            .setValue(isPinned)
    }
    
    //MARK:-  Update Chat to mute value
//    =================================
    func updateChatToMute(isMute: Bool, roomID: String){
        DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomID)
            .child(FireBaseKeys.chatMute.rawValue)
            .setValue(isMute)
    }
    
//    MARK:- Delete room
//    ==================
    func deleteChat(roomID: String,
                    otherUserID: String){
        
        if let idx = usersRoom.firstIndex(where: {$0.roomID == roomID && $0.userID == otherUserID}){
            usersRoom.remove(at: idx)
        }
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomID)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    let roomInfo = ChatListing(json: JSON(value))
                    self?.updateTotalReadCount(isReduce: true,
                                               readMessagesCount: roomInfo.unreadMessageCount,
                                               userID: UserProfile.main.userId)
                    
                    
                    
                }
                
                DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
                    .child(UserProfile.main.userId)
                    .child(roomID)
                    .removeValue(completionBlock: {[weak self](error, dataBase) in
                        printDebug(dataBase)
                        guard let err = error else{
                            return
                        }
                        printDebug(err)
                    })
                
                DataBaseReference.shared.dataBase.child(FireBaseNode.roomGroupIds.rawValue)
                    .child(otherUserID)
                    .child(roomID)
                    .observeSingleEvent(of: .value) {[weak self](snapShot) in
                        
                        guard let strongSelf = self else{ return }
                        if !snapShot.exists(){
                            strongSelf.removeMessageNode(id: roomID)
                        }
                        self?.delegate?.deleteRoom()
                }
        }
    }
    
//    MARK:- Get messages
//    ===================
    func getMessages(roomID: String, timeStamp: Int){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
            .child(roomID)
            .queryOrdered(byChild: FireBaseKeys.timeStamp.rawValue)
            .queryEnding(atValue: Date().unixFirebaseTimestamp)
            .queryLimited(toLast: paginationData)
            .queryStarting(atValue: timeStamp)
            .observeSingleEvent(of: .value) {[weak self](snapshot) in
                
                printDebug(snapshot)
                var messageData: [MessageDetail] = []
                if snapshot.exists(),
                    let value = snapshot.value {
                    let messagesDictionary = JSON(value).dictionaryValue
                    let messagesIDs = Array(messagesDictionary.keys)
                    var messages: [MessageDetail] = []
                    messagesIDs.forEach {(ids) in
                        let messageDetail = MessageDetail(json: messagesDictionary[ids] ?? JSON())
                        messages.append(messageDetail)
                        self?.messageStatusChanged(messageData: messageDetail, roomId: messageDetail.roomID)
                        self?.updateUserReadStatus(messageData: messageDetail, roomId: messageDetail.roomID)
                    }
                    messageData = messages.sorted(by: {$0.timeStamp < $1.timeStamp})
                }
                
                self?.delegate?.getMessageData(messageData: messageData)
                self?.diminishReadCount(roomID: roomID,
                                        userID: UserProfile.main.userId,
                                        isUserBlocked: false,
                                        success: {})
        }
    }
    
//    MARK:- Get new Message
//    =======================
    func getNewMessage(msgTimeStamp: Int,
                       roomId: String){
        
        let handlerValue = DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
            .child(roomId)
            .queryOrdered(byChild: FireBaseKeys.timeStamp.rawValue)
            .queryStarting(atValue: msgTimeStamp)
            .observe(.childAdded) {[weak self](snapshot) in
                printDebug(snapshot)
                if snapshot.exists(),
                    let value = snapshot.value {
                    let jsonData = JSON(value)
                    let message = MessageDetail(json: jsonData)
                    self?.delegate?.getNewMessage(messageData: message)
                }
        }
        handler.append((FireBaseNode.messages.rawValue, roomId, handlerValue))
    }
    
//    MARK:- Update ProductInfo
//    ==========================
    func updateProductInfo(product: UserProduct, roomId: String, otherUserID: String){
        
            let productData = ProductData.productDataInDictionary(productData: product)
            
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.roomGroupIds.rawValue)
                .child(otherUserID)
                .child(roomId)
                .child(FireBaseNode.productInfo.rawValue)
                .updateChildValues(productData)
            
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.roomGroupIds.rawValue)
                .child(UserProfile.main.userId)
                .child(roomId)
                .child(FireBaseNode.productInfo.rawValue)
                .updateChildValues(productData)
    }
    
//    MARK:- Remove an observer on New messages
//    ========================================
    private func removeAllObservers(roomID: String, otherUserID: String){
        
        handler.forEach { (handlerVal) in
            DataBaseReference.shared.dataBase.child(handlerVal.0).child(handlerVal.1).removeObserver(withHandle: handlerVal.2)
        }
        
        if !roomID.isEmpty {
            DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
                .child(roomID).removeAllObservers()
        }
        if !otherUserID.isEmpty{
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.roomGroupIds.rawValue)
                .child(otherUserID).removeAllObservers()
        }
    }
    
//    MARK:- Get Paginated Data
//    ==========================
    func getPaginatedData(roomId: String, msgTimeStamp: Int){
        
        DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
            .child(roomId)
            .queryOrdered(byChild: FireBaseKeys.timeStamp.rawValue)
            .queryEnding(atValue: msgTimeStamp)
            .queryLimited(toLast: paginationData)
            .observeSingleEvent(of: .value) {[weak self](snapshot) in
                
                guard let strongSelf = self else{
                    return
                }
                
                printDebug("PaginatedData: \(snapshot)")
                if snapshot.exists(),
                    let value = snapshot.value{
                    let messagesDictionary = JSON(value).dictionaryValue
                    let messagesIDs = Array(messagesDictionary.keys)
                    var messages: [MessageDetail] = []
                    messagesIDs.forEach {(ids) in
                        let messageDetail = MessageDetail(json: messagesDictionary[ids] ?? JSON())
                        if msgTimeStamp > messageDetail.timeStamp{
                            messages.append(messageDetail)
                            strongSelf.messageStatusChanged(messageData: messageDetail,
                                                            roomId: messageDetail.roomID)
                            strongSelf.updateUserReadStatus(messageData: messageDetail,
                                                            roomId: messageDetail.roomID)
                        }
                    }
                    
                    
                    let sortedData = messages.sorted(by: {$0.timeStamp < $1.timeStamp})
                    strongSelf.delegate?.getPaginatedData(messages: sortedData)
                }
        }
    }
    
//    MARK:- Update Message read update status on another user node
//    =============================================================
    func updateUserReadStatus(messageData: MessageDetail, roomId: String){
        if messageData.senderID != UserProfile.main.userId,
            messageData.messageStatus == .delivered{
            DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
                .child(roomId)
                .child(messageData.messageID)
                .updateChildValues([FireBaseKeys.messageStatus.rawValue: MessageStatus.read.rawValue])
        }
    }
    
//    MARK:- Message Status Changed
//    =============================
    func messageStatusChanged(messageData: MessageDetail, roomId: String){
        
        if messageData.senderID == UserProfile.main.userId &&
            messageData.messageStatus == .delivered{
            let handlerValue = DataBaseReference.shared.dataBase.child(FireBaseNode.messages.rawValue)
                .child(roomId)
                .observe(.childChanged) {[weak self](snapShot) in
                    printDebug(snapShot)
                    guard let strongSelf = self else{
                        return
                    }
                    if snapShot.exists(),
                        let value = snapShot.value{
                        let message = MessageDetail(json: JSON(value))
                        strongSelf.delegate?.updateMessageStatus(message: message)
                    }
            }
            handler.append((FireBaseNode.messages.rawValue, roomId, handlerValue))
        }
    }

//    MARK:- Updated Message UnreadCount
//    ==================================
    func updateReadCount(otherUserID: String, roomID: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(otherUserID)
            .child(roomID)
            .observeSingleEvent(of: .value) {(snapShot) in
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    let jsonData = JSON(value)
                    printDebug(jsonData)
                    let roomInfo = ChatListing(json: jsonData)
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.roomGroupIds.rawValue)
                        .child(otherUserID)
                        .child(roomID)
                        .updateChildValues([FireBaseKeys.unreadMessageCount.rawValue: roomInfo.unreadMessageCount + 1])
                }
        }
    }
    
//    MARK:- Get User data
//    ====================
    func getUserInfo(userID: String,
                     indexPath: IndexPath,
                     success: @escaping ((_ index: IndexPath, _ userData: UserProfile) -> ())){
        
        if userID.isEmpty{return}
        
        let handlerValue = DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(userID)
            .observe(.value) {[weak self](snapShot) in
                
                guard let strongSelf = self else { return }
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    let data = UserProfile(withFireBase: JSON(value))
                    success(indexPath, data)
                }
        }
        handler.append((FireBaseNode.users.rawValue, userID, handlerValue))
        
    }
    
    func getUserInfoWithSingleEvent(userID: String,
                                    success: @escaping ((_ userData: UserProfile) -> ())){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(userID)
            .observeSingleEvent(of: .value, with: {[weak self](snapShot) in
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    let data = UserProfile(withFireBase: JSON(value))
                    success(data)
                }
            })
    }
    
    
    func getUserData(userID: String, chatType: ChatType){
        
        let handlerValue = DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(userID)
            .observe(.value) {[weak self](snapShot) in
                
                guard let strongSelf = self else { return }
                
                if snapShot.exists(),
                    let value = snapShot.value{
                    let data = UserProfile(withFireBase: JSON(value))
                    strongSelf.delegate?.getUserData(userData: data,
                                                     chatType: chatType)
                }
        }
        
        handler.append((FireBaseNode.users.rawValue, userID, handlerValue))
    }
    
    
//    MARK:- Update total Read Count
//    ==============================
    func updateTotalReadCount(isReduce: Bool,
                              readMessagesCount: Int,
                              userID: String,
                              messageType: MessageType = .text,
                              msg: String = ""){
            DataBaseReference.shared.dataBase
                .child(FireBaseNode.users.rawValue)
                .child(userID)
                .observeSingleEvent(of: .value) { (snapShot) in
                    
                    if snapShot.exists(),
                        let value = snapShot.value{
                        let userData = UserProfile(withFireBase: JSON(value))
                        var count: Int = 0
                        if isReduce{
                            let remainigCount = userData.totalUnreadCount - readMessagesCount
                            count = remainigCount > 0 ? remainigCount : 0
                        }else{
                            count = userData.totalUnreadCount + 1
                            if userData.userId != UserProfile.main.userId {
                                self.delegate?.addTotalUnreadCount(otherUserData: userData,
                                                                   type: messageType,
                                                                   text: msg,
                                                                   roomInfo: nil)
                            }
                        }
                        DataBaseReference.shared.dataBase
                            .child(FireBaseNode.users.rawValue)
                            .child(userID)
                            .updateChildValues([FireBaseKeys.totalUnreadCount.rawValue: count])
                    }
            }
    }
    
//    MARK:- ReadCount 0 when get a new message
//    ==========================================
    func reduceReadCount(roomId: String){
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomId)
            .observeSingleEvent(of: .value) { (snapShot) in
                
                if snapShot.exists(){
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.roomGroupIds.rawValue)
                        .child(UserProfile.main.userId)
                        .child(roomId)
                        .updateChildValues([FireBaseKeys.unreadMessageCount.rawValue: 0])
                }
        }
    }
    
//    MARK:- Update a read Count
//    ==========================
    func diminishReadCount(roomID: String,
                           userID: String,
                           isUserBlocked: Bool,
                           success: @escaping(() -> ())){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(userID)
            .child(roomID)
            .observeSingleEvent(of: .value) {[weak self](snapShot) in
                
                guard let strongSelf = self else { return }
                
                if snapShot.exists(), let value = snapShot.value{
                    
                    let roomInfo = ChatListing(json: JSON(value))
                    strongSelf.updateTotalReadCount(isReduce: true,
                                                    readMessagesCount: roomInfo.unreadMessageCount,
                                                    userID: userID)
                    
                    if !isUserBlocked{
                        strongSelf.reduceReadCount(roomId: roomID)
                    }
                    return success()
                }
        }
    }
    
//    MARK:- Check Other User Remove Room
//    ===================================
    func checkAnotherUserRemoveRoom(roomId: String, otherUserID: String){
        
            let handlerValue = DataBaseReference.shared.dataBase
                .child(FireBaseNode.roomGroupIds.rawValue)
                .child(otherUserID)
                .observe(.childRemoved) {[weak self](snapShot) in
                    
                    print(snapShot)
                    if snapShot.exists(), let value = snapShot.value{
                        let roomInfo = ChatListing(json: JSON(value))
                        if roomInfo.roomID == roomId{
                            
                            if let idx = self?.checkRoomExistIndex(roomId: roomId, userId: otherUserID){
                                self?.usersRoom[idx].isExist = false
                            }else{
                                let data: (roomID: String, userID: String, isExist: Bool) = (roomID: roomId, userID: otherUserID, isExist: false)
                              self?.usersRoom.append(data)
                            }
                        }
                    }
            }
            
            handler.append((FireBaseNode.roomGroupIds.rawValue, otherUserID, handlerValue))
    }
    
//    MARK:- Blocked Another User
//    ===========================
    func blockedUser(userID: String,
                     otherUserID: String){
        
        let roomID = createRoomID(firstId: userID, secondId: otherUserID)
       
        self.diminishReadCount(roomID: roomID,
                               userID: userID,
                               isUserBlocked: true,
                               success: {
              
                                DataBaseReference.shared.dataBase
                                    .child(FireBaseNode.roomGroupIds.rawValue)
                                    .child(userID)
                                    .child(roomID).removeValue()
        })
       
        self.diminishReadCount(roomID: roomID,
                               userID: otherUserID,
                               isUserBlocked: true,
                               success: {
                  
                                DataBaseReference.shared.dataBase
                                    .child(FireBaseNode.roomGroupIds.rawValue)
                                    .child(otherUserID)
                                    .child(roomID).removeValue()
        })

        removeMessageNode(id: roomID)
        
        getUserTags(userId: userID,
                    otherUserId: otherUserID,
                    roomId: roomID)
        
        getUserTags(userId: otherUserID,
                    otherUserId: userID,
                    roomId: roomID)
    }
    
    func getUserTags(userId: String,
                     otherUserId: String,
                     roomId: String){
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.users.rawValue)
            .child(userId)
            .observeSingleEvent(of: .value) { (snapShot) in
                
                if snapShot.exists(), let value = snapShot.value {
                    
                    let userData = UserProfile(withFireBase: JSON(value))
                    let tagsID = userData.myTags.components(separatedBy: ",")
                    if !tagsID.isEmpty {
                        tagsID.forEach({[weak self](str) in
                            
                            if !str.isEmpty{
                                self?.removeMembersFromTag(userID: userId,
                                                           tagID: str,
                                                           otherUserID: otherUserId)
                            }else{
                                self?.delegate?.userBlocked()
                            }
                        })
                    }
                }
        }
    }

    private func removeMembersFromTag(userID: String,
                                      tagID: String,
                                      otherUserID: String){
        
        printDebug("otherUserID: \(otherUserID)")
        
        DataBaseReference.shared.dataBase
            .child(FireBaseNode.tagsDetail.rawValue)
            .child(tagID)
            .child(FireBaseNode.members.rawValue)
            .child(otherUserID).removeValue { (error, snapShot) in
                
                printDebug("otherUserID1: \(otherUserID)")
                printDebug("snapShot: \(snapShot)")
                if error == nil{
                    DataBaseReference.shared.dataBase
                        .child(FireBaseNode.roomGroupIds.rawValue)
                        .child(otherUserID)
                        .child(tagID).removeValue()
                    self.diminishReadCount(roomID: tagID,
                                           userID: userID,
                                           isUserBlocked: true, success: {[weak self] in
                                            
                                            self?.delegate?.userBlocked()
                    })
                }
        }
    }
    
    private func removeMessageNode(id: String){
        
        DataBaseReference.shared.dataBase
        .child(FireBaseNode.messages.rawValue)
        .child(id).removeValue()
    }
    
//    MARK:- Check User Blocked
//    =========================
    func checkUserBlocked() {
        
        let handlerValue = DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .observe(.childRemoved) {[weak self](snapShot) in
                printDebug(snapShot)
                if snapShot.exists(), let value = snapShot.value{
                    let data = ChatListing(json: JSON(value))
                    self?.delegate?.userBlockedByAnother(roomData: data)
                }
        }
        handler.append((FireBaseNode.roomGroupIds.rawValue, UserProfile.main.userId, handlerValue))
    }
    
    
    func changeInRoomInfo(roomID: String){
        
        let handlerValue = DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(UserProfile.main.userId)
            .child(roomID).observe(.value) {[weak self](snapShot) in
                printDebug(snapShot)
                
                guard let strongSelf = self else { return }
                
                if snapShot.exists(), let value = snapShot.value{
                    let data = ChatListing(json: JSON(value))
                    strongSelf.delegate?.getUpdatedRoomData(info: data)
                }
        }
        
        handler.append((FireBaseNode.roomGroupIds.rawValue, UserProfile.main.userId, handlerValue))
    }
    
    func checkUserDeleteRoom(userID: String, roomID: String){
        
        let handlerValue = DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(userID)
            .child(roomID).observe(.childRemoved) {[weak self](snapShot) in
                
                guard let strongSelf = self else{ return }
                
                if let idx = strongSelf.checkRoomExistIndex(roomId: roomID, userId: userID){
                    strongSelf.usersRoom[idx].isExist = false
                }else{
                    let data: (roomID: String, userID: String, isExist: Bool) = (roomID: roomID, userID: userID, isExist: false)
                    strongSelf.usersRoom.append(data)
                }
        }
        
        handler.append((FireBaseNode.roomGroupIds.rawValue, userID, handlerValue))
    }
    
    func getUserRoomData(userID: String,
                         roomID: String,
                         roomData: @escaping ((_ info: ChatListing) ->())){
        
        let handlerValue = DataBaseReference.shared.dataBase
            .child(FireBaseNode.roomGroupIds.rawValue)
            .child(userID)
            .child(roomID).observe(.value) { (snapShot) in
                
                if let value = snapShot.value,
                    snapShot.exists(){
                    let roomInfo = ChatListing(json: JSON(value))
                    roomData(roomInfo)
                }
        }
        
        handler.append((FireBaseNode.roomGroupIds.rawValue, userID, handlerValue))
    }
}

