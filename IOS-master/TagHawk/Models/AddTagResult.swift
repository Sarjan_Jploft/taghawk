//
//  AddTagResult.swift
//  TagHawk
//
//  Created by Vikash on 10/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

enum TagStatus: Int {
    
    case active = 1
    case inActive = 2
    case block = 3
    
    init(type: Int){
        
        switch type{
            
        case TagStatus.active.rawValue: self = TagStatus.active
        case TagStatus.inActive.rawValue: self = TagStatus.inActive
        case TagStatus.block.rawValue: self = TagStatus.block
            
        default: self = TagStatus.active
        }
    }
}

struct AddTagResult {
    var id: String
    var name: String
    var ownerName: String
    var ownerEmail: String
    var tagImageURL: String
    var tagLong: Double
    var tagAddress: String
    var tagLat: Double
    var deepLinkUrl: String
    var description: String
    var shareCode: String
    var tagType: TagType
    var tagEntrance: TagEntrance
    var privateTagType: PrivateTagType
    var tagStatus: TagStatus
    var announcement: String
    var tagData: String
    
    init() {
        self.id = ""
        self.name = ""
        self.ownerName = ""
        self.ownerEmail = ""
        self.tagImageURL = ""
        self.tagLong = 0.0
        self.tagAddress = ""
        self.tagLat = 0.0
        self.deepLinkUrl = ""
        self.description = ""
        self.shareCode = ""
        tagEntrance = .publicType
        tagType = .otherType
        privateTagType = .none
        tagStatus = .active
        announcement = ""
        tagData = ""
    }
    
    init(json: JSON) {
        self.id = json[ApiKey._id].stringValue
        self.name = json["name"].stringValue
        self.ownerName = json["ownerName"].stringValue
        self.ownerEmail = json["ownerEmail"].stringValue
        self.tagImageURL = json["tagImageUrl"].stringValue
        self.tagLong = json["tagLongitude"].doubleValue
        self.tagAddress = json["tagAddress"].stringValue
        self.tagLat = json["tagLatitude"].doubleValue
        self.deepLinkUrl = json["link"].stringValue
        self.description = json["description"].stringValue
        self.shareCode = json["shareCode"].stringValue
        self.tagType = TagType(type: json["type"].intValue)
        self.tagEntrance = TagEntrance(type: json["entrance"].intValue)
        self.privateTagType = PrivateTagType(type: json["joinTagBy"].intValue)
        tagStatus = TagStatus(type: json["status"].intValue)
        self.announcement = json["announcement"].stringValue
        self.tagData = json["tagJoinData"].stringValue
    }
}
