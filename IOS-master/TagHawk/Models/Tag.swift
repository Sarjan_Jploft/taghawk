//
//  tagModel.swift
//  TagHawk
//
//  Created by Appinventiv on 11/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Tag {
    
    var tagLatitude : Double
    var tagLongitude : Double
    var type : SelectedFilters.TagType
    var entrance : SelectedFilters.TagEntrance
    var members : Int
    var tagImageUrl : String
    var isMember : Bool
    var description : String
    var created : TimeInterval
    var id : String
    var adminName : String
    var name : String
    var tagAddress : String
    var adminEmail : String
    var status : String
    var products : [Product] = []
    var joinTagBy : Int
    var email : String
    var document_type: String
    var link: String
    var requestStatus: Int
    var isCreatedByMe: Bool
    var announcement: String
    var city: String
    var tagJoinData: String
    
    init(){
        tagLatitude = 0.0
        tagLongitude = 0.0
        type = SelectedFilters.TagType.all
        entrance = SelectedFilters.TagEntrance.all
        members = 0
        tagImageUrl = ""
        isMember = false
        description = ""
        created = 0.0
        id = ""
        adminName = ""
        name = ""
        tagAddress = ""
        adminEmail = ""
        status = ""
        products = []
        joinTagBy = 1
        email = ""
        document_type = ""
        link = ""
        requestStatus = 0
        isCreatedByMe = false
        announcement = ""
        city = ""
        tagJoinData = ""
    }
    
    init(json : JSON){
        self.tagLatitude = json[ApiKey.tagLatitude].doubleValue
        self.tagLongitude = json[ApiKey.tagLongitude].doubleValue
        self.members = json[ApiKey.members].intValue
        self.tagImageUrl = json[ApiKey.tagImageUrl].string ?? json[ApiKey.imageUrl].stringValue
        self.isMember = json[ApiKey.isMember].boolValue
        self.description = json[ApiKey.description].stringValue
        self.created = json[ApiKey.created].doubleValue
        self.id = json[ApiKey._id].stringValue
        self.status = json[ApiKey.status].stringValue
        self.adminName = json[ApiKey.adminName].string ?? json[ApiKey.ownerName].stringValue
        self.name = json[ApiKey.name].stringValue
        self.tagAddress = json[ApiKey.tagAddress].stringValue
        self.adminEmail = json[ApiKey.adminEmail].stringValue
        self.joinTagBy = json[ApiKey.joinTagBy].intValue
        self.type = SelectedFilters.TagType(rawValue: json[ApiKey.type].intValue) ?? SelectedFilters.TagType.other
        self.entrance = SelectedFilters.TagEntrance(rawValue: json[ApiKey.entrance].intValue) ?? SelectedFilters.TagEntrance.publicTag
        self.products = json[ApiKey.productInfo].arrayValue.map { Product(json: $0) }
        self.email = json[ApiKey.email].stringValue
        self.document_type = json["document_type"].stringValue
        self.link = json["link"].stringValue
        self.requestStatus = json["requestStatus"].intValue
        self.isCreatedByMe = json[ApiKey.isCreatedByMe].boolValue
        self.announcement = json[ApiKey.announcement].stringValue
        self.city = json[ApiKey.city].stringValue
        self.tagJoinData = json[ApiKey.tagJoinData].stringValue
    }
    
    func getAddTagData() -> AddTagData {
        var tagData = AddTagData()
        tagData.tagName = name
        tagData.tagType = type.rawValue
        tagData.tagEntrance = entrance.rawValue
        tagData.imageUrl = tagImageUrl
        tagData.thumbUrl = tagImageUrl
        tagData.privateTagType = joinTagBy
        switch joinTagBy {
        case 1:
            tagData.email = tagJoinData
        case 2:
            tagData.password = tagJoinData
        case 3:
            tagData.documents = tagJoinData
        default: break
        }
        tagData.documentType = document_type
        tagData.description = description
        tagData.location = tagAddress
        tagData.city = city
        tagData.lat = tagLatitude
        tagData.long = tagLongitude
        tagData.announcement = announcement
        
        return tagData
    }
    
    static func getTagData(groupDetail: GroupDetail) -> Tag{
        var data = Tag()
        data.tagLatitude = groupDetail.latitude
        data.tagLongitude = groupDetail.longitude
        data.members = groupDetail.members.count + 1
        data.tagImageUrl = groupDetail.tagImage
        data.isMember = true
        data.description = groupDetail.description
//        data.created : TimeInterval
        data.id = groupDetail.tagID
        data.name = groupDetail.tagName
        data.tagAddress = groupDetail.tagAddress
//        data.adminEmail : String
//        data.status = groupDetail.
//        data.products : [Product] = []
        data.joinTagBy = groupDetail.verificationType.rawValue
//        data.email : String
//        data.document_type: String
        data.link = groupDetail.sharelink
        data.requestStatus = 1
        data.isCreatedByMe = true
        data.announcement = groupDetail.announcement
//        data.city: String
        return data
    }
}
