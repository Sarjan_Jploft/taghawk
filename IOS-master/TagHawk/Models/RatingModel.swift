//
//  RatingModel.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 28/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct RatingModel {
    let id: String
    let rating: Double
    var replied: RepliedModel
    let sellerId: String
    let profilePicture: String
    let comment: String
    let fullName: String
    let created: TimeInterval
    let userId: String
    let productName: String
    
    init() {
        id = ""
        rating = 0
        replied = RepliedModel()
        sellerId = ""
        profilePicture = ""
        comment = ""
        fullName = ""
        created = 0
        userId = ""
        productName = ""
    }
    
    init(json: JSON) {
        id              = json[ApiKey._id].stringValue
        rating          = json[ApiKey.rating].doubleValue
        replied         = RepliedModel(json: json[ApiKey.replied])
        sellerId        = json[ApiKey.sellerId].stringValue
        profilePicture  = json[ApiKey.profilePicture].stringValue
        comment         = json[ApiKey.comment].stringValue
        fullName        = json[ApiKey.fullName].stringValue
        created         = json[ApiKey.created].doubleValue
        userId          = json[ApiKey.userId].stringValue
        productName     = json[ApiKey.title].stringValue
    }
}

struct RepliedModel {
    var replyComment: String
    var editedStatus: Bool
    var replyDate: TimeInterval
    var editedDate: TimeInterval
    var replyBtnText: String
    
    init() {
        replyComment = ""
        editedStatus = false
        replyDate = 0
        editedDate = 0
        replyBtnText = ""
    }
    
    init(json: JSON) {
        replyComment = json[ApiKey.replyComment].stringValue
        editedStatus = json[ApiKey.editedStatus].boolValue
        replyDate    = json[ApiKey.replyDate].doubleValue
        editedDate   = json[ApiKey.editedDate].doubleValue
        replyBtnText = ""
    }
}
