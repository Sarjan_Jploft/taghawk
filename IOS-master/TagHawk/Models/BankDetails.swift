//
//  BankDetails.swift
//  TagHawk
//
//  Created by Appinventiv on 04/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

struct BankDetail  {

    let created : TimeInterval
    let id : String
    let accountHolderType : String
    let acountHolderId : String
    let accountNumber : String
    let accountHolderName : String
    let routingNumber : String
    let userId : String
    let firstName : String
    let lastName : String
    let balance : Double
    
     init(){
        created = 0.0
        id = ""
        accountHolderType = ""
        acountHolderId = ""
        accountNumber = ""
        accountHolderName = ""
        routingNumber = ""
        userId = ""
        firstName = ""
        lastName = ""
        balance = 0.0
    }
    
    init(json : JSON) {
        self.created = json[ApiKey.created].doubleValue
        self.id = json[ApiKey._id].stringValue
        self.userId = json[ApiKey.userId].stringValue
        balance = json[ApiKey.balance].doubleValue
        let accountDetails = json[ApiKey.accountDetails]
        accountHolderType = accountDetails[ApiKey.accountHolderType].stringValue
        acountHolderId = accountDetails[ApiKey._id].stringValue
        accountNumber = accountDetails[ApiKey.accountNumber].stringValue
        accountHolderName = accountDetails[ApiKey.accountHolderName].stringValue
        routingNumber = accountDetails[ApiKey.routingNumber].stringValue
        firstName = ""
        lastName = ""
    }
    
    var toJsonDict : JSONDictionary {
        var dict = JSONDictionary()
        var accountDetailsDict = JSONDictionary()
        dict[ApiKey.created] = self.created
        dict[ApiKey._id] = self.id
        dict[ApiKey.userId] = self.userId
        dict[ApiKey.balance] = self.balance
        accountDetailsDict[ApiKey.accountHolderType] = self.accountHolderType
        accountDetailsDict[ApiKey._id] = self.acountHolderId
        accountDetailsDict[ApiKey.accountNumber] = self.accountNumber
        accountDetailsDict[ApiKey.accountHolderName] = self.accountHolderName
        accountDetailsDict[ApiKey.routingNumber] = self.routingNumber
        dict[ApiKey.accountDetails] = accountDetailsDict
        
        return dict
    }
    
}
