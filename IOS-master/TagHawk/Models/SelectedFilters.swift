//
//  SelectedFilters.swift
//  TagHawk
//
//  Created by Appinventiv on 06/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
//28.535517
//77.391029

struct FilterValues {
    var lat : Double = 0.0
    var long : Double = 0.0
    var locationStr : String = ""
    var distance : Int = 5
    var postedWithin : SelectedFilters.PostedWithin = SelectedFilters.PostedWithin.none
    var fromPrice : Int = 0
    var toPrice : Int = 0
    var condiotions : [SelectedFilters.ProductConditionType] = []
    var verifiedSellorsOnly = false
    var rating : Int = 0
    var tagType = SelectedFilters.TagType.all
    var tagEntrance = SelectedFilters.TagEntrance.all
    var members = SelectedFilters.MembersFilterValues.allMembers
    
    init() {
        
    }
}

class SelectedFilters {
    
    static let shared = SelectedFilters()
    var selectedFiltersOnItems = FilterValues()
    var appliedFiltersOnItems = FilterValues()
    
    var selectedFilterOnTag = FilterValues()
    var appliedFiltersOnTag = FilterValues()
    
    var selectedFilterOnShelf = FilterValues()
    var appliedFilterOnShelf = FilterValues()
    
    var appliedSortingOnProducts = SortIngApplied.newest
   
    //var appliedSortingOnShelf = SortIngApplied.newest
    
    var appliedCategoryInShelf = Category()
    
    enum ProductConditionType : Int {
        case none = 0
        case new = 1
        case likeNew = 2
        case good = 3
        case normal = 4
        case flawd = 5
    }
    
    enum ProductConditionStringValues : String {
        case none = "Select"
        case new = "New (never used)"
        case likeNew = "Like New (rarely used)"
        case good = "Good (Gently Used)"
        case normal = "Normal (normal wear)"
        case flawd = "Flawed (with flaw)"
    }
    
    
    enum PostedWithin : Int {
        case none = 0
        case today = 1
        case thisWeek = 2
        case thisMonth = 3
        case last3Montshs = 4
        case thisYear = 5
    }
    
    enum PostedWithinStrValues : String {
        case none = "Select"
      case today = "24 Hours"
        case thisWeek = "One Week"
        case thisMonth = "One Month"
        case last3Montshs = "Last 3 months"
        case thisYear = "One Year"
    }
    
    enum DistanceValues : String {
        case zeroMile = "0 Mile"
        case oneMile = "1 Mile"
        case fiveMile = "5 Miles"
        case tenMile = "10 Miles"
        case thirtyMile = "30 Miles"
        case fiftyMile = "50 Miles"
        case max = "Max"
    }
    
    enum SortIngApplied {
        case newest
        case closest
        case priceHightToLow
        case priceLowToHigh
    }
    
    enum TagEntrance : Int {
        case all = 0
        case privateTag = 1
        case publicTag = 2
        case none
    }
    
    enum TagType : Int {
        case all = 0
        case apartment = 1
        case universities = 2
        case organization = 3
        case club = 4
        case other = 5
        case none
    }
    
    enum TagTypeStringValues : String {
//        case all = "All"
//        case privateTag = "Private"
//        case publicTag = "Public"
//        case none = ""
        
        case all = "All"
        case apartment = "Apartment"
        case universities = "Universities"
        case organization = "Organization"
        case club = "Club"
        case other = "Other"
        case none = ""
        
        
    }
    
    enum TagEnraceStringValues : String {
        case all = "All"
        case privateTag = "Private"
        case publicTag = "Public"
        case none = ""
    }
    
    enum MemberFilterStringValues : String {
        case allMembers = "All Members"
        case above10Members = "Above 10 members"
        case above50Members = "Above 50 members"
        case above100Members = "Above 100 members"
        case above500Members = "Above 500 members"
        case above1000Members = "Above 1000 members"
    }
    
    enum MembersFilterValues : Int {
        case allMembers = 0
        case above10Members = 10
        case above50Members = 50
        case above100Members = 100
        case above500Members = 500
        case above1000Members = 1000
    }
    
    
    func getMemberFilterStringValue(from : MembersFilterValues) -> MemberFilterStringValues {
        
        switch from {
            
            case .allMembers:
                return MemberFilterStringValues.allMembers
            
            case .above10Members:
                return MemberFilterStringValues.above10Members
            
            case .above50Members:
                return MemberFilterStringValues.above50Members
            
            case .above100Members:
                return MemberFilterStringValues.above100Members

            case .above500Members:
                return MemberFilterStringValues.above500Members
            
        case .above1000Members:
            return MemberFilterStringValues.above1000Members

        }
        
    }
    
    func getMemberFilterValue(from : MemberFilterStringValues) -> MembersFilterValues {
        
        switch from {
            
            case .allMembers:
                return MembersFilterValues.allMembers
            
            case .above10Members:
                return MembersFilterValues.above10Members
            
            case .above50Members:
                return MembersFilterValues.above50Members
            
            case .above100Members:
                return MembersFilterValues.above100Members
            
            case .above500Members:
                return MembersFilterValues.above500Members
            
            case .above1000Members:
                return MembersFilterValues.above1000Members
            
        }
        
    }
    
    func resetItemsFilter(){
        self.selectedFiltersOnItems = FilterValues()
        self.appliedFiltersOnItems = FilterValues()
        
//        LocationManager.shared.startUpdatingLocation { (location, error) in
        
//            guard let loc = location else { return }
            self.selectedFiltersOnItems.lat = 0.0
            self.selectedFiltersOnItems.long = 0.0
            
            self.appliedFiltersOnItems.lat = 0.0
            self.appliedFiltersOnItems.long = 0.0
//
//            if self.appliedFiltersOnTag.lat == 0.0{
//                
//                self.appliedFiltersOnTag.lat = LocationManager.shared.latitude
//                self.appliedFiltersOnTag.long = LocationManager.shared.longitude
//
//                self.selectedFilterOnTag.lat = LocationManager.shared.latitude
//                self.selectedFilterOnTag.long = LocationManager.shared.longitude
//
//            }
//        }
    }
    
    func resetTagFilters(){
        self.selectedFilterOnTag = FilterValues()
        self.appliedFiltersOnTag = FilterValues()
        
       // LocationManager.shared.startUpdatingLocation { (location, error) in
            
//            guard let loc = location else { return }
        
        
        self.selectedFilterOnTag.lat = LocationManager.shared.latitude
            self.selectedFilterOnTag.long = LocationManager.shared.longitude
            
            self.appliedFiltersOnTag.lat = LocationManager.shared.latitude
        self.appliedFiltersOnTag.long = LocationManager.shared.longitude
        
//            if self.appliedFiltersOnItems.lat == 0.0{
//
//                self.appliedFiltersOnItems.lat = LocationManager.shared.latitude
//                self.appliedFiltersOnItems.long = LocationManager.shared.longitude
//
//                self.selectedFiltersOnItems.lat = LocationManager.shared.latitude
//                self.selectedFiltersOnItems.long = LocationManager.shared.longitude
//
//            }
       // }
    }
    
    func resetShelfFilter(){
        self.selectedFilterOnShelf = FilterValues()
        self.appliedFilterOnShelf = FilterValues()
        self.appliedCategoryInShelf = Category()
        self.selectedFilterOnShelf.lat = 0.0
        self.selectedFilterOnShelf.long = 0.0
    }
    
    func copySelectedToAppliedForItemFilter(){
        self.appliedFiltersOnItems = self.selectedFiltersOnItems
        self.appliedFiltersOnTag.lat = self.appliedFiltersOnItems.lat
        self.appliedFiltersOnTag.long = self.appliedFiltersOnItems.long
        self.appliedFiltersOnTag.locationStr = self.appliedFiltersOnItems.locationStr
    }
    
    func copyAppliedToSelectedForItemFilter(){
        self.selectedFiltersOnItems = self.appliedFiltersOnItems
    }
    
    func copySelectedTagToAppliedForTagFilter(){
        self.appliedFiltersOnTag = self.selectedFilterOnTag
        self.appliedFiltersOnItems.lat = self.selectedFilterOnTag.lat
        self.appliedFiltersOnItems.long = self.selectedFilterOnTag.long
        self.appliedFiltersOnItems.locationStr = self.selectedFilterOnTag.locationStr
    }
    
    func copyAppliedToSelectedForTagFilter(){
        self.selectedFilterOnTag = self.appliedFiltersOnTag
    }
    
    func copySelectedShelfToAyylyedFilter(){
        self.appliedFilterOnShelf = self.selectedFilterOnShelf
    }

    func copyAppliedShelfFiltersToCopiedFilter(){
        self.selectedFilterOnShelf = self.appliedFilterOnShelf
    }
    
    func getProductConditionString(from condition : ProductConditionType) -> ProductConditionStringValues {
        
        switch condition {
        case .new:
            return ProductConditionStringValues.new
        
        case .likeNew:
            return ProductConditionStringValues.likeNew
        
        case .good:
            return ProductConditionStringValues.good
            
        case .normal:
            return ProductConditionStringValues.normal
            
        case .flawd:
            return ProductConditionStringValues.flawd
            
        case .none:
            return ProductConditionStringValues.none
        
        }
        
    }
    
    func getProductCondition(from str : ProductConditionStringValues) -> ProductConditionType{
        
        switch str {
            
            case .new:
                return ProductConditionType.new
            
            case .likeNew:
                return ProductConditionType.likeNew
            
            case .good:
                return ProductConditionType.good
            
            case .normal:
                return ProductConditionType.normal
            
            case .flawd:
                return ProductConditionType.flawd
            
            case .none:
                return ProductConditionType.none
            
        }
    }
    
    func getPostedWithInString(postedWithin : PostedWithin) -> String {
        
        switch  postedWithin {
            case .today :
                return PostedWithinStrValues.today.rawValue
       
        case .last3Montshs:
            return PostedWithinStrValues.last3Montshs.rawValue
        
        case .thisWeek:
            return PostedWithinStrValues.thisWeek.rawValue

        case .thisMonth:
            return PostedWithinStrValues.thisMonth.rawValue

        case .thisYear:
            return PostedWithinStrValues.thisYear.rawValue
            
        default:
            return PostedWithinStrValues.none.rawValue
            
        }
    }
    
    func getPostedWithIn(postedWithinString : PostedWithinStrValues) -> PostedWithin {
        
        switch postedWithinString {
        case .today:
            return PostedWithin.today
            
        case .thisWeek:
            return PostedWithin.thisWeek
            
        case .thisMonth:
            return PostedWithin.thisMonth
            
        case .thisYear:
            return PostedWithin.thisYear
            
        case .last3Montshs:
            return PostedWithin.last3Montshs

        default:
            return PostedWithin.none
 
        }
    }
    
    func getMilesForIndex(index : Int) -> Int{
        
        switch index {
       
        case 0:
           return 1
      
        case 1:
           return 5
            
        case 2:
            return 10
            
        case 3:
            return 30
            
        case 4:
            return 50
            
        case 5:
            return 60
            
        default:
            return 10
       
        }
    }
    
    
    func getTagTypeStringValue(from : TagType) -> TagTypeStringValues{
        
        switch  from {
        case .apartment:
            return TagTypeStringValues.apartment
            
        case .universities:
            return TagTypeStringValues.universities
            
        case .organization:
            return TagTypeStringValues.organization
            
        case .club:
            return TagTypeStringValues.club
            
        case .other:
            return TagTypeStringValues.other
            
        default:
            return TagTypeStringValues.all
        }
        
    }
    
    func getTagType(from : TagTypeStringValues) -> TagType{
        
        switch  from {
            
        case .apartment:
            return TagType.apartment
            
        case .universities:
            return TagType.universities
            
        case .organization:
            return TagType.organization
            
        case .club:
            return TagType.club
            
        case .other:
            return TagType.other
            
        default:
            return TagType.all
        }
        
    }
    
    func getEntraceTypeStringValue(from : TagEntrance) -> TagEnraceStringValues{
           
           switch  from {
               case .privateTag:
                   return TagEnraceStringValues.privateTag
               
               case .publicTag:
                   return TagEnraceStringValues.publicTag
               
               default:
                   return TagEnraceStringValues.all
           }
           
       }
       
       func getEntranceType(from : TagEnraceStringValues) -> TagEntrance{
           
           switch  from {
           case .privateTag:
               return TagEntrance.privateTag
               
           case .publicTag:
               return TagEntrance.publicTag
               
           default:
               return TagEntrance.all
           }
           
       }
       
    
    
    
    func setSortingAppliedText(sorting : SortIngApplied) -> String {
        
        switch sorting {
            
            case .closest:
                return "Closest"
            
            case .newest:
                return "Newest"
            
            case .priceHightToLow:
                return "Price : High To Low"
            
            case .priceLowToHigh:
                return "Price : Low To High"
            
            default:
                return ""
        }
    }
}
