//
//  ShippingWeightData.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ShippingWeightData {
    
    var fedexPrice : Double = 0
    var uspsPrice : Double = 0
    var weight : String = ""
    var isAvailableInFedex = true
    var isAvailableInUsps = false
    
    
    init(){
        
    }
    
    init(json : JSON){
        self.fedexPrice = json[ApiKey.fedexPrice].doubleValue.rounded(toPlaces: 2)
        self.uspsPrice = json[ApiKey.uspsPrice].doubleValue.rounded(toPlaces: 2)
        self.weight = json[ApiKey.weight].stringValue
        self.isAvailableInFedex = true
        self.isAvailableInUsps = false
    }
    
}
