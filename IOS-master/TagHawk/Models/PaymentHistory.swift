//
//  PaymentHistory.swift
//  TagHawk
//
//  Created by Admin on 6/4/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

enum DeliveryStatus : String{
    case pending = "PENDING"
    case requestForRefund = "REQUEST_FOR_REFUND"
    case refundAccepted = "REFUND_ACCEPTED"
    case completed = "COMPLETED"
    case itemDeliver = "ITEM_DELEVER"
    case requestSuccess = "REFUND_SUCCESS"
    case disputeStarted = "DISPUTE_STARTED"
    case disputeCompleted = "DISPUTE_COMPLETED"
    case declined = "DECLINED"
    case sellarStatementDone = "SELLER_STATEMENT_DONE"
    case disputeCanStart = "DISPUTE_CAN_START"
}

struct PaymentHistory  {

    let deliveryDate : TimeInterval
    let _id : String
    let price : Double
    let productId : String
    let disputeId : String
    let refundDate : TimeInterval
    let userId : String
    let title : String
    let netPrice : Double
    let sellerId : String
    let images : [ProductImage]
    let refundRequestDate : String
    let disputeDate : TimeInterval
    var deliveryStatus : DeliveryStatus
    let reason: String
    let refundId : String
    let disputeCompleteDate : TimeInterval
    let purchasedDate : TimeInterval
    let sellerName: String
    let sellerProfilePic: String
    let buyerName: String
    let buyerImage: String
    
    init(){
        deliveryDate = 0.0
         _id = ""
         price = 0.0
         productId = ""
         disputeId = ""
         refundDate = 0.0
         userId = ""
         title = ""
         netPrice = 0
         sellerId = ""
         images = []
         refundRequestDate = ""
         disputeDate = 0
         deliveryStatus = .completed
         refundId = ""
         disputeCompleteDate = 0
         purchasedDate = 0
        reason = ""
        sellerName = ""
        sellerProfilePic = ""
        buyerName = ""
        buyerImage = ""
    }
    
    init(json : JSON){
        deliveryDate = json[ApiKey.deliveryDate].doubleValue
        _id = json[ApiKey._id].stringValue
        price = json[ApiKey.price].doubleValue
        productId = json[ApiKey.productId].stringValue
        disputeId = json[ApiKey.disputeId].stringValue
        refundDate = json[ApiKey.refundDate].doubleValue
        userId = json[ApiKey.userId].stringValue
        title = json[ApiKey.title].stringValue
        netPrice = json[ApiKey.netPrice].doubleValue
        sellerId = json[ApiKey.sellerId].stringValue
        let imagesArray = json[ApiKey.images].arrayValue
        self.images = imagesArray.map { ProductImage(json: $0) }
        refundRequestDate = json[ApiKey.refundRequestDate].stringValue
        disputeDate = json[ApiKey.disputeDate].doubleValue
        deliveryStatus = DeliveryStatus(rawValue: json[ApiKey.deliveryStatus].stringValue) ?? DeliveryStatus.pending
        refundId = json[ApiKey.refundId].stringValue
        disputeCompleteDate = json[ApiKey.disputeCompleteDate].doubleValue
        purchasedDate = json[ApiKey.purchasedDate].doubleValue
        reason = json["reason"].stringValue
        sellerName = json["sellerName"].stringValue
        sellerProfilePic = json["sellerProfilePicture"].stringValue
        buyerName = json["userName"].stringValue
        buyerImage = json["profilePicture"].stringValue
    }
}
