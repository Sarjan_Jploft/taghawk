//
//  FollowingFollowersModel.swift
//  TagHawk
//
//  Created by Appinventiv on 20/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

class FollowingFollowersModel {

    var userId : String
    var _id : String
    let receiverId : String
    var profilePicture : String
    var fullName : String
    let requestStatus : Int
    let sellerVerified : Bool
    let senderId : String
    let email : String
    var isFollowing : Bool
    let isFollower : Bool
    
    
    init(){
        fullName = ""
        _id = ""
        receiverId = ""
        profilePicture = ""
        requestStatus = 0
        sellerVerified = false
        senderId = ""
        email = ""
        isFollowing = false
        isFollower = false
        userId = ""
    }
    
  
    init(name : String, profilePicture : String, userId : String){
        fullName = name
        self.userId = userId
        self.profilePicture = profilePicture
        _id = ""
        receiverId = ""
        requestStatus = 0
        sellerVerified = false
        senderId = ""
        email = ""
        isFollowing = false
        isFollower = false
    }
    
    init(json : JSON){
        self._id = json[ApiKey._id].stringValue
        self.receiverId = json[ApiKey.receiverId].stringValue
        self.senderId = json[ApiKey.senderId].stringValue
        self.fullName = json[ApiKey.fullName].stringValue
        self.profilePicture = json[ApiKey.profilePicture].stringValue
        self.requestStatus = json[ApiKey.requestStatus].intValue
        self.sellerVerified = json[ApiKey.sellerVerified].boolValue
        self.email = json[ApiKey.email].stringValue
        self.isFollowing = json[ApiKey.isFollowing].boolValue
        self.isFollower = json[ApiKey.isFollower].boolValue
        self.userId = json[ApiKey.userId].stringValue
    }
    
}
