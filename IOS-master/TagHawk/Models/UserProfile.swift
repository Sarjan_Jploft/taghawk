//
//  UserProfile.swift
//  TagHawk
//
//  Created by Vikash on 08/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct UserProfile {
    
    enum UserType : String {
      case guest = "1"
      case notmal = "2"
      case hawkDriver = "3"
      case movingCompany = "4"
    }
    
    enum LoginType: String {
        case facebook   = "FACEBOOK"
        case email      = "EMAIL"
        case none       = ""
    }
    
    var id: String
    var isFacebookLogin: Bool
    var fullName: String
    var email: String
    var isEmailVerified: Bool
   // var userType: [Int]
    var followers: Int
    var following: Int
    var status: Int
    var invitationCode: String
    var myTags: String
    var created: TimeInterval
    var profileCompleted: Int
    var profilePicture: String
    var sellerVerified: Bool
//    var address: String
    var long: Double
    var lat: Double
    var isPhoneVerified: Bool
    var phoneNumber: String
    var officialIdVerified: Bool
    var isDocumentsVerified: Bool
    var govtId: String
    var rating: Double
    var countryCode: String
    let shareUrl: String
    let documents: [JSON]
    var isFollowing : Bool
    var dob : String
    var ssnNumber : String
    var stripeMerchantId : String
    var firstName : String
    var lastName : String
    let refreshToken : String
    var accessToken : String
    var userId : String
    var bankDetails : BankDetail
    var stripeCustomerId : String
    var isNotificationMuted : Bool
    var loginType: LoginType
    var isBlocked : Bool
    var totalUnreadCount: Int
    var deviceToken: String
    var balance : Double
    var pendingAmmount : Double
    var cashOutBalance : Double
    var passport : Bool
    var line1 : String
    var line2 : String
    var city : String
    var state : String
    var zipCode : String
    
    static var main = UserProfile(json: AppUserDefaults.value(forKey: .fullUserProfile)) {
            didSet {
                main.saveToUserDefaults()
            }
    }
    
    init() {
        self.id = ""
        self.isFacebookLogin = false
        self.fullName = ""
        self.email = ""
        self.isEmailVerified = false
        self.myTags = ""
        //self.userType = [Int]()
        self.created = 0.0
        self.followers = 0
        self.following = 0
        self.status = 0
        self.invitationCode = ""
        self.profileCompleted = 0
        self.profilePicture = ""
        self.sellerVerified = false
//        self.address = ""
        self.long = 0.0
        self.lat = 0.0
        self.isPhoneVerified = false
        self.phoneNumber = ""
        self.officialIdVerified = false
        self.isDocumentsVerified = false
        self.govtId = ""
        self.rating = 0.0
        self.countryCode = ""
        self.documents = []
        self.shareUrl = ""
        self.isFollowing = false
        self.dob = ""
         ssnNumber = ""
         stripeMerchantId = ""
         firstName = ""
         lastName = ""
        refreshToken = ""
        accessToken = ""
        userId = ""
        bankDetails = BankDetail()
        stripeCustomerId = ""
        isNotificationMuted = false
        loginType = .none
        isBlocked = false
        totalUnreadCount = 0
        deviceToken = ""
        balance = 0.0
        pendingAmmount = 0.0
        cashOutBalance = 0.0
        passport = false
         line1 = ""
         line2 = ""
         city = ""
         state = ""
         zipCode = ""
    }
    
    init(withFireBase json: JSON) {
        userId = json[FireBaseKeys.userId.rawValue].stringValue
        id = json[FireBaseKeys.userId.rawValue].stringValue
        email = json[FireBaseKeys.email.rawValue].stringValue
        fullName = json[FireBaseKeys.fullName.rawValue].stringValue
        invitationCode = json[FireBaseKeys.invitationCode.rawValue].stringValue
        refreshToken = json[FireBaseKeys.refreshToken.rawValue].stringValue
        accessToken = json[FireBaseKeys.accessToken.rawValue].stringValue
        profilePicture = json[FireBaseKeys.profilePicture.rawValue].stringValue
        totalUnreadCount = json[FireBaseKeys.totalUnreadCount.rawValue].intValue
        myTags = json[FireBaseKeys.myTags.rawValue].stringValue
        deviceToken = json[FireBaseKeys.deviceToken.rawValue].stringValue
        countryCode = ""
        isFacebookLogin = false
        isEmailVerified = false
        isDocumentsVerified = false
        created = 0.0
        followers = 0
        following = 0
        status = 0
        profileCompleted = 0
        sellerVerified = false
//        address = ""
        long = 0.0
        lat = 0.0
        isPhoneVerified = false
        phoneNumber = ""
        officialIdVerified = false
        govtId = ""
        rating = 0.0
        documents = []
        self.shareUrl = ""
        self.isFollowing = false
        self.dob = ""
        ssnNumber = ""
        self.stripeMerchantId = ""
        self.firstName = ""
        self.lastName = ""
        self.bankDetails = BankDetail(json: JSON())
        self.stripeCustomerId = ""
        isNotificationMuted = false
        loginType = .none
        isBlocked = false
        balance = 0.0
        pendingAmmount = 0.0
        cashOutBalance = 0.0
        passport = false
        line1 = ""
        line2 = ""
        city = ""
        state = ""
        zipCode = ""
    }
    
    
    init(json : JSON){
        self.countryCode = json[ApiKey.countryCode].stringValue
        self.id = json[ApiKey.id].stringValue
        self.isFacebookLogin = json[ApiKey.isFacebookLogin].boolValue
        self.fullName = json[ApiKey.fullName].stringValue
        self.email = json[ApiKey.email].stringValue
        self.isEmailVerified = json[ApiKey.isEmailVerified].boolValue
        self.isDocumentsVerified = json[ApiKey.isDocumentsVerified].boolValue
        // self.userType = json["userType"].arrayObject as! [Int]
        self.created = json[ApiKey.created].doubleValue
        self.followers = json[ApiKey.followers].intValue
        self.following = json[ApiKey.following].intValue
        self.status = json[ApiKey.status].intValue
        self.invitationCode = json[ApiKey.invitationCode].stringValue
        self.profileCompleted = json[ApiKey.profileCompleted].intValue
        self.profilePicture = json[ApiKey.profilePicture].stringValue
        self.sellerVerified = json[ApiKey.sellerVerified].boolValue
//        self.address = json[ApiKey.address].stringValue
        self.long = json[ApiKey.long].doubleValue
        self.lat = json[ApiKey.lat].doubleValue
        self.isPhoneVerified = json[ApiKey.isPhoneVerified].boolValue
        self.phoneNumber = json[ApiKey.phoneNumber].stringValue
        self.officialIdVerified = json[ApiKey.officialIdVerified].boolValue
        self.govtId = json[ApiKey.govtId].stringValue
        self.rating = json[ApiKey.sellerRating].doubleValue.rounded(toPlaces: 1)
        self.documents = json[ApiKey.documents].arrayValue
        self.shareUrl = json[ApiKey.shareUrl].stringValue
        self.isFollowing = json[ApiKey.isFollowing].boolValue
        self.dob = json[ApiKey.dob].stringValue
        ssnNumber = json[ApiKey.ssnNumber].stringValue
        self.stripeMerchantId = json[ApiKey.stripeMerchantId].stringValue
        self.firstName = json[ApiKey.firstName].stringValue
        self.lastName = json[ApiKey.lastName].stringValue
        self.refreshToken = json[ApiKey.refreshToken].stringValue
        self.accessToken = json[ApiKey.accessToken].stringValue
        self.fullName = json[ApiKey.fullName].stringValue
        self.bankDetails = BankDetail(json: json[ApiKey.bankDetail])
        self.userId = json[ApiKey.userId].stringValue
        self.stripeCustomerId = json[ApiKey.stripeCustomerId].stringValue
        isNotificationMuted = json[ApiKey.isMute].boolValue
        balance = json[ApiKey.balance].doubleValue
        let login = json[ApiKey.loginType].stringValue
        loginType = LoginType(rawValue: login) ?? .none
        isBlocked = json[ApiKey.isBlocked].boolValue
        passport = json[ApiKey.password].boolValue
        totalUnreadCount = 0
        myTags = ""
        deviceToken = ""
        pendingAmmount = 0.0
        cashOutBalance = json[ApiKey.cashOutBalance].doubleValue.rounded(toPlaces: 2)
        let address = json[ApiKey.address]
        line1 = address[ApiKey.line1].stringValue
        line2 = address[ApiKey.line2].stringValue
        city = address[ApiKey.city].stringValue
        state = address[ApiKey.state].stringValue
        zipCode = address[ApiKey.postalCode].stringValue
    }
   
    func saveToUserDefaults() {
        
        let dict: JSONDictionary = [
            ApiKey.email: email,
            ApiKey.refreshToken:refreshToken,
            ApiKey.accessToken:accessToken,
            ApiKey.fullName:fullName,
            ApiKey.invitationCode : invitationCode,
            ApiKey.userId : userId,
            ApiKey.countryCode : self.countryCode,
            ApiKey.phoneNumber : self.phoneNumber,
            ApiKey.id : self.id,
            ApiKey.isFacebookLogin : self.isFacebookLogin,
            ApiKey.isEmailVerified : self.isEmailVerified,
            ApiKey.isDocumentsVerified : self.isDocumentsVerified,
            ApiKey.created : self.created,
            ApiKey.followers : self.followers,
            ApiKey.following : self.following,
            ApiKey.status : self.status,
            ApiKey.profileCompleted : self.profileCompleted,
            ApiKey.profilePicture : self.profilePicture,
            ApiKey.sellerVerified : self.sellerVerified,
//            ApiKey.address : self.address,
            ApiKey.long : self.long,
            ApiKey.lat : self.lat,
            ApiKey.isPhoneVerified : self.isPhoneVerified,
            ApiKey.officialIdVerified : self.officialIdVerified,
            ApiKey.rating : self.rating,
            ApiKey.documents : [],
            ApiKey.shareUrl : self.shareUrl,
            ApiKey.dob : self.dob,
            ApiKey.ssnNumber : self.ssnNumber,
            ApiKey.stripeMerchantId : self.stripeMerchantId,
            ApiKey.firstName : self.firstName,
            ApiKey.lastName : self.lastName,
            ApiKey.bankDetail : self.bankDetails.toJsonDict,
            ApiKey.stripeCustomerId : self.stripeCustomerId,
            ApiKey.isMute : self.isNotificationMuted,
            ApiKey.loginType : loginType.rawValue,
            ApiKey.balance : self.balance,
            ApiKey.password : self.passport,
            ApiKey.cashOutBalance : self.cashOutBalance,
            ApiKey.address : [ApiKey.line1 : self.line1, ApiKey.line2 : self.line2,
                              ApiKey.city : self.city,
                              ApiKey.state : self.state,
                              ApiKey.postalCode : self.zipCode]
        ]
    
        AppUserDefaults.save(value: dict, forKey: .fullUserProfile)
    }
    
    var fireBaseDictionary: JSONDictionary {
        
        let dic: JSONDictionary = [FireBaseKeys.userId.rawValue: userId,
                                   FireBaseKeys.email.rawValue: email,
                                   FireBaseKeys.fullName.rawValue: fullName,
                                   FireBaseKeys.invitationCode.rawValue: invitationCode,
                                   FireBaseKeys.refreshToken.rawValue: refreshToken,
                                   FireBaseKeys.accessToken.rawValue: accessToken,
                                   FireBaseKeys.profilePicture.rawValue: profilePicture,
                                   FireBaseKeys.myTags.rawValue: myTags,
                                   FireBaseKeys.deviceToken.rawValue: DeviceDetail.fcmToken,
                                   FireBaseKeys.deviceType.rawValue: "2"]
        return dic
    }
    
    var roomDictionary: JSONDictionary{
        
        let dic: JSONDictionary = [FireBaseKeys.roomImage.rawValue: self.profilePicture,
                                   FireBaseKeys.roomName.rawValue: fullName,
                                   FireBaseKeys.pinned.rawValue: false,
                                   FireBaseKeys.chatMute.rawValue: false,
                                   FireBaseKeys.productImage.rawValue: "",
                                   FireBaseKeys.chatType.rawValue: ChatType.single.rawValue,
                                   FireBaseKeys.userType.rawValue: UserType.notmal.rawValue,
                                   FireBaseKeys.otherUserId.rawValue: userId,
                                   FireBaseKeys.unreadMessageCount.rawValue: 0]
        return dic
    }
    
    static func getUserDataInUsermodel(requestData: PendingRequest) -> UserProfile{
        var userData = UserProfile()
        userData.userId = requestData.userID
        userData.fullName = requestData.userName
        userData.profilePicture = requestData.profilePic
        return userData
    }
    
    static func getUserDataFromGroupMembers(data: GroupMembers) -> UserProfile{
        var userData = UserProfile()
        userData.userId = data.id
        userData.fullName = data.name
        userData.profilePicture = data.image
//        userData.typ
        return userData
    }
}
