//
//  POIItem.swift
//  ClusteringDemo
//
//  Created by Appinventiv on 03/11/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import CoreLocation


/// Point of Interest Item which implements the GMUClusterItem protocol.
class TagItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var tag : Tag
    
    init(position: CLLocationCoordinate2D, tag : Tag) {
        self.position = position
        self.tag = tag
    }
}
