//
//  SellerModel.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 01/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SellerProductModel {
    let title           : String
    let prodId          : String
    let fullName        : String
    let profilePicture  : String
    let sellerId        : String
    let _id             : String
    
    init() {
        title           = ""
        prodId          = ""
        fullName        = ""
        profilePicture  = ""
        sellerId        = ""
        _id             = ""
    }
    
    init(json: JSON) {
        title           = json[ApiKey.title].stringValue
        prodId          = json[ApiKey.productId].stringValue
        fullName        = json[ApiKey.fullName].stringValue
        profilePicture  = json[ApiKey.profilePicture].stringValue
        sellerId        = json[ApiKey.sellerId].stringValue
        _id             = json[ApiKey._id].stringValue
    }
}
