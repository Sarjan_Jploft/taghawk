//
//  UserProduct.swift
//  TagHawk
//
//  Created by Vikash on 08/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

enum ProductStatus: Int {
    case selling = 1
    case sold = 2
    case pending = 5
    case none
    
    init(value: Int){
        
        switch value {
            
        case ProductStatus.selling.rawValue: self = ProductStatus.selling
        case ProductStatus.sold.rawValue: self = ProductStatus.sold
        case ProductStatus.pending.rawValue: self = ProductStatus.pending
        default: self = ProductStatus.none
            
        }
    }
    
    static func getStatus(deliveryStatus: DeliveryStatus) -> Int{
        switch deliveryStatus {
            
        case .requestSuccess: return ProductStatus.selling.rawValue
        case .completed: return ProductStatus.sold.rawValue
        default: return ProductStatus.pending.rawValue
        }
    }
    
    var value: String {
        
        switch self {
            
        case .selling: return ""
        case .sold: return "Sold"
        case .pending: return "Pending"
        case .none: return ""
        }
    }
}

struct UserProduct {
    
    var id: String
    var firmPrice: Double
    var productName: String
    var images: [ProductImage]
    var created: Double
    var isPromoted: Bool
    var sellerVerified: Bool
    var productStatus: ProductStatus
    
    init() {
        self.id = ""
        self.firmPrice = 0.0
        self.images = [ProductImage]()
        self.created = 0.0
        self.isPromoted = false
        self.sellerVerified = false
        self.productName = ""
        self.productStatus = .selling
    }
    
    init(json: JSON) {
        self.id = json[ApiKey._id].stringValue
        self.firmPrice = json["firmPrice"].doubleValue
        self.images = ProductImage.returnProductImagesArray(jsonArr:  json["images"].arrayValue)
        self.created = json["created"].doubleValue
        self.isPromoted = json["isPromoted"].boolValue
        self.sellerVerified = json["sellerVerified"].boolValue
        self.productName = json["title"].stringValue
        self.productStatus = ProductStatus(value: json["productStatus"].intValue)
    }
    
    static func returnUserProductArray(jsonArr:[JSON]) -> [UserProduct] {
        var productData = [UserProduct]()
        for element in jsonArr {
            productData.append(UserProduct(json: element))
        }
        return productData
    }
    
    func getAddProductDataFromProduct() -> AddProductData {
        
        var addProductData = AddProductData()
        
        addProductData.title = ""
        addProductData.lat = 0
        addProductData.long = 0
        addProductData.prodId = self.id
        addProductData.price = self.firmPrice
        addProductData.isNegotiable = false
        addProductData.condition = .none
        addProductData.description = ""
        addProductData.shippingAvailability = []
        addProductData.location = ""
        //        self.addProductData.capturedImagesUrl = self.product.images.map { (obj) -> String in return obj.image }
        
        let indexAndNum : [(index : Int, stringUrl : String, img : UIImage?)] = self.images.enumerated().map { (index, element) in
            return (index : index, stringUrl : element.image , img : nil)
        }
        addProductData.productImages = indexAndNum
        
        return addProductData
    }
}
