//
//  CartProduct.swift
//  TagHawk
//
//  Created by Appinventiv on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

class CartProduct {

    let sellerId : String
    var _id : String
    let productDescription : String
    let productId : String
    let productPrice : Double
    let productName : String
    let totalCount : Int
    let sellerName : String
    let productPicUrl : [String]
    let shippingAvailibility : ShippingAvailability
    var productStatus = CartCollectionViewCell.ProductStatus.sold
    
    init(){
       sellerId = ""
       _id = ""
        productDescription = ""
         productId = ""
         productPrice = 0
         productName = ""
         totalCount = 0
         sellerName = ""
         productPicUrl = []
         shippingAvailibility = .pickUp
    }
    
    init(json : JSON){
        self.sellerId = json[ApiKey.sellerId].stringValue
        self._id = json[ApiKey._id].stringValue
        self.productDescription = json[ApiKey.productDescription].stringValue
        self.productId = json[ApiKey.productId].stringValue
        self.productPrice = json[ApiKey.productPrice].doubleValue
        self.productName = json[ApiKey.productName].stringValue
        self.totalCount = json[ApiKey.totalCount].intValue
        self.sellerName = json[ApiKey.sellerName].stringValue
        let pics = json[ApiKey.productPicUrl].arrayValue.map { $0.stringValue }
        self.productPicUrl = pics
        self.shippingAvailibility = ShippingAvailability(rawValue: json[ApiKey.shippingAvailibility].intValue) ?? .pickUp
        self.productStatus = CartCollectionViewCell.ProductStatus(rawValue: json[ApiKey.productStatus].intValue) ?? CartCollectionViewCell.ProductStatus.sold
        
    }
    
}
