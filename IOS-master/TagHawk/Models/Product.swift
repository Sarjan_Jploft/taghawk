//
//  Product.swift
//  TagHawk
//
//  Created by Appinventiv on 29/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON


enum ShippingAvailability : Int {
    case pickUp = 1
    case deliver = 2
    case shipping = 3
    
    func stringValue() -> String {
        switch self {
        case .pickUp: return LocalizedString.Pickup.localized
        case .deliver: return LocalizedString.Delivery.localized
        case .shipping: return LocalizedString.Shipping.localized
        }
    }
    
}

enum ShippingMode : String {
    case fedex = "FEDEX"
    case usps = "USPS"
    case none = ""
}


struct Product {

    let productLatitude : Double
    let productLongitude : Double
    let productHomeLatitude : Double
    let productHomeLongitude : Double
    var userId: String
    let userLatitude : Double
    let userLongitude : Double
    let productAddress : String
    let productLocation:String
    let city:String
    let state:String
    var firmPrice : Double
    var productId : String
    var isSellerVerified : Bool = false
    var sellerRating : Float
    var title : String
    var images : [ProductImage] = []
    let created : TimeInterval
    let followings : Int
    let followers : Int
    let userCreated : TimeInterval
    var isLiked : Bool
    let description : String
    let productCategoryId : String
    let isNegotiable : Bool
    var fullName : String
    var similarProducts : [Product] = []
    var shippingType : [ShippingAvailability] = []
    var condition = SelectedFilters.ProductConditionType.new
    var category : Category
    let sharingUrl : String
    let isCreatedByMe : Bool
    let profilePicture: String
    var productStatus: Int
    var isPromoted: Bool
    let sharedTags: [Tag]
    let isTransactionCost : Bool
    let shippingMode: ShippingMode
    let weight: String
    let shippingPrice: Double
    let isEmailVerified : Bool
    let isFacebookLogin : Bool
    let isPhoneVerified : Bool
    let isDocumentsVerified : Bool
    
    
     init(){
         productLatitude = 0.0
         productLongitude = 0.0
         productHomeLatitude = 0.0
         productHomeLongitude = 0.0
         userLatitude = 0.0
         userLongitude = 0.0
         productAddress = ""
         productLocation = ""
         city = ""
         state = ""
         firmPrice = 0
         productId = ""
         isSellerVerified = false
         sellerRating = 0
         title = ""
         images = []
         created = 0.0
         followings = 0
         followers = 0
         isLiked = false
         description = ""
         productCategoryId = ""
         isNegotiable = false
         userCreated = 0
         fullName = ""
        similarProducts = []
        category = Category(json: JSON())
        sharingUrl = ""
        isCreatedByMe = false
        userId = ""
        profilePicture = ""
        productStatus = 0
        isPromoted = false
        sharedTags = []
        isTransactionCost = false
        shippingMode = .none
        shippingPrice = 0
        weight = ""
        isEmailVerified = false
        isFacebookLogin = false
        isPhoneVerified = false
        isDocumentsVerified = false
    }
    
    init(json : JSON){
        self.title = json[ApiKey.title].stringValue
        let imagesArray = json[ApiKey.images].arrayValue
        self.images = imagesArray.map { ProductImage(json: $0) }
        self.productId = json[ApiKey._id].stringValue
        self.firmPrice = json[ApiKey.firmPrice].doubleValue
        self.userCreated = json[ApiKey.userCreated].doubleValue
        self.productLatitude = json[ApiKey.productLatitude].doubleValue
        self.productLongitude = json[ApiKey.productLongitude].doubleValue
        self.userLatitude = json[ApiKey.userLatitude].doubleValue
        self.userLongitude = json[ApiKey.userLongitude].doubleValue
        self.productAddress = json[ApiKey.productAddress].stringValue
        self.city = json[ApiKey.city].stringValue
        self.state = json[ApiKey.state].stringValue
        let json_location = json[ApiKey.location].dictionary
//        self.productLocation = json_location?[ApiKey.location]?.stringValue ?? ""
        self.productLocation = json[ApiKey.location].stringValue
//        self.productLocation = json_location?[ApiKey.location]?.stringValue ?? ""
        let crdnt = json_location?[ApiKey.coordinates]?.arrayValue
        self.productHomeLongitude = crdnt?[0].double ?? 0
        self.productHomeLatitude = crdnt?[1].double ?? 0
        self.userId = json[ApiKey.userId].stringValue
        self.created = json[ApiKey.created].doubleValue
        self.isSellerVerified = json[ApiKey.sellerVerified].boolValue
        self.sellerRating = json[ApiKey.rating].floatValue
        self.followers = json[ApiKey.followers].intValue
        self.followings = json[ApiKey.followings].intValue
        self.isLiked = json[ApiKey.isLiked].boolValue
        self.productCategoryId = json[ApiKey.productCategoryId].stringValue
        self.isNegotiable = json[ApiKey.isNegotiable].boolValue
        self.description = json[ApiKey.description].stringValue
        self.fullName =  json[ApiKey.fullName].stringValue
        self.similarProducts = json[ApiKey.similarProducts].arrayValue.map { Product(json: $0) }
        self.condition = SelectedFilters.ProductConditionType(rawValue: json[ApiKey.condition].intValue) ?? SelectedFilters.ProductConditionType.new
        category = Category(json: JSON())
        sharingUrl = json[ApiKey.link].stringValue
        isCreatedByMe = json[ApiKey.isCreatedByMe].boolValue
        profilePicture = json[ApiKey.profilePicture].stringValue
        productStatus = json[ApiKey.productStatus].intValue
        isPromoted = json[ApiKey.isPromoted].boolValue
        isTransactionCost = json[ApiKey.isTransactionCost].boolValue
        
        shippingMode = ShippingMode(rawValue: json[ApiKey.shippingType].stringValue) ?? .none
        shippingPrice = json[ApiKey.shippingPrice].doubleValue
        weight = json[ApiKey.weight].stringValue
        
        sharedTags = json[ApiKey.sharedCommunities].arrayValue.map {
            return Tag(json: $0)
        }
      
        self.shippingType = json[ApiKey.shippingAvailibility].arrayValue.map { ShippingAvailability(rawValue: $0.intValue ) ?? ShippingAvailability.pickUp }
        
        isEmailVerified = json[ApiKey.isEmailVerified].boolValue
        isFacebookLogin = json[ApiKey.isFacebookLogin].boolValue
        isPhoneVerified = json[ApiKey.isPhoneVerified].boolValue
        isDocumentsVerified = json[ApiKey.isDocumentsVerified].boolValue
        
    }
    
    
    func getAddProductDataFromProduct(shippingData : [ShippingWeightData] = []) -> AddProductData {
        
        var addProductData = AddProductData()
        
        addProductData.sharedCommunities = self.sharedTags
        addProductData.title = self.title
        addProductData.lat = self.productLatitude
        addProductData.long = self.productLongitude
        addProductData.prodId = self.productId
        addProductData.price = self.firmPrice
        addProductData.isNegotiable = self.isNegotiable
        addProductData.condition = self.condition
        addProductData.description = self.description
        addProductData.shippingAvailability = self.shippingType
        addProductData.location = self.productAddress
        addProductData.isTransactionCost = isTransactionCost
        addProductData.city = self.city
        addProductData.state = self.state
        let filteredShippingData = shippingData.filter { $0.weight == weight }
        
        if let shippingObj = filteredShippingData.first{
            addProductData.shippingData = shippingObj
        }
        addProductData.selectedShippingMode = self.shippingMode
        
        let indexAndNum : [(index : Int, stringUrl : String, img : UIImage?)] = self.images.enumerated().map { (index, element) in
            return (index : index, stringUrl : element.image , img : nil)
        }
        
        addProductData.productImages = indexAndNum

        let cat = AppSharedData.shared.categories.filter { $0.categoryId == self.productCategoryId }
        
        if let firstObj = cat.first{
            addProductData.category = firstObj
        }
        
        return addProductData
    }
    
    
}

struct ProductImage {
    var thumbImage : String = ""
    var image : String = ""
    
    init(json : JSON){
        thumbImage = json[ApiKey.thumbUrl].stringValue
        image = json[ApiKey.url].stringValue
    }
    
    static func returnProductImagesArray(jsonArr:[JSON]) -> [ProductImage] {
        var imageData = [ProductImage]()
        for element in jsonArr {
            imageData.append(ProductImage(json: element))
        }
        return imageData
    }
}
