//
//  Card.swift
//  TagHawk
//
//  Created by Appinventiv on 05/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

class Card {

    enum CardBrand : String {
        case visa = "Visa"
        case amex = "Amex"
        case master = "Master"
        case discover = "Discover"
    }
    
    let brand : CardBrand
    let last4 : String
    let expMonth : String
    let customer : String
    let country : String
    let id : String
    let expYear : String
    let isDefailt : Bool
    
    init(json : JSON){
        brand = CardBrand(rawValue: json[ApiKey.brand].stringValue) ?? CardBrand.visa
        last4 = json[ApiKey.last4].stringValue
        expMonth = json[ApiKey.expMonth].stringValue
        expYear = json[ApiKey.expYear].stringValue
        customer = json[ApiKey.customer].stringValue
        country = json[ApiKey.country].stringValue
        id = json[ApiKey.id].stringValue
        isDefailt = false
    }
    
}
