//
//  GiftPlan.swift
//  TagHawk
//
//  Created by Admin on 5/21/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct GiftPlan {
    
    let days : Int
    let isActive : Bool
    let id : String
    let rewardPoint : Int
    
    init(){
        days = 0
        isActive = true
        id = ""
        rewardPoint = 0
    }
    
    init(json : JSON){
        days = json[ApiKey.days].intValue
        isActive = json[ApiKey.isActive].boolValue
        id = json[ApiKey._id].stringValue
        rewardPoint = json[ApiKey.rewardPoint].intValue
    }
    
}

