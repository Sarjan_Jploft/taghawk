//
//  Category.swift
//  TagHawk
//
//  Created by Appinventiv on 24/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Category {
    
    let categoryId : String
    let categoryName : String
    let categoryImage : String
    let description : String
    let status : String
    
    init(){
        categoryId = ""
        categoryName = ""
        categoryImage = ""
        description = ""
        status = ""
    }
    
    init(json : JSON){
        categoryId = json[ApiKey._id].stringValue
        categoryName = json[ApiKey.name].stringValue
        categoryImage = json[ApiKey.imageUrl].stringValue
        description = json[ApiKey.description].stringValue
        status = json[ApiKey.status].stringValue
    }
    
    
    var getDictionary : JSONDictionary{
        var dict : JSONDictionary = [:]
        dict[ApiKey._id] = self.categoryId
        dict[ApiKey.name] = self.categoryName
        dict[ApiKey.imageUrl] = self.categoryImage
        dict[ApiKey.description] = self.description
        dict[ApiKey.status] = self.status
        return dict
    }
    
}
