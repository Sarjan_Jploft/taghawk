//
//  ShippingModel.swift
//  TagHawk
//
//  Created by Appinventiv on 09/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ShippingModel {
    var fullName: String
    var streetAddress: String
    var apartment: String
    var city: String
    var state: String
    var stateCode : String
    var country: String
    var zipcode: String
    var countryCode: String
    var phoneNumber: String
    var addressType: String
    
    
    init() {
        fullName = ""
        streetAddress = ""
        apartment = ""
        city = ""
        state = ""
        country = ""
        zipcode = ""
        countryCode = "+1"
        phoneNumber = ""
        addressType = ""
        stateCode = ""
    }
    
    init(json: JSON) {
        fullName = json[ApiKey.fullName].stringValue
        streetAddress = json[ApiKey.streetAddress].stringValue
        apartment = json[ApiKey.apartment].stringValue
        city = json[ApiKey.city].stringValue
        state = json[ApiKey.state].stringValue
        country = json[ApiKey.country].stringValue
        zipcode = json[ApiKey.zipcode].stringValue
        countryCode = json[ApiKey.countryCode].stringValue
        phoneNumber = json[ApiKey.phoneNumber].stringValue
        addressType = json[ApiKey.addressType].stringValue
        stateCode = json[ApiKey.stateCode].stringValue
    }
    
    func toJSON() -> JSONDictionary {
        var dict = JSONDictionary()
        dict[ApiKey.contact_name] = fullName
        dict[ApiKey.street1] = streetAddress
        dict[ApiKey.city] = city
        dict[ApiKey.state] = stateCode
        dict[ApiKey.postal_code] = zipcode
        dict[ApiKey.country] = country
        dict[ApiKey.phone] = countryCode + phoneNumber
        dict[ApiKey.email] = UserProfile.main.email
        dict[ApiKey.type] = addressType == LocalizedString.Residence.localized ? LocalizedString.residential.localized : LocalizedString.business.localized
        return dict
    }
}
