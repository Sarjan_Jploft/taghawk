//
//  MerchantData.swift
//  TagHawk
//
//  Created by Admin on 6/14/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

enum DepositAccountType : String {
    case bankAccount = "bank_account"
    case card = "object"
}

enum SsnAndPassportStatus : String {
    case verified = "verified"
    case unverified = "unverified"
    case pending = "pending"
}

struct MerchantData {

    let id : String
    let type : DepositAccountType
    let last4 : String
    let isPaymentEnabled : Bool
    let isAddressVerified : Bool
    let isDobVerified : Bool
    let isPhoneVerified : Bool
    let isNameVerified : Bool
    let isPassportVerified : Bool
    let isSsnVerified : Bool
//    let verified : Bool
    let ssnAndPassPortStatus : SsnAndPassportStatus
    
    init(){
         id = ""
         type =  DepositAccountType.bankAccount
         last4 = ""
         isPaymentEnabled = false
        isAddressVerified = false
        isDobVerified = false
        isPhoneVerified = false
        isNameVerified = false
        isPassportVerified = false
        isSsnVerified = false
//        verified = false
        ssnAndPassPortStatus = .pending
    }
    
    init(json : JSON){
        self.isPaymentEnabled = json[ApiKey.payoutsEnabled].boolValue
        let extAccounts = json[ApiKey.externalAccounts]
        self.id = extAccounts[ApiKey.id].stringValue
        self.last4 = extAccounts[ApiKey.last4].stringValue
        self.type = DepositAccountType(rawValue: extAccounts[ApiKey.object].stringValue) ?? DepositAccountType.bankAccount
        self.isAddressVerified = json[ApiKey.isAddressVerified].boolValue
        self.isDobVerified = json[ApiKey.dob].boolValue
        self.isPhoneVerified = json[ApiKey.phone].boolValue
        self.isNameVerified = json[ApiKey.name].boolValue
        self.isPassportVerified = json[ApiKey.passport].boolValue
        self.isSsnVerified = json[ApiKey.ssnlast4Provided].boolValue
//        self.verified = json[ApiKey.verified].boolValue
        self.ssnAndPassPortStatus = SsnAndPassportStatus(rawValue: json[ApiKey.verification][ApiKey.status].stringValue) ?? SsnAndPassportStatus.pending
    }
}
