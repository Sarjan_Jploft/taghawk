//
//  SearchResult.swift
//  TagHawk
//
//  Created by Appinventiv on 30/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

struct  SearchResult {
    
    let count : Int
    var title : String
    let id : String
    
    
    init(json : JSON){
        count = json[ApiKey.count].intValue
        title = json[ApiKey.title].stringValue
        id = json[ApiKey._id].stringValue
        
        if let tit = json[ApiKey.title].string{
            title = tit
        }else{
            title = json[ApiKey.name].stringValue
        }
    }
}
