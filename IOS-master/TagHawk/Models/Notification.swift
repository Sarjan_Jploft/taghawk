//
//  Notification.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 13/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct NotificationModel {
    let total: String?
    let page: Int?
    let totalPage: Int?
    let nextHit: Int?
    let limit: Int?
    let notificationArr: [Notifications]
    
    init(json: JSON) {
        total = json[ApiKey.total].stringValue
        page = json[ApiKey.page].intValue
        totalPage = json[ApiKey.total_page].intValue
        nextHit = json[ApiKey.next_hit].intValue
        limit = json[ApiKey.limit].intValue
        notificationArr = json[ApiKey.data].arrayValue.map({ (json) -> Notifications in
            return Notifications(json: json)
        })
    }
}

struct Notifications {
    let name: String
    let id: String
    let entityId: String
    let status: Int
    let readStatus: ReadStatus
    let message: String
    let receiverId: String
    let type: NotificationType
    let createdAt: String
    let updatedAt: String
    let created: TimeInterval
    
    init(json: JSON) {
        name = json[ApiKey.name].stringValue
        id = json[ApiKey._id].stringValue
        entityId = json[ApiKey.entityId].stringValue
        status = json[ApiKey.status].intValue
        let readStat = json[ApiKey.readStatus].intValue
        readStatus = ReadStatus(rawValue: readStat) ?? .none
        message = json[ApiKey.message].stringValue
        receiverId = json[ApiKey.receiverId].stringValue
        let notifType = json[ApiKey.type].stringValue
        type = NotificationType(rawValue: notifType) ?? .none
        createdAt = json[ApiKey.createdAt].stringValue
        updatedAt = json[ApiKey.updatedAt].stringValue
        created = json[ApiKey.created].doubleValue
    }
    
    enum ReadStatus: Int {
        case unread       = 0
        case read
        case none
    }
}

enum NotificationType: String {
    case productLike        = "PRODUCT_LIKE"
    case followed           = "FOLLOWED"
    case tagJoined          = "TAG_JOINED"
    case productAdded       = "PRODUCT_ADDED"
    case inviteCodeUsed     = "INVITE_CODE_USED"
    case tagUpdated         = "TAG_UPDATED"
    case tagJoinAccepted    = "TAG_JOIN_ACCEPTED"
    case prodAddedInCart    = "PRODUCT_ADDED_IN_CART"
    case rating             = "RATING"
    case none               = ""
    case getMessage         = "GET_MESSAGE"
    case orderStatus        = "ORDER_STATUS"
    case productSold        = "PRODUCT_SOLD"
    case jUMIOAPPROVAL     = "JUMIO_APPROVAL"
    case stripeUpdate     = "STRIPE_UPDATE"
}
