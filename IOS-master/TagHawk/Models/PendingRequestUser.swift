//
//  PendingRequestUser.swift
//  TagHawk
//
//  Created by Appinventiv on 22/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import Foundation
import SwiftyJSON
//{
//    "statusCode" : 200,
//    "data" : {
//        "page" : 1,
//        "total" : 1,
//        "total_page" : 1,
//        "next_hit" : 0,
//        "limit" : 100,
//        "data" : [
//        {
//        "joinTagBy" : 3,
//        "created" : 1555919585622,
//        "senderName" : "Burger Singh",
//        "_id" : "5cbd72e138ca343595715161",
//        "documentUrl" : [
//        "[Ljava.lang.String;@616fdb2"
//        ],
//        "isSenderVerified" : false,
//        "senderId" : "5cb587c55705f651ac01447c",
//        "requestParameter" : "",
//        "senderProfilePic" : "",
//        "requestStatus" : 4
//        }
//        ]
//    },
//    "message" : "Community Deleted Successfully"
//}

struct PendingRequest {
    
    let joinTagBy: PrivateTagType
    let userName: String
    let requestID: String
    var documentURl: [JSON] = []
    let isUserVerified: Bool
    let userID: String
    let profilePic: String
    let requestVerified: Bool
    
    init(json: JSON) {
        joinTagBy = PrivateTagType.init(type: json["joinTagBy"].intValue)
        userName = json["senderName"].stringValue
        requestID = json["_id"].stringValue
        documentURl = json["documentUrl"].arrayValue
        isUserVerified = json["isSenderVerified"].boolValue
        userID = json["senderId"].stringValue
        profilePic = json["senderProfilePic"].stringValue
        requestVerified = json["requestStatus"].intValue == 1 ? true : false
    }
    
    static func getArrayFromJSON(json: JSON) -> [PendingRequest]{
        var requests: [PendingRequest] = []
        let jsonData = json["data"]
        jsonData.arrayValue.forEach { (data) in
            requests.append(PendingRequest.init(json: data))
        }
        return requests
    }
}
