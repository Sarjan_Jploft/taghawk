//
//  Webservices.swift
//  NewProject
//
//  Created by Harsh Vardhan Kushwaha on 30/08/18.
//  Copyright © 2018 Harsh Vardhan Kushwaha. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

enum WebServices { }

extension NSError {
    
    convenience init(localizedDescription : String) {
        self.init(domain: "AppNetworkingError", code: 0, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    
    convenience init(code : Int, localizedDescription : String) {
        self.init(domain: "AppNetworkingError", code: code, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
}

extension WebServices {
    
    // MARK:- Common POST API
    //=======================
    static func commonPostAPI(parameters: JSONDictionary,
                              endPoint: EndPoint,
                              loader: Bool = true,
                              success : @escaping SuccessResponse,
                              failure : @escaping FailureResponse) {
        
        AppNetworking.POST(endPoint: endPoint.path, parameters: parameters, loader: loader, success: { (json) in
            let code = json[ApiKey.code].intValue
            let msg = json[ApiKey.message].stringValue
            

            switch code {
            case ApiCode.success: success(json)
            default: failure(NSError(code: code, localizedDescription: msg))
            }
        }) { (error) in
            failure(error)
        }
    }
    

    static func commonGetAPI(parameters: JSONDictionary,
                              endPoint: EndPoint,
                              loader: Bool = true,
                              success : @escaping SuccessResponse,
                              failure : @escaping FailureResponse) {
        
        AppNetworking.GET(endPoint: endPoint.path, parameters: parameters, loader: loader, success: { (json) in
            let code = json[ApiKey.code].intValue
            let msg = json[ApiKey.message].stringValue
            switch code {
            case ApiCode.success: success(json)
            default: failure(NSError(code: code, localizedDescription: msg))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    static func commonPutAPI(parameters: JSONDictionary,
                              endPoint: EndPoint,
                              loader: Bool = true,
                              success : @escaping SuccessResponse,
                              failure : @escaping FailureResponse) {
        
        AppNetworking.PUT(endPoint: endPoint.path, parameters: parameters, loader: loader, success: { (json) in
            let code = json[ApiKey.code].intValue
            let msg = json[ApiKey.message].stringValue
            switch code {
            case ApiCode.success: success(json)
            default: failure(NSError(code: code, localizedDescription: msg))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    // MARK:- Refresh Token
    //=====================
    static func refreshToken(success: @escaping SuccessResponse,
                             failure: @escaping FailureResponse) {
        
    
        
//        let params : [String:Any] = [ApiKey.refreshToken:UserProfile.main.refreshToken,
//                                     ApiKey.name:UserProfile.main.firstName]
//        self.commonPostAPI(parameters: params, endPoint: .refresh, success: { (json) in
//            let token = json["token"].stringValue
//            let refreshToken = json["refresh_token"].stringValue
//            UserProfile.main.token = token
//            UserProfile.main.refreshToken = refreshToken
//            UserProfile.main.saveToUserDefaults()
//            success(json)
//        }, failure: failure)
    }

    // MARK:- Sign Up
    //===============
    static func signUp(parameters: JSONDictionary,
                       success: @escaping UserControllerSuccess,
                       failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: .user, success: { (json) in
            UserProfile.main = UserProfile(json: json[ApiKey.data])
            success(UserProfile.main)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    // MARK:- Login
    //=============
    static func login(parameters: JSONDictionary,
                      success: @escaping UserControllerSuccess,
                      failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: .login, success: { (json) in
            UserProfile.main = UserProfile(json: json[ApiKey.data])
            success(UserProfile.main)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func guestLogin(parameters: JSONDictionary,
                      success: @escaping UserControllerSuccess,
                      failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: .guest, success: { (json) in
            UserProfile.main = UserProfile(json: json[ApiKey.data])
            success(UserProfile.main)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    // MARK:- Forgot Password
    //=======================
    static func forgotPassword(parameters: JSONDictionary,
                               success: @escaping SuccessResponse,
                               failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: .forgotpassword, success: success, failure: failure)
    }
    
    // MARK:- Forgot Password
    //=======================
    static func resetPassword(parameters: JSONDictionary,
                              success: @escaping SuccessResponse,
                              failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: .resetpassword, success: success, failure: failure)
    }
    
    
    static func socialLogin(parameters: JSONDictionary,
                            success: @escaping UserControllerSuccess,
                            failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters,
                           endPoint: .sociallogin,
                           success: { (json) in
            UserProfile.main = UserProfile(json: json[ApiKey.data])
            success(UserProfile.main)
        }) { (error) -> (Void) in
            failure(error)
        }
    }

    static func checkSocialLogin(parameters: JSONDictionary,
                            success: @escaping (_ data : JSON) -> (),
                            failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters,
                           endPoint: .checkSocialLogin,
                           success: { (json) in
                            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func getCategories(parameters: JSONDictionary,
                              success: @escaping (_ data : [Category]) -> (),
                              failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.category, loader: true, success: { (json) in
        
            success(json[ApiKey.data].arrayValue.map { Category(json: $0) })
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getProducts(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.product, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func getSuggestions(parameters: JSONDictionary,
                        success: @escaping (_ data : JSON) -> (),
                        failure: @escaping FailureResponse) {
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.suggestions, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getTagSuggestions(parameters: JSONDictionary,
                               success: @escaping (_ data : JSON) -> (),
                               failure: @escaping FailureResponse) {
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.tagSuggestion, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getProductDetails(parameters: JSONDictionary,
                               success: @escaping (_ data : JSON) -> (),
                               failure: @escaping FailureResponse) {
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.productDetail, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func likeUnlikeProduct(parameters: JSONDictionary,
                                  success: @escaping (_ data : JSON) -> (),
                                  failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: WebServices.EndPoint.likeUnlike, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }

    static func reportProduct(parameters: JSONDictionary,
                                  success: @escaping (_ data : JSON) -> (),
                                  failure: @escaping FailureResponse) {
        
        self.commonPutAPI(parameters: parameters, endPoint: WebServices.EndPoint.reportProduct, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }

    static func shareProduct(parameters: JSONDictionary,
                                  success: @escaping (_ data : JSON) -> (),
                                  failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.share, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }

    static func getTagData(parameters: JSONDictionary,
                            success: @escaping (_ data : JSON) -> (),
                            failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.tag, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getTagDetail(parameters: JSONDictionary,
                           success: @escaping (_ data : JSON) -> (),
                           failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.tagDetail, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func getuserTags(parameters: JSONDictionary,
                               success: @escaping (_ data : JSON) -> (),
                               failure: @escaping FailureResponse) {
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.tagUsers, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func addProduct(parameters: JSONDictionary,
                             success: @escaping (_ data : JSON) -> (),
                             failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.product, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func featureProduct(parameters: JSONDictionary,
                           success: @escaping (_ data : JSON) -> (),
                           failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.featureProduct, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func editProduct(parameters: JSONDictionary,
                                  success: @escaping (_ data : JSON) -> (),
                                  failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: WebServices.EndPoint.product, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func addToCart(parameters: JSONDictionary,
                            success: @escaping (_ data : JSON) -> (),
                            failure: @escaping FailureResponse) {
     
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.cart, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getCart(parameters: JSONDictionary,
                          success: @escaping (_ data : JSON) -> (),
                          failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.cart, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
        
    }
    
    static func logout(parameters: JSONDictionary,
                          success: @escaping (_ data : JSON) -> (),
                          failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.logout, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func getPlaces(txt:String, success: @escaping (Bool,JSONDictionaryArray) -> Void, failure: @escaping (Error) -> Void) {
        
//        let url =  "\(EndPoint.placeUrl.rawValue)/input=%5C(\(txt)&key=AIzaSyBLnYswGr0oJrDmUHU3QLCteb_WjDMgv0w"
        
        let url =  "\(EndPoint.placeUrl.rawValue)input=%5C(\(txt)&key=AIzaSyAd1acN1DZiiIg4EqBJR4-rxZIC96Rr47s"
        
        AppNetworking.GET(endPoint: url, parameters: [:], loader: false, success: { json in
            
            if let dict = json.dictionaryObject{
                
                guard let _ = dict["status"] as? String else {
                    success(false, [])
                    return }
                
                guard let data = dict["predictions"] as? JSONDictionaryArray else { return }
                
                success(true, data)
                
            }
            
        }, failure: {(e : Error) -> Void in
            
            // Handle respective error for login
            failure(e)
        })
    }
    
    static func getCordinates(placeId:String, success: @escaping (Bool,Double?,Double?) -> Void, failure: @escaping (Error) -> Void) {
        
        // let url =  "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeId)&key=AIzaSyBLnYswGr0oJrDmUHU3QLCteb_WjDMgv0w"
        
        let url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeId)&key=AIzaSyAd1acN1DZiiIg4EqBJR4-rxZIC96Rr47s"
        
        
        AppNetworking.GET(endPoint: url, parameters: [:], loader: false, success: { json in
            
            printDebug("place json is...\(json)")
            
            if let dict = json.dictionaryObject{
                guard let status = dict["status"] as? String , status == "OK" else {
                    success(false,nil,nil)
                    return }
                guard let result = dict["result"] as? JSONDictionary else {
                    success(false,nil,nil)
                    return }
                guard let geometry = result["geometry"] as? JSONDictionary else {
                    success(false,nil,nil)
                    return }
                guard let location = geometry["location"] as? JSONDictionary else {
                    success(false,nil,nil)
                    return }
                
                guard let lat = location["lat"] as? Double else {
                    success(false,nil,nil)
                    return }
                guard let lng = location["lng"] as? Double else { return }
                success(true, lat, lng)
            }
        }, failure: {(e : Error) -> Void in
            // Handle respective error for login
            failure(e)
        })
    }
    
    static func addTagDetails(parameters: JSONDictionary,
                           success: @escaping (_ data : JSON) -> (),
                           failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.tag, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func editTagDetails(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        
        self.commonPutAPI(parameters: parameters,
                          endPoint: WebServices.EndPoint.tag,
                          loader: true,
                          success: { (json) in
                            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func joinTagDetails(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.joinTag, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    

    static func getUserProfile(parameters: JSONDictionary, success: @escaping (_ data : JSON) -> (),
                                failure: @escaping FailureResponse) {
        
        AppNetworking.GET(endPoint: EndPoint.userProfile.path, parameters: parameters, loader: false, success: { (json) in

            success(json[ApiKey.data])
        }) { (error) in
            failure(error)
        }
    }
    
    static func followUser(parameters: JSONDictionary,
                               success: @escaping (_ data : JSON) -> (),
                               failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.follow, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func createCusomer(parameters: JSONDictionary,
                           success: @escaping (_ data : JSON) -> (),
                           failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.createCustomer, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func sendToken(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.charge, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
        
    }
    
    static func chargeForZeroPrice(parameters: JSONDictionary,
                          success: @escaping (_ data : JSON) -> (),
                          failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.zeroPrize, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
        
    }
    
    static func saveCard(parameters: JSONDictionary,
                          success: @escaping (_ data : JSON) -> (),
                          failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.addCard, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func cashOut(parameters: JSONDictionary,
                         success: @escaping (_ data : JSON) -> (),
                         failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.cashout, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func payOut(parameters: JSONDictionary,
                        success: @escaping (_ data : JSON) -> (),
                        failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.payouts, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    
    static func deleteCard(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters,
                          endPoint: WebServices.EndPoint.deleteCard,
                          loader: true,
                          success: { (json) in
                            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getFollowingFollowersList(parameters: JSONDictionary,
                           success: @escaping (_ data : JSON) -> (),
                           failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.friends, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func getUserProducts(parameters: JSONDictionary,
                                success: @escaping (_ data : JSON) -> (),
                                failure: @escaping FailureResponse) {
    
     AppNetworking.GET(endPoint: EndPoint.userProduct.path, parameters: parameters, headers: [:], loader: false, success: { (json) in
    
        success(json)
     }) { (error) in
        failure(error)
        }
    }
    
    static func updateProfile(parameters: JSONDictionary,
                            success: @escaping (_ data : JSON) -> (),
                            failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters,
                          endPoint: WebServices.EndPoint.updateProfile,
                          loader: true,
                          success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }

    static func removeFriend(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters,
                          endPoint: WebServices.EndPoint.removeFriend,
                          loader: true,
                          success: { (json) in
                            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func deleteProduct(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
                
        AppNetworking.DELETE(endPoint: EndPoint.product.path, parameters: parameters, headers: [:], loader: false, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) in
            failure(error)
        }
    }
    
    static func readNotification(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        
        AppNetworking.PUT(endPoint: EndPoint.readNotification.path, parameters: parameters, headers: [:], loader: false, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) in
            failure(error)
        }
    }
    
    // MARK:- RATINGS
    
    static func getRatings(parameters: JSONDictionary,
                                success: @escaping (_ data : JSON) -> (),
                                failure: @escaping FailureResponse) {
        
        AppNetworking.GET(endPoint: EndPoint.rating.path, parameters: parameters, headers: [:], loader: false, success: { (json) in
            success(json)
        }) { (error) in
            failure(error)
        }
    }
    
    static func postRating(parameters: JSONDictionary,
                           success: @escaping (_ data : JSON) -> (),
                           failure: @escaping FailureResponse) {
        
        AppNetworking.POST(endPoint: EndPoint.rating.path, parameters: parameters, headers: [:], loader: false, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) in
            failure(error)
        }
    }
    
    static func postRatingReply(parameters: JSONDictionary,
                           success: @escaping (_ data : JSON) -> (),
                           failure: @escaping FailureResponse) {
        
        AppNetworking.POST(endPoint: EndPoint.ratingReply.path, parameters: parameters, headers: [:], loader: false, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) in
            failure(error)
        }
    }
    
    static func updateDeviceToken(parameters: JSONDictionary,
                                 success: @escaping (_ data : JSON) -> (),
                                 failure: @escaping FailureResponse) {
        
        AppNetworking.PUT(endPoint: EndPoint.updateToken.path, parameters: parameters, headers: [:], loader: false, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) in
            failure(error)
        }
    }
    
    static func denyRating(parameters: JSONDictionary,
                                  success: @escaping (_ data : JSON) -> (),
                                  failure: @escaping FailureResponse) {
        
        AppNetworking.PUT(endPoint: EndPoint.rating.path, parameters: parameters, headers: [:], loader: false, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) in
            failure(error)
        }
    }
    
    static func saveBankDetails(parameters: JSONDictionary,
                                success: @escaping (_ data : JSON) -> (),
                                failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.saveBankDetails, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    

    static func verifyOtp(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: EndPoint.verifyOtp, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getBankData(parameters: JSONDictionary,
                                          success: @escaping (_ data : JSON) -> (),
                                          failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.getBankData, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
        
    }
    
    static func createMerchant(parameters: JSONDictionary,
                                success: @escaping (_ data : JSON) -> (),
                                failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.createMerchant, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }

    static func getCards(parameters: JSONDictionary,
                         success: @escaping (_ data : JSON) -> (),
                         failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.cardDetails, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func changePassword(parameters: JSONDictionary,
                               success: @escaping (_ data : JSON) -> (),
                               failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.changePassword, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func notificationSettings(parameters: JSONDictionary,
                          success: @escaping (_ data : JSON) -> (),
                          failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: EndPoint.notificationSettings, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getShippingRates(parameters: JSONDictionary,
                          success: @escaping (_ data : JSON) -> (),
                          failure: @escaping FailureResponse) {
        self.commonPostAPI(parameters: parameters, endPoint: EndPoint.shippingRates, loader: true, success: { (json) in
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func viewHtmlContent(parameters: JSONDictionary,
                            success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.contentView, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    

    static func blockUserList(parameters: JSONDictionary,
                                success: @escaping (_ data : JSON) -> (),
                                failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.blockedUsersList, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
            
    static func acceptUserToTag(parameters: JSONDictionary,
                                     success: @escaping (_ data : JSON) -> (),
                                     failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: EndPoint.acceptTagRequest, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func verifyUserEmail(parameters: JSONDictionary,
                                success: @escaping (_ data : JSON) -> (),
                                failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.verifyEmail, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func editTagData(parameters: JSONDictionary,
                     success: @escaping (_ data : JSON) -> (),
                     failure: @escaping FailureResponse){
        
        self.commonPutAPI(parameters: parameters, endPoint: .tag, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getTagProducts(parameters: JSONDictionary,
                                success: @escaping (_ data : JSON) -> (),
                                failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.tagProduct, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
            
        }
    }
    
    static func deleteTag(parameters: JSONDictionary,
                          success: @escaping (_ data : JSON) -> (),
                          failure: @escaping FailureResponse){
        
        AppNetworking.DELETE(endPoint: EndPoint.deleteTag.path, parameters: parameters, headers: [:], loader: false, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) in
            failure(error)
        }
    }
    
    static func exitGroup(parameters: JSONDictionary,
                          success: @escaping (_ data : JSON) -> (),
                          failure: @escaping FailureResponse){
        
        self.commonPutAPI(parameters: parameters, endPoint: .exitTag, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func transferOwnerShip(parameters: JSONDictionary,
                                  success: @escaping (_ data : JSON) -> (),
                                  failure: @escaping FailureResponse){
        
        self.commonPostAPI(parameters: parameters, endPoint: .transferOwnerShip, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func removeMember(parameters: JSONDictionary,
                             success: @escaping (_ data : JSON) -> (),
                             failure: @escaping FailureResponse){
        
        self.commonPutAPI(parameters: parameters, endPoint: .removeMember, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getPendingRequest(parameters: JSONDictionary,
                               success: @escaping (_ data : JSON) -> (),
                               failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.getPendingRequest, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
            
        }
    }
    
    
    static func getPromotionPackages(parameters: JSONDictionary,
                                  success: @escaping (_ data : JSON) -> (),
                                  failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.promotionPackages, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
            
        }
    }
    
    static func acceptRejectRequest(parameters: JSONDictionary,
                                    success: @escaping (_ data : JSON) -> (),
                                    failure: @escaping FailureResponse){
        
        self.commonPutAPI(parameters: parameters, endPoint: .acceptTagRequest, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func cancelTagRequest(parameters: JSONDictionary,
                                    success: @escaping (_ data : JSON) -> (),
                                    failure: @escaping FailureResponse){
        
        self.commonPutAPI(parameters: parameters, endPoint: .acceptTagRequest, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func promoteSpecificProducts(parameters: JSONDictionary,
                                  success: @escaping (_ data : JSON) -> (),
                                  failure: @escaping FailureResponse){
        
        self.commonPostAPI(parameters: parameters, endPoint: .promoteSelected, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func createLAbel(parameters: JSONDictionary,
                                        success: @escaping (_ data : JSON) -> (),
                                        failure: @escaping FailureResponse){
        
        self.commonPostAPI(parameters: parameters, endPoint: .createLabel, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
        
    }
    
    static func reportATag(parameters: JSONDictionary,
                                    success: @escaping (_ data : JSON) -> (),
                                    failure: @escaping FailureResponse){
        
        self.commonPutAPI(parameters: parameters, endPoint: .reportTag, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getBalance(parameters: JSONDictionary,
                                     success: @escaping (_ data : JSON) -> (),
                                     failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.getBalance, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
            
        }
    }
    
    static func addDebitCard(parameters: JSONDictionary,
                         success: @escaping (_ data : JSON) -> (),
                         failure: @escaping FailureResponse) {
        
        self.commonPostAPI(parameters: parameters, endPoint: WebServices.EndPoint.addDebitCard, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func updateMerchant(parameters: JSONDictionary,
                           success: @escaping (_ data : JSON) -> (),
                           failure: @escaping FailureResponse){
        
        self.commonPutAPI(parameters: parameters, endPoint: .updateMerchant, success: { (json) in
            success(json[ApiKey.data])
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func getMerchantStatus(parameters: JSONDictionary,
                           success: @escaping (_ data : JSON) -> (),
                           failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.merchant, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
            
        }
    }
    
    
    static func getCommonData(parameters: JSONDictionary,
                                success: @escaping (_ data : JSON) -> (),
                                failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.commonInfo, loader: true, success: { (json) in
            
            success(json)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func sendNotification(parameters: JSONDictionary){
        
        let jsonData = try? JSONSerialization.data(withJSONObject: parameters)
        
        let request = NSMutableURLRequest(url: URL(string: WebServices.EndPoint.sendnotification.rawValue)!,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        let headers: HTTPHeaders = [
            "Authorization": AppConstants.fireBaseServerKey.rawValue,
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
        ]
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = jsonData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if (error != nil) {
                print(error?.localizedDescription ?? "")
            } else {
                if let data = data, let json = try? JSON(data: data) {
                    print(json)
                }
            }
        })
        dataTask.resume()
    }
    
    static func getProductPaymentHistory(parameters: JSONDictionary,
                                         success: @escaping (_ data : PaymentHistory) -> (),
                                         failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.productPaymentStatus, loader: false, success: { (json) in
            
            let data = PaymentHistory(json: json[ApiKey.data])
            success(data)
            
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func uploadIdentity(parameters: [UploadFileParameter],
                               success: @escaping (_ data : JSON) -> (),
                               failure: @escaping FailureResponse){
        
    
        AppNetworking.POSTWithFiles(endPoint: WebServices.EndPoint.uploadDocuemnt.path,
                                    files : parameters,
                                    success: { (json) in
            
            success(json)
            
        }) { (error) in
            failure(error)
        }
        
    }
}
