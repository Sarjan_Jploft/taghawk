//
//  Webservices+PaymentHistory.swift
//  TagHawk
//
//  Created by Admin on 6/5/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import SwiftyJSON

extension WebServices{
    
    
    static func getPaymentHistory(parameters: JSONDictionary,
                                  success: @escaping (_ data : JSON) -> (),
                                  failure: @escaping FailureResponse) {
        
        self.commonGetAPI(parameters: parameters, endPoint: WebServices.EndPoint.paymentHistory, loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func confirmOrder(parameters: JSONDictionary,
                             success: @escaping (_ data : JSON) -> (),
                             failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: WebServices.EndPoint.confirmOrder , loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func releasePayment(parameters: JSONDictionary,
                             success: @escaping (_ data : JSON) -> (),
                             failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: WebServices.EndPoint.releasePayment , loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func acceptRefund(parameters: JSONDictionary,
                               success: @escaping (_ data : JSON) -> (),
                               failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: WebServices.EndPoint.refundAccept , loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func declineRefund(parameters: JSONDictionary,
                               success: @escaping (_ data : JSON) -> (),
                               failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: WebServices.EndPoint.refundDecline , loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    
    static func requestRefund(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: WebServices.EndPoint.refund , loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    static func releaseRefund(parameters: JSONDictionary,
                              success: @escaping (_ data : JSON) -> (),
                              failure: @escaping FailureResponse) {
        self.commonPutAPI(parameters: parameters, endPoint: WebServices.EndPoint.refundRelease , loader: true, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    static func makeDispute(parameters: JSONDictionary,
                       success: @escaping (_ data : JSON) -> (),
                       failure: @escaping FailureResponse) {
        self.commonPostAPI(parameters: parameters, endPoint: .dispute, success: { (json) in
            success(json)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    
}
