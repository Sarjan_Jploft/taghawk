//
//  Webservice+EndPoints.swift
//  NewProject
//
//  Created by Harsh Vardhan Kushwaha on 30/08/18.
//  Copyright © 2018 Harsh Vardhan Kushwaha. All rights reserved.
//

import Foundation

//Development
let baseUrl = "http://taghawkdev.appskeeper.com:7307/api/v1/"

//Staging
//let baseUrl = "http://taghawkstg.appskeeper.com:7308/api/v1/"

extension WebServices {
    
    enum EndPoint : String {
        case placeUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
        case user = "user"
        case login = "user/login"
        case sociallogin = "user/social-login"
        case signup = "signup"
        case changepassword = "change-password"
        case forgotpassword = "user/forgot-password"
        case resetpassword = "common/change-forgot-password"
        case category = "category"
        case product = "product"
        case suggestions = "product/suggestion"
        case productDetail = "product/info"
        case likeUnlike = "product-like-unlike"
        case share = "product/share"
        case reportProduct = "user-report-product"
        case tag = "tag"
        case tagSuggestion = "tag/suggestion"
        case tagDetail = "tag/info"
        case tagUsers = "tag/user"
        case featureProduct = "product-promotion"
<<<<<<< HEAD
        case cart = "cart"
        case logout = "user/logout"
        
=======
        case joinTag = "tag/request-member"
>>>>>>> 69c27053c5aeaf887379a0e7636ad13d88affb7a
        //.....
        
        case profiledata
        case managefriends = "manage-friend"
        case guest = "user-guest"
        case userProfile = "user/profile"
        case userProduct = "user/product"
        
        case refresh

        var path : String {
            
            let url = baseUrl
            return url + self.rawValue
        }
    }
}
