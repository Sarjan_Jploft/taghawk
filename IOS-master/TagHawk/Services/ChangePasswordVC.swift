//
//  ChangePasswordVC.swift
//  TagHawk
//
//  Created by Appinventiv on 06/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

struct ChangePasswordInput {
    
    var oldPass : String = ""
    var newPass : String = ""
    var confirmPass : String = ""
    
    init(){
        
    }
    
}

class ChangePasswordVC : BaseVC {

    @IBOutlet weak var changePasswordTableView: UITableView!
    @IBOutlet weak var updateButton: UIButton!
    
    var userInput = ChangePasswordInput()
    let settingsControler = SettingsControler()
    let controler = ChangePasswordControler()
    
    private var validationStateArray : [(type : ChangePasswordCell.PasswordFieldType ,state : TextFieldState)] = [
        (ChangePasswordCell.PasswordFieldType.oldPassword,TextFieldState.none),
        (ChangePasswordCell.PasswordFieldType.newPassword,TextFieldState.none),
        (ChangePasswordCell.PasswordFieldType.confirmPassword,TextFieldState.none)
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
        self.settingsControler.logoutDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func updateButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.controler.changePassword(userInput: self.userInput)
    }
    
}

extension ChangePasswordVC{
    
    func setUpSubView(){
        self.updateButton.setAttributes(title: LocalizedString.Update.localized , font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)
        self.configureTableView()
    }
    
    func configureTableView(){
        self.changePasswordTableView.registerNib(nibName: ChangePasswordCell.defaultReuseIdentifier)
        self.changePasswordTableView.separatorStyle = .none
        self.changePasswordTableView.delegate = self
        self.changePasswordTableView.dataSource = self
    }
    
    
    func changePasswordSuccessPopUp(){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = .changePasswordSuccess
        vc.delegateWithType = self
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension ChangePasswordVC {
    
    func configureNavigation(){
        self.navigationItem.title = LocalizedString.changePassword.localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.leftBarItemImage(change: AppImages.backBlack.image, ofVC: self)
    }
}


//MARK:_ Table view datasource and delegates
extension ChangePasswordVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ChangePasswordCell.defaultReuseIdentifier) as? ChangePasswordCell else {
            fatalError("SettingsVC....\(ChangePasswordCell.defaultReuseIdentifier) cell not found") }
        cell.delegate = self
        cell.setUpFor = self.validationStateArray[indexPath.row].type
        cell.didShowError(state: self.validationStateArray[indexPath.row].state)
        cell.populateText(type: self.validationStateArray[indexPath.row].type, userInput: self.userInput)
        return cell
    }
}

extension ChangePasswordVC : ChangePasswordFieldDelagate{

    func texfieldDidChange(type : ChangePasswordCell.PasswordFieldType, text : String?){
        
        switch type {
        case .oldPassword:
            userInput.oldPass = text ?? ""
       
        case .newPassword:
            userInput.newPass = text ?? ""
            
        case .confirmPassword:
            userInput.confirmPass = text ?? ""
      
        }
    }
    
    func textFieldShouldReturn(type : ChangePasswordCell.PasswordFieldType){
        self.view.endEditing(true)
        
    }
}


extension ChangePasswordVC : ChangePasswordControllerDelegate {
    
    func willChangePassword(){
        self.view.showIndicator()
        
    }
    
    func changePasswordSuccess(){
        self.view.hideIndicator()
        self.changePasswordSuccessPopUp()
    }
    
    func failedToChangePassword(message: String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
    }
    
    func invalidInput(textFieldType : ChangePasswordCell.PasswordFieldType, textFieldState : TextFieldState){
        if let ind = self.validationStateArray.firstIndex(where: { (value) -> Bool in
            value.type == textFieldType
        }){
            self.validationStateArray[ind].state = textFieldState
            self.changePasswordTableView.reloadRows(at: [IndexPath(row: ind, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
}


extension ChangePasswordVC : CustomPopUpDelegateWithType{
    
    func okTapped(type : CustomPopUpVC.CustomPopUpFor, product : CartProduct?) {
        self.settingsControler.logout()
    }
    
    func noButtonTapped(type : CustomPopUpVC.CustomPopUpFor, product : CartProduct?){
        
    }
    
    func yesButtonTapped(type : CustomPopUpVC.CustomPopUpFor, product : CartProduct?){
        
    }
    
}


extension ChangePasswordVC : LogoutDelegate {
    
    func willLogout(){
        self.view.showIndicator()
        
    }
    
    func logoutSuccessFully(){
        self.view.hideIndicator()
        AppNavigator.shared.actionsOnLogout()
    }
    
    func failedToLogout(message : String){
        self.view.hideIndicator()
        
    }
    
}
