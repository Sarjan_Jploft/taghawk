//
//  Webservice+EndPoints.swift
//  NewProject
//
//  Created by Harsh Vardhan Kushwaha on 30/08/18.
//  Copyright © 2018 Harsh Vardhan Kushwaha. All rights reserved.
//

import Foundation

//Development
//let baseUrl = "https://taghawkdev.appskeeper.com/api/v1/"
//let baseUrl = "http://13.125.247.137:2000/api/v1/"
//let baseUrl = "http://54.180.152.39:2000/api/v2/"
//let baseUrl = "http://13.209.26.65:2000/api/v2/"



//Staging

//let baseUrl = "https://taghawkstg.appskeeper.com/api/v1/"

//Live
//let baseUrl = "https://api.taghawk.app/api/v1/"
let baseUrl = "https://api.taghawk.app/api/v2/"

//let baseUrl = "http://10.10.8.50:3000/api/v1/"



extension WebServices {
    
    enum EndPoint : String {
        case placeUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
//        case placeUrl = "https://caltexweb.appinventive.com/privacy-policy"
        case privacy = "http://caltexadminst.appinventive.com/term-condition"
        case terms = "1"
        case user = "user"
        case login = "user/login"
        case sociallogin = "user/social-login"
        case checkSocialLogin = "user/checkSocialLogin"
        case signup = "signup"
        case forgotpassword = "user/forgot-password"
        case resetpassword = "common/change-forgot-password"
        case category = "category"
        case product = "product"
        case suggestions = "product/suggestion"
        case productDetail = "product/info"
        case likeUnlike = "product-like-unlike"
        case share = "product/share"
        case reportProduct = "user-report-product"
        case tag = "tag"
        case tagSuggestion = "tag/suggestion"
        case tagDetail = "tag/info"
        case tagUsers = "tag/user"
        case featureProduct = "product-promotion"
        case cart = "cart"
        case logout = "user/logout"
        case joinTag = "tag/request-member"
        case profiledata
        case managefriends = "manage-friend"
        case guest = "user-guest"
        case userProfile = "user/profile"
        case userProduct = "user/product"
        case notificationList = "notification/list"
        case updateProfile = "user/update-profile"
        case readNotification = "notification/mark-read"
        case verifyOtp = "user/verify-phone"
        case follow = "user/friend-follow"
        case friends = "user/friends"
        case removeFriend = "user/friend-remove"
        case rating = "rating"
        case ratingReply = "rating/reply"
        case createCustomer = "payment/create/Customer"
        case updateToken = "user/update-device-token"
        case refresh
        case charge = "payment/create/charges"
        case saveBankDetails = "payment/save/bankDetails"
        case getBankData = "payment/bankData"
        case createMerchant = "payment/create/Merchant"
        case cardDetails = "payment/user/card-details"
        case addCard = "payments/add-card"
        case deleteCard = "payments/delete-card"
        case deviceId = "deviceId"
        case changePassword = "user/change-password"
        case notificationSettings = "user/notification-setting"
        case shippingRates = "shipping-rates"
        case contentView = "content/view"
        case blockedUsersList = "user/blocked-list"
        case acceptTagRequest = "tag/accept-reject-request"
        case tagProduct = "tag/products_new"
        case verifyEmail = "user/verifyEmail"
        case zeroPrize = "product/buy/zeroPrize"
        case deleteTag = "tag/delete/owner"
        case exitTag = "tag/exit"
        case transferOwnerShip = "tag/transfer-ownership"
        case removeMember = "tag/remove-member"
        case getPendingRequest = "tag/pending-request"
        case sendnotification = "https://fcm.googleapis.com/fcm/send"
         case cashout = "payment/cashout"
        case promotionPackages = "promotion-packages"
        case promoteSelected = "product-promotion-selective"
        case createLabel = "shipping-labels"
        case reportTag = "tag/report"
        case paymentHistory = "payment/history"
        case refund = "order/refund"
        case releasePayment = "payment/release"
        case confirmOrder = "order/confirm"
        case refundAccept = "payment/refund/accept"
        case refundDecline = "payment/refund/decline"
        case refundRelease = "payment/refund/release"
        case dispute = "payment/dispute"
        case productPaymentStatus = "product/order/details"
        case getBalance = "user/balance"
        case addDebitCard = "payment/addDebitCard"
        case merchant = "payment/merchant"
        case updateMerchant = "payment/update/Merchant"
        case uploadDocuemnt = "payment/upload/identity"
        case payouts = "payment/payouts"
        case commonInfo = "user/common/info"
        
        var path : String {
            let url = baseUrl
            return url + self.rawValue
        }
    }
}
