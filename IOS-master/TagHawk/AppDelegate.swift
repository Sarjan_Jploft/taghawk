//
//  AppDelegate.swift
//  TagHawk
//
//  Created by Appinventiv on 17/01/19.
//  Copyright © 2019 TagHawk. All1 rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import FBSDKLoginKit
import GoogleMaps
import CoreBluetooth
import GooglePlaces
import AWSS3
import AWSCore
import Firebase
import Stripe
import FirebaseMessaging
import FirebaseInstanceID
import FirebaseDatabase
import FBSDKCoreKit
import Branch

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let shared = UIApplication.shared.delegate as! AppDelegate
    var applicationDidBecomeActive: (() -> ())?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        
        STPPaymentConfiguration.shared().publishableKey = StripeInfo.apiKey.rawValue
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        AppNavigator.shared.configureNavigationBarAppearance()
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        GMSServices.provideAPIKey("AIzaSyANZPqYcRSAmiL8D6RUk4CG0PDcQdXSkd4")
        //let S3_PoolId = "us-east-1:b1f250f2-66a7-4d07-96e9-01817149a439"
       
      let S3_PoolId = "us-east-1:2761e492-b863-41a7-a8eb-373a1d73375b"

        
//        let S3_PoolId = "us-east-1:03f05ee6-cd9a-4a0f-87d4-b3a2c865cab8"
        
        AWSController.setupAmazonS3(withPoolID: S3_PoolId)
//        Fabric.with([Crashlytics.self])
        Fabric.sharedSDK().debug = false
        FirebaseApp.configure()
        Crashlytics.sharedInstance().setUserEmail("sarjan.jploft@gmail.com")
        Crashlytics.sharedInstance().setUserIdentifier("qaapp123")
        Crashlytics.sharedInstance().setUserName("QA")
        Messaging.messaging().delegate = self
        self.registerUserNotifications()
        UserProfile.main.accessToken.isEmpty ? AppNavigator.shared.goToLogin() : AppNavigator.shared.goToHomeVC()
        self.window?.backgroundColor = UIColor.white
        CommonFunctions.getCurrentCountryCode()
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // if you are using the TEST key
        Branch.setUseTestBranchKey(false)
        // listener for Branch Deep Link data
        Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
          // do stuff with deep link data (nav to page, display content, etc)
          print(params as? [String: AnyObject] ?? {})
        }
        
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        guard let navVcs = AppNavigator.shared.tabBar?.viewControllers else { return }
        guard let homeNavigation = navVcs[0] as? UINavigationController else { return }
        let allNavigationVcs = homeNavigation.viewControllers
        for item in allNavigationVcs {
            if item.isKind(of: HomeVC.self){
                guard let homeObj = item as? HomeVC else { return }
                homeObj.getCommonData()
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()
        applicationDidBecomeActive?()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication,
                     handleEventsForBackgroundURLSession identifier: String,
                     completionHandler: @escaping () -> Void) {
        
        //provide the completionHandler to the TransferUtility to support background transfers.
        AWSS3TransferUtility.interceptApplication(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }

    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        printDebug(url)
        
        if url.scheme == AppConstantKeys.fbId.rawValue {
            let facebookDidHandle =  ApplicationDelegate.shared.application(
                app,
                open: url as URL,
                sourceApplication: options[.sourceApplication] as? String,
                annotation: options[.annotation] as Any)
            return facebookDidHandle
        }else if url.scheme == AppConstantKeys.BranchKey.rawValue{
            Branch.getInstance().application(app, open: url, options: options)
            return true
        }else{
            let urlWithEqual = url.absoluteString.replacingOccurrences(of: "&#x3D;", with: "=")
            
            guard let convertedUrl = URL(string : urlWithEqual) else { return false }
            
            let items = convertedUrl.queryItems
            
            let type = Int(items[ApiKey.entrance] ?? "")
            
            let deepLinkType = DeepLinkNavigateType(rawValue: type ?? 0) ?? DeepLinkNavigateType.none
                
                switch deepLinkType {
                    
                case .none:
                    
                    if let token = items[ApiKey.token] {
                        verifyUserEmail(token: token)
                    }
                    break
                case .EMAIL_VERIFICATION:
                    break
               
                case .FORGOT_PASSWORD:
                    if UserProfile.main.accessToken.isEmpty{
                        DispatchQueue.main.async {
                            let vc = ResetPasswordVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
                            vc.resetPasswordToken = items[ApiKey.token] ?? ""
                            AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
                        }
                    }
             
                case .PRODUCT_SHARE:
                    navigateToProdDetails(id: items[ApiKey.id] ?? "")
             
                case .COMMUNITY_INVITE:
                    navigateToTagDetails(id: items[ApiKey.id] ?? "")
              
                case .COMMUNITY_REQUEST:
                    acceptUserToTag(userId: items[ApiKey.user_id] ?? "", communityId: items[ApiKey.id] ?? "")
                
                case .PROFILE_SHARE:
                    navigateToOtherUserProfile(id:  items[ApiKey.id] ?? "")
                }
            
            
    
            
            return true
        }
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      // handler for Universal Links
      Branch.getInstance().continue(userActivity)
      return true
    }
    
    
    private func navigateToProdDetails(id: String) {
        let productScene = ProductDetailVC.instantiate(fromAppStoryboard: .Home)
        productScene.prodId = id
        AppNavigator.shared.parentNavigationControler.pushViewController(productScene, animated: true)
    }
    
    private func navigateToTagDetails(id: String) {
        let tagScene = TagDetailVC.instantiate(fromAppStoryboard: .AddTag)
        tagScene.tagId = id
        AppNavigator.shared.parentNavigationControler.pushViewController(tagScene, animated: true)
    }
    
    private func navigateToOtherUserProfile(id: String) {
        let userProfileScene = OtherUserProfileVC.instantiate(fromAppStoryboard: .Profile)
        userProfileScene.userId = id
        AppNavigator.shared.parentNavigationControler.pushViewController(userProfileScene, animated: true)
    }
    
    internal func acceptUserToTag(userId: String, communityId: String) {
        
        let params: JSONDictionary = [ApiKey.userId: userId, ApiKey.communityId: communityId, ApiKey.status: JoinTagStatus.ACTIVE.rawValue]
        print("deep link api param",params)
        DispatchQueue.global(qos: .background).async {
            
            WebServices.acceptUserToTag(parameters: params, success: { (json) in
                
                let tagData = Tag(json: json[ApiKey.data])
                DispatchQueue.main.async {
                    
                    if let tagDetailVC = AppNavigator.shared.parentNavigationControler.topViewController as? TagDetailVC,
                        tagDetailVC.tag.id == tagData.id {
                        tagDetailVC.updateDataFromPush(data: tagData)
                    } else {
                        let tagScene = TagDetailVC.instantiate(fromAppStoryboard: .AddTag)
                        tagScene.tagId = tagData.id
                        GroupChatFireBaseController.shared.joinGroup(tagData: tagData, data: UserProfile.main)
                        AppNavigator.shared.parentNavigationControler.pushViewController(tagScene, animated: true)
                        //              self.navigateToTagDetails(id: communityId)
                    }
                }
                
            }) { (err) -> (Void) in
                
            }
        }
    }
    
    private func verifyUserEmail(token: String) {
        
        let params: JSONDictionary = [ApiKey.token: token]
        
        WebServices.verifyUserEmail(parameters: params, success: { (json) in
            
            CommonFunctions.showToastMessage(json[ApiKey.message].stringValue)
            if let editProfileVC = AppNavigator.shared.parentNavigationControler.presentedViewController as? EditProfileVC {
             
                editProfileVC.verifySocialProfileButton[1].backgroundColor = #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1)
                editProfileVC.verifySocialProfileButton[1].setTitleColor(.white)
                editProfileVC.verifySocialProfileButton[1].setTitle("Verified")
                
                UserProfile.main.isEmailVerified = true
                UserProfile.main.saveToUserDefaults()
                
            }
        }) { (err) -> (Void) in
     
        }
    }
}

