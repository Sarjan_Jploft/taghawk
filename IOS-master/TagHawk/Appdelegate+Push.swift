//
//  Appdelegate+Push.swift
//  TagHawk
//
//  Created by Appinventiv on 09/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import UserNotifications
import FirebaseMessaging
import FirebaseInstanceID
import SwiftyJSON
import Branch

extension AppDelegate : UNUserNotificationCenterDelegate{
    
    func registerUserNotifications() {
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { (grant, error)  in
                if error == nil, grant {
                    DispatchQueue.main.async {
                        self.registerUNNotificationCategory()
                    }
                }
            })
        } else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    private func registerUNNotificationCategory() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().setNotificationCategories([])
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        //        Auth.auth().setAPNSToken(deviceToken, type: .sandbox)
        //        Auth.auth().setAPNSToken(deviceToken, type: .prod)
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
//        DeviceDetail.deviceToken = token
        AppUserDefaults.save(value: token, forKey: AppUserDefaults.Key.deviceToken)
        NSLog("%@", "Decice token = \(token)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        printDebug("present: \(notification.request.content.userInfo)")
        
        let notifData = notification.request.content.userInfo
        guard let type = notifData[ApiKey.type] as? String else{
            completionHandler([])
            return }
        let entityID = notifData[ApiKey.entityId] as? String ?? ""

        switch type {
            
        case NotificationType.orderStatus.rawValue:
            let topViewController = AppNavigator.shared.parentNavigationControler.topViewController
            AppNavigator.shared.refreshAccountInfoTab(id: entityID)

            if let controller = topViewController as? SingleChatVC{
                
                let userID = notifData["userId"] as? String ?? ""
                let sellerID = notifData["sellerId"] as? String ?? ""
                
                controller.refreshProductStatus(userId : userID,
                                                sellerId: sellerID,
                                                entityId: entityID)
                completionHandler([])
            }else if let controller = topViewController as? AccountInfoAndPaymentHistoryVC {
                controller.paymentHistoryVc.refreshProductStatus(entityId: entityID)
                completionHandler([])
            }
            
        case NotificationType.productSold.rawValue:
            
            let topViewController = AppNavigator.shared.parentNavigationControler.topViewController
            AppNavigator.shared.refreshAccountInfoTab(id: entityID)

            if let controller = topViewController as? SingleChatVC{
                
                let userID = notifData["userId"] as? String ?? ""
                let sellerID = notifData["sellerId"] as? String ?? ""
                
                controller.refreshProductStatus(userId : userID,
                                                sellerId: sellerID,
                                                entityId: entityID)
                completionHandler([])
            }
            
        case NotificationType.getMessage.rawValue:
            
            guard UIApplication.shared.applicationState != .active else{ completionHandler([])
                return }
            if !UserProfile.main.isNotificationMuted {
                let topViewController = AppNavigator.shared.parentNavigationControler.topViewController
                if let tabBar = topViewController as? TagHawkTabBarControler, tabBar.selectedIndex == 2{
                    completionHandler([])
                }else{
                    if (topViewController is SingleChatVC) || (topViewController is GroupChatVC){
                        completionHandler([])
                    }
                }
            }else{
                completionHandler([])
            }
            
        case NotificationType.tagJoinAccepted.rawValue:
            
            if UIApplication.shared.applicationState == .active {
                if acceptUserToTag(communityId: notifData[ApiKey.entityId] as? String ?? "") {
                    completionHandler([])
                } else {
                    completionHandler([.alert, .sound, .badge])
                }
            }else{
                completionHandler([.alert, .sound, .badge])
            }
            
        default:
            completionHandler([.alert, .sound, .badge])
        }
        
        completionHandler([.alert, .sound, .badge])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if application.applicationState == .inactive || application.applicationState == .background {
            
            if let aps = userInfo["aps"] as? [String: Any],
                let badge = aps["badge"] as? Int{
                application.applicationIconBadgeNumber = badge
            }
        }
        //Branch
        Branch.getInstance().handlePushNotification(userInfo)
        //        if application.applicationState == .background {
        performNotificationAction(userInfo, application: application)
        //        }
    }
    
    
    func performNotificationAction(_ userInfo: [AnyHashable: Any], application: UIApplication) {
        
        if let id = userInfo[ApiKey.entityId] as? String,
            let notifType = userInfo[ApiKey.type] as? String {
            let type = NotificationType(rawValue: notifType) ?? .none
            
            switch type {
                
            case .productLike:
                navigateToProdDetails(id: id)
                
            case .tagJoined:
                navigateToTagDetails(id: id)
                
            case .followed:
                let followersScene = FollowingFollowersVC.instantiate(fromAppStoryboard: .Profile)
                followersScene.commingFor = .followers
                followersScene.userId = UserProfile.main.userId
                AppNavigator.shared.parentNavigationControler.pushViewController(followersScene, animated: true)
                
            case .productAdded:
                navigateToProdDetails(id: id)
                
            case .inviteCodeUsed:
                CommonFunctions.showToastMessage(LocalizedString.underDevelopment.localized)
                
            case .tagUpdated:
                navigateToTagDetails(id: id)
                
            case .tagJoinAccepted:
                navigateToTagDetails(id: id)
                
            case .prodAddedInCart:
                navigateToCart(id: id)
                
            case .rating:
                navigateToRating(id: id)
                
            case .getMessage:
                
                if application.applicationState != .active {
                    
                    if let data = userInfo["gcm.notification.roomData"] as? String{
                        if let dataVal = data.data(using: .utf8){
                            let jsonVal = try? JSON(data: dataVal)
                            let roomData = ChatListing(json: JSON(jsonVal ?? JSON()))
                            let topViewController = AppNavigator.shared.parentNavigationControler.topViewController
                            
                            if let controller = topViewController as? TagHawkTabBarControler,
                                controller.selectedIndex != 2{
                                
                                //                            guard application.applicationState != .active else{ return }
                                
                                if roomData.chatType == .single{
                                    
                                    if !(topViewController is SingleChatVC){
                                        let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
                                        scene.vcInstantiated = .messageList
                                        scene.roomInfo = roomData
                                        
                                        if let idx = SingleChatFireBaseController.shared.checkRoomExistIndex(roomId: roomData.roomID, userId: UserProfile.main.userId){
                                            SingleChatFireBaseController.shared.usersRoom[idx].isExist = true
                                        }else{
                                            let data: (roomID: String, userID: String, isExist: Bool) = (roomID: roomData.roomID, userID: UserProfile.main.userId, isExist: true)
                                            SingleChatFireBaseController.shared.usersRoom.append(data)
                                        }
                                        SingleChatFireBaseController.shared.checkAnotherUserRemoveRoom(roomId: roomData.roomID,
                                                                                                       otherUserID: roomData.otherUserID)
                                        AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
                                    }
                                }else{
                                    if !(topViewController is GroupChatVC){
                                        let scene = GroupChatVC.instantiate(fromAppStoryboard: .Messages)
                                        scene.roomData = roomData
                                        AppNavigator.shared.parentNavigationControler.pushViewController(scene, animated: true)
                                    }
                                }
                            }
                        }
                    }
                }
                
            case .orderStatus, .productSold:
                let vc = AccountInfoAndPaymentHistoryVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
                vc.commingFrom = .feomProfile
                vc.selectedTab = AccountInfoAndPaymentHistoryVC.CurrentlySelectedHomeTab.history
                AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
                
            case .stripeUpdate:
            let vc = AccountInfoAndPaymentHistoryVC.instantiate(fromAppStoryboard: AppStoryboard.Settings)
            vc.commingFrom = .feomProfile
            vc.selectedTab = AccountInfoAndPaymentHistoryVC.CurrentlySelectedHomeTab.accountInfo
            AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
                
            default:
                break
                
            }
        }
    }
    
    func navigateToCart(id: String) {
        let cartScene = CartVC.instantiate(fromAppStoryboard: .Home)
        cartScene.commingFor = .productDetails
        AppNavigator.shared.parentNavigationControler.pushViewController(cartScene, animated: true)
    }
    
    private func navigateToProdDetails(id: String) {
        let productScene = ProductDetailVC.instantiate(fromAppStoryboard: .Home)
        productScene.prodId = id
        AppNavigator.shared.parentNavigationControler.pushViewController(productScene, animated: true)
    }
    
    private func navigateToTagDetails(id: String) {
        let tagScene = TagDetailVC.instantiate(fromAppStoryboard: .AddTag)
        tagScene.tagId = id
        AppNavigator.shared.parentNavigationControler.pushViewController(tagScene, animated: true)
    }
    
    private func navigateToRating(id: String) {
        let ratingVC = RatingVC.instantiate(fromAppStoryboard: .Profile)
        var userModel = UserProfile()
        userModel.profilePicture = UserProfile.main.profilePicture
        userModel.fullName = UserProfile.main.fullName
        userModel.created = UserProfile.main.created
        userModel.rating = UserProfile.main.rating
        userModel.id = UserProfile.main.userId
        ratingVC.profileData = userModel
        AppNavigator.shared.parentNavigationControler.pushViewController(ratingVC, animated: true)
    }
    
    private func acceptUserToTag(communityId: String) -> Bool {
        if let tagDetailVC = AppNavigator.shared.parentNavigationControler.topViewController as? TagDetailVC{
            tagDetailVC.updatedChatStatus(id: communityId)
            return true
        }
        return false
    }
}


extension AppDelegate: MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        printDebug("fcmToken...\(fcmToken)")
        DeviceDetail.fcmToken = fcmToken
        if !UserProfile.main.accessToken.isEmpty {
            AppNavigator.shared.tabBar?.homeControler.updateDeviceToken()
        }
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
    }
    
}
