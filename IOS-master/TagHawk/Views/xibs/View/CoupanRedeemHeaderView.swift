//
//  CoupanRedeemHeaderViewCollectionReusableView.swift
//  TagHawk
//
//  Created by Admin on 5/21/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CoupanRedeemHeaderView : UICollectionReusableView {

    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var pointsRemainingLabel: UILabel!
    @IBOutlet weak var totalPointsLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var planPointsLabel: UILabel!
    @IBOutlet weak var dayPromotionLabel: UILabel!
    @IBOutlet weak var redeemForLabel: UILabel!
    @IBOutlet weak var planBackView: UIView!
    @IBOutlet weak var selectItemLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lineView.backgroundColor = AppColors.textfield_Border
        self.planBackView.round(radius: 10)
        self.planBackView.setBorder(width: 1.0, color: AppColors.textfield_Border)
        self.dayPromotionLabel.setAttributes(text: LocalizedString.One_Day_Promotion.localized, font: AppFonts.Galano_Bold.withSize(20), textColor: AppColors.appBlueColor)
        self.redeemForLabel.setAttributes(text: LocalizedString.Redem_For.localized, font: AppFonts.Galano_Medium.withSize(11), textColor: AppColors.lightGreyTextColor)
        self.planPointsLabel.setAttributes(text: "100 Points", font: AppFonts.Galano_Semi_Bold.withSize(16), textColor: UIColor.black)
        self.totalPointsLabel.setAttributes(text: "400 Points", font: AppFonts.Galano_Bold.withSize(20), textColor: AppColors.appBlueColor)
        self.totalLabel.setAttributes(text: LocalizedString.Total.localized, font: AppFonts.Galano_Medium.withSize(12), textColor: AppColors.lightGreyTextColor)
        self.pointsRemainingLabel.setAttributes(text: "", font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.textfield_Border)
        self.selectItemLabel.setAttributes(text: LocalizedString.Select_Items_To_Promote.localized, font: AppFonts.Galano_Medium.withSize(16), textColor: UIColor.black)
    }
 
    func populateData(plan : GiftPlan, totalPoints : Int){
        self.totalPointsLabel.text = "\(totalPoints) \(LocalizedString.Points.localized)"
        self.dayPromotionLabel.text = "\(plan.days) \(LocalizedString.Day_Promotio.localized)"
        self.planPointsLabel.text = "\(plan.rewardPoint) \(LocalizedString.Points.localized)"
    }
    
}
