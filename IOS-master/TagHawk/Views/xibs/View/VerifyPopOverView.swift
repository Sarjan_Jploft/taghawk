//
//  VerifyPopOverView.swift
//  TagHawk
//
//  Created by Vikash on 09/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol VerifyProfileDelegate: class {
    func openEditProfile()
}

class VerifyPopOverView: UIView {

    @IBOutlet weak var verifyLabel: UILabel!
    
    @IBAction func clickVerifyButton(_ sender: UIButton) {
        
    }
    var label: UILabel = UILabel()
    
    weak var delegate: VerifyProfileDelegate?
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        backgroundColor = .clear
        
        label.frame = CGRect(x: 15, y: 20, width: Int(self.layer.frame.width - 30), height: 45)
        label.numberOfLines = 0
        label.textColor = AppColors.blueTextColor
        label.font = AppFonts.Galano_Regular.withSize(12.0)
        label.textAlignment = NSTextAlignment.center
        label.text = "Verify phone number and official ID to become a verified seller."
        self.addSubview(label)

        let btn: UIButton = UIButton()
        btn.frame =  CGRect(x: 15, y: 60, width: 120, height: 50)
        btn.setTitleColor(AppColors.appBlueColor)
        btn.titleLabel?.textAlignment = .left
        btn.titleLabel?.font = AppFonts.Galano_Medium.withSize(12.0)
        btn.setTitle("Click here to verify", for: UIControl.State.normal)
        btn.addTarget(self, action: #selector(self.verifyProfileButton(_:)), for: UIControl.Event.touchUpInside)
        self.addSubview(btn)
    }
    
    @objc func verifyProfileButton(_ sender: UIButton) {
       self.delegate?.openEditProfile()
    }
    
}
