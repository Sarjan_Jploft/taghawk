//
//  MarkerInfoWindowView.swift
//  TagHawk
//
//  Created by Appinventiv on 12/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class MarkerInfoWindowView : UIView {

    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.numberOfLines = 0
        addressLabel.numberOfLines = 0
        self.addressLabel.textColor = AppColors.lightGreyTextColor
    
    }
    
}
