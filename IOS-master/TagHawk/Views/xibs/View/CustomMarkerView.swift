//
//  CustomMarkerView.swift
//  TagHawk
//
//  Created by Appinventiv on 27/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CustomMarkerView: UIView {

    @IBOutlet weak var markerImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        nameLabel.numberOfLines = 2
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        profileImageView.round()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        self.profileImageView.backgroundColor = UIColor.clear
        self.markerImageView.backgroundColor = UIColor.clear
        self.nameLabel.font = AppFonts.Galano_Regular.withSize(13)
    }
    
    func populateData(tag : Tag) {
        profileImageView.setImage_kf(imageString: tag.tagImageUrl, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        nameLabel.text = tag.name
        markerImageView.image = tag.isMember ? AppImages.greenMarker.image : AppImages.blueMarker.image
    }
}
