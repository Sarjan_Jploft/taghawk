//
//  LoaderView.swift
//  CaltexMusic
//
//  Created by Nimish Sharma on 05/07/18.
//  Copyright © 2018 CaltexMusic. All rights reserved.
//

import UIKit
import Lottie

class LoaderView: UIView {
    
    @IBOutlet weak var loaderContainerView: UIView!
    
   // let loader = LOTAnimationView(name: "trail_loading")
      let loader = AnimationView(name: "trail_loading")

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupSubviews()
        self.addObservers()
    }
    
    deinit {
        self.removeObservers()
    }
    
    private func addObservers() {
    
        NotificationCenter.default.addObserver(self, selector: #selector(pauseAnimation), name: UIApplication.willResignActiveNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(resumeAnimation), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    private func setupSubviews() {
        subviews.forEach {$0.backgroundColor = .clear}
        backgroundColor = .clear
        loader.frame = loaderContainerView.bounds
        loader.contentMode = .scaleAspectFill
        loaderContainerView.addSubview(loader)
        //loader.loopAnimation = true
        loader.loopMode = .loop
        loader.play()
    }

    
    @objc func pauseAnimation() {
        loader.pause()
    }
    
    @objc func resumeAnimation() {
        loader.play()
    }
    
    
}

/*
 this extension is kept here because "tag" property is being used here to identify the view at the time of its removal.
 */
let loaderViewTagValue = 9000
extension UIView {
    
    func showIndicator() {
        let view: LoaderView = .instantiateFromNib()
        view.tag = loaderViewTagValue
        view.frame = CGRect(x: 0, y: 0, width: width, height: height)
        addSubview(view)
    
//        self.showIndicatorView()
    }
    
    func hideIndicator() {
        
    DispatchQueue.main.async {
            
        self.subviews.forEach {
            if $0.tag == loaderViewTagValue, let loaderView = $0 as? LoaderView {
                loaderView.pauseAnimation()
                $0.removeFromSuperview()
            }
        }
       
    }
//        self.hideIndicatorView()
        
    }
}
