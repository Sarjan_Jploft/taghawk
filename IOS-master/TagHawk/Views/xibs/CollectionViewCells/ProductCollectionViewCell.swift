//
//  ProductCollectionViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 30/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    enum ProductCellFor {
        case productDetail
        case home
        case tagDetail
    }
    
    @IBOutlet weak var promoteImgView: UIImageView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var blackOverLayView: UIView!
    @IBOutlet weak var selectedImgView: UIImageView! {
        didSet {
            blackOverLayView.isHidden = selectedImgView.isHidden ? true : false
        }
    }
    
    @IBOutlet weak var selectImageView: UIImageView!
    
    var cellFor = ProductCellFor.home{
    
        didSet{
        if self.cellFor == .home{
            self.contentView.round(radius: 10)
            self.drawShadow(shadowOpacity: 0.15)
            self.clipsToBounds = false
        }else if self.cellFor == .tagDetail{
//            self.contentView.round(radius: 10)
//            self.setBorder(width: 0.5, color: AppColors.textfield_Border)
//            self.contentView.round(radius: 10)
            
            self.contentView.round(radius: 10)
            self.drawShadow(shadowOpacity: 0.15)
            self.clipsToBounds = false
            
            }
        }
    
}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       self.selectImageView.isHidden = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
 
    }
    
    func populateData(product : Product){
        if !product.images.isEmpty{
            self.productImageView.setImage_kf(imageString: product.images[0].image, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        }else{
            self.productImageView.image = AppImages.productPlaceholder.image
        }
        promoteImgView.isHidden = !product.isPromoted
    }
}
