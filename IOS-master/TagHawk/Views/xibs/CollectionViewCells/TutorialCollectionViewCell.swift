//
//  TutorialCollectionViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 18/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class TutorialCollectionViewCell : UICollectionViewCell {

    @IBOutlet weak var tutorialImageView: UIImageView!
    @IBOutlet weak var tutorialTitlelabel: UILabel!
    @IBOutlet weak var tutorialDescriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.tutorialTitlelabel.setAttributes(text: "", font: AppFonts.Galano_Heavy.withSize(25), textColor: UIColor.white)
        self.tutorialDescriptionLabel.setAttributes(text: "", font: AppFonts.Galano_Light.withSize(15), textColor: UIColor.white)
        self.tutorialTitlelabel.setAttributes(text: "", font: AppFonts.SourceSansPro_Semibold.withSize(25), textColor: UIColor.white)

    }
    
    
    func populateData(title : String, description : String){
        self.tutorialTitlelabel.text = title
        self.tutorialDescriptionLabel.text = description
    }
    

}
