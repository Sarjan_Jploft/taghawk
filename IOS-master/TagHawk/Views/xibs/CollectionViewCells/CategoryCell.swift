//
//  CategoryCell.swift
//  TagHawk
//
//  Created by Appinventiv on 24/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.categoryImageView.contentMode = .scaleAspectFit
        self.drawShadow(shadowOpacity: 0.05)
        self.clipsToBounds = false
        self.contentView.round(radius: 5)
        self.categoryTitle.setAttributes(font: AppFonts.Galano_Medium.withSize(13) , textColor: UIColor.black)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.categoryImageView.round()
    }
    
    func populateData(category : Category){
        self.categoryImageView.setImage_kf(imageString: category.categoryImage, placeHolderImage: AppImages.customerLogo.image)
        self.categoryTitle.text = category.description
    }
    
}
