//
//  ClusterListCellCollectionViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 15/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ClusterListCellCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewDetails: UIButton!
    @IBOutlet weak var tagImageView: UIImageView!
    @IBOutlet weak var tagNameLabel: UILabel!
    @IBOutlet weak var numberOfMembersLabel: UILabel!
    @IBOutlet weak var founderLabel: UILabel!
    @IBOutlet weak var publicPrivateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.tagNameLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.numberOfMembersLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(14), textColor: UIColor.black)
        self.founderLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(14), textColor: AppColors.lightGreyTextColor)
        self.tagImageView.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.tagImageView.round(radius: 10)
        self.publicPrivateLabel.round(radius: 10)
        self.publicPrivateLabel.setBorder(width: 0.5, color: AppColors.lightGreyTextColor)
        self.publicPrivateLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(15), textColor: AppColors.appBlueColor)
        self.viewDetails.setAttributes(title: LocalizedString.View_Details.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white)
        
        
    }
    
    
    func populateData(tag : Tag){
        
        self.tagNameLabel.text = tag.name
        self.numberOfMembersLabel.text = "\(tag.members) \(LocalizedString.Members.localized)"
        self.founderLabel.text = "\(LocalizedString.Founder.localized): \(tag.adminName)"
        let type = tag.entrance == .privateTag ? LocalizedString.Private.localized : LocalizedString.Public.localized
        self.publicPrivateLabel.text = type
        self.tagImageView.setImage_kf(imageString: tag.tagImageUrl, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
    }

    @IBAction func viewDetailsTapped(_ sender: UIButton) {
   
    }
    
    
    
}
