//
//  CartPriceDetailCell.swift
//  TagHawk
//
//  Created by Appinventiv on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CartPriceDetailCell: UICollectionViewCell {

    
    @IBOutlet weak var totalAmmountLabel: UILabel!
    @IBOutlet weak var totalAmmountAmtLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setUpSubView()
    }
    
    
    func setUpSubView(){
        
        self.contentView.backgroundColor = UIColor.white
        
        self.totalAmmountLabel.setAttributes(text: LocalizedString.Total_Amount.localized, font: AppFonts.Galano_Semi_Bold.withSize(13), textColor: UIColor.black)
    
        self.totalAmmountAmtLabel.setAttributes(text: "$150,050.95", font: AppFonts.Galano_Bold.withSize(13), textColor: UIColor.black)

    }

    func populateData(prod : [CartProduct]){

        var price : Double = 0
       
        for item in prod{
            price += item.productPrice
        }
        self.totalAmmountAmtLabel.text = "$\(price.rounded(toPlaces: 2).removeTrailingZero())"
    }
    
}
