//
//  ApplyPromoceodeCell.swift
//  TagHawk
//
//  Created by Appinventiv on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ApplyPromoceodeCell: UICollectionViewCell {
    
    @IBOutlet weak var applyPromocedeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.backgroundColor = UIColor.white
        self.applyPromocedeLabel.setAttributes(text: LocalizedString.Apply_Promo_Code_Voucher.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), textColor: UIColor.black)
        
    }

}
