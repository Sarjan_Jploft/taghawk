//
//  DocumentCollectionViewCell.swift
//  TagHawk
//
//  Created by Vikash on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class DocumentCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var documentImage: UIImageView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var crossBtn: UIButton!
    
    //MARK: - View's lifecycle
    override func awakeFromNib() {
        self.outerView.layer.cornerRadius = 20
        self.outerView.layer.borderColor = UIColor.lightText.cgColor
        self.outerView.layer.borderWidth = 0.5
    }
    
    @IBAction func crossBtnAction(_ sender: UIButton) {
        
    }
    
}
