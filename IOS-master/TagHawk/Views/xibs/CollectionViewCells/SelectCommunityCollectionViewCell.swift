//
//  SelectCommunityCollectionViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 19/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SelectCommunityCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var tickImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.nameLabel.setAttributes(text: "Havard University", font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
    }
    
    
    func populateData(tag : Tag){
        self.nameLabel.text = tag.name
    }
    
    

}

