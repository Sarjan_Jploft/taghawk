//
//  DocumentCell.swift
//  TagHawk
//
//  Created by Admin on 6/18/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class DocumentCell: UICollectionViewCell {

    @IBOutlet weak var documentImageview: UIImageView!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var outerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

}
