//
//  ProductCollectionViewCellGridView.swift
//  TagHawk
//
//  Created by Apple on 27/12/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ProductCollectionViewCellGridView: UICollectionViewCell {
    
    enum ProductCellFor {
        case productDetail
        case home
        case tagDetail
    }
    
    
    
    @IBOutlet weak var lblFirm: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductAddress: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var promoteImgView: UIImageView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var blackOverLayView: UIView!
    @IBOutlet weak var selectedImgView: UIImageView! {
        didSet {
            blackOverLayView.isHidden = selectedImgView.isHidden ? true : false
        }
    }
    
    @IBOutlet weak var selectImageView: UIImageView!
    
    var cellFor = ProductCellFor.home{
        
        didSet{
            if self.cellFor == .home{
//                self.contentView.round(radius: 10)
//                self.drawShadow(shadowOpacity: 0.15)
//                self.productImageView.round(radius: 10)
//                self.clipsToBounds = false
                
//                self.viewContainer.round(radius: 10)
//                self.viewContainer.clipsToBounds = true
                
                self.productImageView.drawShadow(shadowOpacity: 0.15)
                self.productImageView.round(radius: 10)
                
                
            }else if self.cellFor == .tagDetail{
                //            self.contentView.round(radius: 10)
                //            self.setBorder(width: 0.5, color: AppColors.textfield_Border)
                //            self.contentView.round(radius: 10)
                
//                self.viewContainer.round(radius: 10)
//                self.viewContainer.clipsToBounds = true
//                
//                self.contentView.round(radius: 10)
//                self.drawShadow(shadowOpacity: 0.15)
//                self.clipsToBounds = false
                
                self.productImageView.drawShadow(shadowOpacity: 0.15)
                self.productImageView.round(radius: 10)
                
            }
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    
    func populateData(product : Product){
        self.lblProductName.text = product.title
        let roundedPrice = (product.firmPrice).roundAndFixDigit(product.firmPrice)
        self.lblPrice.text = "$\(roundedPrice)"
        product.isNegotiable == true ? (self.lblFirm.isHidden = false) : (self.lblFirm.isHidden = true)
//        self.lblProductAddress.text = product.productLocation//"Location"
        self.lblProductAddress.text = product.city + ", " + product.state
        //getLoc(product: product)
    
    if !product.images.isEmpty{
            self.productImageView.setImage_kf(imageString: product.images[0].image, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        }else{
            self.productImageView.image = AppImages.productPlaceholder.image
       }
    self.setLikeUnlikeStatus(product: product)
    promoteImgView.isHidden = !product.isPromoted

    }
    
    func setLikeUnlikeStatus(product : Product){
       let heartImage = product.isLiked ? AppImages.filledHeart.image : AppImages.emptyHeart.image
        self.btnLike.setImage(heartImage, for: UIControl.State.normal)
    }
    
    func getLoc(product : Product){
            LocationManager.shared.reverseGeocodeLocationWithLatLon(latitude: product.productHomeLatitude, longitude: product.productHomeLongitude) { (dict, placeMarker, error) in
                guard let data = dict else { return }
    //
                guard let state = data[ApiKey.locality] as? String else { return }
                guard let country = data[ApiKey.administrativeArea] as? String else { return }
                if !state.isEmpty && !country.isEmpty {
                    self.lblProductAddress.text = "\(state), \(country)"
                }
    //            printDebug(data)
                
            }
        }
    
}
