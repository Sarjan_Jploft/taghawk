//
//  ProductDetailImageCellCollectionViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 31/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ProductDetailImageCellColl : UICollectionViewCell {
    
    @IBOutlet weak var produceImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        produceImage.round(radius: 15)
    }

    func populateData(imageUrl : String){
      self.produceImage.setImage_kf(imageString: imageUrl, placeHolderImage: AppImages.rectangularPlaceholder.image, loader: false)
    }

    func populateData(image : UIImage?){
        guard let img = image else { return }
        self.produceImage.image = img
    }

    
}

