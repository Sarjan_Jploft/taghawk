//
//  CapturedImagesCell.swift
//  TagHawk
//
//  Created by Appinventiv on 25/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CapturedImagesCell : UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.clipsToBounds = false
        self.contentView.clipsToBounds = false
        self.imageView.round(radius: 7)
        self.imageView.setBorder(width: 0.5, color: UIColor.white)
    }
    
    func populateImage(image : (index : Int, stringUrl : String, img : UIImage?)) {
        if let prodImg = image.img {
            self.imageView.image = prodImg
        }else {
            self.imageView.setImage_kf(imageString: image.stringUrl, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        }
    }
}
