//
//  RewardCollViewCell.swift
//  TagHawk
//
//  Created by Admin on 5/20/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class RewardCollViewCell: UICollectionViewCell {

    @IBOutlet weak var planNameLabel: UILabel!
    @IBOutlet weak var redeemForLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.planNameLabel.setAttributes(text: LocalizedString.One_Day_Promotion.localized, font: AppFonts.Galano_Bold.withSize(20), textColor: AppColors.appBlueColor)
       
        self.redeemForLabel.setAttributes(text: LocalizedString.Redem_For.localized, font: AppFonts.Galano_Medium.withSize(11), textColor: UIColor.black)
        
        self.pointsLabel.setAttributes(text: "100 Points", font: AppFonts.Galano_Semi_Bold.withSize(16), textColor: UIColor.black)
        self.contentView.round(radius: 10)
        self.contentView.setBorder(width: 1.0, color: AppColors.textfield_Border)
//        self.drawShadow(shadowOpacity: 0.15)
//        self.clipsToBounds = false
    }
    
    func populateData(plan : GiftPlan){
        self.planNameLabel.text = "\(plan.days) \(LocalizedString.Day_Promotio.localized)"
        self.pointsLabel.text = "\(plan.rewardPoint) \(LocalizedString.Points.localized)"
    }
    
}
