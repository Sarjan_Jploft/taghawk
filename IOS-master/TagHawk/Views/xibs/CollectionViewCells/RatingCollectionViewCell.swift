//
//  RatingCollectionViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 07/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class RatingCollectionViewCell : UICollectionViewCell {

    @IBOutlet weak var ratingButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.round(radius: 10)
    }
    
    func setSelectDeselectState(isSelected : Bool){
        if isSelected{
            self.ratingButton.backgroundColor = AppColors.appBlueColor
            self.ratingButton.setImage(AppImages.whiteStar.image, for: UIControl.State.normal)
            self.ratingButton.setTitleColor(UIColor.white)
        }else {
            self.ratingButton.backgroundColor = UIColor.white
            self.ratingButton.setImage(AppImages.blackStar.image, for: UIControl.State.normal)
            self.ratingButton.setTitleColor(UIColor.black)
        }
    }
}
