//
//  PhotoCollectionCell.swift
//  ImagesAndVideosPicker
//
//  Created by Bhavneet Singh on 24/07/18.
//  Copyright © 2018 Bhavneet Singh. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {
    
    var topRightBtnAction: ((_ cell: PhotoCollectionCell) -> Void)?
    
    @IBOutlet weak var mainImageView: ImageViewWithPreview!
    @IBOutlet weak var sideLabel: UILabel!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var videoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.videoImage.isHidden = true
        self.selectedImage.isHidden = true
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.selectedImage.layer.cornerRadius =  self.selectedImage.height/2
    }
}
