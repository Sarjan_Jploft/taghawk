
//  CartCollectionViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 06/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CartCollectionViewCell : UICollectionViewCell {
    
    enum ProductStatus : Int {
        case available = 1
        case sold = 2
    }
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var deliveryTypeLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var soldImageView: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.backgroundColor = UIColor.white
        productImageView.round(radius: 10)
     
      self.priceLabel.setAttributes(font: AppFonts.Galano_Bold.withSize(14), textColor: UIColor.black)
    
      self.titleLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(13), textColor: UIColor.black)
        
     self.deliveryTypeLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(12), textColor: UIColor.black)
        
     self.sellerNameLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(12), textColor: AppColors.lightGreyTextColor)
        self.deliveryTypeLabel.text = "Pick-Up Only"
        self.drawShadow()
        self.clipsToBounds = false
    }
    
    func populateData(prod : CartProduct) {
        self.priceLabel.text = "$\(prod.productPrice.rounded(toPlaces: 2).removeTrailingZero())"
        self.titleLabel.text = prod.productName
        self.sellerNameLabel.text = "\(LocalizedString.Seller_Name.localized) : \(prod.sellerName)"
        self.deliveryTypeLabel.text = prod.shippingAvailibility.stringValue()
        guard let first = prod.productPicUrl.first else { return }
        self.productImageView.setImage_kf(imageString: first, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        self.soldImageView.isHidden = prod.productStatus == .available
    }
    
}
