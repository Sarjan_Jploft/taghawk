//
//  SearchedProduceCollectionViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 30/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SearchedProduceCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var promoteImgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var productImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    func dropShadow(){
        self.contentView.round(radius: 10)
        self.drawShadow(shadowOpacity: 0.15)
        self.clipsToBounds = false
    }
    
   func populateData(product : Product){
        self.titleLabel.text = product.title
        self.priceLabel.text = "$\(product.firmPrice)"
    
    if !product.images.isEmpty{
            self.productImageView.setImage_kf(imageString: product.images[0].image, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        }else{
            self.productImageView.image = AppImages.productPlaceholder.image
       }
    self.setLikeUnlikeStatus(product: product)
    promoteImgView.isHidden = !product.isPromoted

    }
    
    func setLikeUnlikeStatus(product : Product){
       let heartImage = product.isLiked ? AppImages.filledHeart.image : AppImages.emptyHeart.image
        self.likeButton.setImage(heartImage, for: UIControl.State.normal)
    }
    
}
