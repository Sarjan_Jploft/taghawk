//
//  ProductTextFieldCell.swift
//  TagHawk
//
//  Created by Appinventiv on 08/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ProductTextFieldCell: UITableViewCell {

    enum ProductTextFieldFor {
        case productTitle
        case location
        case price
    }
    
    @IBOutlet weak var productTexFfield: CustomTextField!
    
    var setUpFor = ProductTextFieldFor.productTitle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        setUpTextFieldView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setUpTextFieldView(){
        
        switch self.setUpFor {
        case .productTitle:
            self.productTexFfield.placeholder = "\(LocalizedString.Title.localized)"
            self.productTexFfield.rightView = nil
            
        case .location:
            
            let rightBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            rightBtn.setImage(AppImages.location.image, for: UIControl.State.normal)
            self.productTexFfield.placeholder = LocalizedString.Location.localized
            self.productTexFfield.rightView = rightBtn
            
        case .price:
            let rightBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            rightBtn.setImage(AppImages.dollar.image, for: UIControl.State.normal)
            self.productTexFfield.placeholder = LocalizedString.Enter_Price.localized
            self.productTexFfield.rightView = rightBtn
            
        }
        
    }
    
    
}
