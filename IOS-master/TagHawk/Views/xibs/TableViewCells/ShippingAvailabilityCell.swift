//
//  ShippingAvailabilityCell.swift
//  TagHawk
//
//  Created by Appinventiv on 19/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol GetShippingAvailabilityDelegate : class {
    func getShippingAvailability(shipping : ShippingAvailability)
}

class ShippingAvailabilityCell : UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var deliverButton: UIButton!
    @IBOutlet weak var pickUpButton: UIButton!
    @IBOutlet weak var shippingButton: UIButton!
    
    weak var delegate : GetShippingAvailabilityDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.titleLabel.setAttributes(text: LocalizedString.Availability_Options.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)

        self.titleLabel.attributedText = LocalizedString.Shipping_Availability.localized.attributeStringWithAstric()
        
        self.pickUpButton.setAttributes(title: LocalizedString.Pickup.localized, font: AppFonts.Galano_Medium.withSize(13), titleColor: UIColor.black, backgroundColor: UIColor.white)
        
        self.deliverButton.setAttributes(title: LocalizedString.Deliver.localized, font: AppFonts.Galano_Medium.withSize(13), titleColor: UIColor.black, backgroundColor: UIColor.white)

        self.shippingButton.setAttributes(title: LocalizedString.FedEx.localized, font: AppFonts.Galano_Medium.withSize(13), titleColor: UIColor.black, backgroundColor: UIColor.white)

        self.pickUpButton.round(radius: 10)
        self.deliverButton.round(radius: 10)
        self.shippingButton.round(radius: 10)
        
        self.pickUpButton.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.deliverButton.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.shippingButton.setBorder(width: 0.5, color: AppColors.textfield_Border)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func pickUpButtonTapped(_ sender: UIButton) {
//        self.selectDeselectShippingAvailability(shipping: ShippingAvailability.pickUp)
        self.delegate?.getShippingAvailability(shipping: ShippingAvailability.pickUp)
    }
    
    @IBAction func deliverButtonTapped(_ sender: UIButton) {
//        self.selectDeselectShippingAvailability(shipping: ShippingAvailability.deliver)
        self.delegate?.getShippingAvailability(shipping: ShippingAvailability.deliver)
        
    }
    
    @IBAction func shippingButton(_ sender: UIButton) {
//        self.selectDeselectShippingAvailability(shipping: ShippingAvailability.shipping)
        self.delegate?.getShippingAvailability(shipping: ShippingAvailability.shipping)
    }
 
    func selectDeselectShippingAvailability(shipping : [ShippingAvailability]){

        if shipping.contains(ShippingAvailability.pickUp){
            self.pickUpButton.backgroundColor = AppColors.appBlueColor
            self.pickUpButton.setTitleColor(UIColor.white)
        }else{
            self.pickUpButton.backgroundColor = UIColor.white
            self.pickUpButton.setTitleColor(UIColor.black)
        }
        
         if shipping.contains(ShippingAvailability.deliver){
            self.deliverButton.backgroundColor = AppColors.appBlueColor
            self.deliverButton.setTitleColor(UIColor.white)
         }else{
            self.deliverButton.backgroundColor = UIColor.white
            self.deliverButton.setTitleColor(UIColor.black)
        }
        
         if shipping.contains(ShippingAvailability.shipping){
            self.shippingButton.backgroundColor = AppColors.appBlueColor
            self.shippingButton.setTitleColor(UIColor.white)
         }else{
            self.shippingButton.backgroundColor = UIColor.white
            self.shippingButton.setTitleColor(UIColor.black)
        }
        
        
//        switch shipping {
//        case .pickUp:
//
//            self.pickUpButton.backgroundColor = AppColors.appBlueColor
//        self.pickUpButton.setTitleColor(UIColor.white)
//
//            self.deliverButton.backgroundColor = UIColor.white
//            self.deliverButton.setTitleColor(UIColor.black)
//
//            self.shippingButton.backgroundColor = UIColor.white
//        self.shippingButton.setTitleColor(UIColor.black)
//
//        case .deliver:
//
//            self.pickUpButton.backgroundColor = UIColor.white
//            self.pickUpButton.setTitleColor(UIColor.black)
//
//            self.deliverButton.backgroundColor = AppColors.appBlueColor
//            self.deliverButton.setTitleColor(UIColor.white)
//
//            self.shippingButton.backgroundColor = UIColor.white
//            self.shippingButton.setTitleColor(UIColor.black)
//
//        case .shipping:
//
//            self.pickUpButton.backgroundColor = UIColor.white
//            self.pickUpButton.setTitleColor(UIColor.black)
//
//            self.deliverButton.backgroundColor = UIColor.white
//            self.deliverButton.setTitleColor(UIColor.black)
//
//            self.shippingButton.backgroundColor = AppColors.appBlueColor
//        self.shippingButton.setTitleColor(UIColor.white)
//
//        }


    }
    
}
