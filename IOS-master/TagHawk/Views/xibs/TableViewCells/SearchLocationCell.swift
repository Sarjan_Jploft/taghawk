//
//  SearchLocationCell.swift
//  TagHawk
//
//  Created by Appinventiv on 11/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SearchLocationCell : UITableViewCell {

    @IBOutlet weak var searchLocationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func populateData(place : String){
        self.searchLocationLabel.text = place
    }
    
}
