//
//  ProductDetailRatingCell.swift
//  TagHawk
//
//  Created by Appinventiv on 31/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ProductDetailRatingCell : UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var followersAndFollowingLabel: UILabel!
    @IBOutlet weak var memberSinceLabel: UILabel!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var facebookWidth: NSLayoutConstraint!
    @IBOutlet weak var mobileWidth: NSLayoutConstraint!
    @IBOutlet weak var googleWidth: NSLayoutConstraint!
    @IBOutlet weak var officialIdWidth: NSLayoutConstraint!
    @IBOutlet weak var facebookNotation: UIImageView!
    @IBOutlet weak var emailNotation: UIImageView!
    @IBOutlet weak var phoneNotation: UIImageView!
    @IBOutlet weak var officialIdNotation: UIImageView!
    @IBOutlet weak var verifiedImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.ratingButton.round(radius: 10)
        self.ratingButton.setBorder(width: 1.0, color: AppColors.textfield_Border)
        
        self.nameLabel.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(14), textColor: UIColor.black)
        self.memberSinceLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
       self.followersAndFollowingLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(12), textColor: UIColor.black)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.profileImageView.round()
    }
    
    func populateData(product : Product){
        self.nameLabel.text = product.fullName
        self.memberSinceLabel.text = "\(LocalizedString.Member_Since.localized): \(Date(timeIntervalSince1970: product.userCreated / 1000).toString(dateFormat: Date.DateFormat.MMMMyyyy.rawValue))"
        self.ratingButton.setTitle("\(product.sellerRating)")
        self.followersAndFollowingLabel.text = "Followers: \(product.followers) Following: \(product.followings)"
        profileImageView.setImage_kf(imageString: product.profilePicture, placeHolderImage: #imageLiteral(resourceName: "icTabProfileUnactive"), loader: true)
        if product.profilePicture.isEmpty {
            profileImageView.addNameStartingCredential(name: product.fullName)
        }
        
        self.facebookWidth.constant = product.isFacebookLogin ? 35 : 0
        self.googleWidth.constant = product.isEmailVerified ? 35 : 0
        self.mobileWidth.constant = product.isPhoneVerified ? 35 : 0
        self.officialIdWidth.constant = product.isDocumentsVerified ? 35 : 0
        self.verifiedImageView.isHidden = !product.isSellerVerified
    }
    
    
    
    
}
