//
//  DropDownCell.swift
//  TagHawk
//
//  Created by Appinventiv on 05/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown


protocol GetPostedWithinDelegate : class {
    func getPostedWithIn(postedWithin : SelectedFilters.PostedWithin)
}

protocol GetSelectedCategoryDelegate : class {
    func getSelectedCategory(category : Category)
}

protocol GetSelectedConditionDelegate : class {
    func getSelectedCondition(condition : SelectedFilters.ProductConditionType)
}

protocol GetSelectedTagType : class {
    func getSelectedTagType(type : SelectedFilters.TagType)
}

protocol GetSelectedEntranceType : class {
    func getSelectedEntranceType(type : SelectedFilters.TagEntrance)
}

protocol GetAddressTypeDelegate: AnyObject {
    func getAddressType(type: String)
}

class DropDownTableViewCell: UITableViewCell {
    
    enum DropDownFor {
        case postedWithin
        case productCategory
        case condition
        case tagName
        case tagType
        case tagEntrance
        case addressType
    }
    
    
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var dropDownLabel: UILabel!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var titleLabelTop: NSLayoutConstraint!
    
    
    private let dropDown = DropDown()
    weak var postedWithInDelegate : GetPostedWithinDelegate?
    weak var categoryDelegate : GetSelectedCategoryDelegate?
    weak var conditionDelegate : GetSelectedConditionDelegate?
    weak var tagTypeDelegate : GetSelectedTagType?
    weak var tagEntranceDelegate : GetSelectedEntranceType?
    weak var addressTypeDelegate: GetAddressTypeDelegate?
    
    
    var setUpFor = DropDownFor.postedWithin {
        
        didSet{
            
            switch self.setUpFor {
            case .productCategory:
                self.titleView.attributedText = LocalizedString.Product_Category.localized.attributeStringWithAstric()
                
            case .condition:
                
                self.titleView.attributedText = "\(LocalizedString.Condition.localized)*".attributeStringWithAstric()
                
            case .tagType:
                self.titleView.text = LocalizedString.Tag_Type.localized
                
            case .tagEntrance:
                self.titleView.text = LocalizedString.Tag_Entrance.localized
                
            case .tagName:
                self.titleView.text = LocalizedString.Tag_Name.localized
                
            case .postedWithin:
                self.titleView.text = LocalizedString.Posted_Within.localized
                
            case .addressType:
                self.titleView.text = LocalizedString.AddressType.localized
                
                
            default:
                self.titleView.text = LocalizedString.Price.localized
            }
            
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.setUpView()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func dropDownButtonTapped(_ sender: UIButton) {
        self.superview?.endEditing(true)
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
        self.dropDown.dismissMode = .automatic
    
        switch self.setUpFor {
            
        case .postedWithin:
            self.setUpDropDownForPostedWithin()
            
        case .productCategory:
            self.setUpDropDownForProductCategory()
            
        case .condition:
            self.setUpDropDownForCondition()
            
        case .tagName:
            setUpDropDownForTagEntrance()
            
        case .tagType:
            setUpDropDownForTagEntrance()
            
        case .addressType:
            setupDropDownForAddressType()
            
        case .tagEntrance:
            setUpDropDownForTagType()
            
        }
    }
}


extension DropDownTableViewCell {
    
    func setUpView(){
        self.titleView.setAttributes(text: LocalizedString.Posted_Within.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.dropDownView.setBorder(width: 1, color: AppColors.textfield_Border)
        self.dropDownView.round(radius: 10)
        self.dropDownLabel.setAttributes(text: LocalizedString.Search.localized, font: AppFonts.Galano_Regular.withSize(14), textColor: AppColors.lightGreyTextColor)
        self.setUpDropDown()
    }
    
    func setUpDropDown(){
        self.dropDown.backgroundColor = UIColor.white
        self.dropDown.selectionBackgroundColor = UIColor.white
    }
    
    func setUpDropDownLabel(labelText : String, withColor : UIColor){
        self.dropDownLabel.text = labelText
        self.dropDownLabel.textColor = withColor
    }

    
    func setUpDropDownForCondition(){
        
        // This is done as we need enum values and rawvalues both
        let conditionArray = [SelectedFilters.ProductConditionStringValues.new, SelectedFilters.ProductConditionStringValues.likeNew, SelectedFilters.ProductConditionStringValues.good, SelectedFilters.ProductConditionStringValues.normal, SelectedFilters.ProductConditionStringValues.flawd]
        
        self.dropDown.dataSource = conditionArray.map { $0.rawValue }
        
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            
            weakSelf.conditionDelegate?.getSelectedCondition(condition: SelectedFilters.shared.getProductCondition(from: conditionArray[index]))
            
        }
    }
    
    func setUpDropDownForProductCategory(){
        
        let categories = AppSharedData.shared.categories.map { (category) -> String in
            return category.description
        }
        
        self.dropDown.dataSource = categories
        self.dropDown.show()
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            weakSelf.categoryDelegate?.getSelectedCategory(category: AppSharedData.shared.categories[index])
            
        }
    }
    
    
    func setUpDropDownForPostedWithin(){
        
        // This is done as we need enum values and rawvalues both
        let postedWithin = [SelectedFilters.PostedWithinStrValues.today, SelectedFilters.PostedWithinStrValues.thisWeek,SelectedFilters.PostedWithinStrValues.thisMonth, SelectedFilters.PostedWithinStrValues.last3Montshs,
                            SelectedFilters.PostedWithinStrValues.thisYear]
        
        var dataSource: [String] {
            var strArr = [String]()
            postedWithin.forEach { (val) in
                strArr.append(val.rawValue)
            }
            return strArr
        }
        
        self.dropDown.dataSource = dataSource
        
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            
            weakSelf.postedWithInDelegate?.getPostedWithIn(postedWithin: SelectedFilters.shared.getPostedWithIn(postedWithinString: postedWithin[index]))
            
        }
    }
    
    
    func setUpDropDownForTagType(){
        
        // This is done as we need enum values and rawvalues both
//        let tagTypes = [SelectedFilters.TagType.all,SelectedFilters.TagType.privateTag, SelectedFilters.TagType.publicTag]
        let tagTypes = [SelectedFilters.TagType.all,SelectedFilters.TagType.apartment, SelectedFilters.TagType.universities,SelectedFilters.TagType.organization,SelectedFilters.TagType.club,SelectedFilters.TagType.other]
        
        self.dropDown.dataSource = tagTypes.map { SelectedFilters.shared.getTagTypeStringValue(from: $0).rawValue }
        
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            weakSelf.tagTypeDelegate?.getSelectedTagType(type: tagTypes[index])
            
        }
    }
    
    func setUpDropDownForTagEntrance(){
        
        // This is done as we need enum values and rawvalues both
        let tagTypes = [SelectedFilters.TagEntrance.all,SelectedFilters.TagEntrance.privateTag, SelectedFilters.TagEntrance.publicTag]
        
        self.dropDown.dataSource = tagTypes.map { SelectedFilters.shared.getEntraceTypeStringValue(from: $0).rawValue }
        
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            weakSelf.tagEntranceDelegate?.getSelectedEntranceType(type: tagTypes[index])
            
        }
    }
    
    
    func setupDropDownForAddressType() {
        let tagTypes = [LocalizedString.Office.localized, LocalizedString.Residence.localized]
        
        self.dropDown.dataSource = tagTypes
        
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            weakSelf.addressTypeDelegate?.getAddressType(type: tagTypes[index])
        }
    }
    
}
