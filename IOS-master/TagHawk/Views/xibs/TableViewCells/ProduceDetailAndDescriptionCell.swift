//
//  ProduceDetailAndDescriptionCellTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 31/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ProduceDetailAndDescriptionCell : UITableViewCell {
    
    enum ProductDetailCellSetUpFor {
        case productDetail
        case productPreview
    }
    
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productLocationlabel: UILabel!
    @IBOutlet weak var deliceryTypeLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var firmPriceLabel: UILabel!
//    @IBOutlet weak var conditionHeadingLabel: UILabel!
    
    var setUpFor = ProductDetailCellSetUpFor.productDetail{
        didSet {
//            let color = self.setUpFor == .productDetail ? UIColor.black : AppColors.appBlueColor
//            self.conditionLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(17), textColor: color)
//            self.conditionHeadingLabel.setAttributes(text: LocalizedString.Condition.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: color)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.productPriceLabel.setAttributes(font: AppFonts.Galano_Bold.withSize(20), textColor: UIColor.black)
        self.productTitleLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(18), textColor: UIColor.black)
        self.productLocationlabel.setAttributes(font: AppFonts.Galano_Regular.withSize(16), textColor: UIColor.black)
        self.deliceryTypeLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(15), textColor: AppColors.lightGreyTextColor)
        self.timeLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(15), textColor: AppColors.lightGreyTextColor)
        self.descriptionLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(16), textColor: UIColor.black)
        let color = self.setUpFor == .productDetail ? UIColor.black : AppColors.appBlueColor
        self.conditionLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(17), textColor: color)
        self.firmPriceLabel.isHidden = true
        self.productLocationlabel.text = ""
    }
    
    func populatePreviewData(product : AddProductData){
        self.productPriceLabel.text = "$\(product.price.rounded(toPlaces: 2).removeTrailingZero())"
        self.productTitleLabel.text = product.title
        self.productLocationlabel.text = product.location
        self.descriptionLabel.text = product.description
        self.populateDeliveryType(shipping: product.shippingAvailability)
        self.populateProductCondition(condition: product.condition)
        self.firmPriceLabel.isHidden = !product.isNegotiable
    }
    
    func populateData(product : Product){
        self.productPriceLabel.text = "$\(product.firmPrice.rounded(toPlaces: 2).removeTrailingZero())"
        self.productTitleLabel.text = product.title
        self.productLocationlabel.text = " "
        getLoc(product: product)
        self.productTitleLabel.text = product.title
        self.descriptionLabel.text = product.description
        self.populateDeliveryType(shipping: product.shippingType)
        self.populateProductCondition(condition: product.condition)
        let time = LocalizedString.Posted.localized + " " +  Date(timeIntervalSince1970: product.created / 1000).timeAgoSince
        self.timeLabel.text = time
        self.firmPriceLabel.isHidden = !product.isNegotiable
    }
    
    
    func getLoc(product : Product){
        LocationManager.shared.reverseGeocodeLocationWithLatLon(latitude: product.productLatitude, longitude: product.productLongitude) { (dict, placeMarker, error) in
            guard let data = dict else { return }
//
            guard let state = data[ApiKey.locality] as? String else { return }
            guard let country = data[ApiKey.administrativeArea] as? String else { return }
            if !state.isEmpty && !country.isEmpty {
                self.productLocationlabel.text = "\(state), \(country)"
            }
//            printDebug(data)
            
        }
    }
    
 
    func populateDeliveryType(shipping : [ShippingAvailability]){
        
        switch shipping {

            case [.pickUp]:
                self.deliceryTypeLabel.text = "\(LocalizedString.Availability.localized): \(LocalizedString.Pickup.localized)"

            case [.deliver]:
                self.deliceryTypeLabel.text = "\(LocalizedString.Availability.localized): \(LocalizedString.Delivery.localized)"
            
            
            case [.shipping]:
                self.deliceryTypeLabel.text = "\(LocalizedString.Availability.localized): \(LocalizedString.Shipping.localized)"

            
        case [.pickUp, .deliver], [.deliver, .pickUp]:
            self.deliceryTypeLabel.text = "\(LocalizedString.Availability.localized): \(LocalizedString.Pickup.localized + " ," + LocalizedString.Delivery.localized)"
            
            
        case [.pickUp, .shipping], [.shipping, .pickUp]:
            self.deliceryTypeLabel.text = "\(LocalizedString.Availability.localized): \(LocalizedString.Pickup.localized + " ," + LocalizedString.Shipping.localized)"

            
        case [.deliver, .shipping], [.shipping, .deliver]:
            self.deliceryTypeLabel.text = "\(LocalizedString.Availability.localized): \(LocalizedString.Delivery.localized + " ," + LocalizedString.Shipping.localized)"
            
        default:
            self.deliceryTypeLabel.text = "\(LocalizedString.Availability.localized): \(LocalizedString.Pickup.localized + " ," + LocalizedString.Delivery.localized + " ," + LocalizedString.Shipping.localized)"
            
        }
    }
    
    func populateProductCondition(condition : SelectedFilters.ProductConditionType) {
        
        switch condition {
         
            case .new:
                self.conditionLabel.text = LocalizedString.Condition.localized + ": " + LocalizedString.New_Never_Used.localized
            
            case .good:
                self.conditionLabel.text = LocalizedString.Condition.localized + ": " + LocalizedString.Good_Gently_Used.localized

            case .likeNew:
                self.conditionLabel.text = LocalizedString.Condition.localized + ": " + LocalizedString.Like_New_Rarely_Used.localized

            case .flawd:
                self.conditionLabel.text = LocalizedString.Condition.localized + ": " + LocalizedString.Flawed_With_Flaw.localized

            case .normal:
                self.conditionLabel.text = LocalizedString.Condition.localized + ": " + LocalizedString.Normal_Normal_Wear.localized
            
            case .none:
                self.conditionLabel.text = LocalizedString.Condition.localized + ": " + ""
       
        }
    }
}
