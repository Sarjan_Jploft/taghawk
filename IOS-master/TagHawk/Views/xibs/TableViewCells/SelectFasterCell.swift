//
//  SelectFasterCell.swift
//  TagHawk
//
//  Created by Appinventiv on 27/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SellFasterCell : UITableViewCell {
    
    @IBOutlet weak var sellFasterLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backView.round(radius: 10)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.sellFasterLabel.setAttributes(font: AppFonts.Galano_Bold.withSize(17.5), textColor: AppColors.appBlueColor)
       self.descriptionLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(12) , textColor: UIColor.black)
        self.descLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(12) , textColor: UIColor.black)
        self.sellFasterLabel.text = LocalizedString.Sell_Faster.localized
        self.descriptionLabel.text = LocalizedString.Get_10_More_Views.localized
        self.descLabel.text = LocalizedString.Your_Product.localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
