//
//  SearchTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 30/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SearchTableViewCell : UITableViewCell {


    @IBOutlet weak var searchResultLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        countLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(12), textColor: AppColors.grayColor)
        searchResultLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.grayColor)
        searchResultLabel.numberOfLines = 0
    }
  
    func populateData(result : SearchResult){
        searchResultLabel.text = result.title
        countLabel.text = "\(result.count)"
    }
    
    func populatePlace(place : String){
        self.searchResultLabel.text = place
    }
    
    
    
}
