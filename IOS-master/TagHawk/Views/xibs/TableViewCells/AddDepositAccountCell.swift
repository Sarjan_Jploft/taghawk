//
//  AddDepositAccountCellTableViewCell.swift
//  TagHawk
//
//  Created by Admin on 5/23/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class AddDepositAccountCell: UITableViewCell {

    @IBOutlet weak var textFieldBackView: UIView!
    @IBOutlet weak var rightTextField: UITextField!
    @IBOutlet weak var leftTextField: UITextField!
    @IBOutlet weak var iButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var leftLineView: UIView!
    @IBOutlet weak var rightLineView: UIView!
    
    var setUpFor = AddDepositAccountVC.AddDepositAccountCellConditions.routingNumber
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code

        self.selectionStyle = .none
        self.leftLineView.backgroundColor = AppColors.textfield_Border
        self.rightLineView.backgroundColor = AppColors.textfield_Border
        self.rightTextField.delegate = self
        self.leftTextField.delegate = self
        self.leftTextField.font = AppFonts.Galano_Regular.withSize(15)
        self.rightTextField.font = AppFonts.Galano_Regular.withSize(15)
        self.headingLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func iButtonTapped(_ sender: UIButton) {
        
        
    }
    
    func setUpView(){
        
        switch self.setUpFor {
            
            case .routingNumber:
                self.leftTextField.placeholder = LocalizedString.Routin_Number.localized
                self.rightTextField.placeholder = LocalizedString.Verify_Routing.localized
                self.headingLabel.text = LocalizedString.Bank_Routing_Number.localized
                self.leftTextField.keyboardType = .numberPad
                self.rightTextField.keyboardType = .numberPad
            
            case .accountNumber:
                self.leftTextField.placeholder = LocalizedString.Account_Number.localized
                self.rightTextField.placeholder = LocalizedString.Verify_Account.localized
                self.headingLabel.text = LocalizedString.Bank_Account_Number.localized
                self.leftTextField.keyboardType = .numberPad
                self.rightTextField.keyboardType = .numberPad

            case .name:
                self.leftTextField.placeholder = LocalizedString.firstName.localized
                self.rightTextField.placeholder = LocalizedString.lastName.localized
                self.headingLabel.text = LocalizedString.Bank_Account_Holder_Name.localized
                self.leftTextField.keyboardType = .default
                self.rightTextField.keyboardType = .default

        }
        
    }
    
    func populateData(detail : BankDetailsInput){
        
        switch self.setUpFor {
     
        case .routingNumber:
         
            self.leftTextField.text = detail.rountingNumber
            self.rightTextField.text = detail.verifyRouting
            
        case .accountNumber:
            self.leftTextField.text = detail.accountNumbr
            self.rightTextField.text = detail.verifyAccountNumber
            
        case .name:
            self.leftTextField.text = detail.firstName
            self.rightTextField.text = detail.lastName
            
        }
    }
    

}


extension AddDepositAccountCell : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let userEnteredString = textField.text else { return false }
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        
        switch self.setUpFor {
         
        case .routingNumber:
            
            return self.isCharacterAcceptableForRoutingNumber(name: userEnteredString, char: string)
            
        case .accountNumber:
            return self.isCharacterAcceptableForAccountNumber(name : userEnteredString, char : string)
            
        case .name:
            return self.isCharacterAcceptableForName(name: userEnteredString, char: string)
            
        }
    }
    
    func isCharacterAcceptableForRoutingNumber(name : String, char : String)-> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 9{
            return false
        }else{
            return true
        }
    }
    
    func isCharacterAcceptableForAccountNumber(name : String, char : String) -> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 20{
            return false
        }else{
            return true
        }
    }
    
    func isCharacterAcceptableForName(name : String, char : String) -> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 20{
            return false
        }else{
            return true
        }
    }
}
