//
//  ChangePasswordCellTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 06/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ChangePasswordFieldDelagate : class {
    func texfieldDidChange(type : ChangePasswordCell.PasswordFieldType, text : String?)
    func textFieldShouldReturn(type : ChangePasswordCell.PasswordFieldType)
}

class ChangePasswordCell : UITableViewCell {

    enum PasswordFieldType {
        case oldPassword
        case newPassword
        case confirmPassword
    }
    
    @IBOutlet weak var changePassTextField : CustomTextField!
   
    var delegate : ChangePasswordFieldDelagate?
    var textFieldState : TextFieldState = .none
    var setUpFor = PasswordFieldType.oldPassword {
        
        didSet {
            self.changePassTextField.isSecureTextEntry = true
            self.changePassTextField.keyboardType = .default
            self.changePassTextField.autocapitalizationType = .none
            self.changePassTextField.returnKeyType = .next
       
            switch setUpFor {
                
                case .oldPassword:
                    self.changePassTextField.placeholder = LocalizedString.oldPassword.localized
                
                case .newPassword:
                    self.changePassTextField.placeholder = LocalizedString.newPassword.localized

                case .confirmPassword:
                    self.changePassTextField.returnKeyType = .done
                    self.changePassTextField.placeholder = LocalizedString.confirmPassword.localized

                default:
                    break
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureTextField()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureTextField() {
        
        self.changePassTextField.delegate = self
        self.changePassTextField.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)
        self.changePassTextField.textColor = UIColor.black
   
    }
    
    func populateText(type : PasswordFieldType, userInput : ChangePasswordInput){

        switch type {
        case .oldPassword:
            self.changePassTextField.text = userInput.oldPass
        case .newPassword:
            self.changePassTextField.text = userInput.newPass
        case .confirmPassword:
            self.changePassTextField.text = userInput.confirmPass
            
        }
    }
    
    func didShowError(state : TextFieldState){
        self.textFieldState = state
        switch state {
        case .invalid:
            self.changePassTextField.error = state.associatedValue
        default:
            self.changePassTextField.error = nil
        }
    }
}

extension ChangePasswordCell : UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let userEnteredString = textField.text else { return false }
        //        let newString = (userEnteredString as NSString).replacingCharacters(in: range, with: string) as NSString
        
        switch self.textFieldState {
        case .invalid:
            self.changePassTextField.error = nil
        default:
            break
        }
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        
        return self.isCharacterAcceptableForPassword(password: userEnteredString, char: string)
        
    }
 
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.delegate?.textFieldShouldReturn(type: self.setUpFor)
        return true
    }
    
    @objc private func texfieldDidChange(_ textfield: UITextField) {
        self.delegate?.texfieldDidChange(type: self.setUpFor, text: changePassTextField.text)
    }
    
    func isCharacterAcceptableForPassword(password : String, char : String) -> Bool {
        return char.isBackSpace || password.count <= 15
    }
}
