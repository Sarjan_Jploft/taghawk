//
//  PinToTopCell.swift
//  TagHawk
//
//  Created by Appinventiv on 25/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

protocol PinToTopCellDelegate: class {
    func toggleBtnTapped(btn: UIButton)
}

class PinToTopCell: DropDownCell {

    weak var delegate: PinToTopCellDelegate?
    
    @IBOutlet weak var toggleBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func toggleBtnTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.toggleBtnTapped(btn: sender)
    }
}
