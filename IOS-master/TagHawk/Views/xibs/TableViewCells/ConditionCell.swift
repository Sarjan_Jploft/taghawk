//
//  ConditionCell.swift
//  TagHawk
//
//  Created by Appinventiv on 05/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ConditionCell : UITableViewCell {

    enum ConditionCellFor {
        case itemsFilter
        case shelfilter
    }
    
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var newBackView: UIView!
    @IBOutlet weak var newLabel: UILabel!
    @IBOutlet weak var newImageView: UIImageView!
    @IBOutlet weak var newButton: UIButton!
    @IBOutlet weak var likeNewBackView: UIView!
    @IBOutlet weak var likeNewLabel: UILabel!
    @IBOutlet weak var likeNewImageView: UIImageView!
    @IBOutlet weak var likeNewButton: UIButton!
    @IBOutlet weak var goodBackView: UIView!
    @IBOutlet weak var goodLabel: UILabel!
    @IBOutlet weak var goodImageView: UIImageView!
    @IBOutlet weak var goodButton: UIButton!
    @IBOutlet weak var normalBackView: UIView!
    @IBOutlet weak var normalLabel: UILabel!
    @IBOutlet weak var normalImageView: UIImageView!
    @IBOutlet weak var normalButton: UIButton!
    @IBOutlet weak var flawedBackView: UIView!
    @IBOutlet weak var flawedLabel: UILabel!
    @IBOutlet weak var flawedlImageView: UIImageView!
    @IBOutlet weak var flawedButton: UIButton!
    
    var setUpFor = ConditionCellFor.itemsFilter
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func newButtonTapped(_ sender: UIButton) {
        self.selectDeselectCondition(type: SelectedFilters.ProductConditionType.new)
        self.selectConditionSelectionStatus(type: SelectedFilters.ProductConditionType.new)
    }

    @IBAction func likeNewButtonTapped(_ sender: UIButton) {
        self.selectDeselectCondition(type: SelectedFilters.ProductConditionType.likeNew)
        self.selectConditionSelectionStatus(type: SelectedFilters.ProductConditionType.likeNew)
    }
    
    @IBAction func goodButtonTapped(_ sender: UIButton) {
        self.selectDeselectCondition(type: SelectedFilters.ProductConditionType.good)
        self.selectConditionSelectionStatus(type: SelectedFilters.ProductConditionType.good)
    }
    
    @IBAction func normalButtonTapped(_ sender: UIButton) {
        self.selectDeselectCondition(type: SelectedFilters.ProductConditionType.normal)
        self.selectConditionSelectionStatus(type: SelectedFilters.ProductConditionType.normal)
    }
    
    @IBAction func flawdButtonTapped(_ sender: UIButton) {
        self.selectDeselectCondition(type: SelectedFilters.ProductConditionType.flawd)
        self.selectConditionSelectionStatus(type: SelectedFilters.ProductConditionType.flawd)
    }
    
    func setUpView(){
        self.conditionLabel.setAttributes(text: LocalizedString.Conditions.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
        self.newLabel.setAttributes(text: LocalizedString.New_Never_Used.localized, font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
        
        self.likeNewLabel.setAttributes(text: LocalizedString.Like_New_Rarely_Used.localized, font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
        
        self.goodLabel.setAttributes(text: LocalizedString.Good_Gently_Used.localized, font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
        
        self.normalLabel.setAttributes(text: LocalizedString.Normal_Normal_Wear.localized, font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
        
        self.flawedLabel.setAttributes(text: LocalizedString.Flawed_With_Flaw.localized, font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
        
        self.selectConditionSelectionStatus(type : SelectedFilters.ProductConditionType.new)
        self.selectConditionSelectionStatus(type : SelectedFilters.ProductConditionType.likeNew)
        self.selectConditionSelectionStatus(type : SelectedFilters.ProductConditionType.good)
        self.selectConditionSelectionStatus(type : SelectedFilters.ProductConditionType.normal)
        self.selectConditionSelectionStatus(type : SelectedFilters.ProductConditionType.flawd)
        
    }
    
    func selectDeselectCondition(type : SelectedFilters.ProductConditionType){
        self.setUpFor == .itemsFilter ? self.selectDeselectConditionForItems(type : type) : self.selectDeselectConditionForShelf(type : type)
    }
    
    func selectDeselectConditionForItems(type : SelectedFilters.ProductConditionType){
        if SelectedFilters.shared.selectedFiltersOnItems.condiotions.contains(type) {
            SelectedFilters.shared.selectedFiltersOnItems.condiotions = SelectedFilters.shared.selectedFiltersOnItems.condiotions.filter {  $0 != type}
        }else{
            SelectedFilters.shared.selectedFiltersOnItems.condiotions.append(type)
        }
    }
    
    func selectDeselectConditionForShelf(type : SelectedFilters.ProductConditionType){
        if SelectedFilters.shared.selectedFilterOnShelf.condiotions.contains(type) {
            SelectedFilters.shared.selectedFilterOnShelf.condiotions = SelectedFilters.shared.selectedFilterOnShelf.condiotions.filter {  $0 != type}
        }else{
            SelectedFilters.shared.selectedFilterOnShelf.condiotions.append(type)
        }
    }
    
    
    
    func setTickUnTick(type : SelectedFilters.ProductConditionType) -> UIImage {
        
        if self.setUpFor == .itemsFilter {
            return SelectedFilters.shared.selectedFiltersOnItems.condiotions.contains(type) ? AppImages.tickWithCircle.image : AppImages.unTickWithCircle.image
        }else{
            return SelectedFilters.shared.selectedFilterOnShelf.condiotions.contains(type) ? AppImages.tickWithCircle.image : AppImages.unTickWithCircle.image
        }
    }
    
    func selectConditionSelectionStatus(type : SelectedFilters.ProductConditionType){
        
        switch type {
            
            case .new:
                self.newImageView.image = self.setTickUnTick(type: type)
     
            case .likeNew:
            
                self.likeNewImageView.image = self.setTickUnTick(type: type)

            case .good:
                self.goodImageView.image = self.setTickUnTick(type: type)
            
            case .normal:
                self.normalImageView.image = self.setTickUnTick(type: type)
            
            case .flawd:
                self.flawedlImageView.image = self.setTickUnTick(type: type)
            
            case .none:
              break
        }
        
    }
}
