//
//  PhoneNumberCell.swift
//  TagHawk
//
//  Created by Appinventiv on 09/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class PhoneNumberCell: UITableViewCell {

    // MARK:- VARIABLES
    //====================
    
    var countryCodeBtnTap: ((UIButton) -> ())?
    
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var phoneNumbMainView: UIView!
    @IBOutlet weak var countryCodeBtn: UIButton!
    @IBOutlet weak var phoneNumbTextField: UITextField!
    
    // MARK:- LIFE CYCLE
    //====================
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // MARK:- IBACTIONS
    //====================
    
    @IBAction func countryCodeBtnAction(_ sender: UIButton) {
        countryCodeBtnTap?(sender)
    }
    
    
    // MARK:- FUNCTIONS
    //====================
    
    ///Initial Setup
    private func initialSetup() {
        
    }
}
