//
//  PaymentCellTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 28/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol PaymentCellProtocol : class {
    func texfieldDidChange(type : PaymentCell.PaymentCellSetUpFor, textField : UITextField)
    func textFieldShouldReturn(type : PaymentCell.PaymentCellSetUpFor)
}

class PaymentCell : UITableViewCell {

    enum PaymentCellSetUpFor {
        
        case cardNumber
        case expireyAndCvv
        case name
        
    }
    
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var expireyTextField: UITextField!
    @IBOutlet weak var cvvandExpireyBackView: UIView!
    @IBOutlet weak var expireySepratorView: UIView!
    @IBOutlet weak var cvvSepratorView: UIView!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var paymentTextField: UITextField!
    
    var setUpFor = PaymentCellSetUpFor.cardNumber
    weak var delegate : PaymentCellProtocol?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.sepratorView.backgroundColor = AppColors.textfield_Border
        self.cvvSepratorView.backgroundColor = AppColors.textfield_Border
        self.expireySepratorView.backgroundColor = AppColors.textfield_Border
        self.paymentTextField.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)
        self.expireyTextField.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)
        self.cvvTextField.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)

        self.paymentTextField.delegate = self
        self.expireyTextField.delegate = self
        self.cvvTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUpLayOut(){
        // Temp Comment due to payment error
        
//    self.cvvandExpireyBackView.isHidden = self.setUpFor != .expireyAndCvv
//    self.sepratorView.isHidden = self.setUpFor == .expireyAndCvv
//
        self.paymentTextField.isHidden = true
        self.expireyTextField.isHidden = true
        self.cvvTextField.isHidden = true
        self.sepratorView.isHidden = true
        self.cvvandExpireyBackView.isHidden = true
        /*
        switch self.setUpFor {
      
        case .cardNumber:
            self.paymentTextField.placeholder = LocalizedString.Card_Number.localized
            self.paymentTextField.keyboardType = .numberPad
            
        case .name:
            self.paymentTextField.placeholder = LocalizedString.Card_Holder_Name.localized
            self.paymentTextField.keyboardType = .default

        case .expireyAndCvv:
            self.expireyTextField.placeholder = LocalizedString.mmyyyy.localized
            self.cvvTextField.placeholder = LocalizedString.cvv.localized
            self.paymentTextField.keyboardType = .numberPad
            self.expireyTextField.keyboardType = .numberPad
            self.cvvTextField.keyboardType = .numberPad
        }
        */
    }

    @objc private func texfieldDidChange(_ textfield: UITextField) {
        
        var txt = textfield.text ?? ""
        
        self.delegate?.texfieldDidChange(type: self.setUpFor, textField : textfield)
        //printDebug(txt)

    }
}


extension PaymentCell : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let userEnteredString = textField.text else { return false }
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        
        switch self.setUpFor {
      
            case .cardNumber:
                return self.isCharacterAcceptableForCardNumber(name: userEnteredString, char: string)

            case .expireyAndCvv:
                return textField == self.expireyTextField ? self.isCharacterAcceptableForExpirey(textField: textField, expirey: userEnteredString, char: string) : isCharacterAcceptableForCvv(name : userEnteredString, char : string)
           
        case .name:
           
           return self.isCharacterAcceptableForFirstName(name : userEnteredString, char : string)

        }
        
    }
    
    
    func isCharacterAcceptableForCardNumber(name : String, char : String)-> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 19{
            return false
        }else{
            return char.isAllowed(validationStr: AllowedCharactersValidation.cardNumber)
        }
    }
    
    func isCharacterAcceptableForCvv(name : String, char : String) -> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 3{
            return false
        }else{
            return char.isAllowed(validationStr: AllowedCharactersValidation.cardNumber)
        }
    }
    
    func isCharacterAcceptableForExpirey(textField : UITextField, expirey : String, char : String)-> Bool {
       
        if char.isBackSpace && expirey.count == 3{
            textField.text = expirey.replacingOccurrences(of: "/", with: "")
            return true
        } else if char.isBackSpace {
            return true
        } else if expirey.count > 4{
            return false
        }else{
            return char.isAllowed(validationStr: AllowedCharactersValidation.expirey)
        }
    }
    
    func isCharacterAcceptableForFirstName(name : String, char : String) -> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 20{
            return false
        }else{
            return char.isAllowed(validationStr: AllowedCharactersValidation.name)
        }
    }
    
}
