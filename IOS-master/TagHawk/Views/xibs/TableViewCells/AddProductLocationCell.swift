//
//  AddProductLocationCell.swift
//  TagHawk
//
//  Created by Appinventiv on 19/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit


class AddProductLocationCell: UITableViewCell {
    
    var shouldUpdateCurrentLoc = false

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationFieldBackView: UIView!
    @IBOutlet weak var locationTextField: UITextField!
    
    weak var delegate : MapTextFieldTappedDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.titleLabel.setAttributes(text: "\(LocalizedString.Location.localized)*", font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.titleLabel.attributedText = "\(LocalizedString.Location.localized)*".attributeStringWithAstric()
        self.locationFieldBackView.round(radius: 10)
        self.locationFieldBackView.setBorder(width: 0.5, color: AppColors.textfield_Border)
        let currentLocButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        currentLocButton.setImage(AppImages.location.image, for: UIControl.State.normal)
        self.locationTextField.rightView = currentLocButton
        self.locationTextField.rightViewMode = .always
        currentLocButton.addTarget(self, action: #selector(currentLocationButtonTapped), for: UIControl.Event.touchUpInside)
        self.locationTextField.delegate = self
    }

    @objc func currentLocationButtonTapped(){
        LocationManager.shared.startUpdatingLocation { (loc, error) in
            guard let location = loc else { return }
            
            self.getAddressFromReverceGeoCoding(lat: location.coordinate.latitude, long: location.coordinate.longitude)
            LocationManager.shared.stopLocationManger()
        }
    }
    
    func getAddressFromReverceGeoCoding(lat : Double, long : Double){
        LocationManager.shared.reverseGeocodeLocationWithLatLon(latitude: lat, longitude: long) { (dict, placeMarker, error) in
            guard let data = dict else { return }
            guard let add = data[ApiKey.formattedAddress] as? String else { return }
            guard let state = data[ApiKey.administrativeArea] as? String else { return }
            let city = data[ApiKey.locality] as? String ?? ""
            
            self.locationTextField.text = add
            
//            self.locationTextField.text = city + "," + state
//            let cityState = city + "," + state
//            self.delegate?.getSelectedLocation(lat: lat, long: long, address: add)
            
            
            self.delegate?.getSelectedLocation(lat: lat, long: long, address: add, city:city, state:state)
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func populateData(loc : String){
        self.locationTextField.text = loc
        if let loc = locationTextField.text {
            if loc.isEmpty {
                if shouldUpdateCurrentLoc {
                    currentLocationButtonTapped()
                }
            }
        }
    }
}

extension AddProductLocationCell : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.delegate?.mapFieldTapped()
        return false
    }
    
}
