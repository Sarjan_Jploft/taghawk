//
//  SettingsCell.swift
//  TagHawk
//
//  Created by Appinventiv on 01/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import PWSwitch

protocol PushNotificationValueChanged : class {
    func pushNotificationValueChanged(value : Bool)
}


class SettingsCell : UITableViewCell {
    
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var toggleSwitch: PWSwitch!
    @IBOutlet weak var arrowImgView: UIImageView!
    
    
    weak var delegate : PushNotificationValueChanged?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.settingsLabel.font = AppFonts.Galano_Regular.withSize(14)
        self.selectionStyle = .none
        
        self.toggleSwitch.thumbOffFillColor = AppColors.appBlueColor
        self.toggleSwitch.thumbOnFillColor = AppColors.appBlueColor
        
        self.toggleSwitch.thumbOnBorderColor = AppColors.appBlueColor
        self.toggleSwitch.thumbOffBorderColor = AppColors.appBlueColor
        
        self.toggleSwitch.trackOffFillColor = AppColors.textfield_Border
        self.toggleSwitch.trackOnFillColor = AppColors.appBlueColor
        
        self.toggleSwitch.trackOnBorderColor = AppColors.appBlueColor
        self.toggleSwitch.trackOffBorderColor = AppColors.textfield_Border
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func populatedata(type : SettingsVC.SettingOptions){
        self.toggleSwitch.isHidden = type != .pushNotification
        self.settingsLabel.text = type.rawValue
        let loginType = UserProfile.main.loginType
        if type == .changePassword && loginType == .facebook {
            self.settingsLabel.isHidden = true
            self.arrowImgView.isHidden = true
        }else if type == .registerAsHawkDriver{
            self.settingsLabel.isHidden = true
            self.arrowImgView.isHidden = true
        }else {
            self.settingsLabel.isHidden = false
            self.arrowImgView.isHidden = false
        }
    }
    
    func setToggleState(){
        self.toggleSwitch.setOn(!UserProfile.main.isNotificationMuted, animated: true)
    }
    
}


