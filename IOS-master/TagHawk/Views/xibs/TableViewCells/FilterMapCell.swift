//
//  FilterMapCell.swift
//  TagHawk
//
//  Created by Appinventiv on 05/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import GoogleMaps

protocol MapTextFieldTappedDelegate : class {
    func mapFieldTapped()
    func getSelectedLocation(lat : Double, long : Double, address : String, city:String, state:String)

}

class FilterMapCell: UITableViewCell {

    enum FilterMapCellFor {
        case tagFilter
        case itemFilter
    }
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var locationField: CustomTextField!
    @IBOutlet weak var setLocationLabel: UILabel!
    weak var delegate : MapTextFieldTappedDelegate?
    var setUpFor = FilterMapCellFor.itemFilter
    let marker = GMSMarker()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.setUpView()
        locationField.delegate = self
        self.mapView.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setUpView(){
        self.locationField.placeholder = LocalizedString.Location.localized
        self.setLocationLabel.setAttributes(text: LocalizedString.Set_Location.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.mapView.round(radius: 10)
        self.mapView.setBorder(width: 0.5, color: AppColors.textfield_Border)
        
        let currentLocButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        currentLocButton.setImage(AppImages.location.image, for: UIControl.State.normal)
        self.locationField.rightView = currentLocButton
        self.locationField.rightViewMode = .always
        currentLocButton.addTarget(self, action: #selector(currentLocationButtonTapped), for: UIControl.Event.touchUpInside)
    }
    
    func populateData(addressStr : String, lat : Double, long : Double){
        
        if lat == 0.0{
            self.locationField.text = ""
        }else if addressStr.isEmpty {
            self.getAddressFromReverceGeoCoding(lat: lat, long: long)
        } else{
            self.locationField.text = addressStr
        }
    
        self.addMarkerToCordinates(lat : lat, long : long)
        self.setCameraPosition(lat: lat, long: long)
    }
    
    func addMarkerToCordinates(lat : Double, long : Double){
        let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        DispatchQueue.main.async {
            self.marker.position = position
        }
        marker.icon = AppImages.blueMarker.image
        marker.map = mapView
    }
    
    func setCameraPosition(lat : Double, long : Double){
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 12)
        DispatchQueue.main.async {
            self.mapView.animate(to: camera)
        }
    }
    
    @objc func currentLocationButtonTapped(){
      
        LocationManager.shared.startUpdatingLocation { (loc, error) in
            guard let location = loc else { return }
            if self.setUpFor == .tagFilter {
                SelectedFilters.shared.selectedFilterOnTag.lat = location.coordinate.latitude
                SelectedFilters.shared.selectedFilterOnTag.long = location.coordinate.longitude
            } else {
                SelectedFilters.shared.selectedFiltersOnItems.lat = location.coordinate.latitude
                SelectedFilters.shared.selectedFiltersOnItems.long = location.coordinate.longitude
            }
            self.setCameraPosition(lat: location.coordinate.latitude, long: location.coordinate.longitude)
            self.mapView.clear()
            self.addMarkerToCordinates(lat: location.coordinate.latitude, long: location.coordinate.longitude)
            self.getAddressFromReverceGeoCoding(lat: location.coordinate.latitude, long: location.coordinate.longitude)
            LocationManager.shared.stopLocationManger()
        }
    }
    

    func getAddressFromReverceGeoCoding(lat : Double, long : Double){
        LocationManager.shared.reverseGeocodeLocationWithLatLon(latitude: lat, longitude: long) { (dict, placeMarker, error) in
            
            guard let data = dict else { return }
            guard let add = data[ApiKey.formattedAddress] as? String else { return }
            self.locationField.text = add
            if self.setUpFor == .itemFilter {
                SelectedFilters.shared.selectedFiltersOnItems.locationStr = add
            } else{
                SelectedFilters.shared.selectedFilterOnTag.locationStr = add
            }
            
        }
    }
   
}

extension FilterMapCell : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.delegate?.mapFieldTapped()
        return false
    }
    
}
