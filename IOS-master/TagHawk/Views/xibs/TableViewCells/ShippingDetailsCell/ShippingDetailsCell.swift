//
//  ShippingDetailsCell.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ShippingDetailsCell: UITableViewCell {

    // MARK:- VARIABLES
    //====================
    
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var zipCodeLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    
    
    // MARK:- LIFE CYCLE
    //====================
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // MARK:- IBACTIONS
    //====================
    
    
    @IBAction func editBtnAction(_ sender: UIButton) {
        
    }
    
    
    // MARK:- FUNCTIONS
    //====================
    
    ///Initial Setup
    private func initialSetup() {
        mainView.addShadow()
    }
}
