//
//  AddProductTextFieldCell.swift
//  TagHawk
//
//  Created by Appinventiv on 19/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class AddProductTextFieldCell: UITableViewCell {
    @IBOutlet weak var textFieldBackView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLblTop: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       self.textFieldBackView.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.textFieldBackView.round(radius: 10)
        self.titleLabel.setAttributes(text: LocalizedString.Title.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
        titleLabel.attributedText = LocalizedString.Title.localized.attributeStringWithAstric()
        
        self.textFieldBackView.backgroundColor = AppColors.textfield_Border.withAlphaComponent(0.1)
        self.selectionStyle = .none
        self.textField.keyboardType = .default
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
