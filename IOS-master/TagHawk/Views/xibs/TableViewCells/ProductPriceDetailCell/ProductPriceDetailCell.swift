//
//  ProductPriceDetailCell.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ProductPriceDetailCell: UITableViewCell {

    // MARK:- VARIABLES
    //====================
    
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var productImgView: UIImageView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var shippingTypeLbl: UILabel!
    @IBOutlet weak var sellerNameLbl: UILabel!
    
    
    // MARK:- LIFE CYCLE
    //====================
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
 
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        productImgView.round()
    }
    
    // MARK:- IBACTIONS
    //====================
    
    
    
    
    // MARK:- FUNCTIONS
    //====================
    
    ///Initial Setup
    private func initialSetup() {
        mainView.addShadow()
    }
}
