//
//  FollowersCellTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 20/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class FollowersCell : UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var followUnfollowButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var buttonBackImageView: UIImageView!
    @IBOutlet weak var menuButtonWidth: NSLayoutConstraint!
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        profileImageView.round()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.followUnfollowButton.setAttributes(title: LocalizedString.Following.localized, font: AppFonts.Galano_Medium.withSize(15), titleColor: UIColor.black)
        self.nameLabel.setAttributes(text : "Gurpreet Singh",font: AppFonts.Galano_Regular.withSize(15), textColor: UIColor.black)
        self.followUnfollowButton.round(radius: 5)
        self.followUnfollowButton.setBorder(width: 0.5, color: AppColors.lightGreyTextColor)
        self.selectionStyle = .none
        self.sepratorView.backgroundColor = AppColors.textfield_Border
        
        self.nameLabel.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(13), textColor: UIColor.black)
    
        self.followUnfollowButton.setAttributes(title: LocalizedString.Follow.localized, font: AppFonts.Galano_Medium.withSize(12), titleColor: UIColor.white)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension FollowersCell {
    
    func populateData(user : FollowingFollowersModel){
        self.nameLabel.text = user.fullName
        self.profileImageView.setImage_kf(imageString: user.profilePicture, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        if user.profilePicture.isEmpty {
            profileImageView.addNameStartingCredential(name: user.fullName)
        }
      self.buttonBackImageView.isHidden = user.isFollowing
      self.followUnfollowButton.layer.borderWidth = user.isFollowing ? 0.5 : 0
        
        if user.isFollowing{
            self.followUnfollowButton.setAttributes(title: LocalizedString.Following.localized, font: AppFonts.Galano_Medium.withSize(12), titleColor: AppColors.lightGreyTextColor,backgroundColor: UIColor.white)
           
        }else{
            self.followUnfollowButton.setAttributes(title: LocalizedString.Follow.localized, font: AppFonts.Galano_Medium.withSize(12), titleColor: UIColor.white ,backgroundColor: AppColors.appBlueColor)
        }
    }
    
    func menuButton(isHidden : Bool){
        self.menuButtonWidth.constant = isHidden ? 0 : 25
        self.menuButton.isHidden = isHidden
    }
    
}
