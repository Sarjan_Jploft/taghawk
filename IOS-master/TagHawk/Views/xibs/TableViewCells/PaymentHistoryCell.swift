//
//  PaymentHistoryCell.swift
//  TagHawk
//
//  Created by Admin on 6/4/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class PaymentHistoryCell : UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var buttonsBackView: UIView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var completedOrder: UIButton!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var cancelRefundButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
//    @IBOutlet weak var NSLayoutHeightBackView: NSLayoutConstraint!
    
    
   override func awakeFromNib() {
        super.awakeFromNib()
    
         self.selectionStyle = .none
         self.nameLabel.setAttributes(text: "", font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
         self.dateLabel.setAttributes(text: "", font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.lightGreyTextColor)
         self.statusLabel.setAttributes(text: "", font: AppFonts.Galano_Regular.withSize(13), textColor: AppColors.appBlueColor)
         self.leftButton.setAttributes(title: LocalizedString.Refund.localized, font: AppFonts.Galano_Medium.withSize(15), titleColor: AppColors.appBlueColor)
         self.rightButton.setAttributes(title: LocalizedString.Release_Payment.localized, font: AppFonts.Galano_Medium.withSize(15), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
         self.cancelRefundButton.setAttributes(title: LocalizedString.Release_Payment.localized, font: AppFonts.Galano_Medium.withSize(15), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
         self.leftButton.round(radius: 10)
         self.rightButton.round(radius: 10)
         self.cancelRefundButton.round(radius: 10)
    
         self.leftButton.setBorder(width: 1, color: AppColors.appBlueColor)
         self.rightButton.setBorder(width: 1, color: AppColors.appBlueColor)
        
         self.priceLabel.setAttributes(text: "$34 Bile", font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
         self.completedOrder.isHidden = true
         self.sepratorView.backgroundColor = AppColors.textfield_Border
        
         self.rightButton.titleLabel?.lineBreakMode = .byWordWrapping
         self.rightButton.titleLabel?.textAlignment = .center
        
//        self.productImageView.round(radius: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateData(history : PaymentHistory){
       
        if !history.images.isEmpty{
            self.productImageView.setImage_kf(imageString: history.images[0].image, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        }else{
            self.productImageView.image = AppImages.productPlaceholder.image
        }
        
        self.nameLabel.text = history.title
        self.priceLabel.text = "$\(history.price.rounded(toPlaces: 2).removeTrailingZero())"
        self.statusLabel.text = LocalizedString.Sold.localized//history.deliveryStatus.rawValue
        self.dateLabel.text = Date(timeIntervalSince1970: history.purchasedDate / 1000).toString(dateFormat: Date.DateFormat.dMMMyyyy.rawValue)
        self.setView(history: history)
    }
    
    func setView(history : PaymentHistory){
     
        switch history.deliveryStatus {
        
            case .pending:
                self.buttonsBackView.isHidden = history.sellerId == UserProfile.main.userId
                self.statusLabel.text = LocalizedString.Sold.localized//LocalizedString.Pending.localized
                self.cancelRefundButton.isHidden = true
                self.leftButton.setTitle(LocalizedString.Refund.localized)
                self.rightButton.setTitle(LocalizedString.Confirm.localized)
                //Temp Comment
//                self.statusLabel.text = history.sellerId == UserProfile.main.userId  ? LocalizedString.Payment_Pending.localized : LocalizedString.Received_This_Item_Please_Confirm.localized
                
//                self.statusLabel.text = "Sold"
            
            
            
            case .completed:
                self.buttonsBackView.isHidden = true
                self.statusLabel.text = LocalizedString.Completed.localized
                self.statusLabel.text = history.sellerId == UserProfile.main.userId  ? LocalizedString.Payment_Received.localized : LocalizedString.Item_Delivered.localized

                self.cancelRefundButton.isHidden = true

            case .requestForRefund:
                self.statusLabel.text = LocalizedString.Refund_Requested.localized
                if history.sellerId == UserProfile.main.userId {
                    self.buttonsBackView.isHidden = false
                    self.cancelRefundButton.isHidden = true
                    self.leftButton.setTitle(LocalizedString.Decline.localized)
                    self.rightButton.setTitle(LocalizedString.Accept.localized)
                }else{
                    self.buttonsBackView.isHidden = true
                    self.cancelRefundButton.isHidden = false
                    self.cancelRefundButton.setTitle(LocalizedString.Cancel_Refund.localized)
            }
            
            case .refundAccepted:
                self.statusLabel.text = LocalizedString.Refund_Accepted.localized
                if history.sellerId == UserProfile.main.userId {
                    self.buttonsBackView.isHidden = false
                    self.cancelRefundButton.isHidden = true
                    self.leftButton.setTitle(LocalizedString.Decline_Refund.localized)
                    self.rightButton.setTitle(LocalizedString.Release_Refund.localized)
                }else{
                    self.buttonsBackView.isHidden = true
                    self.cancelRefundButton.isHidden = true
            }
        
        case .declined:
            if history.sellerId == UserProfile.main.userId {
                self.buttonsBackView.isHidden = true
                self.cancelRefundButton.isHidden = true
                self.statusLabel.text = LocalizedString.Declined_Refund_For_This_Product.localized
             }else{
                self.statusLabel.text = LocalizedString.Refund_Request_Has_Been_Declined.localized
                self.buttonsBackView.isHidden = false
                self.cancelRefundButton.isHidden = true
                self.leftButton.setTitle(LocalizedString.Cancel_Refund.localized)
                self.rightButton.setTitle(LocalizedString.Despute.localized)
            }
            
        case .requestSuccess:
            self.buttonsBackView.isHidden = true
            self.cancelRefundButton.isHidden = true
            self.statusLabel.text = LocalizedString.Amount_Refunded.localized

        case .disputeStarted:
            
            if history.sellerId == UserProfile.main.userId {
                self.statusLabel.text = LocalizedString.Dispute_Started.localized
                self.buttonsBackView.isHidden = false
                self.cancelRefundButton.isHidden = true
                self.leftButton.setTitle(LocalizedString.Submit_Proof.localized)
                self.rightButton.setTitle(LocalizedString.Release_Refund.localized)
            }else{
                self.buttonsBackView.isHidden = true
                self.cancelRefundButton.isHidden = true
                self.statusLabel.text = LocalizedString.Dispute_Started.localized
            }
            
        case .sellarStatementDone:
            
            if history.sellerId == UserProfile.main.userId {
                self.buttonsBackView.isHidden = true
                self.cancelRefundButton.isHidden = false
                self.cancelRefundButton.setTitle(LocalizedString.Release_Refund.localized)
                self.statusLabel.text = LocalizedString.Submitted_Proof_Admin_Will_Take_Action.localized

            }else{
                self.buttonsBackView.isHidden = true
                self.cancelRefundButton.isHidden = false
                self.cancelRefundButton.setTitle(LocalizedString.Cancel_Refund.localized)
                self.statusLabel.text = LocalizedString.Dispute_Started.localized
            }
            
        case .disputeCanStart:
            self.buttonsBackView.isHidden = true
            self.cancelRefundButton.isHidden = true
            self.statusLabel.text = LocalizedString.Disputable_Order.localized
            
            default:
                self.buttonsBackView.isHidden = true
                self.statusLabel.text = LocalizedString.Completed.localized
                break
        }
        // Temp change
        self.buttonsBackView.isHidden = true
        self.cancelRefundButton.isHidden = true
//        self.NSLayoutHeightBackView.constant = 0
    
    }
}
