//
//  IncludeTransFeeCell.swift
//  TagHawk
//
//  Created by Appinventiv on 17/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import PWSwitch

protocol IncludeTransFeeDelegate: AnyObject {
    func switchToggled(_ value: Bool)
}

class IncludeTransFeeCell: UITableViewCell {

    // MARK:- VARIABLES
    //====================
    
    weak var delegate : IncludeTransFeeDelegate?
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var mainSwitch: PWSwitch!
    @IBOutlet weak var infoBtn: UIButton!
    
    // MARK:- IBACTIONS
    //====================
    
    @IBAction func infoBtnAction(_ sender: UIButton) {
        
    }
    
    @IBAction func mainSwitchAction(_ sender: PWSwitch) {
        self.delegate?.switchToggled(sender.on)
    }
    
    
    // MARK:- LIFE CYCLE
    //====================
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // MARK:- FUNCTIONS
    //====================
    
    ///Initial Setup
    private func initialSetup() {
        self.titleLbl.setAttributes(text: LocalizedString.IncludeTransactionFee.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
        self.mainSwitch.thumbOffFillColor = AppColors.grayColor
        self.mainSwitch.thumbOnFillColor = AppColors.appBlueColor
        
        self.mainSwitch.thumbOnBorderColor = AppColors.appBlueColor
        self.mainSwitch.thumbOffBorderColor = AppColors.grayColor
        
        self.mainSwitch.trackOffFillColor = AppColors.textfield_Border
        self.mainSwitch.trackOnFillColor = AppColors.appBlueColor
        
        self.mainSwitch.trackOnBorderColor = AppColors.appBlueColor
        self.mainSwitch.trackOffBorderColor = AppColors.textfield_Border
    }
    
    func populateData(data : AddProductData){
        self.mainSwitch.setOn(data.isTransactionCost, animated: false)
    }
}
