//
//  SortingDropDownCell.swift
//  TagHawk
//
//  Created by Appinventiv on 08/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

class SortingDropDownCell : DropDownCell {
    
    @IBOutlet weak var dropDownLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
