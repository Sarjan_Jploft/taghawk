//
//  PriceRangeCell.swift
//  TagHawk
//
//  Created by Appinventiv on 05/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class PriceRangeCell: UITableViewCell {

    enum PriceRangeFor{
        case shelfFilter
        case productsFilter
    }
    
    @IBOutlet weak var priceRangeLabel: UILabel!
    @IBOutlet weak var toTextField: CustomTextField!
    @IBOutlet weak var fromTextField: CustomTextField!
    
    var setupFor = PriceRangeFor.productsFilter
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpVoew()
        self.toTextField.keyboardType = .numberPad
        self.fromTextField.keyboardType = .numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func setUpVoew(){
        self.selectionStyle = .none

        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        imageView.image = AppImages.dollar.image
        imageView.contentMode = .center
        
        let imageView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        imageView2.image = AppImages.dollar.image
        imageView2.contentMode = .center
        
        self.fromTextField.rightView = imageView
        self.fromTextField.rightViewMode = .always
       
        self.toTextField.rightView = imageView2
        self.toTextField.rightViewMode = .always
    
        self.fromTextField.placeholder = LocalizedString.From.localized
        self.toTextField.placeholder = LocalizedString.To.localized
        
        self.priceRangeLabel.setAttributes(text: LocalizedString.Price_Range.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
        self.fromTextField.text = SelectedFilters.shared.selectedFiltersOnItems.fromPrice == 0 ? "" : "\(SelectedFilters.shared.selectedFiltersOnItems.fromPrice)"
        
        self.toTextField.text = SelectedFilters.shared.selectedFiltersOnItems.toPrice == 0 ? "" : "\(SelectedFilters.shared.selectedFiltersOnItems.toPrice)"
        
        self.toTextField.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)

        self.fromTextField.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)

        self.toTextField.delegate = self
        self.fromTextField.delegate = self
        
        self.populateData()
    }
    
    
    func populateData(){
        if self.setupFor == .productsFilter{
           self.fromTextField.text = (SelectedFilters.shared.selectedFiltersOnItems.fromPrice == 0 ? "" : "\(SelectedFilters.shared.selectedFiltersOnItems.fromPrice)")
            self.toTextField.text = SelectedFilters.shared.selectedFiltersOnItems.toPrice == 0 ? "" : "\(SelectedFilters.shared.selectedFiltersOnItems.toPrice)"
        }else{
            self.fromTextField.text = (SelectedFilters.shared.selectedFilterOnShelf.fromPrice == 0 ? "" : "\(SelectedFilters.shared.selectedFilterOnShelf.fromPrice)")
            self.toTextField.text = SelectedFilters.shared.selectedFilterOnShelf.toPrice == 0 ? "" : "\(SelectedFilters.shared.selectedFilterOnShelf.toPrice)"
        }
    }
    
}

extension PriceRangeCell : UITextFieldDelegate {
    
    @objc func texfieldDidChange(_ textfield : UITextField){
        if let text = textfield.text {
            if text.count > 8 {
                textfield.text?.removeLast()
            }
        }
        
        if self.setupFor == .productsFilter{
            self.setPriceForProductFilter(textfield)
        }else{
            self.setPriceForShelfFilter((textfield))
        }
    }
    
    func setPriceForProductFilter(_ textfield : UITextField){
        if textfield == self.fromTextField{
            SelectedFilters.shared.selectedFiltersOnItems.fromPrice = Int(textfield.text ?? "") ?? 0
        }else{
            SelectedFilters.shared.selectedFiltersOnItems.toPrice = Int(textfield.text ?? "") ?? 0
        }
    }
    
    func setPriceForShelfFilter(_ textfield : UITextField){
        if textfield == self.fromTextField{
            SelectedFilters.shared.selectedFilterOnShelf.fromPrice = Int(textfield.text ?? "") ?? 0
        }else{
            SelectedFilters.shared.selectedFilterOnShelf.toPrice = Int(textfield.text ?? "") ?? 0
        }
    }
}


