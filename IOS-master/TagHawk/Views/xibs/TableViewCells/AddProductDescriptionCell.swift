//
//  AddProductDescriptionCell.swift
//  TagHawk
//
//  Created by Appinventiv on 19/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class AddProductDescriptionCell : UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionBackView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.descriptionBackView.setBorder(width: 0.5, color: AppColors.textfield_Border)
        self.descriptionBackView.round(radius: 10)
        self.titleLabel.setAttributes(text: "\(LocalizedString.Description.localized)*", font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.titleLabel.attributedText = "\(LocalizedString.Description.localized)*".attributeStringWithAstric()
        self.descriptionBackView.backgroundColor = AppColors.textfield_Border.withAlphaComponent(0.1)
        self.selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
