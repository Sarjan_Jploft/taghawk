//
//  CellWithLabel.swift
//  TagHawk
//
//  Created by Appinventiv on 21/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit


class CellWithLabel : UITableViewCell {

    enum LabelCellSetUpFor {
        case productPreViewCommunity
        case none
    }
    
    @IBOutlet weak var cellLabel: UILabel!
    
    var setUpFor = LabelCellSetUpFor.none {
        
        didSet {
            
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func populateData(txt : String) {
        self.cellLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(17), textColor: UIColor.black)
        self.cellLabel.attributedText = "\(LocalizedString.Share_To_Tags.localized): \(txt)".attributeBoldStringWithAnotherColor(stringsToColor: [txt], size: 16)
        cellLabel.isHidden = txt == "\n" ? true : false
//        switch self.setUpFor {
//
//            case .productPreViewCommunity:
//
//
//
//        default:
//            break
//        }
        
    }
    
    
    
}
