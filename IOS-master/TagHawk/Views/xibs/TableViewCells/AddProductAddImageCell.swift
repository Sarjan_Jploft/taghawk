//
//  AddProductAddImageCellTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 25/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class AddProductAddImageCell : UITableViewCell {

    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var imagesCollectionView : UICollectionView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        self.imagesCollectionView.registerNib(nibName: CapturedImagesCell.defaultReuseIdentifier)
        self.imagesCollectionView.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
