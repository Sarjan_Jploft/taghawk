//
//  SimilatProductsCell.swift
//  TagHawk
//
//  Created by Appinventiv on 31/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol SimilarProductTappedDelegate : class {
    func similarProductTapped(product : Product)
    func likeSimilarProduct(product : Product)
    func viewAllBtnTapped(_ sender: UIButton)
}

extension SimilarProductTappedDelegate {
    func likeSimilarProduct(product : Product){}
}

class SimilatProductsCell: UITableViewCell {

    enum SimilarProductsFor {
        case similarProducts
        case tagProducts
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var similarProductsCollectionView: UICollectionView!
    @IBOutlet weak var viewAllButton: UIButton!
    
    let controler = HomeControler()
    var products : [Product] = []
    var delegate : SimilarProductTappedDelegate?
    var setUpFor = SimilarProductsFor.similarProducts{
        didSet {
            self.viewAllButton.isHidden = self.setUpFor == .similarProducts
            self.titleLabel.text = self.setUpFor == .similarProducts ? LocalizedString.Similar_Items.localized : LocalizedString.Shelf.localized
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.configureCollectionView()
    }

    func configureCollectionView(){
//        self.similarProductsCollectionView.registerNib(nibName: ProductCollectionViewCell.defaultReuseIdentifier)
        self.similarProductsCollectionView.registerNib(nibName: ProductCollectionViewCellGridView.defaultReuseIdentifier)
        self.similarProductsCollectionView.registerNib(nibName: SearchedProduceCollectionViewCell.defaultReuseIdentifier)
        self.similarProductsCollectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.similarProductsCollectionView.delegate = self
        self.similarProductsCollectionView.dataSource = self
        self.similarProductsCollectionView.isScrollEnabled = false
    }
    
    @objc func likeButtonTapped(sender : UIButton){
        
        guard let indexPath = sender.collectionViewIndexPath(self.similarProductsCollectionView) else { return }
        
//      self.controler.likeUnlikeProduct(product: self.products[indexPath.item])
        self.delegate?.likeSimilarProduct(product: self.products[indexPath.item])
        self.products[indexPath.item].isLiked =  !self.products[indexPath.item].isLiked
        self.similarProductsCollectionView.reloadItems(at: [indexPath])
    }
    
    //MARK:- Like button tapped
    @objc func likeButtonTappedTag(_ sender: UIButton) {
        //view.endEditing(true)
        
        //        if self.product.isCreatedByMe{
        //            let vc = AddProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        //            vc.product = self.product
        //            vc.commingFor = .edit
        //            //vc.refreshDelegate = self
        //            self.present(vc, animated: true, completion: nil)
        //}else{
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        //        self.product = self.products[sender.tag]
        self.controler.likeUnlikeProduct(product: self.products[sender.tag])
        //        self.product.isLiked = !self.product.isLiked
        self.products[sender.tag].isLiked = !self.products[sender.tag].isLiked
        self.setIsLikedStatus(self.products[sender.tag],sender)
        //self.likeStatusBackDeleate?.getLikeStatus(product: self.product)
        //}
        
    }
    
    //MARK:- Set Like Status
    func setIsLikedStatus(_ productItem:Product,_ likeButtonAdd:UIButton){
        if !productItem.isCreatedByMe {
            likeButtonAdd.setImage(productItem.isLiked ? AppImages.filledHeart.image : AppImages.emptyHeart.image, for: UIControl.State.normal)
        }
    }
    
    
    
    
    
    
    @IBAction func viewAllBtnAction(_ sender: UIButton) {
        delegate?.viewAllBtnTapped(sender)
    }
}

//MARK:- CollectionView Datasource and Delegats
extension SimilatProductsCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        return self.products.count <= 4 ?  self.products.count : 4
        
        if self.setUpFor == .similarProducts{
            return (self.products.count <= 4 ?  self.products.count : 4)
        }else{
            return (self.products.count <= 6 ?  self.products.count : 6)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.setUpFor == .similarProducts ? 10 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return self.setUpFor == .similarProducts ? 10 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.setUpFor == .similarProducts {
            let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
            let finalContentWidth = contentWidth - 10
            return CGSize(width: finalContentWidth / 2, height: (finalContentWidth / 2) + 47)
        }else{
            let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
            let finalContentWidth = contentWidth - 2
//            return CGSize(width: finalContentWidth / 2, height: finalContentWidth / 2)
            return CGSize(width: finalContentWidth / 2, height: finalContentWidth / 2 + 68)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getProductCell(collectionView, cellForItemAt: indexPath)
    }

    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if self.setUpFor == .similarProducts{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchedProduceCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? SearchedProduceCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
            cell.dropShadow()
            cell.likeButton.addTarget(self, action: #selector(likeButtonTapped), for: UIControl.Event.touchUpInside)
            cell.populateData(product: self.products[indexPath.item])
            return cell
        }else{
//            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ProductCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
//            cell.cellFor = .tagDetail
//            cell.populateData(product: self.products[indexPath.item])
//            return cell
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCellGridView.defaultReuseIdentifier, for: indexPath) as? ProductCollectionViewCellGridView else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
            cell.cellFor = .tagDetail
            cell.populateData(product: self.products[indexPath.item])
            cell.btnLike.tag = indexPath.item
            cell.btnLike.addTarget(self, action: #selector(likeButtonTappedTag(_:)), for: .touchUpInside)
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.similarProductTapped(product: self.products[indexPath.item])
    }
}
