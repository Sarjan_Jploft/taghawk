//
//  TakePasportCell.swift
//  TagHawk
//
//  Created by Admin on 6/13/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import Photos

protocol RemoveDocumentDelegate : class {
    func removeDocumentAt(index : Int)
}

class TakePasportCell : UITableViewCell {

    @IBOutlet weak var takePassportLabel: UILabel!
    @IBOutlet weak var takePassPortCollectionView: UICollectionView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var uploadDocumentButton: UIButton!
    
    var selectedMediaArray : [Media] = []
    weak var removeDocDelegate : RemoveDocumentDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        takePassportLabel.text = LocalizedString.If_You_Dont_Have_A_SSN.localized
        self.takePassPortCollectionView.registerNib(nibName: DocumentCell.defaultReuseIdentifier)
        self.takePassPortCollectionView.delegate = self
        self.takePassPortCollectionView.dataSource = self
        self.uploadDocumentButton.setAttributes(title: LocalizedString.Upload.localized, font: AppFonts.Galano_Semi_Bold.withSize(16), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
 
 
    @objc func removeButtobTapped(sender : UIButton){
        guard let indexPath = sender.collectionViewIndexPath(self.takePassPortCollectionView) else { return }
        removeDocDelegate?.removeDocumentAt(index: indexPath.item)
    }
    
}


extension TakePasportCell : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedMediaArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DocumentCell.defaultReuseIdentifier, for: indexPath) as? DocumentCell else { fatalError("Could not dequeue DocumentCell at index \(indexPath)") }
       
        let asset = self.selectedMediaArray[indexPath.row].asset
        let manager = PHImageManager.default()
        
        manager.requestImage(for: asset,
                             targetSize: CGSize(width: collectionView.frame.width/3,
                                                height: collectionView.frame.width/3),
                             contentMode: .aspectFit,
                             options: nil) {(result, _) in
                                
                            cell.documentImageview.image = result
                            cell.outerView.layer.cornerRadius = 20
                              cell.outerView.layer.borderColor = AppColors.textViewPlaceholderColor.cgColor
                            cell.outerView.layer.borderWidth = 0.5
                                
        }
        cell.crossButton.addTarget(self, action: #selector(removeButtobTapped), for: UIControl.Event.touchUpInside)
        
        return cell
   
    }
    
}
