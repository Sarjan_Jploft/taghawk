//
//  SellarRatingCell.swift
//  TagHawk
//
//  Created by Appinventiv on 07/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class SellarRatingCell : UITableViewCell {

    enum SelectRatingFor {
        case productFilter
        case shelfFilter
    }
    
    @IBOutlet weak var sellorRatingLabel: UILabel!
    @IBOutlet weak var ratingCollectionView: UICollectionView!
    
    var setUpFor = SelectRatingFor.productFilter
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.ratingCollectionView.registerNib(nibName: RatingCollectionViewCell.defaultReuseIdentifier)
        self.ratingCollectionView.delegate = self
        self.ratingCollectionView.dataSource = self
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension SellarRatingCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
       let itemWidth = (collectionView.frame.width - 12) / 6
        
        return CGSize(width: itemWidth - 2 , height: itemWidth * 70 / 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RatingCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? RatingCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        cell.ratingButton.setTitle("\(indexPath.item + 1)")
        cell.ratingButton.addTarget(self, action: #selector(ratingButtonTapped), for: UIControl.Event.touchUpInside)
      
        self.setUpFor == .productFilter ? cell.setSelectDeselectState(isSelected: SelectedFilters.shared.selectedFiltersOnItems.rating == indexPath.item + 1) : cell.setSelectDeselectState(isSelected: SelectedFilters.shared.selectedFilterOnShelf.rating == indexPath.item + 1)
        
        
        return cell
        
    }
    
    @objc func ratingButtonTapped(sender : UIButton){
        
        guard let indexPath = sender.collectionViewIndexPath(self.ratingCollectionView) else { return }
        
        if self.setUpFor == .productFilter{
            SelectedFilters.shared.selectedFiltersOnItems.rating = SelectedFilters.shared.selectedFiltersOnItems.rating == indexPath.item + 1 ? 0 : indexPath.item + 1
        }else {
            SelectedFilters.shared.selectedFilterOnShelf.rating = SelectedFilters.shared.selectedFilterOnShelf.rating == indexPath.item + 1 ? 0 : indexPath.item + 1
        }
        
        self.ratingCollectionView.reloadData()
        
    }
}
