//
//  AddProductWeightCell.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

protocol GetSelectedShippingData : class {
    func getSelectedShippingData(data : ShippingWeightData)
    func getSelectedShippingMode(data : ShippingMode)
}

class AddProductWeightCell: UITableViewCell {

    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var dropDownLabel: UILabel!
    @IBOutlet weak var fedexButton: UIButton!
    @IBOutlet weak var uspsPriceLabel: UILabel!
    @IBOutlet weak var fedexPriceLabel: UILabel!
    @IBOutlet weak var uspsButton: UIButton!
    @IBOutlet weak var noteLabel: UILabel!
    
    
    private let dropDown = DropDown()
    var shippingWeightData : [ShippingWeightData] = []
    weak var delegate : GetSelectedShippingData?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    @IBAction func dropDownButtonTapped(_ sender: UIButton) {
        setUpDropDownForProductWeight()
    }
    
    @IBAction func fedexButtonTapped(_ sender: UIButton) {
        self.delegate?.getSelectedShippingMode(data: ShippingMode.fedex)
    }
    
    @IBAction func uspsButtonTapped(_ sender: UIButton) {
    self.delegate?.getSelectedShippingMode(data: ShippingMode.usps)
    }
    
    func setUpView(){
        self.selectionStyle = .none
        
        self.titleLabel.setAttributes(text: LocalizedString.Choose_Weight.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
        self.titleLabel.attributedText = "\(LocalizedString.Choose_Weight.localized)".attributeStringWithAstric()

        self.dropDownView.setBorder(width: 1, color: AppColors.textfield_Border)
        self.dropDownView.round(radius: 10)
        self.dropDownLabel.setAttributes(text: LocalizedString.Search.localized, font: AppFonts.Galano_Regular.withSize(14), textColor: AppColors.lightGreyTextColor)
        
        self.noteLabel.setAttributes(text: LocalizedString.Will_Receive_Shipping_Label_If_Buyers_Select_FedEx.localized, font: AppFonts.Galano_Regular.withSize(13).italic, textColor: UIColor.black, backgroundColor: UIColor.clear)
        
        self.setUpDropDown()
    }
    
    func setUpDropDown(){
        self.dropDown.backgroundColor = UIColor.white
        self.dropDown.selectionBackgroundColor = UIColor.white
    }
    
    func setUpDropDownLabel(labelText : String, withColor : UIColor){
        self.dropDownLabel.text = labelText.isEmpty ? LocalizedString.Select.localized : labelText
        self.dropDownLabel.textColor = withColor
    }
    
    func setUpDropDownForProductWeight(){

        self.superview?.endEditing(true)
        self.dropDown.anchorView = self.dropDownButton
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
        self.dropDown.dismissMode = .automatic
        
       let shippingData = self.shippingWeightData.map { (data) -> String in
            return data.weight
        }
        
        self.dropDown.dataSource = shippingData
        self.dropDown.show()
       
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            self?.delegate?.getSelectedShippingData(data: weakSelf.shippingWeightData[index])
        }
    }
    
    func populateData(mode : ShippingMode, data : ShippingWeightData, isShippingSelected : Bool){
        self.selectDeselectShippingMode(mode: mode)
        self.dropDownLabel.text = data.weight
        self.fedexPriceLabel.text = "$\(data.fedexPrice)"
        self.uspsPriceLabel.text = "$\(data.uspsPrice)"
      
        self.setUpDropDownLabel(labelText: data.weight, withColor: data.weight.isEmpty ? AppColors.lightGreyTextColor : UIColor.black)
        
        self.uspsPriceLabel.isHidden = data.uspsPrice == 0
        self.fedexPriceLabel.isHidden = data.fedexPrice == 0
        self.uspsButton.isEnabled = data.isAvailableInUsps
        self.fedexButton.isEnabled = data.isAvailableInFedex

        let uspsAvailColor = data.isAvailableInUsps ? UIColor.black : AppColors.lightGreyTextColor
        self.uspsButton.setTitleColor(uspsAvailColor)
        self.uspsPriceLabel.textColor = uspsAvailColor
        
        let fedexAvailColor = data.isAvailableInFedex ? UIColor.black : AppColors.lightGreyTextColor
        self.fedexButton.setTitleColor(fedexAvailColor)
        self.fedexPriceLabel.textColor = (fedexAvailColor)
        
        self.dropDownView.isHidden = !isShippingSelected
        self.uspsButton.isHidden = true
        self.uspsPriceLabel.isHidden = true
    }
    
    
    func selectDeselectShippingMode(mode : ShippingMode){
        
        let fedexSelectionImage = mode == .fedex ? AppImages.tickWithCircle.image : AppImages.unTickWithCircle.image
        
        self.fedexButton.setImage(fedexSelectionImage, for: UIControl.State.normal)
        
        
        let uspsSelectionImage = mode == .usps ? AppImages.tickWithCircle.image : AppImages.unTickWithCircle.image

        self.uspsButton.setImage(uspsSelectionImage, for: UIControl.State.normal)
        
    }
    
    
}
