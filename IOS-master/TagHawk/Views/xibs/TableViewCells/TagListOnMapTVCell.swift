//
//  TagListOnMapTVCell.swift
//  TagHawk
//
//  Created by Apple on 07/01/20.
//  Copyright © 2020 TagHawk. All rights reserved.
//

import UIKit

class TagListOnMapTVCell: UITableViewCell {
    
    
    @IBOutlet weak var tagImageView: UIImageView!
    @IBOutlet weak var tagNameLabel: UILabel!
    @IBOutlet weak var publicPrivateLabel: UILabel!
    @IBOutlet weak var numberOfMembersLabel: UILabel!
    @IBOutlet weak var lblDistanceFromTag: UILabel!
    @IBOutlet weak var btnApply: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func populateData(tag : Tag){
        
        self.tagNameLabel.text = tag.name
        self.numberOfMembersLabel.text = "\(tag.members) \(LocalizedString.Members.localized)"
        //           self.founderLabel.text = "\(LocalizedString.Founder.localized): \(tag.adminName)"
        let type = tag.entrance == .privateTag ? LocalizedString.Private.localized : LocalizedString.Public.localized
        self.publicPrivateLabel.text = type
        self.lblDistanceFromTag.text = ""
        self.tagImageView.setImage_kf(imageString: tag.tagImageUrl, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
        
        //SetApplyJoinButtonTitle(tagItem: tag)
    }
    
    
    
    func SetApplyJoinButtonTitle(tagItem:Tag){
        
        
        if tagItem.requestStatus == JoinTagStatus.ACTIVE.rawValue {
            //self.btnApply.setTitle(LocalizedString.Chat.localized)
            self.btnApply.isHidden = true
            
        } else if tagItem.requestStatus == JoinTagStatus.REJECT.rawValue {
            if tagItem.entrance == .publicTag {
                self.btnApply.setTitle(LocalizedString.Join.localized)
            } else {
                self.btnApply.setTitle(LocalizedString.Apply.localized)
            }
        } else if tagItem.requestStatus == JoinTagStatus.PENDING.rawValue {
            //self.btnApply.setTitle(LocalizedString.Pending.localized)
            self.btnApply.isHidden = true
        }
        if tagItem.entrance == .publicTag {
            self.btnApply.setTitle(LocalizedString.Join.localized)
            if tagItem.requestStatus == JoinTagStatus.ACTIVE.rawValue {
                //self.btnApply.setTitle(LocalizedString.Chat.localized)
                self.btnApply.isHidden = true
            }
        }
        if tagItem.isCreatedByMe {
            self.btnApply.isHidden = true
//            self.btnApply.setTitle(LocalizedString.Edit.localized)
            
        }
        
    }
    
    
    
    
}
