//
//  ShareToCommunityCell.swift
//  TagHawk
//
//  Created by Appinventiv on 19/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol SetCommunitySelection : class {
    func setselectedCommunities(tags : [Tag])
    func viewAllBtnTapped(_ sender: UIButton)
}

class ShareToCommunityCell : UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var selectCommunityCollectionView: UICollectionView!
    @IBOutlet weak var selectedTagCountLbl: UILabel!
    
    var userTags : [Tag] = []
    var selectedUserTags : [Tag] = []
    var shouldHideViewAllBtn = false
    weak var delegate : SetCommunitySelection?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        self.titleLabel.setAttributes(text: LocalizedString.Share_To_Community.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.titleLabel.attributedText = "\(LocalizedString.Share_To_Community.localized)".attributeStringWithAstric()
        self.selectCommunityCollectionView.registerNib(nibName: SelectCommunityCollectionViewCell.defaultReuseIdentifier)
        self.selectCommunityCollectionView.delegate = self
        self.selectCommunityCollectionView.dataSource = self
        self.selectCommunityCollectionView.contentInset = UIEdgeInsets(top: 5, left: 15, bottom: 5, right: 15)
        self.selectionStyle = .none
    }
    
    func setUpCell(_ hideAll: Bool = false) {
        if hideAll {
            viewAllBtn.isHidden = true
            titleLabel.isHidden = true
            selectedTagCountLbl.isHidden = true
        } else {
            viewAllBtn.isHidden = shouldHideViewAllBtn
            titleLabel.isHidden = false
            
            self.titleLabel.attributedText = "\(LocalizedString.Share_To_Community.localized)".attributeStringWithAstric()
            
            selectedTagCountLbl.text = "(\(selectedUserTags.count))"
            selectedTagCountLbl.isHidden = selectedUserTags.count > 0 ? false : true
        }
    }
    
    
    @IBAction func viewAllBtnAction(_ sender: UIButton) {
        delegate?.viewAllBtnTapped(sender)
    }
}


extension ShareToCommunityCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        return self.userTags.count > 6 ? 6 : self.userTags.count
        return self.userTags.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 2
        return CGSize(width: finalContentWidth / 2, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getcommunityCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getcommunityCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SelectCommunityCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? SelectCommunityCollectionViewCell else { fatalError("Could not dequeue SelectCommunityCollectionViewCell at index \(indexPath) in LoginVC") }
       
        cell.populateData(tag: self.userTags[indexPath.item])
        
        if self.selectedUserTags.contains(where: { (item) -> Bool in
            item.id == self.userTags[indexPath.item].id
        }){
            cell.tickImageView.image = AppImages.tickWithSquare.image
       }else{
            cell.tickImageView.image = AppImages.unTickWithSquare.image
        }
        
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let ind = selectedUserTags.firstIndex(where: { (obj) -> Bool in
            return obj.id == self.userTags[indexPath.item].id
        }){
            self.selectedUserTags.remove(at: ind)
        } else{
            self.selectedUserTags.append(self.userTags[indexPath.item])
        }
        
        self.selectCommunityCollectionView.reloadItems(at: [IndexPath(row: indexPath.item, section: 0)])
        self.delegate?.setselectedCommunities(tags: self.selectedUserTags)
        
    }
}
