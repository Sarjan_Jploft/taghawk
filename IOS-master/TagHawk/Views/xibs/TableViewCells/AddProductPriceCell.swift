//
//  AddProductTextFieldCell.swift
//  TagHawk
//
//  Created by Appinventiv on 11/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import PWSwitch


protocol FirmPriceValueChanged : class {
    func firmPriceValueChanged(value : Bool)
    func transSwitchToggle(value: Bool)
    func infoButtonTapped(sender: UIButton)
}

class AddProductPriceCell : UITableViewCell {
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var firmPricelabel: UILabel!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var toggleSwitch: PWSwitch!
//    @IBOutlet weak var includeTransFeeLbl: UILabel!
//    @IBOutlet weak var includeTransFeeSwitch: PWSwitch!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var youEarnLabel: UILabel!
    @IBOutlet weak var serviceFeeLabel: UILabel!
    
    
    weak var delegate : FirmPriceValueChanged?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        priceTextField.delegate = self
        self.selectionStyle = .none
        self.priceLabel.setAttributes(text: LocalizedString.Price.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
       
        self.serviceFeeLabel.setAttributes(text: LocalizedString.Service_Fee.localized, font: AppFonts.Galano_Regular.withSize(15), textColor: UIColor.black)
        
        self.youEarnLabel.setAttributes(text: LocalizedString.You_Earn.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)

//        self.priceLabel.setAttributes(attributedText: LocalizedString.Price.localized.attributeStringWithAstric(), font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
        self.priceLabel.attributedText = LocalizedString.Price.localized.attributeStringWithAstric()
        
        self.textFieldView.round(radius: 10)
        self.textFieldView.setBorder(width: 0.5, color: AppColors.textfield_Border)
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        imageView.image = AppImages.dollar.image
        imageView.contentMode = .center
        self.priceTextField.rightView = imageView
        self.priceTextField.rightViewMode = .always
        self.textFieldView.backgroundColor = AppColors.textfield_Border.withAlphaComponent(0.1)
        self.priceTextField.keyboardType = .decimalPad
        
        self.toggleSwitch.thumbOffFillColor = AppColors.grayColor
        self.toggleSwitch.thumbOnFillColor = AppColors.appBlueColor
        
        self.toggleSwitch.thumbOnBorderColor = AppColors.appBlueColor
        self.toggleSwitch.thumbOffBorderColor = AppColors.grayColor
        
        self.toggleSwitch.trackOffFillColor = AppColors.textfield_Border
        self.toggleSwitch.trackOnFillColor = AppColors.appBlueColor
        
        self.toggleSwitch.trackOnBorderColor = AppColors.appBlueColor
        self.toggleSwitch.trackOffBorderColor = AppColors.textfield_Border
        
        
//        self.includeTransFeeLbl.setAttributes(text: LocalizedString.IncludeTransactionFee.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
//        self.includeTransFeeSwitch.thumbOffFillColor = AppColors.grayColor
//        self.includeTransFeeSwitch.thumbOnFillColor = AppColors.appBlueColor
//
//        self.includeTransFeeSwitch.thumbOnBorderColor = AppColors.appBlueColor
//        self.includeTransFeeSwitch.thumbOffBorderColor = AppColors.grayColor
//
//        self.includeTransFeeSwitch.trackOffFillColor = AppColors.textfield_Border
//        self.includeTransFeeSwitch.trackOnFillColor = AppColors.appBlueColor
//
//        self.includeTransFeeSwitch.trackOnBorderColor = AppColors.appBlueColor
//        self.includeTransFeeSwitch.trackOffBorderColor = AppColors.textfield_Border
//        self.toggleSwitch.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    

    func calculateServicePriceAndEarnings(price : Double){
        
        if price == 0{
            self.serviceFeeLabel.text = "\(LocalizedString.Service_Fee.localized) $0"
            self.youEarnLabel.text = "\(LocalizedString.You_Earn.localized) $0"
        }else if price >= 0.8 && price <= 10{
            self.serviceFeeLabel.text = "\(LocalizedString.Service_Fee.localized) $0.8"
            self.youEarnLabel.text = "\(LocalizedString.You_Earn.localized) $\((price - 0.8).rounded(toPlaces: 2))"
        }else{
          let percentValue = (price * 8 / 100).rounded(toPlaces: 2)
            self.serviceFeeLabel.text = "\(LocalizedString.Service_Fee.localized) $\(percentValue)"
            self.youEarnLabel.text = "\(LocalizedString.You_Earn.localized) $\((price - percentValue).rounded(toPlaces: 2))"
        }
    }
    
    @IBAction func toggleValueChanged(_ sender: PWSwitch) {
        self.delegate?.firmPriceValueChanged(value: sender.on)
    }
    
    @IBAction func includeTransFeeBtnAction(_ sender: PWSwitch) {
        self.delegate?.transSwitchToggle(value: sender.on)
    }
    
    @IBAction func infoBtnAction(_ sender: UIButton) {
        self.delegate?.infoButtonTapped(sender: sender)
    }
    
    func populateData(data : AddProductData){
//        self.toggleSwitch.setOn(!data.isNegotiable, animated: false)
        self.toggleSwitch.setOn(data.isNegotiable, animated: false)
        
        self.priceTextField.text = data.price == 0 ? "" :   "\(data.price)"
//        let str_RoundedPrice = (data.price).rounded(toPlaces: 2) == 0 ? "" :   "\((data.price).rounded(toPlaces: 2))"
//        self.priceTextField.text = str_RoundedPrice
//        self.includeTransFeeSwitch.setOn(data.isTransactionCost, animated: false)
    }
    
}


extension AddProductPriceCell : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let userEnteredString = textField.text else { return false }
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        return self.isCharacterAcceptableForPrice(name: userEnteredString, char: string)
    }
    
    func isCharacterAcceptableForPrice(name : String, char : String)-> Bool {
        
        let dotString = "."
        
        if char.isBackSpace {
            return true
        }else  if name.contains(dotString) {
            if name.components(separatedBy: dotString)[1].count == 2 {
            return false
        }
            return true
      }
        else if name.count > 8{
            return false
        }else{
            return true
        }
    }
}
