//
//  CashOutRequiredDataCell.swift
//  TagHawk
//
//  Created by Admin on 6/13/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CashOutRequiredDataCell: UITableViewCell {

    enum CashoutRequiredCellFor{
        case dob
        case ssn
        case phoneNumber
        case address
    }
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var notationImageView: UIImageView!
    @IBOutlet weak var addOrEditButton: UIButton!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var dobTextField: UITextField!
   
    var setUpFor = CashoutRequiredCellFor.dob

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.dobTextField.tintColor = UIColor.clear
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func populateData(data : CashOutInputDtata){
        if self .setUpFor == .ssn{
            self.setUpForSsn(data: data)
        }else if setUpFor == .dob{
            self.setUpForDob(data: data)
        }else if setUpFor == .phoneNumber{
            self.setUpForPhone(data: data)
        }else{
            self.setUpForAddress()
        }
    }
    
    func setUpForDob(data : CashOutInputDtata){
        self.dobTextField.isHidden = false
        self.headingLabel.text = LocalizedString.dateOfBirth.localized
        self.valueLabel.text = data.dob
        
        if data.dob.isEmpty {
            self.addOrEditButton.setAttributes(title: LocalizedString.Add.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
        }else{
            self.addOrEditButton.setAttributes(title: LocalizedString.Edit.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), titleColor: UIColor.white, backgroundColor: AppColors.segmentTextColor)
        }
    }
 
    func setUpForSsn(data : CashOutInputDtata){
        self.dobTextField.isHidden = true
        self.headingLabel.text = LocalizedString.Last_4_SSN.localized
        self.valueLabel.text = data.ssn
      
        if data.ssn.isEmpty {
            self.addOrEditButton.setAttributes(title: LocalizedString.Add.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
        }else{
            self.addOrEditButton.setAttributes(title: LocalizedString.Edit.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), titleColor: UIColor.white, backgroundColor: AppColors.segmentTextColor)
        }
    }
    
    func setUpForPhone(data : CashOutInputDtata){
        
        self.dobTextField.isHidden = true
        self.headingLabel.text = LocalizedString.phone.localized
        self.valueLabel.text = "\(data.countryCode)\(data.phoneNumber)" 
        
        if data.phoneNumber.isEmpty{
            self.addOrEditButton.setAttributes(title: LocalizedString.Add.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
        }else if !data.isPhoneVerified {
            self.addOrEditButton.setAttributes(title: LocalizedString.Verify.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
        } else {
            self.addOrEditButton.setAttributes(title: LocalizedString.Verified.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), titleColor: UIColor.white, backgroundColor: #colorLiteral(red: 0.2470588235, green: 0.9843137255, blue: 0.9176470588, alpha: 1))
        }
    }
    
    
    func setUpForAddress(){
        self.dobTextField.isHidden = true
        self.headingLabel.text = LocalizedString.Address.localized
        self.valueLabel.text = !UserProfile.main.city.isEmpty ? "\(UserProfile.main.line1), \(UserProfile.main.line2), \(UserProfile.main.city), \(UserProfile.main.state), \(UserProfile.main.zipCode)" : ""
        
        if UserProfile.main.city.isEmpty{
            self.addOrEditButton.setAttributes(title: LocalizedString.Add.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), titleColor: UIColor.white, backgroundColor: AppColors.appBlueColor)
        }else{
            self.addOrEditButton.setAttributes(title: LocalizedString.Edit.localized, font: AppFonts.Galano_Semi_Bold.withSize(11), titleColor: UIColor.white, backgroundColor: AppColors.segmentTextColor)
        }
        
//        self.valueLabel.text = "\(data.countryCode)\(data.phoneNumber)"
        
    }
    
}
