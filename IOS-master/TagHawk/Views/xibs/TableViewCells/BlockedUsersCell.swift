//
//  BlockedUsersCell.swift
//  TagHawk
//
//  Created by Appinventiv on 12/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class BlockedUsersCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var unblockButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        profileImageView.round()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.unblockButton.setAttributes(title: LocalizedString.Unblock.localized, font: AppFonts.Galano_Medium.withSize(12), titleColor: AppColors.lightGreyTextColor,backgroundColor: UIColor.white)
        self.unblockButton.round(radius: 5)
        self.unblockButton.setBorder(width: 0.5, color: AppColors.lightGreyTextColor)
        self.sepratorView.backgroundColor = AppColors.textfield_Border
        self.selectionStyle = .none

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func populateData(user : FollowingFollowersModel){
        self.nameLabel.text = user.fullName
        self.profileImageView.setImage_kf(imageString: user.profilePicture, placeHolderImage: AppImages.productPlaceholder.image, loader: false)
    }
    
}
