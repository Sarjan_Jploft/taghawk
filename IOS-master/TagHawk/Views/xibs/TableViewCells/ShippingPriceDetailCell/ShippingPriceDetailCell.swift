//
//  ShippingPriceDetailCell.swift
//  TagHawk
//
//  Created by Appinventiv on 10/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ShippingPriceDetailCell: UITableViewCell {

    // MARK:- VARIABLES
    //====================
    
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var totalMrpLbl: UILabel!
    @IBOutlet weak var shippingChargesLbl: UILabel!
    @IBOutlet weak var totalAmtLbl: UILabel!
    
    // MARK:- IBACTIONS
    //====================
    
    
    
    // MARK:- LIFE CYCLE
    //====================
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // MARK:- FUNCTIONS
    //====================
    
    ///Initial Setup
    private func initialSetup() {
        mainView.addShadow()
    }
}
