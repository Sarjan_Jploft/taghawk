//
//  FeaturePlanCellTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 27/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import StoreKit

class FeaturePlanCell : UITableViewCell {

    @IBOutlet weak var planNameLabel: UILabel!
    @IBOutlet weak var priceButton: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.planNameLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(14), textColor: UIColor.black)
        self.priceButton.setAttributes(title: "", font: AppFonts.Galano_Semi_Bold.withSize(15), titleColor: AppColors.appBlueColor)
        self.priceButton.round(radius: 10)
        self.priceButton.setBorder(width: 0.5, color: AppColors.appBlueColor)
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
//
//    func populateData(title : String, price : String){
//        self.planNameLabel.text = title
//        self.priceButton.setTitle("$\(price)")
//    }

    func populateData(product : SKProduct){
        self.planNameLabel.text = product.localizedTitle
        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = product.priceLocale
        let priceStr = numberFormatter.string(from: product.price)
        self.priceButton.setTitle(priceStr ?? "")
    }

    func setSelectedDeselectedState(currentProd : SKProduct, selectedProduct : SKProduct?){
            guard let seleProd = selectedProduct else {
            self.priceButton.backgroundColor = UIColor.white
            self.priceButton.setTitleColor(AppColors.appBlueColor)
            return
        }

        let isSelectedProduct = seleProd.productIdentifier == currentProd.productIdentifier

        self.priceButton.backgroundColor =  isSelectedProduct ? AppColors.appBlueColor : UIColor.white

        isSelectedProduct ? self.priceButton.setTitleColor(UIColor.white) : self.priceButton.setTitleColor(AppColors.appBlueColor)

    }


}
