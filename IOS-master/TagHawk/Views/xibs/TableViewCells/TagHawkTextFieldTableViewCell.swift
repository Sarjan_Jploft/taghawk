//
//  TagHawkTextFieldTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 22/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol CustomFieldDelagate : class {
    func texfieldDidChange(type : TagHawkTextFieldTableViewCell.TextFieldType, text : String?)
    func textFieldShouldReturn(type : TagHawkTextFieldTableViewCell.TextFieldType)
}

class TagHawkTextFieldTableViewCell: UITableViewCell {

    enum TextFieldType {
        
        case firstName
        case lastName
        case email
        case fullName
        case password
        case confirmPassword
        case invitationCode
        case other
        
        
    }
    
    @IBOutlet weak var textField: CustomTextField!
    
    weak var delegate : CustomFieldDelagate?
    var textFieldType = TextFieldType.other {
        
        didSet {
            self.textField.isSecureTextEntry = false
            self.textField.keyboardType = .default
            self.textField.autocapitalizationType = .none
            self.textField.returnKeyType = .default

            switch textFieldType {
            case .fullName:
                self.textField.autocapitalizationType = .words
            case .firstName:
                self.textField.autocapitalizationType = .words
            case .lastName:
                self.textField.autocapitalizationType = .words
            case .email:
                self.textField.keyboardType = .emailAddress
            case .password:
                self.textField.isSecureTextEntry = true
                self.textField.returnKeyType = .done
                
            case .invitationCode:
                self.textField.returnKeyType = .done

            default:
                break
            }
        }
    }
    
    var textFieldState : TextFieldState = .none

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textField.delegate = self
        self.textField.addTarget(self, action: #selector(self.texfieldDidChange(_:)), for: .editingChanged)
        self.textField.textColor = UIColor.black
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateText(type : TextFieldType, userInput : UserInput){
        
        switch type {
        case .fullName:
            self.textField.text = userInput.fullName
        case .firstName:
            self.textField.text = userInput.firstName
        case .lastName:
            self.textField.text = userInput.lastName
        case .email:
             self.textField.text = userInput.email
        case .password:
             self.textField.text = userInput.password
        case .invitationCode:
            self.textField.text = userInput.invitationCode
        default:
            break
        }
    }
    
    func didShowError(state : TextFieldState){
        self.textFieldState = state
        switch state {
        case .invalid:
            self.textField.error = state.associatedValue
        default:
           self.textField.error = nil
        }
    }
    
}


extension TagHawkTextFieldTableViewCell : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let userEnteredString = textField.text else { return false }
//        let newString = (userEnteredString as NSString).replacingCharacters(in: range, with: string) as NSString
        
        switch self.textFieldState {
        case .invalid:
            self.textField.error = nil
        default:
           break
        }
       
       if (range.location == 0 && string == " ") || string.containsEmoji {return false}
  
        switch self.textFieldType {
        case .firstName, .lastName:
            return self.isCharacterAcceptableForFirstName(name: userEnteredString, char: string)
        case .email:
            return self.isCharacterAcceptableForEmail(email: userEnteredString, char: string)
        case .password:
            return self.isCharacterAcceptableForPassword(password: userEnteredString, char: string)
        default:
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.delegate?.textFieldShouldReturn(type: self.textFieldType)
        return true
    }
    
     @objc private func texfieldDidChange(_ textfield: UITextField) {
        self.delegate?.texfieldDidChange(type: self.textFieldType, text: textfield.text)
    }
    
    func isCharacterAcceptableForFirstName(name : String, char : String)-> Bool {
        if char.isBackSpace {
            return true
        } else if name.count > 20{
            return false
        }else{
            return char.isAllowed(validationStr: AllowedCharactersValidation.name)
        }
    }
    
    func isCharacterAcceptableForEmail(email : String, char : String) -> Bool {
        return char.isBackSpace || email.count <= 50
    }
    
    func isCharacterAcceptableForPassword(password : String, char : String) -> Bool {
        return char.isBackSpace || password.count <= 15
    }
    
}

