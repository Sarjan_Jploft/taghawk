//
//  ProductDetailMapCell.swift
//  TagHawk
//
//  Created by Appinventiv on 31/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class ProductDetailMapCell: UITableViewCell {
    
    enum MapCellFor {
        case previewProduct
        case tagDetail
        case productDetail
    }
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var milesLabel: UILabel!
    @IBOutlet weak var mapBtn: UIButton!
    
    private let circle = GMSCircle()
    var mapViewTap: (() -> ())?
    var setUpFor = MapCellFor.productDetail
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.selectionStyle = .none
        self.distanceLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(16), textColor: AppColors.lightGreyTextColor)
        self.locationLabel.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(15), textColor: UIColor.black)
        self.mapView.round(radius: 10)
        self.mapView.setBorder(width: 0.5, color: AppColors.textfield_Border)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func mapBtnAction(_ sender: UIButton) {
        mapViewTapped()
    }
    
    func populateData(product : Product){
        
        self.locationLabel.text = product.productAddress
        self.distanceLabel.text = LocalizedString.Your_Location_Distance.localized + ":"
        self.addMarkerToCordinates(lat: product.productLatitude, long: product.productLongitude)
        self.setCameraPosition(lat: product.productLatitude, long: product.productLongitude)
     
        var miles = 0.0

        if SelectedFilters.shared.appliedFiltersOnItems.lat != 0.0{
            let distance : Double = calculateDistanceBetweenTowCord(lat1: product.productLatitude, long1: product.productLongitude, lat2: SelectedFilters.shared.appliedFiltersOnItems.lat, long2: SelectedFilters.shared.appliedFiltersOnItems.long)
            miles = distance * 0.000621371
        }else{
            let distance : Double = calculateDistanceBetweenTowCord(lat1: product.productLatitude, long1: product.productLongitude, lat2: LocationManager.shared.latitude, long2: LocationManager.shared.longitude)
            miles = distance * 0.000621371
        }
        
        miles.round(.up)
        self.milesLabel.text = "\(Int(miles)) \(LocalizedString.Miles.localized)"
        self.getLoc(product : product)
    }
    
    func getLoc(product : Product){
        LocationManager.shared.reverseGeocodeLocationWithLatLon(latitude: product.productLatitude, longitude: product.productLongitude) { (dict, placeMarker, error) in
            guard let data = dict else { return }
            //
            guard let state = data[ApiKey.locality] as? String else { return }
            guard let country = data[ApiKey.administrativeArea] as? String else { return }
            
            self.locationLabel.text = "\(state), \(country)"
            
            //            printDebug(data)
            
        }
    }
    
    func populateData(product : AddProductData){
        self.locationLabel.text = ""
        self.distanceLabel.text = ""
        self.milesLabel.text = ""
        self.addMarkerToCordinates(lat: product.lat, long: product.long)
        self.setCameraPosition(lat: product.lat, long: product.long)
    }

    func addMarkerToCordinates(lat : Double, long : Double){
        let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
//        let marker = GMSMarker(position: position)
//        marker.icon = AppImages.blueMarker.image
//        marker.map = mapView
        circle.map = nil
        circle.position = position
        circle.radius = 2500
        circle.fillColor = #colorLiteral(red: 0.168627451, green: 0.8078431373, blue: 0.9921568627, alpha: 0.4560684419)
        circle.strokeColor = #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 0.6538842429)
        circle.strokeWidth = 2
        circle.map = mapView
    }
    
    func setCameraPosition(lat : Double, long : Double){
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 12)
        mapView.animate(to: camera)
    }
    
    func calculateDistanceBetweenTowCord(lat1 : Double, long1 : Double, lat2 : Double, long2 : Double) -> CLLocationDistance{
        
        let locA = CLLocation(latitude: lat1, longitude: long1)
        let locB = CLLocation(latitude: lat2, longitude: long2)
        let distance = locA.distance(from: locB)
        return distance
        
    }
    
    private func mapViewTapped() {
        if setUpFor == .previewProduct { return }
        mapViewTap?()
        let prodLoc = circle.position
        let mapScene = MapVC.instantiate(fromAppStoryboard: .Home)
        mapScene.productLocation = prodLoc
        AppNavigator.shared.parentNavigationControler.pushViewController(mapScene, animated: true)
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    
    func roundAndFixDigit(_ value:Double)->String{
        
        if value.truncatingRemainder(dividingBy: 1) == 0{
            
            return String(format: "%.0f", value)
            
        }else{
            
            let nf = NumberFormatter()
            nf.numberStyle = .decimal
            nf.minimumFractionDigits = 2
            nf.maximumFractionDigits = 2
            
            if let strNum = nf.string(for: value) {
                print(strNum)
                return strNum
            }
            return ""
            
        }
        
        
    }
   
    
}

extension Float {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}


