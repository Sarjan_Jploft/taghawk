//
//  CardCellTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 05/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CardCell : UITableViewCell {

    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var defaultLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.brandLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.defaultLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(14), textColor: AppColors.lightGreyTextColor)
        self.defaultLabel.text = LocalizedString.Default.localized
        self.sepratorView.backgroundColor = AppColors.textfield_Border
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func populateData(card : Card){
        self.cardNumberLabel.text = "xxxx\(card.last4)"
        self.brandLabel.text = card.brand.rawValue
        self.defaultLabel.isHidden = !card.isDefailt
        
        switch card.brand {
        case .visa:
            self.cardImageView.image = AppImages.visa.image
        
        case .master:
            self.cardImageView.image = AppImages.master.image

        case .discover:
            self.cardImageView.image = AppImages.discover.image

        case .amex:
            self.cardImageView.image = AppImages.amex.image

        }
        
    }
    
}
