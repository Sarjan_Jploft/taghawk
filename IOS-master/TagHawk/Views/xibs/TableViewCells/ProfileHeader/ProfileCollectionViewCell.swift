//
//  ProfileCollectionViewCell.swift
//  TagHawk
//
//  Created by Vikash on 07/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var selectionImageView: UIImageView!
    
    var buttonTapped: ((UIButton) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionImageView.isHidden = true
    }

    @IBAction func clickMoreButton(_ sender: UIButton) {
        buttonTapped?(sender)
    }
    
    
    
}
