//
//  ProfileItemTableViewCell.swift
//  TagHawk
//
//  Created by Vikash on 07/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit


class ProfileItemTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var userProduct: [UserProduct]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.registerCell(with: ProfileCollectionViewCell.self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateData(product: [UserProduct]) {
        self.userProduct = product
        self.collectionView.reloadData()
    }
  
}

extension ProfileItemTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let product = self.userProduct {
            return product.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: ProfileCollectionViewCell.self, indexPath: indexPath)
        if let product = self.userProduct {
            cell.productImage.setImage_kf(imageString: product[indexPath.item].images[0].image, placeHolderImage: UIImage())
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
  
        let height = collectionView.height
        let screenWidth = UIDevice.width
        let mininumInterItemSpacing: CGFloat = 10.0
        let mininumLineItemSpacing: CGFloat = 10.0
        
        //let cellHeight = (height-(4*mininumLineItemSpacing))/3.0
        let cellWidth = (screenWidth-(4*mininumInterItemSpacing))/3.0
        return CGSize(width: cellWidth-1, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

}
