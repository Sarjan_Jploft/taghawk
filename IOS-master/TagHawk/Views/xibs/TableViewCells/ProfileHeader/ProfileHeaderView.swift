//
//  ProfileHeaderView.swift
//  TagHawk
//
//  Created by Vikash on 07/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

enum SocialMedia: Int {
    case facebook = 0
    case email = 1
    case phoneNo = 2
    case document = 3
    case address = 4
}

enum ProfileItem: Int {
    case selling = 0
    case sold = 1
    case saving = 2
}

protocol ProfileHeaderDelegate: class {
    func clickShareButton()
    func clickEditProfileButton()
    func clickSocialMedia(socialMediaType: Int)
    func clickProfileItemDelegate(item: Int)
    func clickverifyButton(btn: Button)
}

class ProfileHeaderView: UITableViewHeaderFooterView {

    //MARK: - IBOutlets
    @IBOutlet var profileItemButtons: [UIButton]!
    @IBOutlet weak var profileView: DACircularProgressView!
    @IBOutlet weak var verifiedButton: UIButton!
    @IBOutlet weak var memberNameLabel: UILabel!
    @IBOutlet weak var memberNameJoiningDateLabel: UILabel!
    @IBOutlet var socialMediaButtons: [UIButton]!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var numberOfFollowersLabel: UILabel!
    @IBOutlet weak var numberOfFollowingLabel: UILabel!
    @IBOutlet weak var isVerfiedLabel: UILabel!
    @IBOutlet weak var userRating: UIButton!
    @IBOutlet weak var underlinedView: UIView!
    @IBOutlet var socialInfoImage: [UIImageView]!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var followingButton: UIButton!
    
    
    //MARK: - Variables
    weak var profileDelegate: ProfileHeaderDelegate?
    
    override func awakeFromNib() {
        self.underlinedView.frame.origin.x = self.profileItemButtons[0].frame.midX - 10
        profileImageView.cornerRadius = profileImageView.frame.height / 2
        profileView.thicknessRatio = 0.05
        profileView.clockwiseProgress = -1
    }
    
    func populateData(userData: UserProfile) {
        self.profileImageView.setImage_kf(imageString: userData.profilePicture, placeHolderImage: UIImage(named: "icTabProfileUnactive")!)
        if userData.profileCompleted == 100 {
             self.verifiedButton.setImage(UIImage(named: "icVerifiedSheild"), for: .normal)
        } else {
             self.verifiedButton.setImage(UIImage(named: "icUnverifySheild"), for: .normal)
        }
        self.memberNameLabel.text = userData.fullName
        self.memberNameJoiningDateLabel.text = "Member Since:"
        self.numberOfFollowersLabel.text = "\(userData.followers)"
        self.numberOfFollowingLabel.text = "\(userData.following)"
        self.isVerfiedLabel.text = userData.profileCompleted == 1 ? "Verified Seller" : "\(userData.profileCompleted)" + "% complete"
        self.userRating.setTitle("\(userData.rating)")
        self.socialInfoImage[0].isHidden = userData.isFacebookLogin ? true : false
        self.socialInfoImage[1].isHidden = userData.isEmailVerified ? true : false
        self.socialInfoImage[2].isHidden = userData.isPhoneVerified ? true : false
        self.socialInfoImage[3].isHidden = userData.officialIdVerified ? true : false
        
        let date = Date(milliseconds: Int(userData.created)).toString(dateFormat: Date.DateFormat.MMMMyyyy.rawValue)
        
        self.memberNameJoiningDateLabel.text = "Member Since: " + date
        
        let profileCompletePercent = CGFloat(userData.profileCompleted)/100
        self.profileView.setProgress(profileCompletePercent, animated: true)
        self.profileView.progressTintColor = AppColors.appBlueColor
        self.profileView.trackTintColor = AppColors.grayColor
        self.profileView.tintColor = AppColors.grayColor
    }

    func getMonth(month: Int) -> String {
        
        switch month {
        case 0:
            return "January"
        case 1:
            return "Feburary"
        case 2:
            return "March"
        case 3:
            return "April"
        case 4:
            return "May"
        case 5:
            return "June"
        case 6:
            return "July"
        case 7:
            return "August"
        case 8:
            return "September"
        case 9:
            return "October"
        case 10:
            return "November"
        default:
            return "December"
        }
    }
    
    //MARK: - IBActions
    @IBAction func clickProfileItemButtonAction(_ sender: UIButton) {
        if sender.tag == 0 {
            UIView.animate(withDuration: 0.4) {
                self.underlinedView.frame.origin.x = self.profileItemButtons[0].frame.midX - 10
            }
        } else if sender.tag == 1 {
            UIView.animate(withDuration: 0.4) {
                self.underlinedView.frame.origin.x = self.profileItemButtons[1].frame.midX - 10
            }
        } else {
            UIView.animate(withDuration: 0.4) {
                self.underlinedView.frame.origin.x = self.profileItemButtons[2].frame.midX - 10
            }
        }
        self.profileDelegate?.clickProfileItemDelegate(item: sender.tag)
    }
    
    @IBAction func clickShareButton(_ sender: UIButton) {
        self.profileDelegate?.clickShareButton()
    }
    
    @IBAction func clickEditButton(_ sender: UIButton) {
        self.profileDelegate?.clickEditProfileButton()
    }
    
    @IBAction func clickSocialMediaIcons(_ sender: UIButton) {
       
        self.profileDelegate?.clickSocialMedia(socialMediaType: sender.tag)
    }
    
    @IBAction func clickPopoverVerifyButton(_ sender: UIButton) {
        self.profileDelegate?.clickverifyButton(btn: self.verifiedButton)
    }
}
