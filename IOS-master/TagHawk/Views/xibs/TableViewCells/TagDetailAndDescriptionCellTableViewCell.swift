//
//  TagDetailAndDescriptionCellTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 18/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class TagDetailAndDescriptionCellTableViewCell: UITableViewCell {

    @IBOutlet weak var tagTitleLabel: UILabel!
    @IBOutlet weak var founderNameLabel: UILabel!
    @IBOutlet weak var founderHeadingLabel: UILabel!
    @IBOutlet weak var tagDescriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.tagTitleLabel.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(15), textColor: UIColor.black)
        self.founderNameLabel.setAttributes(font: AppFonts.Galano_Semi_Bold.withSize(13), textColor: UIColor.black)
        self.founderHeadingLabel.setAttributes(text: LocalizedString.Tag_Owner.localized + ":", font: AppFonts.Galano_Regular.withSize(13), textColor: UIColor.black)
        self.tagDescriptionLabel.setAttributes(font: AppFonts.Galano_Regular.withSize(12), textColor: UIColor.black)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateData(tag : Tag){
        tagTitleLabel.isHidden = true
        self.tagTitleLabel.text = tag.name
        self.founderNameLabel.text = tag.adminName
        self.tagDescriptionLabel.text = tag.description
    }
    
}
