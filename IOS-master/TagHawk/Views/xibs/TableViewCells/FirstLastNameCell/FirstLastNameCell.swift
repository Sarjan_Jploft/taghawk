//
//  FirstLastNameCell.swift
//  TagHawk
//
//  Created by Appinventiv on 04/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class FirstLastNameCell: UITableViewCell {

    @IBOutlet weak var firstNameTextField: CustomTextField!
    @IBOutlet weak var lastNameTextField: CustomTextField!
    
    weak var delegate : CustomFieldDelagate?
    
    var textFieldState : TextFieldState = .none
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func initialSetup() {
        firstNameTextField.placeholder = LocalizedString.firstName.localized + "*"
        lastNameTextField.placeholder = LocalizedString.lastName.localized + "*"
        firstNameTextField.textFieldType = .firstName
        lastNameTextField.textFieldType = .lastName
        firstNameTextField.textColor = UIColor.black
        firstNameTextField.textColor = UIColor.black
        firstNameTextField.error = .none
        lastNameTextField.error = .none
    }
    
    func didShowError(state : TextFieldState, textField: CustomTextField){
        self.textFieldState = state
        switch state {
        case .invalid:
            if textField == firstNameTextField {
                self.firstNameTextField.error = LocalizedString.PleaseEnterFirstName.localized
            } else {
                self.lastNameTextField.error = state.associatedValue
                self.lastNameTextField.error = LocalizedString.PleaseEnterLastName.localized
            }
        default:
            break
        }
    }
}
