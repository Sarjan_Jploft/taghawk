//
//  DistanceCell.swift
//  TagHawk
//
//  Created by Appinventiv on 05/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class DistanceCell : UITableViewCell {

    enum DistanceSleectionFor {
        case tagFilter
        case itemFilter
        case shelfFilter
    }
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var slider: StepSlider!
    
    var distanceSelectionFor = DistanceSleectionFor.itemFilter
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func sliderValueChanged(_ sender: StepSlider) {
        
        switch distanceSelectionFor {
        case .itemFilter:
            SelectedFilters.shared.selectedFiltersOnItems.distance = Int(sender.index)

        case .tagFilter:
            SelectedFilters.shared.selectedFilterOnTag.distance = Int(sender.index)

        default:
            SelectedFilters.shared.selectedFilterOnShelf.distance = Int(sender.index)
        }
        

    }
    
    func setUpView(){
        self.distanceLabel.setAttributes(text: LocalizedString.Distance.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        self.setUpSlider()
    }
    
    func setUpSlider(){
        self.slider.backgroundColor = UIColor.white
       
        self.slider.setTrackCircleImage(AppImages.sliderBlueCircle.image , for: UIControl.State.selected)
        self.slider.setTrackCircleImage(AppImages.sliderGrayCircle.image, for: UIControl.State.normal)
        self.slider.tintColor = AppColors.appBlueColor
        self.slider.labels = [SelectedFilters.DistanceValues.oneMile.rawValue, SelectedFilters.DistanceValues.fiveMile.rawValue, SelectedFilters.DistanceValues.tenMile.rawValue, SelectedFilters.DistanceValues.thirtyMile.rawValue, SelectedFilters.DistanceValues.fiftyMile.rawValue, SelectedFilters.DistanceValues.max.rawValue]
        self.slider.labelColor = UIColor.black
        self.slider.labelFont = AppFonts.Galano_Regular.withSize(11)
//        self.slider.sliderCircleImage = AppImages.filledHeart.image
        self.slider.trackColor = AppColors.lightGreyTextColor.withAlphaComponent(0.5)
        self.slider.sliderCircleColor = AppColors.appBlueColor
        self.slider.trackCircleRadius = 8
        self.slider.sliderCircleRadius = 10
        self.slider.trackHeight = 2
        self.slider.labelOffset = 10
        
    }
    
    func setSelectedIndexInSlider(){
        
        switch distanceSelectionFor {
        case .itemFilter:
            self.slider.setIndex(UInt(SelectedFilters.shared.selectedFiltersOnItems.distance), animated: false)
       
        case .tagFilter:
            self.slider.setIndex(UInt(SelectedFilters.shared.selectedFilterOnTag.distance), animated: false)

        default:
            self.slider.setIndex(UInt(SelectedFilters.shared.selectedFilterOnShelf.distance), animated: false)

        }
        
    }
}
