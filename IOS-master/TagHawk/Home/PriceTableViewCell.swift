//
//  PriceTableViewCell.swift
//  TagHawk
//
//  Created by Appinventiv on 08/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class PriceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceTextField: CustomTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        priceTextField.placeholder = LocalizedString.Enter_Price.localized
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
