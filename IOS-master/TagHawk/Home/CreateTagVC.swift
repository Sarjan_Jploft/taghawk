//
//  AddProductVCV.swift
//  TagHawk
//
//  Created by Appinventiv on 08/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CreateTagVC : BaseVC {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var oneLabel: UILabel!
    @IBOutlet weak var twoLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func leftBarButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}


extension CreateTagVC {
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        
    }
    
}
