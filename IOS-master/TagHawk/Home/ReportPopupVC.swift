//
//  ReportPopupVC.swift
//  TagHawk
//
//  Created by Rishabh Nautiyal on 12/03/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol ReportPopupDelegate: AnyObject {
    func leftBtnTapped()
    func rightBtnTapped(optionSelected: String)
}

class ReportPopupVC: UIViewController {
    
    //MARK:- PROPERTIES
    //=================
    
    var delegate: ReportPopupDelegate?
    
    private var optionButtons: [UIButton] = []
    private var optionLabels: [UILabel] = []
    private var selectedOption = ""
    
    //MARK:- IBOUTLETS
    //================
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var optionsStackView: UIStackView!
    @IBOutlet weak var firstOptionBtn: UIButton!
    @IBOutlet weak var firstOptionLbl: UILabel!
    @IBOutlet weak var secondOptionBtn: UIButton!
    @IBOutlet weak var secondOptionLbl: UILabel!
    @IBOutlet weak var thirdOptionBtn: UIButton!
    @IBOutlet weak var thirdOptionLbl: UILabel!
    @IBOutlet weak var fourthOptionBtn: UIButton!
    @IBOutlet weak var fourthOptionLbl: UILabel!
    @IBOutlet weak var fifthOptionBtn: UIButton!
    @IBOutlet weak var fifthOptionLbl: UILabel!
    @IBOutlet weak var sixthOptionBtn: UIButton!
    @IBOutlet weak var sixthOptionLbl: UILabel!
    @IBOutlet weak var seventhOptionBtn: UIButton!
    @IBOutlet weak var seventhOptionLbl: UILabel!
    @IBOutlet weak var otherTextField: UITextField!
    @IBOutlet weak var viewWithTextField: UIView!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    //MARK:- TARGET ACTIONS
    //=====================
    
    @IBAction func firstBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        selectOptionBtn(sender)
    }
    @IBAction func secondBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        selectOptionBtn(sender)
    }
    
    @IBAction func thirdBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        selectOptionBtn(sender)
    }
    
    @IBAction func fourthBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        selectOptionBtn(sender)
    }
    @IBAction func fifthBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        selectOptionBtn(sender)
    }
    
    @IBAction func sixthBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        selectOptionBtn(sender)
    }
    
    @IBAction func seventhBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        selectOptionBtn(sender)
    }
    
    @IBAction func leftBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        delegate?.leftBtnTapped()
    }
    
    @IBAction func rightBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        if !viewWithTextField.isHidden {
            selectedOption = otherTextField.text ?? ""
        }
        delegate?.rightBtnTapped(optionSelected: selectedOption)
    }
}

//MARK:- PRIVATE FUNCTIONS
//========================
extension ReportPopupVC {
    
    private func initialSetup(){
        viewWithTextField.isHidden = true
        optionButtons = [firstOptionBtn, secondOptionBtn, thirdOptionBtn, fourthOptionBtn, fifthOptionBtn, sixthOptionBtn, seventhOptionBtn]
        optionLabels = [firstOptionLbl, secondOptionLbl, thirdOptionLbl, fourthOptionLbl, fifthOptionLbl, sixthOptionLbl, seventhOptionLbl]
        mainView.cornerRadius = 3
        otherTextField.delegate = self
    }
    
    private func setFont() {
        
        titleLbl.font = AppFonts.Galano_Bold.withSize(15)
        
        descriptionLbl.font = AppFonts.Galano_Medium.withSize(14)
        
        optionLabels.forEach { (lbl) in
            lbl.font = AppFonts.Galano_Regular.withSize(14)
        }
    }
    
    private func selectOptionBtn(_ sender: UIButton) {
        view.endEditing(true)
        var count = 0
        optionButtons.forEach { (btn) in
            btn.isSelected = btn == sender ? true : false
//            if sender.isSelected {
            if btn.isSelected {
                if let optionText = optionLabels[count].text {
                    selectedOption = optionText }
                viewWithTextField.isHidden = sender == seventhOptionBtn ? false : true
            }
            count += 1
        }
    }
}

extension ReportPopupVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        return true
    }
    
}
