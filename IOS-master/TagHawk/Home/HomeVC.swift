//
//  HomeVC.swift
//  TagHawk
//
//  Created by Appinventiv on 28/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown
import AMScrollingNavbar
import Crashlytics

protocol ScrollingDelegate : class {
    func scrollViewScroll(_ scrollView: UIScrollView)
}

class HomeVC : BaseVC {
    
    enum CurrentlySelectedHomeTab {
        case items
        case tags
    }
    
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var NSlayoutCollectionCatTop: NSLayoutConstraint!
    @IBOutlet weak var NSLayoutSegmentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var NSLayoutOptionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewItem: UICollectionView!
    
    @IBOutlet weak var optionsViewTop: NSLayoutConstraint!
    @IBOutlet weak var navigationViewTop: NSLayoutConstraint!
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var giftsButton: UIButton!
    @IBOutlet weak var itemsButton: UIButton!
    @IBOutlet weak var segmentOuterView: UIView!
    @IBOutlet weak var tagsButton: UIButton!
    @IBOutlet weak var segmentInnerView: UIView!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var textFieldBackView: UIView!
    @IBOutlet weak var searchLabel: UILabel!
//    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var currentTabSelectedView: UIView!
//    @IBOutlet weak var tagSelectedView: UIView!
    @IBOutlet weak var currentTabSelectedViewLeading: NSLayoutConstraint!
    @IBOutlet weak var homeScrollView: UIScrollView! {
        didSet {
            self.homeScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    @IBOutlet weak var sortingButton: UIButton!
    @IBOutlet weak var sortingTypeLabel: UILabel!
    @IBOutlet weak var sortingArrowImageView: UIImageView!
    @IBOutlet weak var categoryButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var searchBarRightButton: UIButton!
    @IBOutlet weak var searchBarRightImageView: UIImageView!
    @IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var btnHoriVerShow: UIButton!
    @IBOutlet weak var btnAddTag: UIButton!
    @IBOutlet weak var btnListMapView: UIButton!
    
    
    
    
    //MARK:- Variables
    var isItemSelected = true
    private var rateProductModel = SellerProductModel()
    private let dropDown = DropDown()
    private let controler = HomeControler()
    var homeItemsVc : HomeItemsVC!
    var homeTagsVc : HomeTagVC!
    var datacat:[Category]!
    var SortedDataCat=[Category]()
    private var selectedTab = CurrentlySelectedHomeTab.items
    private var lastSearchText = ""
    private var lastCategorySelected = ""
    private var lastContentOffset: CGFloat = 0
    private var yLastContentOffset : CGFloat = 0
    private var optionsViewTopLast: CGFloat = 0
    private let maxConstant: CGFloat = 64
    private var shouldShowPopUp = true
    private var productCategoryId = ""
    
    
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
        controler.updateDeviceToken()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        printDebug("HomeVC...viewWillAppear")
        self.configureNavigation()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.getCommonData()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.async {
            if !self.homeTagsVc.isNavigatedToTagDetail{
                self.homeTagsVc.setCameraPosition(lat: SelectedFilters.shared.appliedFiltersOnTag.lat, long: SelectedFilters.shared.appliedFiltersOnTag.long, withAnimation: false, withZoomLevel: self.homeTagsVc.mapView.camera.zoom)
            }
            self.homeTagsVc.isNavigatedToTagDetail = false
        }
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.customerDelegate = self
        self.controler.rateProductDelegate = self
        self.controler.currentVersionDelegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //        self.segmentOuterView.round()
        
    }
    
    //MARK:- IBActions
    
    //MARK:- Gift button tapped
    @IBAction func giftButtonTapped(_ sender: UIButton) {
        let vc = MyRewardsVC.instantiate(fromAppStoryboard: AppStoryboard.Gift)
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    //MARK:- Search button tapped
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = SearchVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.searchFrom = self.selectedTab == .items ? .homeItems : .homeTags
        vc.searchTagsDelegate = self
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: false)
    }
    
    //MARK:p- Item button tapped
    @IBAction func itemsButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.isItemSelected = true
        self.selectDeselectTbls()
        self.homeScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    //MARK:- Tag button tapped
    @IBAction func tagsButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.isItemSelected = false
        self.selectDeselectTbls()
        self.homeScrollView.setContentOffset(CGPoint(x: screenWidth, y: 0), animated: true)
    }
    
    //MARK:- More button tapped
    @objc func moreButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
//        self.searchLabel.text = LocalizedString.SearchAll.localized
//        self.lastCategorySelected = self.searchLabel.text!
        let vc = CategoryVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    //MARK:- All button tapped
    @objc func allButtonTapped(_ sender: UIButton) {
        
//        Crashlytics.sharedInstance().crash()
        allButtonTapped()
    }
    
    //Func All Clicked
    func allButtonTapped(){
        self.searchLabel.text = LocalizedString.SearchAll.localized
        self.lastCategorySelected = self.searchLabel.text!
        self.productCategoryId = ""
        homeItemsVc.selectedCategoryId = ""
        methodForGetProduct()
    }
    
    //MARK:- Horizontal vertical Show button tapped
    @IBAction func hoziVerShowButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected{
            homeItemsVc.productView = HomeItemsVC.productViewType.listView
        }else{
            homeItemsVc.productView = HomeItemsVC.productViewType.gridView
        }
//        homeItemsVc.products.removeAll()
        homeItemsVc.homeCollectionView.reloadData()
    }
    
    
    //MARK:- Map/List Button Tapped
    @IBAction func btnMapListTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected{
            homeTagsVc.tblView.isHidden = true
        }else{
            homeTagsVc.tblView.isHidden = false
        }
       
        homeTagsVc.tblView.reloadData()
    }
    
    //MARK:- Add Tag Button Tapped
    @IBAction func btnAddTagTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if UserProfile.main.loginType == .none {
            AppNavigator.shared.tabBar?.showLoginSignupPopup()
            return
        }
        
        let appVC = AddTagViewController.instantiate(fromAppStoryboard: .AddTag)
        
        appVC.tagCreateSuccess = {[weak self] tag in
            self?.homeTagsVc.tags.append(tag)
            self?.homeTagsVc.mapView.clear()
            self?.homeTagsVc.clusterManager.clearItems()
            self?.homeTagsVc.generateClusterItems()
        }
        AppNavigator.shared.parentNavigationControler.pushViewController(appVC, animated: true)
        
    }
    
    //MARK:- Cart button tapped
    @IBAction func cartButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if UserProfile.main.loginType == .none {
            AppNavigator.shared.tabBar?.showLoginSignupPopup()
            return
        }
        let vc = CartVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    //MARK:- Filter button tapped
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if self.selectedTab == .items{
            let vc = FilterVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.delegate = self
            AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
        }else{
            let vc = TagFilterVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
            vc.delegate = self
            AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- Sorting button tapped
    @IBAction func sortingButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
        self.dropDown.dismissMode = .automatic
        
        let dataSource = self.selectedTab == .items ? [LocalizedString.Newest.localized, LocalizedString.Closest.localized, LocalizedString.Price_High_To_Low.localized, LocalizedString.Price_Low_To_High.localized] : [SelectedFilters.MemberFilterStringValues.allMembers.rawValue, SelectedFilters.MemberFilterStringValues.above10Members.rawValue, SelectedFilters.MemberFilterStringValues.above50Members.rawValue, SelectedFilters.MemberFilterStringValues.above100Members.rawValue, SelectedFilters.MemberFilterStringValues.above500Members.rawValue, SelectedFilters.MemberFilterStringValues.above1000Members.rawValue]
        self.dropDown.dataSource = dataSource
        self.dropDown.show()
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let weakSelf = self else { return }
            weakSelf.selectedTab == .items ? weakSelf.handleItemsSorting(index : index) : weakSelf.handleMembersSelection(index : index)
        }
    }
    
    //MARK:- Search bar right button tapped
    @IBAction func searchBarRightButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        if self.selectedTab == .items { return }
        //change comment
        searchBarRightImageView.image = AppImages.whiteimg.image
        self.searchLabel.text = LocalizedString.SearchForTags.localized
        self.homeTagsVc.mapView.clear()
        self.homeTagsVc.tags.removeAll()
        self.lastSearchText = ""
        self.homeTagsVc.controler.getTagsData()
    }
    
    func getCommonData(){
        self.controler.getCommonData()
    }
    
    //MARK:- Members selection
    func handleMembersSelection(index : Int){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        switch index {
        case 0:
            SelectedFilters.shared.appliedFiltersOnTag.members = .allMembers
            
        case 1:
            SelectedFilters.shared.appliedFiltersOnTag.members = .above10Members
            
        case 2:
            SelectedFilters.shared.appliedFiltersOnTag.members = .above50Members
            
        case 3:
            SelectedFilters.shared.appliedFiltersOnTag.members = .above100Members
            
        case 4:
            SelectedFilters.shared.appliedFiltersOnTag.members = .above500Members
            
        case 5:
            SelectedFilters.shared.appliedFiltersOnTag.members = .above1000Members
            
        default:
            SelectedFilters.shared.appliedFiltersOnTag.members = .allMembers
            
        }
        
        self.setSortingTabForTag()
        self.homeTagsVc.mapView.clear()
        self.homeTagsVc.tags.removeAll()
        self.homeTagsVc.controler.getTagsData()
    }
    
    //MARK:- Handle item selections
    func handleItemsSorting(index : Int){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        switch index {
            
        case 0:
            SelectedFilters.shared.appliedSortingOnProducts = .newest
            
        case 1:
            SelectedFilters.shared.appliedSortingOnProducts = .closest
            
        case 2:
            SelectedFilters.shared.appliedSortingOnProducts = .priceHightToLow
            
        case 3:
            SelectedFilters.shared.appliedSortingOnProducts = .priceLowToHigh
            
        default:
            SelectedFilters.shared.appliedSortingOnProducts = .newest
            
        }
        self.setSortingLabelTextForProducts()
        methodForGetProduct()
    }
    
    
    //MARK:- Get Product Function
    func methodForGetProduct(){
        //changed
        self.homeItemsVc.products.removeAll()
        self.homeItemsVc.homeCollectionView.reloadData()
        self.homeItemsVc.resetPage()
//        self.homeItemsVc.controler.getProducts(page: self.page, currentProducts: self.homeItemsVc.products)
        self.homeItemsVc.controler.getProducts(page: self.page, currentProducts: self.homeItemsVc.products, categoryId: productCategoryId)
    }
    
    
    
    //MARK:- Set sorting label text
    func setSortingLabelTextForProducts() {
        if self.selectedTab != .items { return }
        self.sortingTypeLabel.text = SelectedFilters.shared.setSortingAppliedText(sorting: SelectedFilters.shared.appliedSortingOnProducts)
    }
    
    func setSortingTabForTag(){
        self.sortingTypeLabel.text = SelectedFilters.shared.getMemberFilterStringValue(from: SelectedFilters.shared.appliedFiltersOnTag.members).rawValue
    }
}

//MARK:- Private functions
private extension HomeVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.segmentOuterView.round()
        self.segmentOuterView.backgroundColor = AppColors.selectedSegmentColor
        self.shadowView.addShadow()
        self.shadowView.round()
        self.shadowView.clipsToBounds = false
        self.itemsButton.round()
        self.tagsButton.round()
        self.textFieldBackView.round(radius: 1.0)
        self.itemsButton.setAttributes(title: LocalizedString.Items.localized, font: AppFonts.Galano_Medium.withSize(14), titleColor: UIColor.white)
        self.tagsButton.setAttributes(title: LocalizedString.Tags.localized, font: AppFonts.Galano_Medium.withSize(14), titleColor: UIColor.white)
        self.searchLabel.setAttributes(text: LocalizedString.SearchAll.localized, font: AppFonts.Galano_Medium.withSize(14) , textColor: AppColors.lightGreyTextColor)
//        self.currentTabSelectedView.backgroundColor = AppColors.selectedSegmentColor
        self.currentTabSelectedView.backgroundColor = AppColors.whiteColor
        self.currentTabSelectedView.round()
        var scrollHeight: CGFloat {
            var height: CGFloat = 0
            homeScrollView.subviews.forEach { (subView) in
                height += subView.frame.size.height
            }
            return height
        }
        self.homeScrollView.contentSize = CGSize(width: screenWidth * 2, height: scrollHeight)
        self.itemsButton.backgroundColor = UIColor.clear
        self.tagsButton.backgroundColor = UIColor.clear
        self.homeScrollView.bounces = false
        self.homeScrollView.delegate = self
        self.homeScrollView.isPagingEnabled = true
        self.homeScrollView.showsHorizontalScrollIndicator = false
        self.sortingTypeLabel.setAttributes(text: "All", font: AppFonts.Galano_Medium.withSize(13), textColor: UIColor.black)
        self.addItemsVc()
        self.addTagsVc()
        self.getLocation()
        self.selectDeselectTbls()
        self.controler.createCustomer()
        
        if let layout = collectionViewItem.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
        
        NotificationCenter.default.addObserver(self, selector:#selector(methodToRecieveNotification(notification:)) , name: Notification.Name("CategoryRecieved"), object: nil)
        
       
    }
    
    //MARK:- Get Sorted Category
    @objc func methodToRecieveNotification(notification:Notification){
        datacat = AppSharedData.shared.categories
        
//        if datacat.contains(where: { (<#Category#>) -> Bool in
//            <#code#>
//        })
        
        let checkFurniture = datacat.filter { $0.categoryId == "5c7f98bf08b03630ce2d07ea" }
        if !checkFurniture.isEmpty{
            SortedDataCat.append(contentsOf: checkFurniture)
        }
        let checkHousehold = datacat.filter { $0.categoryId == "5c498e0fe41f583b9a4cc1f4" }
        if !checkHousehold.isEmpty{
            SortedDataCat.append(contentsOf: checkHousehold)
        }
        let checkElectronic = datacat.filter { $0.categoryId == "5c498d36e41f583b9a4cc1ef" }
        if !checkElectronic.isEmpty{
            SortedDataCat.append(contentsOf: checkElectronic)
        }
        let checkClothing = datacat.filter { $0.categoryId == "5c498be0e41f583b9a4cc1e8" }
        if !checkClothing.isEmpty{
            SortedDataCat.append(contentsOf: checkClothing)
        }
        let checkFootwear = datacat.filter { $0.categoryId == "5c498de3e41f583b9a4cc1f3" }
        if !checkFootwear.isEmpty{
            SortedDataCat.append(contentsOf: checkFootwear)
        }
        let checkBeauty = datacat.filter { $0.categoryId == "5c498afee41f583b9a4cc1e5" }
        if !checkBeauty.isEmpty{
            SortedDataCat.append(contentsOf: checkBeauty)
        }
        let checkAccessories = datacat.filter { $0.categoryId == "5e0b3b8c43f55b6bae0a6dab" }
        if !checkAccessories.isEmpty{
            SortedDataCat.append(contentsOf: checkAccessories)
        }
        
        DispatchQueue.main.async {
            self.collectionViewItem.reloadData()
        }
        
    }
    
    
    //MARK:- Get Location
    func getLocation(){
        LocationManager.shared.startUpdatingLocation {[weak self] (loc, error) in
            guard let location = loc, let weakSelf = self else { return }
            weakSelf.homeTagsVc.currentLocation = location
            weakSelf.homeItemsVc.currentLocation = location
            LocationManager.shared.stopLocationManger()
        }
    }
    
    //MARK:- Select deselect tab
    func selectDeselectTbls(){
        self.isItemSelected ? self.selectItemTab() : self.selectTagTab()
    }
    
    //MARK:- Select item tab
    func selectItemTab(){
        //self.tagSelectedView.backgroundColor = UIColor.clear
        self.itemsButton.setImage(AppImages.itemsActiv.image, for: UIControl.State.normal)
        self.itemsButton.setTitleColor(UIColor.black)
        self.tagsButton.setImage(AppImages.tagsInactiv.image, for: UIControl.State.normal)
        self.tagsButton.setTitleColor(AppColors.whiteColor)
        self.selectedTab = .items
        //        self.categoryButton.isHidden = false
//        self.currentTabSelectedView.backgroundColor = AppColors.selectedSegmentColor
        self.currentTabSelectedView.backgroundColor = AppColors.whiteColor
        self.setSortingLabelTextForProducts()
        //        self.searchBarRightButton.setImage(AppImages.greySearch.image, for: UIControl.State.normal)
        DispatchQueue.delay(0.1) {
            if self.lastCategorySelected == ""{
                self.searchLabel.text = LocalizedString.SearchAll.localized
            }else{
                self.searchLabel.text = self.lastCategorySelected
            }
            
        }
        
        
        
        //Comment change
        self.searchBarRightImageView.image = AppImages.whiteimg.image
//        self.searchBarRightImageView.isHidden = true
        self.searchBarRightImageView.tintColor = UIColor.clear
        self.homeTagsVc.setCameraPosition(lat: SelectedFilters.shared.appliedFiltersOnTag.lat, long: SelectedFilters.shared.appliedFiltersOnTag.long)
        
        self.btnHoriVerShow.isHidden = false
        self.btnAddTag.isHidden = true
        self.btnListMapView.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.categoryButtonWidth.constant = 30
            self.NSLayoutOptionViewHeight.constant = 194
            self.view.layoutIfNeeded()
        }
        
    }
    
    //MARK:- Select tag tab
    func selectTagTab() {
//        self.tagSelectedView.backgroundColor = AppColors.whiteColor
        self.tagsButton.setImage(AppImages.tagsActiv.image, for: UIControl.State.normal)
        self.tagsButton.setTitleColor(UIColor.black)
        self.itemsButton.setImage(AppImages.itemsInactiv.image, for: UIControl.State.normal)
        self.itemsButton.setTitleColor(AppColors.whiteColor)
        self.selectedTab = .tags
        //        self.categoryButton.isHidden = true
//        self.currentTabSelectedView.backgroundColor = AppColors.segmentTextColor
        self.currentTabSelectedView.backgroundColor = UIColor.white
        
        self.setSortingTabForTag()
        
        if lastSearchText.isEmpty {
            self.searchLabel.text = LocalizedString.SearchForTags.localized
            //Change comment
            self.searchBarRightImageView.image = AppImages.whiteimg.image
           
        }else{
            self.searchLabel.text = lastSearchText
            self.searchBarRightImageView.image = AppImages.cross.image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        }
        
        self.searchBarRightImageView.tintColor = UIColor.black
        self.btnHoriVerShow.isHidden = true
        self.btnAddTag.isHidden = false
        self.btnListMapView.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.categoryButtonWidth.constant = 0
            self.NSlayoutCollectionCatTop.constant = 12
            self.NSLayoutOptionViewHeight.constant = 104
            self.optionsViewTop.constant = 6
            self.view.layoutIfNeeded()
        }
        
    }
    
    //add followers vc
    //====================
    func addItemsVc(){
        self.homeItemsVc = HomeItemsVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        self.homeItemsVc.delegate = self
        self.homeScrollView.frame = self.homeItemsVc.view.frame
        self.homeScrollView.addSubview(self.homeItemsVc.view)
        self.homeItemsVc.willMove(toParent: self)
        self.addChild(self.homeItemsVc)
        self.homeItemsVc.view.frame.size.height = self.homeScrollView.frame.height
        self.homeItemsVc.view.frame.origin = CGPoint.zero
    }
    
    //MARK:- add following vc
    //=====================
    func addTagsVc(){
        self.homeTagsVc =  HomeTagVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        self.homeScrollView.frame = self.homeTagsVc.view.frame
        self.homeScrollView.addSubview(self.homeTagsVc.view)
        self.homeTagsVc.willMove(toParent: self)
        self.addChild(self.homeTagsVc)
        self.homeTagsVc.view.frame.size.height = self.homeScrollView.frame.height
        self.homeTagsVc.view.frame.origin = CGPoint(x: screenWidth, y: 0)
    }
    
    //MARK:- Tating alert
    private func showRateAlert() {
        let vc = RateSellerAlertVC.instantiate(fromAppStoryboard: AppStoryboard.PaymentAndShipping)
        vc.headingText = LocalizedString.RateSeller.localized
        vc.descriptionText = "\(LocalizedString.wouldYouLikeToRate.localized) \(rateProductModel.fullName) \(LocalizedString.forProduct.localized) \(rateProductModel.title)".attributeBoldStringWithAnotherColor(stringsToColor: [rateProductModel.fullName, rateProductModel.title])
        vc.modalPresentationStyle = .overCurrentContext
        
        vc.closeBtnTap = { [weak self] in
            self?.showRateLaterPopup()
        }
        
        vc.rateBtnTap = { [weak self] in
            self?.dismiss(animated: true, completion: {
                self?.showRateOptionsPopup()
            })
        }
        
        AppNavigator.shared.parentNavigationControler.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Rating options popup
    private func showRateOptionsPopup() {
        let rateScene = RateOptionsVC.instantiate(fromAppStoryboard: .PaymentAndShipping)
        rateScene.rateModel = rateProductModel
        rateScene.modalPresentationStyle = .overCurrentContext
        AppNavigator.shared.parentNavigationControler.present(rateScene, animated: true, completion: nil)
    }
    
    private func showRateLaterPopup() {
        showPopup(type: .rateLater)
    }
    
    //MARK:- Show popup
    private func showPopup(type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none, msg : String = ""){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        vc.delegate = self
        vc.msg = msg
        vc.modalPresentationStyle = .overCurrentContext
        AppNavigator.shared.parentNavigationControler.present(vc, animated: true, completion: nil)
    }
}

//MARK:- Navigation related activities
private extension HomeVC {
    func configureNavigation(){
        AppNavigator.shared.parentNavigationControler.setNavigationBarHidden(true, animated: false)
    }
    
}


extension HomeVC : SelectedCategory  {
    
    func selectCategory(category: Category) {
        
    }
    
    func selectCategory(selectedCategoryId : String, categoryName : String) {
        
    }
}

//MARK: scrolview delegate
extension HomeVC  {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging")
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //Temp Comment
        
        if scrollView == homeScrollView {
            if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
                scrollView.contentOffset.y = 0
            }
        }
       
        if !(scrollView is UICollectionView) {
            
            self.currentTabSelectedViewLeading.constant = (scrollView.contentOffset.x / 2) * self.segmentInnerView.width / screenWidth
            
        }
        
        if (scrollView == collectionViewItem){
            return
        }else{
            if scrollView.contentOffset.x == 0.0{
                self.selectItemTab()
            }else if scrollView.contentOffset.x == screenWidth {
                self.selectTagTab()
            }
        }
        
        
    }
    
    //MARKK:- Show tabbar
    func showTabBar(animated:Bool) {
        if let tabBarCon = tabBarController as? TagHawkTabBarControler {
            tabBarCon.showTabBar(animated: animated)
        }
    }
      
    //hide tabBar with animation
    func hideTabBar(animated:Bool) {
        if let tabBarCon = tabBarController as? TagHawkTabBarControler {
            tabBarCon.hideTabBar(animated: animated)
        }
    }
    
    func scrollUpCategoryView(_ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.38, animations: { () -> Void in
                self.optionsViewTop.constant = -45
//                self.NSLayoutSegmentHeight.constant = 0
//                self.segmentOuterView.isHidden = true
//                self.shadowView.isHidden = true
                self.NSlayoutCollectionCatTop.constant = -44
                self.NSLayoutOptionViewHeight.constant = 144//154
                self.view.layoutIfNeeded()
            }, completion: {(success) in
            })
        } else {
            self.optionsViewTop.constant = -45
            self.NSlayoutCollectionCatTop.constant = -44
            self.NSLayoutOptionViewHeight.constant = 144//154
//            self.NSLayoutSegmentHeight.constant = 0
//            self.segmentOuterView.isHidden = true
//            self.shadowView.isHidden = true
            self.view.layoutIfNeeded()
        }
    }
    
    func scrollDownCategoryView() {
        UIView.animate(withDuration: 0.76, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            self.optionsViewTop.constant = 6
            self.NSLayoutOptionViewHeight.constant = 194
            self.NSlayoutCollectionCatTop.constant = 12
//            self.NSLayoutSegmentHeight.constant = 44
//            self.segmentOuterView.isHidden = false
//            self.shadowView.isHidden = false
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

//MARK:- Filter delegates
extension HomeVC : ApplyFilter {
    
    func applyFilter(){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if self.selectedTab == .items{
            self.homeItemsVc.resetPage()
            self.homeItemsVc.products.removeAll()
            self.homeItemsVc.homeCollectionView.reloadData()
//            self.homeItemsVc.controler.getProducts(page: self.page, currentProducts: self.homeItemsVc.products)
            self.homeItemsVc.controler.getProducts(page: self.page, currentProducts: self.homeItemsVc.products, categoryId: productCategoryId)
        }else{
            self.homeTagsVc.mapView.clear()
            self.homeTagsVc.tags.removeAll()
            self.homeTagsVc.controler.getTagsData()
            self.refreshHome()
            self.refreshTagShelf()
        }
    }
}

//MAR:- Search Tags Delehayes
extension HomeVC : GetSearchedTags {
    
    //MARK:- get searched title
    func getSearchedTitle(title: String) {
        self.lastSearchText = title
        self.searchLabel.text = title
        self.searchBarRightImageView.image = AppImages.cross.image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    }
    
    func getSearchedTags(tags: [Tag]) {
        self.homeTagsVc.mapView.clear()
        self.homeTagsVc.tags.removeAll()
        self.homeTagsVc.tags = tags
        if let firstTag = tags.first {
            self.homeTagsVc.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: firstTag.tagLatitude, longitude: firstTag.tagLongitude))
        }
        self.homeTagsVc.mapView.clear()
        self.homeTagsVc.clusterManager.clearItems()
        self.homeTagsVc.generateClusterItems()
    }
    
}

extension HomeVC : ScrollingDelegate {
    
    //MARK:- Manage header view
    func manageHeaderView(_ scrollView: UIScrollView) {
        
        if scrollView == homeScrollView {
            if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
                scrollView.contentOffset.y = 0
            }
        }
        
        if scrollView.isAtBottom {
            showTabBar(animated: true)
            
        } else {
            if (self.lastContentOffset > scrollView.contentOffset.y) {
                showTabBar(animated: true)
                scrollDownCategoryView()
            } else {
                if scrollView.contentOffset.y > -10 {
                    hideTabBar(animated: true)
                    scrollUpCategoryView(true)
                } else {
                    showTabBar(animated: false)
                    scrollDownCategoryView()
                }
            }
        }
        
    }
    
    //MARK:- Scroll view didscroll
    func scrollViewScroll(_ scrollView: UIScrollView) {
        //Temp Comment
        manageHeaderView(scrollView)
        
        if (self.lastContentOffset > scrollView.contentOffset.y) {
            // move up
            
        }else{
            
        }
        
       self.lastContentOffset = scrollView.contentOffset.y
    }
    
    //MARK:- Scroll view did end decelerating
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //manageHeaderView(scrollView)
    }
    
    //MARK:- scroll view did end dragging
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //manageHeaderView(scrollView)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
       // manageHeaderView(scrollView)
    }
}

//MARK:- Create customer delegate
extension HomeVC : CreateCustomerDelegate {
    
    func willrequestCreteCustomer() {
        
    }
    
    func customerCreatedSuccessFully(customerId: String) {
        
    }
    
    func failedToCreateCustomer() {
        
    }
}

//MARK:- Rate product
extension HomeVC: GetRateProductData {
    func denyRatingSuccess() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func rateDataSuccess(sellerData: SellerProductModel) {
        if !sellerData._id.isEmpty {
            rateProductModel = sellerData
            showRateAlert()
        }
    }
}

//MARK:- Popup delegate
extension HomeVC: CustomPopUpDelegateWithType {
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        guard let appUrl = URL(string: "https://searchads.apple.com/?cid=G-NB_App%20in%20Store_Exact-NB_App%20in%20Store_App%20in%20store-IN-EN-KW164") else { return }
        UIApplication.shared.open(appUrl, options: [:]) { (success) in
            
        }
    }
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if type == .rateLater {
            controler.denyRating(sellerId: rateProductModel.sellerId, prodId: rateProductModel.prodId)
        }
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        if type == .rateLater {
            self.dismiss(animated: true, completion: nil)
        }else if type == .normalUpdate{
            guard let appUrl = URL(string: "https://searchads.apple.com/?cid=G-NB_App%20in%20Store_Exact-NB_App%20in%20Store_App%20in%20store-IN-EN-KW164") else { return }
            UIApplication.shared.open(appUrl, options: [:]) { (success) in
                
            }
        }
    }
}

//MARK:- Webservices

extension HomeVC : GetCurrentVersionDelegate {
    
    func willGetCurrentVersion(){
        
    }
    
    func getCurrentVersionSuccessfully(version : Int, type : VersionType){
        if version <= appVersion { return }
        let popUpType = type == .force ? CustomPopUpVC.CustomPopUpFor.forceUpdate : CustomPopUpVC.CustomPopUpFor.normalUpdate
        if shouldShowPopUp{
            shouldShowPopUp = type != .normal
            self.showPopup(type: popUpType)
        }
    }
    
    func failedToUpdateVersion(message : String){
        
    }
    
}



//MARK:- CollectionView Delegate and Data Source

extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var noOfItem = 0
        
//        guard let catCount = SortedDataCat else{
//            return 0
//        }
//
//        noOfItem = catCount.count > 7 ? 7 : catCount.count
        
        
        
        noOfItem = SortedDataCat.count > 7 ? 7 : SortedDataCat.count
        
        return noOfItem
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatListHomeCllctionVCell", for: indexPath) as! CatListHomeCllctionVCell
        
        cell.populateData(category: SortedDataCat[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CatListHeadViewCVView", for: indexPath) as! CatListHeadViewCVView
            headerView.btnAll.addTarget(self, action: #selector(allButtonTapped(_:)), for: .touchUpInside)
            return headerView
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CatListFooterViewCVView", for: indexPath) as! CatListFooterViewCVView
            footerView.btnMore.addTarget(self, action: #selector(moreButtonTapped(_:)), for: .touchUpInside)
            return footerView
        default:
            fatalError("Unexpected element kind")
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 86, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: 86, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width:70, height:80.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let catItem = SortedDataCat[indexPath.item]
        productCategoryId = catItem.categoryId
        homeItemsVc.selectedCategoryId = catItem.categoryId
    
        
        if catItem.categoryId == "5c7f98bf08b03630ce2d07ea"{
            self.searchLabel.text = LocalizedString.SearchFurniture.localized
        }else if catItem.categoryId == "5c498e0fe41f583b9a4cc1f4"{
            self.searchLabel.text = LocalizedString.SearchHousehold.localized
        }else if catItem.categoryId == "5c498d36e41f583b9a4cc1ef"{
            self.searchLabel.text = LocalizedString.SearchElectronic.localized
        }else if catItem.categoryId == "5c498be0e41f583b9a4cc1e8"{
            self.searchLabel.text = LocalizedString.SearchClothing.localized
        }else if catItem.categoryId == "5c498de3e41f583b9a4cc1f3"{
            self.searchLabel.text = LocalizedString.SearchFootwear.localized
        }else if catItem.categoryId == "5c498afee41f583b9a4cc1e5"{
            self.searchLabel.text = LocalizedString.SearchBeauty.localized
        }else if catItem.categoryId == "5e0b3b8c43f55b6bae0a6dab"{
            self.searchLabel.text = LocalizedString.SearchAccessories.localized
        }
        
        self.lastCategorySelected = self.searchLabel.text!
        
        methodForGetProduct()
        
    }
    
}
