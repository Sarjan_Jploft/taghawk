//
//  CategoryVC.swift
//  TagHawk
//
//  Created by Appinventiv on 24/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol SelectedCategory : class {
    
    func selectCategory(category : Category)
    
}

class CategoryVC : BaseVC {
    
    enum CategorySelectionFrom{
        case home
        case searchProducts
        case shelf
        case searchShelf
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var resetButton: UIButton!
    
    //MARK:- variables
    private let controler = CategoriesControler()
    private var categories : [Category] = []
    weak var delegate : SelectedCategory?
    var selectionFrom = CategorySelectionFrom.home
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
    }
    
    //MARK:- IBAction
    @IBAction func backButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Reset button tapped
    @IBAction func resetButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.delegate?.selectCategory(category: Category())
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- Private functions
private extension CategoryVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.resetButton.isHidden = true
        configureCollectionView()
        self.controler.getCategories()
    }
    
    //MARK:- configure collection view
    func configureCollectionView(){
        self.categoryCollectionView.registerNib(nibName: CategoryCell.defaultReuseIdentifier)
        self.categoryCollectionView.showsHorizontalScrollIndicator = false
        self.categoryCollectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
        self.categoryCollectionView.delegate = self
        self.categoryCollectionView.dataSource = self
    }
}

//MARK:- Configure Navigation
private extension CategoryVC {
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.titleLabel.setAttributes(text: LocalizedString.Categories.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.white)
    }
    
}

//MARK:- CollectionView Datasource and Delegats
extension CategoryVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 7.5
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 15
        return CGSize(width: finalContentWidth / 3, height: finalContentWidth / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getProductCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.defaultReuseIdentifier, for: indexPath) as? CategoryCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        cell.populateData(category: self.categories[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch self.selectionFrom  {
            
        case .home:
            
            let vc = SearchedProductsVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.selectedCategory = self.categories[indexPath.item]
            vc.searchKeyWord = ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .searchProducts, .searchShelf, .shelf:
            
            self.delegate?.selectCategory(category: self.categories[indexPath.item])
            self.navigationController?.popViewController(animated: true)
            
        }
        
    }
}

//MARK:- Webservice delegates
extension CategoryVC : GetCategoriesDelegate {
    
    func willRequestCategories(){
        self.view.showIndicator()
    }
    
    func categoriesReceivedSuccessFully(categories : [Category]){
        self.view.hideIndicator()
        self.categories = categories
        AppSharedData.shared.categories = categories
        self.categoryCollectionView.reloadData()
    }
    
    func failedToReceiveCategories(){
        self.view.hideIndicator()
    }
}
