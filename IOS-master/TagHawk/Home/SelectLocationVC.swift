

import UIKit


protocol GedAddressBack : class{
    func getAddress(lat : Double, long:Double,addressString:String)
}

class SelectLocationVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var searchTextField: CustomTextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK:- IBOutlets
    var places : JSONDictionaryArray = []{ didSet{}}
    var placeStringArray : [String] = []
    var placeIdArray : [String] = []
    let controler = SelectLocationControler()
    weak var delegate : GedAddressBack?
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
        // Do any additional setup after loading the view.
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
        self.controler.cordinateDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
        self.searchTextField.becomeFirstResponder()
    }
    
    //MARK:- IBActions
    
    //MARK:- Back button tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- Setup subview
extension SelectLocationVC {
    
    func setUpSubView(){
        self.configureTableView()
        self.searchTextField.delegate = self
        self.searchTextField.addTarget(self, action: #selector(textDidChange), for: UIControl.Event.editingChanged)
        self.searchTextField.returnKeyType = .search
        self.searchTextField.placeholder = LocalizedString.Please_Enter_Your_Place.localized
        self.titleLabel.setAttributes(text: LocalizedString.Choose_Location.localized, font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.white)
    }
    
    //mARK:- Configure tableview
    func configureTableView(){
        self.searchTableView.registerNib(nibName: SearchLocationCell.defaultReuseIdentifier)
        self.searchTableView.rowHeight = UITableView.automaticDimension
        self.searchTableView.estimatedRowHeight = 50
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
    }
}


//MARK:- Configure nvigation
private extension SelectLocationVC {
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
}

//MARK:- Table view delegates
extension SelectLocationVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.placeStringArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchLocationCell.defaultReuseIdentifier) as? SearchLocationCell else {
            fatalError("SettingsVC....\(SearchLocationCell.defaultReuseIdentifier) cell not found") }
        
        cell.populateData(place: self.placeStringArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        self.searchTextField.text = self.placeStringArray[indexPath.row]
        self.controler.getCordinates(placeId: self.placeIdArray[indexPath.row])
    }
}

//MARK:- Textfield delegates
extension SelectLocationVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        
        return true
    }
    
    @objc func textDidChange(_ textField : UITextField) {
        guard let txt = textField.text else { return }
        self.controler.getPlaces(txt: txt.replace(string: " ", withString: ""))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

//MARK:- get places delegate
extension SelectLocationVC : GetPlacesDelegate {
    
    func willGetPlaces(){
        
    }
    
    func placesReceivedSuccessFully(placesStringArray : [String], placeIdArray : [String]){
        self.placeIdArray = placeIdArray
        self.placeStringArray = placesStringArray
        self.searchTableView.reloadData()
    }
    
    func failedToReceivePlace(){
        
    }
    
}

//MARKK:- Get cordinate delegates
extension SelectLocationVC : GetCordinatesDelegate {
    
    func willGetCordinates() {
        self.view.showIndicator()
    }
    
    func cordinatesReceivedSuccessFully(lat: Double, long: Double) {
        self.view.hideIndicator()
        self.delegate?.getAddress(lat: lat, long: long, addressString:  self.searchTextField.text ?? "")
        self.dismiss(animated: true, completion: nil)
    }
    
    func failedToReceiveCordinates() {
        self.view.hideIndicator()
    }
    
}
