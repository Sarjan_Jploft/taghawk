//
//  HomeItemsVC.swift
//  TagHawk
//
//  Created by Appinventiv on 11/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

class HomeItemsVC: BaseVC {
    
    
    
    enum productViewType {
        case listView
        case gridView
    }
    
    
    
    //MARK:- Variables
    var isItemSelected = true
    private let dropDown = DropDown()
    let controler = HomeControler()
    private let categoriesControler = CategoriesControler()
    //private let controlerProductDetail = ProductDetailControler()
   // weak var likeStatusBackDeleate : GetLikeStatusBack?
    var products : [Product] = []
   // var product = Product()
    var productView = productViewType.gridView
    var selectedCategoryId : String = ""
    var serviceCalledFirstTime = false
    let refreshControl = UIRefreshControl()
    var currentLocation : CLLocation = CLLocation(){
        didSet {
            if !serviceCalledFirstTime{
                serviceCalledFirstTime = true
                self.controler.getProducts(page: self.page, currentProducts: self.products)
            }
        }
    }
    var isrefreshed = false
    
    weak var delegate : ScrollingDelegate?
    
    //MARK:- IBOutlets
    @IBOutlet weak var homeCollectionView: UICollectionView!
    
    //View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
        self.categoriesControler.delegate = self
        //self.controlerProductDetail.likeDelegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //            self.segmentOuterView.round()
    }
    
    //MARK:- ACTION
    
    //MARK:- Like button tapped
    @objc func likeButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        //        if self.product.isCreatedByMe{
        //            let vc = AddProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        //            vc.product = self.product
        //            vc.commingFor = .edit
        //            //vc.refreshDelegate = self
        //            self.present(vc, animated: true, completion: nil)
        //}else{
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
//        self.product = self.products[sender.tag]
        self.controler.likeUnlikeProduct(product: self.products[sender.tag])
//        self.product.isLiked = !self.product.isLiked
        self.products[sender.tag].isLiked = !self.products[sender.tag].isLiked
        self.setIsLikedStatus(self.products[sender.tag],sender)
        //self.likeStatusBackDeleate?.getLikeStatus(product: self.product)
        //}
        
    }
    
    //MARK:- Set Like Status
    func setIsLikedStatus(_ productItem:Product,_ likeButtonAdd:UIButton){
        if !productItem.isCreatedByMe {
            likeButtonAdd.setImage(productItem.isLiked ? AppImages.filledHeart.image : AppImages.emptyHeart.image, for: UIControl.State.normal)
        }
    }
    
    
    //MARK:- Refresh
    @objc func refresh(){
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.resetPage()
        self.isrefreshed = true
        self.controler.getProducts(page: self.page, currentProducts: self.products, categoryId: self.selectedCategoryId)
    }
}


//MARK:- Private functions
private extension HomeItemsVC {
    
    //MARK:- Setup view
    func setUpSubView(){
        self.configureCollectionView()
        self.setUpSortingDropDown()
        self.categoriesControler.getCategories()
    }
    
    //MARK:- configure collectionview
    func configureCollectionView(){
        //Change Comment
        //        self.homeCollectionView.registerNib(nibName: ProductCollectionViewCell.defaultReuseIdentifier)
        self.homeCollectionView.registerNib(nibName: ProductCollectionViewCellGridView.defaultReuseIdentifier)
        self.homeCollectionView.registerNib(nibName: ProductCollectionViewCellListView.defaultReuseIdentifier)
        self.homeCollectionView.register(UINib(nibName: "HomeHeaserView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeHeaserView.reuseIdentifier)
        self.homeCollectionView.showsHorizontalScrollIndicator = false
        self.homeCollectionView.contentInset = UIEdgeInsets(top: 10, left: 4, bottom: 10, right: 4)
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.homeCollectionView)
        self.homeCollectionView.delegate = self
        self.homeCollectionView.dataSource = self
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.homeCollectionView.addSubview(refreshControl)
        self.homeCollectionView.alwaysBounceVertical = true
        NotificationCenter.default.addObserver(self, selector:#selector(methodLikeUnlikeStatus(notification:)) , name: Notification.Name("LikeUnlikeStatus"), object: nil)
    }
    
    //Method for update like unlike status
    
    @objc func methodLikeUnlikeStatus(notification: Notification){
        refresh()
    }
    
    
    
    func setUpSortingDropDown(){
        self.dropDown.cellNib = UINib(nibName: "SortingDropDownCell", bundle: nil)
        self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let finalCell = cell as? SortingDropDownCell else { return }
            finalCell.dropDownLabel.textColor = UIColor.green
        }
    }
}

//MARK:- CollectionView Datasource and Delegats
extension HomeItemsVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HomeHeaserView", for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //change
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 2
        
        if self.productView == .gridView{
            
            return CGSize(width: finalContentWidth / 2, height: finalContentWidth / 2 + 68)
        }else{
            
            return CGSize(width: finalContentWidth, height: 138.0)//finalContentWidth / 2 - 20)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getProductCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        //Change Comment
        
        //        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ProductCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        
        
        if self.productView == .gridView{
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCellGridView.defaultReuseIdentifier, for: indexPath) as? ProductCollectionViewCellGridView else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
            cell.cellFor = .home
            cell.populateData(product: self.products[indexPath.item])
            cell.btnLike.tag = indexPath.item
            cell.btnLike.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
            return cell
            
        }else{
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCellListView.defaultReuseIdentifier, for: indexPath) as? ProductCollectionViewCellListView else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
            cell.cellFor = .home
            cell.populateData(product: self.products[indexPath.item])
            cell.btnLike.tag = indexPath.item
            cell.btnLike.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
            return cell
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ProductDetailVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.prodId = self.products[indexPath.item].productId
        vc.prodReportedDelegate = self
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if self.products.count >= 14  {
            if indexPath.item == self.products.count - 1 && self.nextPage != 0{
                if !AppNetworking.isConnectedToInternet {
                    CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                    return
                }
                self.controler.getProducts(page: self.page, currentProducts: self.products, categoryId: self.selectedCategoryId)
            }
        }
    }
}

extension HomeItemsVC : ProductReportedDelegate{
    
    func productReportedSuccessFully(prodId : String){
        
        if let index = self.products.lastIndex(where: { $0.productId == prodId}){
            self.products.remove(at: index)
            self.shouldDisplayEmptyDataView = true
            self.homeCollectionView.reloadData()
            self.homeCollectionView.reloadEmptyDataSet()
        }
    }
    
}

extension HomeItemsVC {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.delegate?.scrollViewScroll(scrollView)
    }
    
}

//MARK:- Get products call back
extension HomeItemsVC : GetProductsDelegate{
    
    func willRequestProducts() {
        if self.page == 1{
            self.shouldDisplayEmptyDataView = false
            self.homeCollectionView.reloadEmptyDataSet()
            if !isrefreshed {
                self.view.showIndicator()
            }
        }
    }
    
    func productsReceivedSuccessFully(products: [Product], nextPage: Int) {
        self.view.hideIndicator()
        
        if self.page == 1{
            self.products.removeAll()
            self.homeCollectionView.reloadData()
        }
        
        if nextPage != 0{
            self.page = nextPage
        }
        
        self.products.append(contentsOf: products)
        self.nextPage = nextPage
        self.homeCollectionView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.homeCollectionView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
    
    func failedToReceiveProducts(message: String) {
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
        self.shouldDisplayEmptyDataView = true
        self.homeCollectionView.reloadEmptyDataSet()
        self.refreshControl.endRefreshing()
        self.isrefreshed = false
    }
}

//MARK:- Webservice delegates
extension HomeItemsVC : GetCategoriesDelegate {
    
    func willRequestCategories(){
        
    }
    
    func categoriesReceivedSuccessFully(categories : [Category]){
        AppSharedData.shared.categories = categories
        
        NotificationCenter.default.post(Notification(name: Notification.Name("CategoryRecieved")))
    }
    
    func failedToReceiveCategories(){
        
    }
    
}
/*
//MARK:- Like unlike delegate
extension HomeItemsVC : LikeUnlikeDelegate{
    
    func willRequestLikeUnlikeProduct() {
        
    }
    
    func productLikeUnlikeSuccessFull(product: Product) {
        self.refreshSellfingTabForUserProfile()
    }
    
    func failedToLikeUnlikeProduct(message: String, product: Product) {
        
    }
}

//MARK:- Like unlike delegate
extension HomeItemsVC : GetLikeStatusBack{
    func getLikeStatus(product: Product) {
        print("current like status of that product is", product.isLiked)
    }
    
}
*/
