//
//  CatListHomeCllctionVCell.swift
//  TagHawk
//
//  Created by Apple on 26/12/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

class CatListHomeCllctionVCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCatCollec: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    
    func populateData(category : Category){
        self.imgCatCollec.setImage_kf(imageString: category.categoryImage, placeHolderImage: AppImages.customerLogo.image)
        self.categoryTitle.text = category.description
    }
    
}

class CatListHeadViewCVView: UICollectionReusableView {
    
    @IBOutlet weak var btnAll: UIButton!
    
}

class CatListFooterViewCVView: UICollectionReusableView {
    
    @IBOutlet weak var btnMore: UIButton!
    
    
}
