//
//  MapVC.swift
//  TagHawk
//
//  Created by Appinventiv on 05/04/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation


class MapVC: BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var currentLocationButton: UIButton!
    
    //MARK:- variables
    var productLocation = CLLocationCoordinate2D()
    private let circle = GMSCircle()
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        updateNavigationBar(withTitle: "Product Location", leftButtons: [], rightButtons: [], navBarColor: .white)
        self.leftBarItemImage(change: UIImage(named: "icBackBlack")!, ofVC: self)
    }
    
    override func leftBarButtonTapped() {
        super.leftBarButtonTapped()
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBAction
    @IBAction func currentLocationButtonTapped(_ sender: UIButton) {
        
        if SelectedFilters.shared.appliedFiltersOnItems.lat != 0.0{
            self.setCameraPosition(lat: SelectedFilters.shared.appliedFiltersOnItems.lat, long: SelectedFilters.shared.appliedFiltersOnItems.long)
        }else{
            LocationManager.shared.startUpdatingLocation {[weak self] (loc, err) in
                if let location = loc,let weakSelf = self {
                    weakSelf.setCameraPosition(lat: location.coordinate.latitude, long: location.coordinate.longitude)
                }
            }
        }
    }
    
    
    func addMarkerToCordinates(lat : Double, long : Double){
        let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        circle.map = nil
        circle.position = position
        circle.radius = 2500
        circle.fillColor = #colorLiteral(red: 0.168627451, green: 0.8078431373, blue: 0.9921568627, alpha: 0.4560684419)
        circle.strokeColor = #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 0.6538842429)
        circle.strokeWidth = 2
        //marker.map = mapView
        circle.map = mapView
        self.setCameraPosition(lat: lat, long: long)
    }
    
    func setCameraPosition(lat : Double, long : Double){
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 14)
        self.mapView.animate(to: camera)
    }
    
    //MARK:- Init set up
    private func initialSetup() {
        self.addMarkerToCordinates(lat: productLocation.latitude, long: productLocation.longitude)
        let marker = GMSMarker()
        marker.icon = AppImages.currentLocation.image
        if SelectedFilters.shared.appliedFiltersOnItems.lat != 0.0{
            marker.position = CLLocationCoordinate2D(latitude: SelectedFilters.shared.appliedFiltersOnItems.lat, longitude: SelectedFilters.shared.appliedFiltersOnItems.long)
            marker.map = mapView
        }else{
            LocationManager.shared.startUpdatingLocation {[weak self] (loc, err) in
                if let location = loc,let weakSelf = self {
                    marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                    marker.map = weakSelf.mapView
                }
            }
        }
    }
}
