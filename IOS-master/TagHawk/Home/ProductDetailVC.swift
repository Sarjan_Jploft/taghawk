//
//  ProductDetailVCV.swift
//  TagHawk
//
//  Created by Appinventiv on 31/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol GetLikeStatusBack : class {
    func getLikeStatus(product : Product)
}

protocol ProductReportedDelegate : class {
    func productReportedSuccessFully(prodId : String)
}

class ProductDetailVC : BaseVC {
    
    //MARK:- IbOutlets
    @IBOutlet weak var gradientImgView: UIImageView!
    @IBOutlet weak var productDetailTableView: UITableView!
    @IBOutlet weak var productImageCollectionView: UICollectionView!
    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var likeButtonShadowView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var backWhiteImageView: UIImageView!
    @IBOutlet weak var shareWhiteImageView: UIImageView!
    @IBOutlet weak var reportWhiteImageView: UIImageView!
    @IBOutlet weak var backBlackImageView: UIImageView!
    @IBOutlet weak var shareBlackImageView: UIImageView!
    @IBOutlet weak var reportBlackImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    //    @IBOutlet weak var statusBarViewHeight: NSLayoutConstraint!
    @IBOutlet weak var statusBarViewWithColor: UIView!
    //    @IBOutlet weak var statusBarBackViewWithColorHeight: NSLayoutConstraint!
    
    
    //MARK:- Variables
    var prodId : String = ""
    private let controler = ProductDetailControler()
    private let cartControler = CartControler()
    var product = Product()
    weak var likeStatusBackDeleate : GetLikeStatusBack?
    private let followControler = FollowingFollowersControler()
    private var shipTypeSelected: ShippingAvailability = .deliver
    weak var prodReportedDelegate : ProductReportedDelegate?
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
        gradientImgView.isHidden = false
        statusBarView.isHidden = false
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
        self.controler.reportProductDelegate = self
        self.cartControler.addProductDelegate = self
        self.cartControler.removeAllDelegate = self
        self.followControler.followUserDelegate = self
        self.controler.likeDelegate = self
    }
    
    override func leftBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func loadView() {
        super.loadView()
    }
    
    //MARK:- IBActions
    
    //MARK:- Like button tapped
    @IBAction func likeButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if self.product.isCreatedByMe{
            let vc = AddProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.product = self.product
            vc.commingFor = .edit
            vc.refreshDelegate = self
            self.present(vc, animated: true, completion: nil)
        }else{
            if !AppNetworking.isConnectedToInternet {
                CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                return
            }
            self.controler.likeUnlikeProduct(product: self.product)
            self.product.isLiked = !self.product.isLiked
            self.setIsLikedStatus()
            self.likeStatusBackDeleate?.getLikeStatus(product: self.product)
        }
        
    }
    
    //MARK:- IBActions
    
    //MARK:- Back button tapped
    @IBAction func bacjButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Report button tapped
    @IBAction func reportButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let reportScene = ReportPopupVC.instantiate(fromAppStoryboard: .Home)
        reportScene.delegate = self
        reportScene.modalPresentationStyle = .overCurrentContext
        navigationController?.present(reportScene, animated: true, completion: nil)
    }
    
    //MARK:- Share button tapped
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.shareProduct(product: self.product)
        CommonFunctions.shareWithSocialMedia(message: self.product.sharingUrl, vcObj: self, sub: product.title + "'s link")
    }
    
    //MARK:- Page control value changed
    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
        self.productImageCollectionView.scrollToItem(at: IndexPath(item: sender.currentPage, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
    }
    
    //MARK:- Chant button tabbed
    @IBAction func chatButtonYapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if UserProfile.main.loginType == .none {
            AppNavigator.shared.tabBar?.showLoginSignupPopup()
            return
        }
        
        if self.product.isCreatedByMe{
            deleteProduct()
        }else{
            chatButton.isEnabled = true
            let scene = SingleChatVC.instantiate(fromAppStoryboard: .Messages)
            scene.productDetail = product
            scene.selectedProductID = product.productId
            scene.vcInstantiated = .productDetail
            navigationController?.pushViewController(scene, animated: true)
        }
    }
    
    //MARK:- Buy now button tapped
    @IBAction func buyNowButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if UserProfile.main.loginType == .none {
            AppNavigator.shared.tabBar?.showLoginSignupPopup()
            return
        }
        
        if self.product.isCreatedByMe{
            
            self.product.isPromoted ?             CommonFunctions.showToastMessage(LocalizedString.Product_AlreadyPromoted.localized) : promoteProduct()
            
        }else{
            if self.product.productStatus == 2 || self.product.productStatus == 5 { return }
            self.buyProduct()
        }
    }
    
    //MARK:- Set up nottom button
    func setUpBottomButtons() {
        let chatButtonTitle = self.product.isCreatedByMe ? LocalizedString.Delete.localized : LocalizedString.Chat.localized
        self.chatButton.setTitle(chatButtonTitle)
        setBuyButtonTitle()
    }
    
    //MARK:- Set buy button title
    func setBuyButtonTitle(){
        
        if self.product.isCreatedByMe{
            
            let buyNowButtonTitle = product.isPromoted ? LocalizedString.Promoted.localized  : LocalizedString.Promote.localized
            self.buyNowButton.setTitle(buyNowButtonTitle)
            
        }else{
            let buyNowButtonTitle = (product.productStatus == 2 || product.productStatus == 5 ? LocalizedString.Sold.localized : LocalizedString.Reserve_Item.localized)
            self.buyNowButton.setTitle(buyNowButtonTitle)
        }
    }
}

private extension ProductDetailVC {
    //MARK:- Set up view
    func setUpSubView(){
        self.likeButton.round()
        likeButton.borderColor = UIColor.lightGray
        likeButton.borderWidth = 0.5
        self.likeButtonShadowView.round()
        
        self.likeButtonShadowView.drawShadow(shadowColor: AppColors.pickShadow , shadowOpacity: 0.1, shadowRadius: Float(self.likeButtonShadowView.frame.height / 2), cornerRadius: Float(self.likeButtonShadowView.frame.height / 2), offset: CGSize(width: 0, height: 2))
        self.tableHeaderView.frame.size.height = screenWidth + 30
        
        self.likeButtonShadowView.clipsToBounds = false
        self.configureTableView()
        self.configureCollectionView()
        self.chatButton.round(radius: 10)
        self.buyNowButton.round(radius: 10)
        
        self.chatButton.setAttributes(title: LocalizedString.Chat.localized, font: AppFonts.Galano_Semi_Bold.withSize(16) , titleColor: AppColors.appBlueColor, backgroundColor: UIColor.clear)
        
        self.buyNowButton.setAttributes(title: LocalizedString.Reserve_Item.localized, font: AppFonts.Galano_Semi_Bold.withSize(16) , titleColor: UIColor.white, backgroundColor: UIColor.clear)
        self.chatButton.setBorder(width: 1.0, color: AppColors.appBlueColor)
        
        self.tableViewTop.constant = Display.typeIsLike == .iphoneX ? -44 : -20
        
        self.titleLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.white)
        
        self.configureNavigationView()
        self.controler.getProductDetails(id: self.prodId)
        
        //        self.statusBarViewHeight.constant = Display.typeIsLike == .iphoneX ? 44 : 20
        
        //        self.statusBarBackViewWithColorHeight.constant = Display.typeIsLike == .iphoneX ? 40 : 20
        
        self.backBlackImageView.isHidden = true
        self.shareBlackImageView.isHidden = true
        self.reportBlackImageView.isHidden = true
    }
    
    //MARK:- Configure navigations
    func configureNavigationView(){
        
        self.backWhiteImageView.image = AppImages.backWhite.image
        self.backWhiteImageView.alpha = 1.0
        
        self.shareWhiteImageView.image = AppImages.shareWhite.image
        self.shareWhiteImageView.alpha = 1.0
        
        self.reportWhiteImageView.image = AppImages.reportWhite.image
        self.reportWhiteImageView.alpha = 1.0
        
        
        self.backBlackImageView.image = AppImages.backBlack.image
        self.backBlackImageView.alpha = 0.0
        
        self.shareBlackImageView.image = AppImages.share.image
        self.shareBlackImageView.alpha = 0.0
        
        self.reportBlackImageView.image = AppImages.reportWhite.image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.reportBlackImageView.tintColor = UIColor.black
        self.reportBlackImageView.alpha = 0.0
        self.statusBarViewWithColor.backgroundColor = .clear
        
    }
    
    //MARK:- Configure page control
    func configurePageControl(){
        self.pageControl.numberOfPages = self.product.images.count
        self.pageControl.currentPage = 0
        self.pageControl.currentPageIndicatorTintColor = AppColors.appBlueColor
        self.pageControl.pageIndicatorTintColor = AppColors.lightGreyTextColor.withAlphaComponent(0.5)
        self.pageControl.isHidden = self.product.images.count < 2
    }
    
    //MARK:- configure tableview
    func configureTableView(){
        self.productDetailTableView.registerNib(nibName: ProduceDetailAndDescriptionCell.defaultReuseIdentifier)
        self.productDetailTableView.registerNib(nibName: ProductDetailRatingCell.defaultReuseIdentifier)
        self.productDetailTableView.registerNib(nibName: ProductDetailMapCell.defaultReuseIdentifier)
        self.productDetailTableView.registerNib(nibName: SimilatProductsCell.defaultReuseIdentifier)
        self.productDetailTableView.rowHeight = UITableView.automaticDimension
        self.productDetailTableView.separatorStyle = .none
        self.productDetailTableView.estimatedRowHeight = 100
        self.productDetailTableView.delegate = self
        self.productDetailTableView.dataSource = self
    }
    
    //MARK:- Configure collectionview
    func configureCollectionView(){
        self.productImageCollectionView.registerNib(nibName: ProductDetailImageCellColl.defaultReuseIdentifier)
        self.productImageCollectionView.showsHorizontalScrollIndicator = false
        self.productImageCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.productImageCollectionView.isPagingEnabled = true
        self.productImageCollectionView.delegate = self
        self.productImageCollectionView.dataSource = self
    }
    
    
    func setIsLikedStatus(){
        if !product.isCreatedByMe {
            self.likeButton.setImage(self.product.isLiked ? AppImages.filledHeart.image : AppImages.emptyHeart.image, for: UIControl.State.normal)
        }
    }
    
    
    //MARK:- Show popup
    func displayPopUp(for type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none, msg : String = ""){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        vc.msg = msg
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //MARK:-
    func buyProduct(){
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        if self.product.shippingType.contains(.shipping) && self.product.shippingType.count > 1{
            let vc = SelectShippingTypePopUp.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.modalPresentationStyle = .overCurrentContext
            vc.delegate = self
            vc.availableShipping = self.product.shippingType
            self.present(vc, animated: true, completion: nil)
        }else{
            guard let shipping = self.product.shippingType.first else { return }
            self.cartControler.addToCart(product: self.product, shipping: shipping)
        }
    }
    
    //MARK:- Promote product
    func promoteProduct() {
        let vc = FeatureProductVC.instantiate(fromAppStoryboard: .Home)
        let prod = product.getAddProductDataFromProduct()
        vc.addProductData = prod
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true) {
        }
    }
    
    //MARK:- Select camera tab
    func selectCameraTab(){
        let vc = CameraVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: true)
        vc.addProductSuccessFullDelegate = self
        self.present(nav, animated: true, completion: nil)
    }
    
    //MARK:- delete product
    func deleteProduct() {
        displayPopUp(for: .deleteProduct)
    }
    
    //MARK:- Show popup
    func showPopup(type : CustomPopUpVC.CustomPopUpFor = CustomPopUpVC.CustomPopUpFor.none, msg : String = ""){
        let vc = CustomPopUpVC.instantiate(fromAppStoryboard: AppStoryboard.PreLogin)
        vc.popupFor = type
        vc.delegateWithType = self
        //        vc.delegate = self
        vc.msg = msg
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @objc private func userProfileBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        if product.userId != UserProfile.main.userId {
            let userProfileScene = OtherUserProfileVC.instantiate(fromAppStoryboard: .Profile)
            userProfileScene.userId = product.userId
            navigationController?.pushViewController(userProfileScene, animated: true)
        } else {
            
            navigationController?.popViewController(animated: true)
            if let navCont = navigationController {
                navCont.viewControllers.forEach { (vc) in
                    if let cont = vc as? TagHawkTabBarControler {
                        cont.showTabBar(animated: true)
                        cont.selectTab(index: 4)
                    }
                }
            }
        }
    }
    
    //MARK:-
    @objc private func ratingsBtnTapped(_ sender: UIButton) {
        view.endEditing(true)
        let ratingsScene = RatingVC.instantiate(fromAppStoryboard: .Profile)
        var userData = UserProfile()
        userData.profilePicture = product.profilePicture
        userData.created = product.userCreated
        userData.fullName = product.fullName
        userData.rating = Double(product.sellerRating)
        ratingsScene.profileData = userData
        ratingsScene.profileData.id = product.userId
        gradientImgView.isHidden = true
        statusBarView.isHidden = true
        AppNavigator.shared.parentNavigationControler.pushViewController(ratingsScene, animated: true)
    }
}

//MARK:- Product posted successfully
extension ProductDetailVC : PostnotherDelegate{
    
    func productPramotedSuccessfull() {
        self.product.isPromoted = true
    }
    
    func postAnother() {
        self.selectCameraTab()
    }
    
}

extension ProductDetailVC : ProductCreatedSuccessFully{
    
    func addProductSuccessFully(prod: AddProductData) {
        let vc = FeatureProductVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.addProductData = prod
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true) {
        }
    }
    
}

//MARK:- Configure Navigation
private extension ProductDetailVC {
    
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

//MARK:- TableView datasource and delegates
extension ProductDetailVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return UITableView.automaticDimension
        case 1:
            return self.product.isFacebookLogin || self.product.isEmailVerified || self.product.isDocumentsVerified || self.product.isPhoneVerified ? 159 : 110
            
        case 2:
            return 280
        default:
            if self.product.similarProducts.count == 0 {
                return 0
            }
            
            let heightOfOneProd = (screenWidth - 30) / 2
            let multiplier : CGFloat = self.product.similarProducts.count > 2 ? 2 : 1
            let totalCellHeight = heightOfOneProd * multiplier
            return totalCellHeight + 90 + 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            return self.getDetailAndDescriptionCell(tableView, indexPath: indexPath)
            
        case 1:
            return self.getRatingCell(tableView, indexPath: indexPath)
            
        case 2:
            return self.getMapCell(tableView, indexPath: indexPath)
            
        default:
            return self.getSimilarProductsCell(tableView, indexPath: indexPath)
        }
    }
    
    func getDetailAndDescriptionCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProduceDetailAndDescriptionCell.defaultReuseIdentifier) as? ProduceDetailAndDescriptionCell else { fatalError("SettingsVC....\(ProduceDetailAndDescriptionCell.defaultReuseIdentifier) cell not found") }
        cell.populateData(product: self.product)
        return cell
        
    }
    
    func getRatingCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailRatingCell.defaultReuseIdentifier) as? ProductDetailRatingCell else { fatalError("SettingsVC....\(ProductDetailRatingCell.defaultReuseIdentifier) cell not found") }
        cell.profileBtn.addTarget(self, action: #selector(userProfileBtnTapped(_:)), for: .touchUpInside)
        cell.ratingButton.addTarget(self, action: #selector(ratingsBtnTapped(_:)), for: .touchUpInside)
        cell.populateData(product: self.product)
        return cell
        
    }
    
    func getMapCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailMapCell.defaultReuseIdentifier, for: indexPath) as? ProductDetailMapCell else { fatalError("Could not dequeue ProductDetailMapCell at index \(indexPath) in LoginVC") }
        cell.populateData(product: self.product)
        cell.mapViewTap = {[weak self] in
            self?.gradientImgView.isHidden = false
            self?.statusBarView.isHidden = false
        }
        return cell
    }
    
    func getSimilarProductsCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SimilatProductsCell.defaultReuseIdentifier, for: indexPath) as? SimilatProductsCell else { fatalError("Could not dequeue SimilatProductsCell at index \(indexPath) in LoginVC") }
        cell.products = self.product.similarProducts
        cell.delegate = self
        cell.viewAllButton.isHidden = true
        cell.similarProductsCollectionView.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//MARK:- CollectionView Datasource and Delegats
extension ProductDetailVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.product.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == self.productImageCollectionView ? 0 : 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width  , height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getProductImageCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductImageCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductDetailImageCellColl.defaultReuseIdentifier, for: indexPath) as? ProductDetailImageCellColl else { fatalError("Could not dequeue ProductDetailImageCellColl  at index \(indexPath) in LoginVC") }
        
        cell.populateData(imageUrl: self.product.images[indexPath.item].image)
        return cell
    }
    
    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchedProduceCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? SearchedProduceCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let browser = SKPhotoBrowser(photos: createWebPhotos())
        browser.initializePageIndex(indexPath.item)
        browser.delegate = self
        present(browser, animated: true, completion: nil)        
    }
}

//MARK:- Scrollview delegates
extension ProductDetailVC  {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let factor = scrollView.contentOffset.y / screenWidth
        self.navigationView.backgroundColor = AppColors.appBlueColor.withAlphaComponent(factor)
        self.statusBarView.backgroundColor = AppColors.appBlueColor.withAlphaComponent(factor)
        gradientImgView.alpha = (1 - factor)
        self.titleLabel.alpha = factor
        if scrollView != self.productImageCollectionView{ return }
        guard let indexPath =  self.productImageCollectionView.indexPathForItem(at: CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y)) else { return }
        self.pageControl.currentPage = indexPath.item
    }
    
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if scrollView != self.productImageCollectionView{ return }
        
        guard let indexPath =  self.productImageCollectionView.indexPathForItem(at: CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y)) else { return }
        self.pageControl.currentPage = indexPath.item
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView != self.productImageCollectionView{ return }
        guard let indexPath =  self.productImageCollectionView.indexPathForItem(at: CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y)) else { return }
        self.pageControl.currentPage = indexPath.item
    }
}

//MAK:- REPORTPOPUPDELEGATE
extension ProductDetailVC: ReportPopupDelegate {
    func leftBtnTapped() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func rightBtnTapped(optionSelected: String) {
        if optionSelected.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.Please_Choose_An_Option.localized)
        } else {
            navigationController?.dismiss(animated: true, completion: nil )
            self.controler.reportProduct(product: self.product, reason: optionSelected)
        }
    }
}

//MARK:- Full view of images
//===========================
extension ProductDetailVC : SKPhotoBrowserDelegate {
    
    func createWebPhotos() -> [SKPhotoProtocol] {
        
        return (0..<self.product.images.count).map { (i: Int) -> SKPhotoProtocol in
            
            let productImage = self.product.images[i]
            
            let photo = SKPhoto.photoWithImageURL( productImage.image, holder: AppImages.productPlaceholder.image)
            
            photo.caption = ""
            photo.shouldCachePhotoURLImage = false
            return photo
        }
    }
}

//MARK:- Similar product delegate
extension ProductDetailVC : SimilarProductTappedDelegate {
    func viewAllBtnTapped(_ sender: UIButton) {
        
    }
    
    func similarProductTapped(product : Product) {
        let vc = ProductDetailVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.prodId = product.productId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Like similar products
    func likeSimilarProduct(product : Product) {
        //        self.product.similarProducts.inde
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        self.controler.likeUnlikeProduct(product: product)
        var convertedProd = product
        convertedProd.isLiked = !product.isLiked
        
        if let ind = self.product.similarProducts.firstIndex(where: { (obj) -> Bool in
            return obj.productId == convertedProd.productId
        }){
            self.product.similarProducts[ind] = convertedProd
        }else{
            
        }
    }
}

//MARK:- Refresh delegate
extension ProductDetailVC : RefreshProductDetails {
    
    func refreshProductDetails() {
        self.controler.getProductDetails(id: self.prodId)
    }
    
}

//MARK:-
extension ProductDetailVC : ProductDetailDelagate {
    
    //MARK:- product not found
    func productNotFound(msg: String) {
        showPopup(type: .notFound, msg: msg)
    }
    
    func productDeleteSuccess() {
        AppNavigator.shared.parentNavigationControler.popViewController(animated: true)
    }
    
    func productDeleteFailed() {
        
    }
    
    
    func willRequestProductDetail() {
        self.view.showIndicator()
        self.productDetailTableView.isHidden = true
        self.buttonContainerView.isHidden = true
    }
    
    func productDetailReceivedSuccessFilly(product: Product) {
        self.view.hideIndicator()
        self.product = product
        self.titleLabel.text = product.title
        likeButton.setImage(product.isCreatedByMe ? AppImages.edit.image : AppImages.emptyHeart.image, for: .normal)
        self.setIsLikedStatus()
        self.configurePageControl()
        self.setUpBottomButtons()
        self.productDetailTableView.reloadData()
        self.productImageCollectionView.reloadData()
        self.productDetailTableView.isHidden = false
        self.buttonContainerView.isHidden = false
        reportButton.isHidden = product.isCreatedByMe ? true : false
        reportWhiteImageView.isHidden = product.isCreatedByMe ? true : false
    }
    
    func failedToReceiveProducts(message: String) {
        self.view.hideIndicator()
    }
}

//MARK:- Report product delegate
extension ProductDetailVC : ReportProductDelegate {
    
    func willReportProduct() {
        self.view.showIndicator()
    }
    
    func productReportedSuccessFully(product: Product, msg: String) {
        CommonFunctions.showToastMessage(msg)
        self.view.hideIndicator()
        self.prodReportedDelegate?.productReportedSuccessFully(prodId: self.prodId)
        self.navigationController?.popViewController(animated: true)
    }
    
    func failedToReportProduct() {
        self.view.hideIndicator()
        
    }
}

extension ProductDetailVC : GetSelectedShippingAvailability  {
    
    //MARK:- Get selected shipping avilability
    func getSelectedShippingAvailability(shipping : ShippingAvailability){
        shipTypeSelected = shipping
        self.cartControler.addToCart(product: self.product, shipping: shipping)
    }
    
}

extension ProductDetailVC : CustomPopUpDelegateWithType {
    
    func noButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        
    }
    
    func yesButtonTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        if type == .deleteProduct {
            self.controler.deleteProduct(self.product.productId)
            
        } else if type == .nonShippingProductAddedToCart {
            let vc = CartVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.commingFor = .productDetails
            self.navigationController?.pushViewController(vc, animated: true)
        }else if type == .removePreviousItemFromCartAndAddNew{
            self.cartControler.removeAllProductsFromCart()
            
        }
    }
    
    func okTapped(type: CustomPopUpVC.CustomPopUpFor, product: CartProduct?) {
        if type == .removePreviousItemFromCartAndAddNew{
            self.cartControler.removeAllProductsFromCart()
            
        } else if type == .notFound {
            navigationController?.popViewController(animated: true)
        }else if type == .productAddedSuccessFullyToCart {
            
            let vc = CartVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.commingFor = .productDetails
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- Webservices delegate

//MARK:- Add to cart delegate
extension ProductDetailVC : AddToCartDelegate {
    
    func willRequestAddProduct(){
        self.view.showIndicator()
    }
    
    func addProductSuccessFully(){
        self.view.hideIndicator()
        if self.product.shippingType.contains(.shipping) && self.product.shippingType.count == 1{
            self.showPopup(type: CustomPopUpVC.CustomPopUpFor.productAddedSuccessFullyToCart, msg: LocalizedString.Product_Added_Successfully.localized)
            
        } else {
            if shipTypeSelected == .shipping {
                self.showPopup(type: CustomPopUpVC.CustomPopUpFor.productAddedSuccessFullyToCart, msg: LocalizedString.Product_Added_Successfully.localized)
                
            } else {
                self.showPopup(type: CustomPopUpVC.CustomPopUpFor.nonShippingProductAddedToCart, msg: LocalizedString.Product_Added_Successfully.localized)
            }
        }
    }
    
    func errorOccuredWhileAddingProduct(errorType: CartControler.AddToCartErrorType, message: String) {
        self.view.hideIndicator()
        
        switch errorType{
            
        case .alreadyAdded:
            displayPopUp(for : CustomPopUpVC.CustomPopUpFor.itemAlreadyAddedToCart, msg : message)
            
        case .shippingProductAlreadyAdded:
            displayPopUp(for : CustomPopUpVC.CustomPopUpFor.removePreviousItemFromCartAndAddNew, msg : message)
            
        default:
            break
            
        }
    }
}


extension ProductDetailVC : RemoveAllProductsFromCartDelegate {
    
    func willRemoveAllProducts(){
        self.view.showIndicator()
    }
    
    func allProductsRemovedSuccessFully(message : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(message)
        //        self.buyProduct(
        self.cartControler.addToCart(product: self.product, shipping: self.shipTypeSelected)
    }
    
    func errorOccuredInRemovingProducts(messahe : String){
        self.view.hideIndicator()
        CommonFunctions.showToastMessage(messahe)
    }
}

extension ProductDetailVC : FollowUserDelegate {
    
    func followUserSuccessFully(userId: String) {
        self.view.hideIndicator()
    }
    
    func failedToFollowUser(message: String) {
        self.view.hideIndicator()
    }
    
    func willFollowUser(){
        self.view.showIndicator()
    }
}

//MARK:- Like unlike delegate
extension ProductDetailVC : LikeUnlikeDelegate{
    
    func willRequestLikeUnlikeProduct() {
        
    }
    
    func productLikeUnlikeSuccessFull(product: Product) {
        self.refreshSellfingTabForUserProfile()
        NotificationCenter.default.post(Notification(name: Notification.Name("LikeUnlikeStatus")))
    }
    
    func failedToLikeUnlikeProduct(message: String, product: Product) {
        
    }
}
