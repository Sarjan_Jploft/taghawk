//
//  HomeTagVC.swift
//  TagHawk
//
//  Created by Appinventiv on 11/02/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import GoogleMaps
import Popover

let kClusterItemCount = 50
let kCameraLatitude = -33.8
let kCameraLongitude = 151.2


class HomeTagVC: BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var addutton: UIButton!
    @IBOutlet weak var clusterListCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewButtom: NSLayoutConstraint!
    
    @IBOutlet weak var tblView: UITableView!
    
    
    //MARK:- Variables
    var controler = HomeControler()
    var tags : [Tag] = []
    var tappedClusterData : [TagItem] = []
    
    var clusterManager: GMUClusterManager!
    var currentLocation : CLLocation = CLLocation() {
        
        didSet {
            if !serviceCalledFirstTime{
                SelectedFilters.shared.appliedFiltersOnTag.lat = currentLocation.coordinate.latitude
                SelectedFilters.shared.appliedFiltersOnTag.long = currentLocation.coordinate.longitude
                self.setCameraPosition(lat: currentLocation.coordinate.latitude, long: currentLocation.coordinate.longitude)
                self.controler.getTagsData()
                //                print("currnt loc lat",currentLocation.coordinate.latitude)
                //                print("currnt loc lng",currentLocation.coordinate.longitude)
            }
        }
    }
    
    var serviceCalledFirstTime = false
    var isNavigatedToTagDetail = false
    
    //MARK:- View life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.tagDelegate = self
    }
    
    //MARK:- Add button tapped
    //Changes
    /*
     @IBAction func addbUttonTapped(_ sender: UIButton) {
     view.endEditing(true)
     
     if UserProfile.main.loginType == .none {
     AppNavigator.shared.tabBar?.showLoginSignupPopup()
     return
     }
     
     let appVC = AddTagViewController.instantiate(fromAppStoryboard: .AddTag)
     
     appVC.tagCreateSuccess = {[weak self] tag in
     self?.tags.append(tag)
     self?.mapView.clear()
     self?.clusterManager.clearItems()
     self?.generateClusterItems()
     }
     AppNavigator.shared.parentNavigationControler.pushViewController(appVC, animated: true)
     }
     */
    //MARK:- Current location button tapped
    @IBAction func currentLocationButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        DispatchQueue.main.async {
            LocationManager.shared.startUpdatingLocation { (loc, erro) in
                guard let location = loc else { return }
                self.setCameraPosition(lat: location.coordinate.latitude, long: location.coordinate.longitude)
                LocationManager.shared.stopLocationManger()
            }
        }
    }
    
    //MARK:- generate cluster item
    func generateClusterItems() {
        
        let context = 0.0001
        for (_, item) in self.tags.enumerated() {
            let position = CLLocationCoordinate2D(latitude: item.tagLatitude + context * randomScale() , longitude: item.tagLongitude + context * randomScale())
            let item =
                TagItem(position: position, tag: item)
            clusterManager.add(item)
        }
        clusterManager.cluster()
    }
    
    //MARK:- Remove tag from map
    func removeparticulatTagFromMap(tagId : String){
        let updatedTags = tags.filter { $0.id != tagId}
        tags = updatedTags
        mapView.clear()
        clusterManager.clearItems()
        generateClusterItems()
    }
}

//MARK:- Private functions
private extension HomeTagVC {
    
    //MARK:- Set up view
    func setUpSubView(){
        self.mapView.settings.rotateGestures = false
        makeClusterMap()
        configureCollectionView()
        configureTableview()
    }
    
    //MARK:- Configure collection view
    func configureCollectionView(){
        self.clusterListCollectionView.registerNib(nibName: ClusterListCellCollectionViewCell.defaultReuseIdentifier)
        self.clusterListCollectionView.showsHorizontalScrollIndicator = false
        self.clusterListCollectionView.isPagingEnabled = true
        self.collectionViewButtom.constant = -150
        self.clusterListCollectionView.delegate = self
        self.clusterListCollectionView.dataSource = self
    }
    
    //MARK:- Configure TableView
    func configureTableview()
    {
        self.tblView.registerNib(nibName: TagListOnMapTVCell.defaultReuseIdentifier)
        
//        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
//        self.setDataSourceAndDelegateForTable(forTableView: self.tblView)
        
    }
    
    
    
    
    
    //MARK:- Make cluster in map
    func makeClusterMap(){
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        self.clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        
    }
    
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
}

//MARK:- Map related methods
extension HomeTagVC {
    
    //MARK:- Add marker to cordinates
    func addMarkerToCordinates(lat : Double, long : Double){
        let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let marker = GMSMarker(position: position)
        marker.icon = AppImages.blueMarker.image
        marker.map = mapView
    }
    
    func setCameraPosition(lat : Double, long : Double, withAnimation : Bool = true, withZoomLevel : Float = 14){
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: withZoomLevel)
        withAnimation ? (self.mapView.animate(to: camera)) : (self.mapView.camera = camera)
        //        self.mapView.animate(to: camera)
    }
}

//MARK:- map delegates
extension HomeTagVC : GMUClusterManagerDelegate {
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        self.setCameraPosition(lat:  cluster.position.latitude, long:  cluster.position.longitude)
        self.mapView.animate(toZoom: self.mapView.camera.zoom + 1.0)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        DispatchQueue.main.async {
            if let mark = marker.userData as? TagItem{
                
                self.setCameraPosition(lat: mark.tag.tagLatitude, long: mark.tag.tagLongitude, withAnimation: false, withZoomLevel: self.mapView.camera.zoom)
                
                self.isNavigatedToTagDetail = true
                let vc = TagDetailVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
                vc.tagId = mark.tag.id
                AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
                
            }
        }
        
        return nil
        
    }
    
}

extension HomeTagVC : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        UIView.animate(withDuration: 0.3) {
            self.collectionViewButtom.constant = -150
            self.view.layoutIfNeeded()
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if let mark = marker.userData as? TagItem{
            let vc = TagDetailVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
            vc.tagId = mark.tag.id
            AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
        }
    }
}

extension HomeTagVC : GMUClusterRendererDelegate{
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        
        if let mark = marker.userData as? TagItem{
            
            let markerView : CustomMarkerView = .instantiateFromNib()
            markerView.populateData(tag: mark.tag)
            markerView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            marker.iconView = markerView
        }
    }
    
}

//MARK:- CollectionView Datasource and Delegats
extension HomeTagVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tappedClusterData.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getProductCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ClusterListCellCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ClusterListCellCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        cell.populateData(tag: self.tappedClusterData[indexPath.item].tag)
        cell.viewDetails.addTarget(self, action: #selector(viewDetailsButtonTapped), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    @objc func viewDetailsButtonTapped(sender : UIButton){
        guard let indexPath = sender.collectionViewIndexPath(self.clusterListCollectionView) else { return }
        let vc = TagDetailVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
        vc.tagId = self.tappedClusterData[indexPath.item].tag.id
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
}


extension HomeTagVC : GetTagsDelegate {
    
    func willRequestCommunities() {
        self.view.showIndicator()
        
    }
    
    func communitiesReceivedSuccessfully(comunities: [Tag]) {
        if comunities.isEmpty {
            CommonFunctions.showToastMessage(LocalizedString.NoTagsFound.localized)
        }
        self.view.hideIndicator()
        self.tags = comunities
        printDebug(self.tags.count)
        self.mapView.clear()
        clusterManager.clearItems()
        generateClusterItems()
        self.setCameraPosition(lat: SelectedFilters.shared.appliedFiltersOnTag.lat, long: SelectedFilters.shared.appliedFiltersOnTag.long)
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
        
    }
    
    func failedToReceiveCommunities() {
        self.view.hideIndicator()
    }
}

//MARK:- Table Delegate and DataSource

extension HomeTagVC:UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tags.count == 0)
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No Data found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
            
            return 0
            
        }
        else
        {
            tableView.backgroundView = nil
            return tags.count
        }
        
        
        
        //return tags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TagListOnMapTVCell.defaultReuseIdentifier) as? TagListOnMapTVCell else{
            fatalError("Could not dequeue taglistonmaptvcell at index \(indexPath) in HomeTagVC")
        }
        
        cell.populateData(tag: tags[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = TagDetailVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
        vc.tagId = tags[indexPath.row].id
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
        
    }
    
    
}
