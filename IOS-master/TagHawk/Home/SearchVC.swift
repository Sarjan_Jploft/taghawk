//
//  SearchVCViewController.swift
//  TagHawk
//
//  Created by Appinventiv on 30/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit

protocol SearchDelegate : class {
    func getSearchData(text : String)
}

protocol GetSearchedTags : class {
    func getSearchedTags(tags : [Tag])
    func getSearchedTitle(title: String)
}

class SearchVC : BaseVC {

    enum SearchVCFrom {
        case homeItems
        case homeTags
        case searchProducts
        case shelf
        case searchShelf
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
    //MARK: Variables
    private var searchResults : [SearchResult] = []
    private var searchResultsForTags : [Tag] = []
    private var controler = SearchControler()
    var searchFrom = SearchVCFrom.homeItems
    weak var delegate : SearchDelegate?
    var searchStr : String = ""
    var selectedCategory = Category()
    weak var searchTagsDelegate : GetSearchedTags?
    var tagId : String = ""
    var tagName : String = ""
    //MARK:- View life cycle


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
        // Do any additional setup after loading the view.
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
        self.searchTextField.becomeFirstResponder()
    }
    
    //MARK:- Back button Tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
       self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        self.searchTextField.text = ""
        self.controler.searchResults(searchText : "")
    }
}

//MARK:- Setup subview
extension SearchVC{
    
    //MARK:- Set up view
    func setUpSubView(){
        self.configureTableView()
        self.searchTextField.delegate = self
        self.searchTextField.placeholder = LocalizedString.Search.localized
        self.searchTextField.addTarget(self, action: #selector(textDidChange), for: UIControl.Event.editingChanged)
        self.searchTextField.text = self.searchFrom == .homeItems ? "" : self.searchStr
        self.searchTextField.returnKeyType = .search
        self.requestData()
    }
    
    //MARK:- Request data
    func requestData(){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        switch self.searchFrom {
        case .homeItems, .searchProducts:
            self.controler.searchResults(searchText: self.searchStr)

        case .homeTags:
            self.controler.searchResultsForTags(searchText: self.searchStr)

        case .shelf, .searchShelf:
            self.controler.searchResultsForShelf(tagId: self.tagId, searchText: self.searchStr)

        }
    }
    
    //MARK:- Configure tableview
    func configureTableView(){
        self.searchTableView.registerNib(nibName: SearchTableViewCell.defaultReuseIdentifier)
        self.searchTableView.rowHeight = UITableView.automaticDimension
        self.searchTableView.estimatedRowHeight = 50
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
    }
}

//MARK:- Configure nvigation
private extension SearchVC{

    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}

//MARK:- Configure tableview
extension SearchVC : UITableViewDataSource, UITableViewDelegate {
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.searchFrom == .homeTags ? self.searchResultsForTags.count : self.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.defaultReuseIdentifier) as? SearchTableViewCell else {
            fatalError("SettingsVC....\(SearchTableViewCell.defaultReuseIdentifier) cell not found") }
        self.searchFrom == .homeTags ? cell.populatePlace(place: self.searchResultsForTags[indexPath.row].name) : cell.populateData(result: self.searchResults[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         self.performSearchOpperation(indexPath : indexPath)
    }
    
    func performSearchOpperation(indexPath : IndexPath){
        
        if self.searchFrom == .homeItems{
            let vc = SearchedProductsVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.searchKeyWord = self.searchResults[indexPath.row].title
            self.navigationController?.pushViewController(vc, animated: true)
       
        }else if self.searchFrom == .shelf{
            let vc = SearchedInShelfVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
            vc.searchKeyWord = self.searchResults[indexPath.row].title
            vc.tagId = self.tagId
            vc.tagName = self.tagName
            self.navigationController?.pushViewController(vc, animated: true)
          
        } else if searchFrom == .homeTags{
            self.searchTagsDelegate?.getSearchedTitle(title: self.searchResultsForTags[indexPath.row].name)
            self.searchTagsDelegate?.getSearchedTags(tags: [self.searchResultsForTags[indexPath.row]])
            self.navigationController?.popViewController(animated: true)
        }else{
            self.delegate?.getSearchData(text: self.searchResults[indexPath.row].title)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//mARK:- Textview delegates
extension SearchVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0 && string == " ") || string.containsEmoji {return false}
        
        guard let userEnteredString = textField.text else { return false }
        
        return self.isCharacterAcceptableForSearch(txt: userEnteredString, char: string)
        
    }
    
    func isCharacterAcceptableForSearch(txt : String, char : String) -> Bool {
        return char.isBackSpace || txt.count <= 20
    }
    
    @objc func textDidChange(_ textField : UITextField) {
        
        switch self.searchFrom {
        case .homeTags:
            self.controler.searchResultsForTags(searchText: textField.text ?? "")

        case .homeItems:
            self.controler.searchResults(searchText: textField.text ?? "")
            
        default:
            self.controler.searchResultsForShelf(tagId: self.tagId, searchText: textField.text ?? "")

        }

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        switch self.searchFrom  {
            
        case .homeItems:
            let vc = SearchedProductsVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
            vc.searchKeyWord = self.searchTextField.text ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .shelf:
            let vc = SearchedInShelfVC.instantiate(fromAppStoryboard: AppStoryboard.AddTag)
            vc.searchKeyWord = textField.text ?? ""
            vc.tagId = self.tagId
            vc.tagName = self.tagName
            self.navigationController?.pushViewController(vc, animated: true)
        case .homeTags:
            self.searchTagsDelegate?.getSearchedTitle(title: textField.text ?? "")
            self.searchTagsDelegate?.getSearchedTags(tags: self.searchResultsForTags)
            self.navigationController?.popViewController(animated: true)
        
        default:
            self.delegate?.getSearchData(text: self.searchTextField.text ?? "")
            self.navigationController?.popViewController(animated: true)
            
        }
        
        return true
    }
}

//MARK:- Api delegates
extension SearchVC : SearchResultDelegate{
   
    func willRequestSearchResult() {
        
    }
    
    func searchResultReceivedSuccessfully(result: [SearchResult]) {
        self.searchResults = result
        self.searchTableView.reloadData()
    }
    
    func searchResultForTagsReceivedSuccessfully(result: [Tag]) {
        self.searchResultsForTags = result
        self.searchTableView.reloadData()
    }
    
    func failedToReceiveSearchResult(message: String) {
        
    }
    
}
