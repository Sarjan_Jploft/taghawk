//
//  SearchedProductsVCViewController.swift
//  TagHawk
//
//  Created by Appinventiv on 30/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import DropDown

class SearchedProductsVC : BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var searchProductCollectionView: UICollectionView!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var sortingButton: UIButton!
    @IBOutlet weak var sortingTypeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var categoryButton: UIButton!
    
    
    //MARK:- Variables
    private var isItemSelected = true
    private var products : [Product] = []
    var searchKeyWord : String = "hello"
    private let controler = SearchedProductsControler()
    private var recognizer : UITapGestureRecognizer?
    var titleView = UILabel()
    private let dropDown = DropDown()
    var selectedCategory = Category()
    
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSubView()
    }
    
    override func bindControler() {
        super.bindControler()
        self.controler.delegate = self
        self.controler.likeDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigation()
        self.setSortingLabelText()
    }
    
    //MARK:- IBActions
    
    //MARK:- Back button Tapped
    @IBAction func backButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        self.refreshHome()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK:- Filter button tapped
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = FilterVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        AppNavigator.shared.parentNavigationControler.pushViewController(vc, animated: true)
    }
    
    //MARK:- Search button tapped
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = SearchVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.searchFrom = .searchProducts
        vc.searchStr = ""
        vc.selectedCategory = self.selectedCategory
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //MARK:- Categories button tapped
    @IBAction func categoryButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = CategoryVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.delegate = self
        vc.selectionFrom = .searchProducts
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Cart button tapped
    @IBAction func cartButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        let vc = CartVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

//MARK:- Private functions
private extension SearchedProductsVC{
    
    //MARK:- Set up view
    func setUpSubView(){
        
        self.sortingTypeLabel.setAttributes(text: LocalizedString.Newest.localized, font: AppFonts.Galano_Medium.withSize(13), textColor: UIColor.black)
        self.configureCollectionView()
        self.controler.getProducts(categoryId: self.selectedCategory.categoryId, searchText: self.searchKeyWord, page: self.page)
        self.searchLabel.setAttributes(text: LocalizedString.Search.localized, font: AppFonts.Galano_Regular.withSize(12) , textColor: AppColors.lightGreyTextColor)
        
        self.titleLabel.setAttributes(font: AppFonts.Galano_Medium.withSize(15), textColor: UIColor.black)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleTapped))
        titleLabel.isUserInteractionEnabled = true
        titleLabel.addGestureRecognizer(tap)
        
        self.searchBackView.isHidden = false
        self.titleLabel.isHidden = false
        self.setSearchLabelText()
        
    }
    
    //MARK:- Check is searching applied
    func isSearchApplied() -> Bool {
        return !self.searchKeyWord.isEmpty
    }
    
    //MARK:- Set search label text
    func setSearchLabelText(){
        if !self.searchKeyWord.isEmpty && !self.selectedCategory.description.isEmpty{
            self.searchLabel.attributedText = "\(self.searchKeyWord) in \(self.selectedCategory.description)".attributeStringWithAnotherColor(stringToColor: self.searchKeyWord)
        }else if !self.searchKeyWord.isEmpty && self.selectedCategory.description.isEmpty{
            self.searchLabel.attributedText = "\(self.searchKeyWord)".attributeStringWithAnotherColor(stringToColor: self.searchKeyWord)
        }else if self.searchKeyWord.isEmpty && !self.selectedCategory.description.isEmpty{
            self.searchLabel.text = "\(LocalizedString.Search.localized) in \(self.selectedCategory.description)"
        }
    }
    
    //MARK:- Configure Collectionview
    func configureCollectionView(){
        self.searchProductCollectionView.registerNib(nibName: SearchedProduceCollectionViewCell.defaultReuseIdentifier)
        self.searchProductCollectionView.registerNib(nibName: ProductCollectionViewCell.defaultReuseIdentifier)
        self.searchProductCollectionView.register(UINib(nibName: "HomeHeaserView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeHeaserView.reuseIdentifier)
        self.searchProductCollectionView.showsHorizontalScrollIndicator = false
        self.searchProductCollectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.setEmptyDataView(heading: LocalizedString.No_Data_Found.localized)
        self.setDataSourceAndDelegate(forScrollView: self.searchProductCollectionView)
        self.searchProductCollectionView.delegate = self
        self.searchProductCollectionView.dataSource = self
    }
    
    func setUpSortingDropDown(){
        
    }
}

//MARK:- Configure Navigation
private extension SearchedProductsVC {
    
    //MARK:- Configure Navigation
    func configureNavigation(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- Configure left bar button
    func configureLeftBarButtons() {
        let leftButton = UIBarButtonItem(image: AppImages.backBlack.image , style: .plain, target: self, action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    //MARK:- init navigation
    func initNavigationItemTitleView(title : String) {
        titleView.text = title
        titleView.font = AppFonts.Galano_Medium.withSize(15)
        titleView.textColor = UIColor.black
        let width = titleView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)).width
        titleView.frame = CGRect(origin:CGPoint.zero, size:CGSize(width: width, height: 500))
        self.navigationItem.titleView = titleView
        recognizer = UITapGestureRecognizer(target: self, action: #selector(titleTapped))
        titleView.isUserInteractionEnabled = true
        titleView.removeGestureRecognizer(recognizer ?? UITapGestureRecognizer())
        titleView.addGestureRecognizer(recognizer ?? UITapGestureRecognizer())
    }
    
    //MARK Title tapped
    @objc private func titleTapped() {
        let vc = SearchVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.searchFrom = .searchProducts
        vc.searchStr = self.searchKeyWord
        vc.selectedCategory = self.selectedCategory
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //MARK:- left bar button tapped
    @objc func likeButtonTapped(sender : UIButton){
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        guard let indexPath = sender.collectionViewIndexPath(self.searchProductCollectionView) else { return }
        
        self.controler.likeUnlikeProduct(product: self.products[indexPath.item])
        self.products[indexPath.item].isLiked =  !self.products[indexPath.item].isLiked
        self.searchProductCollectionView.reloadItems(at: [indexPath])
    }
    
    //MARK:- Sorting button tapped
    @IBAction func sortingButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        
        self.dropDown.anchorView = sender
        self.dropDown.dismissMode = .onTap
        self.dropDown.bottomOffset = CGPoint(x: 0, y:dropDown.anchorView!.plainView.bounds.height)
        self.dropDown.dismissMode = .automatic
        
        self.dropDown.dataSource = [LocalizedString.Newest.localized, LocalizedString.Closest.localized, LocalizedString.Price_High_To_Low.localized, LocalizedString.Price_Low_To_High.localized]
        
        if !AppNetworking.isConnectedToInternet {
            CommonFunctions.showToastMessage(LocalizedString.pleaseCheckInternetConnection.localized)
            return
        }
        
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            
            guard let weakSelf = self else { return }
            
            switch index {
                
            case 0:
                SelectedFilters.shared.appliedSortingOnProducts = .newest
                
            case 1:
                SelectedFilters.shared.appliedSortingOnProducts = .closest
                
            case 2:
                SelectedFilters.shared.appliedSortingOnProducts = .priceHightToLow
                
            case 3:
                SelectedFilters.shared.appliedSortingOnProducts = .priceLowToHigh
                
            default:
                SelectedFilters.shared.appliedSortingOnProducts = .newest
            }
            
            weakSelf.setSortingLabelText()
            weakSelf.products.removeAll()
            weakSelf.searchProductCollectionView.reloadData()
            weakSelf.resetPage()
            weakSelf.controler.getProducts(categoryId: weakSelf.selectedCategory.categoryId, searchText: weakSelf.searchKeyWord, page: weakSelf.page)
        }
    }
    
    //MARK:- Set sorting text
    func setSortingLabelText() {
        self.sortingTypeLabel.text = SelectedFilters.shared.setSortingAppliedText(sorting: SelectedFilters.shared.appliedSortingOnProducts)
    }
    
    func getSizeWhenSearchingApplied(_ collectionView: UICollectionView) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 10
        return CGSize(width: finalContentWidth / 2, height: (finalContentWidth / 2) + 47)
    }
    
    func getSizeWhenSearchingNotApplied(_ collectionView: UICollectionView) -> CGSize {
        let contentWidth = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        let finalContentWidth = contentWidth - 2
        return CGSize(width: finalContentWidth / 3, height: finalContentWidth / 3)
    }
    
}

//MARK:- CollectionView Datasource and Delegats
extension SearchedProductsVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HomeHeaserView", for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return self.isSearchApplied() ? CGSize(width: collectionView.frame.width, height: 0) : CGSize(width: collectionView.frame.width, height: 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.isSearchApplied() ? 10 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return self.isSearchApplied() ? 10 : 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return self.isSearchApplied() ? self.getSizeWhenSearchingApplied(collectionView) : self.getSizeWhenSearchingNotApplied(collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return self.isSearchApplied() ? self.getProductCellWhenSearchingApplied(collectionView, cellForItemAt: indexPath) : self.getProductCellWhenSearchingNotApplied(collectionView, cellForItemAt: indexPath)
    }
    
    func getProductCellWhenSearchingNotApplied(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ProductCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        cell.cellFor = .home
        cell.populateData(product: self.products[indexPath.item])
        return cell
    }
    
    func getProductCellWhenSearchingApplied(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchedProduceCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? SearchedProduceCollectionViewCell else { fatalError("Could not dequeue CategoryCell at index \(indexPath) in LoginVC") }
        cell.dropShadow()
        cell.populateData(product: self.products[indexPath.item])
        cell.likeButton.addTarget(self, action: #selector(likeButtonTapped), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.products.count >= 9  {
            if indexPath.item == self.products.count - 1 && self.nextPage != 0{
                self.controler.getProducts(categoryId: self.selectedCategory.categoryId, searchText: self.searchKeyWord, page: self.page)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ProductDetailVC.instantiate(fromAppStoryboard: AppStoryboard.Home)
        vc.prodId = self.products[indexPath.item].productId
        vc.likeStatusBackDeleate = self
        vc.prodReportedDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SearchedProductsVC : ProductReportedDelegate{
    
    func productReportedSuccessFully(prodId : String){
        
        if let index = self.products.lastIndex(where: { $0.productId == prodId}){
            self.products.remove(at: index)
            self.shouldDisplayEmptyDataView = true
            self.searchProductCollectionView.reloadData()
            self.searchProductCollectionView.reloadEmptyDataSet()
        }
    }
    
}


extension SearchedProductsVC : SelectedCategory  {
    //MARK:- select category
    func selectCategory(category: Category) {
        self.selectedCategory = category
        self.setSearchLabelText()
        self.resetPage()
        self.products.removeAll()
        self.searchProductCollectionView.reloadData()
        self.controler.getProducts(categoryId: self.selectedCategory.categoryId, searchText: self.searchKeyWord, page: self.page)
    }
    
}

extension SearchedProductsVC : SearchDelegate {
    func getSearchData(text: String) {
        self.searchKeyWord = text
        self.titleView.text = text
        self.titleLabel.text = text
        self.setSearchLabelText()
        self.resetPage()
        self.products.removeAll()
        self.searchProductCollectionView.reloadData()
        self.controler.getProducts(categoryId: self.selectedCategory.categoryId, searchText: self.searchKeyWord, page: self.page)
    }
}

//MARK:- Get ike status back delegate
extension SearchedProductsVC : GetLikeStatusBack {
    
    func getLikeStatus(product: Product) {
        if let index = self.products.firstIndex(where: { (obj) -> Bool in
            return obj.productId == product.productId
        }){
            self.products[index].isLiked = product.isLiked
            self.searchProductCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
        }
    }
    
}

//MARK:- Like unlike delegate
extension SearchedProductsVC  : LikeUnlikeDelegate {
    
    func willRequestLikeUnlikeProduct() {
        
    }
    
    func productLikeUnlikeSuccessFull(product: Product) {
        
    }
    
    func failedToLikeUnlikeProduct(message: String, product: Product) {
        
    }
}

//MARK:- Api delegate
extension SearchedProductsVC : SearchProductsDelegate{
    
    func productsReceivedSuccessFully(products : [Product], nextPage : Int){
        self.view.hideIndicator()
        
        if self.page != 0{
            self.page = nextPage
        }
        self.nextPage = nextPage
        self.products.append(contentsOf: products)
        self.searchProductCollectionView.reloadData()
        self.shouldDisplayEmptyDataView = true
        self.searchProductCollectionView.reloadEmptyDataSet()
    }
    
    func willRequestProducts() {
        if self.page == 1{
            self.shouldDisplayEmptyDataView = false
            self.searchProductCollectionView.reloadEmptyDataSet()
            self.view.showIndicator()
        }
    }
    
    func failedToReceiveProducts(message: String) {
        self.view.hideIndicator()
        self.shouldDisplayEmptyDataView = false
        self.searchProductCollectionView.reloadEmptyDataSet()
        CommonFunctions.showToastMessage(message)
    }
}

//MARK:- Apply filter
extension SearchedProductsVC : ApplyFilter {

    func applyFilter(){
        self.resetPage()
        self.products.removeAll()
        self.searchProductCollectionView.reloadData()
        self.controler.getProducts(categoryId: self.selectedCategory.categoryId, searchText: self.searchKeyWord, page: self.page)
    }
    
}
