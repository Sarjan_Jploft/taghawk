//
//  AppNavigator.swift
//  TagHawk
//
//  Created by Appinventiv on 18/01/19.
//  Copyright © 2019 TagHawk. All rights reserved.
//

import UIKit
import AMScrollingNavbar

enum DeeplinkType : String{
    case ResetPassword = "ResetPassword"
}

enum DeepLinkNavigateType: Int {
    case none = 0
    case EMAIL_VERIFICATION = 1
    case FORGOT_PASSWORD = 2
    case PRODUCT_SHARE = 3
    case COMMUNITY_INVITE = 4
    case COMMUNITY_REQUEST = 5
    case PROFILE_SHARE = 6
}

class AppNavigator {

    static let shared = AppNavigator()
    var parentNavigationControler = ScrollingNavigationController()
    var tabBar : TagHawkTabBarControler?

    
     func configureNavigationBarAppearance() {
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: AppFonts.Galano_Medium.withSize(15), NSAttributedString.Key.foregroundColor : UIColor.black]
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().backgroundColor = UIColor.white
        UINavigationBar.appearance().backIndicatorImage = AppImages.backWhite.image
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = AppImages.backWhite.image
        
    }
    
    /// Go To Login Screen
     func goToLogin() {
        
        let tutorialVc = TutorialParentVC.instantiate(fromAppStoryboard: .PreLogin)
        
       self.parentNavigationControler = ScrollingNavigationController(rootViewController: tutorialVc)
        self.parentNavigationControler.isNavigationBarHidden = true
//        self.parentNavigationControler.automaticallyAdjustsScrollViewInsets = false
        
     
        
        AppDelegate.shared.window?.rootViewController = self.parentNavigationControler
        
        AppDelegate.shared.window?.becomeKey()
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    
    func goToHomeVC() {
        
        let tabBarView = TagHawkTabBarControler.instantiate(fromAppStoryboard: .Home)
       
        let homeTab = HomeVC.instantiate(fromAppStoryboard: .Home)
        let navVC1 = ScrollingNavigationController(rootViewController: homeTab)
        navVC1.interactivePopGestureRecognizer?.isEnabled = true
        
        let hawkDriverTab = AccountInfoAndPaymentHistoryVC.instantiate(fromAppStoryboard: .Settings)
        let navVC2 = ScrollingNavigationController(rootViewController: hawkDriverTab)
        navVC2.interactivePopGestureRecognizer?.isEnabled = true
        
        let messageTab = MessageVC.instantiate(fromAppStoryboard: .Home)
        let navVC4 = ScrollingNavigationController(rootViewController: messageTab)
        navVC4.interactivePopGestureRecognizer?.isEnabled = true

        let profileTab = UserProfileVC.instantiate(fromAppStoryboard: .Home)
        let profileNavVC = ScrollingNavigationController(rootViewController: profileTab)
        
        self.tabBar = tabBarView
        tabBarView.viewControllers = [navVC1,navVC2,navVC4,profileNavVC]
        tabBar?.initTabbar()
        tabBarView.selectTab(index: 0)
        parentNavigationControler = ScrollingNavigationController(rootViewController: tabBarView)
        parentNavigationControler.isNavigationBarHidden = true
        parentNavigationControler.interactivePopGestureRecognizer?.isEnabled = true
        sharedAppDelegate.window?.rootViewController = parentNavigationControler
        sharedAppDelegate.window?.makeKeyAndVisible()
        
    }
    
    
    /// Go To Login Screen
    func goToChooseRoleVC() {
        let chooseRoleVc = ChooseUserTypeVC.instantiate(fromAppStoryboard: .PreLogin)
        self.parentNavigationControler = ScrollingNavigationController(rootViewController: chooseRoleVc)
        self.parentNavigationControler.isNavigationBarHidden = true
        self.parentNavigationControler.automaticallyAdjustsScrollViewInsets = false
        AppDelegate.shared.window?.rootViewController = self.parentNavigationControler
        AppDelegate.shared.window?.becomeKey()
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    
    func actionsOnLogout(comingFromGuest: Bool = false){
        UserProfile.main = UserProfile()
        FacebookController.shared.facebookLogout()
//        if comingFromGuest {
//            goToChooseRoleVC()
//        } else {
//            AppNavigator.shared.goToLogin()
//        }
        AppNavigator.shared.goToLogin()
        AppUserDefaults.removeAllValues()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func refreshAccountInfoTab(id : String){
        guard let navVcs = AppNavigator.shared.tabBar?.viewControllers else { return }
        guard let homeNavigation = navVcs[1] as? UINavigationController else { return }
        let allNavigationVcs = homeNavigation.viewControllers
        
        for item in allNavigationVcs {
            if item.isKind(of: AccountInfoAndPaymentHistoryVC.self){
                guard let obj = item as? AccountInfoAndPaymentHistoryVC else { return }
                if let history = obj.paymentHistoryVc  {
                    history.refreshProductStatus(entityId: id)
                }
                
                if let accountInfo = obj.accountInfoVc{
                    accountInfo.refresh()
                }
                
            }
        }
    }
    
}

